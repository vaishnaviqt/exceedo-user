<?php

use App\Models\PropertyList;
use App\Models\Subscription;
use App\Models\LandReport;
use App\Models\Review;
use App\Models\UserService;

if (!function_exists('smtpConnect')) {

    function smtpConnect() {

        try {
            $mail = config()->get('mail');
            $transport = new \Swift_SmtpTransport($mail['mailers']['smtp']['host'], $mail['mailers']['smtp']['port'], $mail['mailers']['smtp']['encryption']);
            $transport->setUsername($mail['mailers']['smtp']['username']);
            $transport->setPassword($mail['mailers']['smtp']['password']);
            $mailer = new \Swift_Mailer($transport);
            $mailer->getTransport()->start();
            $smtp = true;
        } catch (\Swift_TransportException $e) {
            $smtp = false;
        }
        return $smtp;
    }

}

if (!function_exists('getDateWiseData')) {

    function getDateWiseData($now = null, $last = null, $value = null) {
        $arr = array();
        $now = strtotime($now);
        $last = strtotime($last);

        while ($now < $last) {
            $arr[] = date('d', $now);
            $now = strtotime('+1 day', $now);
        }
        $adminPaidAppointment = [];
        $adminPaidDate = [];
        $arrayData = array_column($value->toArray(), 'count_appointment');

        foreach ($arr as $key => $date) {

            if (in_array($date, $arrayData)) {
                foreach ($value as $summary) {
                    if ($summary->count_appointment == $date) {
                        $adminPaidAppointment[$key] = $summary->count_date;
                        $adminPaidDate[$key] = $summary->count_appointment;
                    }
                }
            } else {
                $adminPaidAppointment[$key] = 0;
                $adminPaidDate[$key] = $date;
            }
        }

        $paid = array();
        $paid['paidappointment'] = $adminPaidAppointment = json_encode($adminPaidAppointment, JSON_NUMERIC_CHECK);
        $paid['date'] = $adminPaidDate = json_encode($adminPaidDate, JSON_NUMERIC_CHECK);

        return $paid;
    }

}


if (!function_exists('getMonthWiseData')) {

    function getMonthWiseData($value) {
        $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $adminPaidAppointment = [];
        $adminPaidDate = [];
        $arrayData = array_column($value->toArray(), 'count_appointment');
        foreach ($months as $key => $month) {
            if (in_array($month, $arrayData)) {
                foreach ($value as $summary) {
                    if ($summary->count_appointment == $month) {
                        $adminPaidAppointment[$key] = $summary->count_date;
                        $adminPaidDate[$key] = $summary->count_appointment;
                    }
                }
            } else {
                $adminPaidAppointment[$key] = 0;
                $adminPaidDate[$key] = $month;
            }
        }
        $paid = array();
        $paid['paidappointment'] = $adminPaidAppointment = json_encode($adminPaidAppointment, JSON_NUMERIC_CHECK);
        $paid['date'] = $adminPaidDate = json_encode($adminPaidDate, JSON_NUMERIC_CHECK);

        return $paid;
    }

    if (!function_exists('countLand')) {

        function countLand($status = null, $user = null) {
            $countlands = \DB::table('property_list');
            if (!empty($user) && $user->hasRole('user')) {
                $countlands->where('listed_by', $user->id);
            }
            if (!empty($user) && $user->hasRole('broker')) {
                $countlands->where('listed_by', $user->id);
            }
            if (!empty($user) && $user->hasRole('expert')) {
                $countlands->where('listed_by', $user->id);
            }
            if (!empty($status)) {
                if ($status == 'pending') {
                    $countlands->where('status', 0);
                } else if ($status == 'deleted') {
                    $countlands->whereNotNull('deleted_at');
                } else {
                    $countlands->where('status', $status);
                }
            }
            $countlands = $countlands->count();
            return $countlands;
        }

    }
}
if (!function_exists('expertListShow')) {

    function expertListShow($service = null) {
        try {
            $experts = App\Models\User::with('userService', 'state')->role('expert')->where('is_pending', 1)->get();
        } catch (\Swift_TransportException $e) {
            $experts = array();
        }
        return $experts;
    }

}
if (!function_exists('notificationData')) {

    function notificationData($value = null) {
        try {
            $noti = App\Models\Notification::where('unread', 0)->where('notification_to', $value)->where('user_id', auth()->user()->id)->get();
        } catch (\Swift_TransportException $e) {
            $noti = array();
        }
        return $noti;
    }

}
if (!function_exists('landReportCount')) {

    function landReportCount($status = null) {
        try {
            $reports = App\Models\LandReport::with('user');
            if (auth()->user()->hasRole('user')) {
                $reports->where('user_id', auth()->user()->id);
            }
            if (auth()->user()->hasRole('expert')) {
                $reports->where('expert_id', auth()->user()->id)->whereNull('cancel');
            }
            if (!empty($status)) {
                $status = ($status == 'pending') ? 0 : $status;
                $reports->where('status', $status);
            }
            $reports = $reports->count();
        } catch (\Swift_TransportException $e) {
            $reports = '';
        }
        return $reports;
    }

}

if (!function_exists('userRatings')) {

    function userRatings($rating = null) {
        if (empty($rating)) {
            return '<i class="far fa-star text-warning"></i><i class="far fa-star text-warning"></i><i class="far fa-star text-warning"></i><i class="far fa-star text-warning"></i><i class="far fa-star text-warning"></i>';
        }
        $html = "";
        switch ($rating) {
            case($rating == 5):
                $html = '<i class="fa fa-star text-warning"></i>
                <i class="fa fa-star text-warning"></i>
                <i class="fa fa-star text-warning"></i>
                <i class="fa fa-star text-warning"></i>
                <i class="fa fa-star text-warning"></i>';
                break;
            case($rating == 4):
                $html = '<i class="fa fa-star text-warning"></i>
                <i class="fa fa-star text-warning"></i>
                <i class="fa fa-star text-warning"></i>
                <i class="fa fa-star text-warning"></i>
                <i class="far fa-star text-warning"></i>';
                break;
            case($rating == 3):
                $html = '<i class="fa fa-star text-warning"></i>
                <i class="fa fa-star text-warning"></i>
                <i class="fa fa-star text-warning"></i>
                <i class="far fa-star text-warning"></i>
                <i class="far fa-star text-warning"></i>';
                break;
            case($rating == 2):
                $html = '<i class="fa fa-star text-warning"></i>
                <i class="fa fa-star text-warning"></i>
                <i class="far fa-star text-warning"></i>
                <i class="far fa-star text-warning"></i>
                <i class="far fa-star text-warning"></i>';
                break;
            case($rating == 1):
                $html = '<i class="fa fa-star text-warning"></i>
                <i class="far fa-star text-warning"></i>
                <i class="far fa-star text-warning"></i>
                <i class="far fa-star text-warning"></i>
                <i class="far fa-star text-warning"></i>';
                break;
        }
        return $html;
    }

}

if (!function_exists('stateListing')) {

    function stateListing() {
        try {
            $states = App\Models\State::where('country_id', 101)->get();
        } catch (\Swift_TransportException $e) {
            $states = '';
        }
        return $states;
    }

}
if (!function_exists('reportsCount')) {

    function reportsCount($status = null) {
        try {
            $reports = App\Models\LandReport::with('user');
            if (!empty($status)) {
                $status = ($status == 'pending') ? 0 : $status;
                $reports->where('status', $status);
            }
            $reports = $reports->count();
        } catch (\Swift_TransportException $e) {
            $reports = '';
        }
        return $reports;
    }

}
if (!function_exists('expertPendingProject')) {

    function expertPendingProject() {
        try {
            $reports = App\Models\LandReport::with('user', 'state', 'expert')->where('status', 0)->whereNull('cancel')->where('expert_id', auth()->user()->id)->get();
        } catch (\Swift_TransportException $e) {
            $reports = '';
        }
        return $reports;
    }

}
if (!function_exists('checkUserSubscription')) {

    function checkUserSubscription() {
        $subscription = '';
        try {
            if (Auth::check()) {
                $subscription = Subscription::with('detail', 'package')->where('user_id', auth()->user()->id)->first();
            }
        } catch (\Swift_TransportException $e) {
            $subscription = '';
        }
        return $subscription;
    }

}
if (!function_exists('showRatingReport')) {

    function showRatingReport($expert) {
        $ratingsReport = '';
        try {
            $landReport = LandReport::where('status', 2)->where('expert_id', $expert)->count();
            $review = Review::leftJoin('appointments as ap', 'ap.id', '=', 'reviews.appointment_id')
                            ->where('ap.expert_id', 4)->get();
            $avgRating = '';
            if (count($review)) {
                $total = count($review);
                $ratings = array_sum(array_column($review->toArray(), 'rating'));
                $avgRating = $ratings / $total;
            }
            $ratingsReport = 'Assignment (' . $landReport . ') Ratings(' . $avgRating . ')';
        } catch (\Swift_TransportException $e) {
            $ratingsReport = '';
        }
        return $ratingsReport;
    }

}
if (!function_exists('userValueAddedService')) {

    function userValueAddedService() {
        $service = '';
        try {
            $userServices = UserService::with('service')->where('user_id', auth()->user()->id)->get();
            if (count($userServices)) {
                $service = implode(', ', array_column(array_column($userServices->toArray(), 'service'), 'service_name'));
            }
        } catch (\Swift_TransportException $e) {
            $service = '';
        }
        return $service;
    }

}
if (!function_exists('pendingServiceInquiry')) {

    function pendingServiceInquiry() {
        $service = '';
        try {
            $service = \App\Models\ServiceEnquiry::where('expert_id', auth()->user()->id)->where('status', 0)->whereNull('cancel')->get();
        } catch (\Swift_TransportException $e) {
            $service = '';
        }
        return $service;
    }

}


if (!function_exists('managersList')) {

    function managersList($service = null) {
        try {
            $managers = App\Models\Manager::all();
        } catch (\Swift_TransportException $e) {
            $managers = array();
        }
        return $managers;
    }

}
if (!function_exists('secondaryAdminPermission')) {

    function secondaryAdminPermission($manager_id = null) {
        try {
            $managers = App\Models\ModelHasPermission::where('manager_id', $manager_id)->where('model_id', auth()->user()->id)->first();
        } catch (\Swift_TransportException $e) {
            $managers = array();
        }
        return $managers;
    }

}
