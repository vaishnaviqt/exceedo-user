<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\PropertyList;
use Intervention\Image\ImageManagerStatic as Image;
use Mail;
use App\Models\ValueAddedService;
use App\Models\Appointment;
use App\Models\AgentContact;
use App\Models\ServiceEnquiry;
use App\Models\User;
use App\Mail\UserCreate;
use App\Mail\ReportRequest;
use App\Mail\UserReportRequest;
use App\Mail\AdminReceiveEnquiry;
use Illuminate\Support\Facades\Hash;
use App\Models\State;
use App\Models\LandReport;
use Razorpay\Api\Api;
use App\Models\Payment;
use DB;
use App\Models\Notification;
use App\Models\Package;
use App\Models\Subscription;
use App\Models\SubscriptionDetail;
use App\Mail\AdminLandRequest;
use App\Mail\UserLandRequest;
use Auth;

class HomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $properties = PropertyList::with('propertyImage', 'propertyOtherImage')->where('status', 1)->where('featured', 1)->paginate(6);
        $valueAddedServices = ValueAddedService::where('is_service', 0)->get();
        $cookie_name = "splashscreen";
        $cookie_value = "show";
        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
        if (!isset($_COOKIE[$cookie_name])) {
            $className = 'show';
        } else {
            $className = '';
        }
        return view('home', compact('valueAddedServices', 'properties', 'className'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendEmail(Request $request)
    {
        //
        $rules = [
            'c_name' => 'required|regex:/^[a-z\s]+$/i|max:255',
            'c_email' => 'required|email',
            'c_mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10',
            'c_message' => 'required|max:255',
        ];

        $messages = [
            'c_name.required' => 'Name field is required.',
            'c_email.required' => 'The email field is required.',
            'c_email.email' => 'The email must be a valid email address.',
            'c_mobile.required' => 'The mobile number field is required.',
            'c_message.required' => 'The Message field is required.',
            'c_mobile.regex' => 'The mobile number format is invalid.',
            'c_mobile.min' => 'The mobile number must be at least 10 characters.',
            'c_mobile.max' => 'The mobile number may not be greater than 10 characters.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => 'There are incorect values in the form!',
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }

        $c_name = trim($request->c_name);
        $c_email = trim($request->c_email);
        $c_mobile = trim($request->c_mobile);
        $c_message = trim($request->c_message);
        $data = array('c_name' => $c_name, 'c_email' => $c_email, 'c_mobile' => $c_mobile, 'c_message' => $c_message);
        Mail::send('mail.mail', $data, function ($message) {
            $message->to(config('mail.from.admin_email'), config('app.name'))->subject(env('EMAIL_SUBJECT'));
            $message->from(config('mail.from.address'), config('mail.from.name'));
        });

        if (Mail::failures()) {
            return response()->json(['success' => false, 'msg' => "Something went wrong."]);
        } else {
            return response()->json(['success' => true, 'msg' => "Query submitted Successfully."]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // render sell land page
    public function sellland()
    {
        return view('sellland');
    }

    // render sell land form page
    public function selllandform(Request $request)
    {
        $states = State::where('country_id', 101)->get();
        $value = $request;

        if (!empty($request->all())) {
            return view('selllandform', compact('value', 'states'));
        } else {
            return view('sellland');
        }
        return view('selllandform');
    }


    public function storeproperty(Request $request)
    {
        $userId = auth()->user()->id ?? $request->input('user_id');
        $path = '';
        $api = new Api(config('app.razorpay_api_key'), config('app.seceret_key'));
        //Fetch payment information by razorpay_payment_id
        $payment = $api->payment->fetch($request->input('razorpay_payment_id'));

        if (!empty($payment) && $payment['status'] == 'captured' || $payment['status'] == 'authorized') {
            $paymentId = $payment['id'];
            $amount = $payment['amount'];
            $currency = $payment['currency'];
            $status = $payment['status'];
            $entity = $payment['entity'];
            $orderId = $payment['order_id'];
            $invoiceId = $payment['invoice_id'];
            $method = $payment['method'];
            $bank = $payment['bank'];
            $wallet = $payment['wallet'];
            $bankTranstionId = isset($payment['acquirer_data']['bank_transaction_id']) ? $payment['acquirer_data']['bank_transaction_id'] : '';
        }

        // image upload
        $property_list = new PropertyList();
        $property_image = $request->file('file');
        $path = '';
        if ($request->file) {
            $statement = DB::select("SHOW TABLE STATUS LIKE 'property_list'");
            $nextId = (isset($statement[0]->Auto_increment)) ? $statement[0]->Auto_increment : uniqid();
            $file_name = $userId . '_' . $nextId;
            $fileName = $file_name . '.' . $request->file('file')->getClientOriginalExtension();
            $path = 'propertyImage' . '/' . $request->file('file')->storeAs($userId, $fileName, 'propertyImage');
        }

        try {
            $property_list->image = $path;
            $property_list->listed_by = $userId;
            $property_list->listing_by = trim($request->listing_by);
            $property_list->address = trim($request->user_address);
            $property_list->is_corporate = trim($request->is_corporate);
            $property_list->gst = trim($request->user_gst);
            $property_list->pan = trim($request->user_pan);
            $property_list->business_type = trim($request->business_type);
            $property_list->min_price = trim($request->min_price);
            $property_list->max_price = trim($request->max_price);
            $property_list->latitute = trim($request->latitute);
            $property_list->longitude = trim($request->longitude);
            $property_list->exclusive_channel_partner = trim($request->exclusive_channel_partner);
            $property_list->sign_exclusive_mandate = trim($request->exclusive_mandate);
            $property_list->land_to_get_verified = trim($request->land_to_get_verified);
            $saved = $property_list->save();
            $payment = new Payment;
            $payment->transaction_id = $paymentId;
            $payment->property_list_id = $property_list->id;
            $payment->amount = $amount / 100;
            $payment->currency = $currency;
            $payment->entity = $entity;
            $payment->status = $status;
            $payment->order_id = $orderId;
            $payment->method = $method;
            $payment->bank = $bank;
            $payment->wallet = $wallet;
            $payment->bank_transaction_id = $bankTranstionId;
            $payment->save();
            $list_id = $property_list->id;
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            if (smtpConnect() == true) {
                $property = PropertyList::with('listed')->where('id', $property_list->id)->first();
                $property->amount = $payment->amount;
                Mail::to(config('mail.from.admin_email'))->send(new AdminLandRequest($payment->amount));
                Mail::to($property->listed->email)->send(new UserLandRequest($property));
            }
            return response()->json(['success' => true, 'message' => "Property listed successfully.", 'data' => array('list_id' => $list_id)]);
        } else {
            return response()->json(['error' => true, 'message' => "Something went wrong, Please try again later!", 'data' => array('list_id' => $list_id)]);
        }
    }

    /**
     * show property list
     * @return type
     */
    public function showBuyYourLand(Request $request)
    {
        $propertyList = PropertyList::with('propertyImage')->where('status', 1);
        $range1 = $request->range_1 ?? '';
        $range2 = $request->range_2 ?? '';
        $name = $request->name ?? '';
        $businessType = $request->businesstype ?? '';
        $landArea = $request->land_area ?? '';
        $filter = $request->filter ?? '';
        if (!empty($range1) && !empty($range2)) {
            $propertyList->whereBetween('max_price',  [$range1, $range2]);
        }


        if (!empty($landArea)) {
            // $area = explode('-', $landArea);
            if ($landArea == 50) {
                $propertyList->where('size_of_land', '>=', $landArea);
            } else {
                $area = explode('-', $landArea);
                $propertyList->whereBetween('size_of_land', [$area[0], $area[1]]);
            }
        }
        if (!empty($businessType)) {
            $propertyList->where('business_type', $businessType);
        }
        if (!empty($name)) {
            $propertyList->where('address', 'LIKE', '%' . $name . '%');
        }

        if (!empty($filter) && $filter  == 'price-low-to-high') {
            $propertyList->orderBy('price_unit', 'asc');
        }
        if (!empty($filter) && $filter  == 'price-high-to-low') {
            $propertyList->orderBy('price_unit', 'desc');
        }
        $propertyLists = $propertyList->paginate(6);
        $properties = $propertyList->get();
        if ($request->ajax()) {
            $view = view('buylanddata', compact('propertyLists'))->render();
            return response()->json(['htmll' => $view]);
        }


        return view('buyland', compact('propertyLists', 'range1', 'range2', 'name', 'businessType', 'landArea', 'properties', 'filter'));
    }

    public function showFeasibilityAnalysis()
    {
        return view('feasibility-analysis');
    }

    public function ShowFeasibilityAnalysisForm(Request $request)
    {
        $states = State::where('country_id', 101)->get();
        $value = $request;

        if (!empty($request->all())) {
            return view('feasibility-analysis-form', compact('value', 'states'));
        } else {
            return view('feasibility-analysis');
        }
    }

    public function showLandTitleSearch()
    {
        return view('land-title-search');
    }

    public function showAboutUs()
    {
        $valueAddedServices = ValueAddedService::where('is_service', 0)->take(10)->get();
        return view('aboutus', compact('valueAddedServices'));
    }

    public function ShowMeetTheExpert()
    {

        return view('meet-the-expert');
    }

    public function ShowTermsConditions()
    {
        return view('term-condition');
    }

    public function ShowPrivacyPolicy()
    {
        return view('privacy-policy');
    }

    public function ShowCookiePolicy()
    {
        return view('cookie-policy');
    }

    public function ShowLandReportAnlysisForm()
    {
        return view('land-report-anlysis-form');
    }

    public function ShowLandTitleSearchForm(Request $request)
    {
        $states = State::where('country_id', 101)->get();
        $value = $request;

        if (!empty($request->all())) {
            return view('land-title-search-form', compact('value', 'states'));
        } else {
            return redirect('land-title-search');
        }
    }

    public function ShowBookYourAppointment()
    {
        $services = ValueAddedService::all();
        // @dd(Auth::user());
        return view('book-your-appointment', compact('services'));
    }

    public function LandAppointmentValidition(Request $request)
    {

        $rules = [
            'value_added_service_id' => 'required',
            'date' => 'required|after:' . now(),
            'time' => 'required',
            'remarks' => 'required',
            'terms' => 'required',
        ];

        $messages = [
            'value_added_service_id.required' => 'The Value added service field is required',
            'date.required' => 'The date field is required.',
            'time.required' => 'The time field is required.',
            'remarks.required' => 'The remarks Field is required.',
        ];

        if (!Auth::check()) {
            $rules['name'] = 'required|max:255';
            $rules['email'] = 'required|email|unique:users';
            $rules['mobile'] = 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10|unique:users';

            $messages['name.required'] = 'The name field is required';
            $messages['email.required'] = 'he email field is required.';
            $messages['email.email'] = 'The email must be a valid email address.';
            $messages['mobile.required'] = 'The mobile number field is required.';
            $messages['mobile.regex'] = 'The mobile number format is invalid.';
            $messages['mobile.min'] = 'The mobile number must be at least 10 characters.';
            $messages['mobile.max'] = 'The mobile number may not be greater than 10 characters.';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => 'There are incorect values in the form!',
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }
        $user = auth()->user();
        if (!Auth::check() && !empty($request->input('user_id'))) {
            $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
            $password = substr($random, 0, 10);
            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->mobile = $request->input('mobile');
            $user->password = Hash::make($password);
            $saved = $user->save();
            $user->assignRole('user');
            if ($saved && smtpConnect() == true) {
                $userDetail['name'] = $user->name;
                $userDetail['email'] = $user->email;
                $userDetail['password'] = $password;
                Mail::to($user->email)->send(new UserCreate($userDetail));
            }
        }
        $api = new Api(config('app.razorpay_api_key'), config('app.seceret_key'));
        $order = $api->order->create(array('receipt' => 'order_rcptid_11', 'amount' => $request->input('amount') * 100, 'currency' => 'INR'));

        return response()->json(['user' => $user, 'order_id' => $order['id']]);
    }

    public function LandAppointmentStore(Request $request)
    {

        $userId = auth()->user()->id ?? $request->input('user_id');
        $path = '';
        $api = new Api(config('app.razorpay_api_key'), config('app.seceret_key'));
        //Fetch payment information by razorpay_payment_id
        $payment = $api->payment->fetch($request->input('razorpay_payment_id'));
        if (!empty($payment) && $payment['status'] == 'captured' || $payment['status'] == 'authorized') {
            $paymentId = $payment['id'];
            $amount = $payment['amount'];
            $currency = $payment['currency'];
            $status = $payment['status'];
            $entity = $payment['entity'];
            $orderId = $payment['order_id'];
            $invoiceId = $payment['invoice_id'];
            $method = $payment['method'];
            $bank = $payment['bank'];
            $wallet = $payment['wallet'];
            $bankTranstionId = isset($payment['acquirer_data']['bank_transaction_id']) ? $payment['acquirer_data']['bank_transaction_id'] : '';
        } else {
            return redirect()->back()->with('error', 'Something went wrong, Please try again later!');
        }

        if (!empty($request->file('image'))) {
            $statement = DB::select("SHOW TABLE STATUS LIKE 'land_reports'");
            $nextId = (isset($statement[0]->Auto_increment)) ? $statement[0]->Auto_increment : uniqid();
            $file_name = trim($request->input('land_name')) . '_' . $nextId;
            $fileName = $file_name . '.' . $request->file('image')->getClientOriginalExtension();
            $path = 'land_report_image' . '/' . $request->file('image')->storeAs($userId, $fileName, 'landReport');
        }

        try {
            $Appointment = new Appointment;

            $Appointment->value_added_service_id = $request->input('value_added_service_id');
            $Appointment->remarks = $request->input('remarks');
            $Appointment->date = $request->input('date');
            $Appointment->time = $request->input('time');
            $Appointment->user_id = $request->input('user_id');
            $Appointment->meeting_at = $request->input('date') . ' ' . $request->input('time');
            $Appointment->payment_id = $paymentId;
            $Appointment->status = 0;

            $saved = $Appointment->save();

            $payment = new Payment;
            $payment->transaction_id = $paymentId;
            $payment->appointment_id = $Appointment->id;
            $payment->amount = $amount / 100;
            $payment->currency = $currency;
            $payment->entity = $entity;
            $payment->status = $status;
            $payment->order_id = $orderId;
            $payment->method = $method;
            $payment->bank = $bank;
            $payment->wallet = $wallet;
            $payment->bank_transaction_id = $bankTranstionId;
            $payment->save();
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            return response()->json(['success' => true, 'msg' => "Your appointment request submit successfully, Please check your mail"]);
        } else {
            return response()->json(['success' => false, 'msg' => "Some thing went wrong, Please try again"]);
        }
    }

    public function propertyDetail(Request $request)
    {
        $propertyList = PropertyList::with('propertyImage', 'agent', 'propertyOtherImage')->where('id', $request->id)->first();
        $propertyList->views = $propertyList->views + 1;
        $propertyList->save();
        $html = '';
        if (!empty($propertyList)) {
            $html = view('_property-detail-popup', compact('propertyList'))->render();
        }

        return response($html);
    }

    public function storeServices(Request $request)
    {

        $rules = [
            'name' => 'required|regex:/^[a-z\s]+$/i|max:255',
            'email' => 'required|email',
            'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10',
            'comments' => 'required',
        ];

        $messages = [
            'name.required' => 'The name field is required',
            'email.required' => 'The email field is required.',
            'email.email' => 'The email must be a valid email address.',
            'mobile.required' => 'The mobile number field is required.',
            'mobile.regex' => 'The mobile number format is invalid.',
            'mobile.min' => 'The mobile number must be at least 10 characters.',
            'mobile.max' => 'The mobile number may not be greater than 10 characters.',
            'comments.required' => 'The comment field is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => 'There are incorect values in the form!',
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }
        $service = new ServiceEnquiry;
        $service->value_added_service_id = $request->value_added_service_id;
        $service->name = $request->name;
        $service->email = $request->email;
        $service->mobile = $request->mobile;
        $service->comments = $request->comments;
        if (Auth::user()) {
            $service->user_id = Auth::user()->id;
        }

        $serviceData = $service->save();

        $noti = new Notification;
        $noti->notification_to = 'admin';
        $noti->service_enquiry_id = $service->id;
        $noti->user_id = 1;
        $noti->message = "You recive new service enquiry";
        $noti->save();
        if ($serviceData) {
            if (smtpConnect() == true) {
                Mail::to(config('mail.from.admin_email'))->send(new AdminReceiveEnquiry($serviceData));
            }
            return response()->json(['success' => true, 'msg' => "Service Enquiry Successfully added."]);
        }
    }

    /**
     * Agent Contact us Form detail store
     * 
     * @param Request $request
     * @return type
     */
    public function agentContact(Request $request)
    {
        $rules = [
            'name' => 'required|regex:/^[a-z\s]+$/i|max:255',
            'email' => 'required|regex:/(.+)@(.+)\.(.+)/i|max:191|',
            'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10',
        ];

        $messages = [
            'name.required' => 'The name field is required',
            'email.required' => 'The email field is required.',
            'email.regex' => 'The email must be a valid email address.',
            'email.max' => 'The email must be contain 191 characters.',
            'mobile.required' => 'The mobile number field is required.',
            'mobile.regex' => 'The mobile number format is invalid.',
            'mobile.min' => 'The mobile number must be at least 10 characters.',
            'mobile.max' => 'The mobile number may not be greater than 10 characters.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => 'There are incorect values in the form!',
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }
        $agentContact = new AgentContact;
        $agentContact->land_id = $request->land_id;
        $agentContact->name = $request->name;
        $agentContact->email = $request->email;
        $agentContact->mobile = $request->mobile;
        if (Auth::user()) {
            $agentContact->user_id = Auth::user()->id;
        }
        $agentData = $agentContact->save();
        if ($agentData) {
            return response()->json(['success' => true, 'msg' => "Query submitted Successfully."]);
        } else {
            return response()->json(['success' => false, 'msg' => "Something went wrong."]);
        }
    }

    /**
     * land report validation
     * 
     * @param Request $request
     * @return type
     */
    public function landReportValidation(Request $request)
    {
        $rules = [
            'land_name' => 'required',
            'address' => 'required',
            'state_id' => 'required',
            'city' => 'required',
            'analysis_report' => 'required',
            'selling_more' => 'required',
            'terms' => 'required',
        ];

        $messages = [
            'land_name.required' => 'The Land name field is required',
            'address.required' => 'The Address field is required.',
            'state_id.required' => 'The State field is required.',
            'city.required' => 'The City Field is required.',
            'terms.required' => 'The Privacy Policy field is required.',
        ];
        if (!Auth::check() && empty($request->input('user_id'))) {
            $rules['name'] = 'required|max:255';
            $rules['email'] = 'required|email|unique:users';
            $rules['mobile'] = 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10|unique:users';

            $messages['name.required'] = 'The name field is required';
            $messages['email.required'] = 'he email field is required.';
            $messages['email.email'] = 'The email must be a valid email address.';
            $messages['mobile.required'] = 'The mobile number field is required.';
            $messages['mobile.regex'] = 'The mobile number format is invalid.';
            $messages['mobile.min'] = 'The mobile number must be at least 10 characters.';
            $messages['mobile.max'] = 'The mobile number may not be greater than 10 characters.';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => 'There are incorect values in the form!',
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }
        $user = auth()->user();
        if (!Auth::check() && empty($request->input('user_id'))) {
            $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
            $password = substr($random, 0, 10);
            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->mobile = $request->input('mobile');
            $user->password = Hash::make($password);
            $saved = $user->save();
            $user->assignRole('user');
            if ($saved && smtpConnect() == true) {
                $userDetail['name'] = $user->name;
                $userDetail['email'] = $user->email;
                $userDetail['password'] = $password;
                Mail::to($user->email)->send(new UserCreate($userDetail));
            }
        }
        $api = new Api(config('app.razorpay_api_key'), config('app.seceret_key'));
        $order = $api->order->create(array('receipt' => 'order_rcptid_11', 'amount' => $request->input('amount') * 100, 'currency' => 'INR'));
        return response()->json(['user' => $user, 'order_id' => $order['id']]);
    }

    /**
     * land report detail add
     * 
     * @param Request $request
     * @return type
     */
    public function landReportStore(Request $request)
    {
        //        dd($request->all());
        $userId = $request->input('user_id');
        $path = '';
        $api = new Api(config('app.razorpay_api_key'), config('app.seceret_key'));
        //Fetch payment information by razorpay_payment_id
        $payment = $api->payment->fetch($request->input('razorpay_payment_id'));
        if (!empty($payment) && $payment['status'] == 'captured' || $payment['status'] == 'authorized') {
            $paymentId = $payment['id'];
            $amount = $payment['amount'];
            $currency = $payment['currency'];
            $status = $payment['status'];
            $entity = $payment['entity'];
            $orderId = $payment['order_id'];
            $invoiceId = $payment['invoice_id'];
            $method = $payment['method'];
            $bank = $payment['bank'];
            $wallet = $payment['wallet'];
            $bankTranstionId = isset($payment['acquirer_data']['bank_transaction_id']) ? $payment['acquirer_data']['bank_transaction_id'] : '';
        } else {
            return redirect()->back()->with('error', 'Something went wrong, Please try again later!');
        }

        if (!empty($request->file('image'))) {
            $statement = DB::select("SHOW TABLE STATUS LIKE 'land_reports'");
            $nextId = (isset($statement[0]->Auto_increment)) ? $statement[0]->Auto_increment : uniqid();
            $file_name = trim($request->input('land_name')) . '_' . $nextId;
            $fileName = $file_name . '.' . $request->file('image')->getClientOriginalExtension();
            $path = 'land_report_image' . '/' . $request->file('image')->storeAs($userId, $fileName, 'landReport');
        }

        try {
            $landReport = new LandReport;
            $landReport->user_id = $userId;
            $landReport->land_name = $request->input('land_name');
            $landReport->address = $request->input('address');
            $landReport->state_id = $request->input('state_id');
            $landReport->city = $request->input('city');
            $landReport->analysis_report = $request->input('analysis_report') ?? 0;
            $landReport->selling_more = $request->input('selling_more') ?? 0;
            $landReport->terms = $request->input('terms') ?? 0;
            $landReport->size_of_area = $request->input('size_of_area');
            $landReport->amount = $request->input('amount');
            $landReport->value_added_service = $request->input('service_id');
            $landReport->image = $path;
            $landReport->status = 0;
            $saved = $landReport->save();
            $noti = new Notification;
            $noti->notification_to = 'admin';
            $noti->land_report_id = $landReport->id;
            $noti->user_id = 1;
            $noti->message = "You recive new land report request";
            $noti->save();
            $payment = new Payment;
            $payment->transaction_id = $paymentId;
            $payment->land_report_id = $landReport->id;
            $payment->amount = $amount / 100;
            $payment->currency = $currency;
            $payment->entity = $entity;
            $payment->status = $status;
            $payment->order_id = $orderId;
            $payment->method = $method;
            $payment->bank = $bank;
            $payment->wallet = $wallet;
            $payment->bank_transaction_id = $bankTranstionId;
            $payment->save();
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            if (smtpConnect() == true) {
                //                Mail::to('user-email')->send(new UserReportRequest($payment));
                Mail::to(config('mail.from.admin_email'))->send(new ReportRequest($payment));
            }
            return redirect('land-title-search')->withErrors(['landReport' => true, 'success', 'Thank you for showing interest in Know Your Land (KYL) services. Your land report has been successfully submitted. The KYL team will get back to you shortly. Please visit your inbox for more details on your query']);
        } else {
            return redirect('land-title-search')->withErrors(['landReport' => false, 'error', 'Something went wrong, Please try again later!']);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function storePropertyValidation(Request $request)
    {
        $rules = [
            'listing_by' => 'required',
            'user_address' => 'required|max:255',
            'business_type' => 'required',
            'user_pan' => 'required|regex:/[A-Z]{5}[0-9]{4}[A-Z]{1}$/',
            'is_corporate' => 'required',
            'min_price' => 'required',
            // 'user_gst' => 'regex:^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]1}[1-9A-Z]{1}Z[0-9A-Z]{1}$',
            'max_price' => 'required',
            'exclusive_channel_partner' => 'required',
            // 'exclusive_mandate' => 'required',
            // 'land_to_get_verified' => 'required',
            'file' => 'required|mimes:jpeg,png,jpg,bmp,gif,doc,docx,odt,pdf,zip|max:5120',
        ];

        $messages = [
            'listing_by.required' => 'owner or broker is required.',
            'user_address.required' => 'The address field is required.',
            'user_gst.required' => 'The GST field is required.',
            'business_type.required' => 'What would you do with your property field is required',
            'user_pan.required' => 'The PAN field is required.',
            'is_corporate.required' => 'Company or Individual field is required.',
            'min_price.required' => 'Asking Pricing for Sale field is required.',
            'max_price.required' => 'Asking Pricing for Sale field is required.',
        ];
        if ($request->input('is_corporate') == 'company') {
            $rules['user_gst'] = 'required|regex:^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]1}[1-9A-Z]{1}Z[0-9A-Z]{1}$';
        }


        if (!Auth::check()) {
            $rules['user_name'] = 'required|max:255';
            $rules['user_mobile'] = 'required|unique:users,mobile|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10';
            $rules['user_email'] = 'required|email|unique:users,email|email:rfc,dns';
            $messages['user_name.required'] = 'The name field is required';
            $messages['user_email.required'] = 'The email field is required.';
            $messages['user_email.email'] = 'The email must be a valid email address.';
            $messages['user_mobile.required'] = 'The mobile number field is required.';
            $messages['user_mobile.regex'] = 'The mobile number format is invalid.';
            $messages['user_mobile.min'] = 'The mobile number must be at least 10 characters.';
            $messages['user_mobile.max'] = 'The mobile number may not be greater than 10 characters.';
        }
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => 'There are incorect values in the form!',
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }

        $user = auth()->user();
        if (!Auth::check() && empty($request->input('user_id'))) {
            $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
            $password = substr($random, 0, 10);
            $user = new User;
            $user->name = $request->input('user_name');
            $user->email = $request->input('user_email');
            $user->mobile = $request->input('user_mobile');
            $user->password = Hash::make($password);
            $saved = $user->save();
            $user->assignRole('user');
            if ($saved && smtpConnect() == true) {
                $userDetail['name'] = $user->name;
                $userDetail['email'] = $user->email;
                $userDetail['password'] = $password;
                Mail::to($user->email)->send(new UserCreate($userDetail));
            }
        }
        $api = new Api(config('app.razorpay_api_key'), config('app.seceret_key'));
        $order = $api->order->create(array('receipt' => 'order_rcptid_11', 'amount' => $request->input('amount') * 100, 'currency' => 'INR'));

        return response()->json(['user_id' => $user->id, 'order_id' => $order['id']]);
    }

    /**
     * 
     * @return type
     */
    public function userSubscription()
    {
        $properties = PropertyList::where('status', 1)->get();
        $packages = Package::where('status', 1)->get();
        return view('subscription', compact('packages', 'properties'));
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function userSubscriptionTokenGenerate(Request $request)
    {
        $package = Package::where('id', $request->input('packageId'))->first();
        $api = new Api(config('app.razorpay_api_key'), config('app.seceret_key'));
        $order = $api->order->create(array('receipt' => 'order_rcptid_11', 'amount' => $package->price * 100, 'currency' => 'INR')); // Creates order
        return response()->json(['order_id' => $order['id'], 'amount' => $package->price]);
    }

    public function userSubscriptionPayment(Request $request)
    {
        $api = new Api(config('app.razorpay_api_key'), config('app.seceret_key'));
        //Fetch payment information by razorpay_payment_id
        $payment = $api->payment->fetch($request->input('razorpay_payment_id'));
        if (!empty($payment) && $payment['status'] == 'captured') {
            $paymentId = $payment['id'];
            $amount = $payment['amount'];
            $currency = $payment['currency'];
            $status = $payment['status'];
            $entity = $payment['entity'];
            $orderId = $payment['order_id'];
            $invoiceId = $payment['invoice_id'];
            $method = $payment['method'];
            $bank = $payment['bank'];
            $wallet = $payment['wallet'];
            $bankTranstionId = isset($payment['acquirer_data']['bank_transaction_id']) ? $payment['acquirer_data']['bank_transaction_id'] : '';
        } else {
            return response()->json(['error' => true, 'msg' => "Something went wrong, Please try again later!"]);
        }
        $package = Package::where('id', $request->input('packageId'))->first();
        $subsc = new Subscription;
        $subsc->package_id = $package->id;
        $subsc->user_id = $request->user()->id;
        $subsc->start_date = date('Y-m-d');
        $duration = $package->duration;
        $subsc->end_date = date('Y-m-d', strtotime("+$duration months", strtotime(now())));
        $saved = $subsc->save();

        $propertyId = explode(',', $request->property);
        foreach ($propertyId as $id) {
            $subscDetails = new SubscriptionDetail;
            $subscDetails->property_list_id = $id;
            $subscDetails->subscription_id = $subsc->id;
            $subscDetails->save();
        }

        // Payment detail save in database
        $payment = new Payment;
        $payment->transaction_id = $paymentId;
        $payment->subscription_id = $subsc->id;
        $payment->amount = $amount / 100;
        $payment->currency = $currency;
        $payment->entity = $entity;
        $payment->status = $status;
        $payment->order_id = $orderId;
        $payment->method = $method;
        $payment->bank = $bank;
        $payment->wallet = $wallet;
        $payment->bank_transaction_id = $bankTranstionId;
        $payment->save();
        if ($saved) {
            return response()->json(['success' => true, 'msg' => "Your subscription package actiated"]);
        } else {
            return response()->json(['error' => true, 'msg' => "Something went wrong, Please try again later!"]);
        }
    }
}
