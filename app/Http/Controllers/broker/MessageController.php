<?php

namespace App\Http\Controllers\broker;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Mail\ExpertMessage;
use Carbon\Carbon;
use App\Models\Message;
use Mail;

class MessageController extends Controller {

    //

    public function index(Request $request) {
        $message = Message::where('status', 1)->where('sender_id', auth()->user()->id);
        $showBy = $request->input('show_by') ?? '';
        if (!empty($showBy) && $showBy == 'week') {
            $message->where('created_at', '>', Carbon::now()->startOfWeek())->where('created_at', '<', Carbon::now()->endOfWeek());
        }
        if (!empty($showBy) && $showBy == 'month') {
            $message->where('created_at', '>', Carbon::now()->startOfMonth())->where('created_at', '<', Carbon::now()->endOfMonth());
        }

        if (!empty($showBy) && $showBy == 'year') {
            $message->whereYear('created_at', Carbon::now()->year);
        }
        $messages = $message->orderBy('created_at', 'desc')->paginate(10);
        return view('broker.messages', compact('messages', 'showBy'));
       
    }

    /**
     * message store and mail send
     * 
     * @param Request $request
     * @return type
     */
    public function messageSend(Request $request) {
        $validator = Validator::make($request->all(), [
                    'message' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        try {
            $message = new Message;
            $message->sender_id = auth()->user()->id;
            $message->user_id = 1;
            $message->message = $request->input('message');
            $message->status = 1;
            $saved = $message->save();
            if ($saved) {
                Mail::to(config('mail.from.address'))->send(new ExpertMessage($message));
            }
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            return redirect()->back()->with('success', __('Message send successfully!'));
        } else {
            return back()->withInput()->with('error', __('Something went wrong, Please try again later!'));
        }
    }

}
