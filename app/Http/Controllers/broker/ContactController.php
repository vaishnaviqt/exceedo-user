<?php

namespace App\Http\Controllers\broker;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\AgentContact;
use Auth;

class ContactController extends Controller
{


   

    /**
     * Display a listing of the resource.
     *@param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        
        $paginate = $request->paginate ?? 10;
        $contacts = AgentContact::with('land')->where('user_id', Auth::user()->id)->paginate($paginate);
        return view('broker.contacts', compact('contacts', 'paginate'));
       
    }

   

}

