<?php

namespace App\Http\Controllers\broker;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\PropertyList;
use Razorpay\Api\Api;
Use App\Mail\AdminLandRequest;
use App\Mail\UserLandRequest;
use App\Models\Notification;
use App\Models\Payment;
use Storage;
use Mail;

class ProjectController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $name = $request->name ?? '';
        $address = $request->filter_address ?? '';
        $businessType = $request->filter_business_type ?? '';
        $landArea = $request->land_area ?? '';
        $startDate = !empty($request->created_at) ? Carbon::parse($request->created_at)->format('Y-m-d') : '';
        $allLands = PropertyList::withTrashed()->where('listed_by', Auth::user()->id);
        if (!empty($name)) {
            $allLands->where('land_name', 'like', '%' . $name . '%');
        }
        if (!empty($address)) {
            $allLands->where('address', 'LIKE', '%' . $address . '%');
        }
        if (!empty($businessType)) {
            $allLands->where('business_type', 'LIKE', '%' . $businessType . '%');
        }
        if (!empty($request->created_at)) {
            $allLands->whereDate('created_at', '=', $startDate);
        }

        if (!empty($landArea)) {
            if ($landArea == 200) {
                $allLands->where('size_of_land', '>', $landArea);
            } else {
                $area = explode('-', $landArea);
                $allLands->whereBetween('size_of_land', [$area[0], $area[1]]);
            }
        }

        $allLands = $allLands->orderBy('id', 'desc')->paginate(6);

        return view('broker.projects.projects', compact('name', 'address', 'businessType', 'startDate', 'allLands', 'landArea'));
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function YourLandListingActive(Request $request) {

        $name = $request->name ?? '';
        $address = $request->filter_address ?? '';
        $businessType = $request->filter_business_type ?? '';
        $businessType = $request->filter_business_type ?? '';
        $landArea = $request->land_area ?? '';
        $startDate = !empty($request->created_at) ? Carbon::parse($request->created_at)->format('Y-m-d') : '';
        $activeLands = PropertyList::where('listed_by', Auth::user()->id)->where('status', 1);
        if (!empty($name)) {
            $activeLands->where('land_name', 'like', '%' . $name . '%');
        }
        if (!empty($address)) {
            $activeLands->where('address', 'LIKE', '%' . $address . '%');
        }
        if (!empty($businessType)) {
            $activeLands->where('business_type', 'LIKE', '%' . $businessType . '%');
        }
        if (!empty($request->created_at)) {
            $activeLands->whereDate('created_at', '=', $startDate);
        }

        if (!empty($landArea)) {
            if ($landArea == 200) {
                $activeLands->where('size_of_land', '>', $landArea);
            } else {
                $area = explode('-', $landArea);
                $activeLands->whereBetween('size_of_land', [$area[0], $area[1]]);
            }
        }

        $activeLands = $activeLands->orderBy('id', 'desc')->paginate(6);

        return view('broker.projects.active-projects', compact('activeLands', 'name', 'address', 'businessType', 'startDate', 'landArea'));
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function YourLandListingInactive(Request $request) {

        $name = $request->name ?? '';
        $address = $request->filter_address ?? '';
        $businessType = $request->filter_business_type ?? '';
        $landArea = $request->land_area ?? '';
        $startDate = !empty($request->created_at) ? Carbon::parse($request->created_at)->format('Y-m-d') : '';
        $inactiveLands = PropertyList::where('listed_by', Auth::user()->id)->where('status', 0);
        if (!empty($name)) {
            $inactiveLands->where('land_name', 'like', '%' . $name . '%');
        }
        if (!empty($address)) {
            $inactiveLands->where('address', 'LIKE', '%' . $address . '%');
        }
        if (!empty($businessType)) {
            $inactiveLands->where('business_type', 'LIKE', '%' . $businessType . '%');
        }
        if (!empty($request->created_at)) {
            $inactiveLands->whereDate('created_at', '=', $startDate);
        }
        if (!empty($landArea)) {
            if ($landArea == 200) {
                $inactiveLands->where('size_of_land', '>', $landArea);
            } else {
                $area = explode('-', $landArea);
                $inactiveLands->whereBetween('size_of_land', [$area[0], $area[1]]);
            }
        }
        $inactiveLands = $inactiveLands->orderBy('id', 'desc')->paginate(6);

        return view('broker.projects.inactive-projects', compact('inactiveLands', 'name', 'address', 'businessType', 'startDate', 'landArea'));
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function YourLandListingDeleted(Request $request) {

        $name = $request->name ?? '';
        $address = $request->filter_address ?? '';
        $businessType = $request->filter_business_type ?? '';
        $landArea = $request->land_area ?? '';
        $startDate = !empty($request->created_at) ? Carbon::parse($request->created_at)->format('Y-m-d') : '';
        $deleteLands = PropertyList::withTrashed()->where('listed_by', Auth::user()->id);
        if (!empty($name)) {
            $deleteLands->where('land_name', 'like', '%' . $name . '%');
        }
        if (!empty($address)) {
            $deleteLands->where('address', 'LIKE', '%' . $address . '%');
        }
        if (!empty($businessType)) {
            $deleteLands->where('business_type', 'LIKE', '%' . $businessType . '%');
        }
        if (!empty($request->created_at)) {
            $deleteLands->whereDate('created_at', '=', $startDate);
        }

        if (!empty($landArea)) {
            if ($landArea == 200) {
                $deleteLands->where('size_of_land', '>', $landArea);
            } else {
                $area = explode('-', $landArea);
                $deleteLands->whereBetween('size_of_land', [$area[0], $area[1]]);
            }
        }
        $deleteLands->whereNotNull('deleted_at');

        $deleteLands = $deleteLands->orderBy('id', 'desc')->paginate(6);

        return view('broker.projects.deleted-projects', compact('deleteLands', 'name', 'address', 'businessType', 'startDate', 'landArea'));
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function userPropertyValidation(Request $request) {
        $rules = [
            'land_name' => 'required',
            'land_price' => 'required',
            'price_unit' => 'required',
            'address' => 'required',
            'user_pan' => 'required',
            'is_corporate' => 'required',
            'exclusive_channel_partner' => 'required',
            'exclusive_mandate' => 'required',
            'business_type' => 'required',
            'land_to_get_verified' => 'required',
            'new_images' => 'required',
        ];

        $messages = [
            'listing_by.required' => 'owner or broker is required.',
            'user_address.required' => 'The address field is required.',
            'user_gst.required' => 'The GST field is required.',
            'business_type.required' => 'What would you do with your property field is required',
            'user_pan.required' => 'The PAN field is required.',
            'is_corporate.required' => 'Company or Individual field is required.',
            'min_price.required' => 'Asking Pricing for Sale field is required.',
            'max_price.required' => 'Asking Pricing for Sale field is required.',
        ];
        if ($request->input('is_corporate') == 'company') {
            $rules['user_gst'] = 'required';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
//            dd($validator->errors());
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }

        $api = new Api(config('app.razorpay_api_key'), config('app.seceret_key'));
        $order = $api->order->create(array('receipt' => 'order_rcptid_11', 'amount' => $request->input('land_price') * 100, 'currency' => 'INR'));

        return response()->json(['order_id' => $order['id']]);
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function userPropertyStore(Request $request) {

        $userId = auth()->user()->id ?? $request->input('user_id');
        $path = '';
        $api = new Api(config('app.razorpay_api_key'), config('app.seceret_key'));
        //Fetch payment information by razorpay_payment_id
        $payment = $api->payment->fetch($request->input('razorpay_payment_id'));
        if (!empty($payment) && $payment['status'] == 'captured' || $payment['status'] == 'authorized') {
            $paymentId = $payment['id'];
            $amount = $payment['amount'];
            $currency = $payment['currency'];
            $status = $payment['status'];
            $entity = $payment['entity'];
            $orderId = $payment['order_id'];
            $invoiceId = $payment['invoice_id'];
            $method = $payment['method'];
            $bank = $payment['bank'];
            $wallet = $payment['wallet'];
            $bankTranstionId = isset($payment['acquirer_data']['bank_transaction_id']) ? $payment['acquirer_data']['bank_transaction_id'] : '';
        } else {
            return redirect()->back()->with('error', 'Something went wrong, Please try again later!');
        }
        if (!empty($request->input('new_images'))) {
            $image = $request->new_images;
            $image_parts = explode(";base64,", $image);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file = uniqid() . '. ' . $image_type;
            Storage::disk('propertyImage')->put($file, $image_base64);
            $imagePath = 'storage/propertyimage/' . $file;
        }
        try {
            $property = new PropertyList;
            $property->land_name = $request->input('land_name');
            $property->max_price = $request->input('land_price');
            $property->price_unit = $request->input('price_unit');
            $property->address = $request->input('address');
            $property->latitute = $request->input('latitute');
            $property->longitude = $request->input('longitude');
            $property->gst = $request->input('user_gst');
            $property->pan = $request->input('user_pan');
            $property->is_corporate = $request->input('is_corporate');
            $property->exclusive_channel_partner = $request->input('exclusive_channel_partner');
            $property->sign_exclusive_mandate = $request->input('exclusive_mandate');
            $property->business_type = $request->input('business_type');
            $property->land_to_get_verified = $request->input('land_to_get_verified') ?? null;
            $property->listed_by = Auth::user()->id;
            $property->image = $imagePath;
            $saved = $property->save();

            $noti = new Notification;
            $noti->notification_to = 'admin';
            $noti->land_report_id = $property->id;
            $noti->user_id = 1;
            $noti->message = "You recive new land request";
            $noti->save();

            $payment = new Payment;
            $payment->transaction_id = $paymentId;
            $payment->appointment_id = $property->id;
            $payment->amount = $amount / 100;
            $payment->currency = $currency;
            $payment->entity = $entity;
            $payment->status = $status;
            $payment->order_id = $orderId;
            $payment->method = $method;
            $payment->bank = $bank;
            $payment->wallet = $wallet;
            $payment->bank_transaction_id = $bankTranstionId;
            $payment->save();
        } catch (Exception $ex) {
            $saved = false;
        }
        if ($saved) {
            if (smtpConnect() == true) {
                $property = PropertyList::with('listed')->where('id', $property->id)->first();
                $property->amount = $payment->amount;
                Mail::to(config('mail.from.admin_email'))->send(new AdminLandRequest($payment->amount));
                Mail::to($property->listed->email)->send(new UserLandRequest($property));
            }
            return response()->json(['success' => true, 'msg' => "Land Detail Successfully added."]);
        } else {
            return response()->json(['success' => false, 'msg' => "Something went wrong, Please try again later!"]);
        }
    }

}
