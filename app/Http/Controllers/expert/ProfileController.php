<?php

namespace App\Http\Controllers\expert;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use Storage;
use Illuminate\Validation\Rule;

class ProfileController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $expert = auth()->user();
        return view('expert.profile', compact('expert'));
    }

    /**
     * expert profile update
     * 
     * @param Request $request
     * @return type
     */
    public function profileUpdate(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => ['required'],
                    'mobile' => ['required', 'numeric', 'digits:10', Rule::unique('users')->ignore($request->input('user_id'))],
        ]);

        if ($validator->fails()) {
            $validator->errors()->add('expertProfile', true);
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        try {
            $user = User::where('id', $request->input('user_id'))->first();
            $user->name = $request->input('name');
            $user->mobile = $request->input('mobile');
            $saved = $user->save();
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            return redirect()->back()->with('success', 'Profile Updated Successfully!');
        } else {
            return redirect()->back()->with('error', 'Something went wrong, Please try again later!');
        }
    }

    /**
     * expert profile phot update
     * 
     * @param Request $request
     * @return boolean
     */
    public function profilePhotoUpload(Request $request) {
        $validator = Validator::make($request->all(), ['photo' => 'required|mimes:jpeg,png,jpg,bmp,gif|max:1024']);

        // if validation fails
        if ($validator->fails()) {
            return $validator->errors();
        }
        $path = $request->file('photo')->store(
                'avatars', 'expert'
        );
        if ($path) {
            $path = 'expert_image/' . $path;
        }
        try {
            $previous = str_replace('expert_image/', '', $request->user()->photo);

            $broker = User::find($request->user()->id);
            $broker->photo = $path;
            $broker->save();
            // remove previous avatar
            if (!empty($previous) && Storage::disk('expert')->exists($previous)) {
                Storage::disk('expert')->delete($previous);
            }
        } catch (Exception $ex) {
            report($ex);
            return false;
        }
        return response()->json(['avatar' => Storage::disk('expert')->url(str_replace('expert_image/', '', $broker->photo))]);
    }

}
