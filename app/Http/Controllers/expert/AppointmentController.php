<?php

namespace App\Http\Controllers\expert;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\Appointment;
use App\Models\AppointmentHistory;
use App\Mail\UserRescheduleAppointment;
use App\Mail\UserAppointmentConfirm;
use App\Mail\AdminUserAppointmentCancel;
use App\Mail\ExpertAdminAppointmentReschedule;
use App\Mail\ExpertAppointmentCancellation;
use App\Mail\ExpertAppointmentReschedule;
use App\Mail\ExpertUserAppointmentReschedule;
use App\Mail\ExpertAdminAppointmentCancellation;
use App\Mail\ExpertAdminAppointmentConfirm;
use App\Mail\ExpertAppointmentConfirm;
use App\Mail\ExpertUserAppointmentConfirm;
use Exception;
use App\Models\Notification;
use Illuminate\Support\Facades\Mail;

class AppointmentController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $mode = $request->payment_mode ?? '';

        $totalAppointments = Appointment::where('expert_id', $request->user()->id)->count();
        $upcommingAppointments = Appointment::where('expert_id', $request->user()->id)->where('date', '>', Carbon::now())->count();
        $doneAppointments = Appointment::where('expert_id', $request->user()->id)->where('status', 1)->where('date', '<', Carbon::now())->count();
        $allAppoints = Appointment::where('expert_id', $request->user()->id)->orderBy('created_at', 'DESC')->get();
        return view('expert.appointments.appointments', compact('allAppoints', 'totalAppointments', 'upcommingAppointments', 'doneAppointments', 'mode'));
    }

    /**
     * Appointment show in calender
     * 
     * @param Request $request
     * @return type
     */
    public function appointmentDetails(Request $request) {

        $mode = $request->paymentMode ?? '';
        $appointments = Appointment::where('expert_id', $request->user()->id);
        if (!empty($mode)) {
            $appointments = Appointment::where('payment_mode', 'like', '%' . $mode . '%');
        }
        $appointments = $appointments->get();
        $reports = array();
        $key = 0;
        foreach ($appointments as $appointment) {
            $reports[$key]['id'] = $appointment->id;
            $reports[$key]['status'] = $appointment->status;
            $reports[$key]['title'] = $appointment->remarks;
            $reports[$key]['rescheduled_by'] = $appointment->rescheduled_by;
            $reports[$key]['start'] = Carbon::parse($appointment->date . $appointment->time)->format('Y-m-d H:i:s');
            $reports[$key]['allDay'] = false;
            $reports[$key]['canceled_by'] = $appointment->canceled_by;

            $key++;
        }
        return response()->json($reports);
    }

    public function appointmentConfirm($id) {
        $appoint = Appointment::with('user')->where('id', $id)->firstOrFail();
        if (!empty($appoint) && $appoint->status == 1) {
            return back()->withInput()->with('error', __('You alredy confirm this appointment!'));
        }
        try {
            $appoint->status = 1;
            $saved = $appoint->save();

            // Expert Confirm appointment notification detail store
            $appoint = Appointment::with('expert')->where('id', $appoint->id)->first();
            $noti = new Notification;
            $noti->notification_to = 'user';
            $noti->apppointment_id = $appoint->id;
            $noti->message = "Your appointment confirm with " . $appoint->expert->name . " on " . Carbon::parse($appoint->meeting_at)->format('d M, Y h:i A');
            $noti->save();
            $noti = new Notification;
            $noti->notification_to = 'admin';
            $noti->user_id = 1;
            $noti->apppointment_id = $appoint->id;
            $noti->message = "An appointment confirm with " . $appoint->expert->name . " on " . Carbon::parse($appoint->meeting_at)->format('d M, Y h:i A');
            $noti->save();
            $noti = new Notification;
            $noti->notification_to = 'user';
            $noti->apppointment_id = $appoint->id;
            $noti->user_id = $appoint->user->id;
            $noti->message = "Your appointment confirm with " . $appoint->expert->name . " on " . Carbon::parse($appoint->meeting_at)->format('d M, Y h:i A');
            $noti->save();
        } catch (Exception $ex) {
            $saved = false;
        }
        if ($saved) {
            if (smtpConnect() == true) {
                Mail::to($appoint->user->email)->send(new ExpertUserAppointmentConfirm($appoint));
                Mail::to(config('mail.from.admin_email'))->send(new ExpertAdminAppointmentConfirm($appoint));
                Mail::to($appoint->expert->email)->send(new ExpertAppointmentConfirm($appoint));
            }
            return redirect()->back()->with('success', __('Appointment confirm successfully!'));
        } else {
            return back()->withInput()->with('error', __('Something went wrong, Please try again later!'));
        }
    }

    /**
     * view all appointment with ajax and filter functionality
     * @param Request $request
     * @return type
     */
    public function viewAllAppointment(Request $request) {
        $allDate = $request->input('all_date') ?? '';
        $query = Appointment::where('expert_id', $request->user()->id);
        if ($allDate) {
            $query->whereDate('date', Carbon::parse($allDate)->format('Y-m-d'));
        }
        $allAppoints = $query->orderBy('created_at', 'DESC')->get();
        $view = View('expert.appointments.appointment-list-card', compact('allDate', 'allAppoints'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * pending all appointment with ajax and filter functionality
     * @param Request $request
     * @return type
     */
    public function viewPendingAppointment(Request $request) {
        $pendingDate = $request->input('date') ?? '';
        $pendingAppoints = Appointment::where('expert_id', $request->user()->id)->where('status', '!=', 1);
        if ($pendingDate) {
            $pendingAppoints->whereDate('date', Carbon::parse($pendingDate)->format('Y-m-d'));
        }
        $filterError = "pendingExpert";
        if (empty($pendingDate)) {
            $pendingAppoints->where('date', '>=', Carbon::now());
        }
        $pendingAppoints = $pendingAppoints->orderBy('created_at', 'DESC')->get();
        $view = View('expert.appointments.pending-appointment-list-card', compact('pendingDate', 'pendingAppoints'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * Done all appointment with ajax and filter functionality
     * @param Request $request
     * @return type
     */
    public function viewDoneppointment(Request $request) {
        $doneDate = $request->input('date') ?? '';
        $doneAppoints = Appointment::where('expert_id', $request->user()->id)->where('status', '=', 1);
        if ($doneDate) {
            $doneAppoints->whereDate('date', Carbon::parse($doneDate)->format('Y-m-d'));
        }
        if (empty($doneDate)) {
            $doneAppoints->where('date', '<', Carbon::now());
        }
        $doneAppoints = $doneAppoints->orderBy('created_at', 'DESC')->get();
        $view = View('expert.appointments.done-appointment-list-card', compact('doneDate', 'doneAppoints'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * expert appointment reschedule
     * 
     * @param Request $request
     * @return boolean
     */
    public function viewRescheduleppointment(Request $request) {

        if (strtotime($request->input('date')) <= strtotime(Carbon::now()->format('d-m-Y'))) {
            $response['date'] = true;
            return $response;
        }
        $appointment = Appointment::with('user', 'expert')->where('id', $request->input('id'))->first();
        try {
            $appointHistory = new AppointmentHistory;
            $appointHistory->date = $appointment->date;
            $appointHistory->appointment_id = $appointment->id;
            $appointHistory->user_id = $appointment->user_id;
            $appointHistory->expert_id = $appointment->expert_id;
            $appointHistory->time = $appointment->time;
            $appointHistory->terms = $appointment->terms;
            $appointHistory->remarks = $appointment->remarks;
            $appointHistory->rescheduled_by = 1;
            $saved = $appointHistory->save();
            if ($saved) {
                $appointment->date = Carbon::parse($request->input('date'))->format('Y-m-d');
                $appointment->time = $request->input('time');
                $appointment->meeting_at = Carbon::parse($request->input('date') . ' ' . $request->input('time'))->format('Y-m-d');
                $appointment->remarks = $request->input('remarks');
                $appointment->terms = $request->input('terms');
                $appointment->status = 2;
                $appointment->rescheduled_by = 1;
                $saved = $appointment->save();

                // User Confirm appointment notification detail store
                $appoint = Appointment::with('expert')->where('id', $appointment->id)->first();
                $noti = new Notification;
                $noti->notification_to = 'user';
                $noti->user_id = $appoint->user->id;
                $noti->apppointment_id = $appoint->id;
                $noti->message = $appoint->expert->name . " Rescheduled your appointment on " . Carbon::parse($appoint->meeting_at)->format('d M, Y h:i A');
                $noti->save();
                $noti = new Notification;
                $noti->notification_to = 'admin';
                $noti->user_id = 1;
                $noti->apppointment_id = $appoint->id;
                $noti->message = $appoint->expert->name . " Rescheduled appointment on " . Carbon::parse($appoint->meeting_at)->format('d M, Y h:i A');
                $noti->save();
                $noti = new Notification;
                $noti->notification_to = 'admin';
                $noti->user_id = 1;
                $noti->apppointment_id = $appoint->id;
                $noti->message = "You recive a new appointment with" . $appoint->expert->name . " on " . Carbon::parse($appointment->meeting_at)->format('d M, Y h:i A');
                $noti->save();
                if ($saved && smtpConnect() == true) {
                    Mail::to($appointment->user->email)->send(new ExpertUserAppointmentReschedule($appointment));
                    Mail::to($appoint->expert->email)->send(new ExpertAppointmentReschedule($appoint));
                    Mail::to(config('mail.from.admin_email'))->send(new ExpertAdminAppointmentReschedule($appoint));
                }
            }
        } catch (Exception $ex) {
            return false;
        }
        return response(true);
        return true;
    }

    /**
     * notification mark as read
     * 
     * @param type $id
     */
    public function markReadNotification($id) {
        $noti = Notification::where('id', $id)->first();
        $noti->unread = 1;
        $noti->save();
        return true;
    }

}
