<?php

namespace App\Http\Controllers\expert;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\Payment;
use App\Models\ValueAddedService;
use App\Models\User;
use Auth;

class PaymentController extends Controller
{


   

   /**
     * Display a listing of the resource.
     *@param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $paginate = $request->paginate ?? 10;
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $paymentType = $request->input('paymentType');
        
        $payments = Payment::select(['payments.created_at', 'payments.land_report_id', 'payments.id as paymentId', 'lus.name as reportUsername', 'payments.transaction_id', 'payments.appointment_id', 'payments.amount', 'payments.currency', 'payments.entity', 'payments.order_id', 'payments.method', 'payments.bank', 'payments.bank_transaction_id', 'payments.refund_id', 'payments.refund_Date', 'payments.status', 'pus.name as propertyUser', 'sus.name as subscUser', 'payments.land_report_id', 'payments.property_list_id', 'payments.subscription_id', 'aus.name as appointUserName'])->whereNotNull('transaction_id')
        ->leftJoin('appointments as ap', 'ap.id', '=', 'payments.appointment_id')
        ->leftJoin('land_reports as ls', 'ls.id', '=', 'payments.land_report_id')
        ->leftJoin('property_list as pl', 'pl.id', '=', 'payments.property_list_id')
        ->leftJoin('subscriptions as su', 'su.id', '=', 'payments.subscription_id')
        ->leftJoin('users as aus', 'aus.id', '=', 'ap.user_id')
        ->leftJoin('users as lus', 'lus.id', '=', 'ls.user_id')
        ->leftJoin('users as pus', 'pus.id', '=', 'pl.user_id')
        ->leftJoin('users as sus', 'sus.id', '=', 'su.user_id')
        ->where('ap.user_id', Auth::user()->id)
        ->orWhere('ls.user_id', Auth::user()->id)
        ->orWhere('pl.user_id', Auth::user()->id)
        ->orWhere('su.user_id', Auth::user()->id);
        if (!empty($date)) {
            $payments->whereDate('payments.created_at', $date);
        }
        if (!empty($paymentType)) {
            $payments->where('payments.method', 'Like', '%' . $paymentType . '%');
        }
        $payments = $payments->paginate($paginate);
//        dd($payments);
        return view('expert.payments.payments', compact('payments', 'paginate', 'date', 'paymentType'));
    }

    public function madePayments(Request $request) {
        $paginate = $request->paginate ?? 10;
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $paymentType = $request->input('paymentType');
        $payments = Payment::with('user')->where('user_id', Auth::user()->id);
        if (!empty($date)) {
            $payments->whereDate('created_at', $date);
        }
        if (!empty($paymentType)) {
            $payments->where('method', 'Like', '%' . $paymentType . '%');
        }
        $payments = $payments->paginate($paginate);
       
        return view('expert.payments.made-payments', compact('payments', 'paginate', 'date', 'paymentType'));
    }

   

}
