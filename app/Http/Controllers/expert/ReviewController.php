<?php

namespace App\Http\Controllers\expert;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\Review;
use App\Models\ValueAddedService;
use Auth;

class ReviewController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $customerName = $request->input('customerName');
        $serviceType = $request->input('serviceType');
        $ratingType = $request->ratingType ?? '';
        $startDate = !empty($request->created_at) ? Carbon::parse($request->created_at)->format('Y-m-d') : '';
        $services = ValueAddedService::all();
        $reviews = Review::selectRaw('reviews.id, reviews.user_id, reviews.appointment_id, reviews.rating,reviews.feedback, reviews.created_at, ap.id, ap.expert_id, ap.value_added_service_id, ap.meeting_at, us.name as username, us.email,us.photo, ex.name as expertname, service.service_name')
                ->leftJoin('appointments as ap', 'reviews.appointment_id', '=', 'ap.id')
                ->leftJoin('users as us', 'ap.user_id', '=', 'us.id')
                ->leftJoin('users as ex', 'ap.expert_id', '=', 'ex.id')
                ->leftJoin('value_added_services as service', 'ap.value_added_service_id', '=', 'service.id')
                ->where('ap.expert_id', Auth::user()->id);
        if (!empty($customerName)) {
            $reviews->where('us.name', 'Like', '%' . $customerName . '%');
        }
        if (!empty($serviceType)) {
            $reviews->where('ap.value_added_service_id', $serviceType);
        }
        if (!empty($startDate)) {
            $reviews->whereDate('reviews.created_at', '=', $startDate);
        }
        if (!empty($ratingType)) {
            $reviews->where('reviews.rating', 'LIKE', '%' . $ratingType . '%');
        }
        $reviews = $reviews->orderBy('reviews.id', 'desc')->paginate(10);

        return view('expert.reviews', compact('reviews', 'services', 'customerName', 'serviceType', 'ratingType', 'startDate'));
    }

}
