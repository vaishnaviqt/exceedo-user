<?php

namespace App\Http\Controllers\expert;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\LandReport;
use App\Mail\ExpertReportAccept;
use App\Mail\ExpertReportCancel;
use App\Mail\ExpertReportSend;
use App\Models\ReportDocument;
use App\Models\Notification;
use Mail;
use Exception;

class ProjectController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $name = $request->input('name') ?? '';
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $location = $request->input('location') ?? '';
        $user = auth()->user();
        $allReports = LandReport::LandReports($request, '', $user);
        return view('expert.projects.projects', compact('allReports', 'name', 'date', 'location'));
    }

    /**
     * expert delivered project listing
     * 
     * @param Request $request
     * @return type
     */
    public function deliveredProject(Request $request) {
        $name = $request->input('name') ?? '';
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $location = $request->input('location') ?? '';
        $user = auth()->user();
        $deliveredReports = LandReport::LandReports($request, 1, $user);
        return view('expert.projects.delivered-projects', compact('deliveredReports', 'name', 'date', 'location'));
    }

    /**
     * expert inprogress project listing
     * 
     * @param Request $request
     * @return type
     */
    public function inprogressProject(Request $request) {
        $name = $request->input('name') ?? '';
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $location = $request->input('location') ?? '';
        $user = auth()->user();
        $inprogressReports = LandReport::LandReports($request, 2, $user);
        return view('expert.projects.inprogress-projects', compact('inprogressReports', 'name', 'date', 'location'));
    }

    /**
     * expert pending project listing
     * 
     * @param Request $request
     * @return type
     */
    public function pendingProject(Request $request) {
        $name = $request->input('name') ?? '';
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $location = $request->input('location') ?? '';
        $user = auth()->user();
        $pendingReports = LandReport::LandReports($request, 'pending', $user);
        return view('expert.projects.pending-projects', compact('pendingReports', 'name', 'date', 'location'));
    }

    /**
     * expert confirm project
     * 
     * @param type $id
     * @return type
     */
    public function acceptProject($id) {
        try {
            $project = LandReport::with('expert')->where('id', $id)->first();
            $project->status = 2;
            $saved = $project->save();
            //project report confirm show notification to admin
            $noti = new Notification;
            $noti->notification_to = 'admin';
            $noti->user_id = 1;
            $noti->land_report_id = $project->id;
            $noti->message = "Expert Confirm $project->land_name report";
            $noti->save();
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            $project = LandReport::with('expert')->where('id', $id)->first();
            if (smtpConnect() == true) {
                Mail::to(config('mail.from.admin_email'))->send(new ExpertReportAccept($project));
            }
            return response($saved);
        } else {
            return response($saved);
        }
    }

    /**
     * expert cancel project
     * 
     * @param type $id
     * @return type
     */
    public function cancelProject($id) {
        try {
            $project = LandReport::with('expert')->where('id', $id)->first();
            $project->cancel = 1;
            $saved = $project->save();
            //project report confirm show notification to admin
            $noti = new Notification;
            $noti->notification_to = 'admin';
            $noti->land_report_id = $project->id;
            $noti->message = "Expert cancel $project->land_name report";
            $noti->save();
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            if (smtpConnect() == true) {
                Mail::to(config('mail.from.admin_email'))->send(new ExpertReportCancel($project));
            }
            return response($saved);
        } else {
            return response($saved);
        }
    }

    public function reportUpload(Request $request) {
        $validator = Validator::make($request->all(), ['file' => 'required|mimes:jpeg,png,jpg,bmp,gif,pdf|max:1024']);

        // if validation fails
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        $path = $request->file('file')->store(
                'report',
                'landReport'
        );
        if ($path) {
            $path = 'landReport/' . $path;
        }
        try {
            $document = new ReportDocument;
            $document->title = $path;
            $document->land_report_id = $request->input('id');
            $saved = $document->save();

            if ($saved && smtpConnect() == true) {
                Mail::to(config('mail.from.admin_email'))->send(new ExpertReportSend($document));
            }
            //project report confirm show notification to admin
            $noti = new Notification;
            $project = LandReport::with('user', 'expert')->where('id', $request->input('id'))->first();
            $noti->notification_to = 'admin';
            $noti->land_report_id = $project->id;
            $noti->message = $project->expert->name . " report upload and verify report";
            $noti->save();
        } catch (Exception $ex) {
            report($ex);
            return false;
        }
        return response($saved);
    }

}
