<?php

namespace App\Http\Controllers\expert;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\ServiceEnquiry;
use App\Mail\ExpertReportAccept;
use App\Mail\ExpertEnquiryAccept;
use App\Mail\ExpertReportCancel;
use App\Mail\ExpertEnquiryCancel;
use App\Models\ReportDocument;
use App\Models\Notification;
use Mail;
use Exception;

class ServiceEnquiryController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function serviceInqury(Request $request) {
        $name = $request->input('name') ?? '';
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $user = auth()->user();
        $enquiries = ServiceEnquiry::where('expert_id', auth()->user()->id)->where('status', 1)->orderBy('created_at', 'desc')->paginate();
        return view('expert.projects.service-inquiry', compact('enquiries', 'name', 'date'));
    }

    /**
     * expert confirm project
     * 
     * @param type $id
     * @return type
     */
    public function acceptEnquiry($id) {
        try {
            $enquiry = ServiceEnquiry::with('expert')->where('id', $id)->first();
            $enquiry->status = 1;
            $saved = $enquiry->save();
            //project report confirm show notification to admin
            $noti = new Notification;
            $noti->notification_to = 'admin';
            $noti->user_id = 1;
            $noti->service_enquiry_id = $enquiry->id;
            $noti->message = "Expert Confirm $enquiry->name enquiry";
            $noti->save();
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            if (smtpConnect() == true) {
                Mail::to(config('mail.from.admin_email'))->send(new ExpertEnquiryAccept($enquiry));
            }
            return response($saved);
        } else {
            return response($saved);
        }
    }

    /**
     * expert cancel project
     * 
     * @param type $id
     * @return type
     */
    public function cancelEnquiry($id) {
        try {
            $enquiry = ServiceEnquiry::with('expert')->where('id', $id)->first();
            $enquiry->cancel = 1;
            $saved = $enquiry->save();
            //project report confirm show notification to admin
            $noti = new Notification;
            $noti->notification_to = 'admin';
            $noti->user_id = 1;
            $noti->service_enquiry_id = $enquiry->id;
            $noti->message = "Expert cancel $enquiry->name enquiry";
            $noti->save();
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            if (smtpConnect() == true) {
                Mail::to(config('mail.from.admin_email'))->send(new ExpertEnquiryCancel($enquiry));
            }
            return response($saved);
        } else {
            return response($saved);
        }
    }

}
