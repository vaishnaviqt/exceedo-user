<?php

namespace App\Http\Controllers\admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use App\Models\Payment;
use App\Models\ValueAddedService;
use App\Models\User;
use App\Mail\AdminAddPayment;
use App\Mail\PaymentReceiveByAdmin;
use DB;
use Exception;

class PaymentController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $paginate = $request->input('day') ?? '10';
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $customerName = $request->input('customerName');
        $serviceType = $request->input('serviceType');
        $paymentType = $request->input('paymentType');
        $payments = Payment::select(['payments.*', 'lus.name as reportUsername', 'pus.name as propertyUser','service.service_name as serviceName', 'sus.name as subscUser', 'aus.name as appointUserName'])->whereNotNull('transaction_id')
                ->leftJoin('appointments as ap', 'ap.id', '=', 'payments.appointment_id')
                ->leftJoin('land_reports as ls', 'ls.id', '=', 'payments.land_report_id')
                ->leftJoin('property_list as pl', 'pl.id', '=', 'payments.property_list_id')
                ->leftJoin('subscriptions as su', 'su.id', '=', 'payments.subscription_id')
                ->leftJoin('users as aus', 'aus.id', '=', 'ap.user_id')
                ->leftJoin('users as lus', 'lus.id', '=', 'ls.user_id')
                ->leftJoin('users as pus', 'pus.id', '=', 'pl.user_id')
                ->leftJoin('users as sus', 'sus.id', '=', 'su.user_id')
                ->leftJoin('value_added_services as service', 'service.id', '=', 'ap.value_added_service_id')
                ->whereNull('payments.user_id');
        if (!empty($date)) {
            $payments->whereDate('payments.created_at', $date);
        }
        if (!empty($serviceType)) {
            $payments->where('ap.value_added_service_id', $serviceType);
        }
        if (!empty($customerName)) {
            $payments->where('aus.name', 'Like', '%' . $customerName . '%')
                    ->orWhere('lus.name', 'Like', '%' . $customerName . '%')
                    ->orWhere('pus.name', 'Like', '%' . $customerName . '%')
                    ->orWhere('sus.name', 'Like', '%' . $customerName . '%');
        }
        if (!empty($paymentType)) {
            $payments->where('method', 'Like', '%' . $paymentType . '%');
        }
        $payments = $payments->orderBy('created_at', 'desc')->paginate($paginate);

        $services = ValueAddedService::all();
        $users = User::role(['expert', 'broker'])->get();
        return view('admin.payments.payments', compact('users', 'payments', 'services', 'date', 'customerName', 'serviceType', 'paymentType', 'paginate'));
    }

    public function download($id, Request $request) {
        $invoice_upload = Payment::find($id);
        $file = public_path() . "/invoice" . "/" . $invoice_upload->inovice_upload;

        return response()->download($file);
    }

    public function invoice_upload(Request $request) {

        $id = $request->payment_id;
        $invoice_upload = Payment::find($id);
        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        $filename = time() . '.' . $ext;
        // print_r($filename);
        $file->move('Invoice/', $filename);
        $invoice_upload->inovice_upload = $filename;
        $invoice_upload->save();
        return response()->json([
                    'data' => "okkdata"
        ]);
    }

    public function userPaymentStore(Request $request) {
        $rules = [
            'myInput' => 'required',
            'amount' => 'required',
            'currency' => 'required',
            'transaction_id' => 'required',
            'method' => 'required',
            'status' => 'required',
            'bank' => 'required',
            'file' => 'required',
        ];

        $messages = [
            'myInput.required' => 'The User field is required',
            'amount.required' => 'The Amount field is required',
            'currency.required' => 'The Currency field is required.',
            'transaction_id.required' => 'The Transaction field is required.',
            'method.required' => 'The Method field is required.',
            'status.required' => 'The Status field is required',
            'bank.required' => 'The Bank field is required',
            'file.required' => 'The Attachment field is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        if (!empty($request->file('file'))) {
            $statement = DB::select("SHOW TABLE STATUS LIKE 'payments'");
            $nextId = (isset($statement[0]->Auto_increment)) ? $statement[0]->Auto_increment : uniqid();
            $file_name = time() . '_' . $nextId;
            $fileName = $file_name . '.' . $request->file('file')->getClientOriginalExtension();
            $path = 'payments' . '/' . $request->file('file')->storeAs($request->input('user_id'), $fileName, 'payments');
        }
        try {
            $payment = new Payment;
            $payment->user_id = $request->input('user_id');
            $payment->amount = $request->input('amount');
            $payment->currency = $request->input('currency');
            $payment->transaction_id = $request->input('transaction_id');
            $payment->method = $request->input('method');
            $payment->status = $request->input('status');
            $payment->bank = $request->input('bank');
            $payment->file = $path;
            $payment->description = $request->input('description') ?? '';
            $saved = $payment->save();
        } catch (Exception $ex) {
            $saved = false;
        }
        if ($saved) {
            if ($saved && smtpConnect() == true) {
                $payment = Payment::with('user')->where('id', $payment->id)->first();
                Mail::to($payment->user->email)->send(new PaymentReceiveByAdmin($payment));
                Mail::to(config('mail.from.admin_email'))->send(new AdminAddPayment($payment));
            }
            return response()->json(['success' => true, 'msg' => "Payment Successfully added."]);
        } else {
            return response()->json(['success' => false, 'msg' => "Something went wrong, Please try again later!"]);
        }
    }

    public function userNameAutoSuggest(Request $request) {
        $users = User::where('name', 'like', '%' . $request->keyword . '%')->get()->pluck('name');
        return response($users);
    }

    public function paymentMade(Request $request) {
        $paginate = $request->input('day') ?? '10';
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $customerName = $request->input('customerName');
        $serviceType = $request->input('serviceType');
        $paymentType = $request->input('paymentType');
        $payments = Payment::select(['users.*', 'payments.*'])->whereNotNull('user_id')
                ->leftJoin('users as users', 'users.id', '=', 'payments.user_id');

        if (!empty($date)) {
//            dd($date);
             $payments->whereDate('payments.created_at', $date);
        }
        if (!empty($customerName)) {
            $payments->where('us.name', 'Like', '%' . $customerName . '%');
        }
        if (!empty($paymentType)) {
            $payments->where('method', 'Like', '%' . $paymentType . '%');
        }
        $payments = $payments->paginate($paginate);
        $services = ValueAddedService::all();
        $users = User::role(['expert', 'broker'])->get();
        return view('admin.payments.payments-made', compact('users', 'payments', 'services', 'date', 'customerName', 'serviceType', 'paymentType', 'paginate'));
    }

}
