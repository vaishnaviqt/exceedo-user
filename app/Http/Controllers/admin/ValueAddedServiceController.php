<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\ValueAddedService;
use App\Http\Requests\StoreValueAddedServicesRequest;
use Exception;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class ValueAddedServiceController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StoreValueAddedServicesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'service_name' => 'required|max:200|unique:value_added_services',
            'description' => 'required',
            // 'service_icon' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'price' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        $valueService = new ValueAddedService;
        $valueService->service_name = trim($request->service_name);
        $valueService->description = trim($request->description);
        $valueService->price = trim($request->price);
        $icon_file = $request->file('service_icon');

        if ($icon_file) {
            $newFileName = time() . "." . $icon_file->getClientOriginalExtension();
            //Move Uploaded File
            $destinationPath = public_path('storage/uploads/valueservices');
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0777, true);
            }
            $image_resize = Image::make($icon_file->getRealPath());
            $image_resize->resize(60, 60);
            $image_resize->save($destinationPath . '/' . $newFileName);

            $valueService->service_icon = 'storage/uploads/valueservices/' . $newFileName;
        }

        if ($valueService->save()) {
            return response()->json(['success' => true, 'msg' => 'Value added service successfully added']);
        } else {
            return response()->json(['success' => false, 'msg' => 'Value added service Addition Failed!']);
        }
        die();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        try {
            $ValueAddedService = ValueAddedService::findOrFail($id);
            return $ValueAddedService;
        } catch (Exception $ex) {
            return redirect()->back()->with('error', $ex->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $rules = [
            'editservice_name' => 'required|max:200' . Rule::unique('value_added_services')->ignore($request->serviceid),
            'editdescription' => 'required',
            // 'service_icon' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'editprice' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        //
        try {


            $valueService = ValueAddedService::findOrFail($request->serviceid);
            $valueService->service_name = trim($request->editservice_name);
            $valueService->price = trim($request->editprice);
            $valueService->description = trim($request->editdescription);
            $icon_file = $request->file('editservice_icon');
            if ($icon_file) {
                $newFileName = time() . "." . $icon_file->getClientOriginalExtension();
                //Move Uploaded File
                $destinationPath = public_path('storage/uploads/valueservices');
                if (!file_exists($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                }
                $image_resize = Image::make($icon_file->getRealPath());
                $image_resize->resize(60, 60);
                $image_resize->save($destinationPath . '/' . $newFileName);

                $valueService->service_icon = 'storage/uploads/valueservices/' . $newFileName;
            }

            if ($valueService->save()) {
                return response()->json(['success' => true, 'msg' => 'Value added service successfully updated']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Value added service Updation Failed!']);
            }
            // $valueService->save();
            // die();
            // return response()->json(['success' => false, 'msg' => 'Value added service Updation Failed!']);
        } catch (Exception $ex) {
            return response()->json(['success' => false, 'msg' => 'sorry']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete_valueservice($id ,Request $request) {
      $valueDelete= ValueAddedService::find($id);
      if ( $valueDelete->delete()) {
                return response()->json(['success' => true, 'msg' => 'Value added service successfully Deleted']);
            } else {
                return response()->json(['success' => false, 'msg' => 'Value added service Deleted Failed!']);
    }

       
    }

    //    02-04-2021
    public function valueAddedServices(Request $request) {
        $paginate = $request->paginate ?? 10;
        $allServices = ValueAddedService::all();
        $valueServices = ValueAddedService::where(function ($query) use ($request) {
                    if ($request->service_name != '') {
                        $query->where('service_name', 'like', '%' . $request->service_name . '%');
                    }
                })->paginate($paginate);
        return view('admin/experts/valueaddedservices')->with([
                    'valueServices' => $valueServices,
                    'allServices' => $allServices,
                    'service_name' => $request->service_name
        ]);
    }

}
