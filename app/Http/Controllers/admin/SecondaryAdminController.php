<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\ValueAddedService;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserMessage;
use App\Mail\UserRegistration;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SecondaryAdminController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAdmin(Request $request) {
//        dd('in');
        $rules = [
            'full_name' => 'required|regex:/^[a-z\s]+$/i|max:255',
            'email' => 'required|email|unique:users|email:rfc,dns',
            'mobile_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10|unique:users,mobile',
        ];

        //if id not empty then override the above rules
        if ($request->id) {
            $replace_rules = [
                'full_name' => 'required|regex:/^[a-z\s]+$/i|max:255',
                'email' => 'required|email|email:rfc,dns|' . Rule::unique('users')->ignore($request->id),
                'mobile_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10|' . Rule::unique('users', 'mobile')->ignore($request->id),
            ];
            $rules = array_replace($rules, $rules, $replace_rules);
        }

        $messages = [
            'full_name.required' => 'The full name field is required.',
            'email.required' => 'The email field is required.',
            'email.email' => 'The email must be a valid email address.',
            'mobile_number.required' => 'The mobile number field is required.',
            'mobile_number.regex' => 'The mobile number format is invalid.',
            'mobile_number.min' => 'The mobile number must be at least 10 digits.',
            'mobile_number.max' => 'The mobile number may not be greater than 10 digits.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        if ($request->id) {
            $user = User::find($request->id);
            $user->name = trim($request->full_name);
            $user->email = trim($request->email);
            $user->password = Hash::make(trim($request->mobile_number));
            $user->mobile = trim($request->mobile_number);

            if ($user->update()) {
                if (smtpConnect() == true) {
                    $message = User::where('id', $user->id)->first();

                    Mail::to($user->email)->send(new UserMessage($message));
                    Mail::to(config('mail.from.admin_email'))->send(new UserRegistration($message));
                }
                return response()->json(['success' => true, 'msg' => "Secondary Admin Successfully updated."]);
            } else {
                return response()->json(['success' => false, 'msg' => "Secondary Admin Successfully not updated."]);
            }
            die();
        } else {
            $user = new user();
            $user->name = trim($request->full_name);
            $user->email = trim($request->email);
            $user->password = Hash::make(trim($request->mobile_number));
            $user->mobile = trim($request->mobile_number);
            $user->is_pending = 1;

            if ($user->save()) {
                $user->assignRole('admin');
                if (smtpConnect() == true) {
                    $message = User::where('id', $user->id)->first();
                    Mail::to($user->email)->send(new UserMessage($message));
                }
                return response()->json(['success' => true, 'msg' => "Secondary Admin Successfully added."]);
            } else {
                return response()->json(['success' => false, 'msg' => "Secondary Admin Successfully not added."]);
            }

            die();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update() {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroySecondaryAdmin(Request $request) {
        if ($request->isMethod('post')) {

            try {
                $admin = User::findOrFail($request->deletion_id);
            } catch (ModelNotFoundException $exception) {
                return redirect()->back()->with('error', "Secondary Admin Not Found.");
            }

            if ($admin->delete()) {

                if ($admin->hasRole('admin')) {
                    $admin->removeRole('admin');
                } else {
                    $admin->removeRole('admin');
                }
                return redirect()->back()->with('success', 'Secondary Admin Deleted Successfully!');
            } else {
                return redirect()->back()->with('error', 'Secondary Admin Deletion Failed!');
            }
        } else {
            return redirect()->back()->with('error', 'This is not allowed');
        }
    }

}
