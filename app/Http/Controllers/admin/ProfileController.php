<?php

namespace App\Http\Controllers\admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use Storage;
use Illuminate\Validation\Rule;
use App\Models\ModelHasPermission;

class ProfileController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $admin = auth()->user();
        $allAdmins = ModelHasPermission::with('user')->groupBy('model_id')->get();
        return view('admin.profile', compact('admin','allAdmins'));
    }

    public function profileUpdate(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => ['required'],
                    'mobile' => ['required', 'numeric', 'digits:10', Rule::unique('users')->ignore($request->input('user_id'))],
        ]);

        if ($validator->fails()) {
            $validator->errors()->add('adminProfile', true);
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        try {
            $user = User::where('id', $request->input('user_id'))->first();
            $user->name = $request->input('name');
            $user->mobile = $request->input('mobile');
            $saved = $user->save();
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            return redirect()->back()->with('success', 'Your Profile Updated Successfully!');
        } else {
            return redirect()->back()->with('error', 'Something went wrong, Please try again later!');
        }
    }

    /**
     * admin profile phot update
     * 
     * @param Request $request
     * @return boolean
     */
    public function profilePhotoUpload(Request $request) {
        $validator = Validator::make($request->all(), ['photo' => 'required|mimes:jpeg,png,jpg,bmp,gif|max:1024']);

        // if validation fails
        if ($validator->fails()) {
            return $validator->errors();
        }
        $path = $request->file('photo')->store(
                'avatars', 'admin'
        );
        if ($path) {
            $path = 'admin_image/' . $path;
        }
        try {
            $previous = str_replace('admin_image/', '', $request->user()->photo);

            $broker = User::find($request->user()->id);
            $broker->photo = $path;
            $broker->save();
            // remove previous avatar
            if (!empty($previous) && Storage::disk('admin')->exists($previous)) {
                Storage::disk('admin')->delete($previous);
            }
        } catch (Exception $ex) {
            report($ex);
            return false;
        }
        return response()->json(['avatar' => Storage::disk('admin')->url(str_replace('admin_image/', '', $broker->photo))]);
    }

}
