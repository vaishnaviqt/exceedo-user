<?php

namespace App\Http\Controllers\admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Route;
use App\Models\LandReport;
use App\Mail\UserCreate;
use App\Models\User;
use App\Models\ValueAddedService;
use App\Mail\LandReportAssign;
use Illuminate\Support\Facades\Hash;
use App\Mail\AdminReportAssign;
use App\Mail\AdminNotifyReport;
use App\Mail\AdminConfirmReport;
use App\Mail\DeliveredExpertReport;
use App\Mail\AdminAddLandReport;
use App\Mail\UserReceiveNewReport;
use App\Models\Notification;
use App\Models\ReportDocument;
use Mail;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $experts = User::role('expert')->where('is_pending', 1)->get();
        $services = ValueAddedService::all();
        $serviceType = $request->input('serviceType');
        $name = $request->input('all_expert_id') ?? '';
        $date = !empty($request->input('date')) ?? '';
        $location = $request->input('location') ?? '';
        $allReports = LandReport::landReports($request);
        $user = User::Role('user')->where('is_pending', 1)->get();
        return view('admin.reports.reports', compact('allReports', 'name', 'date', 'location', 'experts', 'services', 'serviceType', 'user'));
    }

    /**
     * all delivered report listing
     * 
     * @param Request $request
     * @return type
     */
    public function deliveredReport(Request $request) {
        $experts = User::role('expert')->where('is_pending', 1)->get();
        $name = $request->input('all_expert_id') ?? '';
        $date = !empty($request->input('date')) ?? '';
        $location = $request->input('location') ?? '';
         $services = ValueAddedService::all();
         $serviceType = $request->input('serviceType');
        $deliveredReports = LandReport::landReports($request, 1);
        $user = User::Role('user')->where('is_pending', 1)->get();
        return view('admin.reports.delivered-reports', compact('deliveredReports', 'name', 'date', 'location', 'experts', 'services', 'serviceType', 'user'));
    }

    /**
     * all Inprogress report listing
     * 
     * @param Request $request
     * @return type
     */
    public function inprogressReport(Request $request) {
        $experts = User::role('expert')->where('is_pending', 1)->get();
        $name = $request->input('all_expert_id') ?? '';
        $date = !empty($request->input('date')) ?? '';
        $location = $request->input('location') ?? '';
        $serviceType = $request->input('serviceType');
        $inprogressReports = LandReport::landReports($request, 2);
         $services = ValueAddedService::all();
        $user = User::Role('user')->where('is_pending', 1)->get();
        return view('admin.reports.inprogress-reports', compact('inprogressReports', 'name', 'date', 'location', 'experts', 'services', 'serviceType', 'user'));
    }

    /**
     * all pending report listing
     * 
     * @param Request $request
     * @return type
     */
    public function pendingReport(Request $request) {
        $experts = User::role('expert')->where('is_pending', 1)->get();
        $name = $request->input('all_expert_id') ?? '';
        $date = !empty($request->input('date')) ?? '';
        $location = $request->input('location') ?? '';
        $pendingReports = LandReport::landReports($request, 'pending');
         $services = ValueAddedService::all();
         $serviceType = $request->input('serviceType');
        $user = User::Role('user')->where('is_pending', 1)->get();
        return view('admin.reports.pending-reports', compact('pendingReports', 'name', 'date', 'location', 'experts', 'services', 'serviceType', 'user'));
    }

    /**
     * report assign to expert
     * 
     * @param Request $request
     * @return type
     */
    public function reportAssignExpert(Request $request) {
        try {
            $report = LandReport::where('id', $request->input('reportId'))->first();
            $report->expert_id = $request->input('expert_id');
            $report->cancel = null;
            $saved = $report->save();
            // after assing apppointment to expert notification detail store 
            $report = LandReport::with('expert')->where('id', $report->id)->first();
            $noti = new Notification;
            $noti->notification_to = 'expert';
            $noti->land_report_id = $report->id;
            $noti->user_id = $report->expert_id;
            $noti->message = "You recive a new report";
            $noti->save();

            if ($saved && smtpConnect() == true) {
                Mail::to($report->expert->email)->send(new LandReportAssign($report));
                Mail::to(config('mail.from.admin_email'))->send(new AdminReportAssign($report));
            }
        } catch (Exception $e) {
            $saved = false;
        }
        return response($saved);
    }

    /**
     * report delivered status change
     * 
     * @param Request $request
     * @return type
     */
    public function reportDelivered(Request $request) {
        try {
            $report = LandReport::with('user', 'valueAdded', 'expert')->where('id', $request->input('id'))->first();
            $report->status = 1;
            $report->ratings = $request->input('ratings');
            $saved = $report->save();

            if ($saved && smtpConnect() == true) {
                $report = LandReport::with('user', 'valueAdded', 'expert')->where('id', $request->input('id'))->first();
                Mail::to($report->user->email)->send(new DeliveredExpertReport($report));
                Mail::to(config('mail.from.admin_email'))->send(new AdminConfirmReport($report));
            }
            $noti = new Notification;
            $noti->notification_to = 'user';
            $noti->land_report_id = $report->id;
            $noti->user_id = $report->expert_id;
            $noti->message = "Admin Delivered your report and please eheck";
            $noti->save();
        } catch (Exception $e) {
            $saved = false;
        }
        return response($saved);
    }

    public function reportFeedback(Request $request) {
        $path = '';
        if (!empty($request->input('report'))) {
            $path = $request->file('report')->store(
                    'report', 'landReport'
            );
            if ($path) {
                $path = 'landReport/' . $path;
            }
        }
        $expert = landReport::with('expert')->where('id', $request->reportId)->first();
        $expertEmail = $expert->expert->email;
    
        try {
            if (!empty($request->input('report'))) {
                $reportDocument = new ReportDocument;
                $reportDocument->feedback = $request->input('feedback');
                $reportDocument->land_report_id = $request->input('reportId');
                $reportDocument->report_by = 1;
                $reportDocument->title = $path;
                $saved = $reportDocument->save();
                if ($saved && smtpConnect() == true) {
                Mail::to($expertEmail)->send(new AdminNotifyReport($reportDocument));
                }
            } else {
                $reportDocument = ReportDocument::where('id', $request->input('documentId'))->first();
                $reportDocument->feedback = $request->input('feedback');
                $reportDocument->report_by = 1;
                $saved = $reportDocument->save();
                if ($saved && smtpConnect() == true) {
                Mail::to($expertEmail)->send(new AdminNotifyReport($reportDocument));
            }
            }


            $report = LandReport::where('id', $reportDocument->land_report_id)->first();
            $noti = new Notification;
            $noti->notification_to = 'expert';
            $noti->land_report_id = $report->id;
            $noti->user_id = $report->expert_id;
            $noti->message = "You recive new feed back $report->land_name";
            $noti->save();
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            return response()->json(['success' => true, 'msg' => "Report feedback successfully send."]);
        } else {
            return response()->json(['success' => false, 'msg' => "Internal server error, Please Try again"]);
        }
    }

    public function ReportValidation(Request $request) {

        $path = '';
        $rules = [
            'land_name' => 'required',
            'address' => 'required',
            'state_id' => 'required',
            'city' => 'required',
            'analysis_report' => 'required',
            'selling_more' => 'required',
            'value_added_service_id' => 'required',
            'terms' => 'required',
        ];

        $messages = [
            'land_name.required' => 'The Land name field is required',
            'address.required' => 'The Address field is required.',
            'state_id.required' => 'The State field is required.',
            'city.required' => 'The City Field is required.',
            'value_added_service_id.required' => 'The Value Added Services Field is required.',
            'terms.required' => 'The Privacy Policy field is required.',
            'analysis_report.required' => 'The Analysis Report field is required.',
            'selling_more.required' => 'The Selling More field is required.',
        ];
        if (empty($request->input('all_user_id'))) {
            $rules['name'] = 'required|max:255';
            $rules['email'] = 'required|email|unique:users';
            $rules['mobile'] = 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10|unique:users';

            $messages['name.required'] = 'The name field is required';
            $messages['email.required'] = 'The email field is required.';
            $messages['email.email'] = 'The email must be a valid email address.';
            $messages['mobile.required'] = 'The mobile number field is required.';
            $messages['mobile.regex'] = 'The mobile number format is invalid.';
            $messages['mobile.min'] = 'The mobile number must be at least 10 characters.';
            $messages['mobile.max'] = 'The mobile number may not be greater than 10 characters.';
}

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }

        try {
            $user = auth()->user();
            if (empty($request->input('all_user_id'))) {
                $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
                $password = substr($random, 0, 10);
                $user = new User;
                $user->name = $request->input('name');
                $user->email = $request->input('email');
                $user->mobile = $request->input('mobile');
                $user->password = Hash::make($password);
                $saved = $user->save();

                $user->assignRole('user');
                if ($saved && smtpConnect() == true) {
                    $userDetail['name'] = $user->name;
                    $userDetail['email'] = $user->email;
                    $userDetail['password'] = $password;
                    Mail::to($user->email)->send(new UserCreate($userDetail));
                }
            }
            $userId = $request->input('all_user_id') ?? $user->id;
            if (!empty($request->file('image'))) {
                $statement = DB::select("SHOW TABLE STATUS LIKE 'land_reports'");
                $nextId = (isset($statement[0]->Auto_increment)) ? $statement[0]->Auto_increment : uniqid();
                $file_name = trim($request->input('land_name')) . '_' . $nextId;
                $fileName = $file_name . '.' . $request->file('image')->getClientOriginalExtension();
                $path = 'land_report_image' . '/' . $request->file('image')->storeAs($userId, $fileName, 'landReport');
            }

            $landreeport = new LandReport();
            $landreeport->land_name = $request->input('land_name');
            $landreeport->user_id = $userId;
            $landreeport->city = $request->input('city');
            $landreeport->address = $request->input('address');
            $landreeport->state_id = $request->input('state_id');
            $landreeport->value_added_service = $request->input('value_added_service_id');
            $landreeport->image = $path;
            $landreeport->analysis_report = $request->input('analysis_report');
            $landreeport->selling_more = $request->input('selling_more');
            $saved = $landreeport->save();
        } catch (Exception $e) {
            $saved = false;
        }
        $landReportData = LandReport::with('user','valueAdded')->where('id', $landreeport->id)->first();

        if ($saved) {
            if (smtpConnect() == true) {
                Mail::to($landReportData->user->email)->send(new UserReceiveNewReport($landReportData));
                Mail::to(config('mail.from.admin_email'))->send(new AdminAddLandReport($landReportData));
            }
            return response()->json(['success' => true, 'msg' => 'Report Stored successfuly']);
        } else {
            return response()->json(['success' => true, 'msg' => 'Some thing went wrong, Please try again']);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function getExpertPopup(Request $request) {
        $service = $request->input('service');
        $users = User::with('services', 'state')->role('expert')->where('is_pending', 1)->get();
        $experts = array();
        if (!empty($service) && count($users)) {
            $i = 0;
            foreach ($users as $user) {
                if (count($user->services)) {
                    $serviceId = array_column($user->services->toArray(), 'id');
                    if (in_array($service, $serviceId)) {
                        $experts[$i]['id'] = $user->id;
                        $state = isset($user->state->name) ? '(' . $user->state->name . ')' : '';
                        $name = $user->name . $state . showRatingReport($user->id);
                        $experts[$i]['name'] = $name;
                        $i++;
}
                }
            }
        } else if(count($users)) {
            $i = 0;
            foreach ($users as $user) {
                $experts[$i]['id'] = $user->id;
                $state = isset($user->state->name) ? '('.$user->state->name.')' : '';
                $name = $user->name . $state . showRatingReport($user->id);
                        $experts[$i]['name'] = $name;
                $i++;
            }
        }
        return response($experts);
    }

}
