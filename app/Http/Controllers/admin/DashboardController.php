<?php

namespace App\Http\Controllers\admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ValueAddedService;
use App\Models\Appointment;
use App\Models\PropertyList;
use App\Models\LandReport;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $total_app = Appointment::all();
        $total_land = PropertyList::all();
        $total_report = LandReport::all();
        $filterDate = $request->day ?? 'month';
        // for pending appointment
        $pendingPayments = DB::table('appointments')
                ->where('status', 0);
        if ($filterDate == "week") {
            $pendingPayments->select(DB::RAW('Day(date) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('date', '>', now()->startOfWeek())
                    ->where('date', '<', now()->endOfWeek());
        } else if ($filterDate == "month") {
            $pendingPayments->select(DB::RAW('Day(date) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('date', '>', now()->startOfMonth()->format('Y-m-d'))
                    ->where('date', '<', now()->endOfMonth()->format('Y-m-d'));
        } else if ($filterDate == "year") {
            $pendingPayments->select(DB::RAW('MONTHNAME(date) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->whereYear('date', Carbon::now()->year);
        }
        $pendingPayments = $pendingPayments->groupBy('count_appointment')->get();

        if ($filterDate == "week") {
            $PendingEarning = getDateWiseData(now()->startOfWeek(), now()->endOfWeek(), $pendingPayments);
        } else if ($filterDate == "month") {
            $PendingEarning = getDateWiseData(now()->startOfMonth(), now()->endOfMonth(), $pendingPayments);
        } else if ($filterDate == "year") {
            $PendingEarning = getMonthWiseData($pendingPayments);
        }
        // getDateWiseData function call from helper
        $pendingAppointment = $PendingEarning['paidappointment'];
        $pendingAppointment = $pendingAppointment;
        $pendingAppointmentTitle = 'Penidng Appointment';

        // for paid appointment
        $paidPayments = DB::table('appointments')
                ->where('status', 1);
        if ($filterDate == "week") {
            $paidPayments->select(DB::RAW('Day(date) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('date', '>', now()->startOfWeek())
                    ->where('date', '<', now()->endOfWeek());
        } else if ($filterDate == "month") {
            $paidPayments->select(DB::RAW('Day(date) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('date', '>', now()->startOfMonth()->format('Y-m-d'))
                    ->where('date', '<', now()->endOfMonth()->format('Y-m-d'));
        } else if ($filterDate == "year") {
            $paidPayments->select(DB::RAW('MONTHNAME(date) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->whereYear('date', Carbon::now()->year);
        }
        $paidPayments = $paidPayments->groupBy('count_appointment')->get();
        if ($filterDate == "week") {
            $AdminEarning = getDateWiseData(now()->startOfWeek(), now()->endOfWeek(), $paidPayments);
        } else if ($filterDate == "month") {
            $AdminEarning = getDateWiseData(now()->startOfMonth(), now()->endOfMonth(), $paidPayments);
        } else if ($filterDate == "year") {
            $AdminEarning = getMonthWiseData($paidPayments);
        }

        // getDateWiseData function call from helper 
        $adminEarningAmount = $AdminEarning['paidappointment'];
        $adminEarningDate = $AdminEarning['date'];
        $doneAppointmentTitle = 'Done Appointment';
    
        // for land detail
        $verifiedLands = DB::table('property_list')
                ->where('status', 1);
        if ($filterDate == "week") {
            $verifiedLands->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfWeek())
                    ->where('created_at', '<', now()->endOfWeek());
        } else if ($filterDate == "month") {
            $verifiedLands->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfMonth()->format('Y-m-d'))
                    ->where('created_at', '<', now()->endOfMonth()->format('Y-m-d'));
        } else if ($filterDate == "year") {
            $verifiedLands->select(DB::raw('MONTHNAME(created_at) as count_appointment'), DB::raw('count(*) as count_date'))->whereYear('created_at', Carbon::now()->year);
        }
        $verifiedLands = $verifiedLands->groupBy('count_appointment')->get();
        if ($filterDate == "week") {
            $landVerified = getDateWiseData(now()->startOfWeek(), now()->endOfWeek(), $verifiedLands);
        } else if ($filterDate == "month") {
            $landVerified = getDateWiseData(now()->startOfMonth(), now()->endOfMonth(), $verifiedLands);
        } else if ($filterDate == "year") {
            $landVerified = getMonthWiseData($verifiedLands);
        }

        $landVerifiedDeatil = $landVerified['paidappointment'];
        $landVerifiedDate = $landVerified['date'];
        
        // for unverified land
        $unverifiedLands = DB::table('property_list')
                ->where('status', 0);
        if ($filterDate == "week") {
            $unverifiedLands->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfWeek())
                    ->where('created_at', '<', now()->endOfWeek());
        } else if ($filterDate == "month") {
            $unverifiedLands->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfMonth()->format('Y-m-d'))
                    ->where('created_at', '<', now()->endOfMonth()->format('Y-m-d'));
        } else if ($filterDate == "year") {
            $unverifiedLands->select(DB::raw('MONTHNAME(created_at) as count_appointment'), DB::raw('count(*) as count_date'))->whereYear('created_at', Carbon::now()->year);
        }
        
        $unverifiedLands = $unverifiedLands->groupBy('count_appointment')->get();
        if ($filterDate == "week") {
            $landUnverified = getDateWiseData(now()->startOfWeek(), now()->endOfWeek(), $unverifiedLands);
        } else if ($filterDate == "month") {
            $landUnverified = getDateWiseData(now()->startOfMonth(), now()->endOfMonth(), $unverifiedLands);
        } else if ($filterDate == "year") {
            $landUnverified = getMonthWiseData($unverifiedLands);
        }

        $landUnverifiedDeatil = $landUnverified['paidappointment'];
        $landUnverifiedDeatil = $landUnverifiedDeatil;      
 
        // for deliver land report detail
        $deliverReports = DB::table('land_reports')
                ->where('status', 1);
        if ($filterDate == "week") {
            $deliverReports->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfWeek())
                    ->where('created_at', '<', now()->endOfWeek());
        } else if ($filterDate == "month") {
            $deliverReports->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfMonth()->format('Y-m-d'))
                    ->where('created_at', '<', now()->endOfMonth()->format('Y-m-d'));
        } else if ($filterDate == "year") {
            $deliverReports->select(DB::raw('MONTHNAME(created_at) as count_appointment'), DB::raw('count(*) as count_date'))->whereYear('created_at', Carbon::now()->year);
}
        $deliverReports = $deliverReports->groupBy('count_appointment')->get();
        if ($filterDate == "week") {
            $reportDelivered = getDateWiseData(now()->startOfWeek(), now()->endOfWeek(), $deliverReports);
        } else if ($filterDate == "month") {
            $reportDelivered = getDateWiseData(now()->startOfMonth(), now()->endOfMonth(), $deliverReports);
        } else if ($filterDate == "year") {
            $reportDelivered = getMonthWiseData($deliverReports);
        }

        $reportDeliveredDeatil = $reportDelivered['paidappointment'];
        $reportDeliveredDate = $reportDelivered['date'];
        
        // for pending land report detail
        $pendingReports = DB::table('land_reports')
                ->where('status', 0);
        if ($filterDate == "week") {
            $pendingReports->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfWeek())
                    ->where('created_at', '<', now()->endOfWeek());
        } else if ($filterDate == "month") {
            $pendingReports->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfMonth()->format('Y-m-d'))
                    ->where('created_at', '<', now()->endOfMonth()->format('Y-m-d'));
        } else if ($filterDate == "year") {
            $pendingReports->select(DB::raw('MONTHNAME(created_at) as count_appointment'), DB::raw('count(*) as count_date'))->whereYear('created_at', Carbon::now()->year);
}
        
        $pendingReports = $pendingReports->groupBy('count_appointment')->get();
        if ($filterDate == "week") {
            $reportPending = getDateWiseData(now()->startOfWeek(), now()->endOfWeek(), $pendingReports);
        } else if ($filterDate == "month") {
            $reportPending = getDateWiseData(now()->startOfMonth(), now()->endOfMonth(), $pendingReports);
        } else if ($filterDate == "year") {
            $reportPending = getMonthWiseData($pendingReports);
        }

        $reportPendingDeatil = $reportPending['paidappointment'];
        $reportPendingDeatil = $reportPendingDeatil;  
 
        return view('admin.dashboard', compact('adminEarningDate', 'pendingAppointment', 'paidPayments', 
                'adminEarningAmount', 'pendingAppointmentTitle', 'doneAppointmentTitle', 
                'landVerifiedDeatil', 'landVerifiedDate', 'landUnverifiedDeatil', 'total_app',
                'total_land', 'filterDate','total_report','reportDeliveredDeatil','reportDeliveredDate','reportPendingDeatil'));
}

}
