<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\State;
use App\Models\City;
use Illuminate\Support\Facades\Mail;
use App\Mail\BrokerMessage;
use App\Mail\BrokerUpdateByAdmin;
use App\Mail\BrokerUpdateConfirmation;
use App\Mail\UserRegistration;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BrokerController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
         $paginate = $request->paginate ?? 10;
          
         $allBrokers = User::role('broker')->get();
          $broker_name = $request->broker_name ?? '';
          $email = $request->email ?? '';
          $mobile = $request->mobile ?? '';

        $states = State::where('country_id', 101)->get();
        $cities = City::all();
        $query = User::selectRaw('users.id, users.name, users.user_unique_id, users.mobile, users.email, users.state_id, users.city_id, users.user_status');
          
            if (!empty($broker_name)) {
            $query->where('users.name', 'like', '%' . $request->broker_name . '%');
        }

            if (!empty($email)) {
                $query->where('email', 'like', '%' . $request->email . '%');
            }

            if (!empty($mobile)) {
                $query->where('mobile', 'like', '%' . $request->mobile . '%');
            }

        $query->groupBy('users.id');
        $brokers = $query->role('broker')->orderBy('users.created_at', 'desc')->paginate($paginate);
        
        return view('admin/brokers/brokers')->with([
                    'paginate' => $paginate,
                    'brokers' => $brokers,
                    'allBrokers' => $allBrokers,
                    'broker_name' => $broker_name,
                    'states' => $states,
                    'cities' => $cities,
                    'email' => $email,
                    'mobile' => $mobile,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    public function userstatus(Request $request) {
        $id = $request->id;
        $user_status = $request->user_status;

      
        $User_status = User::find($id);
        $User_status->user_status = $user_status;
      
         $User_status->save();
        return response()->Json(['success' => true, 'msg' => 'done']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeBroker(Request $request) {
      
        $rules = [
            'full_name' => 'required|regex:/^[a-z\s]+$/i|max:255',
            'email' => 'required|email|unique:users|email:rfc,dns',
            'mobile_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10|unique:users,mobile',
            'state_id' => 'required',
            'city_id' => 'required',
        ];
        
        //if id not empty then override the above rules
            if ($request->id) {
                $replace_rules = [
                    'full_name' => 'required|regex:/^[a-z\s]+$/i|max:255',
                    'email' => 'required|email|email:rfc,dns|' . Rule::unique('users')->ignore($request->id),
                'mobile_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10|' . Rule::unique('users', 'mobile')->ignore($request->id),
                ];
                $rules = array_replace($rules, $rules, $replace_rules);
            }

        $messages = [
            'full_name.required' => 'The full name field is required.',
            'email.required' => 'The email field is required.',
            'email.email' => 'The email must be a valid email address.',
            'mobile_number.required' => 'The mobile number field is required.',
            'mobile_number.regex' => 'The mobile number format is invalid.',
            'mobile_number.min' => 'The mobile number must be at least 10 digits.',
            'mobile_number.max' => 'The mobile number may not be greater than 10 digits.',
            'state_id.required' => 'State Field is required.',
            'city_id.required' => 'City Field is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
if ($request->id) {
    $user = User::find($request->id);
    $user->name = trim($request->full_name);
        $user->email = trim($request->email);
        $user->password = Hash::make(trim($request->mobile_number));
        $user->mobile = trim($request->mobile_number);
            $user->state_id = $request->state_id;
            $user->city_id = $request->city_id;
        
        if ($user->update()) {
                if (smtpConnect() == true) {
            $message = User::where('id', $user->id)->first();
           
                    Mail::to($user->email)->send(new BrokerUpdateByAdmin($message));
                    Mail::to(config('mail.from.admin_email'))->send(new BrokerUpdateConfirmation($message));
            }
              return response()->json(['success' => true, 'msg' => "Broker Successfully updated."]);
        } else {
            return response()->json(['success' => false, 'msg' => "Broker Successfully not updated."]);
        }
        die();
        } else {
        $user = new user();
        $user->name = trim($request->full_name);
        $user->email = trim($request->email);
        $user->password = Hash::make(trim($request->mobile_number));
        $user->mobile = trim($request->mobile_number);
        $user->is_pending = 1;
            $user->state_id = $request->state_id;
            $user->city_id = $request->city_id;

        if ($user->save()) {
            $user->assignRole('broker');
                if (smtpConnect() == true) {
            $message = User::where('id', $user->id)->first();
           
            Mail::to($user->email)->send(new BrokerMessage($message));
            Mail::to(config('mail.from.admin_email'))->send(new UserRegistration($message));
            }
            return response()->json(['success' => true, 'msg' => "Broker Successfully added."]);
        } else {
            return response()->json(['success' => false, 'msg' => "Broker Successfully not added."]);
        }

        die();
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update() {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
    
     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyBrokers(Request $request) {
            try {
            $ixExist = User::select('appointments.id as appointment_id', 'land_reports.id as land_report_id', 'property_list.id as property_list_id')
                            ->leftJoin('appointments', function ($join) {
                                $join->on('appointments.user_id', '=', 'users.id');
                            })
                            ->leftJoin('land_reports', function ($join) {
                                $join->on('land_reports.user_id', '=', 'users.id');
                            })
                            ->leftJoin('property_list', function ($join) {
                                $join->on('property_list.user_id', '=', 'users.id')->orOn('property_list.listed_by', '=', 'users.id');
                            })
                            ->where('users.id', $request->deletion_id)->first();
            if (!empty($ixExist->appointment_id) || !empty($ixExist->land_report_id) || !empty($ixExist->property_list_id)) {
                return redirect()->back()->with('error', 'You can\'t delete this user, beacuse its have appointment, land and land report.');
            } else {
                $user = User::findOrFail($request->deletion_id);
                $user->delete();
                return redirect()->back()->with('success', 'Broker Deleted Successfully!');
            }
            } catch (ModelNotFoundException $exception) {
                return redirect()->back()->with('error', "Broker Not Found.");
            }
                }
    
    public function getCityList(Request $request) {
        $cities = City::where('state_id', $request->input('stateId'))->get();
        return response()->json(['cities' => $cities]);
    }

}
