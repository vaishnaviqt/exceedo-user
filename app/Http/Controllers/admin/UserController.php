<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\ValueAddedService;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserMessage;
use App\Mail\UserUpdateByAdmin;
use App\Mail\UserUpdateConfirmation;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Mail\UserRegistration;
use App\Models\ModelHasPermission;
use App\Mail\SecondaryAdmin;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         $paginate = $request->paginate ?? 10;
          
         $allUsers = User::role('user')->get();
         
          $user_name = $request->user_name ?? '';
          $email = $request->email ?? '';
          $mobile = $request->mobile ?? '';
           
           $query = User::selectRaw('users.id, users.name, users.user_unique_id, users.mobile, users.email, users.user_status');
           
            if (!empty($user_name)) {
            $query->where('users.name', 'like', '%' . $request->user_name . '%');
        }

            if (!empty($email)) {
                $query->where('email', 'like', '%' . $request->email . '%');
             }

             if (!empty($mobile)) {
                $query->where('mobile', 'like', '%' . $request->mobile . '%');
             }

        $query->groupBy('users.id');
        $users = $query->role('user')->orderBy('users.created_at', 'desc')->paginate($paginate);
        
        return view('admin/users/users')->with([
                    'paginate' => $paginate,
                    'users' => $users,
                    'allUsers' => $allUsers,
                    'user_name' => $user_name,
                    'email' => $email,
                    'mobile' => $mobile,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function userstatus(Request $request){
        $id=$request->id;
       $user_status=$request->user_status;
      
      
       $User_status=User::find($id);
       $User_status->user_status=$user_status;

         $User_status->save();
        return response()->Json(['success' => true, 'msg' => 'done']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeUser(Request $request)
    {
      
        $rules = [
            'full_name' => 'required|regex:/^[a-z\s]+$/i|max:255',
            'email' => 'required|email|unique:users|email:rfc,dns',
            'mobile_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10|unique:users,mobile',
        ];
        
        //if id not empty then override the above rules
            if ($request->id) {
                $replace_rules = [
                    'full_name' => 'required|regex:/^[a-z\s]+$/i|max:255',
                    'email' => 'required|email|email:rfc,dns|' . Rule::unique('users')->ignore($request->id),
                    'mobile_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10|'. Rule::unique('users','mobile')->ignore($request->id),
                    
                ];
                $rules = array_replace($rules, $rules, $replace_rules);
            }

        $messages = [
            'full_name.required' => 'The full name field is required.',
            'email.required' => 'The email field is required.',
            'email.email' => 'The email must be a valid email address.',
            'mobile_number.required' => 'The mobile number field is required.',
            'mobile_number.regex' => 'The mobile number format is invalid.',
            'mobile_number.min' => 'The mobile number must be at least 10 digits.',
            'mobile_number.max' => 'The mobile number may not be greater than 10 digits.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
if ($request->id) {
    $user = User::find($request->id);
    $user->name = trim($request->full_name);
        $user->email = trim($request->email);
        $user->password = Hash::make(trim($request->mobile_number));
        $user->mobile = trim($request->mobile_number);
        
        if ($user->update()) {
            if(smtpConnect() == true){
            $message = User::where('id', $user->id)->first();
           
                    Mail::to($user->email)->send(new UserUpdateByAdmin($message));
                    Mail::to(config('mail.from.admin_email'))->send(new UserUpdateConfirmation($message));
            }
              return response()->json(['success' => true, 'msg' => "User Successfully updated."]);
        } else {
            return response()->json(['success' => false, 'msg' => "User Successfully not updated."]);
        }
        die();
}else{
        $user = new user();
        $user->name = trim($request->full_name);
        $user->email = trim($request->email);
        $user->password = Hash::make(trim($request->mobile_number));
        $user->mobile = trim($request->mobile_number);
        $user->is_pending = 1;

        if ($user->save()) {
            $user->assignRole('user');
            if(smtpConnect() == true){
            $message = User::where('id', $user->id)->first();
            Mail::to($user->email)->send(new UserMessage($message));
                    Mail::to(config('mail.from.admin_email'))->send(new UserRegistration($message));
            }
            return response()->json(['success' => true, 'msg' => "User Successfully added."]);
        } else {
            return response()->json(['success' => false, 'msg' => "User Successfully not added."]);
        }

        die();
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyUsers(Request $request) {
            try {
            $ixExist = User::select('appointments.id as appointment_id', 'land_reports.id as land_report_id', 'property_list.id as property_list_id')
                            ->leftJoin('appointments', function ($join) {
                                $join->on('appointments.user_id', '=', 'users.id');
                            })
                            ->leftJoin('land_reports', function ($join) {
                                $join->on('land_reports.user_id', '=', 'users.id');
                            })
                            ->leftJoin('property_list', function ($join) {
                                $join->on('property_list.listed_by', '=', 'users.id');
                            })
                            ->where('users.id', $request->deletion_id)->first();

            if (!empty($ixExist->appointment_id) || !empty($ixExist->land_report_id) || !empty($ixExist->property_list_id)) {
                return redirect()->back()->with('error', 'You can\'t delete this user, beacuse its have appointment, land and land report.');
                } else {
                $user = User::findOrFail($request->deletion_id);
                $user->delete();
                return redirect()->back()->with('success', 'User Deleted Successfully!');
            }
        } catch (ModelNotFoundException $exception) {
            return redirect()->back()->with('error', "User Not Found.");
        }
    }
    
    public function secondaryAdmin(Request $request) {
        $rules = [
            'expert_id' => 'required',
            'manager_id' => 'required|array',
        ];

        $messages = [
            'expert_id.required' => 'The Expert field is required.',
            'manager_id.required' => 'The Permission field is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
}

        ModelHasPermission::where('model_id', $request->input('expert_id'))->whereNotIn('manager_id', $request->input('manager_id'))->delete();
        foreach ($request->input('manager_id') as $manager) {
            $permission = ModelHasPermission::where('model_id', $request->input('expert_id'))->where('manager_id', $manager)->first();
            if (empty($permission)) {
                $permission = new ModelHasPermission;
                $modelRole = \App\Models\ModelHasRole::where('model_id', $request->input('expert_id'))->first();
                $user = User::where('id', $request->input('expert_id'))->first();
                if (!$user->hasRole('admin')) {
                    $user->assignRole('admin');
                }
            }
            $permission->model_id = $request->input('expert_id');
            $permission->manager_id = $manager;
            $permission->model_type = 'App\Models\User';
            $saved = $permission->save();
        }
        if ($saved) {
             if (smtpConnect() == true) {
                 $user = User::where('id', $request->input('expert_id'))->first();
                    Mail::to($user->email)->send(new SecondaryAdmin($user));
                }
            return response()->json(['success' => true, 'msg' => "Secondary admin created successfully"]);
        } else {
            return response()->json(['success' => false, 'msg' => "Internal server error, please try again"]);
        }
    }

    /**
     * edit secondary admin
     * @param Request $request
     * @return type
     */
    public function editSecondaryAdmin(Request $request) {
        $permission = ModelHasPermission::where('model_id', $request->input('id'))->get();
        return response($permission);
    }

    public function deleteSecondaryAdmin($id) {
        $permission = ModelHasPermission::where('model_id', $id)->delete();
        $user = User::where('id', $id)->first();
        $user->removeRole('admin');
        return redirect()->back()->with('success', 'Secondary admin deleted successfully');
    }

}
