<?php

namespace App\Http\Controllers\admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\Appointment;
use App\Models\ModelHasRole;
use App\Mail\ExpertAppointmentAssign;
use App\Models\ValueAddedService;
use App\Models\User;
use App\Mail\AdminUserAppointmentBooked;
use App\Mail\AdminUserAppointmentCancel;
use App\Mail\AdminExpertAppointmentCancel;
use App\Mail\AdminAppointmentCancel;
use App\Mail\AdminAppointmentAssignedto;
use App\Mail\UserAppointmentBooked;
use Razorpay\Api\Api;
use App\Models\Payment;
use App\Models\Notification;
use Illuminate\Support\Facades\Mail;
use Craftsys\Msg91\Facade\Msg91;
use View;
use Exception;

class AppointmentController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $filterError = '';
        $experts = User::role('expert')->where('is_pending', 1)->get();
        $totalAppointments = Appointment::count();
        $upcommingAppointments = Appointment::where('date', '>', Carbon::now())->count();
        $doneAppointments = Appointment::where('status', 1)->where('date', '<', Carbon::now())->count();
        $services = ValueAddedService::all();
        $users = User::role('user')->where('is_pending', 1)->get();
        //All appointment listing with filter
        $allDate = $request->input('all_date') ?? '';
        $allExpert = $request->input('all_expert_id') ?? '';
        $allCustomerId = $request->input('customer_id') ?? '';
        $allPaymentType = $request->input('payment_type') ?? '';
        $serviceId = $request->input('value_added_service_id') ?? '';
        $allAppoints = Appointment::orderBy('created_at', 'DESC')->get();
        if ($request->input('submit') == 'allSearch') {
            if (!empty($allDate) && !empty($allExpert)) {

                $allAppoints = Appointment::whereDate('date', Carbon::parse($allDate)->format('Y-m-d'))->where('expert_id', $allExpert)->orderBy('created_at', 'DESC')->get();
            } elseif ($allDate) {
                $allAppoints = Appointment::whereDate('date', Carbon::parse($allDate)->format('Y-m-d'))->orderBy('created_at', 'DESC')->get();
            } elseif ($allExpert) {

                $allAppoints = Appointment::where('expert_id', $allExpert)->orderBy('created_at', 'DESC')->get();
            }
            $filterError = "allSearch";
        }

        //pending appointment listing with filter
        $pendingDate = $request->input('date') ?? '';
        $pendingExpert = $request->input('expert_id') ?? '';
        $pendingAppoints = Appointment::where('status', '!=', 1);
        if ($request->input('submit') == 'pendingSeacrh') {
            if ($pendingDate) {
                $pendingAppoints->whereDate('date', Carbon::parse($pendingDate)->format('Y-m-d'));
            }
            if ($pendingExpert) {
                $pendingAppoints->where('expert_id', $pendingExpert);
            }
            $filterError = "pendingExpert";
        } else {
            $pendingAppoints->where('date', '>=', Carbon::now());
        }
        $pendingAppoints = $pendingAppoints->orderBy('created_at', 'DESC')->get();
        return view('admin.appointments.appointments', compact('services', 'experts', 'users', 'totalAppointments', 'upcommingAppointments', 'doneAppointments', 'allAppoints', 'pendingAppoints', 'pendingDate', 'pendingExpert', 'filterError', 'allDate', 'allExpert', 'allPaymentType', 'allCustomerId', 'serviceId'));
    }

    /**
     * Appointment hsow in calenderservices
     * 
     * @param Request $request
     * @return type
     */
    public function appointmentDetails(Request $request) {
        $appoint = Appointment::with('service', 'user', 'expert')->whereNotNull('status');
        $expertId = $request->input('expertId') ?? '';
        $userId = $request->input('userId') ?? '';
        $paymentType = $request->input('paymentType') ?? '';
        $valueAddedServiceId = $request->input('valueAddedServiceId') ?? '';
        if (!empty($expertId)) {
            $appoint->where('expert_id', $expertId);
        }
        if (!empty($userId)) {
            $appoint->where('user_id', $userId);
        }
        if (!empty($paymentType)) {
            $appoint->where('payment_mode', 'LIKE', '%' . $paymentType . '%');
        }
        if (!empty($valueAddedServiceId)) {
            $appoint->where('value_added_service_id',   $valueAddedServiceId );
        }
        $appointments = $appoint->get();
        $reports = array();
        $key = 0;
        foreach ($appointments as $appointment) {
            $reports[$key]['title'] = isset($appointment->service->service_name) ? $appointment->service->service_name : '';
            $reports[$key]['status'] = $appointment->status;
            $reports[$key]['rescheduled_by'] = $appointment->rescheduled_by;
            $reports[$key]['expert_id'] = $appointment->expert_id;
            $reports[$key]['id'] = $appointment->id;
            $reports[$key]['canceled_by'] = $appointment->canceled_by;
            $reports[$key]['expert_name'] = isset($appointment->expert->name) ? $appointment->expert->name : 'Pending assing to expert';
            $reports[$key]['user_name'] = isset($appointment->user->name) ? $appointment->user->name : '';
            $reports[$key]['price'] = $appointment->price ;
            $reports[$key]['canceled_by'] = $appointment->canceled_by;
            if (Carbon::parse($appointment->meeting_at)->format('Y-m-d H:i') > date('Y-m-d H:i') && empty($appointment->expert_id)) {
                $reports[$key]['backgroundColor'] = 'yellow';
                $reports[$key]['borderColor'] = 'yellow';
            }elseif($appointment->cancel == 1){
                $reports[$key]['backgroundColor'] = 'red';
                $reports[$key]['borderColor'] = 'red';
            }elseif(Carbon::parse($appointment->meeting_at)->format('Y-m-d H:i') > date('Y-m-d H:i') && !empty($appointment->expert_id)){
                $reports[$key]['backgroundColor'] = 'blue';
                $reports[$key]['borderColor'] = 'blue';   
            }else{
                $reports[$key]['backgroundColor'] = 'green';
                $reports[$key]['borderColor'] = 'green';   
                
            }
            $reports[$key]['start'] = Carbon::parse($appointment->date . $appointment->time)->format('Y-m-d H:i:s');
            $reports[$key]['allDay'] = false;
            $key++;
        }
        return response()->json($reports);
    }

    /**
     * Appointment assign to expert
     * 
     * @param Request $request
     * @return type
     */
    public function assignToExpert(Request $request) {
        try {
            $appointment = Appointment::where('id', $request->input('appointId'))->first();
            $expert_id = $request->input('expertId');
            $appointment->expert_id = $expert_id;
            $saved = $appointment->save();

            // after assing apppointment to expert notification detail store 
            $appoint = Appointment::with('user')->where('id', $appointment->id)->first();
            $noti = new Notification;
            $noti->notification_to = 'expert';
            $noti->user_id = $expert_id;       //change by shivani
            $noti->apppointment_id = $appoint->id;
            $noti->message = "You recive a new appointment with " . $appoint->user->name . " on " . Carbon::parse($appointment->meeting_at)->format('d M, Y h:i A');
            $noti->save();
            $noti = new Notification;
            $noti->notification_to = 'admin';
            $noti->user_id = 1;    //change by shivani
            $noti->apppointment_id = $appoint->id;
            $noti->message = "You recive a new appointment with " . $appoint->expert->name . " on " . Carbon::parse($appointment->meeting_at)->format('d M, Y h:i A');
            $noti->save();
            if ($saved && smtpConnect() == true) {
                $appointment = Appointment::with('expert')->where('id', $appoint->id)->first();
                Mail::to($appoint->expert->email)->send(new ExpertAppointmentAssign($appoint));
                Mail::to(config('mail.from.admin_email'))->send(new AdminAppointmentAssignedto($appoint));
            }
        } catch (Exception $e) {
            $saved = false;
        }
        return response($request->input('expertId'));
    }

    /**
     * view all appointment with ajax and filter functionality
     * @param Request $request
     * @return type
     */
    public function viewAllAppointment(Request $request) {
        $allDate = $request->input('all_date') ?? '';
        $allExpert = $request->input('all_expert_id') ?? '';
        $service_id = $request->input('service_id') ?? '';
        $allServices = ValueAddedService::all();
        
        $experts = User::role('expert')->where('is_pending', 1)->get();
        $allAppoints = Appointment::with('service', 'user', 'expert');

        if (!empty($allExpert)) {

            $allAppoints->where('expert_id', $allExpert);
        }
        if (!empty($allDate)) {

            $allAppoints->whereDate('date', Carbon::parse($allDate)->format('Y-m-d'));
        }

        if (!empty($service_id)) {

            $allAppoints->where('value_added_service_id', $service_id);
        }
        $allAppoints = $allAppoints->orderBy('created_at', 'DESC')->get();
        $filterError = "allSearch";

        $view = View('admin.appointments.appointment-list-card', compact('allServices', 'allDate', 'allExpert', 'allAppoints', 'experts'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * pending all appointment with ajax and filter functionality
     * @param Request $request
     * @return type
     */
    public function viewPendingAppointment(Request $request) {
        $pendingDate = $request->input('date') ?? '';
        $pendingExpert = $request->input('expert_id') ?? '';
        $serviceId = $request->input('serviceId') ?? '';
         $services = ValueAddedService::all();
        $pendingAppoints = Appointment::with('service', 'user', 'expert')->where('status', '!=', 1);

        if ($pendingDate) {
            $pendingAppoints->whereDate('date', Carbon::parse($pendingDate)->format('Y-m-d'));
        }
        if ($pendingExpert) {
            $pendingAppoints->where('expert_id', $pendingExpert);
        }
         if ($serviceId) {
            $pendingAppoints->where('value_added_service_id', $serviceId);
        }
        $filterError = "pendingExpert";
        if (empty($pendingDate) && empty($pendingExpert)) {
            $pendingAppoints->where('date', '>=', Carbon::now());
        }
        $pendingAppoints = $pendingAppoints->orderBy('created_at', 'DESC')->get();
        $view = View('admin.appointments.pending-appointment-list-card', compact('pendingDate', 'pendingExpert', 'pendingAppoints', 'serviceId', 'services'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * Done all appointment with ajax and filter functionality
     * @param Request $request
     * @return type
     */
    public function viewDoneppointment(Request $request) {
        $doneDate = $request->input('date') ?? '';
        $doneExpert = $request->input('expert_id') ?? '';
        $serviceId = $request->input('serviceId') ?? '';
        $experts = User::role('expert')->where('is_pending', 1)->get();
        $services = ValueAddedService::all();
        $doneAppoints = Appointment::where('status', '=', 1);

        if ($doneDate) {
            $doneAppoints->whereDate('date', Carbon::parse($doneDate)->format('Y-m-d'));
        }
        if ($doneExpert) {
            $doneAppoints->where('expert_id', $doneExpert);
        }
        if ($serviceId) {
            $doneAppoints->where('value_added_service_id', $serviceId);
        }
        $filterError = "pendingExpert";
        if (empty($doneDate) && empty($doneExpert)) {
            $doneAppoints->where('date', '<', Carbon::now());
        }
        $doneAppoints = $doneAppoints->orderBy('created_at', 'DESC')->get();
        $view = View('admin.appointments.done-appointment-list-card', compact('doneDate', 'doneExpert', 'doneAppoints', 'experts', 'serviceId', 'services'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * admin appointment booked for user
     * 
     * @param Request $request
     * @return type
     */
    public function addAppointment(Request $request) {
        $rules = [
            'user_id' => 'required',
            'value_added_service_id' => 'required',
            'date' => 'required|after:' . now(),
            'time' => 'required',
            'remarks' => 'required',
            'terms' => 'required',
            'payment_mode' => 'required',
        ];

        $messages = [
            'user_id.required' => 'The User field is required.',
            'value_added_service_id.required' => 'The Value Added Service field is required.',
            'date.required' => 'Appointment Date field required.',
            'time.required' => 'The Appointment Time field is required.',
            'remarks' => 'The Remarks Field is required.',
            'terms' => 'The Terms Field is required',
            'payment_mode' => 'The Payment Mode Field is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        try {
            $appointment = new Appointment;
            $user_id = $request->input('user_id');
            $appointment->user_id = $user_id;
            $appointment->price = $request->input('price');
            $appointment->value_added_service_id = $request->input('value_added_service_id');
            $appointment->date = Carbon::parse($request->input('date'))->format('Y-m-d');
            $appointment->time = Carbon::parse($request->input('time'))->format('H:i:s');
            $appointment->meeting_at = Carbon::parse($request->input('date') . $request->input('time'))->format('Y-m-d H:i:s');
            $appointment->remarks = $request->input('remarks');
            $appointment->payment_mode = $request->input('payment_mode');
            $appointment->terms = $request->input('terms');
            $appointment->status = 0;
            $date = Carbon::parse($request->input('date') . $request->input('time'))->format('d M, Y h:i A');

            $saved = $appointment->save();
            // after assing apppointment to expert notification detail store 
            $appoint = Appointment::with('user')->where('id', $appointment->id)->first();
            $noti = new Notification;
            $noti->notification_to = 'user';
            $noti->user_id = $user_id;  //change by shivani
            $noti->apppointment_id = $appointment->id;
            $noti->message = "Your appointment booked with " . $appoint->user->name . " on " . $date;
            $noti->save();
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            $appoint = Appointment::with('user')->where('id', $appointment->id)->first();
            if (!empty($appoint->user->mobile)) {
                $msg = Msg91::sms()->recipients([
                            ['mobiles' => $appoint->user->mobile, 'name' => $appoint->user->name, 'date' => $date],
                        ])
                        ->flow('60adf8b6c3fad12ed018037c')
                        ->send();
            }

            if (smtpConnect() == true) {
                Mail::to($appoint->user->email)->send(new UserAppointmentBooked($appoint));
                Mail::to(config('mail.from.admin_email'))->send(new AdminUserAppointmentBooked($appoint));
            }
            return response()->json(['success' => true, 'msg' => "Appointment Booked Successfully."]);
        } else {
            return response()->json(['success' => false, 'msg' => "Internal server error, Please Try again"]);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function showAllExpert(Request $request) {
        try {
            $experts = User::role('expert')->where('is_pending', 1)->get();
        } catch (Exception $e) {
            $experts = '';
        }
        return response()->json($experts);
    }

    public function cancelAppoint($id) {
        $saved = false;
        try {
            $api = new Api(config('app.razorpay_api_key'), config('app.seceret_key'));
            $appoint = Appointment::with('expert')->where('id', $id)->first();
            $paymentData = $api->payment->fetch($appoint->payment_id);
            if (!empty($appoint)) {
                $appoint->status = 3;
                $appoint->canceled_by = 2;
                $saved = $appoint->save();
                if (!empty($appoint->payment_id)) {
                    $paymentData = $api->payment->fetch($appoint->payment_id);
                    if (!empty($paymentData) && $paymentData->status == 'captured') {
                        $refund = $api->refund->create(array('payment_id' => $appoint->payment_id));
                        $payment = Payment::where('transaction_id', $appoint->payment_id)->first();
                        $payment->refund_id = $refund['id'];
                        $payment->entity = $refund['entity'];
                        $payment->status = $refund['status'];
                        $payment->refund_Date = date("Y-m-d h:i:s", $refund['created_at']);
                        $saved = $payment->save();
                        //admin appointment cencel show notification to user
                        $noti = new Notification;
                        $noti->notification_to = 'user';
                        $noti->user_id = $appoint->user->id;  //change by shivani
                        $noti->apppointment_id = $appoint->id;
                        $noti->message = "You recive a new appointment with" . $appoint->expert->name . " on " . Carbon::parse($appointment->meeting_at)->format('d M, Y h:i A');
                        $noti->save();
                        $noti = new Notification;
                        $noti->notification_to = 'expert';
                        $noti->user_id = $appoint->expert->id;   //change by shivani
                        $noti->apppointment_id = $appoint->id;
                        $noti->message = "You recive a new appointment with" . $appoint->expert->name . " on " . Carbon::parse($appointment->meeting_at)->format('d M, Y h:i A');
                        $noti->save();
                        $noti = new Notification;
                        $noti->notification_to = 'admin';
                        $noti->user_id = 1;
                        $noti->apppointment_id = $appoint->id;   //change by shivani
                        $noti->message = "You recive a new appointment with" . $appoint->expert->name . " on " . Carbon::parse($appointment->meeting_at)->format('d M, Y h:i A');
                        $noti->save();
                    }
                }
            }
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            if (smtpConnect() == true) {
                $appointment = Appointment::with('user')->where('id', $id)->first();
                Mail::to($appointment->user->email)->send(new AdminUserAppointmentCancel($appointment));
                Mail::to(config('mail.from.admin_email'))->send(new AdminAppointmentCancel($appointment));
                Mail::to($appointment->expert->email)->send(new AdminExpertAppointmentCancel($appointment));
            }
            return redirect()->back()->with('success', __('Appointment canceled and payment refunded successfully!'));
        } else {
            return back()->withInput()->with('error', __('Something went wrong, Please try again later!'));
        }
    }

    /**
     * notification mark as read
     * 
     * @param type $id
     */
    public function markReadNotification($id) {
        $noti = Notification::where('id', $id)->first();
        $noti->unread = 1;
        $noti->save();
        return true;
    }

}
