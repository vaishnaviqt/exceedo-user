<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;
use Validator;
use Illuminate\Validation\Rule;
use Storage;

class PageController extends Controller {

    /**
     * 
     * @return type
     */
    public function index(Request $request) {
        $num = $request->input('showBy') ?? 10;
        $pages = Page::where('status', 1)->paginate($num);
        return view('admin.pages.pages', compact('pages', 'num'));
    }

    /**
     * 
     * @return type
     */
    public function create() {
        return view('admin.pages.page-create');
    }

    /**
     * New Page create
     * 
     * @param Request $request
     * @return boolean
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title' => 'required',
                    'slug' => 'required|unique:pages',
                    'description' => 'required',
                    'description' => 'required',
                    'thump' => 'mimes:jpeg,png,jpg,bmp,gif|max:1024',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        $path = '';
        if ($request->file('thumb')) {
            $path = $request->file('thumb')->store(
                    'thumb', 'pages'
            );
        }
        if ($path) {//        dump()
            $path = 'page_image/' . $path;
        }
        try {
            $page = new Page;
            $page->title = $request->input('title');
            $page->slug = $request->input('slug');
            $page->description = $request->input('description');
            $page->meta_title = $request->input('meta_title') ?? '';
            $page->meta_description = $request->input('meta_description') ?? '';
            $page->meta_keywords = $request->input('meta_keywords') ?? '';
            $page->thumb = $path ?? '';
            $page->status = 1;
            $saved = $page->save();
        } catch (Exception $e) {
            report($ex);
            return false;
        }
        if ($saved) {
            return redirect('admin/pages')->with('success', 'Page Created Successfully!');
        } else {
            return redirect()->back()->with('error', 'Something went wrong, Please try again later!');
        }
    }

    /**
     * ckeditor image upload
     * 
     * @param Request $request
     * @return type
     */
    public function imageUpload(Request $request) {
        if ($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName . '_' . time() . '.' . $extension;
            $request->file('upload')->move(public_path('/storage/pages'), $fileName);
            $url = asset('storage/pages/' . $fileName);
            $funcNum = $request->input('CKEditorFuncNum');
            $msg = 'Image uploaded successfully';
            $response = '<script>window.parent.CKEDITOR.tools.callFunction(1,' . $url . ', ' . $msg . ')</script>';
            @header('Content-type: text/html; charset=utf-8');
            return "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$msg') </script>";
        }
        return view('admin.pages.page-create');
    }

    /**
     * get page detail by slug
     * 
     * @param type $slug
     * @return type
     */
    public function editPage($slug) {
        $page = Page::where('slug', $slug)->first();
        return view('admin.pages.edit-page', compact('page'));
    }

    /**
     * delete page
     * 
     * @param type $id
     * @return type
     */
    public function deletePage($id) {
        $page = Page::where('id', $id)->first();
        if ($page->delete()) {
            return redirect('admin/pages')->with('success', 'Page Deleted Successfully!');
        } else {
            return redirect()->back()->with('error', 'Something went wrong, Please try again later!');
        }
    }

    /**
     * page update
     * 
     * @param Request $request
     * @return boolean
     */
    public function updatePage(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title' => 'required',
                    'slug' => 'required', Rule::unique('users')->ignore($request->input('user_id')),
                    'description' => 'required',
                    'description' => 'required',
                    'thump' => 'mimes:jpeg,png,jpg,bmp,gif|max:1024',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        $path = '';
        if ($request->file('thumb')) {
            $path = $request->file('thumb')->store(
                    'thumb', 'pages'
            );
        }
        if ($path) {
            $path = 'page_image/' . $path;
        }

        try {
            $previous = str_replace('page_image/', '', $request->input('old_image'));
            $page = Page::find($request->input('id'));
            $page->title = $request->input('title');
            $page->slug = $request->input('slug');
            $page->description = $request->input('description');
            $page->meta_title = $request->input('meta_title') ?? '';
            $page->meta_description = $request->input('meta_description') ?? '';
            $page->meta_keywords = $request->input('meta_keywords') ?? '';
            if (!empty($path)) {
                $page->thumb = $path ?? '';
            }
            $page->status = 1;
            $saved = $page->save();
            if ($saved && !empty($path) && !empty($previous) && Storage::disk('pages')->exists($previous)) {
                Storage::disk('pages')->delete($previous);
            }
        } catch (Exception $e) {
            report($ex);
            return false;
        }
        if ($saved) {
            return redirect('admin/pages')->with('success', 'Page Updated Successfully!');
        } else {
            return redirect()->back()->with('error', 'Something went wrong, Please try again later!');
        }
    }

}
