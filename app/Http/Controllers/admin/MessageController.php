<?php

namespace App\Http\Controllers\admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\Message;
use App\Mail\AdminMessage;
use App\Jobs\MessageSend;
use Mail;
use DB;

class MessageController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if (!empty($request)) {
            $message = Message::with('user');
            $date = $request->input('date') ?? '';
            $expertId = $request->input('expert_id') ?? '';
            $subject = $request->input('subject') ?? '';
            $searchDate = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
            if (!empty($expertId)) {

                $message->where('user_id', $expertId)->orWhere('sender_id', $expertId);
            }
            if (!empty($subject)) {
                $message->where('message', 'like', '%' . $subject . '%');
                $message->orWhere('subject', 'like', '%' . $subject . '%');
            }
            if (!empty($searchDate)) {
                $message->whereDate('created_at', $searchDate);
            }
            $messages = $message->orderBy('created_at', 'desc')->paginate(10);
        } else {
            $message = Message::with('user');
            $date = $request->input('date') ?? '';
            $searchDate = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
            $subject = $request->input('subject') ?? '';
            $expertId = $request->input('expert_id') ?? '';
            if (!empty($expertId)) {
                $message->where('user_id', $expertId)->orWhere('sender_id', $expertId);
                ;
            }
            if (!empty($searchDate)) {
                $message->whereDate('created_at', $searchDate);
            }
            $messages = $message->orderBy('created_at', 'desc')->paginate(10);
        }

        $users = User::all();
        return view('admin.messages', compact('messages', 'users', 'expertId', 'searchDate'));
    }

    /**
     * user email auto suggest
     * @param Request $request
     * @return type
     */
    public function userEmailAutoSuggest(Request $request) {
        $users = User::where('email', 'like', '%' . $request->keyword . '%')->get()->pluck('email');
        return response($users);
    }

    public function messageSend(Request $request) {
        $path = '';
       
        if ($request->file) {
            $statement = DB::select("SHOW TABLE STATUS LIKE 'messages'");
            $nextId = (isset($statement[0]->Auto_increment)) ? $statement[0]->Auto_increment : uniqid();
            $file_name = trim($request->user()->first_name) . '_' . trim($request->user()->last_name) . '_' . $nextId;
            $fileName = $file_name . '.' . $request->file('file')->getClientOriginalExtension();
            $path = 'message' . '/' . $request->file('file')->storeAs($request->user()->id, $fileName, 'message');
        }
        if ($request->input('email')) {
            $user = User::where('email', $request->input('email'))->first();
            $this->messageStore($request, $user->id, $path);
        }
        if ($request->input('allExperts')) {
            $users = User::role('expert')->get();
            if (count($users)) {
                foreach ($users as $user) {
                    $this->messageStore($request, $user->id, $path);
                }
            }
        }
        if ($request->input('allBrokers')) {
            $users = User::role('broker')->get();
            if (count($users)) {
                foreach ($users as $user) {
                    $this->messageStore($request, $user->id, $path);
                }
            }
        }
        if ($request->input('allUsers')) {
            $users = User::role('user')->get();
            if (count($users)) {
                foreach ($users as $user) {
                    $this->messageStore($request, $user->id, $path);
                }
            }
        }

        return redirect()->back()->with('success', __('Message send successfully!'));
    }

    private function messageStore($request, $userId, $path) {
        $user = User::where('email', $request->input('email'))->first();
        $message = new Message;
        $message->user_id = $userId;
        $message->sender_id = auth()->user()->id;
        $message->message = $request->input('message');
        $message->subject = $request->input('emailSubject');
        $message->file = $path;
        $message->status = 1;
        $saved = $message->save();
        if ($saved) {
            MessageSend::dispatch($message)->delay(Carbon::now()->addSeconds(5));
        }
    }

}
