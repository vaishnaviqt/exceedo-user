<?php

namespace App\Http\Controllers\admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\PropertyList;
use App\Models\User;
use App\Models\PropertyListImage;
use Illuminate\Support\Facades\Mail;
use App\Mail\LandAssignToAgent;
use App\Mail\LandAssignConfirm;
use App\Mail\AdminNewLandConfirm;
use Str;
use Storage;
use App\Models\LandOtherDocument;
use App\Models\Notification;
use Illuminate\Validation\Rule;
use Exception;

class LandListingController extends Controller
{

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $agents = User::role('broker')->where('is_pending', 1)->get();
        $land = $this->getCount();
        $name = $request->name ?? '';
        $address = $request->address ?? '';
        $businessType = $request->business_type ?? '';
        $startDate = !empty($request->created_at) ? Carbon\Carbon::parse($request->created_at)->format('Y-m-d') : '';
        $allLands = PropertyList::propertyList($request);
        return view('admin.land.listing', compact('allLands', 'name', 'startDate', 'address', 'land', 'agents', 'businessType'));
    }

    /**
     * all active land listing
     * 
     * @param Request $request
     * @return type
     */
    public function activeLandListing(Request $request)
    {
        $agents = User::role('broker')->where('is_pending', 1)->get();
        $land = $this->getCount();
        $name = $request->name ?? '';
        $address = $request->address ?? '';
        $businessType = $request->business_type ?? '';
        $startDate = !empty($request->created_at) ? Carbon\Carbon::parse($request->created_at)->format('Y-m-d') : '';
        $activelands = PropertyList::propertyList($request, 1);
        return view('admin.land.active-land-listing', compact('activelands', 'name', 'address', 'startDate', 'land', 'agents', 'businessType'));
    }

    /**
     * all inactive land listing
     * 
     * @param Request $request
     * @return type
     * 
     */
    public function inActiveListing(Request $request)
    {
        $agents = User::role('broker')->where('is_pending', 1)->get();
        $land = $this->getCount();
        $name = $request->name ?? '';
        $address = $request->address ?? '';
        $businessType = $request->business_type ?? '';
        $startDate = !empty($request->created_at) ? Carbon\Carbon::parse($request->created_at)->format('Y-m-d') : '';
        $inactiveLands = PropertyList::propertyList($request, 2);

        return view('admin.land.inactive-land-listing', compact('inactiveLands', 'name', 'address', 'startDate', 'land', 'agents', 'businessType'));
    }

    /**
     * all requested land listing
     * 
     * @param Request $request
     * @return type
     */
    public function requestListing(Request $request)
    {
        $agents = User::role('broker')->where('is_pending', 1)->get();
        $land = $this->getCount();
        $name = $request->name ?? '';
        $address = $request->address ?? '';
        $businessType = $request->business_type ?? '';
        $startDate = !empty($request->created_at) ? Carbon\Carbon::parse($request->created_at)->format('Y-m-d') : '';
        $requestLands = PropertyList::propertyList($request, 'pending');
        return view('admin.land.request-land-listing', compact('requestLands', 'name', 'address', 'startDate', 'land', 'agents', 'businessType'));
    }

    /**
     * count land listing according status
     * @return type
     */
    public function getCount()
    {
        $land['all'] = PropertyList::all()->count();
        $land['active'] = PropertyList::where('status', 1)->count();
        $land['inactive'] = PropertyList::where('status', 0)->count();
        $land['request'] = PropertyList::where('status', 2)->count();

        return $land;
    }

    /**
     * show expert and broker mobile number
     * 
     * @param type $id
     * @return boolean
     */
    public function showAgentMobile($id)
    {
        try {
            $agent = User::where('id', $id)->first();
        } catch (Exception $ex) {
            return false;
        }
        return response($agent);
    }

    /**
     * property detail add
     * 
     * @param Request $request
     * @return type
     */
    public function propertyStore(Request $request)
    {
        // print_r($request->post());
        // die;
        $rules = [
            'land_name' => 'required',
            'unique_id' => 'required|unique:property_list',
            'land_price' => 'required',
            'price_unit' => 'required',
            'size_of_land' => 'required',
            'nearby_place' => 'required',
            'address' => 'required',
            'descritpion' => 'required',
            'user_id' => 'required',
            'user_pan' => 'required',
            'is_corporate' => 'required',
            // 'exclusive_channel_partner' => 'required',
            // 'exclusive_mandate' => 'required',
            'business_type' => 'required',
            // 'est_payment' => 'required',
            // 'session1_end_time' => 'nullable|after:session1_start_time',
            // 'session2_start_time' => 'nullable|after:session1_end_time',
            //            'session2_end_time' => 'nullable|after:session2_end_time',
        ];
        if ($request->input('is_corporate') == 'company') {
            $rules['user_gst'] = 'required';
        }
        if (!isset($request->input('new_images')[0])) {
            $rules['other_images'] = "required|array|min:1";
        }

        $messages = [
            'land_name.required' => 'The Land Name is required.',
            'land_price.required' => 'The Land Price field is required',
            'size_of_land.required' => 'The land Size field is required.',
            'nearby_place.required' => 'The Near By palace field is required.',
            'address.required' => 'The Address field is required.',
            'user_id.required' => 'The Agent field is required.',
            'user_gst.required' => 'The Gst No field is required.',
            'user_pan.required' => 'The Pan No field is required.',
            'unique_id.required' => 'The Unique Id field is required.',
            // 'session1_end_time.after' => 'The session1 end time must be a date after session1 start time.',
            // 'session2_start_time.after' => 'The session2 start time must be a date after session1 end time.',
            //            'session2_end_time.after' => 'The session2 end time must be a date after session2 start time.',
        ];
        $videoUrl = $request->input('video_link');
        $validator = Validator::make($request->all(), $rules, $messages);
        // $videoId = '';

        // if (!empty($request->file('video_link'))) {
        //     $link = $videoUrl;
        //     $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
        //     if (empty($video_id[1]))
        //         $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..
        //     $video_id = explode("&", $video_id[1]); // Deleting any other params
        //     $videoId = $video_id[0];
        // }


        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => 'There are incorect values in the form!',
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }

        try {
            $propery = new PropertyList;
            $propery->land_name = $request->input('land_name');
            $propery->max_price = $request->input('land_price');
            $propery->price_unit = $request->input('price_unit');
            $propery->size_of_land = $request->input('size_of_land');
            $propery->nearby_place = $request->input('nearby_place');
            $propery->address = $request->input('address');
            $propery->descritpion = $request->input('descritpion');
            $propery->latitute = $request->input('latitute');
            $propery->longitude = $request->input('longitude');
            $user_id = $request->input('user_id');  //change by shivani
            $propery->user_id = $user_id;     //change by shivani
            // $propery->video_link = $videoUrl??null;     //change by shivani
            //      if($request->input('video_link'))
            //     {
            //     $file=$request->file('video_link');
            //     $image_parts = explode(";base64,", $image);
            //     $image_type_aux = explode("video/", $image_parts[0]);
            //      $image_type = $image_type_aux[1];
            //      $image_base64 = base64_decode($image_parts[1]);
            //     $file = uniqid() . '. ' . $image_type;
            //     Storage::disk('propertyImage')->put($file, $image_base64);
            //     $imagePath = 'storage/propertyimage/' . $file;
            //     $propery->video_link = $imagePath;
            // }
            //     print_r($imagePath)."<br>";

            //     die;
            $propery->gst = $request->input('user_gst') ?? null;
            $propery->pan = $request->input('user_pan');
            $propery->is_corporate = $request->input('is_corporate');
            $propery->featured = $request->input('featured') ?? 0;
            $propery->exclusive_channel_partner = $request->input('exclusive_channel_partner');
            $propery->sign_exclusive_mandate = $request->input('exclusive_mandate');
            $propery->business_type = $request->input('business_type');
            $propery->land_to_get_verified = $request->input('land_to_get_verified') ?? null;
            $propery->status = $request->input('status') ?? '0';
            // $propery->est_payment = $request->input('est_payment');
            $propery->wifi = $request->input('wifi') ?? '0';
            $propery->wifi = $request->input('parking') ?? '0';
            $propery->unique_id = $request->input('unique_id');
            // $propery->session1_start_time = $request->input('session1_start_time') ?? NULL;
            // $propery->session1_end_time = $request->input('session1_end_time') ?? NULL;
            // $propery->session2_start_time = $request->input('session2_start_time') ?? NULL;
            // $propery->session2_end_time = $request->input('session2_end_time') ?? NULL;
            $saved = $propery->save();

            //admin property for user
            $noti = new Notification;
            $property = PropertyList::with('expert', 'user')->where('id', $propery->id)->first();
            $noti->notification_to = 'user';
            $noti->user_id = $user_id;  //change by shivani
            $noti->property_id = $property->id;
            $noti->message = "Admin Your Property added";
            $noti->save();


            // if ($request->input('video_link')) {
            //     foreach ($request->input('video_link') as $key => $image) {
            //         $image_parts = explode(";base64,", $image);
            //         $image_type_aux = explode("image/", $image_parts[0]);
            //         $image_type = $image_type_aux[1];
            //         $image_base64 = base64_decode($image_parts[1]);
            //         $file = uniqid() . '. ' . $image_type;
            //         // if ($image_type == 'gif'|| $image_type == 'jpg' || $image_type == 'jpeg' || $image_type == 'png') {
            //         Storage::disk('propertyImage')->put($file, $image_base64);
            //         $imagePath = 'storage/propertyimage/' . $file;
            //         $propertyImage = new PropertyListImage;
            //         $propertyImage->video_link = $imagePath;
            //         $propertyImage->property_list_id = $propery->id;
            //         $propertyImage->save();
            //         // }
            //         if ($key == 0) {
            //             $imageUpdate = PropertyList::where('id', $propery->id)->first();
            //             $imageUpdate->video_link = $imagePath;
            //             $imageUpdate->save();
            //         }
            //     }
            // }
            if (isset($request->input('new_images')[0])) {
                foreach ($request->input('new_images') as $key => $image) {
                    $image_parts = explode(";base64,", $image);
                    $image_type_aux = explode("image/", $image_parts[0]);
                    $image_type = $image_type_aux[1];
                    $image_base64 = base64_decode($image_parts[1]);
                    $file = uniqid() . '. ' . $image_type;
                    if ($image_type == 'gif' || $image_type == 'jpg' || $image_type == 'jpeg' || $image_type == 'png') {
                        Storage::disk('propertyImage')->put($file, $image_base64);
                        $imagePath = 'storage/propertyimage/' . $file;
                        $propertyImage = new PropertyListImage;
                        $propertyImage->image = $imagePath;
                        $propertyImage->property_list_id = $propery->id;
                        $propertyImage->save();
                    }
                    if ($key == 0) {
                        $imageUpdate = PropertyList::where('id', $propery->id)->first();
                        $imageUpdate->image = $imagePath;
                        $imageUpdate->save();
                    }
                }
            }


            if ($request->input('other_sazara_images')) {
                $image_parts = explode(";base64,", $request->input('other_sazara_images'));
                $image_type_aux = explode("application/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                $file = uniqid() . '. ' . $image_type;
                Storage::disk('propertyImage')->put($file, $image_base64);
                $imagePath = 'storage/propertyimage/' . $file;
                $imageUpdate = PropertyList::where('id', $propery->id)->first();
                $imageUpdate->feasibility_report = $imagePath;
                $imageUpdate->save();
            }
            if ($request->input('other_report_images')) {
                $image_parts = explode(";base64,", $request->input('other_report_images'));
                $image_type_aux = explode("application/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                $file = uniqid() . '. ' . $image_type;
                // if ($image_type == 'pdf' ) {
                Storage::disk('propertyImage')->put($file, $image_base64);
                $imagePath = 'storage/propertyimage/' . $file;
                $imageUpdate = PropertyList::where('id', $propery->id)->first();
                $imageUpdate->land_report = $imagePath;
                $imageUpdate->save();
            }
        } catch (Exception $ex) {
            $saved = false;
        }
        if ($saved) {
            if (smtpConnect() == true) {

//                 Mail::to(config('mail.from.admin_email'))->send(new AdminNewLandConfirm($property));
            }
            return response()->json(['success' => true, 'msg' => "Land Detail Successfully added."]);
        } else {
            return response()->json(['success' => false, 'msg' => "Something went wrong, Please try again later!"]);
        }
    }

    /**
     * property detail get by id
     * 
     * @param Request $request
     * @return type
     */
    public function propertyEdit(Request $request)
    {
        $property = PropertyList::with('propertyImage', 'propertyOtherImage')->where('id', $request->id)->first();
        //    dd($property);
        $agents = User::role('broker')->get();
        $view = View('admin.land.land-detail-edit', compact('property', 'agents'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function propertyUpdate(Request $request)
    {
        // print_r($request->post());
        // die;

        $rules = [
            'land_name' => 'required',
            'unique_id' => ['required', Rule::unique('property_list')->ignore($request->input('id'))],
            'land_price' => 'required',
            'price_unit' => 'required',
            'size_of_land' => 'required',
            'nearby_place' => 'required',
            'address' => 'required',
            'descritpion' => 'required',
            'user_id' => 'required',
            'user_pan' => 'required',
            'is_corporate' => 'required',
            // 'exclusive_channel_partner' => 'required',
            // 'exclusive_mandate' => 'required',
            'business_type' => 'required',
            // 'est_payment' => 'required',
            // 'session1_end_time' => 'nullable|after:session1_start_time',
            // 'session2_start_time' => 'nullable|after:session1_end_time',
            //            'session2_end_time' => 'nullable|after:session2_end_time',
        ];
        if ($request->input('is_corporate') == 'company') {
            $rules['user_gst'] = 'required';
        }
        if (!isset($request->input('new_images')[0]) && !isset($request->oldImages)) {
            $rules['other_images'] = "required|array|min:1";
        }

        $messages = [
            'land_name.required' => 'The Land Name is required.',
            'land_price.required' => 'The Land Price field is required',
            'size_of_land.required' => 'The land Size field is required.',
            'nearby_place.required' => 'The Near By palace field is required.',
            'address.required' => 'The Address field is required.',
            'user_id.required' => 'The Agent field is required.',
            'user_gst.required' => 'The Gst No field is required.',
            'user_pan.required' => 'The Pan No field is required.',
            'user_pan.required' => 'The Pan No field is required.',
            'unique_id.required' => 'The Unique Id field is required.',
            // 'session1_end_time.after' => 'The session1 end time must be a date after session1 start time.',
            // 'session2_start_time.after' => 'The session2 start time must be a date after session1 end time.',
            //            'session2_end_time.after' => 'The session2 end time must be a date after session2 start time.',
        ];
        $videoUrl = $request->input('video_link');
        $validator = Validator::make($request->all(), $rules, $messages);
        $videoId = '';

        if (!empty($request->input('video_link'))) {
            $link = $videoUrl;
            $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
            if (empty($video_id[1]))
                $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..
            $video_id = explode("&", $video_id[1]); // Deleting any other params
            $videoId = $video_id[0];
        }
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => 'There are incorect values in the form!',
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }
        if (isset($request->oldImages[0])) {
            $imageid = array_column($request->oldImages, 'id');
            PropertyListImage::whereNotIn('id', $imageid)->get()->each->delete();
        }
        try {
            $propery = PropertyList::with('agent')->where('id', $request->input('id'))->first();

            $propery->land_name = $request->input('land_name');
            $propery->max_price = $request->input('land_price');
            $propery->price_unit = $request->input('price_unit');
            $propery->size_of_land = $request->input('size_of_land');
            $propery->nearby_place = $request->input('nearby_place');
            $propery->address = $request->input('address');
            $propery->descritpion = $request->input('descritpion');
            $propery->latitute = $request->input('latitute');
            $propery->longitude = $request->input('longitude');
            $propery->user_id = $request->input('user_id');
            $propery->video_link = $videoId;
            $propery->featured = $request->input('featured') ?? 0;
            $propery->gst = $request->input('user_gst') ?? null;
            $propery->pan = $request->input('user_pan');
            $propery->is_corporate = $request->input('is_corporate');
            $propery->exclusive_channel_partner = $request->input('exclusive_channel_partner');
            $propery->sign_exclusive_mandate = $request->input('exclusive_mandate');
            $propery->business_type = $request->input('business_type');
            $propery->land_to_get_verified = $request->input('land_to_get_verified') ?? null;
            $propery->status = $request->input('status') ?? '0';
            // $propery->est_payment = $request->input('est_payment');
            $propery->wifi = $request->input('wifi') ?? '0';
            $propery->parking = $request->input('parking') ?? '0';
            // $propery->session1_start_time = $request->input('session1_start_time') ?? NULL;
            // $propery->session1_end_time = $request->input('session1_end_time') ?? NULL;
            // $propery->session2_start_time = $request->input('session2_start_time') ?? NULL;
            // $propery->session2_end_time = $request->input('session2_end_time') ?? NULL;
            $propery->unique_id = $request->input('unique_id');
            $saved = $propery->save();
            if (isset($request->input('new_images')[0])) {
                foreach ($request->input('new_images') as $key => $image) {
                    $image_parts = explode(";base64,", $image);
                    $image_type_aux = explode("image/", $image_parts[0]);
                    $image_type = $image_type_aux[1];
                    $image_base64 = base64_decode($image_parts[1]);
                    $file = uniqid() . '. ' . $image_type;
                    Storage::disk('propertyImage')->put($file, $image_base64);
                    $imagePath = 'storage/propertyimage/' . $file;
                    $propertyImage = new PropertyListImage;
                    $propertyImage->image = $imagePath;
                    $propertyImage->property_list_id = $propery->id;
                    $propertyImage->save();
                }
            }

            if ($request->input('other_report_images')) {
                // foreach ($request->input('other_report_images') as $key => $image) {
                $image_parts = explode(";base64,", $image);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                $file = uniqid() . '. ' . $image_type;
                // if ($image_type == 'pdf' ) {
                Storage::disk('propertyImage')->put($file, $image_base64);
                $imagePath = 'storage/propertyimage/' . $file;
                $imageUpdate = PropertyList::find($request->input('id'));
                $imageUpdate->land_report = $imagePath;
                $imageUpdate->save();
                // }

            }

            if (isset($request->oldOtherImages[0])) {
                $imageid = array_column($request->oldOtherImages, 'id');
                LandOtherDocument::whereNotIn('id', $imageid)->where('property_list_id', $propery->id)->get()->each->delete();
            }
            if ($request->input('other_sazara_images')) {
                $image_parts = explode(";base64,", $request->input('other_sazara_images'));
                $image_type_aux = explode("application/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                $file = uniqid() . '. ' . $image_type;
                Storage::disk('propertyImage')->put($file, $image_base64);
                $imagePath = 'storage/propertyimage/' . $file;
                $imageUpdate = PropertyList::where('id', $propery->id)->first();
                $imageUpdate->feasibility_report = $imagePath;
                $imageUpdate->save();
            }
            if ($request->input('other_report_images')) {
                $image_parts = explode(";base64,", $request->input('other_report_images'));
                $image_type_aux = explode("application/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                $file = uniqid() . '. ' . $image_type;
                // if ($image_type == 'pdf' ) {
                Storage::disk('propertyImage')->put($file, $image_base64);
                $imagePath = 'storage/propertyimage/' . $file;
                $imageUpdate = PropertyList::where('id', $propery->id)->first();
                $imageUpdate->land_report = $imagePath;
                $imageUpdate->save();
            }
        } catch (Exception $ex) {
            $saved = false;
        }
        if ($saved) {
            if (smtpConnect() == true) {
                if (!empty($propery->agent->email)) {
                    Mail::to($propery->agent->email)->send(new LandAssignToAgent($propery));
                }
                Mail::to(config('mail.from.admin_email'))->send(new LandAssignConfirm($propery));
            }
            return response()->json(['success' => true, 'msg' => "Land Detail Successfully updated."]);
        } else {
            return response()->json(['success' => false, 'msg' => "Something went wrong, Please try again later!"]);
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function propertyDelete($id)
    {
        try {
            $property = PropertyList::where('id', $id)->first();
            $property->delete();
        } catch (Exception $ex) {
            return back()->with('success', 'Something went wrong, Please try again later!');
        }
        return back()->with('success', 'Land successfully deleted!');
    }

    /**
     * all expire land listing
     * 
     * @param Request $request
     * @return type
     */
    public function expireLandListing(Request $request)
    {
        $agents = User::role(['expert', 'broker'])->get();
        $land = $this->getCount();
        $name = $request->name ?? '';
        $address = $request->address ?? '';
        $businessType = $request->business_type ?? '';
        $startDate = !empty($request->created_at) ? Carbon\Carbon::parse($request->created_at)->format('Y-m-d') : '';
        $expireLands = PropertyList::propertyList($request, '3',);

        return view('admin.land.expire-land-listing', compact('expireLands', 'name', 'address', 'startDate', 'land', 'agents', 'businessType'));
    }

    /**
     * expire property resubmit
     * 
     * @param Request $request
     * @return type
     */
    public function propertyResbmit(Request $request)
    {

        $propertyList = PropertyList::with('propertyImage', 'propertyOtherImage')->where('id', $request->id)->first();
        if (!empty($propertyList)) {
            try {
                $property = new PropertyList;
                $property->user_id = $propertyList->user_id;
                $property->land_name = $propertyList->land_name;
                $property->price_unit = $propertyList->price_unit;
                $property->size_of_land = $propertyList->size_of_land;
                $property->descritpion = $propertyList->descritpion;
                $property->nearby_place = $propertyList->nearby_place;
                $property->address = $propertyList->address;
                $property->video_link = $propertyList->video_link;
                $property->image = $propertyList->image;
                $property->is_corporate = $propertyList->is_corporate;
                $property->gst = $propertyList->gst;
                $property->pan = $propertyList->pan;
                $property->business_type = $propertyList->business_type;
                $property->min_price = $propertyList->min_price;
                $property->max_price = $propertyList->max_price;
                $property->latitute = $propertyList->latitute;
                $property->longitude = $propertyList->longitude;
                $property->featured = $propertyList->featured;
                $property->exclusive_channel_partner = $propertyList->exclusive_channel_partner;
                $property->sign_exclusive_mandate = $propertyList->sign_exclusive_mandate;
                $property->land_to_get_verified = $propertyList->land_to_get_verified;
                $property->est_payment = $propertyList->est_payment;
                $property->wifi = $propertyList->wifi;
                $property->wifi = $propertyList->wifi;
                $property->parking = $propertyList->parking;
                $property->session1_start_time = $propertyList->session1_start_time;
                $property->session1_end_time = $propertyList->session1_end_time;
                $property->session2_start_time = $propertyList->session2_start_time;
                $property->session2_end_time = $propertyList->session2_end_time;
                $property->status = 1;
                $saved = $property->save();
                if ($saved) {
                    if (count($propertyList->propertyImage)) {
                        foreach ($propertyList->propertyImage as $propertyImages) {
                            $images = new PropertyListImage;
                            $images->image = $propertyImages->image;
                            $images->property_list_id = $property->id;
                            $images->save();
                        }
                    }
                    if (count($propertyList->propertyOtherImage)) {
                        foreach ($propertyList->propertyOtherImage as $otherImages) {
                            $images = new LandOtherDocument;
                            $images->image = $otherImages->image;
                            $images->property_list_id = $property->id;
                            $images->save();
                        }
                    }
                }
            } catch (Exception $e) {
                $saved = false;
            }
        }
        if ($saved) {
            return response()->json(['success' => true, 'msg' => "Land Resubmit successfully!"]);
        } else {
            return response()->json(['success' => false, 'msg' => "Something went wrong, Please try again later!"]);
        }
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function propertyUniqueId(Request $request)
    {

        $unique_ids = PropertyList::whereRaw('unique_id LIKE "%' . strip_tags($request->input('unique_id')) . '%"')->get()->pluck('unique_id');
        return response($unique_ids);
    }
}
