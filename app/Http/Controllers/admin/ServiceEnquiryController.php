<?php

namespace App\Http\Controllers\admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\ServiceEnquiry;
use App\Models\User;
use App\Models\Notification;
use App\Mail\EnquiryAssign;
use App\Models\ValueAddedService;
use Mail;
use DB;
use Auth;
use Exception;

class ServiceEnquiryController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $customerName = $request->input('customerName');
        $customerEmail = $request->input('customerEmail');
        $customerMobile = $request->input('customerMobile');
        $serviceType = $request->input('serviceType');
        $startDate = !empty($request->created_at) ? Carbon::parse($request->created_at)->format('Y-m-d') : '';
        $experts = User::role('expert')->get();
        $services = ValueAddedService::all();

        $enquiries = ServiceEnquiry::selectRaw('service_enquiries.id,service_enquiries.cancel,service_enquiries.expert_id,service_enquiries.status,service_enquiries.user_id, service_enquiries.value_added_service_id, service_enquiries.name,service_enquiries.comments, service_enquiries.created_at,service_enquiries.email,service_enquiries.mobile,us.name as username,ex.name as expertname,service.service_name')
                ->leftJoin('users as us', 'service_enquiries.user_id', '=', 'us.id')
                ->leftJoin('users as ex', 'service_enquiries.expert_id', '=', 'ex.id')
                ->leftJoin('value_added_services as service', 'service_enquiries.value_added_service_id', '=', 'service.id');
        if (!empty($serviceType)) {
            $enquiries->where('service_enquiries.value_added_service_id', $serviceType);
        }
        if (!empty($startDate)) {
            $enquiries->whereDate('service_enquiries.created_at', '=', $startDate);
        }
        if (!empty($customerName)) {
            $enquiries->where('service_enquiries.name', 'Like', '%' . $customerName . '%');
        }
        if (!empty($customerEmail)) {
            $enquiries->where('service_enquiries.email', 'Like', '%' . $customerEmail . '%');
        }
        if (!empty($customerMobile)) {
            $enquiries->where('service_enquiries.mobile', 'Like', '%' . $customerMobile . '%');
        }
        $enquiries = $enquiries->orderBy('service_enquiries.id', 'desc')->paginate(10);
        return view('admin.service-enquiry', compact('enquiries', 'services', 'serviceType', 'startDate','customerMobile','customerEmail', 'customerName'));
    }

    public function enquiryAssignExpert(Request $request) {
        $rules = [
            'expert_id' => 'required',
            'file' => 'required',
            'summary' => 'required',
        ];
        $message = [
            'expert_id.required' => 'Expert field is required',
            'file.required' => 'File field is required',
            'summary.required' => 'Dexcription field is required',
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        $path = '';
        if (!empty($request->file('file'))) {
            $statement = DB::select("SHOW TABLE STATUS LIKE 'service_enquiries'");
            $nextId = (isset($statement[0]->Auto_increment)) ? $statement[0]->Auto_increment : uniqid();
            $file_name = time() . '_' . $nextId;
            $fileName = $file_name . '.' . $request->file('file')->getClientOriginalExtension();
            $path = 'enquiry' . '/' . $request->file('file')->storeAs($request->input('user_id'), $fileName, 'enquiry');
        }
        try {
            $enquiry = ServiceEnquiry::where('id', $request->input('enquiryId'))->first();
            $expert_id = $request->input('expert_id');
            $enquiry->expert_id = $expert_id;
            $enquiry->description = $request->input('summary');
            $enquiry->file = $path;
            $enquiry->cancel = null;
            $saved = $enquiry->save();
            // after assing apppointment to expert notification detail store 
            $enquiry = ServiceEnquiry::with('expert')->where('id', $enquiry->id)->first();
            $noti = new Notification;
            $noti->notification_to = 'expert';
            $noti->user_id = $expert_id;    //change by shivani
            $noti->service_enquiry_id = $enquiry->id;
            $noti->message = "You recive a new enquiry";
            $noti->save();
//            die($saved);
//            if ($saved && smtpConnect() == true) {
//                Mail::to($enquiry->expert->email)->send(new EnquiryAssign($enquiry));
//            }
        } catch (Exception $e) {
            $saved = false;
        }
        return response($saved);
    }

}
