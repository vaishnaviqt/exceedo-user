<?php

namespace App\Http\Controllers\admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ValueAddedService;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\State;
use App\Models\City;

class ExpertController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $paginate = $request->paginate ?? 10;
        $service_id = $request->service_id ?? '';
        $expert_stateid = $request->stateid ?? '';
        $date_of_joining = $request->doj ?? '';
        $expert_name = $request->expert_name ?? '';
        $expert_email = $request->email ?? '';
        $expert_mobile = $request->mobile ?? '';
        $states = State::where('country_id', 101)->get();
        $cities = City::all();
        $allServices = ValueAddedService::all();
        $penidngexperts = User::where('is_pending', '=', 0)->role('expert')->get();
        $allExperts = User::where('is_pending', 1)->role('expert')->get();
        $query = User::selectRaw('users.id, users.name, users.user_unique_id, users.mobile, users.user_status, users.email, st.name as stateName')->with('services')->where('is_pending', '=', 1)
                ->leftJoin('user_services as us', 'users.id', '=', 'us.user_id')
                ->leftJoin('states as st', 'st.id', '=', 'users.state_id');
        if (!empty($expert_name)) {
            $query->where('users.name', 'like', '%' . $request->expert_name . '%');
        }
             if (!empty($expert_stateid)) {
            $query->where('users.state_id', $expert_stateid);
        }
        if (!empty($expert_email)) {
            $query->where('users.email', 'like', '%' . $request->expert_email . '%');
        }
        if (!empty($expert_mobile)) {
            $query->where('users.mobile', $request->expert_mobile);
        }
        if (!empty($date_of_joining)) {
            $query->whereDate('users.created_at', Carbon::parse($request->doj)->format('Y-m-d'));
        }
        if (!empty($service_id)) {
            $query->where('us.value_added_service_id', $service_id);
        }
        $query->groupBy('users.id');
        $experts = $query->role('expert')->orderBy('users.created_at', 'desc')->paginate($paginate);
        return view('admin/experts/experts')->with([
                    'paginate' => $paginate,
                    'penidngexperts' => $penidngexperts,
                    'experts' => $experts,
                    'allExperts' => $allExperts,
                    'expert_name' => $expert_name,
                    'date_of_joining' => $date_of_joining,
                    'service_id' => $service_id,
                    'allServices' => $allServices,
                    'states' => $states,
                    'cities' => $cities, 
            'expert_email' => $expert_email,
            'expert_mobile' => $expert_mobile,
                'expert_stateid' => $expert_stateid,
        ]);
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function showPendingExperts(Request $request) {
        $paginate = $request->paginate ?? 10;
        $all_valueServices = ValueAddedService::get();
        $service_id = $request->service_id ?? '';
        $date_of_joining = $request->doj ?? '';
        $expert_name = $request->expert_name ?? '';
        $expert_email = $request->email ?? '';
        $expert_mobile = $request->mobile ?? '';
        $query = User::selectRaw('users.id, users.name, users.user_unique_id, users.mobile, users.email, st.name as stateName')->with('services')->where('is_pending', '=', 0)
        ->leftJoin('user_services as us', 'users.id', '=', 'us.user_id')
        ->leftJoin('states as st', 'st.id', '=', 'users.state_id');

        if (!empty($expert_name)) {
            $query->where('users.name', 'like', '%' . $expert_name . '%');
        }
        if (!empty($expert_email)) {
            $query->where('users.email', 'like', '%' . $request->expert_email . '%');
        }
        if (!empty($expert_mobile)) {
            $query->where('users.mobile', $request->expert_mobile);
        }
        if (!empty($date_of_joining)) {
            $query->whereDate('users.created_at', Carbon::parse($date_of_joining)->format('Y-m-d'));
        }
        if (!empty($service_id)) {
            $query->whereDate('us.value_added_service_id', $service_id);
        }
        $query->groupBy('users.id');
        $experts = $query->role('expert')->orderBy('users.created_at', 'desc')->paginate($paginate);
        $penidngexperts = User::where('is_pending', '=', 0)->role('expert')->get();
        $allExperts = User::where('is_pending', 1)->role('expert')->get();

        return view('admin/experts/pendingexperts')->with([
                    'paginate' => $paginate,
                    'experts' => $experts,
                    'all_valueServices' => $all_valueServices,
                    'allExperts' => $allExperts,
                    'expert_name' => $expert_name,
                    'date_of_joining' => $date_of_joining,
                    'service_id' => $service_id,
            'penidngexperts' => $penidngexperts,
            'expert_email' => $expert_email,
            'expert_mobile' => $expert_mobile
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        try {
            $expert = User::role(['expert', 'broker'])->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['success' => false, 'msg' => "Fetching Expert Record Failed"]);
        }

        return $expert;
    }

    public function userstatus(Request $request)
    {
        $id = $request->id;
        $user_status = $request->user_status;


        $User_status = User::find($id);
        $User_status->user_status = $user_status;

        $User_status->save();
        return response()->Json(['success' => true, 'msg' => 'done']);
    }

    public function update(Request $request)
    {
        Validator::make($request->all(), [
            'expert_name' => 'required|max:255',
            'email' => 'required|email',
        ])->validate();

        try {
            $user = User::findOrFail($request->edit_client_id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->back()->with('error', "Client Not Found.");
        }

        return response()->json(['success' => true, 'msg' => "Expert Successfully Udpated"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyPendingExperts(Request $request) {
        if ($request->isMethod('post')) {

            try {
                $user = User::findOrFail($request->deletion_id);
            } catch (ModelNotFoundException $exception) {
                return redirect()->back()->with('error', "Expert Not Found.");
            }

            if ($user->delete()) {
                $user->services()->detach();
                if ($user->hasRole('expert')) {
                    $user->removeRole('expert');
                } else {
                    $user->removeRole('broker');
                }
                return redirect()->back()->with('success', 'Expert Deleted Successfully!');
            } else {
                return redirect()->back()->with('error', 'Expert Deletion Failed!');
            }
        } else {
            return redirect()->back()->with('error', 'This is not allowed');
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function approveExpert($id) {
        if (!empty($id)) {
            try {
                $expert = User::where('id', $id)->update(['is_pending' => 1]);
                return response()->json(['success' => true, 'msg' => "Expert approved successfully."]);
            } catch (\Exception $e) {
                return response()->json(['success' => false, 'msg' => "Error in approving expert."]);
            }
        } else {
            return response()->json(['success' => false, 'msg' => "Request parameter not found."]);
        }
    }

    /**
     * city get according state id
     * 
     * @param Request $request
     * @return type
     */
    public function getCityList(Request $request) {
        $cities = City::where('state_id', $request->input('stateId'))->get();
        return response()->json(['cities' => $cities]);
    }
}
