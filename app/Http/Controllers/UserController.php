<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\ValueAddedService;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\ExpertUpdateByAdmin;
use App\Mail\ExpertUpdateConfirmation;
use App\Mail\ExpertMessage;
use App\Mail\ExpertRegistration;
use App\Models\SocialLogin;
use Socialite;

class UserController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLogin() {

        $values = ValueAddedService::get();
        return view('public.login')->with(['values' => $values]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeExpert(Request $request) {

        $rules = [
            'full_name' => 'required|max:255',
            'email' => 'required|email|unique:users|email:rfc,dns',
            'mobile_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10|unique:users,mobile',
            'exp_services' => 'required|array|min:1',
            'exp_services.*' => 'required|numeric',
            'state_id' => 'required',
            'city_id' => 'required',
        ];

        $messages = [
            'full_name.required' => 'The full name field is required.',
            'email.required' => 'The email field is required.',
            'email.email' => 'The email must be a valid email address.',
            'mobile_number.required' => 'The mobile number field is required.',
            'mobile_number.regex' => 'The mobile number format is invalid.',
            'mobile_number.min' => 'The mobile number must be at least 10 characters.',
            'mobile_number.max' => 'The mobile number may not be greater than 10 characters.',
            'exp_services.required' => 'The service field is required.',
            'exp_services.*.required' => 'The service field is required.',
            'state_id.required' => 'The State field is required.',
            'city_id.required' => 'The City field is required.',
        ];
        //if id not empty then override the above rules
        if ($request->id) {
            $replace_rules = [
                'full_name' => 'required|regex:/^[a-z\s]+$/i|max:255',
                'email' => 'required|email|email:rfc,dns|' . Rule::unique('users')->ignore($request->id),
                'mobile_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10|' . Rule::unique('users', 'mobile')->ignore($request->id),
            ];
            $rules = array_replace($rules, $rules, $replace_rules);
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        if ($request->id) {
            $user = User::find($request->id);
            $user->name = trim($request->full_name);
            $user->email = trim($request->email);
            $user->password = Hash::make(trim($request->mobile_number));
            $user->mobile = trim($request->mobile_number);
            $user->city_id = $request->exp_services;
            $user->state_id = $request->state_id;
            $user->city_id = $request->city_id;

            if ($user->update()) {
                if (smtpConnect() == true) {
                    $message = User::where('id', $user->id)->first();

                    Mail::to($user->email)->send(new ExpertUpdateByAdmin($message));
                    Mail::to(config('mail.from.admin_email'))->send(new ExpertUpdateConfirmation($message));
                }
                return response()->json(['success' => true, 'msg' => "Broker Successfully updated."]);
            } else {
                return response()->json(['success' => false, 'msg' => "Broker Successfully not updated."]);
            }
            die();
        } else {
            $user = new user();
            $user->name = trim($request->full_name);
            $user->email = trim($request->email);
            $user->password = trim($request->mobile_number);
            $user->mobile = trim($request->mobile_number);
            $user->city_id = $request->exp_services;
            $user->state_id = trim($request->state_id);
            $user->city_id = trim($request->city_id);
            $user->is_pending = 1;

            if ($user->save()) {
                $user->assignRole('expert');
                $user->services()->attach($request->exp_services);
                if (smtpConnect() == true) {
                    $message = User::where('id', $user->id)->first();

                    Mail::to($user->email)->send(new ExpertMessage($message));
                    Mail::to(config('mail.from.admin_email'))->send(new ExpertRegistration($message));
                }
                return response()->json(['success' => true, 'msg' => "Expert Successfully added."]);
            } else {
                return response()->json(['success' => false, 'msg' => "Expert Successfully not added."]);
            }

            die();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeBroker(Request $request) {
        //
        $rules = [
            'broker_full_name' => 'required|regex:/^[a-z\s]+$/i|max:255',
            'broker_email' => 'required|email|unique:users,email',
            'broker_mobile_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10|unique:users,mobile',
            'broker_services' => 'required|array|min:1',
            'broker_services.*' => 'required|numeric',
        ];

        $messages = [
            'broker_full_name.required' => 'The Full name field is required.',
            'broker_email.required' => 'The Email field is required.',
            'broker_email.email' => 'The Email must be a valid email address.',
            'broker_mobile_number.required' => 'The Mobile number field is required.',
            'broker_mobile_number.regex' => 'The Mobile number format is invalid.',
            'broker_mobile_number.min' => 'The Mobile number must be at least 10 characters.',
            'broker_mobile_number.max' => 'The Mobile number may not be greater than 10 characters.',
            'broker_services.*.required' => 'The Service field is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            //return  Redirect::route('users.create')->withInput()->withErrors($v->errors());
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }

        $user = new user();
        $user->name = trim($request->broker_full_name);
        $user->email = trim($request->broker_email);
        $user->password = trim($request->broker_mobile_number);
        $user->mobile = trim($request->broker_mobile_number);
        $user->is_pending = 0;

        if ($user->save()) {
            $user->assignRole('broker');
            $user->services()->attach($request->broker_services);
            return response()->json(['success' => true, 'msg' => "Broker Successfully added."]);
        } else {
            return response()->json(['success' => false, 'msg' => "Broker Successfully not added."]);
        }

        die();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    /**
     * send otp for login
     *
     * @return \Illuminate\Http\Response
     */
    public function doSendOtp(Request $request) {


        // validate request
        Validator::make($request->all(), [
            'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10',
            'role' => 'required',
        ])->validate();
        // get mobile number
        //Validator::make($request->all(), $rules, $messages);
        $mobile = trim($request->post('mobile'));
        $role = trim($request->post('role'));
        // find user via mobile
        try {
            $users = User::where('mobile', $mobile)->with('roles')->get();

            if ($users->isEmpty()) {
                return response()->json(['errors' => array('mobile' => "Mobile number dose not found"), 'msg' => "Mobile number dose not found"], 404);
            } else {


                if ($users[0]->roles[0]->name != $role) {
                    return response()->json(['errors' => array('mobile' => "Mobile number dose not found"), 'msg' => "Mobile number dose not found"], 404);
                }
                // validate otp cont
                if ($users[0]->otp_count >= 3) {
                    return response()->json(['errors' => array('mobile' => "OTP limit is Exceeded!"), 'msg' => "OTP limit is Exceeded!"], 404);
                }
                $otp_count = $users[0]->otp_count + 1;
                // send otp comes here
                // end of send otp code
                // store OTP information
                $six_digit_random_number = mt_rand(100000, 999999);
                $users->toQuery()->update([
                    'otp' => $six_digit_random_number,
                    'otp_count' => $otp_count,
                    'last_otp_sent' => date("Y-m-d:H:i:s")
                ]);
                // end of code for store OTP here
                return response()->json(['success' => true, 'message' => "OTP sent to your mobile number", 'otp' => $six_digit_random_number], 201);
            }
        } catch (ModelNotFoundException $exception) {
            return response()->json(['errors' => array('mobile' => "Mobile number dose not found"), 'msg' => "Mobile number dose not found"], 404);
        }
        //  login user
    }

    public function doLogin(Request $request) {

        $rules = [
            'role' => 'required|regex:/^[a-z\s]+$/i|max:255',
            'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10',
            'otp' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:6|max:6',
        ];

        $messages = [
            'role.required' => 'role field is required.',
            'mobile.required' => 'The mobile number field is required.',
            'mobile.regex' => 'The mobile number format is invalid.',
            'mobile.min' => 'The mobile number must be at least 10 characters.',
            'mobile.max' => 'The mobile number may not be greater than 10 characters.',
            'otp.required' => 'The otp field is required.',
            'otp.regex' => 'The otp format is invalid.',
            'otp.min' => 'The otp must be at least 6 characters.',
            'otp.max' => 'The otp may not be greater than 6 characters.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            //return  Redirect::route('users.create')->withInput()->withErrors($v->errors());
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }

        $mobile = trim($request->post('mobile'));
        $otp = trim($request->post('otp'));
        $role = trim($request->post('role'));

        $users = User::where([['mobile', '=', $mobile], ['otp', '=', $otp]])->with('roles')->get();
        if ($users->isEmpty()) {
            return response()->json(['success' => false, 'msg' => "The provided credentials do not match our records."]);
        }

        if ($users[0]->roles[0]->name != $role) {
            return response()->json(['success' => false, 'msg' => "The provided credentials do not match our records."]);
        }

        if (Auth::loginUsingId($users[0]->id, $remember = false)) {
            // echo  $users[0]->id ;die;
            $request->session()->regenerate();
            // remove otp, last time, and count
            $users->toQuery()->update([
                'otp' => null,
                'otp_count' => null,
                'last_otp_sent' => null
            ]);

            if ($role == "broker") {
                $redirect = route('broker.projects');
            } else if ($role == "expert") {
                $redirect = route('expert.appointments');
            } else if ($role == "user") {
                $redirect = route('user.dashboard');
            }
            return response()->json(['success' => true, 'msg' => "Login successfully.", "url" => $redirect]);
        }

        return response()->json(['success' => false, 'msg' => "The provided credentials do not match our records."]);
        die();
    }

    public function doLogin_old(Request $request) {
        $credentials = $request->only('mobile', 'otp');
        $mobile = trim($request->post('mobile'));
        $otp = trim($request->post('otp'));
        $users = User::where([['mobile', '=', $mobile], ['otp', '=', $otp]])->get();
        if ($users->isEmpty()) {
            return back()->withErrors([
                        'email' => 'The provided credentials do not match our records.',
            ]);
        }

        if (Auth::loginUsingId($users[0]->id, $remember = false)) {
            // echo  $users[0]->id ;die;
            $request->session()->regenerate();
            // remove otp, last time, and count
            $users->toQuery()->update([
                'otp' => null,
                'otp_count' => null,
                'last_otp_sent' => null
            ]);
            return redirect()->route('broker.appointments');
        }

        return back()->withErrors([
                    'email' => 'The provided credentials do not match our records.',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function doBrokerLogout(Request $request) {

        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('login');
    }

    public function doExpertLogout(Request $request) {

        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('login');
    }

    public function doUserLogout(Request $request) {

        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('login');
    }

    /**
     * Display the sign up page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showSignUp() {

        $values = ValueAddedService::get();
        return view('signup')->with(['values' => $values]);
    }

    /**
     * user social login
     * 
     * @param type $provider
     * @return type
     */
    public function userLogin($provider) {
        session(['role' => 'user']);
        return Socialite::driver(($provider))->redirect();
    }

    /**
     * broker social login
     * 
     * @param type $provider
     * @return type
     */
    public function brokerLogin($provider) {
        session(['role' => 'broker']);
        return Socialite::driver(($provider))->redirect();
    }

    /**
     * expert social login
     * @param type $provider
     * @return type
     */
    public function expertLogin($provider) {
        session(['role' => 'expert']);
        return Socialite::driver(($provider))->redirect();
    }

    public function handleProviderCallback($provider) {
        $user = Socialite::driver($provider)->user();

        $role = session('role');
        $socialUser = SocialLogin::where('provider_id', $user->id)->first();
        $userCheck = User::where('email', $user->getEmail())->first();
        if (!empty($socialUser) || !empty($userCheck)) {

            if (!empty($userCheck)) {
                Auth::loginUsingId($userCheck->id);
                if ($userCheck->hasRole('broker')) {
                    return redirect()->route('broker.appointments');
                } elseif ($userCheck->hasRole('user')) {
                    return redirect()->route('user.dashboard');
                } elseif ($userCheck->hasRole('expert')) {
                    return redirect()->route('expert.appointments');
                }
            }
        }
        $newuser = new User;
        $newuser->name = $user->getName();
        $newuser->email = $user->getEmail();
        $newuser->photo = $user->getAvatar();
        $newuser->password = trim($user->getName());
        $saved = $newuser->save();
        $newuser->assignRole($role);
        if ($saved) {
            $socialuser = new SocialLogin;
            $socialuser->user_id = $newuser->id;
            $socialuser->provider_name = $provider;
            $socialuser->provider_id = $user->id;
            $saved = $socialuser->save();
            Auth::loginUsingId($newuser->id);
            if ($role == 'broker') {
                return redirect()->route('broker.appointments');
            } elseif ($role == 'user') {
                return redirect()->route('user.dashboard');
            } elseif ($role == 'expert') {
                return redirect()->route('expert.appointments');
            } else {
                return redirect()->back();
            }
        }
    }

    public function SendOtp(Request $request) {


        // validate request
        Validator::make($request->all(), [
            'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10',
            'role' => 'required',
        ])->validate();
        // get mobile number
        //Validator::make($request->all(), $rules, $messages);
        $mobile = trim($request->post('mobile'));
        $role = trim($request->post('role'));
        // find user via mobile
        try {

            if (empty($mobile)) {
                return response()->json(['errors' => array('mobile' => "Mobile number dose not found"), 'msg' => "Mobile number dose not found"], 404);
            } else {

                $six_digit_random_number = mt_rand(100000, 999999);
                // end of code for store OTP here
                return response()->json(['success' => true, 'message' => "OTP sent to your mobile number", 'otp' => $six_digit_random_number], 201);
            }
        } catch (ModelNotFoundException $exception) {
            return response()->json(['errors' => array('mobile' => "Mobile number dose not found"), 'msg' => "Mobile number dose not found"], 404);
        }
        //  login user
    }

    public function userSignup(Request $request) {
        $rules = [
            'full_name' => 'required|regex:/^[a-z\s]+$/i|max:255',
            'email' => 'required|email|unique:users|email:rfc,dns',
            'mobile_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10|unique:users,mobile',
            'otp' => 'required|numeric',
        ];

        $messages = [
            'full_name.required' => 'The full name field is required.',
            'email.required' => 'The email field is required.',
            'email.email' => 'The email must be a valid email address.',
            'mobile_number.required' => 'The mobile number field is required.',
            'mobile_number.regex' => 'The mobile number format is invalid.',
            'mobile_number.min' => 'The mobile number must be at least 10 characters.',
            'mobile_number.max' => 'The mobile number may not be greater than 10 characters.',
            'otp.required' => 'The otp field is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }

        $user = new user();
        $user->name = trim($request->full_name);
        $user->email = trim($request->email);
        $user->password = trim($request->mobile_number);
        $user->mobile = trim($request->mobile_number);
        $user->otp = trim($request->otp);

        $user->is_pending = 1;

        if ($user->save()) {
            $user->assignRole('user');
            return response()->json(['success' => true, 'msg' => "USer Successfully added."]);
        } else {
            return response()->json(['success' => false, 'msg' => "User Successfully not added."]);
        }

        die();
    }

}
