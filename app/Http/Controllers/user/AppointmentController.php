<?php

namespace App\Http\Controllers\user;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\Appointment;
use App\Mail\ExpertRescheduleAppointment;
use App\Models\AppointmentHistory;
use App\Mail\AdminAppointmentBooked;
use App\Mail\ExpertAppointmentConfirm;
use App\Mail\AdminAppointmentRequest;
use App\Mail\UserAppointmentRequest;
use App\Models\ValueAddedService;
use App\Mail\UserAppointmentCancel;
use App\Mail\UserAdminAppointmentCancel;
use App\Mail\UserExpertAppointmentCancel;
use App\Mail\UserAdminAppointmentReschedule;
use App\Mail\UserAppointmentReschedule;
use App\Mail\UserExpertAppointmentReschedule;
use App\Mail\UserAppointmentConfirm;
use App\Mail\UserExpertAppointmentConfirm;
use App\Mail\UserAdminAppointmentConfirm;
use App\Models\Payment;
use App\Models\Review;
use Razorpay\Api\Api;
use Illuminate\Support\Facades\Mail;
use Exception;
use Craftsys\Msg91\Facade\Msg91;
use App\Models\Notification;
use Auth;

class AppointmentController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $totalAppointments = Appointment::where('user_id', auth()->user()->id)->whereNotNull('status')->count();
        $upcommingAppointments = Appointment::where('user_id', auth()->user()->id)->where('status', 1)->where('date', '>', Carbon::now())->count();
        $doneAppointments = Appointment::where('user_id', auth()->user()->id)->where('status', 1)->where('date', '<', Carbon::now())->count();
        $services = ValueAddedService::all();
        return view('user.appointments.appointments', compact('totalAppointments', 'upcommingAppointments', 'doneAppointments', 'services'));
    }

    public function addAppointmentValidate(Request $request) {
        $rules = [
            'value_added_service' => 'required|not_in:0',
            'date' => 'required|after:' . date('Y-m-d'),
            'time' => 'required',
            'remarks' => 'required',
            'terms' => 'required',
            'payment_mode' => 'required',
        ];

        $messages = [
            'value_added_service.required' => 'The Value Added Service Field is required.',
            'date.required' => 'The Date field is required',
            'time.required' => 'The Time field is required.',
            'remarks.required' => 'The Remarks palace field is required.',
            'terms.required' => 'The Terms field is required.',
            'payment_mode' => 'The Payment Mode Field is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        $api = new Api(config('app.razorpay_api_key'), config('app.seceret_key'));
        $order = $api->order->create(array('receipt' => 'order_rcptid_11', 'amount' => $request->input('price') * 100, 'currency' => 'INR')); // Creates order
        return response()->json(['order_id' => $order['id']]);
    }

    /**
     * Appointment booked
     * 
     * @param Request $request
     * @return type
     */
    public function addAppointment(Request $request) {
        $api = new Api(config('app.razorpay_api_key'), config('app.seceret_key'));
        //Fetch payment information by razorpay_payment_id
        $payment = $api->payment->fetch($request->input('razorpay_payment_id'));
        if (!empty($payment) && $payment['status'] == 'captured') {
            $paymentId = $payment['id'];
            $amount = $payment['amount'];
            $currency = $payment['currency'];
            $status = $payment['status'];
            $entity = $payment['entity'];
            $orderId = $payment['order_id'];
            $invoiceId = $payment['invoice_id'];
            $method = $payment['method'];
            $bank = $payment['bank'];
            $wallet = $payment['wallet'];
            $bankTranstionId = isset($payment['acquirer_data']['bank_transaction_id']) ? $payment['acquirer_data']['bank_transaction_id'] : '';
        } else {
            return redirect()->back()->with('error', 'Something went wrong, Please try again later!');
        }
        try {
            $appointment = new Appointment;
            $appointment->date = Carbon::parse($request->input('date'))->format('Y-m-d');
            $user_id = $request->user()->id;
            $appointment->user_id = $user_id;
            $appointment->time = $request->input('time');
            $appointment->remarks = $request->input('remarks');
            $appointment->payment_mode = $request->input('payment_mode');
            $appointment->terms = $request->input('terms');
            $appointment->value_added_service_id = $request->input('value_added_service');
            $appointment->meeting_at = Carbon::parse($request->input('date'))->format('Y-m-d') . ' ' . $request->input('time');
            $appointment->price = $amount / 100;
            $appointment->payment_id = $paymentId;
            $appointment->status = 0;
            $date = Carbon::parse($request->input('date') . $request->input('time'))->format('d M, Y h:i A');
            $saved = $appointment->save();

            // Payment detail save in database
            $payment = new Payment;
            $payment->transaction_id = $paymentId;
            $payment->appointment_id = $appointment->id;
            $payment->amount = $amount / 100;
            $payment->currency = $currency;
            $payment->entity = $entity;
            $payment->status = $status;
            $payment->order_id = $orderId;
            $payment->method = $method;
            $payment->bank = $bank;
            $payment->wallet = $wallet;
            $payment->bank_transaction_id = $bankTranstionId;
            $payment->save();

            //Notification detail store in database
            $appoint = Appointment::with('user')->where('id', $appointment->id)->first();
            $noti = new Notification;
            $noti->notification_to = 'admin';
            $noti->user_id = 1;
            $noti->apppointment_id = $appointment->id;
            $noti->message = "You recive a new appointment with " . $appoint->user->name . " on " . $date;
            $noti->save();
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            $appoint = Appointment::with('user')->where('id', $appointment->id)->first();
//            if (!empty($appoint->user->mobile)) {
//                $price = $amount / 100;
//                $website = config('mail.from.admin_website');
//                $mobile = config('mail.from.admin_mobile');
//                $msg = Msg91::sms()->recipients([
//                            ['mobiles' => $appoint->user->mobile, 'name' => $appoint->user->name, 'dateTime' => $date],
//                        ])
//                        ->flow('60adf7ce87f0d70cc2692811')
//                        ->send();
//                $msg = Msg91::sms()->recipients([
//                            ['mobiles' => $mobile, 'name' => 'Admin', 'amount' => $price, 'website' => url('/')],
//                        ])
//                        ->flow('60adf8472f5322158808f048')
//                        ->send();
//            }
            if (smtpConnect() == true) {
                Mail::to($appoint->user->email)->send(new UserAppointmentRequest($appoint));
                Mail::to(config('mail.from.admin_email'))->send(new AdminAppointmentRequest($appoint));
            }
            return redirect()->back()->with('success', __('Appointment Booked successfully!'));
        } else {
            return back()->withInput()->with('error', __('Something went wrong, Please try again later!'));
        }
    }

    public function appointmentDetails() {
        $appointments = Appointment::with('service')->where('user_id', auth()->user()->id)->get();
        $reports = array();
        $key = 0;
        foreach ($appointments as $appointment) {
            $reports[$key]['title'] = isset($appointment->service->service_name) ? $appointment->service->service_name : '';
            $reports[$key]['status'] = $appointment->status;
            $reports[$key]['id'] = $appointment->id;
            $reports[$key]['rescheduled_by'] = $appointment->rescheduled_by;
            $reports[$key]['canceled_by'] = $appointment->canceled_by;
            $reports[$key]['start'] = Carbon::parse($appointment->date . $appointment->time)->format('Y-m-d H:i:s');
            $reports[$key]['allDay'] = false;
            $key++;
        }
        return response()->json($reports);
    }

    /**
     * view all appointment with ajax and filter functionality
     * @param Request $request
     * @return type
     */
    public function viewAllAppointment(Request $request) {
        $allDate = $request->input('all_date') ?? '';
        $service_id = $request->input('service_id') ?? '';
        $allServices = ValueAddedService::all();

        $query = Appointment::with('service','expert')->where('user_id', $request->user()->id);
        if ($allDate) {
            $query->whereDate('date', Carbon::parse($allDate)->format('Y-m-d'));
        }
        if ($service_id) {

            $query->where('value_added_service_id', $service_id);
        }

        $allAppoints = $query->orderBy('created_at', 'DESC')->get();
        $view = View('user.appointments.appointment-list-card', compact('allDate','service_id','allServices', 'allAppoints'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * pending all appointment with ajax and filter functionality
     * @param Request $request
     * @return type
     */
    public function viewPendingAppointment(Request $request) {
        $pendingDate = $request->input('date') ?? '';
       $serviceId = $request->input('serviceId') ?? '';
        $services = ValueAddedService::all();
        $pendingAppoints = Appointment::with('service','expert')->where('user_id', $request->user()->id)->where('status', '!=', 1);
        if ($pendingDate) {
            $pendingAppoints->whereDate('date', Carbon::parse($pendingDate)->format('Y-m-d'));
        }
        if ($serviceId) {
            $pendingAppoints->where('value_added_service_id', $serviceId);
        }
        $filterError = "pendingExpert";
        if (empty($pendingDate)) {
            $pendingAppoints->where('date', '>=', Carbon::now());
        }

        $pendingAppoints = $pendingAppoints->orderBy('created_at', 'DESC')->get();
        $view = View('user.appointments.pending-appointment-list-card', compact('serviceId', 'services','pendingDate', 'pendingAppoints'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * Done all appointment with ajax and filter functionality
     * @param Request $request
     * @return type
     */
    public function viewDoneppointment(Request $request) {
        $doneDate = $request->input('date') ?? '';
        $serviceId = $request->input('serviceId') ?? '';
        $services = ValueAddedService::all();
        $doneAppoints = Appointment::with('service','expert')->where('user_id', $request->user()->id)->where('status', '=', 1);
        if ($doneDate) {
            $doneAppoints->whereDate('date', Carbon::parse($doneDate)->format('Y-m-d'));
        }
        if ($serviceId) {
            $doneAppoints->where('value_added_service_id', $serviceId);
        }

        if (empty($doneDate)) {
            $doneAppoints->where('date', '<', Carbon::now());
        }

        $doneAppoints = $doneAppoints->orderBy('created_at', 'DESC')->get();
        $view = View('user.appointments.done-appointment-list-card', compact('serviceId','services','doneDate', 'doneAppoints'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * user appointment confirm
     * 
     * @param type $id
     * @return type
     */
    public function appointmentConfirm($id) {
        $appoint = Appointment::with('expert')->where('id', $id)->firstOrFail();
        if (!empty($appoint) && $appoint->status == 1) {
            return back()->withInput()->with('error', __('You alredy confirm this appointment!'));
        }
        try {
            $appoint->status = 1;
            $saved = $appoint->save();
            // Expert Confirm appointment notification detail store
            $appoint = Appointment::with('user','expert','service')->where('id', $appoint->id)->first();
            $noti = new Notification;
            $noti->notification_to = 'expert';
            $noti->apppointment_id = $appoint->id;
            $noti->user_id = $appoint->expert->id;
            $noti->message = "Your appointment confirm with " . $appoint->expert->name . " on " . Carbon::parse($appoint->meeting_at)->format('d M, Y h:i A');
            $noti->save();
            $noti = new Notification;
            $noti->notification_to = 'admin';
            $noti->apppointment_id = $appoint->id;
            $noti->user_id = 1;
            $noti->message = "An appointment confirm with " . $appoint->expert->name . " on " . Carbon::parse($appoint->meeting_at)->format('d M, Y h:i A');
            $noti->save();
        } catch (Exception $ex) {
            $saved = false;
        }
        if ($saved) {
            if (!empty($appoint->user->mobile)) {
                $msg = Msg91::sms()->recipients([
                            ['mobiles' => $appoint->user->mobile, 'name' => $appoint->user->name, 'dateTime' => Carbon::parse($appoint->meeting_at)->format('d M, Y h:i A')],
                        ])
                        ->flow('60adf8b6c3fad12ed018037c')
                        ->send();
            }
            if (smtpConnect() == true) {
                Mail::to($appoint->user->email)->send(new UserAppointmentConfirm($appoint));
                Mail::to($appoint->expert->email)->send(new UserExpertAppointmentConfirm($appoint));
                Mail::to(config('mail.from.admin_email'))->send(new UserAdminAppointmentConfirm($appoint));
            }
            return redirect()->back()->with('success', __('Appointment confirm successfully!'));
        } else {
            return back()->withInput()->with('error', __('Something went wrong, Please try again later!'));
        }
    }

    /**
     * User appointment reschedule
     * 
     * @param Request $request
     * @return boolean
     */
    public function rescheduleppointment(Request $request) {
        if (strtotime($request->input('date')) <= strtotime(Carbon::now()->format('d-m-Y'))) {
            $response['date'] = true;
            return $response;
        }
        $appointment = Appointment::with('user', 'expert')->where('id', $request->input('id'))->first();
        try {
            $appointHistory = new AppointmentHistory;
            $appointHistory->date = $appointment->date;
            $appointHistory->appointment_id = $appointment->id;
            $appointHistory->user_id = $appointment->user_id;
            $appointHistory->expert_id = $appointment->expert_id;
            $appointHistory->time = $appointment->time;
            $appointHistory->terms = $appointment->terms;
            $appointHistory->remarks = $appointment->remarks;
            $appointHistory->rescheduled_by = 0;
            $saved = $appointHistory->save();
            if ($saved) {
                $appointment->date = Carbon::parse($request->input('date'))->format('Y-m-d');
                $appointment->time = $request->input('time');
                $appointment->remarks = $request->input('remarks');
                $appointment->terms = $request->input('terms');
                $appointment->meeting_at = Carbon::parse($request->input('date') . ' ' . $request->input('time'))->format('Y-m-d');
                $appointment->status = 2;
                $appointment->rescheduled_by = 0;
                $saved = $appointment->save();

                // User Confirm appointment notification detail store
                $appoint = Appointment::with('user')->where('id', $appointment->id)->first();
                $noti = new Notification;
                $noti->notification_to = 'expert';
                $noti->user_id = $appoint->expert->id;
                $noti->apppointment_id = $appoint->id;
                $noti->message = $appoint->user->name . " Rescheduled your appointment on " . Carbon::parse($appoint->meeting_at)->format('d M, Y h:i A');
                $noti->save();
                $noti = new Notification;
                $noti->notification_to = 'admin';
                $noti->apppointment_id = $appoint->id;
                $noti->user_id = 1;
                $noti->message = $appoint->user->name . " Rescheduled an appointment on " . Carbon::parse($appoint->meeting_at)->format('d M, Y h:i A');
                $noti->save();
                if ($saved)
                    if (!empty($appoint->user->mobile)) {
                        $mobile = config('mail.from.admin_mobile');
                        $msg = Msg91::sms()->recipients([
                                    ['mobiles' => $appoint->user->mobile, 'name' => $appoint->user->name, 'datetime' => Carbon::parse($appoint->meeting_at)->format('d M, Y h:i A')],
                                ])
                                ->flow('60adfa1536714d3e8c6c6581')
                                ->send();
                        if (smtpConnect() == true) {
                            Mail::to($appointment->expert->email)->send(new UserExpertAppointmentReschedule($appointment));
                            Mail::to(config('mail.from.admin_email'))->send(new UserAdminAppointmentReschedule($appointment));
                            Mail::to($appointment->user->email)->send(new UserAppointmentReschedule($appointment));
                        }
                    }
            }
        } catch (Exception $ex) {
            return false;
        }
        return true;
    }

    /**
     * user cancel this appointment
     * @param type $id
     * @return type
     */
    public function cancelAppoint($id) {
        $saved = false;
        try {
            $appoint = Appointment::where('id', $id)->first();
            if (!empty($appoint)) {
                $appoint->status = 3;
                $appoint->canceled_by = 0;
                $saved = $appoint->save();
                //project report confirm show notification to admin
                $noti = new Notification;
                $appoint = Appointment::with('user', 'expert')->where('id', $id)->first();
                $noti->notification_to = 'admin';
                $noti->appointment_id = $appoint->id;
                $noti->user_id = 1;
                $noti->message = $appoint->user->name . " Cancel appointment";
                $noti->save();
                $noti = new Notification;
                $noti->notification_to = 'expert';
                $noti->appointment_id = $appoint->id;
                $noti->user_id = $appoint->expert->id;
                $noti->message = $appoint->user->name . " Cancel appointment";
                $noti->save();
            }
        } catch (Exception $e) {
            $saved = false;
        }
        if ($saved) {
            if (smtpConnect() == true) {
                $appointment = Appointment::with('user','service','expert')->where('id', $id)->first();
                Mail::to($appointment->user->email)->send(new UserAppointmentCancel($appointment));
                Mail::to(config('mail.from.admin_email'))->send(new UserAdminAppointmentCancel($appointment));
                Mail::to($appointment->expert->email)->send(new UserExpertAppointmentCancel($appointment));
            }
            return redirect()->back()->with('success', __('Appointment canceled successfully!'));
        } else {
            return back()->withInput()->with('error', __('Something went wrong, Please try again later!'));
        }
    }

    public function storeReviews(Request $request) {

        $rules = [
            'feedback' => 'required',
        ];

        $messages = [
            'feedback.required' => 'The feedback field is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(array(
                        'success' => false,
                        'message' => 'There are incorect values in the form!',
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 422);
        }
        $review = new Review;
        $review->user_id = Auth::user()->id;
        $review->appointment_id = $request->appointment_id;
        $review->feedback = $request->feedback;
        $review->rating = $request->rating;
        $reviewData = $review->save();

        //project report confirm show notification to admin
        $noti = new Notification;
        $appoint = Appointment::with('user', 'expert')->where('id', $request->appointment_id)->first();
        $noti->notification_to = 'admin';
        $noti->appointment_id = $request->appointment_id;
        $noti->message = "You recive a new review about appointment";
        $noti->save();

        if ($reviewData) {
            return response()->json(['success' => true, 'msg' => "Your feedback submitted succesfully"]);
        }
    }

    /**
     * notification mark as read
     * 
     * @param type $id
     */
    public function markReadNotification($id) {
        $noti = Notification::where('id', $id)->first();
        $noti->unread = 1;
        $noti->save();
        return true;
    }

}
