<?php

namespace App\Http\Controllers\user;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\LandReport;
Use App\Models\State;

class LandReportController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $name = $request->input('name') ?? '';
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $location = $request->input('location') ?? '';
        $states = State::where('country_id', 101)->get();
        $user = auth()->user();
        $allReports = LandReport::landReports($request, '', $user);
        return view('user.reports.landreport', compact('allReports', 'name', 'date', 'location', 'states'));
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function YourLandReportDelivered(Request $request) {
        $name = $request->input('name') ?? '';
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $location = $request->input('location') ?? '';
        $states = State::where('country_id', 101)->get();
        $user = auth()->user();
        $deliveredReports = LandReport::landReports($request, 1, $user);
        return view('user.reports.landreportdelivered', compact('deliveredReports', 'name', 'date', 'location', 'states'));
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function YourLandReportInprogress(Request $request) {
        $name = $request->input('name') ?? '';
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $location = $request->input('location') ?? '';
        $states = State::where('country_id', 101)->get();
        $user = auth()->user();
        $inprogressReports = LandReport::landReports($request, 2, $user);
        return view('user.reports.landreportinprogress', compact('inprogressReports', 'name', 'date', 'location', 'states'));
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function YourLandReportPending(Request $request) {
        $name = $request->input('name') ?? '';
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $location = $request->input('location') ?? '';
        $states = State::where('country_id', 101)->get();
        $user = auth()->user();
        $pendingReports = LandReport::landReports($request, 'pending', $user);
        return view('user.reports.landreportpending', compact('pendingReports', 'name', 'date', 'location', 'states'));
    }

}
