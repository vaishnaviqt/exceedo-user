<?php

namespace App\Http\Controllers\user;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Models\Payment;
use App\Models\Appointment;
use App\Models\ValueAddedService;
use Auth;

class PaymentController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $paginate = $request->paginate ?? 10;
        $services = ValueAddedService::all();
        $serviceType = $request->input('serviceType');
        $startDate = !empty($request->created_at) ? Carbon::parse($request->created_at)->format('Y-m-d') : '';
        $paymentType = $request->input('paymentType');

        $userPayments = Payment::select(['payments.created_at', 'payments.land_report_id','payments.file', 'services.service_name', 'payments.id as paymentId', 'payments.transaction_id', 'payments.appointment_id', 'payments.amount', 'payments.currency', 'payments.entity', 'payments.order_id', 'payments.method', 'payments.bank', 'payments.bank_transaction_id', 'payments.refund_id', 'payments.refund_Date', 'payments.status', 'payments.land_report_id', 'payments.property_list_id', 'payments.subscription_id'])->whereNotNull('transaction_id')
                ->leftJoin('appointments as ap', 'ap.id', '=', 'payments.appointment_id')
                ->leftJoin('value_added_services as services', 'services.id', '=', 'ap.value_added_service_id');
        if (!empty($serviceType)) {
            $userPayments = $userPayments->where('ap.value_added_service_id', $serviceType);
        }
        $userPayments = $userPayments->where('ap.user_id', Auth::user()->id);

        if (!empty($request->created_at)) {
            $userPayments->whereDate('payments.created_at', $startDate);
        }
        if (!empty($paymentType)) {
            $userPayments->where('payments.method', 'Like', '%' . $paymentType . '%');
        }

        $userPayments = $userPayments->orderBy('paymentId', 'desc')->paginate($paginate);

        return view('user.payments', compact('userPayments', 'services', 'serviceType', 'paginate', 'startDate', 'paymentType'));
    }

    public function download($id, Request $request) {

        $invoice_upload = Payment::find($id);
        $file = public_path() . "/invoice" . "/" . $invoice_upload->file;

        return response()->download($file);
    }

}
