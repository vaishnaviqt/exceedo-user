<?php

namespace App\Http\Controllers\user;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Appointment;
use App\Models\LandReport;
use App\Models\PropertyList;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller {

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        // done appointment listing 
        $doneAppointments = Appointment::with('expert', 'service')
                        ->where('user_id', Auth::user()->id)
                        ->where('status', 1)
                        ->take(10)->orderBy('id', 'desc')->get();
        
        $userReports = LandReport::with('user','expert','state')
                        ->where('user_id', Auth::user()->id)
                        ->where('status', 1)
                        ->take(10)->orderBy('id', 'desc')->get();

        $totalappointment = Appointment::where('user_id', Auth::user()->id)->get()->count();
        $totalReport = LandReport::where('user_id', Auth::user()->id)->get()->count();
        $activeLand = PropertyList::where('user_id', Auth::user()->id)->where('status', 1)->get()->count();
        $inactiveLand = PropertyList::where('user_id', Auth::user()->id)->where('status',0)->get()->count();
        // for pending appointment
        $filterDate = $request->day ?? 'month';

        //pending appointment for user
        $pendingPayments = DB::table('appointments')
                ->where('user_id', Auth::user()->id)
                ->where('status', 0);
        if ($filterDate == "week") {
            $pendingPayments->select(DB::RAW('Day(date) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('date', '>', now()->startOfWeek())
                    ->where('date', '<', now()->endOfWeek());
        } else if ($filterDate == "month") {
            $pendingPayments->select(DB::RAW('Day(date) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('date', '>', now()->startOfMonth()->format('Y-m-d'))
                    ->where('date', '<', now()->endOfMonth()->format('Y-m-d'));
        } else if ($filterDate == "year") {
            $pendingPayments->select(DB::RAW('MONTHNAME(date) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->whereYear('date', Carbon::now()->year);
        }
        $pendingPayments = $pendingPayments->groupBy('count_appointment')->get();

        if ($filterDate == "week") {
            $PendingEarning = getDateWiseData(now()->startOfWeek(), now()->endOfWeek(), $pendingPayments);
        } else if ($filterDate == "month") {
            $PendingEarning = getDateWiseData(now()->startOfMonth(), now()->endOfMonth(), $pendingPayments);
        } else if ($filterDate == "year") {
            $PendingEarning = getMonthWiseData($pendingPayments);
        }
        $pendingAppointment = $PendingEarning['paidappointment'];
        $pendingAppointment = $pendingAppointment;
        $pendingAppointmentTitle = 'Penidng Appointment';

        //done appointment for user
        $paidPayments = DB::table('appointments')
                ->where('user_id', Auth::user()->id)
                ->where('status', 1);
        if ($filterDate == "week") {
            $paidPayments->select(DB::RAW('Day(date) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('date', '>', now()->startOfWeek())
                    ->where('date', '<', now()->endOfWeek());
        } else if ($filterDate == "month") {
            $paidPayments->select(DB::RAW('Day(date) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('date', '>', now()->startOfMonth()->format('Y-m-d'))
                    ->where('date', '<', now()->endOfMonth()->format('Y-m-d'));
        } else if ($filterDate == "year") {
            $paidPayments->select(DB::RAW('MONTHNAME(date) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->whereYear('date', Carbon::now()->year);
        }
        $paidPayments = $paidPayments->groupBy('count_appointment')->get();

        if ($filterDate == "week") {
            $AdminEarning = getDateWiseData(now()->startOfWeek(), now()->endOfWeek(), $paidPayments);
        } else if ($filterDate == "month") {
            $AdminEarning = getDateWiseData(now()->startOfMonth(), now()->endOfMonth(), $paidPayments);
        } else if ($filterDate == "year") {
            $AdminEarning = getMonthWiseData($paidPayments);
        }
        $adminEarningAmount = $AdminEarning['paidappointment'];
        $adminEarningDate = $AdminEarning['date'];
        $doneAppointmentTitle = 'Done Appointment';

        // for user verified land 
        $verifiedLands = DB::table('property_list')
                ->where('user_id', Auth::user()->id)
                ->where('status', 1);
        if ($filterDate == "week") {
            $verifiedLands->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfWeek())
                    ->where('created_at', '<', now()->endOfWeek());
        } else if ($filterDate == "month") {
            $verifiedLands->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfMonth()->format('Y-m-d'))
                    ->where('created_at', '<', now()->endOfMonth()->format('Y-m-d'));
        } else if ($filterDate == "year") {
            $verifiedLands->select(DB::raw('MONTHNAME(created_at) as count_appointment'), DB::raw('count(*) as count_date'))->whereYear('created_at', Carbon::now()->year);
        }
        $verifiedLands = $verifiedLands->groupBy('count_appointment')->get();
        if ($filterDate == "week") {
            $landVerified = getDateWiseData(now()->startOfWeek(), now()->endOfWeek(), $verifiedLands);
        } else if ($filterDate == "month") {
            $landVerified = getDateWiseData(now()->startOfMonth(), now()->endOfMonth(), $verifiedLands);
        } else if ($filterDate == "year") {
            $landVerified = getMonthWiseData($verifiedLands);
        }

        $landVerifiedDeatil = $landVerified['paidappointment'];
        $landVerifiedDate = $landVerified['date'];

        // for user unverified land
        $unverifiedLands = DB::table('property_list')
                ->where('user_id', Auth::user()->id)
                ->where('status', 0);
        if ($filterDate == "week") {
            $unverifiedLands->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfWeek())
                    ->where('created_at', '<', now()->endOfWeek());
        } else if ($filterDate == "month") {
            $unverifiedLands->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfMonth()->format('Y-m-d'))
                    ->where('created_at', '<', now()->endOfMonth()->format('Y-m-d'));
        } else if ($filterDate == "year") {
            $unverifiedLands->select(DB::raw('MONTHNAME(created_at) as count_appointment'), DB::raw('count(*) as count_date'))->whereYear('created_at', Carbon::now()->year);
        }
        $unverifiedLands = $unverifiedLands->groupBy('count_appointment')->get();
        if ($filterDate == "week") {
            $landUnverified = getDateWiseData(now()->startOfWeek(), now()->endOfWeek(), $unverifiedLands);
        } else if ($filterDate == "month") {
            $landUnverified = getDateWiseData(now()->startOfMonth(), now()->endOfMonth(), $unverifiedLands);
        } else if ($filterDate == "year") {
            $landUnverified = getMonthWiseData($unverifiedLands);
        }
        $landUnverifiedDeatil = $landUnverified['paidappointment'];
        $landUnverifiedDeatil = $landUnverifiedDeatil;

        return view('user.dashboard', compact('adminEarningDate', 'pendingAppointment', 'paidPayments', 'adminEarningAmount', 'pendingAppointmentTitle', 'doneAppointmentTitle', 'landVerifiedDeatil', 'landVerifiedDate', 
                'landUnverifiedDeatil', 'totalappointment', 'filterDate', 'doneAppointments',
                'userReports','totalReport','activeLand','inactiveLand'));
    }

}
