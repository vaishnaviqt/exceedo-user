<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminNotifyReport extends Mailable {

    use Queueable, SerializesModels;

    public $reportDocument;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reportDocument) {
        $this->reportDocument = $reportDocument;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject("Report Notify By Admin")->markdown('emails.experts.report-notify');
    }

}


