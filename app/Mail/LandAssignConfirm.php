<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LandAssignConfirm extends Mailable
{
    use Queueable, SerializesModels;
    
    public $propery;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($propery)
    {
        $this->propery = $propery;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Land Assign Confirmation")->markdown('emails.admin.land-assign-confirm');
    }
}

