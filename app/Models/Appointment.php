<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model {

    use HasFactory;

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function expert() {
        return $this->belongsTo('App\Models\User', 'expert_id', 'id');
    }

    public function service() {
        return $this->belongsTo('App\Models\ValueAddedService', 'value_added_service_id', 'id');
    }

    public function payment(){
        return $this->belongsTo('App\Models\Payment', 'payment_id', 'transaction_id');
}

}
