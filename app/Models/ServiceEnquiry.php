<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceEnquiry extends Model
{
    use HasFactory;
    
    public function expert(){
        return $this->belongsTo('App\Models\User','expert_id');
    }

    public function service() {
        return $this->belongsTo('App\Models\ValueAddedService', 'value_added_service_id');
}

}
