<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Auth;

class User extends Authenticatable {

    use HasFactory,
        Notifiable,
        HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function booted() {
        parent::boot();

        static::creating(function ($model) {
                 //  $model->user_unique_id =  str_pad($, 4, 0, STR_PAD_RIGHT)."-KYL" ;
                 $mobile = request()->mobile_number;
                 $model->user_unique_id =  $mobile."@KYL" ;
        });
    }

    public function services() {
        return $this->belongsToMany(ValueAddedService::class, 'user_services');
    }

    public function state() {
        return $this->belongsTo(State::class);
    }

    public function userService() {
        return $this->hasMany(UserService::class, 'user_id');
    }

}
