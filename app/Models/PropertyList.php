<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use Carbon\Carbon;

class PropertyList extends Model {

    use HasFactory;
    use SoftDeletes;

    protected $table = 'property_list';

    public function users() {
        return $this->belongsToMany(UserService::class, 'user_services');
    }

    public function agent() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function expert() {
        return $this->belongsTo(User::class, 'expert_id');
    }
    public function listed() {
        return $this->belongsTo(User::class, 'listed_by');
    }

    public function propertyImage() {
        return $this->hasMany(PropertyListImage::class, 'property_list_id');
    }

    public function propertyOtherImage() {
        return $this->hasMany(LandOtherDocument::class, 'property_list_id');
    }

    public function scopeStatus($query, $type, $filterDate) {
        $verifiedLands = DB::table('property_list')
                ->where('user_id', Auth::user()->id)
                ->where('status', $type);
        if ($filterDate == "week") {
            $verifiedLands->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfWeek())
                    ->where('created_at', '<', now()->endOfWeek());
        } else if ($filterDate == "month") {
            $verifiedLands->select(DB::raw('Day(created_at) as count_appointment'), DB::raw('count(*) as count_date'))
                    ->where('created_at', '>', now()->startOfMonth()->format('Y-m-d'))
                    ->where('created_at', '<', now()->endOfMonth()->format('Y-m-d'));
        } else if ($filterDate == "year") {
            $verifiedLands->select(DB::raw('MONTHNAME(created_at) as count_appointment'), DB::raw('count(*) as count_date'))->whereYear('created_at', Carbon::now()->year);
        }
        $verifiedLands = $verifiedLands->groupBy('count_appointment')->get();
        if ($filterDate == "week") {
            $landVerified = getDateWiseData(now()->startOfWeek(), now()->endOfWeek(), $verifiedLands);
        } else if ($filterDate == "month") {
            $landVerified = getDateWiseData(now()->startOfMonth(), now()->endOfMonth(), $verifiedLands);
        } else if ($filterDate == "year") {
            $landVerified = getMonthWiseData($verifiedLands);
        }
        return $landVerified;
    }

    public function scopePropertyList($query, $request, $status = null, $user = null) {
        $name = $request->name ?? '';
        $address = $request->address ?? '';
        $businessType = $request->business_type ?? '';
        $startDate = !empty($request->created_at) ? Carbon::parse($request->created_at)->format('Y-m-d') : '';
        $query->with('propertyImage');
        if (!empty($user) && $user->hasRole('user')) {
            $query->where('listed_by', $user->id);
        }
        if (!empty($user) && $user->hasRole('expert')) {
            $query->where('listed_by', $user->id);
        }
        if (!empty($name)) {
            $query->where('land_name', 'LIKE', '%' . $name . '%');
        }
        if (!empty($address)) {
            $query->where('address', 'LIKE', '%' . $address . '%');
        }
        if (!empty($businessType)) {
            $query->where('business_type', $businessType);
        }
        if (!empty($request->created_at)) {
            $query->whereDate('created_at', '=', $startDate);
        }
        if (!empty($status)) {
            $status = ($status == 'pending') ? 0 : $status;
            $query->where('status', $status);
        }
        return $query->orderBy('id', "desc")->paginate(10);
    }

}
