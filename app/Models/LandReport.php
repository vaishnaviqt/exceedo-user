<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LandReport extends Model {

    use HasFactory;

    /**
     * 
     * @return type
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * 
     * @return type
     */
    public function state() {
        return $this->belongsTo(State::class);
    }

    /**
     * 
     * @return type
     */
    public function expert() {
        return $this->belongsTo(User::class, 'expert_id');
    }
    /**
     * 
     * @return type
     */
    public function reportDocument() {
        return $this->hasOne(ReportDocument::class, 'land_report_id')->latest();
    }
    /**
     * 
     * @return type
     */
    public function valueAdded() {
        return $this->belongsTo(ValueAddedService::class, 'value_added_service');
    }

    /**
     * 
     * @param type $query
     * @param type $request
     * @param type $status
     * @param type $user
     * @return type
     */
    public function scopeLandReports($query, $request, $status = null, $user = null) {
        $name = $request->input('name') ?? '';
        $date = !empty($request->input('date')) ? Carbon::parse($request->input('date'))->format('Y-m-d') : '';
        $location = $request->input('location') ?? '';
        $query->with('user', 'state', 'expert', 'reportDocument', 'valueAdded');
        if (!empty($user) && $user->hasRole('user')) {
            $query->where('user_id', $user->id);
        }
        if (!empty($user) && $user->hasRole('expert')) {
            $query->where('expert_id', $user->id)->whereNull('cancel');
        }
        if (!empty($name)) {
            $query->where('land_name', 'LIKE', "%$name%");
        }
        if (!empty($date)) {
            $query->whereDate('created_at', $date);
        }
        if (!empty($location)) {
            $query->where('state_id', $location);
        }
         $service = $request->input('serviceType') ?? '';
        if (!empty($service)) {
            $query->where('value_added_service', $service);
        }
        if (!empty($status)) {
            $status = ($status == 'pending') ? 0 : $status;
            $query->where('status', $status);
        }
        return $query->orderBy('created_at', 'desc')->paginate(10);
    }

}
