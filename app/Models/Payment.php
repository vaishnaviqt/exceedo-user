<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model {

    use HasFactory;

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    
    public function property() {
        return $this->belongsTo('App\Models\PropertyList', 'property_id', 'id');
    }

}
