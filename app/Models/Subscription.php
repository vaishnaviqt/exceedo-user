<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model {

    use HasFactory;

    public function detail() {
        return $this->hasMany('App\Models\SubscriptionDetail');
    }
    public function package() {
        return $this->belongsTo('App\Models\Package');
    }

}
