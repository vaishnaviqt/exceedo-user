<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EnquiryAssign extends Mailable
{
    use Queueable, SerializesModels;
    
    public $landReport;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($landreport)
    {
        $this->landReport = $landreport;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.experts.enquiry-assign');
    }
}
