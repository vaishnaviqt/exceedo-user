<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgentContact extends Model {

    use HasFactory;
   
    protected $table = 'contact_agents';
    
    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
}

    public function land() {
        return $this->belongsTo('App\Models\PropertyList', 'land_id', 'id');
}

}
