<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleService extends Model {

    use HasFactory;

    protected $table = 'role_services';

    public function serviceName() {
        return $this->belongsTo(Service::class, 'service_id');
    }

}
