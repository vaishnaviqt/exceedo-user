<?php
namespace App\Helper;
use App\Models\ValueAddedService;

class Helper
{
    public function getValueAddedService(){
        $values = ValueAddedService::get();
        return $values;
    }

}
