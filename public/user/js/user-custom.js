$(document).ready(function () {
    var baseUrl = $('meta[name="base_url"]').attr('content');
    //    user profile update
    $('.uploadAvatar[type="file"]').fileupload({
        dataType: 'json',
        url: baseUrl + '/user/upload_user_photo',
        maxFileSize: (1024 * 1024 * 2),
        acceptFileTypes: /(\.|\/)(gif|bmp|jpe?g|png)$/i,
        disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator && navigator.userAgent),
        imageMaxWidth: 800,
        imageMaxHeight: 800,
        imageCrop: true,
        messages: {
            maxFileSize: "File exceeds maximum allowed size of 2MB",
            acceptFileTypes: "Only jpg, jpeg, bmp, png and gif files are allowed"
        },
        done: function (e, data) {
            console.log(data);
            $("#photoLoader").hide();
            if (data.result.photo) {
                var errors = '';
                for (var i in data.result.photo) {
                    if (data.result.photo[i].length) {
                        errors += '<div><strong>' + data.result.photo[i] + '</strong></div>';
                    }
                }
                $(".upload-response").html(errors).addClass('error');
                return false;
            }

            if (data.result.avatar) {
                $(".userPhoto").attr("src", data.result.avatar);
                $(".upload-response").remove();
            }
                $("#photoLoader").hide();
        },
        progress: function (e, data) {
            $("#photoLoader").show();

        },
        processfail: function (e, data) {
            $("#photoLoader").hide();
            swal({
                icon: "error",
                text: data.files[data.index].error
            });
        }
    });

    //Hamburger menu functionality
    $('.hamburgerMenu').click(function () {
        $('.wrapper').toggleClass('collapseSidebar');
    });

    $('.mobileFilterBtn').click(function () {
        $(this).next('.filterForm').addClass('shown');
    });
    $('.m_filterCloseBtn').on('click', function () {
        $('.filterForm').removeClass('shown');
    });

    // datepicker
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepicker').datepicker({
        minDate: today,
        format: 'dd-mm-yyyy'
});
});