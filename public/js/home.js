$(document).ready(function () {
    var baseUrl = $('meta[name="base_url"]').attr('content');
    $(".valueArticlesCarousel").owlCarousel({
        items: 1,
        dots: false,
        nav: true,
        stagePadding: 0,
        autoplay: true,
        autoplayTimeout: 5000,
        margin: 20,
        responsive: {
            0: {
                items: 1,
                stagePadding: 50,
            },
            575: {
                items: 3,
                stagePadding: 0,
            },
            1140: {
                items: 4,
                stagePadding: 0,
            },
            1360: {
                items: 5,
                stagePadding: 0,
            }
        }
    });

    //Status Bar
    buildProgressBar();
    function buildProgressBar() {
        var progressBarMain = $("<div>", {
            class: "progressBarMain"
        });
        var progressBar = $("<div>", {
            class: "progressBar"
        });
        var bar = $("<div>", {
            id: "bar"
        });
        progressBar = progressBar.append(bar);
        progressBarMain.append(progressBar).prependTo('.valueArticlesCarousel');
    }
    statusBar();
    $('.valueArticlesCarousel').on('changed.owl.carousel', function () {
        setTimeout(statusBar, 200);
    });
    function statusBar() {
        var totalSlide = $('.valueArticlesCarousel .owl-item').length;
        var remainingSlide = $('.valueArticlesCarousel .owl-item.active:last').nextAll().length;
        var statusBarSize = (100 - (remainingSlide / totalSlide) * 100) + '%';
        $('#bar').width(statusBarSize);
    }


    //Home Banner
    var sync1 = $("#bannerSlider");
    var sync2 = $("#bannerThumbnails");
    var flag = false;

    var slides = sync1.owlCarousel({
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        items: 1,
        nav: false,
        loop: true,
    }).on('change.owl.carousel', function (e) {
        if (e.namespace && e.property.name === 'position' && !flag) {
            flag = true;
            thumbs.to(e.relatedTarget.relative(e.property.value), 300, true);
            flag = false;
        }
    }).data('owl.carousel');

    var thumbs = sync2.owlCarousel({
        items: 4,
        autoWidth: true,
        nav: true,
        dots: true,
        dotData: true,
        dotsData: true,
        margin: 20,
        autoplay: true,
        autoplayTimeout: 5000,
        loop: true,
        responsive: {
            0: {
                loop: true,
                center: false,
            },
            980: {
                loop: true,
                center: false
            }
        }
    }).on('click', '.thumbBanTiles', function (e) {
        //e.preventDefault(); sync1.trigger('to.owl.carousel', [$(e.target).parents('.owl-item').index(), 300, true]);
    }).on('change.owl.carousel', function (e) {
        if (e.namespace && e.property.name === 'position' && !flag) {
            flag = true;
            slides.to(e.relatedTarget.relative(e.property.value), 300, true);
            flag = false;
        }
    }).data('owl.carousel');

    checkClasses();
    $('#bannerThumbnails').on('translated.owl.carousel', function (event) {
        checkClasses();
    });

    function checkClasses() {
        var total = $('#bannerThumbnails .owl-stage .owl-item.active').length;

        $('#bannerThumbnails .owl-stage .owl-item').removeClass('firstActiveItem');
        $('#bannerThumbnails .owl-stage .owl-item').removeClass('beforeActiveItem');

        $('#bannerThumbnails .owl-stage .owl-item.active').each(function (index) {
            if (index === 0) {
                $(this).addClass('firstActiveItem').prev().addClass('beforeActiveItem');
            }
        });
    }


//    $("#sell_land_info_submit").on('click', function (evt) {
//        let myForm = document.getElementById('upload_property_list');
//        let formData = new FormData(myForm);
//        $.ajax({
//            type: 'POST',
//            url: baseUrl + "/store-services",
//            data: formData,
//            cache: false,
//            contentType: false,
//            processData: false,
//            success: function (data) {
//                toastr.success(data.message);
//                $("#sell_land_info_payment").trigger('click');
//            },
//            error: function (errorResponse) {
//                toastr.error(errorResponse.responseJSON.message);
//            }
//        });
//
//    });


// getting name or id 
    $('.getName').click(function (e) {
        e.preventDefault();
        var serviceId = $(this).data('id');
        var name = $(this).data('name');
        $('#value_added_service_id').val(serviceId);
        $('#book-appointment-title').text(name);
    });



});
