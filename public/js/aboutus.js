$(document).ready(function(){  
    $(".valueArticlesCarousel").owlCarousel({
        items : 1,
        dots:false,
        nav:true,
        stagePadding: 0,
        autoplay:true,
        autoplayTimeout:5000,
        margin:20,
        responsive : {
            0 : {
                items : 1,
                stagePadding: 50,
            },
            575 : {
                items : 3,
                stagePadding: 0,
            },
            1140 : {
                items : 4,
                stagePadding: 0,
            },
            1360 : {
                items : 5,
                stagePadding: 0,
            }
        }
    });

    //Status Bar
    buildProgressBar();                
    function buildProgressBar(){
        var progressBarMain = $("<div>",{
            class:"progressBarMain"
        });
        var progressBar = $("<div>",{
            class:"progressBar"
        });
        var bar = $("<div>",{
            id:"bar"
        });
        progressBar = progressBar.append(bar);
        progressBarMain.append(progressBar).prependTo('.valueArticlesCarousel');
    }
    statusBar(); 
    $('.valueArticlesCarousel').on('changed.owl.carousel', function(){
        setTimeout(statusBar, 200);
    });
    function statusBar(){
        var totalSlide = $('.valueArticlesCarousel .owl-item').length; 
        var remainingSlide = $('.valueArticlesCarousel .owl-item.active:last').nextAll().length; 
        var statusBarSize = (100 - (remainingSlide/totalSlide)*100)+'%';
        $('#bar').width(statusBarSize); 
    } 


    $(".partnersCompanies").owlCarousel({
        items : 5,
        dots:false,
        nav:true,
        margin:20,
        loop:true,
        responsiveClass:true,
        responsive : {
            0 : {
                items : 2 
            },
            575 : {
                items : 3
            },
            1140 : {
                items : 5,
                margin:40
            }
        }
    });

    $(".cliensSaying").owlCarousel({
        items : 3,
        dots:false,
        nav:true,
        margin:20,
        loop:true,
        center: true,
        responsiveClass:true,
        responsive : {
            0 : {
                items : 1 
            },
            575 : {
                items : 3
            },
            1140 : {
                items : 3,
                margin:40
            }
        }
    });
});