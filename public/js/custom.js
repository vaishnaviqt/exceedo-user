$(document).ready(function () {
    //Mobile splash screen


    $('.dropdown-menu').click(function (e) {
        e.stopPropagation();
    });
    $('.mobileNavCloseBtn a').on('click', function () {
        $(".hamburgerBtn").trigger("click");
    });

    /*Service submenu dropdown */
    $('a.subMenuDropdownBtn').on('click', function () {
        $(this).toggleClass('opened');
        $(this).next('.subMenuDropdown').slideToggle();
    });

    //STICKY NAVIGATION
    var lastScrollTop = 0;
    $(window).scroll(function (event) {
        var st = $(this).scrollTop();
        if (st > lastScrollTop) {
            if (st > 50) {
                $('.header').addClass('stickyHeader');
            }
        } else {
            if (st < 50) {
                $('.header').removeClass('stickyHeader');
            }
        }
        lastScrollTop = st;
    });


    //Buy Listing - Filter
    $('#m_filterBtn').click(function () {
        $('.filterSection').addClass('show');
    });
    $(document).on('click', '.filterClose', function () {
        $('.filterSection').removeClass('show');
    })


    // timepicker
//    $('#timepicker').timepicker({
//        uiLibrary: 'bootstrap4'
//    });

    // datepicker
//    var today, datepicker;
//    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
//    datepicker = $('#datepicker').datepicker({
//        minDate: today,
//        format: 'dd-mm-yyyy'
//    });

    // Land Report Anlysis form, Land title search form
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

});


// Activate Floating Label - Input
$(function () {
    $('.has-float-label .gj-unselectable').click(function () {
        $(this).next(label).focus();
    });
    $('.has-float-label .gj-unselectable').focusin(function () {
        $(this).next('label').addClass('active');
    });
    $('.has-float-label .gj-unselectable').focusout(function () {
        if ($(input).val().length <= 0) {
            $(this).next('label').removeClass('active');
        } else {
            $(this).next('label').addClass('active');
        }
    });
});

//Change Box color on select radio button
$(document).ready(function () {
    //change colour when radio is selected
    $('#selectCard input:radio').change(function () {
        // Only remove the class in the specific `box` that contains the radio
        $('div.highlight').removeClass('highlight');
        $(this).closest('.card').addClass('highlight');
    });
});

function openModal() {
    document.getElementById("myModal").style.display = "block";
}

function closeModal() {
    document.getElementById("myModal").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    var captionText = document.getElementById("caption");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
    captionText.innerHTML = dots[slideIndex - 1].alt;
}


// Image light Box
lightbox.option({
    'albumLabel': "picture %1 of %2",
    'fadeDuration': 300,
    'resizeDuration': 150,
    'wrapAround': true
});





