$(document).ready(function () {
    var baseUrl = $('meta[name="base_url"]').attr('content');
//    expert profile update
    $('.uploadAvatar[type="file"]').fileupload({
        dataType: 'json',
        url: baseUrl + '/admin/upload_admin_photo',
        maxFileSize: (1024 * 1024 * 2),
        acceptFileTypes: /(\.|\/)(gif|bmp|jpe?g|png)$/i,
        disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator && navigator.userAgent),
        imageMaxWidth: 800,
        imageMaxHeight: 800,
        imageCrop: true,
        messages: {
            maxFileSize: "File exceeds maximum allowed size of 2MB",
            acceptFileTypes: "Only jpg, jpeg, bmp, png and gif files are allowed"
        },
        done: function (e, data) {
            $("#photoLoader").hide();
            if (data.result.photo) {
                var errors = '';
                for (var i in data.result.photo) {
                    if (data.result.photo[i].length) {
                        errors += '<div><strong>' + data.result.photo[i] + '</strong></div>';
                    }
                }
                $(".upload-response").html(errors).addClass('error');
                return false;
            }

            if (data.result.avatar) {
                $(".userPhoto").attr("src", data.result.avatar);
                $(".upload-response").remove();
            }
//                $("#photoLoader").hide();
        },
        progress: function (e, data) {
            $("#photoLoader").show();

        },
        processfail: function (e, data) {
            $("#photoLoader").hide();
             swal({
                icon: "error",
                text: data.files[data.index].error
            });
        }
    });
    
    //Admin appointment show in calender
    document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('calendar');
    var currentDate = $.datepicker.formatDate('yy-mm-dd', new Date());
    var calendar = new FullCalendar.Calendar(calendarEl, {
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
        },
        initialDate: currentDate,
        navLinks: true, // can click day/week names to navigate views
        businessHours: true, // display business hours
        editable: true,
        selectable: true,
        eventSources: [
            // your event source
            {
                url: baseUrl +
                        "/admin/appointment-details",
                method: "GET",
                failure: function () {
                    alert(
                            "there was an error while fetching reconciliation report!"
                            );
                },
            },
                    // any other sources...
        ],
    });
    calendar.render();
});

    //Hamburger menu functionality
    $('.hamburgerMenu').click(function () {
        $('.wrapper').toggleClass('collapseSidebar');
    });

    //Select Show by
    $('.dropdown-menu a').click(function () {
        $('.selected').text($(this).text());
    });

    // Report page Nav Tabs
    $(".nav-tabs a").click(function () {
        $(this).tab('show');
    });


    // datepicker
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepicker').datepicker({
        minDate: today,
        format: 'dd-mm-yyyy'
    });
    // datepicker1
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepicker1').datepicker({
        minDate: today,
        format: 'dd-mm-yyyy'
    });

    // All Appointment popup 
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#allAppointDate').datepicker({
        minDate: today,
        format: 'dd-mm-yyyy'
    });

    

    // timepicker
    $('#timepicker').timepicker({
        uiLibrary: 'bootstrap4'
    });
    $('#appTime').timepicker({
        uiLibrary: 'bootstrap4'
    });
    

    // Activate Floating Label - Input
    $(function () {
        $('.has-float-label .gj-unselectable').click(function () {
            $(this).next(label).focus();
        });
        $('.has-float-label .gj-unselectable').focusin(function () {
            $(this).next('label').addClass('active');
        });
        $('.has-float-label .gj-unselectable').focusout(function () {
            if ($(input).val().length <= 0) {
                $(this).next('label').removeClass('active');
            } else {
                $(this).next('label').addClass('active');
            }
        });
    });



    //SEARCH FORM
    $(".searchForm .open-mobile-search").click(function () {
        $(this).siblings("form").addClass("searchMobile");
        $("body").css("overflow", "hidden");
    });

    $(".closeSearch").click(function () {
        $(this).parents(".searchForm form").removeClass("searchMobile");
        $("body").css("overflow", "auto");
    });

    $(".acceptedCard").click(function () {
        $(this).parents('.notification').css("display", "none");
        $("#accepted-box").css("display", "block");
    });

    $(".closeCard").click(function () {
        $(this).parents('.notification').css("display", "none");
    });

    // Delivered Report
    $(".deliveredReport-carousel").owlCarousel({
        items: 5,
        dots: false,
        nav: true,
        margin: 20,
        loop: false,
        responsiveClass: true,
        onInitialized: counter, //When the plugin has initialized.
        onTranslated: counter, //When the translation of the stage has finished.
        responsive: {
            0: {
                items: 1
            },
            575: {
                items: 1
            },
            1140: {
                items: 1,
                margin: 40
            }
        }
    });
    function counter(event) {
        var element = event.target;         // DOM element, in this example .owl-carousel
        var items = event.item.count;     // Number of items
        var item = event.item.index + 1;     // Position of the current item

        // it loop is true then reset counter from 1
        if (item > items) {
            item = item - items
        }
        $('#counter').html(+item + " / " + items)
    }

    $(".edit-block").click(function () {
        $(this).parents('.adminFeedback').css("display", "none");
        $(".blockquote-editComment").css('display', 'block');
    });
});

//admin message module show user email autosuggest
$(function () {
    $("#userMailTo").autocomplete({
        source: function (request, response) {
            // Fetch data
            $.ajaxSetup({
                headers:
                        {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });
            $.ajax({
                url: 'user-email-autosuggest',
                type: "POST",
                dataType: "json",
                data: {
                    'keyword': request.term
                },
                success: function (data) {
                    response(data);
                    console.log(data);
                },
            });
        }
    });
});

   //admin message module show user email autosuggest
    $(function () {
        $(document).on('keyup', '#uniqueId', function(){
            

        $("#uniqueId").autocomplete({
            source: function (request, response) {
                // Fetch data
                $.ajaxSetup({
                    headers:
                            {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
                });
                $.ajax({
                    url: 'property-uniqueid-autosuggest',
                    type: "POST",
                    dataType: "json",
                    data: {
                        'unique_id': request.term
                    },
                    success: function (data) {
                        response(data);
                        console.log(data);
                    },
                });
            }
        });
                });
    });
