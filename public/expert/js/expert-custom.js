$(document).ready(function () {
    var baseUrl = $('meta[name="base_url"]').attr('content');
    $('.messageShow').on('click', function () {
        var value = $(this).data('filter');
        $('#filterColumn').val(value);
        $('#messageShowBy').submit();
    });

    //    expert profile update
    $('.uploadAvatar[type="file"]').fileupload({
        dataType: 'json',
        url: baseUrl + '/expert/upload_expert_photo',
        maxFileSize: (1024 * 1024 * 2),
        acceptFileTypes: /(\.|\/)(gif|bmp|jpe?g|png)$/i,
        disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator && navigator.userAgent),
        imageMaxWidth: 800,
        imageMaxHeight: 800,
        imageCrop: true,
        messages: {
            maxFileSize: "File exceeds maximum allowed size of 2MB",
            acceptFileTypes: "Only jpg, jpeg, bmp, png and gif files are allowed"
        },
        done: function (e, data) {
            $("#photoLoader").hide();
            if (data.result.photo) {
                var errors = '';
                for (var i in data.result.photo) {
                    if (data.result.photo[i].length) {
                        errors += '<div><strong>' + data.result.photo[i] + '</strong></div>';
                    }
                }
                $(".upload-response").html(errors).addClass('error');
                return false;
            }

            if (data.result.avatar) {
                $(".userPhoto").attr("src", data.result.avatar);
                $(".upload-response").hide();
            }
                $("#photoLoader").hide();
        },
        progress: function (e, data) {
            $("#photoLoader").show();

        },
        processfail: function (e, data) {
            $("#photoLoader").hide();
             swal({
                icon: "error",
                text: data.files[data.index].error
            });
            
        }
    });

    //Hamburger menu functionality
    $('.hamburgerMenu').click(function () {
        $('.wrapper').toggleClass('collapseSidebar');
    });

    //Select Show by
    $('.dropdown-menu a').click(function () {
        $('.selected').text($(this).text());
    });

    // Report page Nav Tabs
    $(".nav-tabs a").click(function () {
        $(this).tab('show');
    });


    // datepicker
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepicker').datepicker({
        minDate: today,
        format: 'dd-mm-yyyy'
    });
    // datepicker1
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepicker1').datepicker({
        maxDate: today,
        format: 'dd-mm-yyyy'
    });

    // timepicker
    $('#timepicker').timepicker({
        uiLibrary: 'bootstrap4'
    });


    //SEARCH FORM
    $(".searchForm .open-mobile-search").click(function () {
        $(this).siblings("form").addClass("searchMobile");
        $("body").css("overflow", "hidden");
    });

    $(".closeSearch").click(function () {
        $(this).parents(".searchForm form").removeClass("searchMobile");
        $("body").css("overflow", "auto");
    });

//    $(".acceptedCard").click(function () {
//        $(this).parents('.notification').css("display", "none");
//        $("#accepted-box").css("display", "block");
//    });

//    $(".closeCard").click(function () {
//        $(this).parents('.notification').css("display", "none");
//    });

    // Delivered Report
    $(".deliveredReport-carousel").owlCarousel({
        items: 5,
        dots: false,
        nav: true,
        margin: 20,
        loop: true,
        responsiveClass: true,
        onInitialized: counter, //When the plugin has initialized.
        onTranslated: counter, //When the translation of the stage has finished.
        responsive: {
            0: {
                items: 1
            },
            575: {
                items: 1
            },
            1140: {
                items: 1,
                margin: 40
            }
        }
    });
    function counter(event) {
        var element = event.target;         // DOM element, in this example .owl-carousel
        var items = event.item.count;     // Number of items
        var item = event.item.index + 1;     // Position of the current item

        // it loop is true then reset counter from 1
        if (item > items) {
            item = item - items
        }
        $('#counter').html(+item + " / " + items)
    }

    $(".edit-block").click(function () {
        $(this).parents('.adminFeedback').css("display", "none");
        $(".blockquote-editComment").css('display', 'block');
    });
});