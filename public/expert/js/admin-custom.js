$(document).ready(function(){ 
  
    //Hamburger menu functionality
    $('.hamburgerMenu').click(function(){
      $('.wrapper').toggleClass('collapseSidebar');
    }); 

    //Select Show by
    $('.dropdown-menu a').click(function(){
        $('.selected').text($(this).text());
     });

    // Report page Nav Tabs
     $(".nav-tabs a").click(function(){
        $(this).tab('show');
      });


    // datepicker
    var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#datepicker').datepicker({
        minDate: today,
        format: 'dd-mm-yyyy'
    });
    // datepicker1
    var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#datepicker1').datepicker({
        maxDate: today,
        format: 'dd-mm-yyyy'
    });

    // timepicker
    $('#timepicker').timepicker({
      uiLibrary: 'bootstrap4'
    }); 

    // Activate Floating Label - Input
    $(function() {
      $('.has-float-label .gj-unselectable').click(function() {
        $(this).next(label).focus();
      });
      $('.has-float-label .gj-unselectable').focusin(function() {
          $(this).next('label').addClass('active');
      });
      $('.has-float-label .gj-unselectable').focusout(function() {
          if ($(input).val().length <= 0) {
              $(this).next('label').removeClass('active');
            } else {
              $(this).next('label').addClass('active');
            }
      });
    });


    
  //SEARCH FORM
  $(".searchForm .open-mobile-search").click(function(){
    $(this).siblings("form").addClass("searchMobile");
    $("body").css("overflow", "hidden");
  });

  $(".closeSearch").click(function(){
    $(this).parents(".searchForm form").removeClass("searchMobile");
    $("body").css("overflow", "auto");
  });

//  $(".acceptedCard").click(function(){
//    $(this).parents('.notification').css("display", "none");
//    $("#accepted-box").css("display", "block");
//  });

//  $(".closeCard").click(function(){
//    $(this).parents('.notification').css("display", "none");
//  });

  // Delivered Report
  $(".deliveredReport-carousel").owlCarousel({
    items : 5,
    dots:false,
    nav:true,
    margin:20,
    loop:true,
    responsiveClass:true,
    onInitialized  : counter, //When the plugin has initialized.
    onTranslated : counter, //When the translation of the stage has finished.
    responsive : {
        0 : {
            items : 1 
        },
        575 : {
            items : 1
        },
        1140 : {
            items : 1,
            margin:40
        }
    }
  });
    function counter(event) {
      var element   = event.target;         // DOM element, in this example .owl-carousel
      var items     = event.item.count;     // Number of items
      var item      = event.item.index + 1;     // Position of the current item
    
    // it loop is true then reset counter from 1
    if(item > items) {
      item = item - items
    }
    $('#counter').html(+item+" / "+items)
  }

  $(".edit-block").click(function(){
    $(this).parents('.adminFeedback').css("display", "none");
    $(".blockquote-editComment").css('display', 'block');
  });
});