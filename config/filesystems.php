<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Default Filesystem Disk
      |--------------------------------------------------------------------------
      |
      | Here you may specify the default filesystem disk that should be used
      | by the framework. The "local" disk, as well as a variety of cloud
      | based disks are available to your application. Just store away!
      |
     */

    'default' => env('FILESYSTEM_DRIVER', 'local'),
    /*
      |--------------------------------------------------------------------------
      | Filesystem Disks
      |--------------------------------------------------------------------------
      |
      | Here you may configure as many filesystem "disks" as you wish, and you
      | may even configure multiple disks of the same driver. Defaults have
      | been setup for each driver as an example of the required options.
      |
      | Supported Drivers: "local", "ftp", "sftp", "s3"
      |
     */
    'disks' => [
        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],
        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],
        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
        ],
        'broker' => [
            'driver' => 'local',
            'root' => storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'broker_image'),
            'url' => env('APP_URL') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'broker_image',
            'visibility' => 'public',
        ],
        'expert' => [
            'driver' => 'local',
            'root' => storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'expert_image'),
            'url' => env('APP_URL') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'expert_image',
            'visibility' => 'public',
        ],
        'admin' => [
            'driver' => 'local',
            'root' => storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'admin_image'),
            'url' => env('APP_URL') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'admin_image',
            'visibility' => 'public',
        ],
        'user' => [
            'driver' => 'local',
            'root' => storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'user_image'),
            'url' => env('APP_URL') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'user_image',
            'visibility' => 'public',
        ],
        'pages' => [
            'driver' => 'local',
            'root' => storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'page_image'),
            'url' => env('APP_URL') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'page_image',
            'visibility' => 'public',
        ],
        'propertyImage' => [
            'driver' => 'local',
            'root' => storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'propertyimage'),
            'url' => env('APP_URL') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'propertyimage',
            'visibility' => 'public',
        ],
        'landReport' => [
            'driver' => 'local',
            'root' => storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'land_report_image'),
            'url' => env('APP_URL') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'land_report_image',
            'visibility' => 'public',
        ],
        'payments' => [
            'driver' => 'local',
            'root' => storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'payments'),
            'url' => env('APP_URL') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'payments',
            'visibility' => 'public',
        ],
        'enquiry' => [
            'driver' => 'local',
            'root' => storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'enquiry'),
            'url' => env('APP_URL') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'enquiry',
            'visibility' => 'public',
        ],
        'message' => [
            'driver' => 'local',
            'root' => storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'message'),
            'url' => env('APP_URL') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'message',
            'visibility' => 'public',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Symbolic Links
      |--------------------------------------------------------------------------
      |
      | Here you may configure the symbolic links that will be created when the
      | `storage:link` Artisan command is executed. The array keys should be
      | the locations of the links and the values should be their targets.
      |
     */
    'links' => [
        public_path('storage') => storage_path('app/public'),
    ],
];
