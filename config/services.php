<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Third Party Services
      |--------------------------------------------------------------------------
      |
      | This file is for storing the credentials for third party services such
      | as Mailgun, Postmark, AWS and more. This file provides the de facto
      | location for this type of information, allowing packages to have
      | a conventional file to locate the various service credentials.
      |
     */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],
    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],
    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    "msg91" => [
        'key' => env("Msg91_KEY"),
    ],
    'facebook' => [
        'client_id' => '308564920751090',
        'client_secret' => '5eb652b952b0fcc070f950f4f97aeb5b',
        'redirect' => 'http://exceedo.qtsolvdev.com/login/facebook/callback',
    ],
    'google' => [
        'client_id' => '655477341690-m2182nsn41rsdt9a9omh7m6kiphgjkks.apps.googleusercontent.com',
        'client_secret' => '1GqyiZ9oe9xmQndCfgB0mpYM',
        'redirect' => 'http://exceedo.qtsolvdev.com/login/google/callback',
    ],
];
