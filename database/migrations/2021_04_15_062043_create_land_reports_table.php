<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLandReportsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('land_reports', function (Blueprint $table) {
            $table->id();
            $table->string('land_name')->nullable();
            $table->integer('state_id')->nullable();
            $table->integer('value_added_service')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('expert_id')->nullable();
            $table->integer('size_of_area')->nullable();
            $table->double('amount')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('image')->nullable();
            $table->integer('ratings')->nullable();
            $table->tinyInteger('analysis_report')->default(0)->nullable();
            $table->tinyInteger('selling_more')->default(0)->nullable();
            $table->tinyInteger('terms')->default(0)->nullable();
            $table->tinyInteger('cancel')->default(0)->nullable();
            $table->tinyInteger('status')->default(0)->nullable()->comment = '0 = pending, 2 = inprogress, 1 delivered';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('land_reports');
    }

}
