<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('expert_id')->nullable();
            $table->foreignId('value_added_service_id')->nullable();
            $table->string('remarks')->nullable();
            $table->date('date')->nullable();
            $table->time('time')->nullable();
            $table->dateTime('meeting_at')->nullable();
            $table->string('payment_mode')->nullable();
            $table->tinyInteger('canceled_by')->nullable()->comment('0 = User, 1 = expert 2 = admin');
            $table->float('price')->nullable();
            $table->string('payment_id')->nullable();
            $table->tinyInteger('rescheduled_by')->nullable()->comment('0 = User, 1 = expert');
            $table->tinyInteger('terms')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('appointments');
    }

}
