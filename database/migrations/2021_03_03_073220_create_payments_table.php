<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_id');
            $table->integer('property_list_id')->nullable();
            $table->integer('appointment_id')->nullable();
            $table->integer('land_report_id')->nullable();
            $table->integer('subscription_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->double('amount');
            $table->string('currency')->nullable();
            $table->string('file')->nullable();
            $table->text('description')->nullable();
            $table->string('status')->nullable();
            $table->string('order_id')->nullable();
            $table->string('method')->nullable();
            $table->string('amount_refunded')->nullable();
            $table->string('bank')->nullable();
            $table->string('wallet')->nullable();
            $table->string('entity')->nullable();
            $table->string('refund_Date')->nullable();
            $table->string('bank_transaction_id')->nullable();
            $table->string('refund_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('payments');
    }

}
