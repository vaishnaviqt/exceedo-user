<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValueAddedServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('value_added_services', function (Blueprint $table) {
            $table->id();
            $table->string('service_name', 200);
            $table->text('description')->nullable();
            $table->float('price')->nullable();
            $table->string('service_icon',200)->nullable();
            $table->tinyInteger('show_for_broker')->default(0);
            $table->tinyInteger('is_service')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('value_added_services');
    }
}
