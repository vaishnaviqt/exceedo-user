<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyListTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('property_list', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('listed_by')->nullable();
            $table->string('mobile', 12)->nullable();
            $table->enum('listing_by', ['owner', 'broker'])->nullable();
            $table->string('land_name', 250)->nullable();
            $table->string('unique_id', 250)->nullable();
            $table->string('price_unit')->nullable();
            $table->string('size_of_land')->nullable();
            $table->text('descritpion')->nullable();
            $table->string('nearby_place')->nullable();
            $table->string('address', 250)->nullable();
            $table->text('video_link', 250)->nullable();
            $table->string('image', 250)->nullable();
            $table->enum('is_corporate', ['company', 'individual']);
            $table->string('gst')->nullable();
            $table->string('pan')->nullable();
            $table->enum('business_type', ['sale', 'collaboration', 'lease'])->nullable();
            $table->string('min_price')->nullable();
            $table->string('max_price')->nullable();
            $table->string('latitute')->nullable();
            $table->string('longitude')->nullable();
            $table->tinyInteger('featured')->default(0);
            $table->string('exclusive_channel_partner')->nullable();
            $table->string('sign_exclusive_mandate')->nullable();
            $table->string('land_to_get_verified')->nullable();
            $table->integer('est_payment')->nullable();
            $table->smallInteger('wifi')->default('0');
            $table->smallInteger('parking')->default('0');
            $table->time('session1_start_time')->nullable();
            $table->time('session1_end_time')->nullable();
            $table->time('session2_start_time')->nullable();
            $table->time('session2_end_time')->nullable();
            $table->integer('views')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('property_list');
    }

}
