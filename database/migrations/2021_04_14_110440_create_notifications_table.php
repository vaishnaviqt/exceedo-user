<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->string('notification_to')->nullable();
            $table->text('message')->nullable();
            $table->integer('apppointment_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('land_report_id')->nullable();
            $table->text('property_id')->nullable();
            $table->tinyInteger('unread')->nullable()->default(0);
            $table->tinyInteger('read')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('notifications');
    }

}
