<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        if (Permission::get()->count() == 0) {

            //Create User Permissions
            Permission::create(['name' => 'report', 'guard_name' => 'web']);
            Permission::create(['name' => 'land-listing', 'guard_name' => 'web']);
            Permission::create(['name' => 'appointment', 'guard_name' => 'web']);
        }

        if (Role::get()->count() == 0) {

            Role::create(['name' => 'admin'])->givePermissionTo(Permission::all());
            Role::create(['name' => 'expert']);
            Role::create(['name' => 'broker']);
            Role::create(['name' => 'user']);

        }
    }
}
