<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ValueAddedServicesTableSeeder extends Seeder {

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run() {


        \DB::table('value_added_services')->delete();

        \DB::table('value_added_services')->insert(array(
            0 =>
            array(
                'service_name' => 'Land Title Search',
                'description' => NULL,
                'price' => 500,
                'service_icon' => NULL,
                'show_for_broker' => 0,
                'is_service' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 =>
            array(
                'service_name' => 'Feasibility Analysis',
                'description' => NULL,
                'service_icon' => NULL,
                'price' => 400,
                'show_for_broker' => 0,
                'is_service' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 =>
            array(
                'service_name' => 'Buy your Land',
                'description' => NULL,
                'service_icon' => NULL,
                'price' => 700,
                'show_for_broker' => 1,
                'is_service' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 =>
            array(
                'service_name' => 'Sell your Land',
                'description' => NULL,
                'service_icon' => NULL,
                'price' => 600,
                'show_for_broker' => 1,
                'is_service' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 =>
            array(
                'service_name' => 'Surveying',
                'description' => NULL,
                'service_icon' => 'storage/uploads/valueservices/Surveying-icon.svg',
                'price' => 600,
                'show_for_broker' => 1,
                'is_service' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 =>
            array(
                'service_name' => 'Master Planning',
                'description' => NULL,
                'service_icon' => 'storage/uploads/valueservices/Master-Planning-icon.svg',
                'price' => 600,
                'show_for_broker' => 1,
                'is_service' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 =>
            array(
                'service_name' => 'Architectural Services',
                'description' => NULL,
                'service_icon' => 'storage/uploads/valueservices/Architectural-Services-icon.svg',
                'price' => 600,
                'show_for_broker' => 1,
                'is_service' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 =>
            array(
                'service_name' => 'Legal Advisory',
                'description' => NULL,
                'service_icon' => 'storage/uploads/valueservices/Legal-Advisory-icon.svg',
                'price' => 600,
                'show_for_broker' => 1,
                'is_service' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
    }

}
