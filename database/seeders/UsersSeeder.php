<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //

        if (User::get()->count() == 0) {

            $admUser = User::create([
                        'name' => 'Exceedo Admin',
                        'email' => 'admin@admin.com',
                        'mobile' => '',
                        'email_verified_at' => now(),
                        'password' => Hash::make('exceedo123'),
                        'remember_token' => Str::random(10),
            ]);

            $admUser->assignRole('admin');

            $borkerUser = User::create([
                        'name' => 'Exceedo Broker',
                        'email' => 'broker@admin.com',
                        'mobile' => 9649959788,
                        'email_verified_at' => now(),
                        'password' => Hash::make('broker123'),
                        'remember_token' => Str::random(10),
            ]);

            $borkerUser->assignRole('broker');

            $expertUser1 = User::create([
                        'name' => 'Exceedo Expert1',
                        'email' => 'expert1@admin.com',
                        'mobile' => '',
                        'email_verified_at' => now(),
                        'password' => Hash::make('expert123'),
                        'remember_token' => Str::random(10),
            ]);

            $expertUser1->assignRole('expert');


            $expertUser2 = User::create([
                        'name' => 'Exceedo Expert2',
                        'email' => 'expert2@admin.com',
                        'mobile' => '',
                        'email_verified_at' => now(),
                        'password' => Hash::make('expert123'),
                        'remember_token' => Str::random(10),
            ]);

            $expertUser2->assignRole('expert');

            $expertUser3 = User::create([
                        'name' => 'Exceedo Expert3',
                        'email' => 'expert3@admin.com',
                        'mobile' => 9649959787,
                        'email_verified_at' => now(),
                        'password' => Hash::make('expert123'),
                        'remember_token' => Str::random(10),
            ]);

            $expertUser3->assignRole('expert');

            $expertUser4 = User::create([
                        'name' => 'Exceedo Expert4',
                        'email' => 'expert4@admin.com',
                        'mobile' => 9649959787,
                        'email_verified_at' => now(),
                        'password' => Hash::make('expert123'),
                        'remember_token' => Str::random(10),
            ]);

            $expertUser4->assignRole('expert');

            $expertUser5 = User::create([
                        'name' => 'Exceedo Expert5',
                        'email' => 'expert5@admin.com',
                        'mobile' => 9649959787,
                        'email_verified_at' => now(),
                        'password' => Hash::make('expert123'),
                        'remember_token' => Str::random(10),
            ]);

            $expertUser5->assignRole('expert');

            $expertUser6 = User::create([
                        'name' => 'Exceedo Expert6',
                        'email' => 'expert6@admin.com',
                        'mobile' => 9649959787,
                        'email_verified_at' => now(),
                        'password' => Hash::make('expert123'),
                        'remember_token' => Str::random(10),
            ]);

            $expertUser6->assignRole('expert');

            $expertUser7 = User::create([
                        'name' => 'Exceedo Expert7',
                        'email' => 'expert7@admin.com',
                        'mobile' => 9649959787,
                        'email_verified_at' => now(),
                        'password' => Hash::make('expert123'),
                        'remember_token' => Str::random(10),
            ]);

            $expertUser7->assignRole('expert');

            $expertUser8 = User::create([
                        'name' => 'Exceedo Expert8',
                        'email' => 'expert8@admin.com',
                        'mobile' => '',
                        'email_verified_at' => now(),
                        'password' => Hash::make('expert123'),
                        'remember_token' => Str::random(10),
            ]);

            $expertUser8->assignRole('expert');
            $expertUser9 = User::create([
                        'name' => 'Exceedo User',
                        'email' => 'user@admin.com',
                        'mobile' => 9649959789,
                        'email_verified_at' => now(),
                        'password' => Hash::make('expert123'),
                        'remember_token' => Str::random(10),
            ]);

            $expertUser9->assignRole('user');
        }
    }

}
