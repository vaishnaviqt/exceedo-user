<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $package=[
        ['title' => 'Basic', 'price'=> 500, 'show_land'=>10, 'unlimited' => 0, 'status' => 1],
        ['title' => 'Pro', 'price'=> 800, 'show_land'=> NULL, 'unlimited' => 1, 'status' => 1],
       
        ];

        DB::table('packages')->insert($package);
    }
}
