<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('login/user/{provider}', [App\Http\Controllers\UserController::class, 'userLogin']);
Route::get('login/broker/{provider}', [App\Http\Controllers\UserController::class, 'brokerLogin']);
Route::get('login/expert/{provider}', [App\Http\Controllers\UserController::class, 'expertLogin']);

Route::get('login/{provider}/callback', [App\Http\Controllers\UserController::class, 'handleProviderCallback']);

Route::post('/signupOtp', [App\Http\Controllers\UserController::class, 'SendOtp'])->name('signupOtp');
Route::post('/userSignup', [App\Http\Controllers\UserController::class, 'userSignup'])->name('userSignup');

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/buy', [App\Http\Controllers\HomeController::class, 'showBuy'])->name('buy');
Route::post('/sendemail', [App\Http\Controllers\HomeController::class, 'sendEmail'])->name('sendemail');
Route::any('/buy-your-land', [App\Http\Controllers\HomeController::class, 'showBuyYourLand'])->name('buyyourland');
Route::get('/sell-your-land', [App\Http\Controllers\HomeController::class, 'showSellYourLand'])->name('sellyourland');
Route::get('/feasibility-analysis', [App\Http\Controllers\HomeController::class, 'showFeasibilityAnalysis'])->name('feasibilityanalysis');
Route::get('/land-title-search', [App\Http\Controllers\HomeController::class, 'showLandTitleSearch'])->name('landtitlesearch');
Route::get('/aboutus', [App\Http\Controllers\HomeController::class, 'showAboutUs'])->name('aboutus');

Route::get('/meet-the-expert', [App\Http\Controllers\HomeController::class, 'ShowMeetTheExpert'])->name('meettheexpert');
Route::get('/terms-conditions', [App\Http\Controllers\HomeController::class, 'ShowTermsConditions'])->name('termsconditions');
Route::get('/privacy-policy', [App\Http\Controllers\HomeController::class, 'ShowPrivacyPolicy'])->name('privacypolicy');
Route::get('/cookie-policy', [App\Http\Controllers\HomeController::class, 'ShowCookiePolicy'])->name('cookiepolicy');
Route::get('/land-report-anlysis-form', [App\Http\Controllers\HomeController::class, 'ShowLandReportAnlysisForm'])->name('landreportanlysisform');
Route::any('/land-title-search-form', [App\Http\Controllers\HomeController::class, 'ShowLandTitleSearchForm'])->name('landtitlesearchform');
Route::get('/book-your-appointment', [App\Http\Controllers\HomeController::class, 'ShowBookYourAppointment'])->name('bookyourappointment');
Route::get('/login', [App\Http\Controllers\UserController::class, 'showLogin'])->name('login');
Route::post('/storeexpert', [App\Http\Controllers\UserController::class, 'storeExpert'])->name('storeexpert');
Route::post('/storebroker', [App\Http\Controllers\UserController::class, 'storeBroker'])->name('storebroker');
// sell lend routing
Route::get('/sell-your-land', [App\Http\Controllers\HomeController::class, 'sellland'])->name('sellland');
Route::post('/sell-your-land-form', [App\Http\Controllers\HomeController::class, 'selllandform'])->name('selllandform');
Route::post('/store_property', [App\Http\Controllers\HomeController::class, 'storeproperty'])->name('store_property');
Route::get('/sign-up', [App\Http\Controllers\UserController::class, 'showSignUp'])->name('signup');

//03-05-2021
Route::post('/store_property_validation', [App\Http\Controllers\HomeController::class, 'storePropertyValidation']);

Route::post('/store-land-appointment', [App\Http\Controllers\HomeController::class, 'LandAppointmentStore'])->name('store-land-appointment');
Route::post('/store_appointment_validation', [App\Http\Controllers\HomeController::class, 'LandAppointmentValidition']);
// by vaishnavi
Route::post('/store-services', [App\Http\Controllers\HomeController::class, 'storeServices'])->name('store-services');
//by vaishnavi 25/03/21
Route::post('/agent-contacts', [App\Http\Controllers\HomeController::class, 'agentContact']);
//23-04-2021
Route::post('/generate-land-report-payment-token', [App\Http\Controllers\HomeController::class, 'paymentTokenGenerate']);

Route::get('/admin', function () {
    if (Auth::check()) {
        return redirect()->route('admin.dashboard');
    }
    return view('admin/login');
})->name('admin-login');

Route::post('/admin/adminlogin', [App\Http\Controllers\admin\AdminLoginController::class, 'doAdminLogin'])->name('adminlogin');
// send OTP to user
Route::post('/sendOtp', [App\Http\Controllers\UserController::class, 'doSendOtp'])->name('sendOtp');
Route::post('/login', [App\Http\Controllers\UserController::class, 'doLogin'])->name('login');
/* Route::get('/admin/dashboard', function() {
  return view('admin/dashboard');
  })->name('admin.dashboard'); */

Route::prefix('admin')->middleware(['auth', 'role:admin'])->name('admin.')->group(function () {

    Route::any('/dashboard', [App\Http\Controllers\admin\DashboardController::class, 'index'])->name('dashboard');

    Route::any('/experts/{search?}', [App\Http\Controllers\admin\ExpertController::class, 'index'])->name('experts');
    Route::any('/value-added-services/{search?}', [App\Http\Controllers\admin\ValueAddedServiceController::class, 'valueAddedServices'])->name('value-added-services');

    Route::any('/pendingexperts', [App\Http\Controllers\admin\ExpertController::class, 'showPendingExperts'])->name('showpendingexperts');

    Route::post('/delete-pendingexpert', [App\Http\Controllers\admin\ExpertController::class, 'destroyPendingExperts'])->name('delete-pendingexpert');

    Route::get('/get-expert/{id}', [App\Http\Controllers\admin\ExpertController::class, 'edit'])->name('get-expert');

    Route::post('/update-expert/', [App\Http\Controllers\admin\ExpertController::class, 'update'])->name('update-expert');
    Route::get('/approve-expert/{id}', [App\Http\Controllers\admin\ExpertController::class, 'approveExpert'])->name('approve-expert');

    Route::get('/addvalue', function () {
        return view('admin/addvalue');
    })->name('addvalue');

    Route::resource('valueservices', App\Http\Controllers\admin\ValueAddedServiceController::class);
    Route::post('/assign-expert-popup', [App\Http\Controllers\admin\ReportController::class, 'getExpertPopup'])->name('reports');
    Route::any('/appointments', [App\Http\Controllers\admin\AppointmentController::class, 'index'])->name('appointments');
    Route::any('/reports', [App\Http\Controllers\admin\ReportController::class, 'index'])->name('reports');
    // by vaishnavi 
    Route::any('/land-listing', [App\Http\Controllers\admin\LandListingController::class, 'index'])->name('land-listing');

    Route::any('/payments', [App\Http\Controllers\admin\PaymentController::class, 'index'])->name('payments');
    // 27-04-2021
    Route::post('/user-payment-store', [App\Http\Controllers\admin\PaymentController::class, 'userPaymentStore']);
    Route::post('/user-name-autosuggest', [App\Http\Controllers\admin\PaymentController::class, 'userNameAutoSuggest'])->name('user-name-autosuggest');
    Route::any('/rating-and-review', [App\Http\Controllers\admin\RatingReviewController::class, 'index'])->name('rating-and-review');
    Route::any('/messages', [App\Http\Controllers\admin\MessageController::class, 'index'])->name('messages');
    Route::get('/your-profile', [App\Http\Controllers\admin\ProfileController::class, 'index'])->name('your-profile');

//        16-03-2021
    Route::post('/user-email-autosuggest', [App\Http\Controllers\admin\MessageController::class, 'userEmailAutoSuggest']);
    Route::post('/messages-send', [App\Http\Controllers\admin\MessageController::class, 'messageSend']);

//    18-03-2021
    Route::post('/profile-update', [App\Http\Controllers\admin\ProfileController::class, 'profileUpdate']);
    Route::post('/upload_admin_photo', [App\Http\Controllers\admin\ProfileController::class, 'profilePhotoUpload']);

//    24-03-2021
    Route::post('/assign-to-expert', [App\Http\Controllers\admin\AppointmentController::class, 'assignToExpert']);

//    19-03-2021
    Route::any('/pages', [App\Http\Controllers\admin\PageController::class, 'index']);
    Route::get('/page-create', [App\Http\Controllers\admin\PageController::class, 'create']);
    Route::post('/page-store', [App\Http\Controllers\admin\PageController::class, 'store']);
    Route::post('/imageUpload', [App\Http\Controllers\admin\PageController::class, 'imageUpload']);
    Route::get('/edit-page/{slug}', [App\Http\Controllers\admin\PageController::class, 'editPage']);
    Route::post('/update-page', [App\Http\Controllers\admin\PageController::class, 'updatePage']);
    Route::get('/delete-page/{id}', [App\Http\Controllers\admin\PageController::class, 'deletePage']);

//    23-03-2021
    Route::get('/appointment-details', [App\Http\Controllers\admin\AppointmentController::class, 'appointmentDetails']);

//    25-03-2021
    Route::any('/active-land-listing', [App\Http\Controllers\admin\LandListingController::class, 'activeLandListing']);
    Route::any('/inactive-land-listing', [App\Http\Controllers\admin\LandListingController::class, 'inActiveListing']);
    Route::any('/request-land-listing', [App\Http\Controllers\admin\LandListingController::class, 'requestListing']);

    Route::get('/adminlogout', [App\Http\Controllers\admin\AdminLoginController::class, 'doAdminLogout'])->name('adminlogout');

//    26-03-2021
    Route::post('/view-all-appointment', [App\Http\Controllers\admin\AppointmentController::class, 'viewAllAppointment']);
    Route::post('/view-pending-appointment', [App\Http\Controllers\admin\AppointmentController::class, 'viewPendingAppointment']);
    Route::post('/view-done-appointment', [App\Http\Controllers\admin\AppointmentController::class, 'viewDoneppointment']);

//    05-04-2021
    Route::post('/get-city-list', [App\Http\Controllers\admin\ExpertController::class, 'getCityList']);
    // 06-04-2021
    Route::any('/users/{search?}', [App\Http\Controllers\admin\UserController::class, 'index'])->name('users');
    Route::post('/delete-user', [App\Http\Controllers\admin\UserController::class, 'destroyUsers'])->name('delete-user');
    Route::post('/storeuser', [App\Http\Controllers\admin\UserController::class, 'storeUser'])->name('storeuser');
//  28-05-2021
    Route::post('/delete-admin', [App\Http\Controllers\admin\SecondaryAdminController::class, 'destroySecondaryAdmin'])->name('delete-admin');
    Route::post('/storesecondaryadmin', [App\Http\Controllers\admin\SecondaryAdminController::class, 'storeAdmin'])->name('storesecondaryadmin');

// 07-04-2021    
    Route::any('/brokers/{search?}', [App\Http\Controllers\admin\BrokerController::class, 'index'])->name('brokers');
    Route::post('/delete-broker', [App\Http\Controllers\admin\BrokerController::class, 'destroyBrokers'])->name('delete-broker');
    Route::post('/storebroker', [App\Http\Controllers\admin\BrokerController::class, 'storeBroker'])->name('storebroker');

//    06-04-2021
    Route::post('/add-appointment', [App\Http\Controllers\admin\AppointmentController::class, 'addAppointment']);
    Route::post('/show-all-expert', [App\Http\Controllers\admin\AppointmentController::class, 'showAllExpert']);
    Route::get('/cancel-appointment/{id}', [App\Http\Controllers\admin\AppointmentController::class, 'cancelAppoint']);

//    07-04-2021
    Route::get('/show-agent-mobile/{id}', [App\Http\Controllers\admin\LandListingController::class, 'showAgentMobile']);
    Route::post('/property-detail-store', [App\Http\Controllers\admin\LandListingController::class, 'propertyStore']);

//    08-04-2021
    Route::post('/property-detail-edit', [App\Http\Controllers\admin\LandListingController::class, 'propertyEdit']);
    Route::get('/deleteland/{id}', [App\Http\Controllers\admin\LandListingController::class, 'propertyDelete']);
    Route::post('/property-detail-update', [App\Http\Controllers\admin\LandListingController::class, 'propertyUpdate']);
    //    14-04-2021
    Route::get('/mark-read-notification/{id}', [App\Http\Controllers\admin\AppointmentController::class, 'markReadNotification']);

//    19-04-2021
    Route::any('/delivered-reports', [App\Http\Controllers\admin\ReportController::class, 'deliveredReport']);
    Route::any('/inprogress-reports', [App\Http\Controllers\admin\ReportController::class, 'inprogressReport']);
    Route::any('/pending-reports', [App\Http\Controllers\admin\ReportController::class, 'pendingReport']);

//    21-04-2021
     Route::post('/report-validation', [App\Http\Controllers\admin\ReportController::class, 'ReportValidation']);
    Route::post('/report-assing-expert', [App\Http\Controllers\admin\ReportController::class, 'reportAssignExpert']);
    Route::post('/report-delivered', [App\Http\Controllers\admin\ReportController::class, 'reportDelivered']);

//    26-04-2021
    Route::post('/report-feedback', [App\Http\Controllers\admin\ReportController::class, 'reportFeedback']);

//    27-04-2021
    Route::any('/expire-land-listing', [App\Http\Controllers\admin\LandListingController::class, 'expireLandListing']);
    Route::post('/property-resubmit', [App\Http\Controllers\admin\LandListingController::class, 'propertyResbmit']);

//    30-04-2021
    Route::post('/property-uniqueid-autosuggest', [App\Http\Controllers\admin\LandListingController::class, 'propertyUniqueId']);
//    10-05-2021
    Route::any('/payment-made', [App\Http\Controllers\admin\PaymentController::class, 'paymentMade']);

//   17-05-2021
    Route::post('/enquiry-assing-expert', [App\Http\Controllers\admin\ServiceEnquiryController::class, 'enquiryAssignExpert']);
    Route::any('/service-enquiry', [App\Http\Controllers\admin\ServiceEnquiryController::class, 'index'])->name('service-enquiry');
    // by shivani
    Route::any('/edit-valueservice/{id}', 'App\Http\Controllers\admin\ValueAddedServiceController@edit')->name('edit-valueservice');
    Route::post('/update-valueservice', 'App\Http\Controllers\admin\ValueAddedServiceController@update')->name('update-valueservice');

    Route::post('/add-valueservice', 'App\Http\Controllers\admin\ValueAddedServiceController@store')->name('update-valueservice');
    
    Route::post('/invoice_load', [App\Http\Controllers\admin\PaymentController::class, 'invoice_upload'])->name('invoice_load');
    Route::get('/download/{id}', [App\Http\Controllers\admin\PaymentController::class, 'download'])->name('download');
    Route::get('/delete_valueservice/{id}', [App\Http\Controllers\admin\ValueAddedServiceController::class, 'delete_valueservice'])->name('delete_valueservice');
    Route::get('/user-status', 'App\Http\Controllers\admin\ExpertController@userstatus')->name('user-status');
    Route::get('/broker-status', 'App\Http\Controllers\admin\BrokerController@userstatus')->name('broker-status');
    Route::get('/users-status', 'App\Http\Controllers\admin\UserController@userstatus')->name('users-status');
    
//    30-06-2021
    Route::post('/add-secondary-admin', 'App\Http\Controllers\admin\UserController@secondaryAdmin');
    Route::post('/editsecondaryAdmin', 'App\Http\Controllers\admin\UserController@editSecondaryAdmin');
    Route::get('/delete-secondary-admin/{id}', 'App\Http\Controllers\admin\UserController@deleteSecondaryAdmin');
});

Route::prefix('expert')->middleware(['auth', 'role:expert'])->name('expert.')->group(function () {
    Route::any('/appointments', [App\Http\Controllers\expert\AppointmentController::class, 'index'])->name('appointments');
    Route::any('/projects', [App\Http\Controllers\expert\ProjectController::class, 'index'])->name('projects');
    // 18/05/2021
    Route::any('/enquiry', [App\Http\Controllers\expert\ServiceEnquiryController::class, 'index'])->name('enquiry');
    Route::get('/accept-enquiry/{id}', [App\Http\Controllers\expert\ServiceEnquiryController::class, 'acceptEnquiry']);
    Route::get('/cancel-enquiry/{id}', [App\Http\Controllers\expert\ServiceEnquiryController::class, 'cancelEnquiry']);

    Route::any('/payments', [App\Http\Controllers\expert\PaymentController::class, 'index'])->name('payments');
    Route::any('/reviews', [App\Http\Controllers\expert\ReviewController::class, 'index'])->name('reviews');
    Route::any('/messages', [App\Http\Controllers\expert\MessageController::class, 'index'])->name('messages');
    Route::get('/profile', [App\Http\Controllers\expert\ProfileController::class, 'index'])->name('profile');
    Route::get('/expertlogout', [App\Http\Controllers\UserController::class, 'doExpertLogout'])->name('expertlogout');

    Route::post('/expert-messages-send', [App\Http\Controllers\expert\MessageController::class, 'messageSend']);
    //    17-03-2021
    Route::post('/profile', [App\Http\Controllers\expert\ProfileController::class, 'profileUpdate']);
    Route::post('/upload_expert_photo', [App\Http\Controllers\expert\ProfileController::class, 'profilePhotoUpload']);

//    25-03-2021
    Route::any('/appointment-details', [App\Http\Controllers\expert\AppointmentController::class, 'appointmentDetails']);
    Route::get('/confirm-appointment/{id}', [App\Http\Controllers\expert\AppointmentController::class, 'appointmentConfirm']);

//    30/03/2021
    Route::post('/view-all-appointment', [App\Http\Controllers\expert\AppointmentController::class, 'viewAllAppointment']);
    Route::post('/view-pending-appointment', [App\Http\Controllers\expert\AppointmentController::class, 'viewPendingAppointment']);
    Route::post('/view-done-appointment', [App\Http\Controllers\expert\AppointmentController::class, 'viewDoneppointment']);
    Route::post('/reschedule-appointment', [App\Http\Controllers\expert\AppointmentController::class, 'viewRescheduleppointment']);

    //    14-04-2021
    Route::get('/mark-read-notification/{id}', [App\Http\Controllers\expert\AppointmentController::class, 'markReadNotification']);

//    20-04-2021
    Route::any('/delivered-projects', [App\Http\Controllers\expert\ProjectController::class, 'deliveredProject']);
    Route::any('/inprogress-projects', [App\Http\Controllers\expert\ProjectController::class, 'inprogressProject']);
    Route::any('/pending-projects', [App\Http\Controllers\expert\ProjectController::class, 'pendingProject']);
    Route::get('/accept-project/{id}', [App\Http\Controllers\expert\ProjectController::class, 'acceptProject']);
    Route::get('/cancel-project/{id}', [App\Http\Controllers\expert\ProjectController::class, 'cancelProject']);

    Route::post('/project-report-upload', [App\Http\Controllers\expert\ProjectController::class, 'reportUpload']);
    //12/05/2021
    Route::any('/your-land-listing', [App\Http\Controllers\expert\LandListingController::class, 'index'])->name('your-land-listing');
    Route::any('/your-land-listing-active', [App\Http\Controllers\expert\LandListingController::class, 'YourLandListingActive'])->name('your-land-listing-active');
    Route::any('/your-land-listing-inactive', [App\Http\Controllers\expert\LandListingController::class, 'YourLandListingInactive'])->name('your-land-listing-inactive');
    Route::any('/your-land-listing-deleted', [App\Http\Controllers\expert\LandListingController::class, 'YourLandListingDeleted'])->name('your-land-listing-deleted');
    Route::post('/user-property-store', [App\Http\Controllers\expert\LandListingController::class, 'userPropertyStore']);

    Route::post('/store_property_validation', [App\Http\Controllers\expert\LandListingController::class, 'userPropertyValidation']);

//    20-05-2021
    Route::any('/made-payments', [App\Http\Controllers\expert\PaymentController::class, 'madePayments'])->name('made-payments');
    Route::any('/service-enquiry', [App\Http\Controllers\expert\ServiceEnquiryController::class, 'serviceInqury']);
    Route::any('/accept-enquiry', [App\Http\Controllers\expert\ServiceEnquiryController::class, 'acceptEnquiry']);
    Route::any('/cancel-enquiry', [App\Http\Controllers\expert\ServiceEnquiryController::class, 'cancelEnquiry']);
});

Route::prefix('broker')->middleware(['auth', 'role:broker'])->name('broker.')->group(function () {
    Route::get('/appointments', [App\Http\Controllers\broker\AppointmentController::class, 'index'])->name('appointments');
    Route::any('/projects', [App\Http\Controllers\broker\ProjectController::class, 'index'])->name('projects');
//  11-05-2021
    Route::any('/payments', [App\Http\Controllers\broker\PaymentController::class, 'index'])->name('payments');
    Route::any('/made-payments', [App\Http\Controllers\broker\PaymentController::class, 'madePayments'])->name('made-payments');
    Route::get('/reviews', [App\Http\Controllers\broker\ReviewController::class, 'index'])->name('reviews');
    Route::any('/contacts', [App\Http\Controllers\broker\ContactController::class, 'index'])->name('contacts');
    Route::any('/messages', [App\Http\Controllers\broker\MessageController::class, 'index'])->name('messages');
    Route::get('/profile', [App\Http\Controllers\broker\ProfileController::class, 'index'])->name('profile');
    Route::get('/brokerlogout', [App\Http\Controllers\UserController::class, 'doBrokerLogout'])->name('brokerlogout');
    Route::post('/message-send', [App\Http\Controllers\broker\MessageController::class, 'messageSend']);
//  26-04-2021
    Route::any('/projects', [App\Http\Controllers\broker\ProjectController::class, 'index'])->name('projects');
    Route::any('/active-projects', [App\Http\Controllers\broker\ProjectController::class, 'YourLandListingActive'])->name('active-projects');
    Route::any('/inactive-projects', [App\Http\Controllers\broker\ProjectController::class, 'YourLandListingInactive'])->name('inactive-projects');
    Route::any('/deleted-projects', [App\Http\Controllers\broker\ProjectController::class, 'YourLandListingDeleted'])->name('deleted-projects');

//    17-03-2021
    Route::post('/profile', [App\Http\Controllers\broker\ProfileController::class, 'profileUpdate']);
    Route::post('/upload_broker_photo', [App\Http\Controllers\broker\ProfileController::class, 'profilePhotoUpload']);
//   12-05-2021  route for land report
    Route::any('/your-land-report', [App\Http\Controllers\broker\LandReportController::class, 'index'])->name('your-land-report');
    Route::any('/your-land-report-delivered', [App\Http\Controllers\broker\LandReportController::class, 'YourLandReportDelivered'])->name('your-land-report-delivered');
    Route::any('/your-land-report-inprogress', [App\Http\Controllers\broker\LandReportController::class, 'YourLandReportInprogress'])->name('your-land-report-inprogress');
    Route::any('/your-land-report-pending', [App\Http\Controllers\broker\LandReportController::class, 'YourLandReportPending'])->name('your-land-report-pending');
    Route::post('/store_property_validation', [App\Http\Controllers\broker\ProjectController::class, 'userPropertyValidation']);
    Route::post('/user-property-store', [App\Http\Controllers\broker\ProjectController::class, 'userPropertyStore']);
});

Route::prefix('user')->middleware(['auth', 'role:user'])->name('user.')->group(function () {
    Route::any('/dashboard/{search?}', [App\Http\Controllers\user\DashboardController::class, 'index'])->name('dashboard');
    Route::any('/your-land-report', [App\Http\Controllers\user\LandReportController::class, 'index'])->name('your-land-report');
    Route::any('/your-land-report-delivered', [App\Http\Controllers\user\LandReportController::class, 'YourLandReportDelivered'])->name('your-land-report-delivered');
    Route::any('/your-land-report-inprogress', [App\Http\Controllers\user\LandReportController::class, 'YourLandReportInprogress'])->name('your-land-report-inprogress');
    Route::any('/your-land-report-pending', [App\Http\Controllers\user\LandReportController::class, 'YourLandReportPending'])->name('your-land-report-pending');

    Route::get('/appointments', [App\Http\Controllers\user\AppointmentController::class, 'index'])->name('appointments');
    Route::any('/your-land-listing', [App\Http\Controllers\user\LandListingController::class, 'index'])->name('your-land-listing');
    Route::any('/your-land-listing-active', [App\Http\Controllers\user\LandListingController::class, 'YourLandListingActive'])->name('your-land-listing-active');
    Route::any('/your-land-listing-inactive', [App\Http\Controllers\user\LandListingController::class, 'YourLandListingInactive'])->name('your-land-listing-inactive');
    Route::any('/your-land-listing-deleted', [App\Http\Controllers\user\LandListingController::class, 'YourLandListingDeleted'])->name('your-land-listing-deleted');

    Route::any('/payments', [App\Http\Controllers\user\PaymentController::class, 'index'])->name('payments');
    Route::get('/profile', [App\Http\Controllers\user\ProfileController::class, 'index'])->name('profile');

    Route::get('/userlogout', [App\Http\Controllers\UserController::class, 'doUserLogout'])->name('userlogout');

//    18-03-2021
    Route::post('/profile', [App\Http\Controllers\user\ProfileController::class, 'profileUpdate']);
    Route::post('/upload_user_photo', [App\Http\Controllers\user\ProfileController::class, 'profilePhotoUpload']);

//    23-03-2021
    Route::post('add-appointment', [App\Http\Controllers\user\AppointmentController::class, 'addAppointment']);
    Route::get('/appointment-details', [App\Http\Controllers\user\AppointmentController::class, 'appointmentDetails']);

//    30-03-2021
    Route::post('/view-all-appointment', [App\Http\Controllers\user\AppointmentController::class, 'viewAllAppointment']);
    Route::post('/view-pending-appointment', [App\Http\Controllers\user\AppointmentController::class, 'viewPendingAppointment']);
    Route::post('/view-done-appointment', [App\Http\Controllers\user\AppointmentController::class, 'viewDoneppointment']);
    Route::post('/reschedule-appointment', [App\Http\Controllers\user\AppointmentController::class, 'viewRescheduleppointment']);
    Route::get('/confirm-appointment/{id}', [App\Http\Controllers\user\AppointmentController::class, 'appointmentConfirm']);
    Route::post('/reschedule-appointment', [App\Http\Controllers\user\AppointmentController::class, 'rescheduleppointment']);
//    06-04-2021
    Route::get('/cancel-appointment/{id}', [App\Http\Controllers\user\AppointmentController::class, 'cancelAppoint']);
    //  12-04-2021 by vaishnavi    
    Route::post('/user-property-store', [App\Http\Controllers\user\LandListingController::class, 'userPropertyStore']);
    // 14-05-2021
    Route::post('/store_property_validation', [App\Http\Controllers\user\LandListingController::class, 'userPropertyValidation']);
//    12-04-2021
    Route::post('/add-appointment-validate', [App\Http\Controllers\user\AppointmentController::class, 'addAppointmentValidate']);
//   15-04-2021
    Route::post('/store-reviews', [App\Http\Controllers\user\AppointmentController::class, 'storeReviews']);
//    14-04-2021
    Route::get('/mark-read-notification/{id}', [App\Http\Controllers\user\AppointmentController::class, 'markReadNotification']);
});

//16-03-2021
Route::post('property-detail', [App\Http\Controllers\HomeController::class, 'propertyDetail']);

//15-04-2021
Route::post('land-report-validation', [App\Http\Controllers\HomeController::class, 'landReportValidation']);
Route::post('store-land-report-detail', [App\Http\Controllers\HomeController::class, 'landReportStore']);

//04-05-2021
Route::get('user-subscription', [App\Http\Controllers\HomeController::class, 'userSubscription']);

//05-05-2021
Route::post('user-subscription-token-generate', [App\Http\Controllers\HomeController::class, 'userSubscriptionTokenGenerate']);
Route::post('user-subscription-payment', [App\Http\Controllers\HomeController::class, 'userSubscriptionPayment']);

