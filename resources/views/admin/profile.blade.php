@extends('admin.layouts.master')
@section('title')
Your Profile
@endsection
@section('class')
paymentsBody
@endsection
@section('content')
@include('flash-message')

<div class="pageTitle d-flex align-items-center">
    <h1>Your Profile</h1>
</div><!--//pageTitle-->

<div class="profileSection mb-5">
    <div class="profileUserImg">
        <img src="{{asset('images/loader.gif')}}" id="photoLoader">
        @php
        $path = asset('broker/images/profile-pic.png');
        if(Storage::disk('admin')->exists(str_replace('admin_image', '', $admin->photo)))
        {
        $path = Storage::disk('admin')->url(str_replace('admin_image/', '', $admin->photo));
        }
        @endphp
        <img class="userPhoto" src="{{ $path }}" alt="{{ $admin->name }}" />
        <div class="cameraIcon">
            <form>
                @csrf
                <button class="d-none"><i class="fas fa-camera-retro"></i></button>
                <input class="uploadAvatar" type="file" name="photo" />
            </form>
        </div>
    </div><!--//profileUserImg-->
    <div class="profileUserDetail">
        <div class="row mb-3">
            <div class="col-lg-3 col-4">
                <span>Name</span>
            </div>
            <div class="col">
                {{ $admin->name }}
            </div>
        </div><!--//row-->

        <div class="row mb-3">
            <div class="col-lg-3 col-4">
                <span>Mobile Number </span>
            </div>
            <div class="col">
                {{ $admin->mobile }}
            </div>
        </div><!--//row-->

        <div class="row">
            <div class="col-lg-3 col-4">
                <span>Email id</span>
            </div>
            <div class="col">
                {{ $admin->email }}
            </div>
        </div><!--//row-->
    </div><!--//profileUserDetail-->

    <div class="editProfileBtn">
        <a href="javascript:void(0);" class="btn btn-primary editprofileButton" data-toggle="modal" data-target="#editProfile">Edit your Profile</a>
    </div>
</div><!--//profileSection-->
@if(auth()->user()->id == 1)
<h2>Secondary Admin</h2>
@if (count($allAdmins) > 0)
@foreach($allAdmins as $admin)

<div class="profileSection">
    <div class="profileUserImg">
        <img src="{{asset('broker/images/profile-pic.png')}}" alt="" />
<!--        <div class="cameraIcon">
            <input type="file" />
        </div>-->
    </div><!--//profileUserImg-->
    <div class="profileUserDetail">
        <div class="row mb-3">
            <div class="col-lg-3 col-4">
                <span>Name</span>
            </div>
            <div class="col">
                {{ ucwords($admin->user->name) }}
            </div>
        </div><!--//row-->

        <div class="row mb-3">
            <div class="col-lg-3 col-4">
                <span>Mobile Number </span>
            </div>
            <div class="col">
                {{ $admin->user->mobile }}
            </div>
        </div><!--//row-->

        <div class="row">
            <div class="col-lg-3 col-4">
                <span>Email id</span>
            </div>
            <div class="col">
                {{ $admin->user->email }}
            </div>
        </div><!--//row-->
    </div><!--//profileUserDetail-->

    <a href="javascript:void(0);" onclick="editSecondaryAdmin({{$admin->model_id}})" class="secondaryAdmin">
        Edit
    </a> 

    <div class="delProfileBtn">
        <a href="{{ url('admin/delete-secondary-admin/'.$admin->model_id) }}"></a>
    </div> 
</div><!--//profileSection-->
@endforeach
@else
<h1> No Data Found </h1>
@endif  
@endif

@endsection
@section('modal')
<div class="modal fade w-400" id="editProfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="popupForm">
                    <form action="{{ url('admin/profile-update') }}" method="post">
                        @csrf
                        <div class="popupHeading">
                            <h2>Edit Your Profile</h2>
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="hidden" name="user_id" value="{{ $admin->id }}">
                            <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Name" id="Name" name="name" value="{{ old('name', $admin->name ) }}">
                            <label for="Name" class="floating-label">Name</label>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="text" class="form-control @error('mobile') is-invalid @enderror" placeholder="Mobile Number" id="number" name="mobile" value="{{ old('mobile', $admin->mobile) }}" disabled>
                            <label for="number" class="floating-label">Mobile Number</label>
                            @error('mobile')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="text" class="form-control @error('phone') is-invalid @enderror" placeholder="Email id" id="email" name="email" value="{{ $admin->email }}" disabled>
                            <label for="email" class="floating-label">Email id</label>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div> 

                        <div class="formBtn">
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div> 
                    </form>
                </div><!--//popupForm--> 
            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->   
<!--//delete modal for secondary admin --> 
<div class="modal alert-model" id="Delete" data-keyboard="true"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close-bt" data-dismiss="modal">&times;</button>
            <!-- Modal body -->
            <div class="modal-body alert-warning">
                <h4 class="text-warning">Delete Secondary Admin</h4>
                <p class="text-warning">Are you sure you want to delete this Secondary Admin?</p>
                <div class="col-lg-12 text-center">

                    <form name="admin_deletion" id="admin_deletion" method="post" action="{{ route('admin.delete-admin') }}">
                        @csrf
                        <button type="button" class="no-bt" data-dismiss="modal">No</button>
                        <input type="hidden" name="deletion_id" id="deletion_id">
                        <button type="submit" class="yes-bt">Yes</button>
                    </form>
                </div>
            </div>
            <!--//Modal body-->

        </div>
    </div>
</div>

@if($errors->has('adminProfile'))
<script>
    $(document).ready(function () {
        $('.editprofileButton').click();
    });</script>
@endif
@endsection
@section('custom-script')
<script type="text/javascript">
    function deleteAdmin(id){

    $('#deletion_id').val(id);
    };
    function editSecondaryAdmin(id){
    $('#permissionExpertId').val(id);
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
    type: 'POST',
            url: "{{ url('admin/editsecondaryAdmin') }}",
            data: {
            id: id,
            },
            success: function (data) {
            var columnArr = data.map(function(row) {
            return row['manager_id'];
            });
            console.log(columnArr);
            var manager = <?php echo managersList() ?>;
            $('#multiple-checkboxes2').html('');
            $.each(manager, function(index, value){;
                var selected = '';
                if(jQuery.inArray( value.id, columnArr )){
                    selected = 'seleced';
                }
                $('#multiple-checkboxes2').append('<option value="'+value.id+'"'+selected+'>'+value.name+'</option>')
            });
             $.magnificPopup.open({
                    items: {
                        src: '#secondary-Admin-popup',
                        callbacks: {
                            beforeOpen: function () {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        type: 'inline'
                    }

                });
            
            },
    });
    }
</script>
@endsection