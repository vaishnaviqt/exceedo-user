@extends('admin.layouts.master')
@section('title')
Rating & Reviews
@endsection
@section('class')
paymentsBody
@endsection
@section('content')
 


<div class="pageTitle d-flex align-items-center">
    <h1>{{ __('Rating & Reviews') }}</h1> 
</div><!--//pageTitle-->


                <!-- //Search Form Div Start -->
                <div class="searchForm"> 
    <a href="javascript:void(0);" class="open-mobile-search">{{ __('Search') }}</a>                                   
    <form action="{{ url('admin/rating-and-review')}}" method="post">
        @csrf
                        <span>Search</span>
                        <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
                        <ul class="d-flex flex-wrap width-18">
                            <li>
                                <div class="form-group">
                <input class="form-control" id="datepicker" type="text" name="created_at" 
                    value="{{ !empty($startDate) ? Carbon\Carbon::parse($startDate)->format('d-m-Y') : '' }}" 
                    placeholder="Date"/>
                                </div><!-- //form-group -->
                            </li>
                            <li>
                                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Customer Name" name="customerName" id="customerName" value="{{ $customerName }}">
                                </div><!-- //form-group -->
                            </li>
                            <li>
                                <div class="form-group">
                    <select class="form-control" id="expertType" name="expertType">
                        <option value="">{{ __('Experts') }}</option>
                        @if(count($experts))
                        @foreach($experts as $expert)
                        @php
                        $selected = '';
                        if($expert->id == $expertType){
                        $selected = 'selected';
                        }
                        @endphp
                        <option value="{{ $expert->id }}" {{ $selected }}>{{ $expert->name }}</option>
                        @endforeach
                        @endif
                                    </select>
                                </div><!-- //form-group --> 
                            </li>
                            <li>
                                <div class="form-group">
                                    <select class="form-control" id="ratingType" name="ratingType">
                        <option value="">{{ __('Rating Star') }}</option>
                                        <option value="1" @if($ratingType == 1) {{ __('selected') }} @endif>
                                        1 Star </option>
                                        <option value="2" @if($ratingType == 2) {{ __('selected') }} @endif>
                                        2 Star</option>
                                        <option value="3" @if($ratingType == 3) {{ __('selected') }} @endif>
                                        3 Star</option>
                                        <option value="4" @if($ratingType == 4) {{ __('selected') }} @endif>
                                        4 Star</option>
                                        <option value="5" @if($ratingType == 5) {{ __('selected') }} @endif>
                                        5 Star</option>
                                    </select>
                                </div><!-- //form-group --> 
                            </li>
                            <li>
                                <div class="form-group">
                    <select class="form-control" id="serviceType" name="serviceType">
                        <option value="">{{ __('Lead Suggest') }}</option>
                        @if(count($services))
                        @foreach($services as $service)
                        @php
                        $selected = '';
                        if($service->id == $serviceType){
                        $selected = 'selected';
                        }
                        @endphp
                        <option value="{{ $service->id }}" {{ $selected }}>{{ $service->service_name }}</option>
                        @endforeach
                        @endif
                                    </select>
                                </div><!-- //form-group --> 
                            </li>
                            <li>
                <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
                            </li>
                        </ul><!-- //ul -->
                    </form><!-- //form -->
                </div><!-- //search-form -->
                <!-- //Search Form Div End -->

                <div class="rating-reviews">
    <!--    <h4>10 Nov 2020</h4>-->
    @forelse($reviews as $review)
                    <div class="comments-review width-25">
                        <div class="d-flex justify-content-start flex-lg-nowrap flex-wrap mb-2">
            @php
            $path = asset('broker/images/profile-pic.png');
            if(Storage::disk('user')->exists(str_replace('user_image', '', $review->photo)))
            {
            $path = Storage::disk('user')->url(str_replace('user_image/', '', $review->photo));
            }
            @endphp
            <div><img class="d-flex align-self-start mr-3" src="{{ $path }}" alt="No Image"></div>
                            <div>
                <h5>{{ isset($review->username) ?  ucwords($review->username) : ''}}</h5>
                <span>{{ isset($review->email) ?  ($review->email) : ''}}</span>
                            </div>
                            <div>
                <div class="placeholder" style="color: lightgray;">
                    {!! userRatings($review->rating) !!}
                            </div>

                <label><span>{{ __('Lead Suggest') }} :</span>
                    {{ isset($review->service_name) ?  ucwords($review->service_name) : ''}} </label>                                           
                            </div>
                            <div>
                <span>{{ __('Appointment Date and Time') }}</span>
                <label>{{ isset($review->meeting_at) ? date('d M Y, H:i a', strtotime($review->meeting_at)) : '' }}</label>
                            </div>
                            <div>
                <span>{{ __('Expert Name') }}</span>
                <label>{{isset($review->expertname) ? ucwords($review->expertname) : ''}}</label>
                            </div>
                        </div><!--//d-flex justify-content-start flex-lg-nowrap flex-wrap-->
        <div><span>{{ __('Feedback') }} : </span>{{ ucfirst($review->feedback) }}</div>
                    </div><!--//comments-review-->
    @empty
    <div class="card-body"> 
        {{ __('No Data Found') }}
    </div>
    @endforelse
    <div class="col-12">
        <ul class="pagination justify-content-center">
            {{ $reviews->links() }}
        </ul>
    </div><!--//payment-table-desktop-->
                </div><!--//rating-reviews-->

@endsection
@section('modal')
@endsection
@section('custom-script')
    @endsection