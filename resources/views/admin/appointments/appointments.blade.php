@extends('admin.layouts.master')
@section('title')
Appointments
@endsection
@section('content')
<style>

    .loader{
        position: fixed;
        top:0;
        left:0;
        right: 0;
        bottom:0;
        display:flex;
        align-items:center;
        justify-content:center;
        z-index: 9999;
        background: #ffffffa6;
    }
    .loader img{
        width:80px;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
<div class="loader d-none">
    <img src="{{ asset('images/Spin-1s-200px.gif') }}">
</div>
<section class="reportsBody" id="inline-popups">
    @include('flash-message')
    <h2>Appointments</h2>
    <div class="countCards">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <a href="javascript:void(0);" class="nav-link allAppointButton">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/all-appointments-icon.svg') }}" alt="All Appointments" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $totalAppointments }}</strong></p>
                        <p>All Appointments</p>
                    </div>
                </article><!--//countCardArt-->
                </a>
            </div><!--//col-lg-4-->

            <div class="col-lg-4 col-md-6">
                <a href="javascript:void(0);" class="nav-link pendingAppointButton">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/upcoming-appointments-icon.svg') }}" alt="Upcoming Appointments" />                                     
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $upcommingAppointments }}</strong></p>
                        <p>Upcoming Appointments</p>
                    </div>
                </article><!--//countCardArt-->
                </a>
            </div><!--//col-lg-4-->

            <div class="col-lg-4 col-md-6">
                <a href="javascript:void(0);" class="nav-link doneAppointButton">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/done-appointments-icon.svg') }}" alt="Done Appointments" />                                
                    </div>
                    <div class="countRepDetail donepApointButton" href="javascript:;">
                        <p><strong>{{ $doneAppointments }}</strong></p>
                        <p>Done Appointments</p>
                    </div>
                </article><!--//countCardArt-->
                </a>
            </div><!--//col-lg-4-->                       
        </div><!--//row-->
    </div><!--//countCards-->

    <!-- //Search Form Div Start -->
    <div class="searchForm">
        <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                  
        <form method="post" action="{{ url('admin/appointments') }}">
            @csrf
            <span>Search</span>
            <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
            <ul class="d-flex flex-wrap">
                <li>
                    <div class="form-group">
                        <select class="form-control" id="expertId" name="all_expert_id">
                            <option value="">Select Expert</option>
                            @if(count($experts))
                            @foreach($experts as $expert)
                            <option value="{{ $expert->id }}" @if($expert->id == $allExpert) {{ __('selected') }} @endif>{{ $expert->name }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div><!-- //form-group --> 
                </li>
                <li>
                    <div class="form-group">
                        <select class="form-control" id="customerId" name="customer_id">
                            <option value="">Select Customer</option>
                            @if(count($users))
                            @foreach($users as $user)
                            <option value="{{ $user->id }}" @if($user->id == $allCustomerId) {{ __('selected') }} @endif>{{ $user->name }}</option>
                            @endforeach
                            @endif
                        </select>

                    </div><!-- //form-group -->
                </li>                                            
<!--                <li>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Appointment" id="appointment">
                    </div> //form-group 
                </li>-->
                <li>
                    <div class="form-group">
                        <select class="form-control" id="paymentType" name="payment_type">
                            <option value="">Payment Method</option>
                            <option value="cash" @if($allPaymentType == 'cash') {{ __('selected') }} @endif>Cash</option>
                            <option value="card" @if($allPaymentType == 'card') {{ __('selected') }} @endif>Card</option>
                        </select>
                    </div><!-- //form-group --> 
                </li>
                <li>
                     <div class="form-group">
                        <select class="form-control" id="serviceId" name="value_added_service_id">
                            <option value="">Select Value Added Services</option>
                            @if(count($services))
                            @foreach($services as $service)
                            @php
                            $selected = '';
                            if($serviceId == $service->id){
                            $selected = 'selected';
                            }
                            @endphp
                            <option value="{{ $service->id }}" {{ $selected }}>{{ $service->service_name }}</option>
                            @endforeach
                            @endif

                        </select>
                    </div>
                </li>
                <li>
                    <button class="btn btn-primary" type="submit" id="submitAppointFilter">Search</button>
                </li>



            </ul><!-- //ul -->
        </form><!-- //form -->
    </div><!-- //searchForm -->
    <!-- //Search Form Div End -->

    <div class="customStyleCalendar">
        <div id="calendar"></div>
    </div>
</section><!--//pageContainer-->

<!-- approval-appointment-popup Start-->
@include('admin.appointments.__models')
<!-- view-all-appointments-popup Start-->

@endsection

@section('custom-script')

<script>
var baseUrl = $('meta[name="base_url"]').attr('content');
$(document).ready(function () {

   var url = "/admin/appointment-details?expertId=<?php echo $allExpert; ?>&userId=<?php echo $allCustomerId; ?>&paymentType=<?php echo $allPaymentType; ?>&valueAddedServiceId=<?php echo $serviceId; ?>";
    var calendar = $('#calendar').fullCalendar({
        editable: true,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        events: baseUrl + url,
        selectable: true,
        selectHelper: true,
        select: function (start, end, allDay)
        {
            var start = $.fullCalendar.formatDate(start, "DD-MM-Y");
            var end = $.fullCalendar.formatDate(end, "HH:mm A");
            $('.appDate').val(start);
            $('.appTime').val(end);
            $('#valueAddedServices').val('');
            $('#user_id').val('');
            $('#remarks').val('');
            $("#terms").prop("checked", false);

            $('#addAppointment').modal('show');
        },
//        editable: true,
//        eventResize: function (event)
//        {
//            var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
//            var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
//            var title = event.title;
//            var id = event.id;
//            $.ajax({
//                url: "update.php",
//                type: "POST",
//                data: {title: title, start: start, end: end, id: id},
//                success: function () {
//                    calendar.fullCalendar('refetchEvents');
//                    alert('Event Update');
//                }
//            })
//        },
//        eventDrop: function (event)
//        {
//            var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
//            var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
//            var title = event.title;
//            var id = event.id;
//            $.ajax({
//                url: "update.php",
//                type: "POST",
//                data: {title: title, start: start, end: end, id: id},
//                success: function ()
//                {
//                    calendar.fullCalendar('refetchEvents');
//                    alert("Event Updated");
//                }
//            });
//        },
        eventClick: function (event)
        {

            var title = event.title;
            var dateTime = $.fullCalendar.formatDate(event.start, "dddd, DD MMMM, hh:mm a");
            var evDate = $.fullCalendar.formatDate(event.start, "DD-MM-Y");
            var evTime = $.fullCalendar.formatDate(event.start, "HH:mm:ss");
            var eventId = event.id;
            var expertid = event.expert_id;
            var rescheduled_by = event.rescheduled_by;
            var canceled_by = event.canceled_by;
            var rescheduleButton = '';
            var button = '';
            if (rescheduled_by == null && !expertid && new Date() <= event.start) {
                var button = `<ul class="confirmAppointment">
        <li>Pending assign to expert</li>
</ul>`;
            }
            if (event.status == 3) {
                if (canceled_by == 2) {
                    var text = "You cancel this appointment";
                } else if (canceled_by == 0) {
                    var text = "User cancel this appointment";
                }
                button = `<ul class="confirmAppointment">
                        <li>` + text + `</li>
                    </ul>`;
            } else if (event.status != 1 && rescheduled_by == 1) {
                button = `<ul class="confirmAppointment">
                        <li>Expert reschedule this appointment, waiting user confirmation</li>
                    </ul>`;
            } else if (event.status != 1 && rescheduled_by == 0) {
                button = `<ul class="confirmAppointment">
                        <li>User reschedule this appointment, waiting expert confirmation</li>
                    </ul>`;
            } else if (event.status == 1 && new Date() <= event.start) {
                button = `<ul class="confirmAppointment">
                        <li><a class="adminCancelAppointment" data-href="` + baseUrl + "/admin/cancel-appointment/" + eventId + `" href="javascript:;">Cancel the Appointment</a></li>
                    </ul>`;
            }
            var html = `<div class="row">
                <div class="col-12">
                    <ul>
                        <li>
                            <h2>` + title + `</h2>
                            <span class="dateTime">` + dateTime + ` </span>
                            
                        </li>
                                <li>user: `+event.user_name+`</li>
                                <li>Expert: `+event.expert_name+`</li>
                                <li>Amount: Rs `+event.price+`</li>
                    </ul>
                    ` + button + `
                </div>         
            </div>`;
            $('.suggestAnotherSchedule').html(html);
            $('#approval-appointment-popup').modal('show');

        },
    });
});


$(".appointments").mCustomScrollbar({
    theme: "dark",
    scrollButtons: {scrollType: "stepped"},
    live: "on"
});
$('#valueAddedServices').on('change', function () {
    var serviceId = $(this).val();
    var price = '';
<?php
foreach ($services as $service) {
    $serviceId = $service->id ?? 'null';
    $price =  $service->price + ($service->price*18/100) ?? 'null';

    ?>
        if (serviceId == <?php echo $serviceId ?>) {
            price = <?php echo $price ?>
        }
    <?php
}
?>
    var data = '<span>You need to Pay</span> INR ' + price;
    $('#servicePrice').html(data);
    $('#serviceInputPrice').val(price);
});

// show all expert in land listing
$('body').on('click', '.showAllExpert', function () {
    var appointId = $(this).data('id');
    var selectId = $(this).data('select');
    $.ajax({
        url: "show-all-expert",
        type: "POST",
        data: '',
        success: function (data)
        {
            if (data) {
                $('.' + selectId + appointId).html('');
                $('.' + selectId + appointId).html('<option value="">Assigned to Expert</option>');
                $.each(data, function (key, value) {
                    $('.' + selectId + appointId).append('<option value="' + value.id + '">' + value.name + '</option>')
                });
            }
        }
    })
});


// Appointment Assign To expert
$('body').on('click', '.assignButton', function (e) {
    e.preventDefault();
    var expertId = $(this).parent().parent().find('.assignToExpert').val();
    var appointId = $(this).data('id');
    if (expertId == '') {
        swal("Please Select Expert");
        return false;
    }
    $('.loader').toggleClass('d-none');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: baseUrl + "/admin/assign-to-expert",
        type: "POST",
        data: {appointId: appointId, expertId: expertId, },
        success: function (data)
        {
            $('.loader').toggleClass('d-none');
            if (data) {
                $('#myModal-thankyou').modal('show');
            } else {
                $('#myModal-thankyouError').modal('show');
            }
        }
    })

});
$('body').on('click', '#addAppointButton', function (e) {
    e.preventDefault();
    var formValues = $('#addAppointForm').serialize();

    $('#addAppointButton').prop("disabled", true);
    $('.loader').toggleClass('d-none');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "post",
        url: "add-appointment",
        data: formValues,
        success: function (data) {
            $('.loader').toggleClass('d-none');

            if (data.success == true) {
                swal(data.msg)
                        .then((value) => {
                            location.reload();
                        });
            } else {
                swal(data.msg);


            }
        },
        error: function (errorResponse) {
            $('.loader').toggleClass('d-none');
            $('#addAppointButton').prop("disabled", false);
            $('.error-span').remove();
            $.each(errorResponse.responseJSON.errors, function (field_name, error) {
                if (field_name == "terms") {

                    $(document).find('[name=' + field_name + ']').parent().parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                } else {
                    $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                }
            })
        }
    });
});


//Admin Cancel appointment

$('body').on('click', '.adminCancelAppointment', function (e) {
    e.preventDefault();
    var link = $(this).data('href');
    swal({
        text: "Are you sure want to cancel appointment?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
            .then((willDelete) => {
                if (willDelete) {
                    window.location.href = link;
                } else {
                    return false;
                }
            });
});

</script>

<script>
    $(document).ready(function () {

        $('body').on('click', '.viewAllAppointSearchForm', function () {
            viewAllAppointment();
        });
        $('.allAppointButton').on('click', function () {
            viewAllAppointment();
        });
    });
    function viewAllAppointment() {
        var all_expert_id = $('#allAppointExpert').val();
        var all_date = $('#allAppointDate').val();
        let service_id = $('#serviceId').val();
        $('.loader').toggleClass('d-none');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "post",
            url: "view-all-appointment",
            data: {
                all_date: all_date,
                all_expert_id: all_expert_id,
                service_id:service_id
            },
            success: function (data) {
                $('.loader').toggleClass('d-none');
                $('#allAppointmentCard').html(data.html);
                $(".appointments").mCustomScrollbar({
                    theme: "dark",
                    scrollButtons: {scrollType: "stepped"},
                    live: "on"
                });
                $.magnificPopup.open({
                    items: {
                        src: '#view-all-appointments-popup',
                        callbacks: {
                            beforeOpen: function () {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        type: 'inline'
                    }

                });
            },
            error: function () {
                alert("Fetching Expert Record Failed");
                return false;
            }
        });
        return false;
    }
    $(document).ready(function () {
        $('body').on('click', '.viewPendingAppointSearchForm', function () {
            viewPendingAppointment();
        });
        $('.pendingAppointButton').on('click', function () {
            viewPendingAppointment();
        });
    });
    function viewPendingAppointment() {
        var expert_id = $('#pendingExpertId').val();
        var date = $('#pendingDate').val();
        var serviceId = $('#upcommingServiceId').val();
        $('.loader').toggleClass('d-none');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "post",
            url: "view-pending-appointment",
            data: {
                date: date,
                expert_id: expert_id,
                serviceId: serviceId,
            },
            success: function (data) {
                $('.loader').toggleClass('d-none');
                $('#pendingAppointmentCard').html(data.html);
                $(".appointments").mCustomScrollbar({
                    theme: "dark",
                    scrollButtons: {scrollType: "stepped"},
                    live: "on"
                });
                $.magnificPopup.open({
                    items: {
                        src: '#pending-appointment-popup',
                        callbacks: {
                            beforeOpen: function () {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        type: 'inline'
                    }

                });
            },
            error: function () {
                alert("Fetching Expert Record Failed");
                return false;
            }
        });
        return false;
    }

    //view done appointment
    $(document).ready(function () {
        $('body').on('click', '.viewDoneAppointSearchForm', function () {
            viewDoneAppointment();
        });
        $('.doneAppointButton').on('click', function () {
            viewDoneAppointment();
        });
    });
    function viewDoneAppointment() {
        var expert_id = $('#doneExpertId').val();
        var date = $('#doneDate').val();
        var serviceId = $('#doneserviceId').val();
        $('.loader').toggleClass('d-none');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "post",
            url: "view-done-appointment",
            data: {
                date: date,
                expert_id: expert_id,
                serviceId: serviceId,
            },
            success: function (data) {
                $('.loader').toggleClass('d-none');
                $('#pendingAppointmentCard').html(data.html);
                $(".appointments").mCustomScrollbar({
                    theme: "dark",
                    scrollButtons: {scrollType: "stepped"},
                    live: "on"
                });
                $.magnificPopup.open({
                    items: {
                        src: '#pending-appointment-popup',
                        callbacks: {
                            beforeOpen: function () {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        type: 'inline'
                    }

                });
            },
            error: function () {
                alert("Fetching Expert Record Failed");
                return false;
            }
        });
        return false;
    }
</script>

<script>
    $(document).ready(function() {
    $(".js-example-basic-single").select2();
  });

</script>





<script>
    $(document).ready(function () {

        // datepickers
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#allAppointDate').datepicker({
            minDate: today,
            format: 'dd-mm-yyyy'
        });

        $(document).on('click',"#resetForm",function(){
            $('#allAppointExpert option').removeAttr('selected')
            .filter('[value=""]')
            .attr('selected', true);
            $('#allAppointDate').val('');
            $('#serviceId option').removeAttr('selected')
            .filter('[value=""]')
            .attr('selected', true);
            viewAllAppointment();
        })
        $(document).on('click',"#resetCompleteForm",function(){
            $('#doneExpertId option').removeAttr('selected')
            .filter('[value=""]')
            .attr('selected', true);
            $('#doneDate').val('');
            $('#serviceId option').removeAttr('selected')
            .filter('[value=""]')
            .attr('selected', true);
            viewDoneAppointment();
        })
        $(document).on('click',"#resetUpcommingForm",function(){
            $('#pendingExpertId option').removeAttr('selected')
            .filter('[value=""]')
            .attr('selected', true);
            $('#pendingDate').val('');
            $('#upcommingServiceId option').removeAttr('selected')
            .filter('[value=""]')
            .attr('selected', true);
            viewPendingAppointment();
        })

    });

 </script>



@endsection
