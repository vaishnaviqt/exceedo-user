<div class="col-12">
    <h2>Upcoming Appointments 
        <!--<span>(06 Oct 2020)</span>-->
    </h2>
</div><!--//.col-12 -->  

<div class="col-12">
    <div class="searchForm">
        <form method="post" action="{{ url('admin/appointments') }}" id="upcommingAppointForm">
            @csrf
            <ul class="d-flex flex-wrap">
                <li>
                    <div class="form-group">
                        <select class="form-control" name="expert_id" id="pendingExpertId">
                            <option value="">Select Expert</option>
                            @php
                            $experts= expertListShow();
                            @endphp
                            @if(count($experts))
                            @foreach($experts as $expert)
                            <option value="{{ $expert->id }}" @if($pendingExpert == $expert->id ) {{ __('selected') }} @endif>{{ $expert->name }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div><!-- //form-group --> 
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control" type="text" name="date" id="pendingDate"value="{{ !empty($pendingDate) ? Carbon\Carbon::parse($pendingDate)->format('Y-m-d') : '' }}" placeholder=""/>
                    </div><!-- //form-group --> 
                </li>
                <li>
                     <div class="form-group">
                        <select class="form-control" id="upcommingServiceId" name="value_added_service_id">
                            <option value="">Select Value Added Services</option>
                            @if(count($services))
                            @foreach($services as $service)
                            @php
                            $selected = '';
                            if($serviceId == $service->id){
                            $selected = 'selected';
                            }
                            @endphp
                            <option value="{{ $service->id }}" {{ $selected }}>{{ $service->service_name }}</option>
                            @endforeach
                            @endif

                        </select>
                    </div>
                </li>

                <li>
                    <div class="row">
                        <div class="col-lg-6">                            
                            <input type="button" value="Search" name="submit" class="btn btn-primary viewPendingAppointSearchForm">
                        </div>
                        <div class="col-6">
                            <input type="button" id="resetUpcommingForm"  class="btn btn-primary" value="Reset">
                    </div>
                    </div>
                </li>
            </ul><!-- //ul -->
        </form><!-- //form -->
    </div><!--//searchForm-->
</div><!-- //.col-12-->

<div class="col-12">
    <div class="appointments pendingAppoint">
        <form>
            @forelse($pendingAppoints as $pending)

            <ul class="appointments-list">
                <li class="order-md-1 order-1">
                    <h3>{{ isset($pending->service->service_name) ? $pending->service->service_name : '' }}</h3>
                </li>
                <li class="order-md-2 order-3">
                    {{ !empty($pending->date) ? Carbon\Carbon::parse($pending->date . ' ' . $pending->time)->format('d M, Y h:i A') : '' }}
                    <br>
                    Service:     {{ isset($pending->service->service_name) ? $pending->service->service_name : '' }}
                    <br>
                    Amount:     Rs {{ $pending->price }}
                    <br>
               @if($pending->rescheduled_by == 1 && $pending->status == 0)
               @if(isset($pending->expert->name))
               {{ isset($pending->expert->name) ? $pending->expert->name : ''  }} expert rescheduled this appointment.
               @endif
               @elseif($pending->rescheduled_by == 0 && $pending->status == 0)
               @if(isset($pending->user->name))
               {{ $pending->user->name  }} user rescheduled this appointment..
               @endif
               @endif
               
               @if($pending->canceled_by == 2 && $pending->status == 3)
               You cancel this appointment.
               @elseif($pending->canceled_by == 0 && $pending->status == 3)
               @if(isset($pending->user->name))
               {{ $pending->user->name  }} user cancel this appointment.
               @endif
               @endif
                </li>
                <li class="order-md-3 order-2">
                    <div class="form-group">
                        @php
                        $expertsAll= expertListShow($pending->value_added_service_id);
                        @endphp
                        <select class="form-control assignToExpert pendingAppointExpert-{{ $pending->id }}">
                            <option value="">Assigned to Expert</option>
                            @if(count($expertsAll))
                            @foreach($expertsAll as $expert)
                            @php
                            $serviceId = array();
                            if(count($expert->userService)){
                            $serviceId =  array_column($expert->userService->toArray(), 'value_added_service_id');
                            }
                            @endphp
                            @if(count($serviceId) && in_array($pending->value_added_service_id, $serviceId) || $expert->id == $pending->expert_id)
                            <option value="{{ $expert->id }}" @if($expert->id == $pending->expert_id) {{ __('selected') }} @endif>{{ $expert->name }}</option>
                            @endif
                            @endforeach
                            @endif
                        </select>
                    </div><!-- //form-group --> 
                </li>
                <li class="order-md-4 order-4 w-100">
                     User: {{ isset($pending->user->name) ? $pending->user->name : '' }}
                    <br>
                    Expert: {{ isset($pending->expert->name) ? $pending->expert->name : 'Not assign yet' }}
                </li>
                <li class="order-md-4 order-4 w-100 text-right">
                    <button class="btn btn-primary assignButton" data-id="{{ $pending->id }}" type="button">Assign</button>
                    <button class="btn btn-primary showAllExpert" data-id="{{ $pending->id }}" data-select="pendingAppointExpert-" type="button">Show All</button>
                </li>
            </ul><!--//appointments-list-->
            @empty
            <p class="no-data mb-0">No Pending Appointment Available</p>
            @endforelse
        </form>
        <!--//form-->
    </div>
    <!--//appointments -->
</div>
<script type="text/javascript">
    $(document).ready(function () {
        // datepicker
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#pendingDate').datepicker({
            // maxDate: today,
            format: 'dd-mm-yyyy'
        });
    })
</script>
<!--//col-12-->
