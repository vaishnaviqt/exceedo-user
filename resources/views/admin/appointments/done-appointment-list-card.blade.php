

<div class="col-12">
    <h2>Complete Appointments 
        <!--<span>(06 Oct 2020)</span>-->
    </h2>

</div><!--//.col-12 -->  
<div class="col-12">
    <div class="d-none viewAppointmentError ml-5 mt-2 mb-3"></div>
    <div class="searchForm">
        <form method="post" action="{{ url('admin/appointments') }}" id="viewDoneAppointSearchForm">
            @csrf
            <ul class="d-flex flex-wrap">
                <li>
                    <div class="form-group">
                        <select class="form-control" name="done_expert_id" id="doneExpertId">
                            <option value="">Select Expert</option>
                            @if(count($experts))
                            @foreach($experts as $expert)
                            <option value="{{ $expert->id }}" @if($doneExpert == $expert->id ) {{ __('selected') }} @endif>{{ $expert->name }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div><!-- //form-group --> 
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control" type="text" name="done_date" id="doneDate" value="{{ !empty($doneDate) ? Carbon\Carbon::parse($doneDate)->format('Y-m-d') : '' }}" placeholder="" />
                    </div><!-- //form-group --> 
                </li>
                <li>
                     <div class="form-group">
                        <select class="form-control" id="doneserviceId" name="value_added_service_id">
                            <option value="">Select Value Added Services</option>
                            @if(count($services))
                            @foreach($services as $service)
                            @php
                            $selected = '';
                            if($serviceId == $service->id){
                            $selected = 'selected';
                            }
                            @endphp
                            <option value="{{ $service->id }}" {{ $selected }}>{{ $service->service_name }}</option>
                            @endforeach
                            @endif

                        </select>
                    </div>
                </li>

                <li>
                    <div class="row">
                        <div class="col-lg-6">                            
                            <input type="button" value="Search" name="submit" class="btn btn-primary viewDoneAppointSearchForm">
                        </div>
                        <div class="col-6">
                            <input type="button" id="resetCompleteForm"  class="btn btn-primary" value="Reset">
                    </div>
                    </div>
                </li>
            </ul><!-- //ul -->
        </form><!-- //form -->
    </div><!--//searchForm-->
</div><!-- //.col-12-->
<div class="col-12">
    <div class="appointments">
        <form>
            @forelse($doneAppoints as $appoint)
            <ul class="appointments-list">
                <li class="order-md-1 order-1"><h3>{{  isset($appoint->service->service_name) ? $appoint->service->service_name : '' }}</h3></li>
                <li class="order-md-2 order-3">
                    {{ !empty($appoint->date) ? Carbon\Carbon::parse($appoint->date .' '.$appoint->time)->format('d M, Y h:i A') : ''}}
                    <br>
                    Service:     {{ isset($appoint->service->service_name) ? $appoint->service->service_name : '' }}
                    <br>
                    Amount:     Rs {{ $appoint->price }}
                </li>
                <li class="order-md-4 order-4">
                    User: {{ isset($appoint->user->name) ? $appoint->user->name : '' }}
                    <br>
                    Expert: {{ isset($appoint->expert->name) ? $appoint->expert->name : 'Not assign yet' }}
                </li>
            </ul><!--//appointments-list-->
            @empty
            <p class="no-data mb-0">No Appintment Available</p>
            @endforelse

        </form>
        <!--//form-->
    </div>
    <!--//appointments -->
</div>
<!--//col-12-->
<script type="text/javascript">
    $(document).ready(function () {
        // datepicker
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#doneDate').datepicker({
            // maxDate: today,
            format: 'dd-mm-yyyy'
        });
    })
</script>
