

<div class="col-12">
    <h2>All Appointments 
        <!--<span>(06 Oct 2020)</span>-->
    </h2>

</div><!--//.col-12 -->  
<div class="col-12">
    <div class="d-none viewAppointmentError ml-5 mt-2 mb-3"></div>
    <div class="searchForm">
        <form method="post" action="{{ url('admin/appointments') }}" id="viewAllAppointSearchForm">
            @csrf
            <ul class="d-flex flex-wrap">
                <li>
                    <div class="form-group">
                        <select class="form-control" name="all_expert_id" id="allAppointExpert">
                            <option value="">Select Expert</option>
                            @if(count($experts))
                            @foreach($experts as $expert)
                            <option value="{{ $expert->id }}" @if($allExpert == $expert->id ) {{ __('selected') }} @endif>{{ $expert->name }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div><!-- //form-group --> 
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control" type="text" name="all_date" id="allAppointDate" value="{{ !empty($allDate) ? Carbon\Carbon::parse($allDate)->format('Y-m-d') : '' }}" placeholder=""/>
                    </div>
                </li>


                <li>
                    <div class="form-group">
                        <select class="form-control" id="serviceId" name="value_added_service_id">
                            <option value="">Select Value Added Services</option>

                            {{-- @if(count((is_countable($services)?$services:['service_name']))) --}}
                            @foreach($allServices as $service)
                            <option value="{{ $service->id }}">{{ $service->service_name }}</option>
                            @endforeach

                        </select>
                    </div>
                </li>

                    <!-- //form-group -->

                <li>
                    <div class="row row_10">
                        <div class="col-6">
                            <input type="button" value="Search" name="submit" class="btn btn-primary viewAllAppointSearchForm">
                        </div>
                        <div class="col-6">
                    <input type="button" id="resetForm"  class="btn btn-primary" value="Reset">
                        </div>
                    </div><!--//row-->
                </li>
            </ul><!-- //ul -->
        </form><!-- //form -->
    </div><!--//searchForm-->
</div><!-- //.col-12-->
<div class="col-12">
    <div class="appointments">
        <form>
            @forelse($allAppoints as $appoint)
            <ul class="appointments-list">
                <li class="order-md-1 order-1"><h3>{{ isset($appoint->service->service_name) ? $appoint->service->service_name : '' }}</h3></li>
                <li class="order-md-2 order-3">
                    {{ !empty($appoint->date) ? Carbon\Carbon::parse($appoint->date .' '.$appoint->time)->format('d M, Y h:i A') : ''}}
                <br>
               Service:     {{ isset($appoint->service->service_name) ? $appoint->service->service_name : '' }}
               <br>
               Amount:     Rs {{ $appoint->price }}
               <br>
               @if($appoint->rescheduled_by == 1 && $appoint->status == 0)
               @if(isset($appoint->expert->name))
               {{ isset($appoint->expert->name) ? $appoint->expert->name : ''  }} expert rescheduled this appointment.
               @endif
               @elseif($appoint->rescheduled_by == 0 && $appoint->status == 0)
               @if(isset($appoint->user->name))
               {{ $appoint->user->name  }} user rescheduled this appointment..
               @endif
               @endif
               
               @if($appoint->canceled_by == 2 && $appoint->status == 3)
               You cancel this appointment.
               @elseif($appoint->canceled_by == 0 && $appoint->status == 3)
               @if(isset($appoint->user->name))
               {{ $appoint->user->name  }} user cancel this appointment.
               @endif
               @endif
                </li>
                @if(Carbon\Carbon::parse($appoint->meeting_at)->format('Y-m-d') > date('Y-m-d'))
                <li class="order-md-3 order-2">
                    <div class="form-group">
                        @php
                        $expertsAll= expertListShow($appoint->value_added_service_id);
                        @endphp
                        <select class="form-control assignToExpert assignToExpert-{{$appoint->id}}">
                            <option value="">Assigned to Expert</option>
                            @if(count($expertsAll))
                            @foreach($expertsAll as $expert)
                            @php
                            $serviceId = array();
                            if(count($expert->userService)){
                            $serviceId =  array_column($expert->userService->toArray(), 'value_added_service_id');
                            }
                            @endphp
                            @if(count($serviceId) && in_array($appoint->value_added_service_id, $serviceId) || $expert->id == $appoint->expert_id)
                            <option value="{{ $expert->id }}" @if($expert->id == $appoint->expert_id) {{ __('selected') }} @endif>{{ $expert->name }}</option>
                            @endif
                            @endforeach
                            @endif
                        </select>
                    </div><!-- //form-group --> 
                </li>
                @endif
                <li class="order-md-4 order-4 w-100">
                    User: {{ isset($appoint->user->name) ? $appoint->user->name : '' }}
                    <br>
                    Expert: {{ isset($appoint->expert->name) ? $appoint->expert->name : 'Not assign yet' }}
                </li>
                @if(Carbon\Carbon::parse($appoint->meeting_at)->format('Y-m-d') > date('Y-m-d'))
                <li class="order-md-4 order-4 w-100 text-right">
                    <button class="btn btn-primary assignButton" data-id="{{ $appoint->id }}" type="button">Assign</button>
                    <button class="btn btn-primary showAllExpert" data-id="{{ $appoint->id }}" data-select="assignToExpert-" type="button">Show All</button>
                </li>
                @endif
            </ul><!--//appointments-list-->
            @empty
            <p class="no-data mb-0">No Appintment Available</p>
            @endforelse

        </form><!--//form-->
    </div><!--//appointments -->
</div>
@section('custom-script')
<script>
    $(document).ready(function () {

        // datepicker
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#allAppointDate').datepicker({
//            minDate: today,
            format: 'dd-mm-yyyy'
        });
 
    });
        </script>
         @endsection<!--//col-12-->
