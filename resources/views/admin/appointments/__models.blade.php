<style>
    .modal-backdrop.fade.show{
        z-index: 1049
    }
    .modal-content.popup-body .modal-body{
        padding:0
    }
</style>

<!-- view-all-appointments-popup Start-->
<div id="view-all-appointments-popup" class="mfp-with-anim mfp-hide viewAll-appointment">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row" id="allAppointmentCard">
        </div><!--/row-->

    </div><!-- //popup-body --> 
</div><!-- //view-all-appointments-popup End -->


<!-- Pending appintment Popup Start-->
<div id="pending-appointment-popup"class="mfp-with-anim mfp-hide viewAll-appointment">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row" id="pendingAppointmentCard">
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Pending appintment Popup End -->

<div class="modal fade w-400" id="addAppointment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <form action="{{ url('user/add-appointment') }}" method="post" id="addAppointForm">
                    @csrf
                    <div class="popupForm">
                        <div class="popupHeading">
                            <h2>Book your Appointment</h2>
                        </div>
                        <div class="form-group mb-3">
                            <select class="form-control js-example-basic-single" id="user_id" value="" name="user_id" onclick="this.setAttribute('value', this.value);">
                                <option value="">User</option>
                                @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select> 
                        </div>
                        <div class="form-group mb-3">
                            <input type="hidden" name="price" value="" id="serviceInputPrice">
                            <select class="form-control" id="valueAddedServices" value="" name="value_added_service_id" onclick="this.setAttribute('value', this.value);">
                                <option value="">Value Added Services</option>
                                @foreach($services as $service)
                                <option value="{{ $service->id }}">{{ $service->service_name }}</option>
                                @endforeach
                            </select> 
                        </div>

                        <div class="form-group mb-3">
                            <input type="text" class="form-control form-calendar2 appDate" id="datepicker" placeholder="Appointment Date" name="date" value="" id="appDate">
                        </div>

                        <div class="form-group mb-3">
                            <input type="text" class="form-control form-time" placeholder="Appointment Time" name="time" id="appTime">
                            <!-- <label for="appTime" class="floating-label">Appointment Time</label> -->
                        </div> 

                        <div class="form-group mb-3">
                            <input type="text" class="form-control" placeholder="Any Further Remarks" name="remarks" id="remarks">
                        </div>
                        <div class="form-group mb-3">
                            <select class="form-control" id="payment_mode" value="" name="payment_mode">
                            <option value="">Payment Mode</option>
                            <option value="Cash">Cash</option>
                            <option value="Cheque">cheque</option>
                            <option value="Online">online</option>
                            <option value="Others">others</option>
                            </select>
                        </div>

                        <div class="termsCheck">
                            <div class="customCheckbox">
                                <input type="checkbox" name="terms" id="terms" name="terms" value="1">
                                <label for="terms">&nbsp;</label>
                            </div>
                            <span>I agree to the <a href="javascript:void(0);">Privacy Policy</a>, <a href="javascript:void(0);">Terms of services</a> and <a href="javascript:void(0);">Cookie Policy</a> of this Website</span>
                        </div>
                    </div><!--//popupForm-->
                    <div class="addAppBottomBox">
                        <div class="row align-items-center">
                            <div class="col-sm-6">
                                <div class="reportTotalVal" id="servicePrice">
                                    <!--<span>You need to Pay</span> <span id="servicePrice"></span>-->
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">                                 
                                <button class="btn btn-primary" type="button" id="addAppointButton">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->

<!--appointment detail popup-->
<div class="modal fade urban-planning-popup approval-appointment" id="approval-appointment-popup" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close" data-dismiss="modal"></button>
            <div class="modal-body suggestAnotherSchedule">

            </div>
        </div>
    </div>
</div>
<!--appointment detail popup-->

<!-- Urban Planning Discussion-popup Start-->
<div id="urban-planning-popup" class="mfp-with-anim mfp-hide urban-planning-popup">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12">
                <ul>
                    <li>
                        <h2>Urban Planning Discussion</h2>
                        <span class="dateTime">Monday, 06 October, 08:00 am to 08:30 am </span>
                        <!--//threeDotMenu Start-->
                        <div class="dropdown">
                            <button class="btn" type="button" data-toggle="dropdown"></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="JavaScript:void(0);">Link</a></li>
                                <li><a href="JavaScript:void(0);">Link</a></li>
                            </ul>
                        </div>
                        <!--//threeDotMenu End-->
                    </li>
                    <li>
                        <span class="joinMeeting">Join the Meeting </span>
                        <a href="javascript:void(0);">https://us02web.zoom.us/j/303164402?pwd=WXg4M3lQUTh6NDZlS3lPQUlkMGNXZz09</a>
                    </li>
                    <li>
                        <label>Meeting ID: <span>303 164 402</span></label>
                        <label>Password: <span>095215</span></label>
                    </li>
                </ul>
                <footer>
                    Please join the meeting 10 minutes before
                </footer>
            </div><!--//.col-12 -->           
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Urban Planning Discussion-popup End -->




<!-- Suggest Another Time Popup Start-->
<div id="suggest-another-time-popup" class="mfp-with-anim mfp-hide suggestAnotherTime">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12">
                <h2>Suggest Another Time</h2>
            </div><!--//.col-12 -->  

            <div class="col-12">
                <form>
                    <div class="form-filds">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <span class="has-float-label">
                                        <input class="form-control" id="datepicker" type="text" placeholder=" "/>
                                        <label for="datepicker">Appointment Date</label>
                                    </span>
                                </div>
                            </div><!-- //col-12 -->

                            <div class="col-12">
                                <div class="form-group">
                                    <span class="has-float-label">
                                        <input class="form-control" id="timepicker" type="text" placeholder=""/>
                                        <label for="timepicker">Appointment Time</label>
                                    </span>
                                </div>
                            </div><!-- //col-12 -->

                            <div class="col-12">
                                <div class="form-group mb-3">
                                    <span class="has-float-label">
                                        <input class="form-control" id="remarks" type="text" placeholder=" "/>
                                        <label for="remarks">Any Further Remarks</label>
                                    </span>
                                </div>
                            </div><!-- //col-12 -->

                            <div class="col-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="privacyPolicy" name="privacyPolicy">
                                    <label class="custom-control-label" for="privacyPolicy">I agree to the <span> Privacy Policy, Terms of services</span> and <span> Cookie Policy</span> of this Website</label>
                                </div>
                            </div><!--//col-12-->

                            <div class="col-12">
                                <button class="btn btn-primary js-btn-next" type="button" title="Next">Submit</button>
                            </div><!--//col-12-->
                        </div><!--//.row-->
                    </div><!--//form-filds-->
                </form><!--//form-->
            </div><!-- //.col-12-->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Suggest Another Time Popup End -->

<!-- Thank You Modal -->
<div class="modal fade thankyou" id="myModal-thankyou" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close" data-dismiss="modal"></button>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h1><img src="{{ asset('adminassets/images/svg-icons/success-icon.svg')}}" alt=""></h1>
                        <h2>Thankyou for Assigning Expert</h2>
                        <span>Expert will get notification to get report done</span>
                        <!-- <a href="javascript:void(0);" class="btn btn-primary">Ok</a> -->
                        <input type="submit" class="btn btn-primary" value="Ok" data-dismiss="modal">
                    </div><!-- //.col-12 -->
                </div><!--/row-->
            </div>
        </div>
    </div>
</div>
<div class="modal fade thankyou" id="myModal-thankyouError" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close" data-dismiss="modal"></button>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <h1><img src="{{ asset('adminassets/images/svg-icons/success-icon.svg')}}" alt=""></h1>

                        <span>Something went wrong, Please try again later!</span>
                        <!-- <a href="javascript:void(0);" class="btn btn-primary">Ok</a> -->
                        <input type="submit" class="btn btn-primary" value="Ok" data-dismiss="modal">
                    </div><!-- //.col-12 -->
                </div><!--/row-->
            </div>
        </div>
    </div>
</div>