@extends('admin.layouts.master')
@section('title')
Your Land Listing
@endsection
@section('class')
reportsBody land-listing
@endsection
@section('content')
<h2>Request Land Listing</h2> 
<!-- //Nav tabs Start -->
@include('admin.land.navtab')
<!-- //Nav tabs End -->
<!-- //Search Form Div Start -->
@include('admin.land.__common_filter')
<!-- //search-form -->
<!-- //Tab panes Start -->
<div class="valueAddedServices">
    <div class="tab-pane" id="requestLandlisting">
        <div class="row">
            @forelse($requestLands as $requestland)
            <div class="col-lg-6 col-12">
                <div class="card inactiveLand">
                    <div class="card-body">
                        <div>
                            <h5>{{$requestland->land_name}}<img src="{{ asset('adminassets/images/svg-icons/land-listing-active-icon.svg') }}" alt=""></h5>
                            <span>{{$requestland->address}}</span>
                        </div><!-- //div-->
                        <div class="d-flex justify-content-end align-items-start">
                            @if($requestland->status == 2)
                            <span class="badge badge-success">Requested</span><!--//badge-success-->
                            @endif
                            <!--//threeDotMenu Start-->
                            <div class="dropdown action-btn ">
                                <button class="btn" type="button" data-toggle="dropdown"></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="JavaScript:void(0);" class="landEditForm" data-id="{{ $requestland->id }}">Edit</a></li>
                                    <li><a href="{{ url('admin/deleteland/'.$requestland->id) }}" class="landDeleteForm" data-id="{{ $requestland->id }}">Delete</a></li>
                                 <li><a href="{{ asset('storage/'.$requestland->image) }}" target="_blank">Sazra Plan</a></li>
                                </ul>
                            </div>
                            <!--//threeDotMenu End-->
                        </div><!-- //div-->
                        <div>
                            <span class="landPrice">₹ {{ $requestland->max_price }} {{ $requestland->price_unit }}</span>
                        </div><!-- //div-->                                                    
                        <div class="bottom-border"></div><!-- //div-->


                        <div class="row w-100 no-gutters">
                            <div class="col-6">
                                @if($requestland->wifi  == 1 || $requestland->parking == 1)
                                <div class="featFacilities">
                                    <span class="featFaTitle">Facilities :</span>
                                    @if($requestland->parking == 1)
                                    <span class="faci-icon parking-icon"></span>
                                    @endif
<!--                                    <span class="faci-icon noSmooking-icon"></span>-->
                                    @if($requestland->wifi == 1)
                                    <span class="faci-icon wifi-icon"></span>
                                    @endif
                                </div>
                                @endif
                            </div><!--//col-6-->

                            <div class="col-6"> 
                                <ul class="day-view">
                                    <li>{{ $requestland->created_at->diffForHumans() }}</li>
                                    <!--<li>{{ $requestland->views ?? 0 }} views</li>-->
                                </ul>
                            </div><!--//col-6-->
                        </div><!--//row-->

                    </div><!-- //card-body -->
                </div><!-- //card inactiveLand-->
            </div><!-- //col-lg-6 col-12-->
            @empty         
            <div class="col-12">
                <div class="no-content-msg">   
                    No Property Available 
                </div>
            </div>
            @endforelse
        </div><!-- //row row_20 -->
        {{$requestLands->links()}}
    </div><!--//tab-pane id=requestLandlisting -->
</div><!-- //Tab panes End -->


@endsection
@section('modal')              
<!-- see-Graph-popup -->
<div id="seeGraph-popup" class="mfp-with-anim mfp-hide seeGraph-popup">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-6"><h2>Views on your lisitng</h2></div><!-- //.col-6 -->
            <div class="col-6 showBy">
                <div class="dropdown">
                    <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="selected">Month</span><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Week</a></li>
                        <li><a href="#">Month</a></li>
                        <li><a href="#">Year</a></li>
                    </ul>
                </div><!-- //dropdown -->
                <span class="float-right pt-2">Show by</span>
            </div><!-- //showBy-->
            <div class="col-12">
                <div class="viwe-graph">
                    <img src="{{ asset('adminassets/images/graph.png') }}" alt="" class="w-100">
                </div>
            </div><!-- //col-12-->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //see-Graph-popup -->

<!-- Schedule-Visit-popup -->
<div id="schedule-Visit-popup" class="mfp-with-anim mfp-hide schedule-Visit">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12"><h2>Views on your lisitng</h2></div><!-- //.col-12 -->
            <div class="col-12">
                <form>
                    <div class="scheduleVisit">
                        <div class="selectExpart">
                            <div class="form-group">
                                <select class="form-control" id="reportTypeExpert">
                                    <option>Select Expert</option>
                                    <option>Abhit Bhatia</option>
                                    <option>Manish Bhandari</option>
                                    <option>Vipin Vindal</option>
                                </select>
                            </div><!-- //form-group -->
                        </div><!-- //selectExpart-->
                        <div class="selectDate">
                            <div class="form-group">
                                <input class="form-control" id="datepicker1" type="text" placeholder="Select Date"/>
                            </div><!-- //form-group -->
                        </div><!--//selectDate-->
                        <div class="selectTime">
                            <div class="form-group input-group">
                                <input class="form-control" id="timepicker" type="text" placeholder="Select Time"/>
                            </div>
                        </div><!--//selectTime-->
                        <div class="scheduleVisit-btn">
                            <input type="submit" class="btn btn-primary" value="Schedule">
                        </div><!-- //scheduleVisit-btn-->
                    </div><!--//scheduleVisit-->
                </form>
            </div><!-- //.col-12 -->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Schedule-Visit-popup -->

@endsection


@section('custom-script')
@endsection
