<!-- //Search Form Div Start -->
<div class="searchForm">
    <!-- <a href="javascript:void(0);" class="addLandListing-btn" data-toggle="modal" data-target="#addLandListing">{{ __('Add Land Listing') }}</a> -->
    <a href="javascript:void(0);" class="open-mobile-search">{{ __('Search') }}</a>
    @php
    if(request()->is('admin/land-listing')){
    $url = url('admin/land-listing');
    }else if(request()->is('admin/active-land-listing')){
    $url = url('admin/active-land-listing');
    }else if(request()->is('admin/inactive-land-listing')){
    $url = url('admin/inactive-land-listing');
    }else if(request()->is('admin/request-land-listing')){
    $url = url('admin/request-land-listing');
    }else if(request()->is('admin/expire-land-listing')){
    $url = url('admin/expire-land-listing');
    }
    @endphp
    <form action="{{ $url }}" method="POST">
        @csrf
        <span>Search</span>
        <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
        <ul class="d-flex flex-wrap">
            <li>
                <div class="form-group">
                    <input type="text" name="name" class="form-control" placeholder="Land Name" value="{{$name ?? ''}}" id="Name">
                </div><!-- //form-group -->
            </li>
            <li>
                <div class="form-group">
                    <select class="form-control" id="business_type" name="business_type">
                        <option value="">{{ __('Land Type') }}</option>
                        <option value="Commercial" @if($businessType=='Commercial' ) {{ __('selected') }} @endif>{{ __('Commercial') }}</option>
                        <option value="Industrial" @if($businessType=='Industrial' ) {{ __('selected') }} @endif>{{ __('Industrial') }}</option>
                        <option value="Institutional" @if($businessType=='Institutional' ) {{ __('selected') }} @endif>{{ __('Institutional') }}</option>
                        <option value="Recreational" @if($businessType=='Recreational' ) {{ __('selected') }} @endif>{{ __('Recreational') }}</option>
                        <option value="Mixed_use" @if($businessType=='Mixed_use' ) {{ __('selected') }} @endif>{{ __('Mixed_use') }}</option>
                        <option value="Agricultural" @if($businessType=='Agricultural' ) {{ __('selected') }} @endif>{{ __('Agricultural') }}</option>
                        <option value="Others" @if($businessType=='Others' ) {{ __('selected') }} @endif>{{ __('Others') }}</option>
                    </select>
                </div><!-- //form-group -->
            </li>
            <li>
                <div class="form-group">
                    <input type="text" name="address" class="form-control" placeholder="Address " value="{{$address ?? ''}}" id="land-area">
                </div><!-- //form-group -->
            </li>
            <li>
                <div class="form-group">
                    <input class="form-control" id="filterDatepicker" type="text" name="created_at" value="{{ !empty($startDate) ? Carbon\Carbon::parse($startDate)->format('d-m-Y') : '' }}" placeholder="Date" />
                </div><!-- //form-group -->
            </li>
            <li>
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
                    </div>
                </div>
            </li>
        </ul><!-- //ul -->
    </form><!-- //form -->
</div><!-- //search-form -->
<script>
    $(document).ready(function() {
        formReset = function() {
            window.location.href = "<?php url('admin/land-listing'); ?>";
        }
        // datepicker
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#filterDatepicker').datepicker({
            // maxDate: today,
            format: 'dd-mm-yyyy'
        });
    })
</script>