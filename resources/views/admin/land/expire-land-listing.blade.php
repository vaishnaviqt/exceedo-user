@extends('admin.layouts.master')
@section('title')
Your Land Listing
@endsection
@section('class')
reportsBody land-listing
@endsection
@section('content')

<h2>Expire Land Listing</h2>

<!-- //Nav tabs Start -->
@include('admin.land.navtab')
<!-- //Nav tabs End -->
@include('admin.land.__common_filter')
<!-- //Tab panes Start -->
<div class="valueAddedServices">
    <div class="tab-pane" id="inactiveLandlisting">
        <div class="row">
            @forelse($expireLands as $inactiveland)
            <div class="col-lg-6 col-12">
                <div class="card inactiveLand">
                    <div class="card-body">
                        <div>
                            <h5>{{$inactiveland->land_name }}<img src="{{ asset('adminassets/images/svg-icons/land-listing-active-icon.svg') }}" alt=""></h5>
                            <span>{{$inactiveland->address}}</span>
                        </div><!-- //div-->
                        <div class="d-flex justify-content-end align-items-start">
                            <span class="badge badge-success">{{ __('Expired') }}</span><!--//badge-success-->
                            <!--//threeDotMenu Start-->
                            <div class="dropdown action-btn">
                                <button class="btn" type="button" data-toggle="dropdown"></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="JavaScript:;" class="landResubmit" data-id="{{ $inactiveland->id }}">Resubmit</a></li>
                                </ul>
                            </div>
                            <!--//threeDotMenu End-->
                        </div><!-- //div-->
                        <div>
                            <span class="landPrice">₹ {{ $inactiveland->max_price }} {{ $inactiveland->price_unit }}</span>
                        </div><!-- //div-->                                                    
                        <div class="bottom-border"></div><!-- //div-->

                        <div class="row w-100 no-gutters">
                            <div class="col-6">
                                @if($inactiveland->wifi  == 1 || $inactiveland->parking == 1)
                                <div class="featFacilities">
                                    <span class="featFaTitle">Facilities :</span>
                                    @if($inactiveland->parking == 1)
                                    <span class="faci-icon parking-icon"></span>
                                    @endif
                                    <!--<span class="faci-icon noSmooking-icon"></span>-->
                                    @if($inactiveland->wifi == 1)
                                    <span class="faci-icon wifi-icon"></span>
                                    @endif
                                </div>
                                @endif
                            </div><!--//col-6-->

                            <div class="col-6"> 
                                <ul class="day-view">
                                    <li>{{ $inactiveland->created_at->diffForHumans() }}</li>
                                    <li>{{ $inactiveland->views ?? 0 }} views</li>
                                </ul>
                            </div><!--//col-6-->
                        </div><!--//row-->
                    </div><!-- //card-body -->
                </div><!-- //card inactiveLand-->
            </div><!-- //col-lg-6 col-12-->
            @empty
            <div class="col-12">
                <div class="no-content-msg">   
                    No Property Available 
                </div>
            </div>
            @endforelse
        </div><!-- //row row_20 -->
        {{$expireLands->links()}}
    </div><!--//tab-pane id=inactiveLandlisting -->
</div><!-- //Tab panes End -->


@endsection
@section('modal')              
<!-- see-Graph-popup -->
<div id="seeGraph-popup" class="mfp-with-anim mfp-hide seeGraph-popup">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-6"><h2>Views on your lisitng</h2></div><!-- //.col-6 -->
            <div class="col-6 showBy">
                <div class="dropdown">
                    <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="selected">Month</span><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Week</a></li>
                        <li><a href="#">Month</a></li>
                        <li><a href="#">Year</a></li>
                    </ul>
                </div><!-- //dropdown -->
                <span class="float-right pt-2">Show by</span>
            </div><!-- //showBy-->
            <div class="col-12">
                <div class="viwe-graph">
                    <img src="{{ asset('adminassets/images/graph.png') }}" alt="" class="w-100">
                </div>
            </div><!-- //col-12-->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //see-Graph-popup -->

<!-- Schedule-Visit-popup -->
<div id="schedule-Visit-popup" class="mfp-with-anim mfp-hide schedule-Visit">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12"><h2>Views on your lisitng</h2></div><!-- //.col-12 -->
            <div class="col-12">
                <form>
                    <div class="scheduleVisit">
                        <div class="selectExpart">
                            <div class="form-group">
                                <select class="form-control" id="reportTypeExpert">
                                    <option>Select Expert</option>
                                    <option>Abhit Bhatia</option>
                                    <option>Manish Bhandari</option>
                                    <option>Vipin Vindal</option>
                                </select>
                            </div><!-- //form-group -->
                        </div><!-- //selectExpart-->
                        <div class="selectDate">
                            <div class="form-group">
                                <input class="form-control" id="datepicker1" type="text" placeholder="Select Date"/>
                            </div><!-- //form-group -->
                        </div><!--//selectDate-->
                        <div class="selectTime">
                            <div class="form-group input-group">
                                <input class="form-control" id="timepicker" type="text" placeholder="Select Time"/>
                            </div>
                        </div><!--//selectTime-->
                        <div class="scheduleVisit-btn">
                            <input type="submit" class="btn btn-primary" value="Schedule">
                        </div><!-- //scheduleVisit-btn-->
                    </div><!--//scheduleVisit-->
                </form>
            </div><!-- //.col-12 -->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Schedule-Visit-popup -->

@endsection
@section('custom-script')
<script>
    $(document).ready(function () {
        $('.landResubmit').on('click', function () {
            var id = $(this).data('id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "property-resubmit",
                type: "post",
                data: {id: id},
                success: function (data)
                {
                    if (data.success) {
                        swal(data.msg)
                                .then((value) => {
                                    location.reload();
                                });
                    } else {
                        swal(data.msg);
                    }
                },
            });
        });
    });
</script>
@endsection

