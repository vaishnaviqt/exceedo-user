<form id="propertyStoreForm">
    <div class="popupHeading">
        <h2>Edit Land Listing</h2>
    </div>

    <div class="addLand">
        <div>
            <div class="form-group">
                <label for="Name">Land Name <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="land_name" value="{{ $property->land_name }}" placeholder="Enter Land Name" id="landName">
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group">
                <label for="uniqueId">Unique Id <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="unique_id" placeholder="Enter Unique Id" id="uniqueId" value="{{ $property->unique_id }}">
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group land_price">
                <label for="number">Land Price <span class="text-danger">*</span></label>
                <input type="number" min="0" class="form-control" name="land_price" placeholder="Enter Price" id="landPrice" value="{{ $property->max_price }}">
                <select class="form-control" id="reportType" name="price_unit">
                    <option value="lakh" @if($property->price_unit == 'lakh') {{ __('selected') }} @endif>Lakh</option>
                    <option value="cr" @if($property->price_unit == 'cr') {{ __('selected') }} @endif>Cr.</option>
                </select>
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group">
                <label for="number">Size of Land <span>(in Acres)</span><span class="text-danger">*</span></label>
                <input type="number" min="0" class="form-control" placeholder="Land Size" value="{{ $property->size_of_land }}" name="size_of_land" id="sizeLand">
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group">
                <label for="nearbyPlace">Nearby Place <span class="text-danger">*</span></label>
                <input type="text" class="form-control" placeholder="Enter Nearby Palace" value="{{ $property->nearby_place }}" name="nearby_place" id="nearbyPlace">
            </div>
        </div>
        <!--//div-->
        <div class="flex-100">
            <div class="form-group">
                <label for="user_address">Address <span class="text-danger">*</span></label>
                <input type="hidden" class="form-control" name="id" id="propertyId" value="{{ $property->id }}">
                <input type="hidden" class="form-control" name="latitute" id="latitute" value="{{ $property->latitute }}">
                <input type="hidden" class="form-control" name="longitude" id="longitude" value="{{ $property->longitude }}">
                <input type="text" class="form-control" placeholder="" name="address" id="user_address" value="{{ $property->address }}">
            </div>
        </div>
        <!--//div-->
        <div class="flex-100">
            <div class="form-group">
                <label for="description">Description <span class="text-danger">*</span></label>
                <textarea id="description" cols="30" rows="10" name="descritpion" class="form-control">{{ $property->descritpion }}</textarea>
            </div>
        </div>
        <!--//div-->

        <div>
            <div class="form-group">
                <label for="agentName">Agent Name <span class="text-danger">*</span></label>
                <select class="form-control" id="agentName" name="user_id">
                    <option value="">Select Agent</option>
                    @foreach($agents as $agent)
                    <option value="{{ $agent->id }}" @if($agent->id == $property->user_id) {{__('selected') }} @endif>{{ $agent->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group">
                <label for="agentMobile">Agent Mobile Number</label>
                <input type="text" class="form-control" placeholder="" value="{{ isset($property->agent->mobile) ? $property->agent->mobile : '' }}" id="agentMobile" disabled>
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group">
                <label for="user_pan">Pan No <span class="text-danger">*</span></label>
                <input class="form-control" name="user_pan" id="user_pan" type="text" value="{{ $property->pan }}" placeholder=" ">
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group" style="display:none;">
                <label for="session1_start_time">Start Time</label>
                <input class="form-control" name="session1_start_time" id="session1_start_time" type="text" value="{{ $property->session1_start_time }}" placeholder=" ">
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group" style="display:none;">
                <label for="session1_end_time">End Time</label>
                <input class="form-control" name="session1_end_time" id="session1_end_time" type="text" value="{{ $property->session1_end_time }}" placeholder=" ">
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group" style="display:none;">
                <label for="session2_start_time">Start Time </label>
                <input class="form-control" name="session2_start_time" id="session2_start_time" type="text" value="{{ $property->session2_start_time }}" placeholder=" ">
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group" style="display:none;">
                <label for="session2_end_time">End Time</label>
                <input class="form-control" name="session2_end_time" id="session2_end_time" type="text" value="{{ $property->session2_end_time }}" placeholder=" ">
            </div>
        </div>
        <!--//div-->


        <div>
            <div class="form-group" style="display:none;">
                <label for="user_pan">Est. Payment Per Month <span class="text-danger">*</span></label>
                <input class="form-control" name="est_payment" id="est_payment" type="text" value="{{ $property->est_payment }}" placeholder=" ">
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group landSize">
                <div class="selectRadio w-100">
                    <label>Facilities ? </label>
                    <div class="d-flex flex-row ">
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" id="wifi" name="wifi" value="1" @if($property->wifi == 1) {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="wifi">Wifi</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" id="parking" name="parking" value="1" @if($property->parking == 1) {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="parking">Parking</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group landSize">
                <div class="selectRadio w-100">
                    <label>In both of these, Which one you are ? <span class="text-danger">*</span></label>

                    <div class="d-flex flex-row is_corporate_error">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="company" name="is_corporate" value="company" @if($property->is_corporate == 'company') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="company">Company</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="individual" name="is_corporate" value="individual" @if($property->is_corporate == 'individual') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="individual">Individual</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//div-->
        <div class="gstInputFiled" style="@if($property->is_corporate == 'individual') display: none @endif">
            <div class="form-group">
                <label for="user_gst">GST No (if Applicable)<span class="text-danger clsGstno">*</span></label>
                <input class="form-control" name="user_gst" id="user_gst" value="{{ $property->gst }}" type="text" placeholder=" ">
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group">
                <div class="selectRadio w-100">
                    <label>
                        Do you want to attend the KYL as the role and exclusive Channel Partner to transact your land ?
                        <span class="text-danger">*</span>
                    </label>

                    <div class="d-flex flex-row exclusive_channel_partner_error">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="yes" name="exclusive_channel_partner" value="yes" @if($property->exclusive_channel_partner == 'yes') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="yes">Yes</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="no" name="exclusive_channel_partner" value="no" @if($property->exclusive_channel_partner == 'no') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="no">No</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group">
                <div class="selectRadio w-100" id="radiobox">
                    <label>
                        Then User is promoted to sign exclusive mandate with KYL <span class="text-danger">*</span>
                    </label>

                    <div class="d-flex flex-row exclusive_mandate_error">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="agree" name="exclusive_mandate" value="agree" @if($property->sign_exclusive_mandate == 'agree') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="agree">I agree</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="disagree" name="exclusive_mandate" value="disagree" @if($property->sign_exclusive_mandate == 'disagree') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="disagree">I disagree</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group" style="display:nonne;">
                <div class="selectRadio w-100">
                    <label>
                        Do you want to visit KYL Team to your Land to get verified?
                        <span class="text-danger">*</span>
                    </label>

                    <div class="d-flex flex-row land_to_get_verified_error">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input rdogst" id="yes2" name="land_to_get_verified" value="yes" @if($property->land_to_get_verified == 'yes') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="yes2">Yes</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input rdogst" id="no2" name="land_to_get_verified" value="no" @if($property->land_to_get_verified == 'no') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="no2">No</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group">
                <div class="selectRadio w-100">
                    <label>What would you do with your property ? <span class="text-danger">*</span></label>

                    <div class=" business_type_error">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="Commercial" name="business_type" value="Commercial" @if($property->business_type == 'Commercial') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="Commercial">Commercial</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="Industrial" name="business_type" value="Industrial" @if($property->business_type == 'Industrial') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="Industrial">Industrial</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="Institutional" name="business_type" value="Institutional" @if($property->business_type == 'Institutional') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="Institutional">Lease</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="Recreational" name="business_type" value="Recreational" @if($property->business_type == 'Recreational') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="Recreational">Recreational</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="Mixed_use" name="business_type" value="Mixed_use" @if($property->business_type == 'Mixed_use') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="Mixed_use">Mixed_use</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="Agricultural" name="business_type" value="Agricultural" @if($property->business_type == 'Agricultural') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="Agricultural">Agricultural</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="Others" name="business_type" value="Others" @if($property->business_type == 'Others') {{ __('checked') }} @endif>
                            <label class="custom-control-label" for="Others">Others</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//div-->


        <div class="flex-100 other_images_error">
            <div class="uploadPhoto">
                <input type="file" id="other_images" name="other_images[]" multiple>
                <h4>Upload Photo or Just drag and drop</h4>
                <h5>Property Images</h5>
            </div>
            <!--//uploadPhoto-->
        </div>
        <!--//div-->
        <div class="flex-100">
            <div class="uploadImgSection preview-images-zone uploadOtherImgSection">
                @foreach($property->propertyImage as $key => $image)
                <div class="preview-image preview-show">
                    <a href="javascript:void(0);" class="close_div cancelImageDb">
                        <img src="{{ url('adminassets/images/svg-icons/gray-close-btn.svg') }}" alt="">
                    </a>
                    <img src="{{ asset($image->image) }}" alt="">
                    <input type="hidden" name="oldImages[{{$key}}][image]" value="{{ $image->image }}">
                    <input type="hidden" name="oldImages[{{$key}}][id]" value="{{ $image->id }}">
                </div>
                @endforeach
            </div>
            <!--//uploadImgSection-->
        </div>
        <!--//div-->

        <div class="flex-100">
            <div class="form-group">
                <label for="videoLink">Video Link</label>
                @php
                $videLink = '';
                if(!empty($property->video_link)){
                $videLink = 'https://www.youtube.com/watch?v=' . $property->video_link;
                }

                @endphp
                <input type="text" class="form-control" name="video_link" placeholder="Enter Video Link" id="videoLink" value="{{ $videLink }}">
            </div>
        </div>


        <div class="flex-100 mb-4">
            <div class="uploadPhoto">
                <input type="file" value="{{$property->land_report}}" id="other_report_images" name="other_report">
                <h4>Upload Photo or Just drag and drop</h4>
                <h5>Feasibility Analysis Report</h5>
            </div>
        </div>
        <div class="flex-100">
            @if(!empty($property->feasibility_report))
            <a href="{{ asset($property->feasibility_report) }}" target="_blank">Feasibility report</a>
            @endif
        </div>


        <!--//div-->
        <div class="flex-100">
            <div class="uploadPhoto">
                <input type="file" id="other_sazara_images" name="other_sazara">
                <h4>Upload Photo or Just drag and drop</h4>
                <h5>Land Title Search Report</h5>
            </div>
            <!--//uploadPhoto-->
        </div>

        <!--//div-->
        <div class="flex-100">
            @if(!empty($property->land_report))
            <a href="{{ asset($property->land_report) }}" target="_blank">Land report</a>
            @endif
        </div>

        <!--//uploadImgSection-->
        <!-- </div> -->


        <!--//div-->
        <div>

            <div class="form-group">
                <label for="status">Status <span class="text-danger">*</span></label>
                <select class="form-control" id="status" name="status">
                    <option value="0" @if($property->status == 0) {{__('selected')}} @endif>{{ __('Inactive') }}</option>
                    <option value="1" @if($property->status == 1) {{__('selected')}} @endif>{{ __('Active') }}</option>
                    <option value="2" @if($property->status == 2) {{__('selected')}} @endif>{{ __('Requested') }}</option>
                </select>
            </div>
        </div>
        <!--//div-->
        <div>
            <div class="form-group">
                <div class="selectRadio w-100">
                    <label></label>
                    <div class="d-flex flex-row business_type_error">
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" id="featured" name="featured" value="1" @if($property->featured == 1) {{__('checked')}} @endif>
                            <label class="custom-control-label" for="featured">featured</label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--//div-->

    </div>
    <!--//addLand-->
    <div class="formBtn">
        <button type="button" class="btn btn-primary propertyUpdateButton">Add Listing</button>
    </div>
</form>
<script>
    $(document).ready(function() {

        $('.rdogst').on('click', function() {
            //alert($(this).val());
            if ($(this).val() == "company") {
                $('.clsGstno').show();
            } else if ($(this).val() == "individual") {
                $('.clsGstno').hide();
            }
        });

        $("input[name=exclusive_channel_partner]").on("change", function() {

            var test = $(this).val();
            // alert(test);
            if (test == "yes") {
                $("#radiobox").show();
            } else if (test == "no") {
                $("#radiobox").hide();

            }
        });

        $('#company').on('change', function() {
            alert
            $('.gstInputFiled').css('display', 'block');
        });
        $('#individual').on('change', function() {
            $('.gstInputFiled').css('display', 'none');
        });
    });
</script>