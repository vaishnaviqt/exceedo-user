<style>
    div.pac-container.pac-logo {
        z-index: 99999999999 !important;
    }

    ul.ui-autocomplete {
        z-index: 1100;
    }
</style>
<div class="countCards">
    <div class="row">
        <div class="col-lg-3 col-md-6">
            @php
            $land = 0;
            if(isset($allLand)){
            $land = $allLand->total();
            }else{
            $land = countLand();
            }
            @endphp
            <a class="nav-link @if(request()->is('admin/land-listing')) {{ __('active') }} @endif" href="{{url('admin/land-listing')}}">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/listing-icon.svg') }}" alt="all reports" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{countLand()}}</strong></p>
                        <p>All Land Listing</p>
                    </div>
                </article>
                <!--//countCardArt-->
            </a>
        </div>
        <!--//col-lg-3-->

        <div class="col-lg-3 col-md-6">
            @php
            $active = 0;
            if(isset($activeLand)){
            $active = $activeLand->total();
            }else{
            $active = countLand(1);
            }
            @endphp
            <a class="nav-link @if(request()->is('admin/active-land-listing')) {{ __('active') }} @endif" href="{{ url('admin/active-land-listing') }}">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/active-land-listing-icon.svg') }}" alt="Delivered Reports" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{countLand(1)}}</strong></p>
                        <p>Active Land Listing</p>
                    </div>
                </article>
            </a>
            <!--//countCardArt-->
        </div>
        <!--//col-lg-3-->

        <div class="col-lg-3 col-md-6">
            @php
            $expire = 0;
            if(isset($expireLands)){
            $expire = $expireLands->total();
            }else{
            $expire = countLand(3);
            }
            @endphp
            <a class="nav-link @if(request()->is('admin/expire-land-listing')) {{ __('active') }} @endif" href="{{ url('admin/expire-land-listing')}}">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/inactive-land-listing-icon.svg') }}" alt="Inprogress Reports" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $expire }}</strong></p>
                        <p>Expire land listing</p>
                    </div>
                </article>
            </a>

            <!--//countCardArt-->
        </div>
        <!--//col-lg-3-->

        <div class="col-lg-3 col-md-6">
            @php
            $requested = 0;
            if(isset($requestLands)){
            $requested = $requestLands->total();
            }else{
            $requested = countLand('pending');
            }
            @endphp
            <a class="nav-link @if(request()->is('admin/request-land-listing')) {{ __('active') }} @endif" href="{{ url('admin/request-land-listing')}}">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/request-land-listing-icon.svg') }}" alt="Pending Reports" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ countLand('pending') }}</strong></p>
                        <p>Request Land Lisiting</p>
                    </div>
                </article>
            </a>
            <!--//countCardArt-->
        </div>
        <!--//col-lg-3-->
    </div>
    <!--//row-->
</div>
<!--//countCards-->

<div class="reportNavBar">
    <div class="genReportLink">
        <a href="javascript:void(0);" data-toggle="modal" data-target="#addLandListing" class="addLandListing-btn">{{ __('Add Land Listing') }}</a>
    </div>

    <div class="reportTabs">
        <ul class="nav nav-tabs" style="display:none;">
            <li class="nav-item">
                @php
                $land = 0;
                if(isset($allLand)){
                $land = $allLand->total();
                }else{
                $land = countLand();
                }
                @endphp
                <a class="nav-link @if(request()->is('admin/land-listing')) {{ __('active') }} @endif" href="{{url('admin/land-listing')}}">All Land Listing <span>({{ $land }})</span></a>
            </li>
            <li class="nav-item">
                @php
                $active = 0;
                if(isset($activeLand)){
                $active = $activeLand->total();
                }else{
                $active = countLand(1);
                }
                @endphp
                <a class="nav-link @if(request()->is('admin/active-land-listing')) {{ __('active') }} @endif" href="{{ url('admin/active-land-listing') }}">Active Land Listing <span>({{ $active }})</span></a>
            </li>
            <!--            <li class="nav-item">
                @php
                $inactive = 0;
                if(isset($inactiveLands)){
                $inactive = $inactiveLands->total();
                }else{
                $inactive = countLand('inactive');
                }
                @endphp
                <a class="nav-link @if(request()->is('admin/inactive-land-listing')) {{ __('active') }} @endif" href="{{ url('admin/inactive-land-listing') }}">Inactive land listing <span>({{ $inactive }})</span></a>
            </li>-->
            <li class="nav-item">
                @php
                $requested = 0;
                if(isset($requestLands)){
                $requested = $requestLands->total();
                }else{
                $requested = countLand('pending');
                }
                @endphp
                <a class="nav-link @if(request()->is('admin/request-land-listing')) {{ __('active') }} @endif" href="{{ url('admin/request-land-listing')}}">Request land listing <span>({{ $requested }})</span></a>
            </li>
            <li class="nav-item">
                @php
                $expire = 0;
                if(isset($expireLands)){
                $expire = $expireLands->total();
                }else{
                $expire = countLand(3);
                }
                @endphp
                <a class="nav-link @if(request()->is('admin/expire-land-listing')) {{ __('active') }} @endif" href="{{ url('admin/expire-land-listing')}}">Expire land listing <span>({{ $expire }})</span></a>
            </li>
        </ul>
    </div>
</div>
<!--//reportNavBar-->


<!-- Edit Profile model -->
<div class="modal fade w-600" id="addLandListing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered addLandListing" role="document">
        <div class="modal-content">
            <div class="modal-body p-0">
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="popupForm propertyEditForm">
                    <form id="propertyStoreForm">
                        <div class="popupHeading">
                            <h2>Add Land Listing</h2>
                        </div>

                        <div class="addLand">
                            <div>
                                <div class="form-group">
                                    <label for="Name">User Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="land_name" placeholder="Enter Land Name" id="landName">
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group">
                                    <label for="Name">Unique Id <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="unique_id" placeholder="Enter Unique Id" id="uniqueId">
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group land_price">
                                    <label for="number">Land Price <span class="text-danger">*</span></label>
                                    <input type="number" min="0" class="form-control" name="land_price" placeholder="Enter Price" id="landPrice">
                                    <select class="form-control" id="price_unit" name="price_unit">
                                        <option value="lakh">Lakh/acre</option>
                                        <option value="cr">Cr/acre</option>
                                    </select>
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group landSize">
                                    <label for="number">Size of Land <span>(in Acres)</span><span class="text-danger">*</span></label>
                                    <input type="number" min="0" class="form-control" placeholder="Land Size in acres" name="size_of_land" id="sizeLand">
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group">
                                    <label for="nearbyPlace">Nearby Place <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Nearby Palace" name="nearby_place" id="nearbyPlace">
                                </div>
                            </div>
                            <!--//div-->
                            <div class="flex-100">
                                <div class="form-group">
                                    <label for="user_address">Address <span class="text-danger">*</span></label>
                                    <input type="hidden" class="form-control" name="latitute" id="latitute">
                                    <input type="hidden" class="form-control" name="longitude" id="longitude">
                                    <input type="text" class="form-control" placeholder="" name="address" id="user_address">
                                </div>
                            </div>
                            <!--//div-->
                            <div class="flex-100">
                                <div class="form-group">
                                    <label for="description">Description <span class="text-danger">*</span></label>
                                    <textarea id="description" cols="30" rows="10" name="descritpion" class="form-control"></textarea>
                                </div>
                            </div>
                            <!--//div-->

                            <div>
                                <div class="form-group">
                                    <label for="agentName">Agent Name <span class="text-danger">*</span></label>
                                    <select class="form-control" id="agentName" name="user_id">
                                        <option value="">Select Agent</option>
                                        @foreach($agents as $agent)
                                        <option value="{{ $agent->id }}">{{ $agent->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group">
                                    <label for="agentMobile">Agent Mobile Number</label>
                                    <input type="text" class="form-control" placeholder="" id="agentMobile" disabled>
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group">
                                    <label for="user_pan">Pan No <span class="text-danger">*</span></label>
                                    <input class="form-control" name="user_pan" id="user_pan" type="text" placeholder=" ">
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group" style="display:none;">
                                    <label for="session1_start_time">Start Time </label>
                                    <input class="form-control" name="session1_start_time" id="session1_start_time" type="text" placeholder=" ">
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group" style="display:none;">
                                    <label for="session1_end_time">End Time</label>
                                    <input class="form-control" name="session1_end_time" id="session1_end_time" type="text" placeholder=" ">
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group" style="display:none;">
                                    <label for="session2_start_time">Start Time </label>
                                    <input class="form-control" name="session2_start_time" id="session2_start_time" type="text" placeholder=" ">
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group" style="display:none;">
                                    <label for="session2_end_time">End Time </label>
                                    <input class="form-control" name="session2_end_time" id="session2_end_time" type="text" placeholder=" ">
                                </div>
                            </div>
                            <!--//div-->


                            <div>
                                <div class="form-group" style="display:none;">
                                    <label for="user_pan">Est. Payment Per Month <span class="text-danger">*</span></label>
                                    <input class="form-control" name="est_payment" id="est_payment" type="text" placeholder=" ">
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group landSize">
                                    <div class="selectRadio w-100">
                                        <label>Facilities ? </label>
                                        <div class="d-flex flex-row ">
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" class="custom-control-input" id="wifi" name="wifi" value="1">
                                                <label class="custom-control-label" for="wifi">Wifi</label>
                                            </div>
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" class="custom-control-input" id="parking" name="parking" value="1">
                                                <label class="custom-control-label" for="parking">Parking</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group landSize is_corporate_error">
                                    <div class="selectRadio w-100">
                                        <label>In both of these, Which one you are ? <span class="text-danger">*</span></label>

                                        <div class="d-flex flex-row ">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="company" name="is_corporate" value="company">
                                                <label class="custom-control-label" for="company">Company</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="individual" name="is_corporate" value="individual">
                                                <label class="custom-control-label" for="individual">Individual</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--//div-->
                            <div class="gstInputFiled">
                                <div class="form-group">
                                    <label for="user_gst">GST No (if Applicable)<span class="text-danger clsGstno">*</span></label>
                                    <input class="form-control" name="user_gst" id="user_gst" type="text" placeholder=" ">
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group">
                                    <div class="selectRadio w-100">
                                        <label>
                                            Do you want to attend the KYL as the role and exclusive Channel Partner to transact your land ?
                                            <span class="text-danger">*</span>
                                        </label>

                                        <div class="d-flex flex-row exclusive_channel_partner_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input rdogst" id="yes" name="exclusive_channel_partner" value="yes">
                                                <label class="custom-control-label" for="yes">Yes</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input rdogst" id="no" name="exclusive_channel_partner" value="no">
                                                <label class="custom-control-label" for="no">No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group">
                                    <div class="selectRadio w-100" id="radiobox">
                                        <label>
                                            Then User is promoted to sign exclusive mandate with KYL <span class="text-danger">*</span>
                                        </label>

                                        <div class="d-flex flex-row exclusive_mandate_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="agree" name="exclusive_mandate" value="agree">
                                                <label class="custom-control-label" for="agree">I agree</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="disagree" name="exclusive_mandate" value="disagree">
                                                <label class="custom-control-label" for="disagree">I disagree</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group">
                                    <div class="selectRadio w-100" style="display:none;">
                                        <label>
                                            Do you want to visit KYL Team to your Land to get verified?
                                            <span class="text-danger">*</span>
                                        </label>

                                        <div class="d-flex flex-row land_to_get_verified_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="yes2" name="land_to_get_verified" value="yes">
                                                <label class="custom-control-label" for="yes2">Yes</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="no2" name="land_to_get_verified" value="no">
                                                <label class="custom-control-label" for="no2">No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group">
                                    <div class="selectRadio w-100">
                                        <label>What would you do with your property ? <span class="text-danger">*</span></label>

                                        <div class=" business_type_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Commercial" name="business_type" value="Commercial">
                                                <label class="custom-control-label" for="Commercial">Commercial</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Industrial" name="business_type" value="Industrial">
                                                <label class="custom-control-label" for="Industrial">Industrial</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Institutional" name="business_type" value="Institutional">
                                                <label class="custom-control-label" for="Institutional">Lease</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Recreational" name="business_type" value="Recreational">
                                                <label class="custom-control-label" for="Recreational">Recreational</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Mixed_use" name="business_type" value="Mixed_use">
                                                <label class="custom-control-label" for="Mixed_use">Mixed_use</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Agricultural" name="business_type" value="Agricultural">
                                                <label class="custom-control-label" for="Agricultural">Agricultural</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Others" name="business_type" value="Others">
                                                <label class="custom-control-label" for="Others">Others</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--//div-->


                            <div class="flex-100 other_images_error">
                                <div class="uploadPhoto">
                                    <input type="file" id="other_images" name="other_images[]" accept="image/png, image/gif, image/jpeg,image/png" multiple>
                                    <h4>Upload Photo or Just drag and drop</h4>
                                    <h5>Property Images</h5>
                                </div>
                                <!--//uploadPhoto-->
                            </div>
                            <!--//div-->
                            <div class="flex-100">
                                <div class="uploadImgSection preview-images-zone uploadOtherImgSection">
                                </div>
                                <!--//uploadImgSection-->
                            </div>
                            <!--//div-->


                            <!-- <div class="flex-100">
                                <div class="form-group ">
                                    <label for="">Video Link</label>
                                    <input type="file" class="form-control" name="video_link" placeholder="Enter Video Link" id="video_link">
                                </div>
                            </div>
                            <div class="flex-100">
                                <div class="uploadImgSection preview-images-zone uploadOthervideoSection">
                                </div>
                        
                            </div> -->

                            <div class="flex-100 mb-4">
                                <div class="uploadPhoto">
                                    <input type="file" id="other_report_images" name="other_report" multiple>
                                    <h4>Upload Photo or Just drag and drop</h4>
                                    <h5>Feasibility Analysis Report</h5>
                                </div>
                                <!--//uploadPhoto-->
                            </div>
                            <div class="flex-100">
                                <div class="uploadImgSection uploadReportImgSection preview-images-zone">
                                </div>
                                <!--//uploadImgSection-->
                            </div>
                            <div class="flex-100 mb-4 mt-4">
                                <div class="uploadPhoto">
                                    <input type="file" id="other_sazara_images" name="other_sazara">
                                    <h4>Upload Photo or Just drag and drop</h4>
                                    <h5>Land Title Search Report</h5>
                                </div>
                                <!--//uploadPhoto-->
                            </div>


                            <!--//div-->
                            <div class="flex-100">
                                <div class="uploadImgSection uploadSazaraImgSection preview-images-zone">
                                </div>
                                <!--//uploadImgSection-->
                            </div>
                            <!--//div-->

                            <div>
                                <div class="form-group">
                                    <label for="status">Status <span class="text-danger">*</span></label>
                                    <select class="form-control" id="status" name="status">
                                        <option value="0">{{ __('Inactive') }}</option>
                                        <option value="1">{{ __('Active') }}</option>
                                        <option value="2">{{ __('Requested') }}</option>
                                    </select>
                                </div>
                            </div>
                            <!--//div-->
                            <div>
                                <div class="form-group">
                                    <div class="selectRadio w-100">
                                        <label></label>
                                        <div class="d-flex flex-row business_type_error">
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" class="custom-control-input" id="featured" name="featured" value="1">
                                                <label class="custom-control-label" for="featured">featured</label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--//div-->
                        </div>
                        <!--//addLand-->
                        <div class="formBtn">
                            <button type="button" class="btn btn-primary propertyStoreButton">Add Listing</button>
                        </div>
                    </form>
                </div>
                <!--//popupForm-->
            </div>
            <!--//modal-body-->
        </div>
    </div>
</div>
<!--//modal-->
<script>
    $(document).ready(function() {

        $('.rdogst').on('click', function() {
            //alert($(this).val());
            if ($(this).val() == "company") {
                $('.clsGstno').show();
            } else if ($(this).val() == "individual") {
                $('.clsGstno').hide();
            }
        });

        $("input[name=exclusive_channel_partner]").on("change", function() {

            var test = $(this).val();
            // alert(test);
            if (test == "yes") {
                $("#radiobox").show();
            } else if (test == "no") {
                $("#radiobox").hide();

            }
        });


        var start_step = 30;
        $("#session1_start_time").timepicker({
            'step': start_step,
            'timeFormat': 'H:i'
        });
        $("#session1_end_time").timepicker({
            'step': start_step,
            'timeFormat': 'H:i'
        });
        $("#session2_start_time").timepicker({
            'step': start_step,
            'timeFormat': 'H:i'
        });
        $("#session2_end_time").timepicker({
            'step': start_step,
            'timeFormat': 'H:i'
        });
        $('.addLandListing-btn').on('click', function() {
            $("#propertyStoreForm")[0].reset()
        });
        //        land detail edit
        $('.landEditForm').on('click', function() {
            var id = $(this).data('id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "property-detail-edit",
                type: "post",
                data: {
                    id: id
                },
                success: function(data) {
                    if (data.html) {
                        document.getElementById('other_images').addEventListener('change', readImage, false);
                        document.getElementById('other_sazara_images').addEventListener('change', readSazaraImage, false);
                        document.getElementById('other_report_images').addEventListener('change', readReportImage, false);
                        console.log(data.html);
                        $('.propertyEditForm').html(data.html);
                        $('.addLandListing-btn').click();

                        var start_step = 30;
                        $("#session1_start_time").timepicker({
                            'step': start_step,
                            'timeFormat': 'H:i'
                        });
                        $("#session1_end_time").timepicker({
                            'step': start_step,
                            'timeFormat': 'H:i'
                        });
                        $("#session2_start_time").timepicker({
                            'step': start_step,
                            'timeFormat': 'H:i'
                        });
                        $("#session2_end_time").timepicker({
                            'step': start_step,
                            'timeFormat': 'H:i'
                        });
                        document.getElementById('other_sazara_images').addEventListener('change', readSazaraImage, false);
                        document.getElementById('other_report_images').addEventListener('change', readReportImage, false);
                        document.getElementById('other_images').addEventListener('change', readImage, false);
                        //                        $.magnificPopup.open({
                        //                            items: {
                        //                                src: '#addLandListing',
                        //                                callbacks: {
                        //                                    beforeOpen: function () {
                        //                                        this.st.mainClass = this.st.el.attr('data-effect');
                        //                                    }
                        //                                },
                        //                                type: 'inline'
                        //                            }
                        //
                        //                        });
                    }
                },
            });
        });
        $('body').on('click', '.propertyUpdateButton', function() {
            var id = $(this).data('id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "property-detail-update",
                type: "post",
                data: $('#propertyStoreForm').serialize(),
                success: function(data) {
                    if (data.success) {
                        swal({
                            title: "Good job!",
                            text: data.msg,
                            icon: "success",
                        }).then((value) => {
                            location.reload();
                        });
                    } else {

                        swal({
                            title: "error!",
                            text: data.msg,
                            icon: "error",
                        })
                    }
                },
                error: function(errorResponse) {
                    //                $('#service-enquiry').prop("disabled", false);
                    //                $('#service-enquiry').text('Submit');
                    $('.error-span').remove();
                    $.each(errorResponse.responseJSON.errors, function(field_name, error) {
                        if (field_name == 'land_price') {
                            $(document).find('[name=' + field_name + ']').parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'size_of_land') {
                            $(document).find('[name=' + field_name + ']').parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'is_corporate') {
                            $(document).find('[name=' + field_name + ']').parent().parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'exclusive_mandate') {
                            $(document).find('[name=' + field_name + ']').parent().parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'exclusive_channel_partner') {
                            $(document).find('[name=' + field_name + ']').parent().parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'land_to_get_verified') {
                            $(document).find('[name=' + field_name + ']').parent().parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'business_type') {
                            $(document).find('[name=' + field_name + ']').parent().parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'other_images') {
                            $('.other_images_error').append('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else {
                            $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        }
                    })
                }
            });
        });

        $('body').on('click', '.cancelImageDb', function() {
            $(this).parent().remove();
        });
        $('body').on('click', '.landDeleteForm', function(e) {
            e.preventDefault();
            var link = $(this).attr('href');
            swal({
                    text: "Are you sure want to delete appointment?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location.href = link;
                    } else {
                        return false;
                    }
                });
        });
        $('#agentName').on('change', function() {
            var agentId = $(this).val();
            $.ajax({
                url: "show-agent-mobile/" + agentId,
                type: "get",
                success: function(data) {
                    console.log(data.mobile);
                    if (data) {
                        $('#agentMobile').val(data.mobile);
                    }
                }
            });
        });
        //land detail store with ajax
        $('body').on('click', '.propertyStoreButton', function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "property-detail-store",
                type: "post",
                data: $('#propertyStoreForm').serialize(),
                success: function(data) {
                    console.log(data);
                    if (data.success) {
                        swal({
                            title: "Good job!",
                            text: data.msg,
                            icon: "success",
                        }).then((value) => {
                            location.reload();
                        });
                    } else {

                        swal({
                            title: "error!",
                            text: data.msg,
                            icon: "error",
                        })
                    }
                },
                error: function(errorResponse) {
                    //                $('#service-enquiry').prop("disabled", false);
                    //                $('#service-enquiry').text('Submit');
                    $('.error-span').remove();
                    $.each(errorResponse.responseJSON.errors, function(field_name, error) {
                        if (field_name == 'land_price') {
                            $(document).find('[name=' + field_name + ']').parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'size_of_land') {
                            $(document).find('[name=' + field_name + ']').parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'is_corporate') {
                            $('.is_corporate_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'exclusive_channel_partner') {
                            $(document).find('[name=' + field_name + ']').parent().parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'land_to_get_verified') {
                            $(document).find('[name=' + field_name + ']').parent().parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'business_type') {
                            $(document).find('[name=' + field_name + ']').parent().parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'other_images') {
                            $('.other_images_error').append('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else {
                            $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        }
                    })
                }
            });
        });
    });


    if ($('#other_images').length) {
        document.getElementById('other_images').addEventListener('change', readImage, false);
        //$(".preview-images-zone").sortable();
    }

    if ($('#video_link').length) {
        document.getElementById('video_link').addEventListener('change', video_link, false);
        //$(".preview-images-zone").sortable();
    }
    if ($('#other_sazara_images').length) {
        document.getElementById('other_sazara_images').addEventListener('change', readSazaraImage, false);
        //$(".preview-images-zone").sortable();
    }
    if ($('#other_report_images').length) {
        document.getElementById('other_report_images').addEventListener('change', readReportImage, false);
        //$(".preview-images-zone").sortable();
    }

    var num = 1;
    var tmp_files = new Array();

    function readReportImage() {
        if (window.File && window.FileList && window.FileReader) {

            var files = event.target.files; //FileList object
            // alert(files);
            //   console.log(files);

            var output = $(".uploadReportImgSection");
            for (let i = 0; i < files.length; i++) {
                var file = files[i];
                tmp_files.push(file);
                // console.log(tmp_files)
            }

            //$("#other_images").val('');

            for (let j = 0; j < tmp_files.length; j++) {
                var file_set = tmp_files[j];
                var fileSize = files[j].size;
                console.log(fileSize);
                if (fileSize > 2000000) {
                    jQuery('#valid-img.invalid-feedback').html('The File size cannot be more than 2 MB.');
                    jQuery('#valid-img.invalid-feedback').delay(2000).fadeOut(1000);
                } else {
                    var picReader = new FileReader();
                    picReader.addEventListener('load', function(event) {
                        var picFile = event.target;
                        console.log(picFile.result);
                        var image = "<?php echo url('adminassets/images/svg-icons/gray-close-btn.svg') ?>";
                        var html = `
                                <div class="preview-image preview-show-` + num + `">
                                <a href="javascript:void(0);" class="close_div image-cancel" data-no="` + num + `">
                            <img src="` + image + `" alt="">
                            </a>
                            <img id="pro-ss-img-` + num + `" src="` + picFile.result + `" alt="">
                            <input type="hidden" name="other_report_images" id="other_report_images` + j + `" value="` + picFile.result + `">
                            </div>
                            `;
                        output.append(html);
                        num = num + 1;
                        if (output.find('.preview-image').length > 0) {
                            output.removeClass('d-none');
                        }
                    });
                    picReader.readAsDataURL(file_set);
                }

            }
            tmp_files = new Array();
        } else {
            console.log('Browser not support');
        }
    }

    var num = 1;
    var tmp_files = new Array();

    function readSazaraImage() {
        if (window.File && window.FileList && window.FileReader) {

            var files = event.target.files; //FileList object
            console.log(files);
            alert(files);

            var output = $(".uploadSazaraImgSection");
            for (let i = 0; i < files.length; i++) {
                var file = files[i];
                tmp_files.push(file);
                // console.log(tmp_files)
            }

            //$("#other_images").val('');

            for (let j = 0; j < tmp_files.length; j++) {
                var file_set = tmp_files[j];
                var fileSize = files[j].size;
                console.log(fileSize);
                if (fileSize > 2000000) {
                    jQuery('#valid-img.invalid-feedback').html('The File size cannot be more than 2 MB.');
                    jQuery('#valid-img.invalid-feedback').delay(2000).fadeOut(1000);
                } else {
                    var picReader = new FileReader();
                    picReader.addEventListener('load', function(event) {
                        var picFile = event.target;
                        console.log(picFile.result);
                        var image = "<?php echo url('adminassets/images/svg-icons/gray-close-btn.svg') ?>";
                        var html = `
                                <div class="preview-image preview-show-` + num + `">
                                <a href="javascript:void(0);" class="close_div image-cancel" data-no="` + num + `">
                            <img src="` + image + `" alt="">
                            </a>
                            <img id="pro-sazara-img-` + num + `" src="` + picFile.result + `" alt="">
                            <input type="hidden" name="other_sazara_images" id="other_sazara_images` + j + `" value="` + picFile.result + `">
                            </div>
                            `;
                        output.append(html);
                        num = num + 1;
                        if (output.find('.preview-image').length > 0) {
                            output.removeClass('d-none');
                        }
                    });
                    picReader.readAsDataURL(file_set);
                }

            }
            tmp_files = new Array();
        } else {
            console.log('Browser not support');
        }
    }

    $('body').on('click', '.image-cancel', function() {
        let no = $(this).data('no');
        $(".preview-image.preview-show-" + no).remove();
        if ($(".uploadOtherImgSection").find('.preview-image').length == 0) {
            $(".uploadOtherImgSection").addClass('d-none');
        }
        if ($(".uploadReportImgSection").find('.preview-image').length == 0) {
            $(".uploadReportImgSection").addClass('d-none');
        }
    });

    var num = 1;
    var tmp_files = new Array();

    function readImage() {
        if (window.File && window.FileList && window.FileReader) {

            var files = event.target.files; //FileList object
            //   console.log(files);

            var output = $(".uploadOtherImgSection");
            for (let i = 0; i < files.length; i++) {
                var file = files[i];
                tmp_files.push(file);
                // console.log(tmp_files)
            }

            //$("#other_images").val('');

            for (let j = 0; j < tmp_files.length; j++) {
                var file_set = tmp_files[j];
                var fileSize = files[j].size;
                console.log(fileSize);
                if (fileSize > 2000000) {
                    jQuery('#valid-img.invalid-feedback').html('The File size cannot be more than 2 MB.');
                    jQuery('#valid-img.invalid-feedback').delay(2000).fadeOut(1000);
                } else {
                    var picReader = new FileReader();
                    picReader.addEventListener('load', function(event) {
                        var picFile = event.target;
                        var image = "<?php echo url('adminassets/images/svg-icons/gray-close-btn.svg') ?>";
                        var html = `
                <div class="preview-image preview-show-` + num + `">
                <a href="javascript:void(0);" class="close_div image-cancel" data-no="` + num + `">
            <img src="` + image + `" alt="">
            </a>
            <img id="pro-img-` + num + `" src="` + picFile.result + `" alt="">
            <input type="hidden" name="new_images[]" id="old-image` + j + `" value="` + picFile.result + `">
            </div>
            `;
                        output.append(html);
                        num = num + 1;
                        if (output.find('.preview-image').length > 0) {
                            output.removeClass('d-none');
                        }
                    });
                    picReader.readAsDataURL(file_set);
                }

            }
            tmp_files = new Array();
        } else {
            console.log('Browser not support');
        }
    }

    var num = 1;
    var tmp_files = new Array();

    function video_link() {
        if (window.File && window.FileList && window.FileReader) {

            var files = event.target.files; //FileList object
            //   console.log(files);

            var output = $(".uploadOthervideoSection");
            for (let i = 0; i < files.length; i++) {
                var file = files[i];
                tmp_files.push(file);
                // console.log(tmp_files)
            }

            //$("#video_link").val('');

            for (let j = 0; j < tmp_files.length; j++) {
                var file_set = tmp_files[j];
                var fileSize = files[j].size;
                console.log(fileSize);
                alert(fileSize);
                if (fileSize > 10000000) {
                    jQuery('#valid-img.invalid-feedback').html('The File size cannot be more than 4 MB.');
                    jQuery('#valid-img.invalid-feedback').delay(2000).fadeOut(1000);
                } else {

                    var picReader = new FileReader();
                    picReader.addEventListener('load', function(event) {
                        var picFile = event.target;
                        // alert(picFile.result);
                        var image = "<?php echo url('adminassets/images/svg-icons/gray-close-btn.svg') ?>";
                        var html = `
                <div class="preview-image preview-show-` + num + `">
                <a href="javascript:void(0);" class="close_div image-cancel" data-no="` + num + `">
            <video src="' + image + '" width="320" height="240" controls> </video>
            </a>
            <video id="pro-img-` + num + `" src="` + picFile.result + `"  width="320" height="240" controls> </video>
            <input type="hidden" name="video_link" id="old-image` + j + `" value="` + picFile.result + `">
            </div>
            `;
                        //    <video src="' + picFile + '" width="320" height="240" controls> </video>
                        output.append(html);
                        num = num + 1;
                        if (output.find('.preview-image').length > 0) {
                            output.removeClass('d-none');
                        }
                    });
                    picReader.readAsDataURL(file_set);
                }

            }
            tmp_files = new Array();
        } else {
            console.log('Browser not support');
        }
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_map_key')}}&libraries=places&callback=initAutocomplete" async defer></script>
<script>
    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('user_address'));
        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
        //    autocomplete.setFields(['address_component']);
        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener('place_changed', onPlaceChanged);
    }

    function onPlaceChanged() {
        var place = autocomplete.getPlace();
        document.getElementById('latitute').value = place.geometry.location.lat();
        document.getElementById('longitude').value = place.geometry.location.lng();
    }
</script>