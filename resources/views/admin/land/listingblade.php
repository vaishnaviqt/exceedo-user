@extends('admin.layouts.master')
@section('title')
Your Land Listing
@endsection
@section('class')
reportsBody land-listing
@endsection
@section('content')
<h2>All Land Listing</h2> 
@include('flash-message')
@include('admin.land.navtab')
<!-- //Search Form Div Start -->
@include('admin.land.__common_filter')
<!-- //search-form -->
<!-- //Search Form Div End -->
<!-- //Tab panes Start -->
<div class="valueAddedServices">
    <div class="tab-pane active" id="allLandlisting">
        <div class="card-deck">
            <div class="row row_20">
                @forelse($allLands as $land )
                <div class="col-lg-6 col-12">
                    <div class="card activeLand">
                        <div class="card-body">
                            <div>
                                <h5>{{ $land->land_name }}<img src="{{ asset('adminassets/images/svg-icons/land-listing-active-icon.svg') }}" alt=""></h5>
                                <span>{{$land->address}}</span>
                            </div><!-- //div-->
                            <div class="d-flex justify-content-end align-items-start">
                                @if($land->status == 1)
                                <span class="badge badge-success">{{ __('Active') }}</span><!--//badge-success-->
                                @elseif($land->status == 2)
                                <span class="badge badge-warning">{{ __('Requested') }}</span>
                                @elseif($land->status == 0)
                                <span class="badge badge-danger">{{ __('Inactive') }}</span>
                                @else
                                <span class="badge badge-danger">{{ __('Expired') }}</span>
                                @endif
                                <!--//threeDotMenu Start-->
                                @if($land->status != 3)
                                <div class="dropdown action-btn">
                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="JavaScript:void(0);" class="landEditForm" data-id="{{ $land->id }}">Edit</a></li>
                                        <li><a href="{{ url('admin/deleteland/'.$land->id) }}" class="landDeleteForm" data-id="{{ $land->id }}">Delete</a></li>  
                                        <li><a href="{{ asset('storage/'.$land->image) }}" target="_blank">Sazra Plan</a></li>
                               
                                    </ul>
                                </div>
                                @endif
                                <!--//threeDotMenu End-->
                            </div>
                            <div>
                                <span class="landPrice">₹ {{ $land->max_price }} {{ $land->price_unit }}</span>
                            </div><!-- //div-->
                            <div>
                                <span class="listingExpiring">Your Listing expiring date is {{ date('d M, Y', strtotime("+6 months", strtotime($land->created_at))) }} </span>
                            </div><!-- //div-->
                            <div class="bottom-border"></div><!-- //div-->
                            <div class="row w-100 no-gutters">
                                <div class="col-6">
                                    @if($land->wifi  == 1 || $land->parking == 1) 
                                    <div class="featFacilities">
                                        <span class="featFaTitle">Facilities :</span>
                                        @if($land->parking == 1)
                                        <span class="faci-icon parking-icon"></span>
                                        @endif
                                        <!--<span class="faci-icon noSmooking-icon"></span>-->
                                        @if($land->wifi == 1)
                                        <span class="faci-icon wifi-icon"></span>
                                        @endif
                                    </div> 
                                    @endif
                                </div><!--//col-6-->
                                <div class="col-6">
                                    <ul class="day-view">
                                        <li>{{ $land->created_at->diffForHumans() }}</li>
                                        <li>{{ $land->views ?? 0 }} views</li>
                                    </ul>
                                </div><!--//col-6-->
                            </div><!--//row-->
                        </div><!-- //card-body -->
                    </div><!-- //card activeLand -->
                </div><!-- //col-lg-6 col-12-->
                @empty
                <div class="col-12">
                    <div class="no-content-msg">   
                        No Property Available 
                    </div>
                </div>                
                @endforelse
            </div><!-- //row row_20 -->
            {{$allLands->links()}}
        </div><!-- //card-deck -->
    </div><!--//tab-pane id=allLandlisting -->
</div><!-- //Tab panes End -->

@endsection

@section('modal')
<!-- see-Graph-popup -->
<div id="seeGraph-popup" class="mfp-with-anim mfp-hide seeGraph-popup">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-6"><h2>Views on your lisitng</h2></div><!-- //.col-6 -->
            <div class="col-6 showBy">
                <div class="dropdown">
                    <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="selected">Month</span><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Week</a></li>
                        <li><a href="#">Month</a></li>
                        <li><a href="#">Year</a></li>
                    </ul>
                </div><!-- //dropdown -->
                <span class="float-right pt-2">Show by</span>
            </div><!-- //showBy-->
            <div class="col-12">
                <div class="viwe-graph">
                    <img src="{{ asset('adminassets/images/graph.png') }}" alt="" class="w-100">
                </div>
            </div><!-- //col-12-->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //see-Graph-popup -->

<!-- Schedule-Visit-popup -->
<div id="schedule-Visit-popup" class="mfp-with-anim mfp-hide schedule-Visit">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12"><h2>Views on your lisitng</h2></div><!-- //.col-12 -->
            <div class="col-12">
                <form>
                    <div class="scheduleVisit">
                        <div class="selectExpart">
                            <div class="form-group">
                                <select class="form-control" id="reportTypeExpert">
                                    <option>Select Expert</option>
                                    <option>Abhit Bhatia</option>
                                    <option>Manish Bhandari</option>
                                    <option>Vipin Vindal</option>
                                </select>
                            </div><!-- //form-group -->
                        </div><!-- //selectExpart-->
                        <div class="selectDate">
                            <div class="form-group">
                                <input class="form-control" id="datepicker1" type="text" placeholder="Select Date"/>
                            </div><!-- //form-group -->
                        </div><!--//selectDate-->
                        <div class="selectTime">
                            <div class="form-group input-group">
                                <input class="form-control" id="timepicker" type="text" placeholder="Select Time"/>
                            </div>
                        </div><!--//selectTime-->
                        <div class="scheduleVisit-btn">
                            <input type="submit" class="btn btn-primary" value="Schedule">
                        </div><!-- //scheduleVisit-btn-->
                    </div><!--//scheduleVisit-->
                </form>
            </div><!-- //.col-12 -->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Schedule-Visit-popup -->

@endsection
@section('custom-script')

<script>


    var placeSearch, autocomplete;

    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
                document.getElementById('user_address'));

        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
//    autocomplete.setFields(['address_component']);
        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener('place_changed', onPlaceChanged);
    }

    function onPlaceChanged() {
        var place = autocomplete.getPlace();
        document.getElementById('latitute').value = place.geometry.location.lat();
        document.getElementById('longitude').value = place.geometry.location.lng();
    }

    $(document).ready(function () {
        $('.addLandListing-btn').on('click', function () {
            $('#addLandListing').modal('show');


        });
    });

</script>

<script type="text/javascript">
    $(document).ready(function () {
        //change colour when radio is selected
        $('#whoYouAre input:radio').change(function () {
            // Only remove the class in the specific `box` that contains the radio
            $('.custom-radio.highlight').removeClass('highlight');
            $(this).closest('.custom-radio').addClass('highlight');

            $('.btn-primary.opacity1').removeClass('opacity1');
            $('.btn-primary').addClass('opacity1');
        });
    });
</script>
@endsection