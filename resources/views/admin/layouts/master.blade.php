<!DOCTYPE html>
<html lang="en">
    <head>
              <style>

    .loader{
        position: fixed;
        top:0;
        left:0;
        right: 0;
        bottom:0;
        display:flex;
        align-items:center;
        justify-content:center;
        z-index: 9999;
        background: #ffffffa6;
    }
    .loader img{
        width:80px;
    }
</style>
        @include('admin.partials.head')
    </head>
    <body>  
 
<div class="loader d-none">
    <img src="{{ asset('images/Spin-1s-200px.gif') }}">
</div>
        <div class="wrapper">
        <div class="sidebar">
            @include('admin.partials.sidebar')
        </div><!--//sidebar--> 

            <div class="main">
                <header class="header"> 
                    @include('admin.partials.header')
                </header><!--//header-->

                <section class="pageContainer  @yield('class')">
                    @yield('content')

                </section><!--//pageContainer-->

                <footer class="footer">
                    @include('admin.partials.footer')
                </footer>    
            </div><!--//main-->
        </div><!--//wrapper--> 


        <div class="modal fade" id="join_secondaryadmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content"> 
                    <div class="modal-body p-0"> 
                        <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                        <div class="row no-gutters">
                            <div class="col-md-12">
                                <div class="loginPageBox" id="signup">
                                    <div class="" id="myTabContentSignup">
                                        <div class="alert alert-success alert-block user-msg" style="display: none;">
                                            <button type="button" class="close"  data-dismiss="alert">Ã—</button>
                                        </div>
                                        <form name="frm_admin_signup" id="frm_admin_signup" method="post">
                                            @csrf
                                            <div class="popupForm">  

                                                <div class="popupHeading">
                                                    <h2>Add Secondary Admin</h2>
                                                </div> 

                                                <input type="hidden" class="form-control"  name="id" id="id">
                                                <div class="form-group floating-field">
                                                    <input type="text" class="form-control" placeholder="Full Name" name="full_name" id="full_name">
                                                    <label for="full_name" class="floating-label">Full Name</label>
                                                </div>

                                                <div class="form-group floating-field">
                                                    <input type="text" class="form-control" placeholder="Email id" name="email" id="email">
                                                    <label for="email" class="floating-label">Email id</label>
                                                </div>

                                                <div class="form-group floating-field mobile-field">
                                                    <input type="text" class="form-control" placeholder="Mobile Number" name="mobile_number" id="mobile_number">
                                                    <label for="mobile" class="floating-label">Mobile Number</label>
                                                    <span class="mobileCode">+91</span>
                                                </div>  

                                                <div class="formBtn">
                                                    <button type="submit" id="btn_admin" class="btn btn-primary">Submit</button>
                                                </div>

                                            </div>
                                        </form>

                                    </div>

                </div>
            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- secondary-Admin-popup Start-->
        <div id="secondary-Admin-popup" class="mfp-with-anim mfp-hide secondary-Admin-popup">
            <div class="popup-body">
                <button title="Close (Esc)" type="button" class="mfp-close"></button>
                <div class="row">
                    <div class="col-12 editSecondaryAdmin">
                        <h3>Add Secondary Admin</h3>
                        <form id="addSecondaryAdminForm">
                            <ul class="addSecondaryAdmin">
                                <li>
                                    <div class="form-group">
                                        <select class="form-control" id="permissionExpertId" name="expert_id">
                                            <option value="">Select Expert</option> 
                                            @php
                                            $experts = expertListShow();
                                            @endphp
                                            @foreach($experts as $expert)
                                            <option value="{{ $expert->id }}">{{ $expert->name }}</option>
                                            @endforeach
                                        </select>
                                    </div><!-- //form-group -->
                                </li>
                                <li>
                                    <select id="multiple-checkboxes2" multiple="multiple" name="manager_id[]">
                                        @php
                                        $managers = managersList();
                                        @endphp
                                        @foreach($managers as $manager)
                                        <option value="{{ $manager->id }}">{{ $manager->name }}</option>
                                        @endforeach
                                    </select>
                                </li>
                                <li><input type="button" value="Add" class="btn btn-primary" id="addSecondaryButton"></li>
                            </ul>
                        </form><!--//FORM-->                    
                    </div><!-- //.col-12 -->                
                </div><!--/row-->
            </div><!-- //popup-body --> 
        </div>
        <!-- //secondary-Admin-popup End -->  
        @yield('modal')
        @include('admin.partials.javascripts')

        @section('custom-script')
        @show

        <script>
            $(document).ready(function () {
                $('#addSecondaryButton').on('click', function () {
                    $('.loader').toggleClass('d-none');
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('admin/add-secondary-admin') }}",
                        data: $("#addSecondaryAdminForm").serialize(),
                        dataType: "json",
                        success: function (data) {
                            $('.loader').toggleClass('d-none');
                            if (data.success) {
                                $('#addSecondaryAdminForm')[0].reset();
                                swal(data.msg, {icon: "success", })
                                        .then((value) => {
                                            location.reload();
                                        });
                            } else {
                                swal(data.msg, {icon: "error", })
                            }
                        },
                        error: function (errorResponse) {
                            $('.loader').toggleClass('d-none');
                            $.each(errorResponse.responseJSON.errors, function (field_name, error) {

                                $(document).find('[name=' + field_name + ']').after('<span class="text-strong text-danger error-span "  role="alert">' + error + '</span>');
                            })
                        }
                    });
                })

            })
            // Inline popups
            $('#inline-popups').magnificPopup({
                delegate: 'a',
                removalDelay: 500,
                callbacks: {
                    beforeOpen: function () {
                        this.st.mainClass = this.st.el.attr('data-effect');
                    }
                },
                midClick: true
            });
            // Multiple-Checkboxes
            $(document).ready(function () {
                $('#multiple-checkboxes, #multiple-checkboxes2').multiselect({
                    includeSelectAllOption: false,
                });
            });
            $('.secondary-Admin-show').click(function () {
                $('.secondary-Admin-edit').css('display', 'flex');
                $('ul.secondary-Admin-show').css('display', 'none');
            });
            $('.save-secondary-Admin').click(function () {
                $('.secondary-Admin-edit').css('display', 'none');
                $('ul.secondary-Admin-show').css('display', 'flex');
            });
        </script>

        <script type="text/javascript">
            function editAdmin(data) {
                $(document).find("span.error-span").remove();
                $('.alert-block').hide();
                $('#id').val(data['id']);
                $('#full_name').val(data['name']);
                $('#email').val(data['email']);
                $('#mobile_number').val(data['mobile']);
            }
            ;
            
            $('#add_admin').on('click', function (e) {
            e.preventDefault();
            $('#frm_admin_signup')[0].reset();
            $(document).find("span.error-span").remove();
            });


            var data = '{{Session('
            success
            ')}}';
                    var err = '{{Session('
            error
            ')}}';
            if (data) {

            swal({
            text: data,
                    icon: "success",
                }).then((value) => {
                    location.reload();
                });
            }

            if (err) {
            swal({
            text: err,
                    icon: "error",
                }).then((value) => {
                    location.reload();
                });
            }
        </script>

    </body>
</html>