@extends('admin.layouts.master')
@section('title')
  Experts
@endsection
@section('content')
<h2>Value added services</h2>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
           <form id="frm_value_added_service" method="POST" enctype="multipart/form-data" action="{{ route('admin.valueservices.store') }}">
            @csrf
            <ul class="d-flex flex-wrap form-fields" id="services-inline-popups">
                <li>
                    <div class="form-group">
                        <input type="text" class="form-control" name="service_name" placeholder="Service Name" id="service_name">
                         @if ($errors->has('service_name'))
                             <span class="invalid-feedback" role="alert">{{ $errors->first('service_name') }}</span>
                        @endif
                    </div><!-- //form-group -->
                </li>
                <li>
                    <div class="form-group">
                        <textarea class="form-control" rows="3" placeholder="Service Description" name="description" id="description"></textarea>
                    </div><!-- //form-group -->
                </li>
                <li>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="service_icon" id="service_icon">
                        <label class="custom-file-label" for="service_icon">Service Icon</label>
                    </div>
                </li>
                <li>
                     <button type="submit" class="btn btn-primary w-100 mt-4">Add Services</button>
                </li>
            </ul><!-- //ul -->
           </form>
 @endsection