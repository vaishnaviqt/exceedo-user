@extends('admin.layouts.master')
@section('title')
Users
@endsection
@section('content')
<style>

    .loader{
        position: fixed;
        top:0;
        left:0;
        right: 0;
        bottom:0;
        display:flex;
        align-items:center;
        justify-content:center;
        z-index: 9999;
        background: #ffffffa6;
    }
    .loader img{
        width:80px;
    }
</style>
<div class="loader d-none">
    <img src="{{ asset('images/Spin-1s-200px.gif') }}">
</div>

<div> 
    <div class="pageTitle d-flex align-items-center">
        <h2>Users</h2>

        <div class="sortDropdown">
            <label>Show</label>
            <div class="dropdown custom-dropdown">
                <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle min-w-60">
                    <span class="selected">{{ $paginate ?? 10 }}</span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" class="userPaginate" data-value="10">10</a></li>
                    <li><a href="#"  class="userPaginate" data-value="25">25</a></li>
                    <li><a href="#"  class="userPaginate" data-value="50">50</a></li>
                    <li><a href="#"  class="userPaginate" data-value="100">100</a></li>
                </ul>
            </div><!-- //dropdown -->
            <!--//paginate form-->
            <form method="post" id="paginateSubmitForm">
                @csrf
                <input type="hidden" name="paginate" id="paginateHiddenFiled">
            </form>
            <!--end Paginate Form-->
        </div><!--//sortDropdown-->
    </div><!--//pageTitle-->
    @include('flash-message')
    <div class="row">
        <div class="div col-lg-9">
            <!-- //Search Form Div Start -->
            <div class="searchForm">
                <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                   
                <form  method="post" action="{{ route('admin.users') }}" id="frm_experts_search">
                    @csrf
                    <span>Search</span>
                    <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
                    <ul class="d-flex flex-wrap">
                        <li>
                            <div class="form-group">
                                <input type="text" name="user_name" class="form-control" placeholder="Name"  value="{{$user_name ?? ''}}" id="Name">
                            </div><!-- //form-group -->
                        </li>

                        <li>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="Email"  value="{{$email ?? ''}}" id="email">
                            </div><!-- //form-group -->
                        </li>

                        <li>
                            <div class="form-group">
                                <input type="number" name="mobile" class="form-control" placeholder="Mobile Number"  value="{{$mobile ?? ''}}" id="mobile">
                            </div><!-- //form-group -->
                        </li>
                        <li>
                            <button class="btn btn-primary" data-toggle="tab" onclick="formReset()">Reset</button>
                        </li>
                        <li>
                            <button type="submit" class="btn btn-primary" name="">Search</button>
                        </li> 
                    </ul><!-- //ul -->
                </form><!-- //form -->
            </div><!-- //search-form -->
            <!-- //Search Form Div End -->
        </div>
        <div class="div col-lg-3">
            <div class="reportNavBar pt-2 mb-2">                        
                <div class="genReportLink">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#join_user" id="add_user">Add User</a>
                </div>  
            </div><!--//reportNavBar-->
        </div>
    </div><!--//row-->


    <div class="customTable border">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>User Name</th>
                    <th>Mobile</th>
                    <th>Email Id</th>
                    <th>User Id</th>
                    <th>Date Of Joining</th>
                    <th width="20%"></th>
                </tr>
            </thead>
            <tbody>

                @if (count($users) > 0)
                @foreach($users as $user)
                <tr>
                    <td scope="row">
                        <span class="m_tableTitle">User Name</span>
                        {{ $user->name }}
                    </td>
                    <td>
                        <span class="m_tableTitle">Mobile</span>
                        {{ $user->mobile }}
                    </td>
                    <td>
                        <span class="m_tableTitle">Email Id</span>
                        {{ $user->email }}
                    </td>
                    <td>
                        <span class="m_tableTitle">User Id</span>
                        {{ $user->user_unique_id }}

                        @foreach ($user->roles->pluck('name') as $rle)
                        {{ ucfirst($rle) }}
                        @endforeach
                    </td>
                    <td>
                        <span class="m_tableTitle">Email Id</span>
                        {{ Carbon\Carbon::parse($user->created_at)->format('d M, Y h:i:s') }}
                    </td>
                    <td>
                        <a href="javascript:void(0);" class="deleteRow" data-toggle="modal" data-target="#join_user" onclick="editUser({{$user}})" id="useredit">
                            Edit
                        </a> 
                        <a data-toggle="modal" data-target="#Delete" href="javascript:void(0);" class="deleteRow" onclick="deleteUser({{$user->id}})" >
                            <img src="{{ asset('adminassets/images/svg-icons/delete-icon.svg') }}" alt="">
                        </a>
                        @if($user->user_status==1)
                        <button class="btn btn-primary" onclick="ActiveUser({{$user->id}})">Active</button>
                        @else
                        <button class="btn btn-danger" id="deactive" onclick="DeavtiveUser({{$user->id}})">Deactive</button>
                        @endif
                    </td>
                </tr>
                @endforeach
                @else

                <tr>
                    <td scope="row" colspan="4">Users not found</td>
                </tr>

                @endif
            </tbody>
        </table>

        <div class="col-12">
            {{ $users->links() }}
        </div><!-- //col-12 pagination -->
    </div><!--//payment-table-desktop-->
</div>    
@endsection
@section('modal')
<!-- Modal -->     
<!-- Join User -->
<div class="modal fade" id="join_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="row no-gutters">
                    <div class="col-md-12">
                        <div class="loginPageBox" id="signup">
                            <div class="" id="myTabContentSignup">
                                <div class="alert alert-success alert-block user-msg" style="display: none;">
                                    <button type="button" class="close"  data-dismiss="alert">×</button>
                                </div>
                                <form name="frm_user_signup" id="frm_user_signup" method="post">
                                    @csrf
                                    <div class="popupForm">  
                                        <input type="hidden" class="form-control"  name="id" id="editid">
                                        <div class="form-group floating-field">
                                            <input type="text" class="form-control" placeholder="Full Name" name="full_name" id="editname">
                                            <label for="editname" class="floating-label">Full Name</label>
                                        </div><!-- floating-field -->

                                        <div class="form-group floating-field">
                                            <input type="text" class="form-control" placeholder="Email id" name="email" id="editemail">
                                            <label for="editemail" class="floating-label">Email id</label>
                                        </div><!-- floating-field -->

                                        <div class="form-group floating-field mobile-field">
                                            <input type="text" class="form-control" placeholder="Mobile Number" name="mobile_number" id="editmobile_number">
                                            <label for="editmobile_number" class="floating-label">Mobile Number</label>
                                            <span class="mobileCode">+91</span>
                                        </div><!-- floating-field --> 



                                        <div class="formBtn">
                                            <button type="submit" id="btn_user" class="btn btn-primary">Submit</button>
                                        </div>

                                    </div><!--//popupForm-->
                                </form>

                            </div><!--//tab-content--> 

                        </div><!--//loginPageBox-->
                    </div><!--//col-md-8-->
                </div><!--//row-->
            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->
<!-- Expert delete confirm model window -->

<div class="modal alert-model" id="Delete" data-keyboard="true"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close-bt" data-dismiss="modal">&times;</button>
            <!-- Modal body -->
            <div class="modal-body alert-warning">
                <h4 class="text-warning">Delete User</h4>
                <p class="text-warning">Are you sure you want to delete this User?</p>
                <div class="col-lg-12 text-center">

                    <form name="user_deletion" id="user_deletion" method="post" action="{{ route('admin.delete-user') }}">
                        @csrf
                        <button type="button" class="no-bt" data-dismiss="modal">No</button>
                        <input type="hidden" name="deletion_id" id="deletion_id">
                        <button type="submit" class="yes-bt">Yes</button>
                    </form>
                </div>
            </div>
            <!--//Modal body-->

        </div>
    </div>
</div>
@endsection
@section('custom-script')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
    formReset = function() {
        window.location.href = "<?php url('admin/users'); ?>";
    }

    function ActiveUser(id) {
        var id = id;
        var user_status = 0;
        $.ajax({
            url: "{{route('admin.users-status')}}",
            type: "get",
            data: {
                id: id,
                user_status: user_status
            },
            success: function(data) {
                if (data.success) {
                    location.reload();
                } else {
                    $('.expert-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
                }
            }
        });
    }

    function DeavtiveUser(id) {
        var id = id;
        var user_status = 1;
        $.ajax({
            url: "{{route('admin.users-status')}}",
            type: "get",
            data: {
                id: id,
                user_status: user_status
            },
            success: function(data) {
                if (data.success) {
                    location.reload();
                } else {
                    $('.expert-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
                }
            }
        });
    }
</script>

<script type="text/javascript">
                            function editUser(data){
                            $(document).find("span.error-span").remove();
                            $('.alert-block').hide();
                            $('#editid').val(data['id']);
                            $('#editname').val(data['name']);
                            $('#editemail').val(data['email']);
                            $('#editmobile_number').val(data['mobile']);
                            };
                            function deleteUser(id){

                            $('#deletion_id').val(id);
                            };
                            $('.userPaginate').on('click', function (e) {
                            e.preventDefault();
                            var value = $(this).data('value');
                            $('#paginateHiddenFiled').val(value);
                            $('#paginateSubmitForm').submit();
                            return false;
                            });
                            $('#add_user').on('click', function (e) {
                            e.preventDefault();
                            $('#frm_user_signup')[0].reset();
                            $(document).find("span.error-span").remove();
                            });
                            $.ajaxSetup({
                            headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                            });
                            $('#frm_user_signup').submit(function (e) {
                            e.preventDefault();
                            $(document).find("span.error-span").remove();
                            $('.alert-block').hide();
                            var formData = new FormData(this);
                            $('#btn_user').prop("disabled", true);
                            $('#btn_user').text('Sending...');
                            $('.loader').toggleClass('d-none');
                            $.ajax({
                            type: 'POST',
                                    url: "{{ route('admin.storeuser') }}",
                                    data: $("#frm_user_signup").serialize(),
                                    dataType: "json",
                                    success: function (data) {
                                    $('.loader').toggleClass('d-none');
                                    $('#btn_user').prop("disabled", false);
                                    $('#btn_user').text('Submit');
                                    if (data.success) {
                                    $('#frm_user_signup')[0].reset();
                                    swal(data.msg, {icon: "success", })
                                            .then((value) => {
                                            location.reload();
                                            });
                                    } else {
                                    $('.user-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
                                    }
                                    },
                                    error: function (errorResponse) {
                                    $('.loader').toggleClass('d-none');
                                    $('#btn_user').prop("disabled", false);
                                    $('#btn_user').text('Submit');
                                    $.each(errorResponse.responseJSON.errors, function  (field_name, error) {

                                    $(document).find('[name=' + field_name + ']').after('<span class="text-strong text-danger error-span "  role="alert">' + error + '</span>');
                                    })
                                    }
                            });
                            });

</script>
@endsection
