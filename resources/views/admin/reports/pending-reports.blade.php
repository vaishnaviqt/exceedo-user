@extends('admin.layouts.master')
@section('title')
Your Land Reports
@endsection
@section('class')
reportsBody
@endsection
@section('content') 
<h2>Pending Reports</h2>
@include('admin.reports.__reports-common-header')
<!-- //Tab panes Start -->
<div class="tab-content" id="inline-popups">
    <div class="tab-pane active" id="allReport">
        @forelse($pendingReports as $report)
        <div class="dataTable border mb-3">
            <div class="row">
                <div class="data col-lg-4 col-md-6 mb-1">
                    <h4>
                        <a href="javascript:void(0);">{{ $report->size_of_land }} {{ __('Acres') }} {{ $report->land_name }}</a>
                    </h4>
                    <span>{{ isset($report->user->mobile) ? $report->user->mobile : '' }}</span> | <span>{{ isset($report->user->email) ? $report->user->email : '' }}</span>
                </div><!--//data -->

                <div class="data col-lg-3 col-md-6 mb-1">
                    <label>
                        {{ __('Report :') }}  <span>
                         @if(!empty($report->valueAdded))
                            {{ isset($report->valueAdded->service_name) ? $report->valueAdded->service_name : '' }}
                            @endif
                        </span>
                    </label>
                    <label>
                        {{ __('Status :') }}  <span class="pending">{{ __('Pending') }}</span>
                    </label>
                </div><!--//data -->

                <div class="data col-lg-3 col-md-6 mb-1">
                     @if(empty($report->expert_id) || $report->cancel == 1)
                    <label>
                        Assigned to :  <a href="#select-expert-popup" data-effect="mfp-zoom-in" data-expert="{{ $report->id }}" class="selectExpartAssign">Select the Expert</a>
                    </label>
                    @else
                    <label>
                        Pending expert confirmation
                    </label>
                    @endif
                    <label></label>
                </div><!--//data -->
  
            </div><!-- /.row -->
        </div><!-- //dataTable -->
        @empty
        <div class="no-content-msg"> 
                No Reports Available 
        </div><!--//no-content-msg-->
        @endforelse
    </div>
</div><!-- //Tab panes End -->
{{ $pendingReports->links() }}              
@endsection

@section('modal')


<!-- Delivered Report-popup -->
<div id="deliveredReport-popup" class="mfp-with-anim mfp-hide deliveredReport">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12">
                <div class="delivered-report-title">
                    <h2>Delivered Report</h2>
                    <div id="counter"></div>
                </div><!--//delivered-report-title -->                    
            </div>
            <div class="col-12">

                <div class="deliveredReport-carousel owl-carousel">
                    <div class="deliveredReport-items">
                        <div class="reportBox">
                            <!--//threeDotMenu Start-->
                            <div class="dropdown">
                                <button class="btn" type="button" data-toggle="dropdown"></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                </ul>
                            </div><!-- //dropdown-->
                            <!--//threeDotMenu End-->
                            <div class="media">
                                <img src="{{ asset('adminassets/images/project-map.png') }}" alt="Generic placeholder image">
                                <div class="media-body">
                                    <h5 class="mt-0">21 Acres Land Town</h5>
                                    <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                    <label class="landTitle">Land Title Search</label>
                                    <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                    <div class="expartUpdate">
                                        <h6>
                                            Delivered by : <span>Expert Musolani</span>
                                            <span class="daysAgo">2 days ago</span>
                                        </h6>
                                        <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                    </div><!-- //expartUpdate-->
                                </div><!-- //media-body -->
                            </div><!--//media -->
                            <blockquote class="blockquote blockquote-reverse adminFeedback">
                                <a href="javascript:void(0);" class="edit-block"><img src="{{ asset('adminassets/images/svg-icons/gray-edit-icon.svg') }}" alt=""></a>
                                <header class="blockquote-header">Feeback by Admin</header>
                                <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </blockquote><!--//blockquote-->

                            <blockquote class="blockquote blockquote-reverse blockquote-editComment">
                                <form>
                                    <textarea name="" id="" rows="3" placeholder="Write Feedback"></textarea>
                                    <input type="submit" value="" class="btn-primary"> 
                                </form>
                            </blockquote><!--//blockquote blockquote-editComment-->
                        </div><!-- //reportBox -->
                    </div><!--//deliveredReport-items -->

                    <div class="deliveredReport-items">
                        <div class="reportBox">
                            <!--//threeDotMenu Start-->
                            <div class="dropdown">
                                <button class="btn" type="button" data-toggle="dropdown"></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                </ul>
                            </div><!-- //dropdown-->
                            <!--//threeDotMenu End-->
                            <div class="media">
                                <img src="{{ asset('adminassets/images/project-map.png') }}" alt="Generic placeholder image">
                                <div class="media-body">
                                    <h5 class="mt-0">21 Acres Land Town</h5>
                                    <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                    <label class="landTitle">Land Title Search</label>
                                    <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                    <div class="expartUpdate">
                                        <h6>
                                            Delivered by : <span>Expert Musolani</span>
                                            <span class="daysAgo">2 days ago</span>
                                        </h6>
                                        <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                    </div><!-- //expartUpdate-->
                                </div><!-- //media-body -->
                            </div><!--//media -->
                            <blockquote class="blockquote blockquote-reverse adminFeedback">
                                <a href="javascript:void(0);" class="edit-block"><img src="{{ asset('adminassets/images/svg-icons/gray-edit-icon.svg') }}" alt=""></a>
                                <header class="blockquote-header">Feeback by Admin</header>
                                <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </blockquote><!--//blockquote-->

                            <blockquote class="blockquote blockquote-reverse blockquote-editComment">
                                <form>
                                    <textarea name="" id="" rows="3" placeholder="Write Feedback"></textarea>
                                    <input type="submit" value="" class="btn-primary"> 
                                </form>
                            </blockquote><!--//blockquote blockquote-editComment-->
                        </div><!-- //reportBox -->
                    </div><!--//deliveredReport-items -->

                    <div class="deliveredReport-items">
                        <div class="reportBox">
                            <!--//threeDotMenu Start-->
                            <div class="dropdown">
                                <button class="btn" type="button" data-toggle="dropdown"></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                </ul>
                            </div><!-- //dropdown-->
                            <!--//threeDotMenu End-->
                            <div class="media">
                                <img src="{{ asset('adminassets/images/project-map.png') }}" alt="Generic placeholder image">
                                <div class="media-body">
                                    <h5 class="mt-0">21 Acres Land Town</h5>
                                    <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                    <label class="landTitle">Land Title Search</label>
                                    <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                    <div class="expartUpdate">
                                        <h6>
                                            Delivered by : <span>Expert Musolani</span>
                                            <span class="daysAgo">2 days ago</span>
                                        </h6>
                                        <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                    </div><!-- //expartUpdate-->
                                </div><!-- //media-body -->
                            </div><!--//media -->
                            <blockquote class="blockquote blockquote-reverse adminFeedback">
                                <a href="javascript:void(0);" class="edit-block"><img src="{{ asset('adminassets/images/svg-icons/gray-edit-icon.svg') }}" alt=""></a>
                                <header class="blockquote-header">Feeback by Admin</header>
                                <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </blockquote><!--//blockquote-->

                            <blockquote class="blockquote blockquote-reverse blockquote-editComment">
                                <form>
                                    <textarea name="" id="" rows="3" placeholder="Write Feedback"></textarea>
                                    <input type="submit" value="" class="btn-primary"> 
                                </form>
                            </blockquote><!--//blockquote blockquote-editComment-->
                        </div><!-- //reportBox -->
                    </div><!--//deliveredReport-items -->
                </div><!--//carousel--> 

            </div><!-- //.col-12-->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Delivered Report-popup -->

<!-- Inprogress Report-popup -->
<div id="inprogressReport-popup" class="mfp-with-anim mfp-hide deliveredReport">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12">
                <div class="delivered-report-title inprogress-report-title">
                    <h2>Inprogress Report</h2>
                    <div id="counter"></div>
                </div><!--//delivered-report-title -->                    
            </div>
            <div class="col-12 deliveredReport-carousel owl-carousel">
                <div class="deliveredReport-items">
                    <div class="reportBox">
                        <!--//threeDotMenu Start-->
                        <div class="dropdown">
                            <button class="btn" type="button" data-toggle="dropdown"></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="JavaScript:void(0);">Link</a></li>
                                <li><a href="JavaScript:void(0);">Link</a></li>
                            </ul>
                        </div><!-- //dropdown-->
                        <!--//threeDotMenu End-->
                        <div class="media">
                            <img src="{{ asset('adminassets/images/project-map.png') }}" alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mt-0">21 Acres Land Town</h5>
                                <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                <label class="landTitle">Land Title Search</label>
                                <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                <div class="expartUpdate">
                                    <h6>
                                        Delivered by : <span>Expert Musolani</span>
                                        <span class="daysAgo">2 days ago</span>
                                    </h6>
                                    <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                </div><!-- //expartUpdate-->
                            </div><!-- //media-body -->
                        </div><!--//media -->
                        <blockquote class="blockquote blockquote-reverse blockquote-editComment">
                            <form>
                                <textarea name="" id="" rows="3" placeholder="Write Feedback"></textarea>
                                <input type="submit" value="" class="btn-primary"> 
                            </form>
                        </blockquote><!--//blockquote blockquote-editComment-->
                    </div><!-- //reportBox -->
                </div><!--//deliveredReport-items -->

                <div class="deliveredReport-items">
                    <div class="reportBox">
                        <!--//threeDotMenu Start-->
                        <div class="dropdown">
                            <button class="btn" type="button" data-toggle="dropdown"></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="JavaScript:void(0);">Link</a></li>
                                <li><a href="JavaScript:void(0);">Link</a></li>
                            </ul>
                        </div><!-- //dropdown-->
                        <!--//threeDotMenu End-->
                        <div class="media">
                            <img src="{{ asset('adminassets/images/project-map.png') }}" alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mt-0">21 Acres Land Town</h5>
                                <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                <label class="landTitle">Land Title Search</label>
                                <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                <div class="expartUpdate">
                                    <h6>
                                        Delivered by : <span>Expert Musolani</span>
                                        <span class="daysAgo">2 days ago</span>
                                    </h6>
                                    <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                </div><!-- //expartUpdate-->
                            </div><!-- //media-body -->
                        </div><!--//media -->
                        <blockquote class="blockquote blockquote-reverse blockquote-editComment">
                            <form>
                                <textarea name="" id="" rows="3" placeholder="Write Feedback"></textarea>
                                <input type="submit" value="" class="btn-primary"> 
                            </form>
                        </blockquote><!--//blockquote blockquote-editComment-->
                    </div><!-- //reportBox -->
                </div><!--//deliveredReport-items -->
            </div><!-- //.col-12 deliveredReport-carousel-->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Inprogress Report-popup -->

<!-- Request Report-popup -->
<div id="requestReport-popup" class="mfp-with-anim mfp-hide deliveredReport">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12">
                <div class="delivered-report-title request-report-title">
                    <h2>Request Report</h2>
                    <div id="counter"></div>
                </div><!--//delivered-report-title -->                    
            </div>
            <div class="col-12 deliveredReport-carousel owl-carousel">
                <div class="deliveredReport-items">
                    <div class="reportBox">
                        <!--//threeDotMenu Start-->
                        <div class="dropdown">
                            <button class="btn" type="button" data-toggle="dropdown"></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="JavaScript:void(0);">Link</a></li>
                                <li><a href="JavaScript:void(0);">Link</a></li>
                            </ul>
                        </div><!-- //dropdown-->
                        <!--//threeDotMenu End-->
                        <div class="media">
                            <img src="{{ asset('adminassets/images/project-map.png') }}" alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mt-0">21 Acres Land Town</h5>
                                <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                <label class="landTitle">Land Title Search</label>
                                <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                <div class="expartUpdate">
                                    <h6>
                                        Delivered by : <a href="#select-expert-popup" data-effect="mfp-zoom-in" class="selectExpart">Select the Expert</a>
                                        <span class="daysAgo">2 days ago</span>
                                    </h6>                                                           
                                </div><!-- //expartUpdate-->
                            </div><!-- //media-body -->
                        </div><!--//media -->
                        <blockquote class="blockquote blockquote-reverse">
                            <p class="mb-0">Expert has not uploaded the Report yet</p>
                            <p class="mb-0">When he was uploaded the Report then you can able to comment </p>
                        </blockquote><!--//blockquote blockquote-editComment-->
                    </div><!-- //reportBox -->
                </div><!--//deliveredReport-items -->
            </div><!-- //.col-12 deliveredReport-carousel-->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Request Report-popup -->

<!-- View Report-popup -->
<div id="view-report-popup" class="mfp-with-anim mfp-hide deliveredReport viewReportPopup ">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12 deliveredReport-carousel owl-carousel">
                <div class="map-images">
                    <img src="{{ asset('adminassets/images/view-report-img.png') }}" alt="">
                </div><!--//map-images-->

                <div class="map-images">
                    <img src="{{ asset('adminassets/images/view-report-img.png') }}" alt="">
                </div><!--//map-images-->
            </div><!-- //.col-12 deliveredReport-carousel-->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //View Report-popup -->


@endsection


@section('custom-script')




@endsection