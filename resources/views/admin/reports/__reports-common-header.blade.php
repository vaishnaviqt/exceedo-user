<style>

    .loader{
        position: fixed;
        top:0;
        left:0;
        right: 0;
        bottom:0;
        display:flex;
        align-items:center;
        justify-content:center;
        z-index: 9999;
        background: #ffffffa6;
    }
    .loader img{
        width:80px;
    }
</style>
<div class="loader d-none">
    <img src="{{ asset('images/Spin-1s-200px.gif') }}">
</div>
<div class="countCards">
    <div class="row">
        <div class="col-lg-3 col-md-6">
            @php
            $report = 0;
            if(isset($allReports)){
            $report = $allReports->total();
            }else{
            $report = reportsCount();
            }
            @endphp
            <a class="nav-link @if(request()->is('admin/reports')) {{ __('active') }} @endif" href="{{ url('admin/reports') }}">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/admin-reports-icon.svg') }}" alt="all reports" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ reportsCount() }}</strong></p>
                        <p>All Reports</p>
                    </div>
                </article>
                <!--//countCardArt-->
            </a>
        </div>
        <!--//col-lg-3-->

        <div class="col-lg-3 col-md-6">
            @php
            $delivered = 0;
            if(isset($deliveredReports)){
            $delivered = $deliveredReports->total();
            }else{
            $delivered = reportsCount(1);
            }
            @endphp
            <a class="nav-link @if(request()->is('admin/delivered-reports')) {{ __('active') }} @endif" href="{{ url('admin/delivered-reports') }}">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/delivered-Reports-icon.svg') }}" alt="Delivered Reports" />                                    
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ reportsCount(1) }}</strong></p>
                        <p>Delivered Reports</p>
                    </div>
                </article>
                <!--//countCardArt-->
            </a>

        </div>
        <!--//col-lg-3-->

        <div class="col-lg-3 col-md-6">
            @php
            $inprogress = 0;
            if(isset($inprogressReports)){
            $inprogress = $inprogressReports->total();
            }else{
            $inprogress = reportsCount(2);
            }
            @endphp
            <a class="nav-link @if(request()->is('admin/inprogress-reports')) {{ __('active') }} @endif" href="{{ url('admin/inprogress-reports') }}">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/inprogress-reports-icon.svg') }}" alt="Inprogress Reports" />                                    
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ reportsCount(2) }}</strong></p>
                        <p>Inprogress Reports</p>
                    </div>
                </article>
                <!--//countCardArt-->
            </a>
        </div>
        <!--//col-lg-3-->

        <div class="col-lg-3 col-md-6">
            @php
            $pending = 0;
            if(isset($pendingReports)){
            $pending = $pendingReports->total();
            }else{
            $pending = reportsCount('pending');
            }
            @endphp
            <a class="nav-link @if(request()->is('admin/pending-reports')) {{ __('active') }} @endif" href="{{ url('admin/pending-reports') }}">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/request-reports-icon.svg') }}" alt="Pending Reports" />                                    
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ reportsCount('pending') }}</strong></p>
                        <p>Pending Reports</p>
                    </div>
                </article>
                <!--//countCardArt-->
            </a>
        </div>
        <!--//col-lg-3-->
    </div>
    <!--//row-->
</div>
<!--//countCards-->

<!-- //Nav tabs Start -->
<ul class="nav nav-tabs" style="display:none;">
    <li class="nav-item">
        @php
        $report = 0;
        if(isset($allReports)){
        $report = $allReports->total();
        }else{
        $report = reportsCount();
        }
        @endphp
        <a class="nav-link @if(request()->is('admin/reports')) {{ __('active') }} @endif"  href="{{ url('admin/reports') }}">All REPORTS <span>({{ $report }})</span></a>
    </li>
    <li class="nav-item">
        @php
        $delivered = 0;
        if(isset($deliveredReports)){
        $delivered = $deliveredReports->total();
        }else{
        $delivered = reportsCount(1);
        }
        @endphp
        <a class="nav-link @if(request()->is('admin/delivered-reports')) {{ __('active') }} @endif" href="{{ url('admin/delivered-reports') }}">DELIVERED REPORTS <span>({{ $delivered }})</span></a>
    </li>
    <li class="nav-item">
        @php
        $inprogress = 0;
        if(isset($inprogressReports)){
        $inprogress = $inprogressReports->total();
        }else{
        $inprogress = reportsCount(2);
        }
        @endphp
        <a class="nav-link @if(request()->is('admin/inprogress-reports')) {{ __('active') }} @endif" href="{{ url('admin/inprogress-reports') }}">INPROGRESS REPORTS <span>({{ $inprogress }})</span></a>
    </li>
    <li class="nav-item">
        @php
        $pending = 0;
        if(isset($pendingReports)){
        $pending = $pendingReports->total();
        }else{
        $pending = reportsCount('pending');
        }
        @endphp
        <a class="nav-link @if(request()->is('admin/pending-reports')) {{ __('active') }} @endif" href="{{ url('admin/pending-reports') }}">Pending Reports<span>({{ $pending }})</span></a>
    </li>
</ul>
<!-- //Nav tabs End -->


<div class="genReportLink text-right mb-3">
    <div class="genReportLink">
        <a href="javascript:void(0);" data-toggle="modal" data-target="#addLandListing" class="addLandReport-btn">{{ __('Add Land Report') }}</a>
    </div>
</div>

<!-- //Search Form Div Start -->
<div class="searchForm">
    <a href="javascript:void(0);" class="open-mobile-search">Search</a>
    @if(request()->is('admin/reports'))
    <form action="{{ url('admin/reports') }}" method="post">
        @elseif(request()->is('admin/delivered-reports'))
        <form action="{{ url('admin/delivered-reports') }}" method="post">
            @elseif(request()->is('admin/inprogress-reports'))
            <form action="{{ url('admin/inprogress-reports') }}" method="post">
                @elseif(request()->is('admin/pending-reports'))
                <form action="{{ url('admin/pending-reports') }}" method="post">
                    @endif
                    @csrf
                    <span>Search</span>
                    <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
                    <ul class="d-flex flex-wrap">
                        <li>

                            <div class="form-group">
                                <select class="form-control" id="expertId" name="all_expert_id">
                                    <option value="">Select Expert</option>
                                    {{-- @if(count($experts)) --}}
                                    @foreach($experts as $expert)
                                    <option value="{{ $expert->id }}" @if($expert->id== $name) {{ __('selected') }} @endif>{{ $expert->name }}</option>
                                    @endforeach
                                    {{-- @endif --}}
                                </select>
                            </div>
                        </li>

                        <li>

                            <div class="form-group">
                                <select class="form-control" id="serviceType" name="serviceType">
                                    <option value="">Services</option>
                                    @if(count($services))
                                    @foreach($services as $service)
                                    @php
                                    $selected = '';
                                    if($service->id == $serviceType){
                                    $selected = 'selected';
                                    }
                                    @endphp
                                    <option value="{{ $service->id }}" {{ $selected }}>{{ $service->service_name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </li>

                        <li>
                            @php
                            $states = stateListing();
                            @endphp
                            <div class="form-group">
                                <select class="form-control" id="reportType" name="location">
                                    <option value="">Select State</option>
                                    @foreach($states as $state)
                                    <option value="{{ $state->id }}" @if($location == $state->id) {{ __('selected') }} @endif>{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div><!-- //form-group -->
                        </li>
                        <li>
                            <div class="form-group">
                                <input class="form-control" id="filterDatepicker" type="text" placeholder="Date" name="date" value="{{ !empty($date) ? Carbon\Carbon::parse($date)->format('d-m-Y') : '' }}" />
                            </div><!-- //form-group -->
                        </li>
                        <li>
                            <div class="row row_10">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary">Search</button>
                                </div>
                                <div class="col-6">
                                    <button class="btn btn-primary" data-toggle="tab" onclick="formReset()">Reset</button>
                                </div>
                            </div>                            
                        </li>
                    </ul><!-- //ul -->
                </form><!-- //form -->
                </div><!-- //search-form -->
                <!-- //Search Form Div End -->


                <!-- Select the Expert-popup -->
                <div id="select-expert-popup" class="mfp-with-anim mfp-hide select-expert">
                    <div class="popup-body">
                        <button title="Close (Esc)" type="button" class="mfp-close"></button>
                        <div class="row">
                            <div class="col-12">
                                <h2>Assign Expert</h2>
                            </div><!-- //.col-12 -->
                            <div class="col-12">
                                <form class="reportAssignForm">
                                    <div class="scheduleVisit">
                                        <div class="selectExpart">
                                            <div class="form-group">

                                                <input type="hidden" name="reportId" value="" id="reportId">
                                                <select class="form-control assignToExpert" name="expert_id">
                                                    <option value="">Assigned to Expert</option>
                                                </select>
                                            </div><!-- //form-group -->
                                        </div><!-- //selectExpart-->
                                        <div class="scheduleVisit-btn">
                                            <input type="button" class="btn btn-primary" id="reportAssingSubmitButton" value="Assignee">
                                        </div><!-- //scheduleVisit-btn-->
                                        <div class="scheduleVisit-btn">
                                            <input type="button" class="btn btn-primary" id="searahallExpert" value="All Experts">
                                        </div><!-- //scheduleVisit-btn-->
                                    </div>
                                    <!--//scheduleVisit-->
                                </form>
                            </div><!-- //.col-12 -->
                        </div>
                        <!--/row-->
                    </div><!-- //popup-body --> 
                </div><!-- //Select the Expert-popup -->

                <!-- thankyou-popup-popup -->
                <div id="thankyou-popup" class="mfp-with-anim mfp-hide thankyou">
                    <div class="popup-body">
                        <button title="Close (Esc)" type="button" class="mfp-close"></button>
                        <div class="row">
                            <div class="col-12">
                                <h1><img src="{{ asset('adminassets/images/svg-icons/success-icon.svg') }}" alt=""></h1>
                                <h2>Thankyou for Assigning Expert</h2>
                                <span>Expert will get notification to get report done</span>
                                <!-- <a href="javascript:void(0);" class="btn btn-primary">Ok</a> -->
                                <input type="submit" class="btn btn-primary" value="Ok">
                            </div><!-- //.col-12 -->
                        </div>
                        <!--/row-->
                    </div><!-- //popup-body --> 
                </div><!-- //thankyou-popup-popup -->


                <!-- Select the Expert-popup -->
                <div id="notify-expert-about-report" class="mfp-with-anim mfp-hide select-expert">
                    <div class="popup-body">
                        <button title="Close (Esc)" type="button" class="mfp-close"></button>
                        <div class="row">
                            <div class="col-12">
                                <h2>Report feedback</h2>
                            </div><!-- //.col-12 -->
                            <div class="col-12">
                                <form class="reportFeedbackForm" id="reportFeedbackForm" enctype="multipart/form-data">
                                    <span class="text-danger feedbackFormError"></span>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="report" id="reportFile">
                                        <label class="custom-file-label" for="reportFile">Report</label>
                                    </div>
                                    <div class="scheduleVisit">
                                        <div class="selectExpart">
                                            <div class="form-group">
                                                <input type="hidden" name="reportId" value="" id="expertReportId">
                                                <input type="hidden" name="documentId" value="" id="documentId">
                                                <textarea  name="feedback" value="" class="form-control" id="adminfeedback"></textarea>

                                            </div><!-- //form-group -->
                                        </div><!-- //selectExpart-->
                                        <div class="scheduleVisit-btn">
                                            <input type="button" class="btn btn-primary" id="reportFeedbackSubmitButton" value="submit">
                                        </div><!-- //scheduleVisit-btn-->
                                    </div><!--//scheduleVisit-->
                                </form>
                            </div><!-- //.col-12 -->
                        </div><!--/row-->
                    </div><!-- //popup-body --> 
                </div><!-- //Select the Expert-popup -->

                <!-- thankyou-popup-popup -->
                <div id="reportDeliveredToUser" class="mfp-with-anim mfp-hide thankyou">
                    <div class="popup-body">
                        <button title="Close (Esc)" type="button" class="mfp-close"></button>
                        <div class="popupHeading mb-3">
                            <h2>Select Rating</h2>
                        </div>

                        <form id="deliveredReportForm"> 
                            <div class="form-group mb-2"> 
                                <input type="hidden" id="deliveredReportId" value="" name="id">
                                <select class="form-control" id="adminfeedback" name="ratings">
                                    <option>Select Ratings</option>

                                    @for($i =1; $i <= 10; $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <span class="lineNormal">Are you sure want to deivered this report?</span>
                            <!-- <a href="javascript:void(0);" class="btn btn-primary">Ok</a> -->
                            <input type="button" class="btn btn-primary" value="cancel">
                            <input type="button" class="btn btn-primary deliveredReportButton" value="Ok"> 
                        </form> 
                    </div><!-- //popup-body --> 
                </div><!-- //thankyou-popup-popup -->

                {{-- modal --}}
                <div class="modal fade w-600" id="addLandListing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered addLandListing" role="document">
                        <div class="modal-content">
                            <div class="modal-body p-0">
                                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                                <div class="popupForm propertyEditForm">
                                    <form id="reportStoreForm" enctype="multipart/form-data">
                                        <div class="popupHeading">
                                            <h2>Add Land Report</h2>
                                        </div>

                                        <div class="addLand">
                                            <div>
                                                <div class="form-group">
                                                    <label for="user">{{ __('Users') }} <span class="text-danger">*</span></label>
                                                    <select class="form-control" id="UserId" name="all_user_id">
                                                        <option value="">Select Users</option>
                                                        @foreach ($user as $users)
                                                        <option value="{{ $users->id }}" mobile="{{ $users->mobile }}"
                                                                name="{{ $users->name }}" email="{{ $users->email }}">
                                                            {{ $users->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>

                                            <div>
                                                <div class="form-group">
                                                    <label for="name">{{ __('User Name') }} <span
                                                            class="text-danger">*</span></label>
                                                    <input class="form-control" id="name" type="text" placeholder="Enter User Name"
                                                           name="name" />
                                                </div>
                                            </div>
                                            <!--//div-->

                                            <div>
                                                <div class="form-group">
                                                    <label for="email">{{ __('Email') }} <span class="text-danger">*</span></label>
                                                    <input class="form-control" id="email" type="email" placeholder="Enter Email"
                                                           name="email" />
                                                </div>
                                            </div>
                                            <!--//div-->

                                            <div>
                                                <div class="form-group">
                                                    <label for="mobile">{{ __('mobile number') }} <span
                                                            class="text-danger">*</span></label>
                                                    <input class="form-control" id="mobile" type="text"
                                                           placeholder="Enter Mobile Number" name="mobile" />
                                                </div>
                                            </div>
                                            <!--//div-->

                                            <div>
                                                <div class="form-group">
                                                    <label for="land_name">{{ __('Land Name') }} <span
                                                            class="text-danger">*</span></label>
                                                    <input class="form-control" id="land_name" type="text" placeholder="Enter Land Name"
                                                           name="land_name" />
                                                </div>
                                            </div>
                                            <!--//div-->
                                            <div>
                                                <div class="form-group">
                                                    <label for="address">{{ __('Full Address') }} <span
                                                            class="text-danger">*</span></label>
                                                    <input class="form-control" id="address" type="text"
                                                           placeholder="Enter full address" name="address" />
                                                </div>
                                            </div>
                                            <!--//div-->
                                            <div>
                                                <div class="form-group">
                                                    <label for="state_id">{{ __('State') }} <span
                                                            class="text-danger">*</span></label>
                                                    <select class="form-control select-label"
                                                            onclick="this.setAttribute('value', this.value);" id="state_id" name="state_id">
                                                        <option value="">--</option>
                                                        @foreach ($states as $state)
                                                        <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <!--//div-->
                                            <div>
                                                <div class="from-group">
                                                    <label for="valueAddedServices">{{ __('Value Added Services') }} <span
                                                            class="text-danger">*</span></label>

                                                    <select class="form-control" id="valueAddedServices" value=""
                                                            name="value_added_service_id">
                                                        <option value="">Value Added Services</option>
                                                        <option value="2">Feasibility Analysis
                                                        </option>
                                                        <option value="1">Land Title Search
                                                        </option>

                                                    </select>
                                                </div>
                                            </div>

                                            <div>
                                                <div class="form-group landSize">
                                                    <label for="city">{{ __('Village/Town/District') }} <span
                                                            class="text-danger">*</span></label>
                                                    <input class="form-control" id="city" type="text"
                                                           placeholder="Enter Village/Town/District" name="city" />

                                                </div>
                                            </div>
                                            <!--//div-->
                                            <div>
                                                <div class="form-group">
                                                    <div class="custom-file">
                                                        <label class="custom-file-label" for="customFile">{{ __('Sazra') }}</label>
                                                        <input type="file" class="custom-file-input" id="customFile" name="image">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--//div-->
                                            <div class="flex-100">
                                                <div class="form-group">
                                                    <label>{{ __('Do you want Land analysis Report ?') }} <span
                                                            class="text-danger">*</span></label>
                                                    <div class="d-flex flex-row">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="report"
                                                                   name="analysis_report" value="1">
                                                            <label class="custom-control-label"
                                                                   for="report">{{ __('Yes') }}</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="reportNo"
                                                                   name="analysis_report" value="1">
                                                            <label class="custom-control-label"
                                                                   for="reportNo">{{ __('No') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--//div-->
                                            <div class="flex-100">
                                                <div class="form-group">
                                                    <label>{{ __('Are you interesting in Selling Land ?') }} <span
                                                            class="text-danger">*</span></label>
                                                    <div class="d-flex flex-row">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="land"
                                                                   name="selling_more" value="1">
                                                            <label class="custom-control-label" for="land">{{ __('Yes') }}</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="landNo"
                                                                   name="selling_more" value="0">
                                                            <label class="custom-control-label"
                                                                   for="landNo">{{ __('No') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--//div-->

                                            <div>
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="privacyPolicy"
                                                               name="terms" value="1">
                                                        <label class="custom-control-label"
                                                               for="privacyPolicy">{{ __('I agree to the') }}<span>
                                                                {{ __('Privacy Policy, Terms of services') }}</span>
                                                            {{ __('and') }} <span> {{ __('Cookie Policy') }}</span>
                                                            {{ __('of this Website') }}</label>

                                                    </div>

                                                </div>
                                            </div>
                                            <!--//div-->

                                        </div>
                                        <!--//div-->
                                </div>
                                <!--//addLand-->
                                <div class="formBtn">
                                    <button class="btn btn-primary landReportSubmitButton" type="button"
                                            title="Next">{{ __('Submit') }}</button>
                                </div>
                                </form>
                            </div>
                        </div>
                        <!--//modal-body-->
                    </div>
                </div>
                {{-- /modal --}}



                @section('custom-script')
                <script>
                    formReset = function () {
                        window.location.href = "<?php url('admin/reports'); ?>";
                    }

                    $(document).ready(function () {

                        $('.admin-notify-report').on('click', function (e) {
                            e.preventDefault();
                            var id = $(this).data('id');
                            var documentId = $(this).data('document');
                            $('#expertReportId').val(id);
                            $('#documentId').val(documentId);
                            $.magnificPopup.open({
                                items: {
                                    src: '#notify-expert-about-report',
                                    callbacks: {
                                        beforeOpen: function () {
                                            this.st.mainClass = this.st.el.attr('data-effect');
                                        }
                                    },
                                    type: 'inline'
                                }

                            });

                        });
                        $('.selectExpartAssign').on('click', function (e) {
                            e.preventDefault();
                            var id = $(this).data('expert');
                            $('#reportId').val(id);
                            $.magnificPopup.open({
                                items: {
                                    src: '#select-expert-popup',
                                    callbacks: {
                                        beforeOpen: function () {
                                            this.st.mainClass = this.st.el.attr('data-effect');
                                        }
                                    },
                                    type: 'inline'
                                }

                            });

                        });

                        $('#reportAssingSubmitButton').on('click', function () {
                            $('.loader').toggleClass('d-none');
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            $.ajax({
                                type: 'post',
                                url: "report-assing-expert",
                                data: $(".reportAssignForm").serialize(),
                                success: function (data) {
                                    ;
                                    $('.loader').toggleClass('d-none');
                                    if (data) {
                                        $('.reportAssignForm')[0].reset();
                                        swal({
                                            title: "Good job!",
                                            text: "Report assinged to successfully!",
                                            icon: "success",
                                        }).then((value) => {
                                            location.reload();
                                        });
                                    } else {
                                        swal({
                                            text: "Something went wrong!",
                                            icon: "error",
                                        });
                                    }
                                },
                                error: function (errorResponse) {
                                    $('.loader').toggleClass('d-none');
                                    $('#service-feedback').prop("disabled", false);
                                    $('#service-feedback').text('Submit');
                                    $.each(errorResponse.responseJSON.errors, function (field_name, error) {
                                        $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');

                                    })
                                }
                            });
                        });

                        //report feed back submit
                        $('#reportFeedbackSubmitButton').on('click', function () {
                            $('.loader').toggleClass('d-none');

                            var reportFile = $('#reportFile').val();
                            var adminfeedback = $('#adminfeedback').val();
                            if (reportFile == '' && adminfeedback == '') {
                                $('.loader').toggleClass('d-none');
                                $('.feedbackFormError').text('Atleast one input field select');
                                return false;
                            }
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            var formData = new FormData(document.getElementById("reportFeedbackForm"));
                            $.ajax({
                                type: 'post',
                                url: "report-feedback",
                                data: formData,
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function (data) {
                                    $('.loader').toggleClass('d-none');
                                    if (data) {
                                        $('.reportFeedbackForm')[0].reset();
                                        swal({
                                            title: "Good job!",
                                            text: "Report feedback successfully send!",
                                            icon: "success",
                                        }).then((value) => {
                                            location.reload();
                                        });
                                    } else {
                                        swal({
                                            text: "Something went wrong!",
                                            icon: "error",
                                        });
                                    }
                                },
                                error: function (errorResponse) {
                                    $('.loader').toggleClass('d-none');
                                    $('.error-span').remove();
                                    $.each(errorResponse.responseJSON.errors, function (field_name, error) {
                                        $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');

                                    })
                                }
                            });
                        });
                        $('.customCheckReport').on('click', function () {
//                            $('.loader').toggleClass('d-none');
                            var id = $(this).data('id');
                            $('#deliveredReportId').val(id);
                            $.magnificPopup.open({
                                items: {
                                    src: '#reportDeliveredToUser',
                                    callbacks: {
                                        beforeOpen: function () {
                                            this.st.mainClass = this.st.el.attr('data-effect');
                                        }
                                    },
                                    type: 'inline'
                                }

                            });

                        });
                    });


                    $('.deliveredReportButton').on('click', function () {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: 'post',
                            url: "report-delivered",
                            data: $("#deliveredReportForm").serialize(),
                            success: function (data) {
                                $('.loader').toggleClass('d-none');
                                if (data) {
                                    $('.reportAssignForm')[0].reset();
                                    swal({
                                        title: "Good job!",
                                        text: "Report delivered successfully!",
                                        icon: "success",
                                    }).then((value) => {
                                        location.reload();
                                    });
                                } else {
                                    swal({
                                        text: "Something went wrong!",
                                        icon: "error",
                                    });
                                }
                            },
                        });
                    });
                </script>
                <script>
                    $(document).ready(function () {
                        // datepicker
                        var today, datepicker;
                        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                        datepicker = $('#filterDatepicker').datepicker({
                            // maxDate: today,
                            format: 'dd-mm-yyyy'
                        });
                    })
                </script>
                <script>
                    $(document).ready(function () {

                        $('#UserId').on('change', function () {
                            let mobile = $('option:selected', this).attr('mobile');
                            let email = $('option:selected', this).attr('email');
                            let name = $('option:selected', this).attr('name');
                            $("#email").val(email);
                            $("#mobile").val(mobile);
                            $("#name").val(name);
                            console.log(mobile, email)
                        });



                        $('body').on('click', '.landReportSubmitButton', function () {
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            var form = $('#reportStoreForm')[0];
                            var formData = new FormData(form);
                            $('.loader').toggleClass('d-none');

                            $.ajax({
                                type: "post",
                                enctype: 'multipart/form-data',
                                url: "report-validation",
                                processData: false,
                                contentType: false,
                                data: formData,
                                success: function (data) {
                                    $('#reportStoreForm')[0].reset();
                                    $('.loader').toggleClass('d-none');

                                    if (data.success == true) {
                                        swal(data.msg)
                                                .then((value) => {
                                                    location.reload();
                                                });
                                    } else {
                                        swal(data.msg);
                                    }

                                },
                                error: function (errorResponse) {
                                    $('.loader').toggleClass('d-none');
                                    $('.error-span').remove();
                                    $.each(errorResponse.responseJSON.errors, function (field_name, error) {
                                        if (field_name == 'analysis_report') {
                                            $(document).find('[name=' + field_name + ']').parent()
                                                    .parent().after(
                                                    '<span class="text-strong error-span text-danger" role="alert">' +
                                                    error + '</span>');
                                        } else if (field_name == 'selling_more') {
                                            $(document).find('[name=' + field_name + ']').parent()
                                                    .parent().after(
                                                    '<span class="text-strong error-span text-danger" role="alert">' +
                                                    error + '</span>');
                                        } else {
                                            $(document).find('[name=' + field_name + ']').parent()
                                                    .after(
                                                            '<span class="text-strong error-span text-danger" role="alert">' +
                                                            error + '</span>');
                                        }
                                    })
                                }
                            });
                        });



                    });

                </script>
                @endsection