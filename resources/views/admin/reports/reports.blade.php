@extends('admin.layouts.master')
@section('title')
Your Land Reports
@endsection
@section('class')
reportsBody
@endsection
@section('content') 

<h2>Your Land Reports</h2>
@include('admin.reports.__reports-common-header')

<!-- //Tab panes Start -->
<div class="tab-content">
    <div class="tab-pane active" id="allReport">
        @forelse($allReports as $report)
        <div class="dataTable border mb-3">
            @if(isset($report->reportDocument->id) && $report->status == 2 && $report->reportDocument->report_by == 0)
                <!--//threeDotMenu Start-->
                <div class="dropdown action-btn">
                    <button class="btn" type="button" data-toggle="dropdown"></button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="javascript:;" data-id="{{ $report->id }}" data-document="{{isset($report->reportDocument->id) ? $report->reportDocument->id : ''}}" class="admin-notify-report">Notify about Report</a></li>
                    </ul>
                </div>
                <!--//threeDotMenu End-->
            @endif

            <div class="row">
                <div class="data col-lg-4 col-md-6 mb-1">
                    <h4>
                        <a href="#deliveredReport-popup" data-effect="mfp-zoom-in">{{ $report->size_of_land }} Acres {{ $report->land_name }}</a>
                    </h4>
                    <span>{{ isset($report->user->mobile) ? $report->user->mobile : '' }}</span> | <span>{{ isset($report->user->email) ? $report->user->email : '' }}</span>
                </div><!--//data -->

                <div class="data col-lg-3 col-md-6 mb-1">
                    <label>
                        Report :  <span>
                            @if(!empty($report->valueAdded))
                            {{ isset($report->valueAdded->service_name) ? $report->valueAdded->service_name : '' }}
                            @endif
                        </span>
                    </label>
                    <label>
                        Status : 
                        @if($report->status == 0)
                        <span class="pending"> Pending</span>
                        @elseif($report->status == 2)
                        <span class="inprogress"> Inprogress</span>
                        @elseif($report->status == 1)
                        <span class="delivered"> Delivered</span>
                        @endif
                    </label>
                </div><!--//data -->
                @if($report->status == 0)
                <div class="data col-lg-3 col-md-6 mb-1">
                    @if(empty($report->expert_id) || $report->cancel == 1)
                    <label>
                        Assigned to :  <a href="javascript:;" data-expert="{{ $report->id }}" data-service="{{ $report->value_added_service }}" class="selectExpartAssign">Select the Expert</a>
                    </label>
                    @else
                    <label>
                        Pending expert confirmation
                    </label>
                    @endif
                    <label></label>
                </div><!--//data -->

                @elseif($report->status == 2)
                <div class="data col-lg-3 col-md-6 mb-1">
                    <label>
                        Assigned to :  <span>{{ isset($report->expert->name) ? $report->expert->name : '' }}</span>
                    </label>
                    @if($report->status == 2)
                    <label>
                        <div class="custom-control custom-checkbox reportFine">
                            <input type="button" id="customCheck-{{ $report->id }}" class="custom-control-input customCheckReport" name="example1" data-id="{{ $report->id }}">
                            <label class="custom-control-label" for="customCheck-{{ $report->id }}">Click the icon, If report is fine</label>
                        </div>
                    </label>
                    @else
                    <label>
                        <span class="statusChanges">Your Status is Changed</span>
                    </label>
                    @endif
                </div><!--//data -->
                @else
                <div class="data col-lg-3 col-md-6 mb-1">
                    <label>
                        Assigned to :  <span>{{ isset($report->expert->name) ? $report->expert->name : '' }}</span>
                    </label>
                    <label>
                        <span class="statusChanges">Your Status is Changed</span>
                    </label>
                </div><!--//data -->
                @endif
                <div class="data col-lg-2 col-md-6 mb-1 text-right">
                    @php

                    $pdf = '';
                    if(!empty($report->reportDocument)){
                    if(Storage::disk('landReport')->exists(str_replace('landReport', '', $report->reportDocument->title)))
                    {
                    $pdf = Storage::disk('landReport')->url(str_replace('landReport/', '', $report->reportDocument->title));
                    }
                    }
                    @endphp
                    @if(!empty($pdf))
                    <a href="{{ $pdf }}" class="btn btn-primary  @if($report->status == 0 && empty($report->expert_id)) {{ __('d-none') }} @endif" target="_blank">Download PDF</a>
                    @endif
                </div><!--//data --> 
            </div><!-- /.row -->
            @if($report->status == 2 && isset($report->reportDocument->id) && $report->reportDocument->report_by == 0)
                <div class="text-danger font-11">
                    Expert report submited, please review
                </div>
            @endif
        </div><!-- //dataTable -->
        @empty
        <div class="no-content-msg"> 
                No Reports Available 
        </div><!--//no-content-msg -->
        @endforelse
    </div><!-- //Tab panes End -->
</div>

{{ $allReports->links() }}              
@endsection

@section('modal')
<!-- Select the Expert-popup -->
<div id="select-expert-popup" class="mfp-with-anim mfp-hide select-expert">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12"><h2>Assignee Expert</h2></div><!-- //.col-12 -->
            <div class="col-12">
                <form>
                    <div class="scheduleVisit">
                        <div class="selectExpart">
                            <div class="form-group">
                                <select class="form-control" id="reportType">
                                    <option>Select Expert</option>
                                    <option>Abhit Bhatia</option>
                                    <option>Manish Bhandari</option>
                                    <option>Vipin Vindal</option>
                                </select>
                            </div><!-- //form-group -->
                        </div><!-- //selectExpart-->
                        <div class="scheduleVisit-btn">
                            <input type="submit" class="btn btn-primary" value="Assignee">
                        </div><!-- //scheduleVisit-btn-->
                    </div><!--//scheduleVisit-->
                </form>
            </div><!-- //.col-12 -->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Select the Expert-popup -->

<!-- thankyou-popup-popup -->
<div id="thankyou-popup" class="mfp-with-anim mfp-hide thankyou">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12">
                <h1><img src="{{ asset('adminassets/images/svg-icons/success-icon.svg') }}" alt=""></h1>
                <h2>Thankyou for Assigning Expert</h2>
                <span>Expert will get notification to get report done</span>
                <!-- <a href="javascript:void(0);" class="btn btn-primary">Ok</a> -->
                <input type="submit" class="btn btn-primary" value="Ok">
            </div><!-- //.col-12 -->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //thankyou-popup-popup -->

<!-- Delivered Report-popup -->
<div id="deliveredReport-popup" class="mfp-with-anim mfp-hide deliveredReport">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12">
                <div class="delivered-report-title">
                    <h2>Delivered Report</h2>
                    <div id="counter"></div>
                </div><!--//delivered-report-title -->                    
            </div>
            <div class="col-12">

                <div class="deliveredReport-carousel owl-carousel">
                    <div class="deliveredReport-items">
                        <div class="reportBox">
                            <!--//threeDotMenu Start-->
                            <div class="dropdown">
                                <button class="btn" type="button" data-toggle="dropdown"></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                </ul>
                            </div><!-- //dropdown-->
                            <!--//threeDotMenu End-->
                            <div class="media">
                                <img src="{{ asset('adminassets/images/project-map.png') }}" alt="Generic placeholder image">
                                <div class="media-body">
                                    <h5 class="mt-0">21 Acres Land Town</h5>
                                    <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                    <label class="landTitle">Land Title Search</label>
                                    <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                    <div class="expartUpdate">
                                        <h6>
                                            Delivered by : <span>Expert Musolani</span>
                                            <span class="daysAgo">2 days ago</span>
                                        </h6>
                                        <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                    </div><!-- //expartUpdate-->
                                </div><!-- //media-body -->
                            </div><!--//media -->
                            <blockquote class="blockquote blockquote-reverse adminFeedback">
                                <a href="javascript:void(0);" class="edit-block"><img src="{{ asset('adminassets/images/svg-icons/gray-edit-icon.svg') }}" alt=""></a>
                                <header class="blockquote-header">Feeback by Admin</header>
                                <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </blockquote><!--//blockquote-->

                            <blockquote class="blockquote blockquote-reverse blockquote-editComment">
                                <form>
                                    <textarea name="" id="" rows="3" placeholder="Write Feedback"></textarea>
                                    <input type="submit" value="" class="btn-primary"> 
                                </form>
                            </blockquote><!--//blockquote blockquote-editComment-->
                        </div><!-- //reportBox -->
                    </div><!--//deliveredReport-items -->

                    <div class="deliveredReport-items">
                        <div class="reportBox">
                            <!--//threeDotMenu Start-->
                            <div class="dropdown">
                                <button class="btn" type="button" data-toggle="dropdown"></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                </ul>
                            </div><!-- //dropdown-->
                            <!--//threeDotMenu End-->
                            <div class="media">
                                <img src="{{ asset('adminassets/images/project-map.png') }}" alt="Generic placeholder image">
                                <div class="media-body">
                                    <h5 class="mt-0">21 Acres Land Town</h5>
                                    <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                    <label class="landTitle">Land Title Search</label>
                                    <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                    <div class="expartUpdate">
                                        <h6>
                                            Delivered by : <span>Expert Musolani</span>
                                            <span class="daysAgo">2 days ago</span>
                                        </h6>
                                        <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                    </div><!-- //expartUpdate-->
                                </div><!-- //media-body -->
                            </div><!--//media -->
                            <blockquote class="blockquote blockquote-reverse adminFeedback">
                                <a href="javascript:void(0);" class="edit-block"><img src="{{ asset('adminassets/images/svg-icons/gray-edit-icon.svg') }}" alt=""></a>
                                <header class="blockquote-header">Feeback by Admin</header>
                                <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </blockquote><!--//blockquote-->

                            <blockquote class="blockquote blockquote-reverse blockquote-editComment">
                                <form>
                                    <textarea name="" id="" rows="3" placeholder="Write Feedback"></textarea>
                                    <input type="submit" value="" class="btn-primary"> 
                                </form>
                            </blockquote><!--//blockquote blockquote-editComment-->
                        </div><!-- //reportBox -->
                    </div><!--//deliveredReport-items -->

                    <div class="deliveredReport-items">
                        <div class="reportBox">
                            <!--//threeDotMenu Start-->
                            <div class="dropdown">
                                <button class="btn" type="button" data-toggle="dropdown"></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                </ul>
                            </div><!-- //dropdown-->
                            <!--//threeDotMenu End-->
                            <div class="media">
                                <img src="{{ asset('adminassets/images/project-map.png') }}" alt="Generic placeholder image">
                                <div class="media-body">
                                    <h5 class="mt-0">21 Acres Land Town</h5>
                                    <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                    <label class="landTitle">Land Title Search</label>
                                    <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                    <div class="expartUpdate">
                                        <h6>
                                            Delivered by : <span>Expert Musolani</span>
                                            <span class="daysAgo">2 days ago</span>
                                        </h6>
                                        <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                    </div><!-- //expartUpdate-->
                                </div><!-- //media-body -->
                            </div><!--//media -->
                            <blockquote class="blockquote blockquote-reverse adminFeedback">
                                <a href="javascript:void(0);" class="edit-block"><img src="{{ asset('adminassets/images/svg-icons/gray-edit-icon.svg') }}" alt=""></a>
                                <header class="blockquote-header">Feeback by Admin</header>
                                <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </blockquote><!--//blockquote-->

                            <blockquote class="blockquote blockquote-reverse blockquote-editComment">
                                <form>
                                    <textarea name="" id="" rows="3" placeholder="Write Feedback"></textarea>
                                    <input type="submit" value="" class="btn-primary"> 
                                </form>
                            </blockquote><!--//blockquote blockquote-editComment-->
                        </div><!-- //reportBox -->
                    </div><!--//deliveredReport-items -->
                </div><!--//carousel--> 

            </div><!-- //.col-12-->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Delivered Report-popup -->

<!-- Inprogress Report-popup -->
<div id="inprogressReport-popup" class="mfp-with-anim mfp-hide deliveredReport">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12">
                <div class="delivered-report-title inprogress-report-title">
                    <h2>Inprogress Report</h2>
                    <div id="counter"></div>
                </div><!--//delivered-report-title -->                    
            </div>
            <div class="col-12 deliveredReport-carousel owl-carousel">
                <div class="deliveredReport-items">
                    <div class="reportBox">
                        <!--//threeDotMenu Start-->
                        <div class="dropdown">
                            <button class="btn" type="button" data-toggle="dropdown"></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="JavaScript:void(0);">Link</a></li>
                                <li><a href="JavaScript:void(0);">Link</a></li>
                            </ul>
                        </div><!-- //dropdown-->
                        <!--//threeDotMenu End-->
                        <div class="media">
                            <img src="{{ asset('adminassets/images/project-map.png') }}" alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mt-0">21 Acres Land Town</h5>
                                <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                <label class="landTitle">Land Title Search</label>
                                <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                <div class="expartUpdate">
                                    <h6>
                                        Delivered by : <span>Expert Musolani</span>
                                        <span class="daysAgo">2 days ago</span>
                                    </h6>
                                    <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                </div><!-- //expartUpdate-->
                            </div><!-- //media-body -->
                        </div><!--//media -->
                        <blockquote class="blockquote blockquote-reverse blockquote-editComment">
                            <form>
                                <textarea name="" id="" rows="3" placeholder="Write Feedback"></textarea>
                                <input type="submit" value="" class="btn-primary"> 
                            </form>
                        </blockquote><!--//blockquote blockquote-editComment-->
                    </div><!-- //reportBox -->
                </div><!--//deliveredReport-items -->

                <div class="deliveredReport-items">
                    <div class="reportBox">
                        <!--//threeDotMenu Start-->
                        <div class="dropdown">
                            <button class="btn" type="button" data-toggle="dropdown"></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="JavaScript:void(0);">Link</a></li>
                                <li><a href="JavaScript:void(0);">Link</a></li>
                            </ul>
                        </div><!-- //dropdown-->
                        <!--//threeDotMenu End-->
                        <div class="media">
                            <img src="{{ asset('adminassets/images/project-map.png') }}" alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mt-0">21 Acres Land Town</h5>
                                <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                <label class="landTitle">Land Title Search</label>
                                <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                <div class="expartUpdate">
                                    <h6>
                                        Delivered by : <span>Expert Musolani</span>
                                        <span class="daysAgo">2 days ago</span>
                                    </h6>
                                    <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                </div><!-- //expartUpdate-->
                            </div><!-- //media-body -->
                        </div><!--//media -->
                        <blockquote class="blockquote blockquote-reverse blockquote-editComment">
                            <form>
                                <textarea name="" id="" rows="3" placeholder="Write Feedback"></textarea>
                                <input type="submit" value="" class="btn-primary"> 
                            </form>
                        </blockquote><!--//blockquote blockquote-editComment-->
                    </div><!-- //reportBox -->
                </div><!--//deliveredReport-items -->
            </div><!-- //.col-12 deliveredReport-carousel-->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Inprogress Report-popup -->

<!-- Request Report-popup -->
<div id="requestReport-popup" class="mfp-with-anim mfp-hide deliveredReport">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12">
                <div class="delivered-report-title request-report-title">
                    <h2>Request Report</h2>
                    <div id="counter"></div>
                </div><!--//delivered-report-title -->                    
            </div>
            <div class="col-12 deliveredReport-carousel owl-carousel">
                <div class="deliveredReport-items">
                    <div class="reportBox">
                        <!--//threeDotMenu Start-->
                        <div class="dropdown">
                            <button class="btn" type="button" data-toggle="dropdown"></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="JavaScript:void(0);">Link</a></li>
                                <li><a href="JavaScript:void(0);">Link</a></li>
                            </ul>
                        </div><!-- //dropdown-->
                        <!--//threeDotMenu End-->
                        <div class="media">
                            <img src="{{ asset('adminassets/images/project-map.png') }}" alt="Generic placeholder image">
                            <div class="media-body">
                                <h5 class="mt-0">21 Acres Land Town</h5>
                                <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                <label class="landTitle">Land Title Search</label>
                                <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                <div class="expartUpdate">
                                    <h6>
                                        Delivered by : <a href="#select-expert-popup" data-effect="mfp-zoom-in" class="selectExpart">Select the Expert</a>
                                        <span class="daysAgo">2 days ago</span>
                                    </h6>                                                           
                                </div><!-- //expartUpdate-->
                            </div><!-- //media-body -->
                        </div><!--//media -->
                        <blockquote class="blockquote blockquote-reverse">
                            <p class="mb-0">Expert has not uploaded the Report yet</p>
                            <p class="mb-0">When he was uploaded the Report then you can able to comment </p>
                        </blockquote><!--//blockquote blockquote-editComment-->
                    </div><!-- //reportBox -->
                </div><!--//deliveredReport-items -->
            </div><!-- //.col-12 deliveredReport-carousel-->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Request Report-popup -->

<!-- View Report-popup -->
<div id="view-report-popup" class="mfp-with-anim mfp-hide deliveredReport viewReportPopup ">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12 deliveredReport-carousel owl-carousel">
                <div class="map-images">
                    <img src="{{ asset('adminassets/images/view-report-img.png') }}" alt="">
                </div><!--//map-images-->

                <div class="map-images">
                    <img src="{{ asset('adminassets/images/view-report-img.png') }}" alt="">
                </div><!--//map-images-->
            </div><!-- //.col-12 deliveredReport-carousel-->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //View Report-popup -->
@php
 $experts = expertListShow();
 @endphp
 <script>
 $(document).ready(function () {
                    $('.selectExpartAssign').on('click', function(){
                    var service = $(this).data('service');
                        $.ajax({
                                type: "post",
                                url: "assign-expert-popup",
                                data: {service: service},
                                success: function (data) {
                                    $('.assignToExpert').html('');
                                          $('.assignToExpert').append('<option value="">Select Expert</option>');
                                      $.each(data, function(i, expert){
                                          $('.assignToExpert').append('<option value="'+expert.id+'">'+expert.name+'</option>');
                                      })
                                }
                            });
  
                        });

                    $('#searahallExpert').on('click', function(){
                             $.ajax({
                                type: "post",
                                url: "assign-expert-popup",
                                success: function (data) {
                                    $('.assignToExpert').html('');
                                          $('.assignToExpert').append('<option value="">Select Expert</option>');
                                      $.each(data, function(i, expert){
                                          $('.assignToExpert').append('<option value="'+expert.id+'">'+expert.name+'</option>');
                                      })
                                }
                            });
                    });
});
</script>
@endsection
@section('custom-script')

@endsection