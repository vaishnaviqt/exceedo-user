@extends('admin.layouts.master')
@section('title')
 Your Land Listing
@endsection
@section('class')
reportsBody land-listing
@endsection
@section('content')
 
 <h2>Your Land Listing</h2> 

                <div class="countCards">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <article class="countCardArt">
                                <div class="coutRepIcon">
                                    <img src="{{ asset('adminassets/images/svg-icons/listing-icon.svg') }}" alt="all reports" />
                                </div>
                                <div class="countRepDetail">
                                    <p><strong>47</strong></p>
                                    <p>All Land Listing</p>
                                </div>
                            </article><!--//countCardArt-->
                        </div><!--//col-lg-3-->
                        
                        <div class="col-lg-3 col-md-6">
                            <article class="countCardArt">
                                <div class="coutRepIcon">
                                    <img src="{{ asset('adminassets/images/svg-icons/active-land-listing-icon.svg') }}" alt="Delivered Reports" />                                    
                                </div>
                                <div class="countRepDetail">
                                    <p><strong>32</strong></p>
                                    <p>Active Land Listing</p>
                                </div>
                            </article><!--//countCardArt-->
                        </div><!--//col-lg-3-->
                        
                        <div class="col-lg-3 col-md-6">
                            <article class="countCardArt">
                                <div class="coutRepIcon">
                                    <img src="{{ asset('adminassets/images/svg-icons/inactive-land-listing-icon.svg') }}" alt="Inprogress Reports" />                                    
                                </div>
                                <div class="countRepDetail">
                                    <p><strong>12</strong></p>
                                    <p>Inactive Land Listing</p>
                                </div>
                            </article><!--//countCardArt-->
                        </div><!--//col-lg-3-->
                        
                        <div class="col-lg-3 col-md-6">
                            <article class="countCardArt">
                                <div class="coutRepIcon">
                                    <img src="{{ asset('adminassets/images/svg-icons/request-land-listing-icon.svg') }}" alt="Pending Reports" />                                    
                                </div>
                                <div class="countRepDetail">
                                    <p><strong>12</strong></p>
                                    <p>Request Land Lisiting</p>
                                </div>
                            </article><!--//countCardArt-->
                        </div><!--//col-lg-3-->                        
                    </div><!--//row-->
                </div><!--//countCards-->

                <!-- //Nav tabs Start -->
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#allLandlisting">All Land Listing <span>(47)</span></a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#activeLandlisting">Active Land Listing <span>(12)</span></a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#inactiveLandlisting">Inactive land listing <span>(12)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#requestLandlisting">Request land listing <span>(4)</span></a>
                    </li>
                </ul>
                <!-- //Nav tabs End -->

                <!-- //Search Form Div Start -->
                <div class="searchForm">
                    <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                  
                    <form>
                        <span>Search</span>
                        <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
                        <ul class="d-flex flex-wrap">
                            <li>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Land Name" id="land-name">
                                </div><!-- //form-group -->
                            </li>
                            <li>
                                <div class="form-group">
                                    <select class="form-control" id="reportType">
                                        <option>Land Type</option>
                                        <option>Land Type 01</option>
                                        <option>Land Type 02</option>
                                        <option>Land Type 03</option>
                                    </select>
                                </div><!-- //form-group --> 
                            </li>
                            <li>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Land Area " id="land-area">
                                </div><!-- //form-group -->
                            </li>
                            <li>
                                <div class="form-group">
                                    <input class="form-control" id="datepicker" type="text" placeholder="Date"/>
                                </div><!-- //form-group -->
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="btn btn-primary">Search</a>
                            </li>
                        </ul><!-- //ul -->
                    </form><!-- //form -->
                </div><!-- //search-form -->
                <!-- //Search Form Div End -->
                
                <!-- //Tab panes Start -->
                <div class="tab-content valueAddedServices" id="inline-popups">
                    <div class="tab-pane active" id="allLandlisting">
                        <div class="card-deck">
                            <div class="row row_20">
                                <div class="col-lg-6 col-12">
                                    <div class="card activeLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land <img src="{{ asset('adminassets/images/svg-icons/land-listing-active-icon.svg') }}" alt=""></h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Active</span><!--//badge-success-->

                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="listingExpiring">Your Listing expiring date is 22 Oct </span>
                                            </div><!-- //div-->
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card activeLand -->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card inactiveLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Inactive</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->                                                    
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card inactiveLand-->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card for-sale-land">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->                                                  
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div class="sale-land-details">
                                                <div>Mobile Number : <span>+91-9560456464</span></div>
                                                <div>Who you are : <span>Owner</span></div>
                                                <div>Status : <span class="inprogress">Inprogress</span></div>
                                                <div><img src="{{ asset('adminassets/images/svg-icons/for-sale-icon.svg') }}" alt=""><span>For Sale</span></div>
                                            </div><!-- //div-->
                                            <div class="flex-fill w_100">
                                                <a href="javascript:void(0);" class="btn btn-primary">Download Sazra Map</a>
                                            </div><!-- //flex-fill -->
                                            <div class="flex-fill">
                                                <a href="#schedule-Visit-popup" data-effect="mfp-zoom-in" class="schedule-visit">Schedule a Visit</a>
                                            </div><!-- //flex-fill-->
                                            <div class="flex-fill">
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                </ul>
                                            </div><!-- //flex-fill-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card for-sale-land -->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card for-sale-land">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->                                                  
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div class="sale-land-details">
                                                <div>Mobile Number : <span>+91-9560456464</span></div>
                                                <div>Who you are : <span>Owner</span></div>
                                                <div>Status : <span class="pending">Pending</span></div>
                                                <div><img src="{{ asset('adminassets/images/svg-icons/for-sale-icon.svg') }}" alt=""><span>For Sale</span></div>
                                            </div><!-- //div-->
                                            <div class="flex-fill w_100">
                                                <a href="javascript:void(0);" class="btn btn-primary">Download Sazra Map</a>
                                            </div><!-- //flex-fill -->
                                            <div class="flex-fill">
                                                <a href="#schedule-Visit-popup" data-effect="mfp-zoom-in" class="schedule-visit">Schedule a Visit</a>
                                            </div><!-- //flex-fill-->
                                            <div class="flex-fill">
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                </ul>
                                            </div><!-- //flex-fill-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card for-sale-land -->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card inactiveLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Inactive</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->                                                    
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card inactiveLand-->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card for-sale-land">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->                                                  
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div class="sale-land-details">
                                                <div>Mobile Number : <span>+91-9560456464</span></div>
                                                <div>Who you are : <span>Owner</span></div>
                                                <div>Status : <span class="pending">Pending</span></div>
                                                <div><img src="{{ asset('adminassets/images/svg-icons/for-sale-icon.svg') }}" alt=""><span>For Sale</span></div>
                                            </div><!-- //div-->
                                            <div class="flex-fill w_100">
                                                <a href="javascript:void(0);" class="btn btn-primary">Download Sazra Map</a>
                                            </div><!-- //div-->
                                            <div class="flex-fill">
                                                <a href="#schedule-Visit-popup" data-effect="mfp-zoom-in" class="schedule-visit">Schedule a Visit</a>
                                            </div>
                                            <div class="flex-fill">
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card for-sale-land -->
                                </div><!-- //col-lg-6 col-12-->
                            </div><!-- //row row_20 -->
                        </div><!-- //card-deck -->
                    </div><!--//tab-pane id=allLandlisting -->

                    <div class="tab-pane" id="activeLandlisting">
                        <div class="card-deck">
                            <div class="row row_20">
                                <div class="col-lg-6 col-12">
                                    <div class="card activeLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land <img src="{{ asset('adminassets/images/svg-icons/land-listing-active-icon.svg') }}" alt=""></h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Active</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="listingExpiring">Your Listing expiring date is 22 Oct </span>
                                            </div><!-- //div-->
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card activeLand -->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card activeLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Active</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card activeLand -->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card activeLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land <img src="{{ asset('adminassets/images/svg-icons/land-listing-active-icon.svg') }}" alt=""></h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Active</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="listingExpiring">Your Listing expiring date is 22 Oct </span>
                                            </div><!-- //div-->
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card activeLand -->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card activeLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Active</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card activeLand -->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card activeLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Active</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card activeLand -->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card activeLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land <img src="{{ asset('adminassets/images/svg-icons/land-listing-active-icon.svg') }}" alt=""></h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Active</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="listingExpiring">Your Listing expiring date is 22 Oct </span>
                                            </div><!-- //div-->
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card activeLand -->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card activeLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Active</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card activeLand -->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card activeLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land <img src="{{ asset('adminassets/images/svg-icons/land-listing-active-icon.svg') }}" alt=""></h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Active</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="listingExpiring">Your Listing expiring date is 22 Oct </span>
                                            </div><!-- //div-->
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card activeLand -->
                                </div><!-- //col-lg-6 col-12-->
                            </div><!-- //row row_20 -->
                        </div><!-- //card-deck -->
                    </div><!--//tab-pane id=activeLandlisting -->

                    <div class="tab-pane" id="inactiveLandlisting">
                        <div class="card-deck">
                            <div class="row row_20">
                                <div class="col-lg-6 col-12">
                                    <div class="card inactiveLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land <img src="{{ asset('adminassets/images/svg-icons/land-listing-active-icon.svg') }}" alt=""></h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Inactive</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->                                                    
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card inactiveLand-->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card inactiveLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Inactive</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->                                                    
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card inactiveLand-->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card inactiveLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land <img src="{{ asset('adminassets/mages/svg-icons/land-listing-active-icon.svg') }}" alt=""></h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Inactive</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->                                                    
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card inactiveLand-->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card inactiveLand">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="badge badge-success">Inactive</span><!--//badge-success-->
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Your Views : 132</a></li>
                                                        <li><a href="#seeGraph-popup" data-effect="mfp-zoom-in">See Graph</a></li>
                                                        <li><a href="#">Send Renew Sms</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->                                                    
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div>
                                                <div class="featFacilities">
                                                    <span class="featFaTitle">Facilities :</span>
                                                    <span class="faci-icon parking-icon"></span>
                                                    <span class="faci-icon noSmooking-icon"></span>
                                                    <span class="faci-icon wifi-icon"></span>
                                                </div>
                                            </div><!-- //div-->
                                            <div>
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                    <li>169 views</li>
                                                </ul>
                                            </div><!-- //div-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card inactiveLand-->
                                </div><!-- //col-lg-6 col-12-->
                            </div><!-- //row row_20 -->
                        </div><!-- //card-deck -->
                    </div><!--//tab-pane id=inactiveLandlisting -->

                    <div class="tab-pane" id="requestLandlisting">
                        <div class="card-deck">
                            <div class="row row_20">
                                <div class="col-lg-6 col-12">
                                    <div class="card for-sale-land">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->                                                  
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div class="sale-land-details">
                                                <div>Mobile Number : <span>+91-9560456464</span></div>
                                                <div>Who you are : <span>Owner</span></div>
                                                <div>Status : <span class="inprogress">Inprogress</span></div>
                                                <div><img src="{{ asset('adminassets/images/svg-icons/land-listing-active-icon.svg') }}" alt=""><span>For Sale</span></div>
                                            </div><!-- //div-->
                                            <div class="flex-fill w_100">
                                                <a href="javascript:void(0);" class="btn btn-primary">Download Sazra Map</a>
                                            </div><!-- //flex-fill -->
                                            <div class="flex-fill">
                                                <a href="#schedule-Visit-popup" data-effect="mfp-zoom-in" class="schedule-visit">Schedule a Visit</a>
                                            </div><!-- //flex-fill-->
                                            <div class="flex-fill">
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                </ul>
                                            </div><!-- //flex-fill-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card for-sale-land -->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card for-sale-land">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->                                                  
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div class="sale-land-details">
                                                <div>Mobile Number : <span>+91-9560456464</span></div>
                                                <div>Who you are : <span>Owner</span></div>
                                                <div>Status : <span class="pending">Pending</span></div>
                                                <div><img src="{{ asset('adminassets/images/svg-icons/for-sale-icon.svg') }}" alt=""><span>For Sale</span></div>
                                            </div><!-- //div-->
                                            <div class="flex-fill w_100">
                                                <a href="javascript:void(0);" class="btn btn-primary">Download Sazra Map</a>
                                            </div><!-- //flex-fill -->
                                            <div class="flex-fill">
                                                <a href="#schedule-Visit-popup" data-effect="mfp-zoom-in" class="schedule-visit">Schedule a Visit</a>
                                            </div><!-- //flex-fill-->
                                            <div class="flex-fill">
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                </ul>
                                            </div><!-- //flex-fill-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card for-sale-land -->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card for-sale-land">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->                                                  
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div class="sale-land-details">
                                                <div>Mobile Number : <span>+91-9560456464</span></div>
                                                <div>Who you are : <span>Owner</span></div>
                                                <div>Status : <span class="inprogress">Inprogress</span></div>
                                                <div><img src="{{ asset('adminassets/images/svg-icons/for-sale-icon.svg') }}" alt=""><span>For Sale</span></div>
                                            </div><!-- //div-->
                                            <div class="flex-fill w_100">
                                                <a href="javascript:void(0);" class="btn btn-primary">Download Sazra Map</a>
                                            </div><!-- //flex-fill -->
                                            <div class="flex-fill">
                                                <a href="#schedule-Visit-popup" data-effect="mfp-zoom-in" class="schedule-visit">Schedule a Visit</a>
                                            </div><!-- //flex-fill-->
                                            <div class="flex-fill">
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                </ul>
                                            </div><!-- //flex-fill-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card for-sale-land -->
                                </div><!-- //col-lg-6 col-12-->

                                <div class="col-lg-6 col-12">
                                    <div class="card for-sale-land">
                                        <div class="card-body">
                                            <div>
                                                <h5>21 Acres Town Land</h5>
                                                <span>1012, Sirhind Near Fategarh Sahib, Chandigarh, Punjab</span>
                                            </div><!-- //div-->
                                            <div>
                                                <span class="landPrice">₹ 19.21 Cr</span>
                                            </div><!-- //div-->                                                  
                                            <div class="bottom-border"></div><!-- //div-->
                                            <div class="sale-land-details">
                                                <div>Mobile Number : <span>+91-9560456464</span></div>
                                                <div>Who you are : <span>Owner</span></div>
                                                <div>Status : <span class="pending">Pending</span></div>
                                                <div><img src="{{ asset('adminassets/images/svg-icons/for-sale-icon.svg') }}" alt=""><span>For Sale</span></div>
                                            </div><!-- //div-->
                                            <div class="flex-fill w_100">
                                                <a href="javascript:void(0);" class="btn btn-primary">Download Sazra Map</a>
                                            </div><!-- //flex-fill -->
                                            <div class="flex-fill">
                                                <a href="#schedule-Visit-popup" data-effect="mfp-zoom-in" class="schedule-visit">Schedule a Visit</a>
                                            </div><!-- //flex-fill-->
                                            <div class="flex-fill">
                                                <ul class="day-view">
                                                    <li>2 days ago</li>
                                                </ul>
                                            </div><!-- //flex-fill-->
                                        </div><!-- //card-body -->
                                    </div><!-- //card for-sale-land -->
                                </div><!-- //col-lg-6 col-12-->
                            </div><!-- //row row_20 -->
                        </div><!-- //card-deck -->
                    </div><!--//tab-pane id=requestLandlisting -->

                </div><!-- //Tab panes End -->
                
                           
@endsection

@section('modal')

               
 <!-- see-Graph-popup -->
    <div id="seeGraph-popup" class="mfp-with-anim mfp-hide seeGraph-popup">
        <div class="popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close"></button>
            <div class="row">
                <div class="col-6"><h2>Views on your lisitng</h2></div><!-- //.col-6 -->
                <div class="col-6 showBy">
                    <div class="dropdown">
                        <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                          <span class="selected">Month</span><span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">Week</a></li>
                        <li><a href="#">Month</a></li>
                        <li><a href="#">Year</a></li>
                      </ul>
                    </div><!-- //dropdown -->
                    <span class="float-right pt-2">Show by</span>
                </div><!-- //showBy-->
                <div class="col-12">
                    <div class="viwe-graph">
                    <img src="{{ asset('adminassets/images/graph.png') }}" alt="" class="w-100">
                    </div>
                </div><!-- //col-12-->
            </div><!--/row-->
        </div><!-- //popup-body --> 
    </div><!-- //see-Graph-popup -->

    <!-- Schedule-Visit-popup -->
    <div id="schedule-Visit-popup" class="mfp-with-anim mfp-hide schedule-Visit">
        <div class="popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close"></button>
            <div class="row">
                <div class="col-12"><h2>Views on your lisitng</h2></div><!-- //.col-12 -->
                <div class="col-12">
                    <form>
                        <div class="scheduleVisit">
                            <div class="selectExpart">
                                <div class="form-group">
                                    <select class="form-control" id="reportType">
                                        <option>Select Expert</option>
                                        <option>Abhit Bhatia</option>
                                        <option>Manish Bhandari</option>
                                        <option>Vipin Vindal</option>
                                    </select>
                                </div><!-- //form-group -->
                            </div><!-- //selectExpart-->
                            <div class="selectDate">
                                <div class="form-group">
                                    <input class="form-control" id="datepicker1" type="text" placeholder="Select Date"/>
                                </div><!-- //form-group -->
                            </div><!--//selectDate-->
                            <div class="selectTime">
                                <div class="form-group input-group">
                                    <input class="form-control" id="timepicker" type="text" placeholder="Select Time"/>
                                </div>
                            </div><!--//selectTime-->
                            <div class="scheduleVisit-btn">
                                <input type="submit" class="btn btn-primary" value="Schedule">
                            </div><!-- //scheduleVisit-btn-->
                        </div><!--//scheduleVisit-->
                    </form>
                </div><!-- //.col-12 -->
            </div><!--/row-->
        </div><!-- //popup-body --> 
    </div><!-- //Schedule-Visit-popup -->

@endsection


@section('custom-script')




    @endsection