<script type="text/javascript" src="{{ asset('adminassets/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminassets/js/bootstrap.min.js') }}"></script>
<!-- //Magnific Popup JS -->
<script type="text/javascript" src="{{ asset('adminassets/js/jquery.magnific-popup.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('adminassets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>

<!-- //Multiselect JS -->
<script src="{{ asset('adminassets/js/bootstrap-multiselect.js') }}"></script>

<script type="text/javascript" src="{{ asset('adminassets/js/gijgo.min.js') }}"></script>

@if( Request::route()->getName() == 'admin.appointments' )  
<!--<script type="text/javascript" src="{{ asset('adminassets/js/calendar-main.js') }}"></script>-->

@endif

<script type="text/javascript" src="{{ asset('adminassets/js/owl.carousel.js') }}"></script>

@if( Request::route()->getName() == 'admin.messages' )
<script src="{{ asset('adminassets/js/jquery.mockjax.jss') }}"></script>
<script src="{{ asset('adminassets/js/autoselectdd-bootstrap-typeahead.js') }}"></script>
@endif
<script type="text/javascript" src="{{ asset('adminassets/js/admin-custom.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
<script type="text/javascript" src="{{ asset('adminassets/js/gijgo.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/vendor/jquery.ui.widget.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/load-image.all.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/canvas-to-blob.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/jquery.fileupload.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/jquery.fileupload-process.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/jquery.fileupload-validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/jquery.fileupload-image.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>-->
<script type="text/javascript" src="{{ asset('js/Chart.js') }}"></script>
<script src="{{ asset('adminassets/js/jquery.timepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>

<!-- 
 <script type="text/javascript" src="{{ asset('adminassets/js/owl.carousel.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
