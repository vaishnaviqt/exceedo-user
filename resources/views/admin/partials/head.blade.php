<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>{{ config('app.name', 'Exceedo') }} &#8211; @yield('title')</title>
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/css/bootstrap.min.css') }}">

<!-- //Magnific Popup Css -->
<link rel="stylesheet" href="{{ asset('adminassets/css/magnific-popup.css') }}">
@if( Request::route()->getName() == 'admin.dashboard' ) 
<!-- //Multiselect Css -->
<link rel="stylesheet" href="{{ asset('adminassets/css/bootstrap-multiselect.css') }}">
@endif
@if( Request::is('admin')) 
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css?v=1.1') }}">
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/css/admin-style.css?v=1.1') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/css/admin-responsive.css?v=1.1') }}">
 <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}"> 
<link rel="stylesheet" href="{{ asset('adminassets/css/jquery.mCustomScrollbar.min.css') }}">

@if( Request::route()->getName() == 'admin.appointments' )  
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/css/calendar-main.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/css/bootstrap-float-label.min.css') }}">
@endif
@if( Request::route()->getName() == 'admin.appointments'  || Request::route()->getName() == 'admin.reports' || Request::route()->getName() == 'admin.land-listing' || Request::route()->getName() == 'admin.payments' || Request::route()->getName() == 'admin.rating-and-review' || Request::route()->getName() == 'admin.messages' || Request::route()->getName() == 'admin.your-profile')  
@endif
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css" />
<!-- <link href="{{ asset('adminassets/css/gijgo.min.css') }}" rel="stylesheet" type="text/css" /> -->
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.css" rel="stylesheet" type="text/css" />


@if( Request::route()->getName() == 'admin.reports' )  
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/css/owl.carousel.css') }}">
@endif
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 

<script type="text/javascript" src="{{ asset('adminassets/js/jquery.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('upload/css/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ asset('upload/css/jquery.fileupload-ui.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<link rel="stylesheet" href="{{ asset('adminassets/css/jquery.timepicker.min.css')}}"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="base_url" content="{{ url('/') }}">