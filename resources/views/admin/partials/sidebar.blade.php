 <a href="javascript:void(0);" class="hamburgerMenu"></a>
            <div class="sidebarLogo">
                <a href="{{ route('admin.dashboard') }}">
                    <img src="{{ asset('adminassets/images/logo.svg') }}" alt="Know your Land" />
                    <span>Know your Land</span>
                </a>
            </div><!--//sidebarLogo-->

            <div class="sideNavigation customScrollbar">
                <ul>
                    <li><a href="{{ route('admin.dashboard') }}" class="nav_dashboard @if( Request::route()->getName() == 'admin.dashboard' ) active @endif">Dashboard</a></li>
        @php
        $appointment = secondaryAdminPermission(3);
        @endphp
        @if(auth()->user()->id == 1 || !empty($appointment))
                    <li><a href="{{ route('admin.appointments') }}" class="nav_appoint @if( Request::route()->getName() == 'admin.appointments' ) active @endif">Appointments</a></li>
        @endif

        @php
        $reports = secondaryAdminPermission(1);
        @endphp
        @if(auth()->user()->id == 1 || !empty($reports))
                    <li><a href="{{ route('admin.reports') }}" class="nav_reports @if( Request::route()->getName() == 'admin.reports' ) active @endif">Reports</a></li>
        @endif
        @php
        $land = secondaryAdminPermission(2);
        @endphp
        @if(auth()->user()->id == 1 || !empty($land))
                    <li><a href="{{ route('admin.land-listing') }}" class="nav_land  @if( Request::route()->getName() == 'admin.land-listing' ) active @endif">Land Listing</a></li>
        @endif

        @if(auth()->user()->id ==1)
                    <li><a href="{{ url('admin/pages') }}" class="nav_land  @if( request()->is('admin/pages', 'admin/page-create', 'admin/edit-page/*') ) active @endif">Pages</a></li>
                    <li><a href="{{ route('admin.experts') }}" class="nav_experts @if( Request::route()->getName() == 'admin.experts' ) active @endif">Experts</a></li>
                    <li><a href="{{ route('admin.users') }}" class="nav_profile @if( Request::route()->getName() == 'admin.users' ) active @endif">Users</a></li>
                    <li><a href="{{ route('admin.brokers') }}" class="nav_experts @if( Request::route()->getName() == 'admin.brokers' ) active @endif">Brokers</a></li>
                    <li><a href="{{ url('admin/value-added-services') }}" class="nav_experts @if( request()->is('admin/experts-value-added-services')  ) active @endif">Value Added Services</a></li>
                    <li><a href="{{ route('admin.service-enquiry') }}" class="nav_rating @if( Request::route()->getName() == 'admin.service-enquiry' ) active @endif">Service Enquiries</a></li>
                    <li><a href="{{ route('admin.payments') }}" class="nav_payments @if( Request::route()->getName() == 'admin.payments' ) active @endif">Payments</a></li>
                    <li><a href="{{ route('admin.rating-and-review') }}" class="nav_rating @if( Request::route()->getName() == 'admin.rating-and-review' ) active @endif">Rating & Reviews</a></li>
                    <li><a href="{{ route('admin.messages') }}" class="nav_msg @if( Request::route()->getName() == 'admin.messages' ) active @endif">Message</a></li>
        @endif
                    <li><a href="{{ route('admin.your-profile') }}" class="nav_profile  @if( Request::route()->getName() == 'admin.your-profile' ) active @endif">Your Profile</a></li>
                    <li><a href="{{ route('admin.adminlogout') }}" class="nav_logout">Logout</a></li>
                </ul>
            </div><!--//navigation-->
@if(auth()->user()->id ==1)
            <div class="addSecondAdminBtn" id="inline-popups">
                <!--<a href="#secondary-Admin-popup" data-effect="mfp-zoom-in" class="btn btn-primary">Add Secondary Admin</a>-->
    <a href="#secondary-Admin-popup" data-effect="mfp-zoom-in" class="btn btn-primary secondaryAdminAddButton">Add Secondary Admin</a>
            </div>
@endif
