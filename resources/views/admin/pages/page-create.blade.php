@extends('admin.layouts.master')
@section('title')
Send Messages
@endsection
@section('class')
message-admin
@endsection
@section('content') 
<script src="http://localhost/kdblog/source/public/packages/ckeditor/ckeditor.js"></script>
<script src="http://localhost/kdblog/source/public/packages/ckeditor/adapters/jquery.js"></script>
<h2>New Page Create</h2> 
@include('flash-message')
<form action="{{ url('/admin/page-store') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <input id="title" type="text" name="title" value="{{ old('title') }}" class="form-control @error('title') is-invalid @enderror" placeholder="Title" required>
        @error('title')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <input id="slug" type="text" name="slug" value="{{ old('slug') }}" class="form-control @error('slug') is-invalid @enderror" placeholder="Slug" required>
        @error('slug')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <textarea class="form-control @error('description') is-invalid @enderror" data-url="{{ url('/') }}" id="description" name="description" required>{{ old('description') }}</textarea>
        @error('description')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <input id="thumb" type="file" name="thumb" value="{{ old('thumb') }}" class="form-control @error('thumb') is-invalid @enderror">
        @error('thumb')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <input id="metaTitle" type="text" name="meta_title" value="{{ old('meta_title') }}" class="form-control @error('meta_title') is-invalid @enderror" placeholder="Meta Title">
        @error('meta_title')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <textarea class="form-control @error('meta_description') is-invalid @enderror" placeholder="meta Description" id="meta_description" name="meta_description" rows="8">{{ old('meta_description') }}</textarea>
        @error('meta_description')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <textarea class="form-control @error('meta_keywords') is-invalid @enderror" placeholder="Meta Keywords" id="meta_keywords" name="meta_keywords" rows="8">{{ old('meta_keywords') }}</textarea>
        @error('meta_keywords')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Send</button>
    <!-- //form-group-->
</form>
<script>
$(document).ready(function () {
    $('#description').ckeditor();
    $('#title').on('keyup', function () {
        var title = $(this).val();
        var slug = title.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
        $('#slug').val(slug);
    });
});
</script>
@endsection
@section('modal')
@endsection
@section('custom-script')
@endsection