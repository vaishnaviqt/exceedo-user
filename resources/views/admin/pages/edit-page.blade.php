@extends('admin.layouts.master')
@section('title')
Send Messages
@endsection
@section('class')
message-admin
@endsection
@section('content') 
<script src="http://localhost/kdblog/source/public/packages/ckeditor/ckeditor.js"></script>
<script src="http://localhost/kdblog/source/public/packages/ckeditor/adapters/jquery.js"></script>
<h2>Edit Page</h2> 
@include('flash-message')
<form action="{{ url('/admin/update-page') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <input type="hidden" name="id" value="{{ $page->id }}">
        <input id="title" type="text" name="title" value="{{ old('title', $page->title) }}" class="form-control @error('title') is-invalid @enderror" placeholder="Title" required>
        @error('title')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <input id="slug" type="text" name="slug" value="{{ old('slug', $page->slug) }}" class="form-control @error('slug') is-invalid @enderror" placeholder="Slug" required>
        @error('slug')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <textarea class="form-control @error('description') is-invalid @enderror" data-url="{{ url('/') }}" id="description" name="description" required>{{ old('description', $page->description) }}</textarea>
        @error('description')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        @php
        $path = '';
        if(Storage::disk('pages')->exists(str_replace('page_image', '', $page->thumb)))
        {
        $path = Storage::disk('pages')->url(str_replace('page_image/', '', $page->thumb));
        }
        @endphp
        <img src="{{ $path }}" class="img-fluid img-thumbnail w-25" alt="{{ $page->title }}" />
        <input type="hidden" name="old_image" value="{{ $page->thumb }}">
        <input id="thumb" type="file" name="thumb" value="{{ old('thumb') }}" class="form-control @error('thumb') is-invalid @enderror">
        @error('thumb')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <input id="metaTitle" type="text" name="meta_title" value="{{ old('meta_title') }}" class="form-control @error('meta_title') is-invalid @enderror" placeholder="Meta Title">
        @error('meta_title')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <textarea class="form-control @error('meta_description') is-invalid @enderror" placeholder="meta Description" id="meta_description" name="meta_description" rows="8">{{ old('meta_description', $page->meta_description) }}</textarea>
        @error('meta_description')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <textarea class="form-control @error('meta_keywords') is-invalid @enderror" placeholder="Meta Keywords" id="meta_keywords" name="meta_keywords" rows="8">{{ old('meta_keywords', $page->meta_keywords) }}</textarea>
        @error('meta_keywords')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Send</button>
    <!-- //form-group-->
</form>
<script>
$(document).ready(function () {
    $('#description').ckeditor();
});
</script>
@endsection
@section('modal')
@endsection
@section('custom-script')
@endsection