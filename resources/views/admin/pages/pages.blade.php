@extends('admin.layouts.master')
@section('title')
Payments
@endsection
@section('class')
paymentsBody
@endsection
@section('content')
<div>
    @include('flash-message')
    <div class="pageTitle d-flex align-items-center">
        <h1>Pages</h1>
        
        <div class="sortDropdown">
            <label>Show by</label>
            <div class="dropdown custom-dropdown">
                <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle min-w-60">
                    <span class="selected">{{ !empty($num) ? $num : 10}}</span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a class="pagnateData" data-page="10" href="javascript:;">10</a></li>
                    <li><a class="pagnateData" data-page="25" href="javascript:;">25</a></li>
                    <li><a class="pagnateData" data-page="50" href="javascript:;">50</a></li>
                    <li><a class="pagnateData" data-page="100" href="javascript:;">100</a></li>
                </ul>
            </div><!-- //dropdown -->
            <form action="{{ url('admin/pages') }}" method="post" id="showByForm">
                @csrf
                <input type="hidden" name="showBy" value="" id="show_by"> 
            </form> 
        </div><!--//sortDropdown-->
    </div><!--//pageTitle-->

    <div class="reportNavBar mb-2 pt-2">                        
        <div class="genReportLink">
            <a href="{{ url('admin/page-create') }}">Add New Page</a>
        </div>
    </div><!--//reportNavBar-->
  

    <div class="payment-table-desktop border">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Thump</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($pages as $page)
                <tr>
                    <td scope="row" style="width:45%">{{ $page->title }}</td>
                    <td style="width:30%">
                        @php
                        $path = '';
                        if(Storage::disk('pages')->exists(str_replace('page_image', '', $page->thumb)))
                        {
                        $path = Storage::disk('pages')->url(str_replace('page_image/', '', $page->thumb));
                        }
                        @endphp
                        <img src="{{ $path }}" class="img-fluid img-thumbnail d-block" alt="{{ $page->title }}" />
                    </td>
                    <td>{{ Carbon\Carbon::parse($page->updated_at)->diffForHumans() }}</td>
                    <td>
                        <a href="{{ url('admin/edit-page/'.$page->slug) }}" class="mr-2">
                            <img src="{{ asset('adminassets/images/svg-icons/edit-black-icon.svg') }}" alt="">    
                        </a>
                        <a href="{{ url('admin/delete-page/'.$page->id) }}" class="">
                            <img src="{{ asset('adminassets/images/svg-icons/delete-icon.svg') }}" alt=""></a>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="5">No Pages Available</td>
                </tr>
                @endforelse
            </tbody>
        </table>
        <div class="col-12">
            {{ $pages->links() }}
        </div><!-- //col-12 pagination -->
    </div><!--//payment-table-desktop-->
    <div class="payment-table-Mobile border">
        @forelse($pages as $page)
        <div class="d-flex flex-wrap justify-content-between payment-list">
            <div style="width: 80%"><strong>{{ $page->title }}</strong></div>
            <div>
                <a href="{{ url('admin/edit-page/'.$page->slug) }}" class="mr-2">
                    <img src="{{ asset('adminassets/images/svg-icons/edit-black-icon.svg') }}" alt="">    
                </a>
                <a href="{{ url('admin/edit-delete/'.$page->id) }}">
                    <img src="{{ asset('adminassets/images/svg-icons/delete-icon.svg') }}" alt="">
                </a>
            </div>
            <div class="w-100">{{ Carbon\Carbon::parse($page->updated_at)->diffForHumans() }}</div>
        </div><!-- //d-flex flex-wrap-->
        @empty
        <div class="d-flex flex-wrap justify-content-between payment-list">
            <strong>No Pages Available</strong>
        </div><!--//payment-table-Mobile-->
        @endforelse
    </div><!--// payment-table-Mobile-->

</div>
@endsection
@section('modal')
@endsection
@section('custom-script')
<script>
    $(document).ready(function () {
        $('.pagnateData').on('click', function () {
            var showBy = $(this).data('page');
            $('#show_by').val(showBy);
            $('#showByForm').submit();
        });
    });
</script>

@endsection