@extends('admin.layouts.master')
@section('title')
Service Enquiry
@endsection
@section('class')
paymentsBody
@endsection
@section('content')
<style>
    .loader{
        position: fixed;
        top:0;
        left:0;
        right: 0;
        bottom:0;
        display:flex;
        align-items:center;
        justify-content:center;
        z-index: 9999;
        background: #ffffffa6;
    }
    .loader img{
        width:80px;
    }
</style>
<div class="loader d-none">
    <img src="{{ asset('images/Spin-1s-200px.gif') }}">
</div>

<div class="pageTitle d-flex align-items-center">
    <h2>{{ __('Service Enquiries') }}</h2> 
</div><!--//pageTitle-->

<!-- //Search Form Div Start -->
<div class="searchForm"> 
    <a href="javascript:void(0);" class="open-mobile-search">{{ __('Search') }}</a>                                   
    <form action="{{ url('admin/service-enquiry')}}" method="post">
        @csrf
        <span>Search</span>
        <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
        <ul class="d-flex flex-wrap width-18">
            <li>
                <div class="form-group">
                    <input class="form-control" id="filterDatepicker" type="text" name="created_at" 
                           value="{{ !empty($startDate) ? Carbon\Carbon::parse($startDate)->format('d-m-Y') : '' }}" 
                           placeholder="Date"/>
                </div><!-- //form-group -->
            </li>
            <li>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Customer Name" name="customerName" id="customerName" value="{{ $customerName }}">
                </div><!-- //form-group -->
            </li>

            <li>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Customer Email" name="customerEmail" id="customerEmail" value="{{ $customerEmail }}">
                </div><!-- //form-group -->
            </li>

            <li>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Customer Mobile Number" name="customerMobile" id="customerMobile" value="{{ $customerMobile }}">
                </div><!-- //form-group -->
            </li>


            <li>
                <div class="form-group">
                    <select class="form-control" id="serviceType" name="serviceType">
                        <option value="">{{ __('Services') }}</option>
                        @if(count($services))
                        @foreach($services as $service)
                        @php
                        $selected = '';
                        if($service->id == $serviceType){
                        $selected = 'selected';
                        }
                        @endphp
                        <option value="{{ $service->id }}" {{ $selected }}>{{ $service->service_name }}</option>
                        @endforeach
                        @endif
                    </select>
                </div><!-- //form-group --> 
            </li>
            <li>
                <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
            </li>
        </ul><!-- //ul -->
    </form><!-- //form -->
</div><!-- //search-form -->
<!-- //Search Form Div End -->

<div class="rating-reviews">
    <!--    <h4>10 Nov 2020</h4>-->


    <div class="customTable border">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Description</th>
                    <th>Service</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($enquiries as $enquiry)
                <tr>
                    <td scope="row">
                        <span class="m_tableTitle">Name</span>
                        {{ isset($enquiry->name) ?  ucwords($enquiry->name) : ''}}
                    </td>
                    <td>
                        <span class="m_tableTitle">Email</span>
                        {{ isset($enquiry->email) ?  ($enquiry->email) : ''}}
                    </td>
                    <td>
                        <span class="m_tableTitle">Mobile</span>
                        {{ isset($enquiry->mobile) ?  ucwords($enquiry->mobile) : ''}}
                    </td>
                    <td>
                        <span class="m_tableTitle">Description</span>
                        {{ isset($enquiry->description) ?  ucwords($enquiry->description) : ''}}
                    </td>
                    <td>
                        <span class="m_tableTitle">Service</span>
                        {{ isset($enquiry->service_name) ?  ucwords($enquiry->service_name) : ''}}
                    </td>
                    <td>
                        <span class="m_tableTitle">Created At</span>
                        {{ Carbon\Carbon::parse($enquiry->created_at)->format('d M, Y') }}
                    </td>
                    <td>
                        <span class="m_tableTitle">Action</span>
                        @if($enquiry->status == 0)
                        @if(empty($enquiry->expert_id) || $enquiry->cancel == 1)
                        <a href="javascript:;" data-expert="{{ $enquiry->id }}" class="selectExpartAssign">Select the Expert</a>
                        @else
                        Pending expert confirmation
                        @endif
                        @else
                        Expert confirm this service enquiry.
                        @endif
                    </td>
                </tr>
                @empty
                <tr>
                    <td scope="row" colspan="4">No data available</td>
                </tr>
                @endforelse
            </tbody>
        </table>
        <div class="col-12">
            {{ $enquiries->links() }}
        </div><!-- //col-12 pagination -->
    </div><!--//payment-table-desktop-->


</div><!--//rating-reviews-->
@endsection
@section('modal')
<!-- Select the Expert-popup -->
<div id="select-expert-popup" class="mfp-with-anim mfp-hide select-expert">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12"><h2>Assign Expert</h2></div><!-- //.col-12 -->
            <div class="col-12">
                <form class="enquiryAssignForm" id="enquiryAssignForm">
                    <div class="scheduleVisit">
                        <div class="selectExpart mb-4">
                            <div class="form-group mb-3">
                                @php
                                $experts = expertListShow();
                                @endphp
                                <input type="hidden" name="enquiryId" value="" id="enquiryId">
                                <select class="form-control SelectExparttAssign"id="expertId" name="expert_id">
                                    <option value="">Select Expert</option>
                                    @php
                                    $experts = expertListShow();
                                    @endphp
                                    @if(count($experts))
                                    @foreach($experts as $expert)
                                    <option value="{{ $expert->id }}">{{ $expert->name }} {{ isset($expert->state->name) ? '(' . $expert->state->name . ')' : ''  }} {{ showRatingReport($expert->id) }} </option>
                                    @endforeach
                                    @endif
                                </select>
                            </div><!-- //form-group -->

                            <div class="scheduleVisit-btn">
                            <input type="button" class="btn btn-primary" id="serviceEnquiryAllExpert" value="All Experts">
                        </div>
                        </div><!-- //selectExpart-->
                        <div class="custom-file mb-4">
                            <input type="file" class="custom-file-input" name="file" id="enquiryFile">
                            <label class="custom-file-label" for="enquiryFile">File</label>
                        </div>
                        <div class="form-group w-100 mb-3">
                            <textarea class="form-control" id="summary" name="summary"></textarea>

                        </div>
                        <div class="scheduleVisit-btn">
                            <input type="button" class="btn btn-primary" id="enquiryAssingSubmitButton" value="Assignee">
                        </div><!-- //scheduleVisit-btn-->

                    </div><!--//scheduleVisit-->
                </form>
            </div><!-- //.col-12 -->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Select the Expert-popup -->
@endsection
@section('custom-script')
@php
 $experts = expertListShow();
 @endphp
<script>
    $(document).ready(function () {
        $('.admin-notify-report').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var documentId = $(this).data('document');
            $('#expertReportId').val(id);
            $('#documentId').val(documentId);
        });
        $('.selectExpartAssign').on('click', function (e) {
            var id = $(this).data('expert');

            $('#enquiryId').val(id);
            $.ajax({
                type: "post",
                url: "assign-expert-popup",
                data: {service: id},
                success: function (data) {
                    $('.SelectExparttAssign').html('');
                            $('.SelectExparttAssign').append('<option value="">Select Expert</option>');
                        $.each(data, function(i, expert){
                            $('.SelectExparttAssign').append('<option value="'+expert.id+'">'+expert.name+'</option>');
                        })
                }
             });
            $.magnificPopup.open({
                items: {
                    src: '#select-expert-popup',
                    callbacks: {
                        beforeOpen: function () {
                            this.st.mainClass = this.st.el.attr('data-effect');
                        }
                    },
                    type: 'inline'
                }
            });
        });

        $('#enquiryAssingSubmitButton').on('click', function () {
            $('.loader').toggleClass('d-none');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formData = new FormData(document.getElementById("enquiryAssignForm"));
            $.ajax({
                type: 'post',
                url: "enquiry-assing-expert",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    ;
                    $('.loader').toggleClass('d-none');
                    if (data) {
                        $('.enquiryAssignForm')[0].reset();
                        swal({
                            title: "Good job!",
                            text: "Service Enquiry assinged to successfully!",
                            icon: "success",
                        }).then((value) => {
                            location.reload();
                        });
                    } else {
                        swal({
                            text: "Something went wrong!",
                            icon: "error",
                        });
                    }
                },
                error: function (errorResponse) {
                    $('.loader').toggleClass('d-none');
                    $('#service-feedback').prop("disabled", false);
                    $('#service-feedback').text('Submit');
                    $('.error-span').remove();
                    $.each(errorResponse.responseJSON.errors, function (field_name, error) {
                        $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    })
                }
            });
        });



        $('#serviceEnquiryAllExpert').on('click', function(){
                $.ajax({
                        type: "post",
                        url: "assign-expert-popup",
                        success: function (data) {
                            $('.SelectExparttAssign').html('');
                                    $('.SelectExparttAssign').append('<option value="">Select Expert</option>');
                                $.each(data, function(i, expert){
                                    $('.SelectExparttAssign').append('<option value="'+expert.id+'">'+expert.name+'</option>');
                                })
                        }
                });

         });

        $('.customCheckReport').on('click', function () {
//                            $('.loader').toggleClass('d-none');
            var id = $(this).data('id');
            $('#deliveredReportId').val(id);
            $.magnificPopup.open({
                items: {
                    src: '#reportDeliveredToUser',
                    callbacks: {
                        beforeOpen: function () {
                            this.st.mainClass = this.st.el.attr('data-effect');
                        }
                    },
                    type: 'inline'
                }
            });
        });
    });


    $('.deliveredReportButton').on('click', function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: "report-delivered",
            data: $("#deliveredReportForm").serialize(),
            success: function (data) {
                $('.loader').toggleClass('d-none');
                if (data) {
                    $('.reportAssignForm')[0].reset();
                    swal({
                        title: "Good job!",
                        text: "Report delivered successfully!",
                        icon: "success",
                    }).then((value) => {
                        location.reload();
                    });
                } else {
                    swal({
                        text: "Something went wrong!",
                        icon: "error",
                    });
                }
            },
        });
    });
</script>



<script>
    $(document).ready(function () {
        // datepicker
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#filterDatepicker').datepicker({
            maxDate: today,
            format: 'dd-mm-yyyy'
        });
    })
</script>

@endsection