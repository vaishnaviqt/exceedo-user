<!DOCTYPE html>
<html lang="en">
<head>
     @include('admin.partials.head')
</head>
<body>  
    <div class="wrapper">
        

        <div class="main">

            <section class="pageContainer">
                
                
<form  class="bg-white shadow rounded-lg p-8 max-w-login mx-auto"  method="POST"   action="{{ route('adminlogin') }}" >
     @csrf
    @if ($errors->any())
    <p class="text-center font-semibold text-danger my-3">
        @if ($errors->has('email'))
            {{ $errors->first('email') }}
        @else
            {{ $errors->first('password') }}
        @endif
        </p>
    @endif

    <div class="mb-6 {{ $errors->has('email') ? ' has-error' : '' }}">
        <label class="block font-bold mb-2" for="email">{{ __('Email Address') }}</label>
        <input class="form-control form-input" id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
        @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">{{ $errors->first('email') }}</span>
        @endif
    </div>

    <div class="mb-6 {{ $errors->has('password') ? ' has-error' : '' }}">
        <label class="block font-bold mb-2" for="password">{{ __('Password') }}</label>
        <input class="form-control form-input" id="password" type="password" name="password" required>
         @error('password')
            <span class="invalid-feedback" role="alert">{{ $message }}</span>
        @enderror
    </div>

    <div class="flex mb-6">
       
        <div class="ml-auto">
            <a class="text-primary dim font-bold no-underline" href="">
                {{ __('Forgot Your Password?') }}
            </a>
        </div>
       
    </div>

    <button class="w-full btn btn-default btn-primary hover:bg-primary-dark" type="submit">
        {{ __('Login') }}
    </button>
</form>
              
            </section><!--//pageContainer-->

            <footer class="footer">
                @include('admin.partials.footer')
            </footer>    
        
        </div><!--//main-->

    </div><!--//wrapper--> 
 
</body>
</html>