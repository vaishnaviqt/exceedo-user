@extends('admin.layouts.master')
@section('title')
Brokers
@endsection
@section('content')
<style>
    .loader{
        position: fixed;
        top:0;
        left:0;
        right: 0;
        bottom:0;
        display:flex;
        align-items:center;
        justify-content:center;
        z-index: 9999;
        background: #ffffffa6;
    }
    .loader img{
        width:80px;
    }
</style>
<div class="loader d-none">
    <img src="{{ asset('images/Spin-1s-200px.gif') }}">
</div>

<div>
    <div class="pageTitle d-flex align-items-center">
        <h2>Brokers</h2>
        
        <div class="sortDropdown">
            <label>Show</label>
            <div class="dropdown custom-dropdown">
                <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle min-w-60">
                    <span class="selected">{{ $paginate ?? 10 }}</span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" class="brokerPaginate" data-value="10">10</a></li>
                    <li><a href="#"  class="brokerPaginate" data-value="25">25</a></li>
                    <li><a href="#"  class="brokerPaginate" data-value="50">50</a></li>
                    <li><a href="#"  class="brokerPaginate" data-value="100">100</a></li>
                </ul>
            </div><!-- //dropdown -->
            <!--//paginate form-->
            <form method="post" id="paginateSubmitForm">
                @csrf
                <input type="hidden" name="paginate" id="paginateHiddenFiled">
            </form>
            <!--end Paginate Form-->
        </div><!--//sortDropdown-->
    </div><!--//pageTitle-->
 
 @include('flash-message')
    <!-- //Search Form Div Start -->
    <div class="row">
        <div class="col-lg-9">
            <div class="searchForm">
                <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                   
                <form  method="post" action="{{ route('admin.brokers') }}" id="frm_brokers_search">
                    @csrf
                    <span>Search</span>
                    <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
                    <ul class="d-flex flex-wrap">
                        <li>
                            <div class="form-group">
                                <input type="text" name="broker_name" class="form-control" placeholder="Name"  value="{{$broker_name ?? ''}}" id="Name">
                            </div><!-- //form-group -->
                        </li>

                        <li>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="Email"  value="{{$email ?? ''}}" id="Email">
                            </div><!-- //form-group -->
                        </li>

                        <li>
                            <div class="form-group">
                                <input type="number" name="mobile" class="form-control" placeholder="Mobile Number"  value="{{$mobile ?? ''}}" id="Mobile">
                            </div><!-- //form-group -->
                        </li>
                        <li>
                            <button class="btn btn-primary" data-toggle="tab" onclick="formReset()">Reset</button>
                        </li>
                        <li>
                            <button type="submit" class="btn btn-primary" name="">Search</button>
                        </li> 
                    </ul><!-- //ul -->
                </form><!-- //form -->
            </div><!-- //search-form -->
            <!-- //Search Form Div End -->
        </div>
        <div class="col-lg-3">            
            <div class="reportNavBar pt-2 mb-2">                        
                <div class="genReportLink">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#join_broker" id="add_broker">Add Broker</a>
                </div>  
            </div><!--//reportNavBar-->
        </div>
    </div><!--//row-->

    <div class="customTable border">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Broker Name</th>
                    <th>Mobile</th>
                    <th>Email Id</th>
                    <th>Broker ID</th>
                    <th>Date Of Joining</th>
                    <th width="20%"></th>
                </tr>
            </thead>
            <tbody>

                @if (count($brokers) > 0)
                @foreach($brokers as $broker)
                <tr>
                    <td scope="row">
                        <span class="m_tableTitle">Broker Name</span>
                        {{ $broker->name }}
                    </td>
                    
                    <td>
                        <span class="m_tableTitle">Mobile</span>
                        {{ $broker->mobile }}
                    </td>
                    <td>
                        <span class="m_tableTitle">Email Id</span>
                        {{ $broker->email }}
                    </td>
                    <td>
                        <span class="m_tableTitle">Broker Id</span>
                        {{ $broker->user_unique_id }}

                        @foreach ($broker->roles->pluck('name') as $rle)
                        {{ ucfirst($rle) }}
                        @endforeach
                    </td>
                    <td>
                        <span class="m_tableTitle">Email Id</span>
                        {{ Carbon\Carbon::parse($broker->created_at)->format('d M, Y h:i:s') }}
                    </td>
                    <td>
                        <a href="javascript:void(0);" class="deleteRow" data-toggle="modal" data-target="#join_broker" onclick="editBroker({{$broker}})" id="brokeredit">
                            Edit
                        </a> 
                        <a data-toggle="modal" data-target="#Delete" href="javascript:void(0);" class="deleteRow" onclick="deleteBroker({{$broker->id}})" >
                            <img src="{{ asset('adminassets/images/svg-icons/delete-icon.svg') }}" alt="">
                        </a>
                        @if($broker->user_status==1)
                        <button class="btn btn-primary" onclick="ActiveUser({{$broker->id}})">Active</button>
                        @else
                        <button class="btn btn-danger" onclick="DeavtiveUser({{$broker->id}})">Deactive</button>
                        @endif
                    </td>
                </tr>
                @endforeach
                @else

                <tr>
                    <td scope="row" colspan="4">Broker not found</td>
                </tr>

                @endif
            </tbody>
        </table>

        <div class="col-12">
            {{ $brokers->links() }}
        </div><!-- //col-12 pagination -->
    </div><!--//payment-table-desktop-->

            </div><!--//tab-pane -->
        </div><!-- //Tab panes End -->
    </div><!-- //col-12 -->  

</div><!-- //row -->     
@endsection
@section('modal')
<!-- Modal -->     
<!-- Join User -->
<div class="modal fade" id="join_broker" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="row no-gutters">
                    <div class="col-md-12">
                        <div class="loginPageBox" id="signup">
                            <div class="" id="myTabContentSignup">
                                <div class="alert alert-success alert-block broker-msg" style="display: none;">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                </div>
                                <form name="frm_broker_signup" id="frm_broker_signup" method="post">
                                    @csrf
                                    <div class="popupForm">  

                                        <input type="hidden" class="form-control"  name="id" id="brokerid">
                                        <div class="form-group floating-field">
                                            <input type="hidden" class="form-control" class="editBroker"  name="edit" value="">
                                            <input type="text" class="form-control" placeholder="Full Name" name="full_name" id="editname">
                                            <label for="editname" class="floating-label">Full Name</label>
                                        </div><!-- floating-field -->

                                        <div class="form-group floating-field">
                                            <input type="text" class="form-control" placeholder="Email id" name="email" id="editemail">
                                            <label for="editemail" class="floating-label">Email id</label>
                                        </div><!-- floating-field -->

                                        <div class="form-group floating-field mobile-field">
                                            <input type="text" class="form-control" placeholder="Mobile Number" name="mobile_number" id="editmobile_number">
                                            <label for="mobile" class="floating-label">Mobile Number</label>
                                            <span class="mobileCode">+91</span>
                                        </div><!-- floating-field --> 

                                        <div class="form-group floating-field">
                                            <select name="state_id" class="form-control" id="state_id" onclick="this.setAttribute('value', this.value);">
                                                <option value="">Select State</option>
                                                @foreach($states as $state)
                                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                            <label for="state_id" class="floating-label">Select State</label>
                                        </div><!-- floating-field -->
                                        <div class="form-group floating-field">
                                            <select name="city_id" class="form-control" id="city_id" onclick="this.setAttribute('value', this.value);">
                                                <option value="">Select City</option>
                                            </select>
                                            <label for="city_id" class="floating-label">Select City</label>
                                        </div><!-- floating-field -->
                                       
                                        <div class="formBtn">
                                            <button type="submit" id="btn_broker" class="btn btn-primary">Submit</button>
                                        </div>

                                    </div><!--//popupForm-->
                                </form>
                               
                            </div><!--//tab-content--> 

                        </div><!--//loginPageBox-->
                    </div><!--//col-md-8-->
                </div><!--//row-->
            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->
<!-- Expert delete confirm model window -->

<div class="modal alert-model" id="Delete" data-keyboard="true"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close-bt" data-dismiss="modal">&times;</button>
            <!-- Modal body -->
            <div class="modal-body alert-warning">
                <h4 class="text-warning">Delete Broker</h4>
                <p class="text-warning">Are you sure you want to delete this Broker?</p>
                <div class="col-lg-12 text-center">

                    <form name="broker_deletion" id="broker_deletion" method="post" action="{{ route('admin.delete-broker') }}">
                        @csrf
                        <button type="button" class="no-bt" data-dismiss="modal">No</button>
                        <input type="hidden" name="deletion_id" id="deletion_id">
                        <button type="submit" class="yes-bt">Yes</button>
                    </form>
                </div>
            </div>
            <!--//Modal body-->

        </div>
    </div>
</div>
@endsection
@section('custom-script')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script type="text/javascript">
    formReset = function() {
        window.location.href = "<?php url('admin/brokers'); ?>";
    }
    $(document).ready(function() {
        $('#state_id').on('change', function() {
    var stateId = $(this).val();
    $.ajax({
    type: 'POST',
            url: "{{ url('admin/get-city-list') }}",
            data: {
            stateId: stateId,
            },
            success: function (data) {
            if (data.cities){
            $('#city_id').html('');
            $('#city_id').append('<option value="">Select City</option>');
            $.each(data.cities, function (key, value) {
            $('#city_id').append('<option value="' + value.id + '">' + value.name + '</option>');
            });
            }
            },
    });
    });
    });
</script>
<script>
    function ActiveUser(id) {
        var id = id;
        var user_status = 0;
        $.ajax({
            url: "{{route('admin.broker-status')}}",
            type: "get",
            data: {
                id: id,
                user_status: user_status
            },
            success: function(data) {
                $('#btn_expert').prop("disabled", false);
                $('#btn_expert').text('Submit');
                if (data.success) {
                    swal("Broker Status Updated successfully.")
                        .then((value) => {
                            location.reload();
                        });
                } else {
                    $('.expert-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
                }
            }
        });
    }

    function DeavtiveUser(id) {
        var id = id;
        var user_status = 1;
        $.ajax({
            url: "{{route('admin.broker-status')}}",
            type: "get",
            data: {
                id: id,
                user_status: user_status
            },
            success: function(data) {
                $('#btn_expert').prop("disabled", false);
                $('#btn_expert').text('Submit');
                if (data.success) {
                    swal("Broker Status Updated successfully.")
                        .then((value) => {
                            location.reload();
                        });
                } else {
                    $('.expert-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
                }
            }
        });
    }
</script>
    <script type="text/javascript">
 function editBroker(data){
     $(document).find("span.error-span").remove();     
 $('.alert-block').hide();
    $('#brokerid').val(data['id']); 
    $('#editname').val(data['name']);
    $('#editemail').val(data['email']);
    $('#editmobile_number').val(data['mobile']);
    $('#state_id').val(data['state_id']);
    $('.editBroker').val('edit');
    <?php foreach($cities as $city){  ?>
            if(data['state_id'] == <?php echo $city->state_id ?>){
                var cityId = "<?php echo $city->id; ?>";
                var cityName = "<?php echo $city->name; ?>";
                $('#city_id').append('<option value="'+cityId+'">'+cityName+'</option>');
            }
    <?php } ?>
    $('#city_id').val(data['city_id']);
    };

    
    function deleteBroker(id){
       
    $('#deletion_id').val(id);
    };
$('.brokerPaginate').on('click', function (e) {
    e.preventDefault();
    var value = $(this).data('value');
    $('#paginateHiddenFiled').val(value);
    $('#paginateSubmitForm').submit();
    return false;
});
$('#add_broker').on('click', function (e) {
    e.preventDefault();
    $('#frm_broker_signup')[0].reset();
    $(document).find("span.error-span").remove();
});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$('#frm_broker_signup').submit(function (e) {
    e.preventDefault();
    $(document).find("span.error-span").remove();
    $('.alert-block').hide();
    var formData = new FormData(this);
    $('#btn_broker').prop("disabled", true);
    $('#btn_broker').text('Sending...');
    $('.loader').toggleClass('d-none');
    $.ajax({
        type: 'POST',
        url: "{{ route('admin.storebroker') }}",
        data: $("#frm_broker_signup").serialize(),
        dataType: "json",
        success: function (data) {
            $('.loader').toggleClass('d-none');
            $('#btn_broker').prop("disabled", false);
            $('#btn_broker').text('Submit');
            if (data.success) {
                $('#frm_broker_signup')[0].reset();
                swal(data.msg,{icon: "success",})
                        .then((value) => {
                            location.reload();
                        });
            } else {
                $('.broker-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
            }
        },
        error: function (errorResponse) {
            $('.loader').toggleClass('d-none');
            $('#btn_broker').prop("disabled", false);
            $('#btn_broker').text('Submit');
             $.each(errorResponse.responseJSON.errors, function  (field_name,error) {
                   
                        $(document).find('[name=' + field_name + ']').after('<span class="text-strong text-danger error-span"  role="alert">' + error + '</span>');
                })
        }
    });
});

</script>
@endsection
