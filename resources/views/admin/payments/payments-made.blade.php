@extends('admin.layouts.master')
@section('title')
Payments
@endsection
@section('class')
paymentsBody
@endsection
@section('content')

<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css">
<div> 

    <div class="pageTitle d-flex align-items-center">
        <h1>Payments</h1>

        <div class="sortDropdown">
            <label>Show</label>
            <div class="dropdown custom-dropdown">
                <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle min-w-60">
                    <span class="selected">{{ $paginate ?? '' }}</span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="javascript:;"  class="paymentPaginate" data-value="10">10</a></li>
                    <li><a href="javascript:;"  class="paymentPaginate" data-value="25">25</a></li>
                    <li><a href="javascript:;"  class="paymentPaginate" data-value="50">50</a></li>
                    <li><a href="javascript:;"  class="paymentPaginate" data-value="100">100</a></li>
                </ul>
            </div><!-- //dropdown --> 
        </div><!--//sortDropdown-->
    </div><!--//pageTitle-->

    <!-- //Search Form Div Start -->
    <div class="searchForm searchForm-sm">
        <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                   
        <form action="{{ url('admin/payment-made') }}" method="post">
            @csrf
            <span>Search</span>
            <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
            <ul class="d-flex flex-wrap">
                <li>
                    <div class="form-group">
                        <input class="form-control" id="datePickerSearch" type="text" placeholder="Date" name="date" value="{{ !empty($date) ? Carbon\Carbon::parse($date)->format('d-m-Y') : '' }}"/>
                    </div><!-- //form-group -->
                </li>
                <li>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Customer Name" name="customerName" id="customerName" value="{{ $customerName }}">
                    </div><!-- //form-group -->
                </li>
                <li>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Payment Type" name="paymentType" id="paymentType" value="{{ $paymentType }}">
                    </div><!-- //form-group -->
                </li>

                <li>
                    <button type="submit" class="btn btn-primary">Search</button>
                </li>
            </ul><!-- //ul -->
        </form><!-- //form -->
    </div><!-- //search-form -->
    <!-- //Search Form Div End -->

    <div class="reportNavBar">                        
        <div class="genReportLink">
            <a href="javascript:void(0);" class="userlisting" data-toggle="modal" data-target="#addLandListing"> Add Payments</a>
        </div> 

        <div class="reportTabs">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('admin/payments') }}">Payment Received</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active " href="{{ url('admin/payment-made') }}">Payment Mades</a>
                </li> 
            </ul>
        </div>
    </div><!--//reportNavBar-->

    <div class="payment-table-desktop border">
        <div class="horizontalScrollTable customScrollbar">  
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>User</th>
                        <th>Transaction Id</th>
                        <th>Amount</th>
                        <th>Currency</th>
                        <th>Payment Method</th>
                        <th>Status</th>
                        <th>Bank</th>
                        <th>File</th>
                        <th>created At</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($payments as $payment)
                    <tr>

                        @php
                        $name = '';
                        if(isset($payment->user->name) && !empty($payment->user->name)){
                        $name = $payment->user->name;
                        }
                        @endphp

                        <td scope="row">{{ $name }}</td>
                        <td scope="row">{{ $payment->transaction_id }}</td>
                        <td scope="row">{{ $payment->amount }}</td>
                        <td scope="row">{{ $payment->currency }}</td>
                        <td scope="row">{{ $payment->method }}</td>
                        <td scope="row">{{ $payment->status }}</td>
                        <td scope="row">{{ $payment->bank }}</td>
                        @php
                        $path = '';
                        if(Storage::disk('payments')->exists(str_replace('payments', '', $payment->file)))
                        {
                        $path = Storage::disk('payments')->url(str_replace('payments/', '', $payment->file));
                        }
                        @endphp
                        <td scope="row">
                            <a href="{{ $path }}">View</a>
                        </td>
                        <td scope="row">{{ Carbon\Carbon::parse($payment->created_at)->format('d M, Y') }}</td>
                    </tr>
                    @empty
                    <tr>
                        <td scope="row" colspan="7">No Records Available</td>

                    </tr>
                    @endif
                </tbody>
            </table>
        </div>

        <div>
            {{ $payments->links() }}
        </div><!-- //col-12 pagination -->
    </div><!--//payment-table-desktop-->

    <div class="payment-table-Mobile border">
        @forelse($payments as $payment)
        <div class="d-flex flex-wrap justify-content-between payment-list">
            <div><strong>{{ $payment->transaction_id }}</strong></div>
            <div></div>
            <div class="w-100"><strong>{{ $payment->order_id }}</strong></div>
            <div class="w-100"><strong>{{ $payment->amount .' '. $payment->currency }}</strong></div>
            <div class="w-100">{{ $payment->Entity }}</div>
            <div>{{ !empty($payment->refund_Date) ? Carbon\Carbon::parse($payment->refund_Date)->format('D, d M Y h:i A') : Carbon\Carbon::parse($payment->created)->format('D, d M Y h:i A') }}</div>
            <div>{{ $payment->method }}</div>
        </div><!-- //d-flex flex-wrap-->
        @empty
        <div class="d-flex flex-wrap justify-content-between payment-list">
            <p> No Records Available </p>
        </div>
        @endforelse

        <div class="col-12">
            {{ $payments->links() }}
        </div><!-- //col-12 pagination -->
    </div><!--//payment-table-Mobile-->

</div>  
<form method="post" id="paginateSubmitForm">
    @csrf
    <input type="hidden" name="day" id="paginateHiddenFiled">
</form>
@endsection
@section('modal')
<!-- Edit Profile model -->
<div class="modal fade w-600" id="addLandListing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered addLandListing" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="popupForm">
                    <form id="propertyStoreForm" enctype="multipart/form-data">
                        <div class="popupHeading">
                            <h2>Add Payment</h2>
                        </div>

                        <div class="addLand">
                            <div>
                                <div class="form-group">
                                    <label for="Name">User<span class="text-danger">*</span></label>
                                    <input type="hidden" name="user_id" id="myHidden" value="Default">
                                    <input type="text" name="myInput" id="myInput" class="form-control" placeholder="" value="">
                                </div>
                            </div>
                            <div>
                                <div class="form-group land_price">
                                    <label for="number"> Amount <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="amount" placeholder="Enter Amount" id="amount">
                                    <select class="form-control" id="reportType" name="currency">
                                        <option value="INR">INR</option>
                                    </select>
                                </div>
                            </div><!--//div-->
                            <div>
                                <div class="form-group">
                                    <label for="transaction_id">Transaction Id <span class="text-danger">*</span></label>
                                    <input class="form-control" name="transaction_id" id="transaction_id" type="text" placeholder=" ">
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label for="method">Payment Method <span class="text-danger">*</span></label>
                                    <input class="form-control" name="method" id="method" type="text" placeholder=" ">
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label for="status">Status<span class="text-danger">*</span></label>
                                    <input class="form-control" name="status" id="status" type="text" placeholder=" ">
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label for="bank"> Bank <span class="text-danger">*</span></label>
                                    <input class="form-control" name="bank" id="bank" type="text" placeholder=" ">
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label for="attachment">Attachment<span class="text-danger">*</span></label>
                                    <input class="form-control" name="file" id="attachment" type="file">
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label for="description">Assignment</label>
                                    <textarea class="form-control" name="description" id="description"></textarea>
                                </div>
                            </div>
                        </div><!--//addLand-->

                        <div class="formBtn">
                            <button type="button" class="btn btn-primary propertyStoreButton">Add Payment</button>
                        </div> 
                    </form>
                </div><!--//popupForm--> 
            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->
@endsection
@section('custom-script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
var myDataArray = [
<?php foreach ($users as $user) { ?>
        {label: "<?php echo $user->name; ?>", value: "<?php echo $user->id; ?>"},
<?php } ?>
];
$.widget('ui.myCustomWidget', $.ui.autocomplete, {
    renderItem: function (ul, item) {
        return $('<li>')
                .append($('<a>').html(decodeURI(item.label)))
                .appendTo(ul);
    }
});

$('#myInput').myCustomWidget({
    source: myDataArray,
    minLength: 1,
    delay: 0,
    create: function () {
        $(this).myCustomWidget('widget')
                .addClass('myClass')
                .css({
                    'max-height': 500,
                    'z-index': 9999,
                    'top': '155px',
                    'left': '780px',
                    'overflow-y': 'scroll',
                    'overflow-x': 'hidden'
                });
    },
    select: function (event, ui) {
        $('#myInput').val(ui.item.label);
        $('#myHidden').val(ui.item.value);
        return false;
    }
});
</script>

<script>
    //payment store with ajax
    $(document).ready(function () {
        $('.userlisting').on('click', function () {
            $("#propertyStoreForm")[0].reset()
        });

        $('body').on('click', '.propertyStoreButton', function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formData = new FormData(document.getElementById("propertyStoreForm"));
            $.ajax({
                url: "user-payment-store",
                type: "post",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data)
                {
                    if (data.success) {
                        swal({
                            title: "Good job!",
                            text: data.msg,
                            icon: "success",
                        }).then((value) => {
                            location.reload();
                        });
                    } else {

                        swal({
                            title: "error!",
                            text: data.msg,
                            icon: "error",
                        })
                    }
                }, error: function (errorResponse) {
                    $('.error-span').remove();
                    $.each(errorResponse.responseJSON.errors, function (field_name, error) {
                        if (field_name == 'amount') {
                            $(document).find('[name=' + field_name + ']').parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else {
                            $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        }
                    })
                }
            });
        });
    });
</script>

<script>
    $(document).ready(function () {
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#datePickerSearch').datepicker({
            maxDate: today,
            format: 'dd-mm-yyyy'
        });

        $('.paymentPaginate').on('click', function () {
            $('#paginateHiddenFiled').val($(this).data('value'));
            $('#paginateSubmitForm').submit();
        });
    });
</script>
@endsection