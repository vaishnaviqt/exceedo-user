<!DOCTYPE html>
<html lang="en">
<head>
@section('title')
  Admin Login
@endsection
@include('admin.partials.head')
<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css?v=2.1') }}">
</head>
<body>  

<div class="adminLoginWrapper"> 
        <div class="leftloginBanner">
            <div class="adminLoginTitle">
                <h2>Making every <br>
                    <strong>square yard count</strong></h2>
            </div>
        </div>
        <div class="rightadminLoginSection">
            
            <!--//Login section-->
            <section class="adminLoginBox shown" id="admin_login"> 
                <div class="ad_logo">
                    <a href="{{ route('admin-login') }}"> 
                        <img src="{{ asset('images/logo-black.svg') }}" alt="logo">
                        Know your Land
                    </a>
                </div><!--//logo-->
        <form  class=""  method="POST"   action="{{ route('adminlogin') }}" >
             @csrf
            @if ($errors->any())
            <p class="text-center font-semibold text-danger my-3">
                @if ($errors->has('email'))
                    {{ $errors->first('email') }}
                @else
                    {{ $errors->first('password') }}
                @endif
                </p>
            @endif
                <div class="adminLoginForm">
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label>Username</label>
                        <input type="text" placeholder="Type your Username" class="form-control" type="email" id="email" name="email" value="{{ old('email') }}" required autofocus/>
                         @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">{{ $errors->first('email') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label>Password</label>
                        <input type="password" placeholder="Enter your Password" class="form-control" name="password" id="password" />
                        @error('password')
                         <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="row align-items-center">
                        <div class="col-7">
                            <a href="javascript:void(0);" class="font_12" onclick="gotoSection('admin_forgot');">Forgot Password ?</a>
                        </div>
                        <div class="col-5 text-right adminLoginBtn">
                            
                                <button class="btn btn-primary" type="submit">Login</button>
                            
                        </div>
                    </div>
                </div><!--//adminLoginForm--> 
</form>

            </section><!--//adminLoginBox-->

            
            <!--//forgot section-->
            <div class="backBtn_forgot">
                <a href="javascript:void(0);" class="ad_backLogin" onclick="gotoSection('admin_login');"></a>
                <span>Forgot Password</span>
            </div>
            <section class="adminLoginBox" id="admin_forgot"> 
                <div class="ad_logo">
                    <a href="index.html"> 
                        <img src="{{ asset('images/logo-black.svg') }}" alt="logo">
                        Know your Land
                    </a>
                </div><!--//logo-->

                <div class="adminLoginForm">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" placeholder="Enter your Email" class="form-control" />
                    </div> 

                    <div class="row align-items-center">
                        <div class="col-12 adminLoginBtn mb-3">
                            <button class="btn btn-primary" onclick="gotoSection('admin_reset');">Send a Link</button>
                        </div>

                        <div class="col-12 text-center">
                            <a href="javascript:void(0);" class="font_12">Resend Link</a>
                        </div>
                    </div>
                </div><!--//adminLoginForm--> 
            </section><!--//adminLoginBox-->

            
            <!--//Reset password section-->
            <section class="adminLoginBox" id="admin_reset"> 
                <div class="ad_logo">
                    <a href="index.html"> 
                        <img src="{{ asset('images/logo-black.svg') }}" alt="logo">
                        Know your Land
                    </a>
                </div><!--//logo-->

                <div class="adminLoginForm">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" placeholder="Enter your Password" class="form-control" />
                    </div>

                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" placeholder="Enter your Confirm Password" class="form-control" />
                    </div>

                    <div class="row align-items-center"> 
                        <div class="col-12 adminLoginBtn">
                            <button class="btn btn-primary">Save Changes</button>
                        </div>
                    </div>
                </div><!--//adminLoginForm--> 
            </section><!--//adminLoginBox-->

        </div><!--//rightadminLoginSection-->
    </div><!--//adminLoginWrapper-->   
 
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>    

    <script type="text/javascript"> 
        function gotoSection(section){ 
            $('.adminLoginBox').removeClass('shown');
            $('#'+section).addClass('shown');
            
            if(section=='admin_forgot'){
                $('.backBtn_forgot').addClass('shown');
            }else{
                $('.backBtn_forgot').removeClass('shown');
            }
        }
    </script>
 
</body>
</html>