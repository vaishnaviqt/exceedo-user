@extends('admin.layouts.master')
@section('title')
Send Messages
@endsection
@section('class')
message-admin
@endsection
@section('content') 
@include('flash-message')
<div class="pageTitle d-flex align-items-center">
    <h1>Send Messages</h1> 
</div><!--//pageTitle-->

<form action="{{ url('admin/messages-send') }}" method="post" id="messageSendForm" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <input id="userMailTo" type="text" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" placeholder="To" required>
        @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <input class="form-control"  name="emailSubject" type="text" value="" placeholder="Subject"/>
        @error('subject')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <input class="form-control"  name="file" type="file" value=""/>
        @error('file')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div>
        <div class="form-group">
            <div class="selectRadio w-100">
                <label></label>
                <div class="d-flex flex-row business_type_error">
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input" id="allExperts" name="allExperts" value="1">
                        <label class="custom-control-label" for="allExperts">All Experts</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input" id="allBrokers" name="allBrokers" value="1">
                        <label class="custom-control-label" for="allBrokers">All Brokers</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input" id="allUsers" name="allUsers" value="1">
                        <label class="custom-control-label" for="allUsers">All Users</label>
                    </div>
                </div>
            </div>
        </div>
    </div><!--//div-->

    <div class="form-group">
        <textarea class="form-control @error('message') is-invalid @enderror" placeholder="Type your Message here" id="message" name="message" rows="8" required>{{ old('message') }}</textarea>
        <button type="button" class="btn btn-primary adminMessageSend">Send</button>
        @error('message')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div><!-- //form-group-->
</form>

<div class="pageTitle d-flex align-items-center">
    <h2>All Queries</h2> 
</div><!--//pageTitle-->

<!-- //Search Form Div Start -->
<div class="searchForm">
    <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                  
    <form action="{{ url('admin/messages') }}" method="post">
        @csrf
        <span>Search</span>
        <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
        <ul class="d-flex flex-wrap">
            <li>
                <div class="form-group">
                    <input class="form-control" id="filterDatepicker" name="date" type="text" @if(!empty($searchDate)) value="{{Carbon\Carbon::parse($searchDate)->format('d-m-Y')}}" @endif placeholder="Date of Joining" />
                </div><!-- //form-group -->
            </li>
            <li>
                <div class="form-group">
                    <select class="form-control" id="services" name="expert_id">
                        <option value="">Expert Name</option>
                        @foreach($users as $user)
                        <option value="{{ $user->id }}" @if($user->id == $expertId) selected @endif>{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div><!-- //form-group --> 
            </li>
            <li>
                <div class="form-group">
                    <input class="form-control" name="subject" type="text" placeholder="Subject" @if(!empty($subject)) value="{{ $subject}}" @endif />
                </div><!-- //form-group -->
            </li>
            <li>
                <button class="btn btn-primary">Search</button>
            </li>
            <li>
                <button class="btn btn-primary" onclick="formReset()">Reset</button>
            </li>
        </ul><!-- //ul -->
    </form><!-- //form -->
</div><!-- //search-form -->
<!-- //Search Form Div End -->

<div class="rating-reviews">
    @forelse($messages as $message)
    <div class="message-box d-md-flex">
        <div>
            <img src="{{ asset('adminassets/images/svg-icons/message-icon.svg') }}" alt="">
        </div> 
        <div style="width: 70%">
            <span>Send to : </span>
            <h6>{{ isset($message->user->name) ? $message->user->name : ''}}</h6>
            <p><span>Message : </span> {{ $message->message }}</p>
            @if(!empty($message->subject))
            <p><span>Subject : </span> {{ $message->subject }}</p>
            @endif
            @php
            $path="";
            if(Storage::disk('message')->exists(str_replace('message', '', $message->file)))
        {
        $path = Storage::disk('message')->url(str_replace('message/', '', $message->file));
        }
        @endphp
        @if(!empty($path))
        <p><span>File : </span> <a href="{{ $path }}" target="_blank">Download</a></p>
            @endif
        </div>
        <div>
            <span class="day-count">{{ Carbon\Carbon::parse($message->created_at)->diffForHumans() }}</span>
        </div><!-- //div-->
    </div>
    <!--//message-box  d-md-flex-->
    @empty
    <div class="message-box d-md-flex">
        <div>
            <p>No Message Available</p>
        </div><!-- //div-->
    </div>
    @endforelse
    {{ $messages->links() }}
</div><!-- //rating-reviews -->        
<script>
    $(document).ready(function () {
        $('.adminMessageSend').on('click', function () {
            var userEmail = $('#userMailTo').val();
            var message = $('#message').val();
            var allExpert = $('#allExperts:checkbox:checked').length > 0 ? false : true;
            var allBrokers = $('#allBrokers:checkbox:checked').length > 0 ? false : true;
            var allUsers = $('#allUsers:checkbox:checked').length > 0 ? false : true;
            if (userEmail == '' && allExpert && allBrokers && allUsers) {
                swal("Please Select One contect");
                return false;
            } else if (message == '') {
                $('#message').after('<span class="text-danger">Message filed is required</span>');
            } else {
                $('#messageSendForm').submit();
            }
        });
    });
</script>
                <script>
    formReset = function() {
        window.location.href = "<?php url('admin/messages'); ?>";
    }
    $(document).ready(function() {
        // datepicker
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#filterDatepicker').datepicker({
            format: 'dd-mm-yyyy'
        });
    })
</script>
@endsection
@section('modal')
@endsection
@section('custom-script')
@endsection