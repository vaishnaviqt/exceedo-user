@extends('admin.layouts.master')
@section('title')
Experts
@endsection
@section('content')
<style>

    .loader{
        position: fixed;
        top:0;
        left:0;
        right: 0;
        bottom:0;
        display:flex;
        align-items:center;
        justify-content:center;
        z-index: 9999;
        background: #ffffffa6;
    }
    .loader img{
        width:80px;
    }

    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked+.slider {
        background-color: #2196F3;
    }

    input:focus+.slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<div class="loader d-none">
    <img src="{{ asset('images/Spin-1s-200px.gif') }}">
</div>

<div class="pageTitle d-flex align-items-center">
    <h2>Experts</h2>
    <div class="sortDropdown">
        <label>Show</label>
        <div class="dropdown custom-dropdown">
            <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle min-w-60">
                <span class="selected">{{ $paginate ?? 10 }}</span><span class="caret"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="#" class="expertPaginate" data-value="10">10</a></li>
                <li><a href="#"  class="expertPaginate" data-value="25">25</a></li>
                <li><a href="#"  class="expertPaginate" data-value="50">50</a></li>
                <li><a href="#"  class="expertPaginate" data-value="100">100</a></li>
            </ul>
        </div><!-- //dropdown -->
        <form method="post" id="paginateSubmitForm">
            @csrf
            <input type="hidden" name="paginate" id="paginateHiddenFiled">
        </form>
        <!--//paginate form-->
    </div>
    <!--//sortDropdown-->
</div>
<!--//pageTitle-->

<div class="countCards">
    <div class="row">
        <div class="col-lg-4 col-md-6">
            <a class="nav-link active" href="{{url('admin/experts')}}">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/experts-icon.svg') }}" alt="all reports" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $allExperts->count() }}</strong></p>
                        <p>All Experts</p>
                    </div>
                </article>
            </a>
            <!--//countCardArt-->
        </div>
        <!--//col-lg-4-->

        <div class="col-lg-4 col-md-6">
            <a class="nav-link" href="{{url('admin/pendingexperts')}}">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/experts-icon.svg') }}" alt="all reports" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $penidngexperts->count() }}</strong></p>
                        <p>Pending Experts</p>
                    </div>
                </article>
                <!--//countCardArt-->
            </a>
        </div>
        <!--//col-lg-4-->

        <div class="col-lg-4 col-md-6">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('adminassets/images/svg-icons/value-services-icon.svg') }}" class="w-100" alt="Delivered Reports" />                                    
                </div>
                <div class="countRepDetail">
                    <p><strong>{{ $penidngexperts->count() }}</strong></p>
                    <p>All Value Added Services</p>
                </div>
            </article>
            <!--//countCardArt-->
        </div>
        <!--//col-lg-4-->
    </div>
    <!--//row-->
</div>
<!--//countCards-->
@include('flash-message')
<div class="reportNavBar">                        
    <div class="genReportLink">
        <a href="javascript:void(0);" data-toggle="modal" data-target="#joinBroker_expert">Add Expert</a>
    </div> 

    <div class="reportTabs" style="display:none;">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#allExperts">All Experts <span>({{ $allExperts->count() }})</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('admin/pendingexperts')}}">Pending Experts <span>({{ $penidngexperts->count() }})</span></a>
            </li>
        </ul>
    </div>
</div>
<!--//reportNavBar-->


<div class="searchForm">
    <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                   
    <form  method="post" action="" id="frm_experts_search">
        @csrf
        <span>Search</span>
        <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
        <ul class="d-flex flex-wrap">
            <li>
                <div class="form-group">
                    <input type="text" name="expert_name" class="form-control" placeholder="Name"  value="{{$expert_name ?? ''}}" id="Name">
                </div><!-- //form-group -->
            </li>
            <li>
                <div class="form-group">
                    <input class="form-control" name="email" type="text" value="{{$expert_email ?? ''}}" placeholder="Email" />
                </div><!-- //form-group -->
            </li>
            <li>
                <div class="form-group">
                    <input class="form-control" name="mobile" type="text" value="{{$expert_mobile ?? ''}}" placeholder="Mobile" />
                </div><!-- //form-group -->
            </li>
            <li>
                <select class="form-control" id="adminfeedback" name="ratings">
                    <option>Select Ratings</option>

                    @for($i =1; $i <= 10; $i++) <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
            </li>

            <li>
                <div class="form-group">
                    <input class="form-control" name="doj" id="datepicker_pending_expert" type="text" value="{{$date_of_joining ?? ''}}" placeholder="Date of Joining" />
                </div><!-- //form-group -->
            </li>
            <li>
                <div class="form-group">
                    <select name="service_id" class="form-control" id="service_id">
                        <option value="">Please select</option>
                        @foreach($allServices as $valueService)
                        @php
                        $selected = '';
                        if($service_id == $valueService->id){
                        $selected = 'selected';
                        }
                        @endphp
                        <option value="{{ $valueService->id }}" {{$selected}}>{{ $valueService->service_name }}</option>
                        @endforeach
                    </select>
                </div><!-- //form-group --> 
            </li>
            <li>
                <div class="form-group">
                    <select name="stateid" class="form-control" id="expert_stateid">
                        <option value="">select state</option>
                        @foreach($states as $state)
                        @php
                        $selected = '';
                        if($expert_stateid == $state->id){
                        $selected = 'selected';
                        }
                        @endphp
                        <option value="{{ $state->id }}" {{$selected}}>{{ $state->name }}</option>
                        @endforeach
                    </select>
                </div><!-- //form-group -->
            </li>
            <li>
                <div class="row row_10">
                    <div class="col-6">
                        <button type="submit" class="btn btn-primary" name="">Search</button>
                    </div>
                    <div class="col-6">
                        <button class="btn btn-primary" data-toggle="tab" onclick="formReset()">Reset</button>
                    </div>
                </div>
            </li>
        </ul><!-- //ul -->
    </form><!-- //form -->
</div><!-- //search-form -->
<!-- //Search Form Div End -->

<div class="customTable border">
    <div class="horizontalScrollTable customScrollbar"> 
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Expert Name</th>
                    <th>State</th>
                    <th>Rating and Reports</th>
                    <th>Expert Services</th>
                    <th>Mobile</th>
                    <th>Email id</th>
                    <th>User ID</th>
                    <th>Date Of Joining</th>
                    <th width="15%"></th>
                </tr>
            </thead>
            <tbody>

                @if (count($experts) > 0)
                @foreach($experts as $expert)
                <tr>
                    <td scope="row">
                        <span class="m_tableTitle">Expert Name</span>
                        {{ $expert->name }}
                    </td>
                    <td>
                        <span class="m_tableTitle">State</span>
                        {{ $expert->stateName }}
                    </td>
                    <td>
                        <span class="m_tableTitle">Rating and Reports</span>
                        {{ showRatingReport($expert->id) }}
                    </td>
                    <td>
                        <span class="m_tableTitle">Expert Services</span>
                        {{ (isset($expert->services) && is_array($expert->services->toArray())) ? implode(', ', array_column($expert->services->toArray(), 'service_name')) : '' }}
                    </td>
                    <td>
                        <span class="m_tableTitle">Mobile</span>
                        {{ $expert->mobile }}
                    </td>
                    <td>
                        <span class="m_tableTitle">Email id</span>
                        {{ $expert->email }}
                    </td>
                    <td>
                        <span class="m_tableTitle">User ID</span>
                        {{ $expert->user_unique_id }}

                    </td>
                    <td>
                        <span class="m_tableTitle">Date Of Joining</span>
                        {{ Carbon\Carbon::parse($expert->created_at)->format('d M, y H: A') }}
                    </td>
                    <td>
                        <a href="javascript:void(0);" class="deleteRow" data-toggle="modal" data-target="#joinBroker_expert" onclick="editExpert({{$expert}})" id="brokeredit">
                            Edit
                        </a>
                        <a data-toggle="modal" data-target="#Delete" href="javascript:void(0);" class="deleteRow" onclick="deleteExpert({{$expert->id}})" >
                            <img src="{{ asset('adminassets/images/svg-icons/delete-icon.svg') }}" alt="">
                        </a>
                        @if($expert->user_status==1)
                        <button class="btn btn-primary" onclick="ActiveUser({{$expert->id}})">Active</button>
                        @else
                        <button class="btn btn-danger" onclick="DeavtiveUser({{$expert->id}})">Deactive</button>
                        @endif
                    </td>
                </tr>
                @endforeach
                @else

                <tr>
                    <td scope="row" colspan="4">Experts not found</td>
                </tr>

                @endif
            </tbody>
        </table>
    </div>
    <div>
        {{ $experts->links() }}
    </div><!-- //col-12 pagination -->
</div>
<!--//payment-table-desktop-->



@endsection
@section('modal')
<!-- Modal -->     
<!-- Join Kyl as Broker & Expert -->
<div class="modal fade" id="joinBroker_expert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="row no-gutters">
                    <div class="col-md-12">
                        <div class="loginPageBox" id="signupBrokerBox">
                            <div class="" id="myTabContentSignup">
                                <div class="alert alert-success alert-block expert-msg" style="display: none;">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                </div>
                                <form name="frm_expert_signup" id="frm_expert_signup" method="post">
                                    @csrf
                                    <div class="popupForm">  
                                        <div class="form-group floating-field">
                                            <input type="hidden" class="form-control"  name="id" id="editid">
                                            <input type="text" class="form-control" placeholder="Full Name" name="full_name" id="editname">
                                            <label for="editname" class="floating-label">Full Name</label>
                                        </div><!-- floating-field -->

                                        <div class="form-group floating-field">
                                            <input type="text" class="form-control" placeholder="Email id" name="email" id="editemail">
                                            <label for="editemail" class="floating-label">Email id</label>
                                        </div><!-- floating-field -->

                                        <div class="form-group floating-field mobile-field">
                                            <input type="text" class="form-control" placeholder="Mobile Number" name="mobile_number" id="editmobile_number">
                                            <label for="editmobile_number" class="floating-label">Mobile Number</label>
                                            <span class="mobileCode">+91</span>
                                        </div><!-- floating-field --> 

                                        <div class="form-group floating-field">
                                            <label for="services_s2" class="mb-1">Services</label>
                                            <select id="expert_services" multiple="multiple" class="form-control" name="exp_services[]" >
                                                @foreach($allServices as $value)
                                                <option value="{{ $value->id }}">{{ $value->service_name }}</option>
                                                @endforeach
                                            </select>

                                        </div><!-- floating-field -->

                                        <div class="form-group floating-field">
                                            <select name="state_id" class="form-control" id="state_id" onclick="this.setAttribute('value', this.value);" value="">
                                                <option value="">Select State</option>
                                                @foreach($states as $state)
                                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                            <label for="state_id" class="floating-label">Select State</label>
                                        </div> 

                                        <div class="form-group floating-field">
                                            <select name="city_id" class="form-control" id="city_id" onclick="this.setAttribute('value', this.value);" value="">
                                                <option value="">Select City</option>
                                            </select>
                                            <label for="city_id" class="floating-label">Select City</label>
                                        </div>

                                        <div class="form-group">
                                        </div><!-- floating-field -->


                                        <div class="formBtn">
                                            <button type="submit" id="btn_expert" class="btn btn-primary">Submit</button>
                                        </div>

                                    </div>
                                    <!--//popupForm-->
                                </form>
                                >
                            </div>
                            <!--//tab-content-->

                        </div>
                        <!--//loginPageBox-->
                    </div>
                    <!--//col-md-8-->
                </div>
                <!--//row-->
            </div>
            <!--//modal-body-->
        </div>
    </div>
</div>
<!--//modal-->
<!-- Expert delete confirm model window -->

<div class="modal alert-model" id="Delete" data-keyboard="true"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close-bt" data-dismiss="modal">&times;</button>
            <!-- Modal body -->
            <div class="modal-body">
                <h4>Delete Expert</h4>
                <p>Are you sure you want to delete this Expert?</p>
                <div class="col-lg-12 text-center">

                    <form name="expert_deletion" id="expert_deletion" method="post" action="{{ route('admin.delete-pendingexpert') }}">
                        @csrf
                        <button type="button" class="no-bt" data-dismiss="modal">No</button>
                        <input type="hidden" name="deletion_id" id="deletion_id">
                        <button type="submit" class="yes-bt">Yes</button>
                    </form>
                </div>
            </div>
            <!--//Modal body-->

        </div>
    </div>
</div>
@endsection
@section('custom-script')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script type="text/javascript">
                                                formReset = function() {
                                                window.location.href = "<?php url('admin/experts'); ?>";
                                                }
                                                //$('input.cb-value').prop("checked", true);
                                                $(document).ready(function() {
                                                $('[name="user_status"]').prop('checked', true);
                                                $('[name="user_status"]').change(function(id) {
                                                if ($('[name="user_status"]:checked').is(":checked")) {
                                                $('[name="user_status"]').val(1);
                                                console.log($('[name="user_status"]').val(1))
                                                } else {
                                                $('[name="user_status"]').val();
                                                console.log($('[name="user_status"]').val(0))
                                                }
                                                });
                                                });
                                                deleteUser = function(id) {
                                                console.log(id);
                                                $('#expert_id').val(id);
                                                $.ajax({
                                                type: "GET",
                                                        url: 'user-status',
                                                        dataType: 'JSON',
                                                        success: function(data) {

                                                        },
                                                        error: function() {
                                                        alert("Fetching Record Failed");
                                                        }
                                                });
                                                }

                                                function ActiveUser(id) {
                                                var id = id;
                                                var user_status = 0;
                                                $.ajax({
                                                url: "{{route('admin.user-status')}}",
                                                        type: "get",
                                                        data: {
                                                        id: id,
                                                                user_status: user_status
                                                        },
                                                        success: function(data) {
                                                        $('#btn_expert').prop("disabled", false);
                                                        $('#btn_expert').text('Submit');
                                                        if (data.success) {
                                                        $('#frm_expert_signup')[0].reset();
                                                        swal("Expert Status Updated successfully.")
                                                                .then((value) => {
                                                                location.reload();
                                                                });
                                                        } else {
                                                        $('.expert-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
                                                        }
                                                        }
                                                });
                                                }

                                                function DeavtiveUser(id) {
                                                var id = id;
                                                var user_status = 1;
                                                $.ajax({
                                                url: "{{route('admin.user-status')}}",
                                                        type: "get",
                                                        data: {
                                                        id: id,
                                                                user_status: user_status
                                                        },
                                                        success: function(data) {
                                                        $('#btn_expert').prop("disabled", false);
                                                        $('#btn_expert').text('Submit');
                                                        if (data.success) {
                                                        $('#frm_expert_signup')[0].reset();
                                                        swal("Expert Status Updated successfully.")
                                                                .then((value) => {
                                                                location.reload();
                                                                });
                                                        } else {
                                                        $('.expert-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
                                                        }
                                                        }
                                                });
                                                }

                                                $(document).ready(function(){
                                                $('#state_id').on('change', function(){
                                                var stateId = $(this).val();
                                                $.ajax({
                                                type: 'POST',
                                                        url: "{{ url('admin/get-city-list') }}",
                                                        data: {
                                                        stateId: stateId,
                                                        },
                                                        success: function (data) {
                                                        if (data.cities){
                                                        $('#city_id').html('');
                                                        $('#city_id').append('<option value="">Select City</option>');
                                                        $.each(data.cities, function (key, value) {
                                                        $('#city_id').append('<option value="' + value.id + '">' + value.name + '</option>');
                                                        });
                                                        }
                                                        },
                                                });
                                                });
                                                });
// datepicker1
                                                var today, datepicker;
                                                today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                                                datepicker = $('#datepicker_pending_expert').datepicker({
                                                maxDate: today,
                                                        format: 'dd-mm-yyyy'
                                                });
                                                function deleteExpert(id){
                                                $('#deletion_id').val(id);
                                                }

                                                $('.expertPaginate').on('click', function (e) {
                                                e.preventDefault();
                                                var value = $(this).data('value');
                                                $('#paginateHiddenFiled').val(value);
                                                $('#paginateSubmitForm').submit();
                                                return false;
                                                });
                                                $(document).ready(function () {
                                                $('#expert_services').select2();
                                                $('#broker_services').select2();
                                                });
                                                $.ajaxSetup({
                                                headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                }
                                                });
                                                $('#frm_expert_signup').submit(function (e) {
                                                e.preventDefault();
                                                $(document).find("span.error-span").remove();
                                                $('.alert-block').hide();
                                                var formData = new FormData(this);
                                                $('#btn_expert').prop("disabled", true);
                                                $('#btn_expert').text('Sending...');
                                                $.ajax({
                                                type: 'POST',
                                                        url: "{{ route('storeexpert') }}",
                                                        data: $("#frm_expert_signup").serialize(),
                                                        dataType: "json",
                                                        success: function (data) {
                                                        $('#btn_expert').prop("disabled", false);
                                                        $('#btn_expert').text('Submit');
                                                        if (data.success) {
                                                        $('#frm_expert_signup')[0].reset();
                                                        swal("Expert added successfully.")
                                                                .then((value) => {
                                                                location.reload();
                                                                });
                                                        } else {
                                                        $('.expert-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
                                                        }
                                                        },
                                                        error: function (errorResponse) {
                                                        $('#btn_expert').prop("disabled", false);
                                                        $('#btn_expert').text('Submit');
                                                        $.each(errorResponse.responseJSON.errors, function (field_name, error) {
                                                        if (field_name == 'exp_services') {
                                                        $('#expert_services').next().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                                                        } else {
                                                        $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                                                        }
                                                        })
                                                        }
                                                });
                                                });
// broker signup form
                                                $('#frm_broker_signup').submit(function (e) {
                                                e.preventDefault();
                                                $(document).find("span.error-span").remove();
                                                $('.alert-block').hide();
                                                var formData = new FormData(this);
                                                $('#btn_broker').prop("disabled", true);
                                                $('#btn_broker').text('Sending...');
                                                $.ajax({
                                                type: 'POST',
                                                        url: "{{ route('storebroker') }}",
                                                        data: formData,
                                                        cache: false,
                                                        contentType: false,
                                                        processData: false,
                                                        success: function (data) {
                                                        $('#btn_broker').prop("disabled", false);
                                                        $('#btn_broker').text('Submit');
                                                        if (data.success) {
                                                        $('#frm_broker_signup')[0].reset();
                                                        swal("Broker added successfully.")
                                                                .then((value) => {
                                                                location.reload();
                                                                });
                                                        } else {
                                                        $('.expert-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
                                                        }
                                                        },
                                                        error: function (errorResponse) {
                                                        $('#btn_broker').prop("disabled", false);
                                                        $('#btn_broker').text('Submit');
                                                        $.each(errorResponse.responseJSON.errors, function (field_name, error) {
                                                        if (field_name == 'broker_services') {
                                                        $('#broker_services').next().after('<span class="text-strong error-span" role="alert">' + error + '</span>');
                                                        } else {
                                                        $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span" role="alert">' + error + '</span>');
                                                        }
                                                        })
                                                        }
                                                });
                                                });
</script>
<script>
 function editExpert(data) {
                                                var services = data.services;
                                                var serviceIds = services.map(function(row) {
                                                return row['id'];
                                                });
                                                $('#expert_services').html('');
                                                $('#expert_services').append('<option value="">Please select</option>');
                                                <?php foreach ($allServices as $services) { ?>
                                                    var selected = '';
                                                    if (jQuery.inArray(<?php echo $services->id ?>, serviceIds) !== - 1){
                                                    selected = 'selected';
                                                    }
                                                    $('#expert_services').append('<option value="<?php echo $services->id; ?>" ' + selected + '><?php echo $services->service_name; ?></option>');


                                                <?php } ?>
     $(document).find("span.error-span").remove();     
 $('.alert-block').hide();
    $('#editid').val(data['id']); 
    $('#editname').val(data['name']);
    $('#editemail').val(data['email']);
    $('#editmobile_number').val(data['mobile']);
    $('#state_id').val(data['state_id']);
<?php foreach ($cities as $city) { ?>
    console.log(<?php print_r($city); ?>);
                                                    if (data['city_id'] == <?php echo $city->id ?>){
                var cityId = "<?php echo $city->id; ?>";
                var cityName = "<?php echo $city->name; ?>";
                                                    $('#city_id').append('<option value="' + cityId + '">' + cityName + '</option>');
            }
    <?php } ?>
    $('#city_id').val(data['city_id']);
    };
</script>
@endsection