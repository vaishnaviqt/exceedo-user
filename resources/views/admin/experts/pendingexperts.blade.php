@extends('admin.layouts.master')
@section('title')
Experts
@endsection
@section('content')
<div>
    
    <div class="pageTitle d-flex align-items-center">
        <h2>Pending Experts</h2>
        
        <div class="sortDropdown">
            <label>Show</label>
            <div class="dropdown custom-dropdown">
                <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle min-w-60">
                    <span class="selected">{{ $paginate ?? 10 }}</span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" class="expertPaginate" data-value="10">10</a></li>
                    <li><a href="#"  class="expertPaginate" data-value="25">25</a></li>
                    <li><a href="#"  class="expertPaginate" data-value="50">50</a></li>
                    <li><a href="#"  class="expertPaginate" data-value="100">100</a></li>
                </ul>
            </div><!-- //dropdown -->
            <!--//paginate form-->
            <form method="post" id="paginateSubmitForm">
                @csrf
                <input type="hidden" name="paginate" id="paginateHiddenFiled">
            </form>
            <!--end Paginate Form-->
        </div>
        <!--//sortDropdown-->
    </div>
    <!--//pageTitle-->
  
    <div class="countCards">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <a class="nav-link" href="{{url('admin/experts')}}">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/experts-icon.svg') }}" alt="all reports" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $allExperts->count() }}</strong></p>
                        <p>All Experts</p>
                    </div>
                    </article>
                </a>
                <!--//countCardArt-->
            </div>
            <!--//col-lg-4-->

            <div class="col-lg-4 col-md-6">
                <a class="nav-link active" href="{{url('admin/pendingexperts')}}">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                            <img src="{{ asset('adminassets/images/svg-icons/experts-icon.svg') }}" alt="all reports" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $penidngexperts->count() }}</strong></p>
                        <p>Pending Experts</p>
                    </div>
                    </article>
                    <!--//countCardArt-->
                </a>
            </div>
            <!--//col-lg-4-->

            <div class="col-lg-4 col-md-6">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/value-services-icon.svg') }}" class="w-100" alt="Delivered Reports" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $penidngexperts->count() }}</strong></p>
                        <p>All Value Added Services</p>
                    </div>
                </article>
                <!--//countCardArt-->
            </div>
            <!--//col-lg-4-->
        </div>
        <!--//row-->
    </div>
    <!--//countCards-->

    <div class="reportNavBar flex-row" style="display:none;">
        <div class="reportTabs">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.experts')}}">All Experts <span>({{ $allExperts->count() }})</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  active" data-toggle="tab" href="#valueAddedServices">Pending Experts <span>({{ $penidngexperts->count() }})</span></a>
                </li>
            </ul>
        </div>
    </div>
    <!--//reportNavBar-->
       
    <div class="searchForm">
        <a href="javascript:void(0);" class="open-mobile-search">Search</a>
        <form  method="post" action="" id="frm_experts_search">
            @csrf
            <span>Search</span>
            <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
            <ul class="d-flex flex-wrap">
                <li>
                    <div class="form-group">
                        <input type="text" name="expert_name" class="form-control" placeholder="Name" id="Name" value="{{$expert_name ?? ''}}">
                    </div><!-- //form-group -->
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control" name="email" type="text" value="{{$expert_email ?? ''}}" placeholder="Email" />
                    </div><!-- //form-group -->
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control" name="mobile" type="text" value="{{$expert_mobile ?? ''}}" placeholder="Mobile" />
                    </div><!-- //form-group -->
                </li>
                <li>
                    <div class="form-group">
                        <input class="form-control" name="doj" id="datepicker_pending_expert" type="text" value="{{$date_of_joining ?? ''}}" placeholder="Date of Joining" />
                    </div><!-- //form-group -->
                </li>
                <li>

                    <div class="form-group">
                        <select name="service_id" class="form-control" id="service_id">
                            <option value="">Please select</option>
                            @foreach($all_valueServices as $valueService)
                            @php
                            $selected = '';
                            if($service_id == $valueService->id){
                            $selected = 'selected';
                            }
                            @endphp
                            <option value="{{ $valueService->id }}" {{$selected}}>{{ $valueService->service_name }}</option>
                            @endforeach

                        </select>
                    </div><!-- //form-group -->
                </li>
            </ul>
            <ul class="d-flex flex-wrap justify-content-end">
                <li>
                    <div class="form-group">
                        <div class="row row_10">
                            <div class="col-6">
                    <button type="submit" class="btn btn-primary" name="">Search</button>
                            </div>
                            <div class="col-6">
                    <button class="btn btn-primary" data-toggle="tab" onclick="formReset()">Reset</button>
                            </div>
                        </div>
                    </div>
                </li>
            </ul><!-- //ul -->
        </form><!-- //form -->
    </div><!-- //search-form -->
    <!-- //Search Form Div End -->
    @include('admin.layouts.flash-message')
    <div class="customTable border">
        <div class="horizontalScrollTable customScrollbar"> 
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Expert Name</th>
                    <th>State</th>
                    <th>Rating and Reports</th>
                    <th>Expert Services</th>
                    <th>Mobile</th>
                    <th>Email id</th>
                    <th>User ID</th>
                    <th>Date Of Joining</th>
                    <th width="15%"></th>
                </tr>
            </thead>
            <tbody>

                @if (count($experts) > 0)
                @foreach($experts as $expert)
                <tr>
                    <td scope="row">
                        <span class="m_tableTitle">Expert Name</span>
                        {{ $expert->name }}
                    </td>
                    <td>
                        <span class="m_tableTitle">State</span>
                        {{ $expert->stateName }}
                    </td>
                    <td>
                        <span class="m_tableTitle">Rating and Reports</span>
                        {{ showRatingReport($expert->id) }}
                    </td>
                    <td>
                        <span class="m_tableTitle">Expert Services</span>
                        {{ (isset($expert->services) && is_array($expert->services->toArray())) ? implode(', ', array_column($expert->services->toArray(), 'service_name')) : '' }}
                    </td>
                    <td>
                        <span class="m_tableTitle">Mobile</span>
                        {{ $expert->mobile }}
                    </td>
                    <td>
                        <span class="m_tableTitle">Email id</span>
                        {{ $expert->email }}
                    </td>
                    <td>                                    
                        <span class="m_tableTitle">User ID</span>
                        {{ $expert->user_unique_id }}
                    </td>
                    <td>                                    
                        <span class="m_tableTitle">Date Of Joining</span>
                        {{ Carbon\Carbon::parse($expert->created_at)->format('d M, y H: A') }}
                    </td>
                    <td class="text-right pr-2">
                        <!-- <a class="dropdown-item" data-effect="mfp-zoom-in" data-target="#editExpert" href="javascript:void();">Edit</a> -->
                        <a href="javascript:void(0);" data-effect="mfp-zoom-in" class="approve_expert" data-id="{{$expert->id}}">Approve</a>

                        <a data-toggle="modal" data-target="#Delete" href="javascript:void(0);" class="deleteRow" onclick="deleteExpert({{$expert->id}})" >
                            <img src="{{ asset('adminassets/images/svg-icons/delete-icon.svg') }}" alt="">
                        </a>
                    </td>
                </tr>
                @endforeach
                @else

                <tr>
                    <td scope="row" colspan="4">Experts not found</td>
                </tr>
                @endif
            </tbody>
        </table>
        </div>

        <div>
            {{ $experts->links() }}
        </div><!-- //col-12 pagination -->
    </div><!--//payment-table-desktop-->

</div><!-- //row -->     
@endsection
@section('modal')

<div id="editExpert"class="mfp-with-anim mfp-hide suggestAnotherTime">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12">
                <h2>Update Expert details</h2>
            </div><!--//.col-12 -->

            <div class="col-12">
                <div class="alert alert-success alert-block" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                </div>
                <form name="frm_update_expert" id="frm_update_expert" method="post">
                    @csrf
                    <input type="hidden" name="expert_id" id="expert_id">
                    <div class="form-filds">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <span class="has-float-label">
                                        <input class="form-control" name="expert_name" id="expert_name" type="text" placeholder=""/>
                                        <label for="datepicker">Expert Name</label>
                                    </span>
                                </div>
                            </div><!-- //col-12 -->

                            <div class="col-12">
                                <div class="form-group">
                                    <span class="has-float-label">
                                        <input class="form-control" name="email" id="email" type="text" placeholder=""/>
                                        <label for="timepicker">Email</label>
                                    </span>
                                </div>
                            </div><!-- //col-12 -->

                            <div class="col-12">
                                <div class="form-group mb-3">
                                    <span class="has-float-label">
                                        <input readonly="readonly" class="form-control" id="mobile_no" type="text" placeholder=" "/>
                                        <label for="remarks">Mobile</label>
                                    </span>
                                </div>
                            </div><!-- //col-12 -->

                            <div class="col-12">
                                <div class="form-group mb-3">
                                    <span class="has-float-label">
                                        <select name="is_pending" id="is_pending">
                                            <option value="0">Pending</option>
                                            <option value="1">Approved</option>
                                        </select>
                                        <label for="remarks">Status</label>
                                    </span>
                                </div>
                            </div><!-- //col-12 -->

                            <div class="col-12">
                                <button type="submit" class="btn btn-primary js-btn-next" id="btn_expert_update" type="button" title="Update">Update</button>

                            </div><!--//col-12-->
                        </div><!--//.row-->
                    </div><!--//form-filds-->
                </form><!--//form-->
            </div><!-- //.col-12-->
        </div><!--/row-->
    </div><!-- //popup-body -->
</div><!-- //Suggest Another Time Popup End -->



<!-- Expert delete confirm model window -->

<div class="modal alert-model" id="Delete" data-keyboard="true"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close-bt" data-dismiss="modal">&times;</button>
            <!-- Modal body -->
            <div class="modal-body">
                <h4>Delete Expert</h4>
                <p>Are you sure you want to delete this Expert?</p>
                <div class="col-lg-12 text-center">

                    <form name="expert_deletion" id="expert_deletion" method="post" action="{{ route('admin.delete-pendingexpert') }}">
                        @csrf
                        <button type="button" class="no-bt" data-dismiss="modal">No</button>
                        <input type="hidden" name="deletion_id" id="deletion_id">
                        <button type="submit" class="yes-bt">Yes</button>
                    </form>
                </div>
            </div>
            <!--//Modal body-->

        </div>
    </div>
</div>
@endsection

@section('custom-script')
@parent
<script>
    formReset = function() {
        window.location.href = "<?php url('admin/pendingexperts'); ?>";
    }
    $('.expertPaginate').on('click', function(e) {
    e.preventDefault();
    var value = $(this).data('value');
    $('#paginateHiddenFiled').val(value);
    $('#paginateSubmitForm').submit();
    return false;
    });
    // datepicker1
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#datepicker_pending_expert').datepicker({
    maxDate: today,
            format: 'dd-mm-yyyy'
    });

    function deleteExpert(id) {
    $('#deletion_id').val(id);
    }

    /*$('.expert_edit').magnificPopup({
     type:'inline',
     midClick: true
     });*/

    function getExpert(id){
    //alert(id);
    $("#editExpert form").trigger('reset');
    // $("#edit_client_id").val(id);
    //$("#edit_email").attr('data-validation-req-params','{ "user_id" : "'+id+'" }');
    $.ajax({
    type: "GET",
            url: 'get-expert/' + id,
            dataType: 'JSON',
            success: function(data) {
            /*alert('HII');
             console.log(data);*/
            if (data.success == false) {
            alert(data.msg);
            return false;
            }
            $("#expert_name").val(data.name);
            $("#email").val(data.email);
            $("#expert_id").val(data.id);
            $.magnificPopup.open({
            items: {
            src: '#editExpert',
                    callbacks: {
                    beforeOpen: function() {
                    this.st.mainClass = this.st.el.attr('data-effect');
                    }
                    },
                    type: 'inline'
            }

            }); // end of magnificPopup
            },
            error: function(){
            alert("Fetching Expert Record Failed");
            return false;
            }
    });
    return false;
    }

// Update ajax

    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $('#frm_update_expert').submit(function(e) {
    e.preventDefault();
    $(document).find("span.error-span").remove();
    $('.alert-block').hide();
    var formData = new FormData(this);
    $('#btn_expert_update').prop("disabled", true);
    $('#btn_expert_update').text('Updating...');
    $.ajax({
    type:'POST',
            url:"{{ route('admin.update-expert') }}",
            data: formData,
            processData: false,
            contentType: false,
            success:function(data){
            $('#btn_expert_update').prop("disabled", false);
            $('#btn_expert_update').text('Update');
            if (data.success){
            $('#frm_update_expert')[0].reset();
            $('.custom-file-label').html('');
            $('.alert-block').show().append('<strong>' + data.msg + '</strong>');
            } else{
            $('.alert-block').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
            }
            },
            error:function (errorResponse){
            $('#btn_expert_update').prop("disabled", false);
            $('#btn_expert_update').text('Update');
            $.each(errorResponse.responseJSON.errors, function(field_name, error){
            $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span" role="alert">' + error + '</span>')
            })
            }

    });
    });
    $('.approve_expert').click(function(e) {
    e.preventDefault();
    var expertId = $(this).attr('data-id');
    swal({
    title: "Are you sure?",
            text: "Approve this expert!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
    })
            .then((willApprove) => {
            if (willApprove) {
            $.ajax({
            type: 'GET',
                    url: "{{ url('admin/approve-expert') }}/" + expertId,
                    success: function(data) {
                    if (data.success) {
                    swal(data.msg, {
                    icon: "success",
                    })
                            .then((value) => {
                            location.reload();
                            });
                    } else {
                    swal(data.msg, {
                    icon: "error",
                    });
                    }
                    },
                    error: function (jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                    } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                    } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    swal(msg, {
                    icon: "error",
                    });
                    },
            });
            } else {
            swal("Approval cancelled!");
            }
            });
    });
</script>
@endsection