@extends('admin.layouts.master')
@section('title')
Experts
@endsection
@section('content')
<div>
    <div class="pageTitle d-flex align-items-center">
        <h2>Value Added Services</h2>

        <div class="sortDropdown">
            <label>Show</label>
            <div class="dropdown custom-dropdown">
                <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle min-w-60">
                    <span class="selected">10</span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#">10</a></li>
                    <li><a href="#">25</a></li>
                    <li><a href="#">50</a></li>
                    <li><a href="#">100</a></li>
                </ul>
            </div><!-- //dropdown --> 
        </div>
        <!--//sortDropdown-->
    </div>
    <!--//pageTitle-->

    <div class="row">
        <div class="col-lg-9">
            <!-- //Search Form Div Start -->
            <div class="searchForm">
                <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                   
                <form method="post" action="" id="frm_value_added_search">
                    @csrf
                    <span>Search</span>
                    <a href="javascript:void(0);" class="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
                    <ul class="d-flex flex-wrap">
                        <li>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Service Name" name="service_name" value="{{ $service_name ?? '' }}" id="serviceName">
                            </div><!-- //form-group -->
                        </li>
                        <li> 
                            <button type="submit" class="btn btn-primary" name="">Search</button>
                        </li> 
                    </ul><!-- //ul -->
                </form><!-- //form -->
            </div><!-- //search-form -->
            <!-- //Search Form Div End -->        
        </div>
        <div class="col-lg-3">
            <div class="reportNavBar pt-2 mb-2" id="services-inline-popups">                        
                <div class="genReportLink">
                    <a href="#javascript:void(0);" data-toggle="modal" data-target="#landDetail-popup">Add Services</a>
                </div>  
            </div>
            <!--//reportNavBar-->
        </div>
    </div>
    <!--//row-->

    <div class="row row_20">
        @if (count($valueServices) > 0)

        @foreach($valueServices as $valueService)
        <div class="col-md-6">
            <div class="card mb-4">
                <div class="card-body">
                    <img class="d-flex align-self-start mr-3" src="{{ url('/') }}/{{ $valueService->service_icon }}" alt="">
                    <div class="media-body">
                        <h5 class="mt-0">{{ $valueService->service_name }}</h5>
                        <p>Price: {{ $valueService->price }}</p>
                        <p>{{ $valueService->description }}</p>
                    </div>
                    <!--//media-body -->
                    <div class="media-body">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#valueeEdit-popup" id="brokeredit" onclick="getEditvalue({{$valueService->id}})">
                            Edit
                        </a>
                        <!-- <a href="javascript:void(0);" class="text-danger" onclick="getDeletevalue({{$valueService->id}})">
                            Delete
                        </a> -->
                         <a data-toggle="modal" data-target="#Delete" href="javascript:void(0);" class="    text-danger" onclick="deleteUser({{$valueService->id}})" >
                            Delete
                        </a>
                        <!-- <a href="{{url('admin/delete_valueservice').'/'.$valueService->id}}" class="text-danger">Delete</a> -->
                    </div>
                </div>
                <!--//card-body -->
            </div>
            <!--//card -->
        </div>
        <!--//col-md-6-->
        @endforeach
        @else   
        Value Added services not found..
        @endif

        {{ $valueServices->links() }}
    </div>
    <!--//row row_20-->



</div>    
@endsection

@section('modal')
<!-- value-added-services-popup -->
<div class="modal fade" id="landDetail-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="popupForm"> 
                    <div class="popupHeading">
                        <h2>Value added services</h2> 
                    </div> 

                    <form id="frm_value_added_service" method="POST" enctype="multipart/form-data" action="">
                        @csrf
                        <ul class="d-flex flex-wrap form-fields">

                            <li>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="service_name" placeholder="Service Name" id="service_name">
                                </div><!-- //form-group -->
                            </li>
                            <li>
                                <div class="form-group">
                                    <input type="number" class="form-control" name="price" placeholder="Service Price" id="price">
                                </div><!-- //form-group -->
                            </li>
                            <li>
                                <div class="form-group">
                                    <textarea class="form-control" rows="3" placeholder="Service Description" name="description" id="description"></textarea>
                                </div><!-- //form-group -->
                            </li>
                            <li>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="service_icon" id="service_icon">
                                    <label class="custom-file-label" for="service_icon">Service Icon</label>
                                </div>
                            </li>
                            <li>
                                <button type="submit" id="btn_add_service" class="btn btn-primary w-100 mt-4">Add Services</button>
                            </li>
                        </ul><!-- //ul -->
                    </form>
                </div><!-- //popup-body --> 

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="valueeEdit-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-0">
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="popupForm">
                    <div class="popupHeading">
                        <h2>Edit Value added services</h2>
                    </div>

                    <form id="frm_value_edit_service" method="POST" enctype="multipart/form-data" action="">
                        @csrf
                        <ul class="d-flex flex-wrap form-fields">

                            <li>
                                <div class="form-group">
                                    <input type="hidden" vlaue="" name="serviceid" id="serviceid">
                                    <input type="text" class="form-control" name="editservice_name" placeholder="Service Name" id="editservice_name">
                                    @if ($errors->has('service_name'))
                                    <span class="invalid-feedback" role="alert">{{ $errors->first('service_name') }}</span>
                                    @endif
                                </div><!-- //form-group -->
                            </li>
                            <li>
                                <div class="form-group">
                                    <input type="number" class="form-control" name="editprice" placeholder="Service Price" id="editprice">
                                </div><!-- //form-group -->
                            </li>
                            <li>
                                <div class="form-group">
                                    <textarea class="form-control" rows="3" placeholder="Service Description" name="editdescription" id="editdescription"></textarea>
                                </div><!-- //form-group -->
                            </li>
                            <li>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="editservice_icon" id="editservice_icon">
                                    <label class="custom-file-label" for="service_icon">Service Icon</label>
                                </div>
                            </li>
                            <li>
                                <button type="submit" id="btn_edit_service" class="btn btn-primary w-100 mt-4">Edit Services</button>
                            </li>
                        </ul><!-- //ul -->
                    </form>
                </div><!-- //popup-body -->

            </div>
        </div>
    </div>
</div>

<!--Delete model-->

<div class="modal alert-model" id="Delete" data-keyboard="true"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close-bt bg-info p-2 text-right  " data-dismiss="modal">&times;</button>
            <!-- Modal body -->
            <div class="modal-body alert-warning">
                <h4 class="text-warning">Delete Service</h4>
                <p class="text-warning">Are you sure you want to delete this Service?</p>
                <div class="col-lg-12 text-center">
                   
                        <button  class="no-bt btn btn-danger" data-dismiss="modal">No</button>
                        
                        <button type="submit"  class="yes-bt btn btn-primary  value_deletion"> <input type="hidden" name="deletion_id" id="deletion_id">Yes</button>
                
                </div>
            </div>
            <!--//Modal body-->

        </div>
    </div>
</div>


@endsection

@section('custom-script')
@parent
<script>
    // Inline popups
    $('#services-inline-popups').magnificPopup({
    delegate: 'a',
            removalDelay: 500,
            callbacks: {
            beforeOpen: function() {
            this.st.mainClass = this.st.el.attr('data-effect');
            },
                    close: function() {

                    $('#btn_add_service').prop("disabled", false);
                    $('#btn_add_service').text('Add Services');
                    $(document).find("span.error-span").remove();
                    location.reload();
                    }
            },
            midClick: true
    });
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $('#frm_value_added_service').submit(function(e) {
    e.preventDefault();
    $(document).find("span.error-span").remove();
    $('.alert-block').hide();
    var formData = new FormData(this);
    $('#btn_add_service').prop("disabled", true);
    $('#btn_add_service').text('Sending...');
    $.ajax({

    type: 'POST',
            url: 'add-valueservice',
            data: formData,
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
            $('#btn_add_service').prop("disabled", false);
            $('#btn_add_service').text('Add Services');
            if (data.success) {
            console.log(data);
            swal(data.msg)
                    .then((value) => {
                    location.reload();
                    });
            } else {
            $('.alert-block').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
            }
            },
            error: function(errorResponse) {
            console.log("error");
            $('#btn_add_service').prop("disabled", false);
            $('#btn_add_service').text('Add Services');
            $.each(errorResponse.responseJSON.errors, function(field_name, error) {
            $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>')
            })
            }

    });
    });
    //get edit value added services
    getEditvalue =
            function(id) {
            $("#serviceid").val(id);
            $.ajax({
            type: "GET",
                    url: 'edit-valueservice/' + id,
                    dataType: 'JSON',
                    success: function(data) {
                    console.log(data);
                    $("#editservice_name").val(data.service_name);
                    $("#editprice").val(data.price);
                    $("#editdescription").val(data.description);
                    $("#editservice_icon").val(data.service_icon);
                    },
                    error: function() {
                    alert("Fetching Job Record Failed");
                    }
            });
            }
             function deleteUser(id){
                            $('#deletion_id').val(id);
                            };
            $(".value_deletion").click(function(){
                var id=$("#deletion_id").val();
                $.ajax({
                     type: "GET",
                    url: 'delete_valueservice/' + id,
                    success: function(data) {
                  location.reload();
                    },
            });

            });
          

    //update value added services

    $('#frm_value_edit_service').submit(function(e) {
    e.preventDefault();
    $(document).find("span.error-span").remove();
    $('.alert-block').hide();
    var formData = new FormData(this);
    $('#btn_edit_service').prop("disabled", true);
    $('#btn_edit_service').text('Sending...');
    $.ajax({

    type: 'POST',
            url: 'update-valueservice',
            data: formData,
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
            $('#btn_edit_service').prop("disabled", false);
            $('#btn_edit_service').text('Edit Services');
            if (data.success) {
            swal(data.msg)
                    .then((value) => {
                    location.reload();
                    });
            } else {
            $('.alert-block').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
            }
            },
            error: function(errorResponse) {
            $('#btn_edit_service').prop("disabled", false);
            $('#btn_edit_service').text('Edit Services');
            $.each(errorResponse.responseJSON.errors, function(field_name, error) {
            console.log(field_name);
            $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>')
            })
            }

    });
    });
</script>
@endsection