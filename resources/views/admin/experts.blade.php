@extends('admin.layouts.master')
@section('title')
Experts
@endsection
@section('content')
<div class="row">
    <div class="col-6">
        <h2>Experts</h2>
    </div><!-- //col-6 pageTitle-->

    <div class="col-6 text-right showBy mb-1">
        <div class="dropdown">
            <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="selected">10</span><span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="#">10</a></li>
                <li><a href="#">25</a></li>
                <li><a href="#">50</a></li>
                <li><a href="#">100</a></li>
            </ul>
        </div><!-- //dropdown -->
        <span class="float-right pt-2">Show</span>
    </div><!-- //col-6 showBy -->

    <div class="col-12">
        <div class="countCards">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <article class="countCardArt">
                        <div class="coutRepIcon">
                            <img src="{{ asset('adminassets/images/svg-icons/experts-icon.svg') }}" alt="all reports" />
                        </div>
                        <div class="countRepDetail">
                            <p><strong>{{ $allExperts->count() }}</strong></p>
                            <p>All Experts</p>
                        </div>
                    </article><!--//countCardArt-->
                </div><!--//col-lg-4-->

                <div class="col-lg-4 col-md-6">
                    <article class="countCardArt">
                        <div class="coutRepIcon">
                            <img src="{{ asset('adminassets/images/svg-icons/value-services-icon.svg') }}" class="w-100" alt="Delivered Reports" />                                    
                        </div>
                        <div class="countRepDetail">
                            <p><strong>{{ $allServices->count() }}</strong></p>
                            <p>All Value Added Services</p>
                        </div>
                    </article><!--//countCardArt-->
                </div><!--//col-lg-4-->                       
            </div><!--//row-->
        </div><!--//countCards-->
    </div><!-- //col-12 -->


    <div class="col-12">
        <!-- //Nav tabs Start -->
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#allExperts">All Experts <span>({{ $allExperts->count() }})</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('admin.experts-value-added-services')}}">Value Added Services <span>({{ $allServices->count() }})</span></a>
            </li>
        </ul>
        <!-- //Nav tabs End -->                        

        <!-- //Tab panes Start -->
        <div class="tab-content">
            <div class="tab-pane allExperts active" id="allExperts">
                <!-- //Search Form Div Start -->
                <div class="searchForm">
                    <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                   
                    <form  method="get" action="" id="frm_experts_search">
                        <span>Search</span>
                        <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
                        <ul class="d-flex flex-wrap">
                            <li>
                                <div class="form-group">
                                    <input type="text" name="expert_name" class="form-control" placeholder="Name"  value="{{$expert_name ?? ''}}" id="Name">
                                </div><!-- //form-group -->
                            </li>
                            <li>
                                <div class="form-group">
                                    <input class="form-control" name="doj" id="datepicker_pending_expert" type="text" value="{{$date_of_joining ?? ''}}" placeholder="Date of Joining"/>
                                </div><!-- //form-group -->
                            </li>
                            <li>
                                <div class="form-group">
                                    <select name="service_id" class="form-control" id="service_id">
                                        <option value="">Please select</option>
                                        @foreach($allServices as $valueService)
                                        @php
                                        $selected = '';
                                        if($service_id == $valueService->id){
                                        $selected = 'selected';
                                        }
                                        @endphp
                                        <option value="{{ $valueService->id }}" {{$selected}}>{{ $valueService->service_name }}</option>
                                        @endforeach
                                    </select>
                                </div><!-- //form-group --> 
                            </li>
                            <li>
                                <button type="submit" class="btn btn-primary" name="">Search</button>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="btn btn-reserved" data-toggle="modal" data-target="#joinBroker_expert">Add Expert</a>
                            </li>
                        </ul><!-- //ul -->
                    </form><!-- //form -->
                </div><!-- //search-form -->
                <!-- //Search Form Div End -->

                <div class="payment-table-desktop border">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Expert Name</th>
                                <th>Expert Services</th>
                                <th>Email id</th>
                                <th>User ID</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>

                            @if (count($experts) > 0)
                            @foreach($experts as $expert)
                            <tr>
                                <td scope="row">{{ $expert->name }}</td>
                                <td>{{ (isset($expert->services) && is_array($expert->services->toArray())) ? implode(', ', array_column($expert->services->toArray(), 'service_name')) : '' }}</td>
                                <td>{{ $expert->mobile }}</td>
                                <td>{{ $expert->email }}</td>
                                <td><a href="javascript:void(0);" class="deleteRow">See all Reviews</a> <a href="javascript:void(0);" class="deleteRow"><img src="{{ asset('adminassets/images/svg-icons/delete-icon.svg') }}" alt=""></a></td>
                            </tr>
                            @endforeach
                            @else

                            <tr>
                                <td scope="row" colspan="4">Experts not found</td>
                            </tr>

                            @endif
                        </tbody>
                    </table>

                    <div class="col-12">
                        {{ $experts->links() }}
                    </div><!-- //col-12 pagination -->
                </div><!--//payment-table-desktop-->

                <div class="payment-table-Mobile border">
                    <div class="d-flex flex-wrap justify-content-between payment-list">
                        <div><strong>Abhit Bhatia</strong></div>
                        <div class="w-100">Feasibility Report Analysis</div>
                        <div class="w-100">abhitbhatia@gmail.com</div>
                        <div class="w-100">3242523</div>
                        <div><a href="javascript:void(0);">See all Reviews</a></div>
                        <div><a href="javascript:void(0);"><img src="{{ asset('adminassets/images/svg-icons/delete-icon.svg') }}" alt=""></a></div>
                    </div><!-- //d-flex flex-wrap-->

                    <div class="d-flex flex-wrap justify-content-between payment-list">
                        <div><strong>Vipin Vindal</strong></div>
                        <div class="w-100">Land Listing</div>
                        <div class="w-100">vipin.vindal@gmail.com</div>
                        <div class="w-100">1283293</div>
                        <div><a href="javascript:void(0);">See all Reviews</a></div>
                        <div><a href="javascript:void(0);"><img src="{{ asset('adminassets/images/svg-icons/delete-icon.svg') }}" alt=""></a></div>
                    </div><!-- //d-flex flex-wrap-->

                    <div class="d-flex flex-wrap justify-content-between payment-list">
                        <div><strong>Abhit Bhatia</strong></div>
                        <div class="w-100">Feasibility Report Analysis</div>
                        <div class="w-100">abhitbhatia@gmail.com</div>
                        <div class="w-100">3242523</div>
                        <div><a href="javascript:void(0);">See all Reviews</a></div>
                        <div><a href="javascript:void(0);"><img src="{{ asset('adminassets/images/svg-icons/delete-icon.svg') }}" alt=""></a></div>
                    </div><!-- //d-flex flex-wrap-->

                    <div class="d-flex flex-wrap justify-content-between payment-list">
                        <div><strong>Vipin Vindal</strong></div>
                        <div class="w-100">Land Listing</div>
                        <div class="w-100">vipin.vindal@gmail.com</div>
                        <div class="w-100">1283293</div>
                        <div><a href="javascript:void(0);">See all Reviews</a></div>
                        <div><a href="javascript:void(0);"><img src="{{ asset('adminassets/images/svg-icons/delete-icon.svg') }}" alt=""></a></div>
                    </div><!-- //d-flex flex-wrap-->

                    <div class="col-12">
                        <ul class="pagination pagination-sm justify-content-center">
                            <li class="page-item disabled">
                                <a class="page-link prev" href="#!" tabindex="-1">Prev</a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#!">1</a></li>
                            <li class="page-item"><a class="page-link" href="#!">2</a></li>
                            <li class="page-item"><a class="page-link" href="#!">3</a></li>
                            <li class="page-item">
                                <a class="page-link next" href="#!">Next</a>
                            </li>
                        </ul>
                    </div><!-- //col-12 pagination -->
                </div><!--//payment-table-Mobile-->
            </div><!--//tab-pane -->
        </div><!-- //Tab panes End -->
    </div><!-- //col-12 -->  

</div><!-- //row -->     
@endsection
@section('modal')
<!-- Modal -->     
<!-- Join Kyl as Broker & Expert -->
<div class="modal fade" id="joinBroker_expert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="row no-gutters">
                    <div class="col-md-12">
                        <div class="loginPageBox" id="signupBrokerBox">
                            <ul class="nav nav-tabs" role="tablist"> 
                                <li>
                                    <a class="active" data-toggle="tab" href="#broker_signup" role="tab" aria-selected="false">Broker</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#expert_signup" role="tab"  aria-selected="false">Expert</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContentSignup">
                                <div class="tab-pane fade show active" id="broker_signup" role="tabpanel">
                                    <form name="frm_broker_signup" id="frm_broker_signup" method="post">
                                        @csrf
                                        <div class="popupForm">                                
                                            <div class="form-group floating-field">
                                                <input type="text" class="form-control" placeholder="Full Name" name="broker_full_name" id="broker_full_name">
                                                <label for="full_name" class="floating-label">Full Name</label>
                                            </div><!-- floating-field -->

                                            <div class="form-group floating-field">
                                                <input type="text" class="form-control" placeholder="Email id" id="broker_email" name="broker_email">
                                                <label for="email" class="floating-label">Email id</label>
                                            </div><!-- floating-field -->

                                            <div class="form-group floating-field mobile-field">
                                                <input type="text" class="form-control" placeholder="Mobile Number" name="broker_mobile_number" id="broker_mobile_number">
                                                <label for="mobile_number" class="floating-label">Mobile Number</label>
                                                <span class="mobileCode">+91</span>
                                            </div><!-- floating-field --> 

                                            <div class="form-group floating-field">
                                                <select class="form-control" id="broker_services" name="broker_services[]" multiple="multiple">
                                                    @foreach($allServices as $value)
                                                    @if($value->is_service == 1 && $value->show_for_broker == 1)
                                                    <option value="{{ $value->id }}">{{ $value->service_name }}</option>
                                                    @endif
                                                    @endforeach
                                                </select>

                                                <label for="services_s1" class="floating-label">Services</label>
                                            </div><!-- floating-field -->


                                            <div class="formBtn">
                                                <button type="submit" id="btn_broker" class="btn btn-primary">Submit</button>
                                            </div>

                                        </div><!--//popupForm-->
                                    </form>

                                </div><!--//broker_signup-->

                                <div class="tab-pane fade" id="expert_signup" role="tabpanel">
                                    <div class="alert alert-success alert-block expert-msg" style="display: none;">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                    </div>

                                    <form name="frm_expert_signup" id="frm_expert_signup" method="post">
                                        @csrf
                                        <div class="popupForm">                                
                                            <div class="form-group floating-field">
                                                <input type="text" class="form-control" placeholder="Full Name" name="full_name" id="full_name">
                                                <label for="full_name" class="floating-label">Full Name</label>
                                            </div><!-- floating-field -->

                                            <div class="form-group floating-field">
                                                <input type="text" class="form-control" placeholder="Email id" name="email" id="email">
                                                <label for="email" class="floating-label">Email id</label>
                                            </div><!-- floating-field -->

                                            <div class="form-group floating-field mobile-field">
                                                <input type="text" class="form-control" placeholder="Mobile Number" name="mobile_number" id="mobile_number">
                                                <label for="mobile" class="floating-label">Mobile Number</label>
                                                <span class="mobileCode">+91</span>
                                            </div><!-- floating-field --> 

                                            <div class="form-group floating-field">
                                                <select id="expert_services" multiple="multiple" class="form-control" name="exp_services[]" >
                                                    @foreach($allServices as $value)
                                                    <option value="{{ $value->id }}">{{ $value->service_name }}</option>
                                                    @endforeach
                                                </select>

                                                <label for="services_s2" class="floating-label">Services</label>
                                            </div><!-- floating-field -->


                                            <div class="formBtn">
                                                <button type="submit" id="btn_expert" class="btn btn-primary">Submit</button>
                                            </div>

                                        </div><!--//popupForm-->
                                    </form>

                                </div><!--//expert_signup-->
                            </div><!--//tab-content--> 

                        </div><!--//loginPageBox-->
                    </div><!--//col-md-8-->
                </div><!--//row-->
            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->
@endsection
@section('custom-script')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#expert_services').select2();
        $('#broker_services').select2();
    });


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#frm_expert_signup').submit(function (e) {
        e.preventDefault();
        $(document).find("span.error-span").remove();
        $('.alert-block').hide();
        var formData = new FormData(this);

        $('#btn_expert').prop("disabled", true);
        $('#btn_expert').text('Sending...');

        $.ajax({
            type: 'POST',
            url: "{{ route('storeexpert') }}",
            data: $("#frm_expert_signup").serialize(),
            dataType: "json",
            success: function (data) {
                $('#btn_expert').prop("disabled", false);
                $('#btn_expert').text('Submit');
                if (data.success) {
                    $('#frm_expert_signup')[0].reset();
                    swal("Broker added successfully.")
                            .then((value) => {
                                location.reload();
                            });
                } else {
                    $('.expert-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
                }
            },
            error: function (errorResponse) {
                $('#btn_expert').prop("disabled", false);
                $('#btn_expert').text('Submit');
                $.each(errorResponse.responseJSON.errors, function (field_name, error) {
                    if (field_name == 'exp_services') {
                        $('#expert_services').next().after('<span class="text-strong error-span" role="alert">' + error + '</span>');
                    } else {
                        $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span" role="alert">' + error + '</span>');
                    }
                })
            }
        });
    });

    // broker signup form
    $('#frm_broker_signup').submit(function (e) {
        e.preventDefault();
        $(document).find("span.error-span").remove();
        $('.alert-block').hide();
        var formData = new FormData(this);
        $('#btn_broker').prop("disabled", true);
        $('#btn_broker').text('Sending...');

        $.ajax({
            type: 'POST',
            url: "{{ route('storebroker') }}",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $('#btn_broker').prop("disabled", false);
                $('#btn_broker').text('Submit');
                if (data.success) {
                    $('#frm_broker_signup')[0].reset();
                    swal("Broker added successfully.")
                            .then((value) => {
                                location.reload();
                            });
                } else {
                    $('.expert-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
                }
            },
            error: function (errorResponse) {
                $('#btn_broker').prop("disabled", false);
                $('#btn_broker').text('Submit');
                $.each(errorResponse.responseJSON.errors, function (field_name, error) {
                    if (field_name == 'broker_services') {
                        $('#broker_services').next().after('<span class="text-strong error-span" role="alert">' + error + '</span>');
                    } else {
                        $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span" role="alert">' + error + '</span>');
                    }
                })
            }
        });
    });
</script>
@endsection