@extends('admin.layouts.master')
@section('title')
Admin Dashboard
@endsection
@section('content')

<div class="pageTitle">
	<h1>Dashboard</h1> 
</div>

<div class="countCards">
    <div class="row">
        <div class="col-lg-4 col-md-6">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('adminassets/images/svg-icons/appointments-icon.svg') }}" alt="All Appointments" />
                </div>
                <div class="countRepDetail">
                    <p><strong>{{ $total_app->count()}}</strong></p>
                    <p>All Appointments</p>
                </div>
            </article><!--//countCardArt-->
        </div><!--//col-lg-4-->

        <div class="col-lg-4 col-md-6">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('adminassets/images/svg-icons/listing-icon.svg') }}" alt="All Land Listing" />                                    
                </div>
                <div class="countRepDetail">
                    <p><strong>{{ $total_land->count()}}</strong></p>
                    <p>All Land Listing</p>
                </div>
            </article><!--//countCardArt-->
        </div><!--//col-lg-4-->

        <div class="col-lg-4 col-md-6">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('adminassets/images/svg-icons/admin-reports-icon.svg') }}" alt="All Reports" />                                    
                </div>
                <div class="countRepDetail">
                    <p><strong>{{ $total_report->count()}}</strong></p>
                    <p>All Reports</p>
                </div>
            </article><!--//countCardArt-->
        </div><!--//col-lg-4-->                       
    </div><!--//row-->
</div><!--//countCards-->

<div class="div-repeat d-none">
    <div class="pageTitle d-flex align-items-center">
        <h3>Appointments</h3>
        
        <div class="sortDropdown">
            <label>Show by</label>
            <div class="dropdown custom-dropdown">
                <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle">
                    <span class="selected">{{$filterDate}}</span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" class="expertPaginate" data-value="week">Week</a></li>
                    <li><a href="#" class="expertPaginate" data-value="month">Month</a></li>
                    <li><a href="#" class="expertPaginate" data-value="year">Year</a></li>
                </ul>
            </div><!-- //dropdown --> 
        </div><!--//sortDropdown-->
    </div><!--//pageTitle-->
 
    <div class="graphDiv border">
        <canvas id="myChart" width="400" height="300"></canvas>
    </div> 
</div><!--//div-repeat -->

<div class="div-repeat d-none">
    <div class="pageTitle d-flex align-items-center">
        <h3>Land Listing</h3>
        
        <!--
        <div class="sortDropdown">
            <label>Show by</label>
            <div class="dropdown custom-dropdown">
                <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle">
                    <span class="selected">Month</span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#">Week</a></li>
                    <li><a href="#">Month</a></li>
                    <li><a href="#">Year</a></li>
                </ul>
            </div>
        </div>
        --> 
    </div><!--//pageTitle-->
 
    <div class="graphDiv border">
        <canvas id="landdetailChart" width="400" height="300"></canvas>
    </div> 
</div><!--//div-repeat -->

<div class="div-repeat d-none">
    <div class="pageTitle d-flex align-items-center">
        <h3>Reports</h3>
        
        <!--
        <div class="sortDropdown">
            <label>Show by</label>
            <div class="dropdown custom-dropdown">
                <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle">
                    <span class="selected">Month</span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#">Week</a></li>
                    <li><a href="#">Month</a></li>
                    <li><a href="#">Year</a></li>
                </ul>
            </div> 
        </div>
        --> 
    </div><!--//pageTitle--> 

    <div class="graphDiv border d-none">
        <canvas id="landreportChart" width="400" height="300"></canvas>
    </div> 
</div><!--//div-repeat --> 

<form method="post" id="paginateSubmitForm">
    @csrf
    <input type="hidden" name="day" id="paginateHiddenFiled">
</form>
@endsection
@section('custom-script')
<script>
$('.expertPaginate').on('click', function (e) {
e.preventDefault();
var value = $(this).data('value');
$('#paginateHiddenFiled').val(value);
$('#paginateSubmitForm').submit();
return false;
});
window.onload = function () {
// Admin appointment chart show
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
type: 'line',
        data: {
        labels: {!! $adminEarningDate !!},
                datasets: [{
                label: "{!! $doneAppointmentTitle !!}",
                        data: {!! $adminEarningAmount !!},
                        backgroundColor: 'transparent',
                        borderColor: "#4bc0c0",
                        fill: true,
                        borderWidth: 2
                },
                {
                label: "{!! $pendingAppointmentTitle !!}",
                        data: {!! $pendingAppointment !!},
                        backgroundColor: 'transparent',
                        borderColor: "#ff6384",
                        fill: true,
                        borderWidth: 2
                },
                ]
        },
        options: {
        scales: {
        yAxes: [{
        ticks: {
        beginAtZero:true
        }
        }]
        }
        }
});
// Admin land chart show
var ctx = document.getElementById('landdetailChart').getContext('2d');
var myChart = new Chart(ctx, {
type: 'line',
        data: {
        labels: {!! $landVerifiedDate !!},
                datasets: [{
                label: "verified lands",
                        data: {!! $landVerifiedDeatil !!},
                        backgroundColor: 'transparent',
                        borderColor: "#4bc0c0",
                        fill: true,
                        borderWidth: 2
                },
                {
                label: "Not verified lands",
                        data: {!! $landUnverifiedDeatil !!},
                        backgroundColor: 'transparent',
                        borderColor: "#ff6384",
                        fill: true,
                        borderWidth: 2
                },
                ]
        },
        options: {
        scales: {
        yAxes: [{
        ticks: {
        beginAtZero:true
        }
        }]
        }
        }
});

// Admin report chart show
var ctx = document.getElementById('landreportChart').getContext('2d');
var myChart = new Chart(ctx, {
type: 'line',
        data: {
        labels: {!! $reportDeliveredDate !!},
                datasets: [{
                label: "Delivered reports",
                        data: {!! $reportDeliveredDeatil !!},
                        backgroundColor: 'transparent',
                        borderColor: "#4bc0c0",
                        fill: true,
                        borderWidth: 2
                },
                {
                label: "Pending reports",
                        data: {!! $reportPendingDeatil !!},
                        backgroundColor: 'transparent',
                        borderColor: "#ff6384",
                        fill: true,
                        borderWidth: 2
                },
                ]
        },
        options: {
        scales: {
        yAxes: [{
        ticks: {
        beginAtZero:true
        }
        }]
        }
        }
});
};

</script>
@endsection