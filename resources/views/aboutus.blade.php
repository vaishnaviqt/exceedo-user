@extends('layouts.master')

@section('title')
About us
@endsection

@section('splashscreen')
<div class="innerPageBanner aboutBanner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12">
                <h1>About us</h1>
                <p>Exceedo Know Your Land is a comprehensive land solutions
                    platform that offers value-added services in buying, selling,
                    and developing a land -parcel.</p>
                <a href="javascript:void(0);" class="btn btn-primary">Browse our Services</a>
            </div>
        </div>
    </div><!--//container-->
</div><!--//aboutBanner-->

@endsection
@section('content')

<section class="services" id="services">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <h2>Our Services</h2>
                <p>KYL offers a unique, one-to-one consultation service with the experts and specialists on board to resolve all land-related queries with first-hand analysis and real-time problem diagnosis.</p>
            </div>

            <div class="col-12">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-3">
                        <article class="featureArticle">
                            <div class="featureArtImg">
                                <a href="javascript:void(0);"><img src="images/service_1.png" alt="Land Title Search"></a>
                            </div>
                            <div class="featureArtCont">
                                <h3>
                                    <a href="javascript:void(0);">
                                        Land Title Search
                                    </a>
                                </h3>
                                <p class="shortDetail">It is a long established fact that a reader will  be distracted</p>
                            </div><!--//featFacilities-->
                        </article><!--//featureArticle-->
                    </div><!--//col-sm-3-->

                    <div class="col-lg-3 col-md-6 col-sm-3">
                        <article class="featureArticle">
                            <div class="featureArtImg">
                                <a href="javascript:void(0);"><img src="images/service_2.png" alt="Feasibility Analysis"></a>
                            </div>
                            <div class="featureArtCont">
                                <h3>
                                    <a href="javascript:void(0);">
                                        Feasibility Analysis
                                    </a>
                                </h3>
                                <p class="shortDetail">It is a long established fact that a reader will  be distracted</p>
                            </div><!--//featFacilities-->
                        </article><!--//featureArticle-->
                    </div><!--//col-sm-3-->

                    <div class="col-lg-3 col-md-6 col-sm-3">
                        <article class="featureArticle">
                            <div class="featureArtImg">
                                <a href="javascript:void(0);"><img src="images/service_3.png" alt="Buy your Land"></a>
                            </div>
                            <div class="featureArtCont">
                                <h3>
                                    <a href="javascript:void(0);">
                                        Buy your Land
                                    </a>
                                </h3>
                                <p class="shortDetail">It is a long established fact that a reader will  be distracted</p>
                            </div><!--//featFacilities-->
                        </article><!--//featureArticle-->
                    </div><!--//col-sm-3-->

                    <div class="col-lg-3 col-md-6 col-sm-3">
                        <article class="featureArticle">
                            <div class="featureArtImg">
                                <a href="javascript:void(0);"><img src="images/service_4.png" alt="Sell your Land"></a>
                            </div>
                            <div class="featureArtCont">
                                <h3>
                                    <a href="javascript:void(0);">
                                        Sell your Land
                                    </a>
                                </h3>
                                <p class="shortDetail">It is a long established fact that a reader will  be distracted</p>
                            </div><!--//featFacilities-->
                        </article><!--//featureArticle-->
                    </div><!--//col-sm-3-->

                </div>
            </div>
        </div>
    </div>
</section><!-- //services -->
@if(count($valueAddedServices))
<section class="services grayBackground" id="moreValueAddedServices">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <h2>Our Value Added Services</h2>
                <p>KYL offers a unique, one-to-one consultation service with the experts and specialists on board to resolve all land-related queries with first-hand analysis and real-time problem diagnosis.</p>
            </div>
        </div>
        @foreach($valueAddedServices as $key => $services)
        @if($key == 0 || $key == 5)
        <div class="row moreValueAddRow">
            @endif
            <div class="col-lg col-md-4 col-sm-6">
                <article class="valArticle">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#serviceModal" data-id="{{ $services->id }}" data-name="{{ $services->service_name }}" class="getName modelopen">
                        @if(!empty($services->service_icon))
                        <div class="valArtIcon">
                            <img src="{{ asset($services->service_icon) }}" alt="{{ $services->service_name }}" />
                        </div>
                        @endif
                        <h3>{{ $services->service_name }}<span class="titleArrowIcon"></span></h3>
                        <p>{{ $services->description ?? '' }}</p> 
                    </a>
                </article>
            </div>
            @if($key == 9|| $key == 4)
        </div>
        @endif
        @endforeach
    </div>
    <!--//container-->
</section><!-- //Our Value Added Services -->
@endif

<section class="services clientsPartners">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="float-left">Clients & Partners</h2>
            </div>
            <div class="col-sm-12">
                <ul class="partnersCompanies owl-carousel">
                    <li>
                        <img src="images/complany_logo_01.svg" alt="">
                    </li>
                    <li>
                        <img src="images/complany_logo_02.svg" alt="">
                    </li>
                    <li>
                        <img src="images/complany_logo_03.svg" alt="">
                    </li>
                    <li>
                        <img src="images/complany_logo_04.svg" alt="">
                    </li>
                    <li>
                        <img src="images/complany_logo_01.svg" alt="">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- //Clients & Partners   -->

<section class="services">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>What our Clients are Saying</h2>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="cliensSaying owl-carousel">
                    <article>
                        <!-- <span class="top-coma"><img src="images/svg-icons/open.svg" alt=""></span> -->
                        <p>It gives me great pleasure to recommend the services of 'Exceedo' for effective land-use type utilisation in Haryana, Our commercial showrooms in Ambala would not have been possible without their valuable contribution.</p>
                        <!-- <span class="bottom-coma"><img src="images/svg-icons/close.svg" alt=""></span> -->
                        <div class="clientdetails">
                            <div class="clientImg"><img src="images/client-img.png" alt=""></div>
                            <div class="companayName">Mr. Pradeep Jain <span>Cisco Infra Pvt. Ltd.</span></div>
                        </div>
                    </article>

                    <article>
                        <!-- <span class="top-coma">"</span> -->
                        <p>It gives me great pleasure to recommend the services of 'Exceedo' for effective land-use type utilisation in Haryana, Our commercial showrooms in Ambala would not have been possible without their valuable contribution.</p>
                        <!-- <span class="bottom-coma">"</span> -->
                        <div class="clientdetails">
                            <div class="clientImg"><img src="images/client-img.png" alt=""></div>
                            <div class="companayName">Mr. Pradeep Jain <span>Cisco Infra Pvt. Ltd.</span></div>
                        </div>
                    </article>

                    <article>
                        <!-- <span class="top-coma">"</span> -->
                        <p>It gives me great pleasure to recommend the services of 'Exceedo' for effective land-use type utilisation in Haryana, Our commercial showrooms in Ambala would not have been possible without their valuable contribution.</p>
                        <!-- <span class="bottom-coma">"</span> -->
                        <div class="clientdetails">
                            <div class="clientImg"><img src="images/client-img.png" alt=""></div>
                            <div class="companayName">Mr. Pradeep Jain <span>Cisco Infra Pvt. Ltd.</span></div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //What our Clients are Saying -->


<section class="home_contactSection minHeight_100">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="contactTitle">
                    <h2>Contact Us</h2>
                    <h3>Please contact us if you need to learn more about KYL and our services.</h3>
                </div>

                <div class="contactForm">

                    <div class="form-group floating-field">
                        <input type="text" class="form-control" placeholder="Name" id="c_name">
                        <label for="c_name" class="floating-label">Name</label>
                    </div><!-- floating-field -->

                    <div class="form-group floating-field">
                        <input type="text" class="form-control" placeholder="Email" id="c_email">
                        <label for="c_email" class="floating-label">Email</label>
                    </div><!-- floating-field -->

                    <div class="form-group floating-field">
                        <input type="text" class="form-control" placeholder="Mobile Number" id="c_mobile">
                        <label for="c_mobile" class="floating-label">Mobile Number</label>
                    </div><!-- floating-field -->

                    <div class="form-group floating-field">
                        <textarea class="form-control" placeholder="Message" id="c_message"></textarea>
                        <label for="c_message" class="floating-label">Message</label>
                    </div><!-- floating-field -->

                    <div class="contactSubmitBtn">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div><!--//col-md-6-->

            <div class="col-md-6">
                <div class="contactRightCont">
                    <div class="contAdditionBtns">
                        <a href="tel:9315495030" class="btn btn-phone">+919315495030</a>
                        <a href="mailto:mail@kylnow.com" class="btn btn-mail">mail@kylnow.com</a>
                    </div>

                    <div class="locationTooltipBox">
                        <a href="javascript:void(0);" class="locationIcon"></a>
                        <div class="locationTooltip">
                            <h3>Exceedo Developers & Consultants Pvt. Ltd.</h3>
                            <p>Freedom Fighter Colony, Block B, Neb Sarai,
                                Sainik Farm, New Delhi, Delhi 110030</p>
                        </div>
                    </div>

                    <div class="viewLgMapBtn">
                        <a href="https://maps.google.com/maps?ll=28.510225,77.201031&z=17&t=m&hl=en&gl=IN&mapclient=embed&cid=15876987383248892398" class="btn" target="_blank">View Large map</a>
                    </div>
                </div><!--//contactRightCont-->
            </div><!--//col-md-6-->
        </div><!--//row-->
    </div><!--//container-->
</section><!--//home_contactSection-->
@endsection


