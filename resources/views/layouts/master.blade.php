<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials.head')
    <style>
        .hogaya {
            height: 840px;
            overflow: auto;
        }

        .slider1 {
            height: 600px;
            overflow: auto;
        }

        .slider1::-webkit-scrollbar-track {
            -webkit-box-shadow: none !important;
            background-color: transparent !important;
        }

        .slider1::-webkit-scrollbar {
            width: 16px !important;
            background-color: transparent;
        }

        .slider1::-webkit-scrollbar-thumb {
            /* background-color: #acacac; */
        }


        .hogaya::-webkit-scrollbar-track {
            -webkit-box-shadow: none !important;
            background-color: transparent !important;
        }

        .hogaya::-webkit-scrollbar {
            width: 16px !important;
            background-color: transparent;
        }

        .hogaya::-webkit-scrollbar-thumb {
            /* background-color: #acacac; */
        }
    </style>
</head>

<body>

    @yield('splashscreen')

    <a href="{{('meet-the-expert')}}" class="meetExpertBtn"></a>

    <div class="wrapper">
        <header class="header @if(request()->is('buy-your-land', 'sell-your-land', 'land-title-search-form', 'sell-your-land-form') != true) transparentHeader @endif">
            @include('partials.header')
        </header>
        <!--//header-->

        @yield('content')
        @if(request()->is('buy-your-land') != true)
        <footer class="footer">
            @include('partials.footer')
        </footer>
        <!--//footer-->
        @endif
    </div>
    <!--//wrapper-->


    <!-- Modal -->
    <!-- serviceModal -->
    <div class="modal fade w-400 modelopen" id="serviceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                    <div class="row no-gutters">
                        <div class="col-md-5">
                            <div class="popupFormPic">
                                <img src="images/anciliary-services-pic.png">
                            </div>
                        </div>
                        <!--//col-md-4-->
                        <div class="col-md-7">
                            <div class="popupForm">
                                <form id="frm_added_service">
                                    <div class="popupHeading">
                                        <h2 id="book-appointment-title"></h2>
                                    </div>
                                    <input type="hidden" name="value_added_service_id" id="value_added_service_id">
                                    <div class="form-group floating-field">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Name" id="name" name="name" value="{{ old('name') }}">
                                        <label for="name" class="floating-label">Name</label>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group floating-field">
                                        <input type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Email" id="email" name="email" value="{{ old('email') }}">
                                        <label for="email" class="floating-label">Email</label>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group floating-field mobile-field">
                                        <input type="text" class="form-control @error('mobile') is-invalid @enderror" placeholder="Mobile Number" id="mobile" name="mobile" value="{{ old('mobile') }}">
                                        <label for="mobile" class="floating-label">Mobile Number</label>
                                        <span class="mobileCode">+91</span>
                                        @error('mobile')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group floating-field mb-20">
                                        <textarea class="form-control @error('comments') is-invalid @enderror" placeholder="Comments" id="comments" name="comments">{{old('comments')}}
                                        </textarea>
                                        <label for="comments" class="floating-label">Comments</label>
                                        @error('comments')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="formBtn">
                                        <button type="submit" id="service-enquiry" class="btn btn-primary btn-submit"> Submit </button>
                                    </div>
                                </form>
                            </div>
                            <!--//popupForm-->
                        </div>
                        <!--//col-md-8-->
                    </div>
                    <!--//row-->

                </div>
                <!--//modal-body-->
            </div>
        </div>
    </div>
    <!--//modal-->

    @yield('modal')

    @include('partials.javascripts')

    @section('custom-script')
    @show

    <script type="text/javascript">
        $(document).ready(function() {
            $.scrollify({
                //section : ".scrollSection",
                setHeights: false,
                scrollSpeed: 2000,
            });

            scrollifyConditionFun();
            $(window).resize(scrollifyConditionFun);

            function scrollifyConditionFun() {
                if ($(window).width() <= 1350) {
                    $.scrollify.disable();
                    $(".scrolly").removeAttr("style");
                } else {
                    $.scrollify.enable();
                }
            }
        });
    </script>
    <script type="text/javascript">
        formReset = function() {
            $("#frm_added_service").trigger('reset');
        }
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#frm_added_service").trigger('reset');
            $(".btn-submit").click(function(e) {
                e.preventDefault();
                var serviceId = $("#value_added_service_id").val();
                var name = $("#name").val();
                var email = $("#email").val();
                var mobile = $("#mobile").val();
                var comments = $("#comments").val();
                $.ajax({
                    type: 'post',
                    url: "{{ route('store-services') }}",
                    data: {
                        value_added_service_id: serviceId,
                        name: name,
                        email: email,
                        mobile: mobile,
                        comments: comments
                    },
                    success: function(data) {
                        $('#service-enquiry').prop("disabled", false);
                        $('#service-enquiry').text('Submit');
                        if (data.success) {
                            $('#frm_added_service')[0].reset();
                            swal({
                                title: "Thank you for showing interest in Know Your Land (KYL) services!",
                                text: "Your enquiry has been successfully submitted. The KYL team will get back to you shortly. Please visit your inbox for more details on your query!",
                                icon: "success",
                            }).then((value) => {
                                location.reload();
                            });
                        } else {
                            swal({
                                text: "Something went wrong!",
                                icon: "error",
                            });
                        }
                    },
                    error: function(errorResponse) {
                        $('#service-enquiry').prop("disabled", false);
                        $('#service-enquiry').text('Submit');
                        $.each(errorResponse.responseJSON.errors, function(field_name, error) {
                            $(document).find('[name=' + field_name + ']').next().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        })
                    }
                });
            });
        });
    </script>

</body>

</html>