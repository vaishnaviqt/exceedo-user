<div class="homeBanner">
            <div id="bannerSlider" class="owl-carousel"> 
                <div class="item">
                    <img src="images/home-banner-4.jpg" alt="" class="banMainImg">
                    <div class="container">
                        <div class="bannerContent">
                            <span class="banTopLine"></span>
                            <h2>Buy your Land</h2>
                            <p>A comprehensive land solutions platform that enables you to discover the best potential of your land and its development.</p>

                            <div class="banKnowBtn">
                                <a href="javascript:void(0);" class="btn btn-primary">know More</a>
                            </div>
                        </div><!--//bannerContent-->
                    </div><!--//container-->
                </div>
                <div class="item">
                    <img src="images/home-banner-5.jpg" alt="" class="banMainImg">
                    <div class="container">
                        <div class="bannerContent">
                            <span class="banTopLine"></span>
                            <h2>Sell your Land</h2>
                            <p>A comprehensive land solutions platform that enables you to discover the best potential of your land and its development.</p>

                            <div class="banKnowBtn">
                                <a href="javascript:void(0);" class="btn btn-primary">know More</a>
                            </div>
                        </div><!--//bannerContent-->
                    </div><!--//container-->
                </div> 
                <div class="item">
                    <img src="images/home-banner-3.jpg" alt="" class="banMainImg">
                    <div class="container">
                        <div class="bannerContent">
                            <span class="banTopLine"></span>
                            <h2>Feasibility Analysis</h2>
                            <p>A comprehensive land solutions platform that enables you to discover the best potential of your land and its development.</p>

                            <div class="banKnowBtn">
                                <a href="javascript:void(0);" class="btn btn-primary">know More</a>
                            </div>
                        </div><!--//bannerContent-->
                    </div><!--//container-->
                </div>
                <div class="item">
                    <img src="images/home-banner-2.jpg" alt="" class="banMainImg">
                    <div class="container">
                        <div class="bannerContent">
                            <span class="banTopLine"></span>
                            <h2>Land Title Search</h2>
                            <p>A comprehensive land solutions platform that enables you to discover the best potential of your land and its development.</p>

                            <div class="banKnowBtn">
                                <a href="javascript:void(0);" class="btn btn-primary">know More</a>
                            </div>
                        </div><!--//bannerContent-->
                    </div><!--//container-->
                </div>
            </div>

            <div id="bannerThumbnails" class="owl-carousel">
                <div class="thumbBanTiles" data-dot="<button role='button' class='owl-dot'>01</button>" onclick="location.href = 'buy-your-land.html';">
                    <img src="images/home-banner-4.jpg">
                    <div class="thumbContent">
                        <h3>Buy your Land</h3>
                        <span class="thumbHeadSep"></span>
                        <p>It is a long established fact that a reader will  be distracted</p>
                    </div>
                </div>
                <div class="thumbBanTiles" data-dot="<button role='button' class='owl-dot'>02</button>" onclick="location.href = 'sell-your-land.html';">
                    <img src="images/home-banner-5.jpg">
                    <div class="thumbContent">
                        <h3>Sell your Land</h3>
                        <span class="thumbHeadSep"></span>
                        <p>It is a long established fact that a reader will  be distracted</p>
                    </div>
                </div> 
                <div class="thumbBanTiles" data-dot="<button role='button' class='owl-dot'>03</button>" onclick="location.href = 'feasibility-analysis.html';">
                    <img src="images/home-banner-3.jpg">
                    <div class="thumbContent">
                        <h3>Feasibility Analysis</h3>
                        <span class="thumbHeadSep"></span>
                        <p>It is a long established fact that a reader will  be distracted</p>
                    </div>
                </div>
                <div class="thumbBanTiles" data-dot="<button role='button' class='owl-dot'>04</button>" onclick="location.href = 'land-title-search.html';">
                    <img src="images/home-banner-2.jpg">
                    <div class="thumbContent">
                        <h3>Land Title Search</h3>
                        <span class="thumbHeadSep"></span>
                        <p>It is a long established fact that a reader will  be distracted</p>
                    </div>
                </div>
            </div>
        </div><!--//homeBanner-->