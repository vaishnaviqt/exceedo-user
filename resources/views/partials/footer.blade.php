 <div class="container">
                <div class="footerLinks">
                    <div class="row">
                        <div class="col-md-2 d-none d-sm-block">
                            <div class="footerLogo">
                                <a href="{{ url('/') }}"><img src="{{ asset('images/logo-black.png') }}" alt="logo"></a>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="footerNav">
                                <ul>
                                    <li><a href="{{ route('aboutus') }}">About Us</a></li>
                                    <li><a href="{{ route('aboutus') }}#services">Services</a></li>
                                    <li><a href="{{ route('meettheexpert') }}">Meet the Expert</a></li>
                                    <li><a href="{{ route('landtitlesearch') }}">Land Title Search</a></li>
                                    <li><a href="{{ route('feasibilityanalysis') }}">Feasibility Analysis</a></li>

                                    <li class="d-none d-md-block"><a href="{{ route('buyyourland') }}">Buy Land</a></li>
                                    <li class="d-none d-md-block"><a href="{{ route('sellland') }}">Sell Land</a></li>
                                </ul>
                                <ul>
                                    <li class="d-block d-md-none"><a href="{{ route('buyyourland') }}">Buy Land</a></li>
                                    <li class="d-block d-md-none"><a href="{{ route('sellland') }}">Sell Land</a></li>

                                    <li><a href="{{ route('cookiepolicy') }}">Cookie Policy</a></li>
                                    <li><a href="{{ route('privacypolicy') }}"">Privacy Policy</a></li>
                                    <li><a href="{{ route('termsconditions') }}">Terms & Conditions</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="footerSocial">
                                <h3>Connect with us</h3>
                                <a href="javascript:void(0);" class="facebook"> </a>
                                <a href="javascript:void(0);" class="twitter"> </a>
                                <a href="javascript:void(0);" class="linkedin"> </a>
                            </div>
                        </div>
                    </div><!--//row-->
                </div><!--//footerLinks-->

                <div class="footerCopyright">
                    <div class="footerLogo d-block d-sm-none">
                        <a href="index.html"><img src="{{ asset('images/logo-black.png') }}" alt="logo"></a>
                    </div>

                    Copyright © {{date('Y')}} - Exceedo Know Your Land LLP. All Rights Reserved
                </div>

            </div><!--//container-->
