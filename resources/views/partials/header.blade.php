<div class="container">
    <div class="logo">
        <a href="{{'home'}}">
            <img src="{{ asset('images/logo.png') }}" class="whiteLogo" alt="logo">
            <img src="{{ asset('images/logo-black.svg') }}" class="darkLogo" alt="logo">
            Know your Land
        </a>
    </div>
    <!--//logo-->

    <div class="navigation">
        <ul>
            <li><a class="@if(request()->is('buy-your-land')) {{ __('active') }} @endif" href="{{ route('buyyourland') }}">Buy</a></li>
            <li><a class="@if(request()->is('sell-your-land', 'sell-your-land-form')) {{ __('active') }} @endif" href="{{ route('sellland') }}">Sell</a></li>
            <li><a class="@if(request()->is('meettheexpert', 'bookyourappointment')) {{ __('active') }} @endif" href="{{route('meettheexpert')}}">Meet the Expert</a></li>
            <li><a class="@if(request()->is('feasibility-analysis','/feasibility-analysis-form')) {{ __('active') }} @endif" href="{{ route('feasibilityanalysis') }}">Feasibility Analysis</a></li>
            <li><a class="@if(request()->is('land-title-search', 'land-title-search-form')) {{ __('active') }} @endif" href="{{ route('landtitlesearch') }}">Land Title Search</a></li>
            <!-- <li><a href="meet-the-expert.html">Meet the Expert</a></li>  -->
        </ul>
    </div>
    @if(!Auth::check())
    <div class="headerUserBtn">
        <a href="{{ route('login') }}" class="btn">Login / Join</a>
    </div>
    @endif

    <div class="hamburgerMenu">
        <a href="javascript:void(0);" class="hamburgerBtn" id="hamburgerMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
        @if(Auth::check())
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="hamburgerMenuButton">
            <div class="mobileNavCloseBtn">
                <a href="javascript:void(0);"></a>
            </div>
            <div class="loginUserInfo">
                <div class="userAvtar">
                    @php
                    $path = asset('broker/images/svg-icons/user-icon.svg');
                    if(Storage::disk('broker')->exists(str_replace('broker_image', '', auth()->user()->photo)))
                    {
                    $path = Storage::disk('broker')->url(str_replace('broker_image/', '', auth()->user()->photo));
                    }
                    if(Storage::disk('expert')->exists(str_replace('expert_image', '', auth()->user()->photo)))
                    {
                    $path = Storage::disk('expert')->url(str_replace('expert_image/', '', auth()->user()->photo));
                    }
                    if(Storage::disk('user')->exists(str_replace('user_image', '', auth()->user()->photo)))
                    {
                    $path = Storage::disk('user')->url(str_replace('user_image/', '', auth()->user()->photo));
                    }
                    @endphp
                    <img src="{{ $path }}" alt="Abhit Bhatia" />
                </div>
                <div class="userName">
                    <h3>{{ auth()->user()->name }}</h3>
                    <h4>{{ auth()->user()->mobile }}</h4>
                </div>
            </div>
            <ul class="mainMenuMobile">
                <li><a class="@if(request()->is('buy-your-land')) {{ __('active') }} @endif" href="{{ route('buyyourland') }}">Buy</a></li>
                <li><a class="@if(request()->is('sell-your-land', 'sell-your-land-form')) {{ __('active') }} @endif" href="{{ route('sellland') }}">Sell</a></li>
                <li><a class="@if(request()->is('feasibility-analysis')) {{ __('active') }} @endif" href="{{ route('feasibilityanalysis') }}">Feasibility Analysis</a></li>
                <li><a class="@if(request()->is('land-title-search', 'land-title-search-form')) {{ __('active') }} @endif" href="{{ route('landtitlesearch') }}">Land Title Search</a></li>
            </ul>
            <ul>
                @php
                $appointments ='';
                $land = '';
                $report = '';
                $projects = '';
                $logout = '';
                if(auth()->user()->hasRole('user')){
                $appointments = url('user/appointments');
                $land = url('user/your-land-listing');
                $report = url('user/your-land-report');
                $logout = url('user/userlogout');

                }elseif(auth()->user()->hasRole('expert')){
                $appointments = url('expert/appointments');
                $land = url('expert/your-land-listing');
                $projects = url('user/projects');
                $logout = url('expert/expertlogout');

                }elseif(auth()->user()->hasRole('broker')){
                $appointments = url('broker/appointments');
                $land = url('broker/projects');
                $projects = url('broker/your-land-report');
                $logout = url('broker/brokerlogout');
                }
                @endphp
                @if(!empty($appointments))
                <li><a href="{{ $appointments }}">Appointment</a></li>
                @endif
                @if(!empty($land))
                <li><a href="{{ $land }}">Your Listing</a></li>
                @endif
                @if(!empty($report))
                <li><a href="{{ $report }}">Your Land Report</a></li>
                @endif
                @if(!empty($projects))
                <li><a href="{{ $projects }}">Your Projects</a></li>
                @endif
                @if(!empty($logout))
                <li class="desktopLogout"><a href="{{ $logout }}">Logout</a></li>
                @endif
            </ul>
            @if(!empty($logout))
            <div class="mobileLoginBtn">
                <a href="{{ $logout }}" class="btn">Logout</a>
            </div>
            @endif
        </div>
        @else
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="hamburgerMenuButton">
            <div class="mobileNavCloseBtn">
                <a href="javascript:void(0);"></a>
            </div>
            <ul class="mainMenuMobile">
                <li><a class="@if(request()->is('buy-your-land')) {{ __('active') }} @endif" href="{{ route('buyyourland') }}">Buy</a></li>
                <li><a class="@if(request()->is('sell-your-land', 'sell-your-land-form')) {{ __('active') }} @endif" href="{{ route('sellland') }}">Sell</a></li>
                <li><a class="@if(request()->is('meettheexpert', 'bookyourappointment')) {{ __('active') }} @endif" href="{{route('meettheexpert')}}">Meet the Expert</a></li>
                <li><a class="@if(request()->is('feasibilityanalysis')) {{ __('active') }} @endif" href="{{ route('feasibilityanalysis') }}">Feasibility Analysis</a></li>
                <li><a class="@if(request()->is('landtitlesearch', 'land-title-search-form')) {{ __('active') }} @endif" href="{{ route('landtitlesearch') }}">Land Title Search</a></li>

            </ul>
            <ul>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li><a href="{{ route('aboutus') }}#services">Services</a></li>
                <!-- <li><a href="{{ route('meettheexpert') }}">Meet the Expert</a></li> -->
                <li><a href="{{'home#contactUs'}}">Contact Us</a></li>
            </ul>

            <div class="mobileLoginBtn">
                <a href="{{ route('login') }}" class="btn">Login / Join</a>
            </div>
        </div>
        <!--//dropdown-menu-->
        @endif


        <!-- BELOW DROPDOWN HTML - WHEN USER IS LOGIN - @DEV - PLEASE MAKE DYNAMIC -->
        <!-- 
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="hamburgerMenuButton">
            <div class="mobileNavCloseBtn">
                <a href="javascript:void(0);"></a>
            </div>
            <div class="loginUserInfo">
                <div class="userAvtar"> 
                    <img src="images/svg-icons/userIcon.svg" alt="Abhit Bhatia" />
                </div>
                <div class="userName">
                    <h3>Abhit Bhatia</h3>
                    <h4>+9560465467</h4>
                </div>
            </div>
            <ul class="mainMenuMobile">
                <li><a href="buy-your-land.html">Buy</a></li> 
                <li><a href="sell-your-land.html">Sell</a></li>  
                <li><a href="feasibility-analysis.html">Feasibility Analysis</a></li>
                <li><a href="land-title-search.html">Land Title Search</a></li>  
            </ul>
            <ul> 
                <li><a href="javascript:void(0);">Dashboard</a></li> 
                <li><a href="javascript:void(0);">Your Land Report</a></li> 
                <li><a href="javascript:void(0);">Your Listing</a></li> 
                <li class="desktopLogout"><a href="javascript:void(0);">Logout</a></li> 
            </ul>
            
            <div class="mobileLoginBtn">
                <a href="javscript:void(0);" class="btn">Logout</a>
            </div>
        </div>
        -->

    </div>
    <!--//hamburgerMenu-->
</div>
<!--//container-->