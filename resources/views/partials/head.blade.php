<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<title>{{ config('app.name', 'Exceedo') }} &#8211; @yield('title')</title>

<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-float-label.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css?v=2.1') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css?v=2.1') }}">

<link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('css/animate.css') }}">
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/multistep-form.css') }}">
<link rel="stylesheet" href="{{ asset('css/range-slider.css') }}">
<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
<meta name="base_url" content="{{ url('/') }}">
<meta name="csrf-token" content="{{ csrf_token() }}">


