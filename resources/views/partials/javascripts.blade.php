<script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.scrollify.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/custom.js?v=2.1') }}"></script>
<script type="text/javascript" src="{{ asset('js/home.js?v=2.1') }}"></script>
<script type="text/javascript" src="{{ asset('js/toastr.min.js?v=2.1') }}"></script>
<script type="text/javascript" src="{{ asset('js/multistep-form.js?v=2.1') }}"></script>
<script type="text/javascript" src="{{ asset('js/range-slider.js?v=2.1') }}"></script>
<script type="text/javascript" src="{{ asset('js/aboutus.js?v=2.5') }}"></script>
 <script type="text/javascript" src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
 <script type="text/javascript" src="{{ asset('js/detail-tabContent-scrolling.js') }}"></script>
<script type="text/javascript" src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


