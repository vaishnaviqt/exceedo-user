<!-- //Nav tabs Start -->
<ul class="nav nav-tabs">
    <li class="nav-item">
        @php
        $report = 0;
        if(isset($allReports)){
        $report = $allReports->total();
        }else{
        $report = landReportCount();
        }
        @endphp
        <a class="nav-link @if(request()->is('expert/projects')) {{ __('active') }} @endif"  href="{{ url('expert/projects') }}">Projects <span>({{ $report }})</span></a>
    </li>
    <li class="nav-item">
        @php
        $delivered = 0;
        if(isset($deliveredReports)){
        $delivered = $deliveredReports->total();
        }else{
        $delivered = landReportCount(1);
        }
        @endphp
        <a class="nav-link @if(request()->is('expert/delivered-projects')) {{ __('active') }} @endif" href="{{ url('expert/delivered-projects') }}">Delivered Projects <span>({{ $delivered }})</span></a>
    </li>
    <li class="nav-item">
        @php
        $inprogress = 0;
        if(isset($inprogressReports)){
        $inprogress = $inprogressReports->total();
        }else{
        $inprogress = landReportCount(2);
        }
        @endphp
        <a class="nav-link @if(request()->is('expert/inprogress-projects')) {{ __('active') }} @endif" href="{{ url('expert/inprogress-projects') }}">Inprogress Projects <span>({{ $inprogress }})</span></a>
    </li>
    <li class="nav-item">
        @php
        $pending = 0;
        if(isset($pendingReports)){
        $pending = $pendingReports->total();
        }else{
        $pending = landReportCount('pending');
        }
        @endphp
        <a class="nav-link @if(request()->is('expert/pending-projects')) {{ __('active') }} @endif" href="{{ url('expert/pending-projects') }}">Pending Projects <span>({{ $pending }})</span></a>
    </li>
    <li class="nav-item">
        @php
        $pending = 0;
        if(isset($pendingReports)){
        $pending = $pendingReports->total();
        }else{
        $pending = landReportCount('pending');
        }
        @endphp
        <a class="nav-link @if(request()->is('expert/service-enquiry')) {{ __('active') }} @endif" href="{{ url('expert/service-enquiry') }}">{{ userValueAddedService() }} <span>({{ $pending }})</span></a>
    </li>
</ul>
<!-- //Nav tabs End -->

<!-- //Search Form Div Start -->
<div class="searchForm">
    <a href="javascript:void(0);" class="open-mobile-search">Search</a>
    @php
    $url ='';
    if(request()->is('expert/projects')){
    $url =url('expert/projects');
    }else if(request()->is('expert/delivered-projects')){
    $url =url('expert/delivered-projects');
    }else if(request()->is('expert/inprogress-projects')){
    $url =url('expert/inprogress-projects');
    }else if(request()->is('expert/pending-projects')){
    $url =url('expert/pending-projects');
    }
    @endphp
    <form action="{{ $url }}" method="post">
        @csrf
        <span>Search</span>
        <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('expert/images/svg-icons/close-icon.svg') }}" alt=""></a>
        <ul class="d-flex flex-wrap">
            <li>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Name" id="name" name="name" value="{{ $name }}">
                </div><!-- //form-group -->
            </li>
            <!--                    <li>
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Report Type</option>
                                            <option>Delivered</option>
                                            <option>Pending</option>
                                            <option>Inprogress</option>
                                        </select>
                                    </div> //form-group  
                                </li>-->
            @php
            $states = stateListing();
            @endphp
            <li>
                <div class="form-group">
                    <select class="form-control" id="reportType" name="location">
                        <option value="">Select State</option>
                        @foreach($states as $state)
                        <option value="{{ $state->id }}" @if($location == $state->id) {{ __('selected') }} @endif>{{ $state->name }}</option>
                        @endforeach
                    </select>
                </div><!-- //form-group -->
            </li>
            <li>
                <div class="form-group">
                    <input class="form-control" id="filterDatepicker" type="text" placeholder="Date" name="date" value="{{ !empty($date) ? Carbon\Carbon::parse($date)->format('d-m-Y') : ''}}"/>
                </div><!-- //form-group -->
            </li>
            <li>
                <button type="submit" class="btn btn-primary">Search</button>
            </li>
        </ul><!-- //ul -->
    </form><!-- //form -->
</div><!-- //search-form -->
<!-- //Search Form Div End -->
<script>
    $(document).ready(function () {
        // datepicker
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#filterDatepicker').datepicker({
            minDate: today,
            format: 'dd-mm-yyyy'
        });
    })
</script>