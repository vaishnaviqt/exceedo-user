@extends('expert.layouts.master')
@section('title')
Your Projects
@endsection
@section('class')
reportsBody
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <h2>Your Projects</h2> 
    </div><!-- //col-12 pageTitle -->
    @include('expert.projects.__project-common-header')
    <div class="col-12">
        @include('expert.projects.__common-menu')
        <!-- //Tab panes Start -->
        <div class="tab-content">
            <div class="tab-pane active" id="projects">
                <div class="row row_20">
                    @forelse($allReports as $report)
                    <div class="col-lg-6 col-md-12 col-12 card-deck">
                        <div class="reportBox border">
                            <div class="media">
                                @if($report->status == 1)
                                <img src="{{ asset('expert/images/project-map.png')}}" alt="Generic placeholder image">
                                @elseif($report->status == 2)
                                <form class="reportFileUploadForm-{{ $report->id }}">
                                    <div class="uploadReport">
                                        <div class="uploadBtn">
                                            <input type="hidden" name="id" value="{{ $report->id }}">
                                            <input type="file" class="custom-file-input customFile" name="file" data-id="{{ $report->id }}">
                                            <label for="customFile" class="custom-file-label">
                                                <img src="{{ asset('expert/images/svg-icons/upload-icon.svg')}}">
                                            </label>
                                        </div><!--//uploadBtn -->                                                            
                                        <span>Upload Report<br>If Ready</span>
                                    </div><!--//uploadReport -->
                                </form>
                                @endif

                                <div class="media-body">
                                    <h5 class="mt-0">{{ $report->size_of_land }} {{ __('Acres') }} {{ $report->land_name }}</h5>
                                    <span>{{ isset($report->user->mobile) ? $report->user->mobile : '' }} |</span> <span>{{ isset($report->user->email) ? $report->user->email : '' }}</span>
                                    <label class="landTitle">{{ isset($report->valueAdded->service_name) ? $report->valueAdded->service_name : '' }}</label>
                                    <label class="landLocation">{{ $report->address }}, {{ $report->city }}, {{ isset($report->state->name) ? $report->state->name : '' }}</label>
                                    <div class="expartUpdate">
                                        <h6>
                                            @if($report->status == 2)
                                            InProgress by : <span>{{ isset($report->expert->name) ? $report->expert->name : '' }}</span>
                                            @elseif($report->status == 1)
                                            Delivered by : <span>{{ isset($report->expert->name) ? $report->expert->name : '' }}</span>
                                            @elseif($report->status == 0)
                                            Pending by : <span>{{ isset($report->expert->name) ? $report->expert->name : '' }}</span>
                                            @endif
                                            <span class="daysAgo">{{ $report->created_at->diffForHumans() }}</span>
                                        </h6>
                                        @php
                                        $pdf = '';
                                        if(!empty($report->reportDocument)){
                                        if(Storage::disk('landReport')->exists(str_replace('landReport', '', $report->reportDocument->title)))
                                        {
                                        $pdf = Storage::disk('landReport')->url(str_replace('landReport/', '', $report->reportDocument->title));
                                        }
                                        }
                                        @endphp
                                        <a href="{{ $pdf }}" class="btn btn-primary" target="_blank">Download PDF</a>
                                    </div><!-- //expartUpdate-->
                                </div><!-- //media-body -->
                            </div><!--//media -->
                           @if($report->status == 2)
                            @if(isset($report->reportDocument->id) && $report->reportDocument->report_by == 1)
                            <blockquote class="blockquote blockquote-reverse">
                                <p class="text-danger">Admin  submitted feedback about report</p>

                                <p class="mb-0">{{ $report->reportDocument->feedback ?? '' }}</p>
                            </blockquote>
                            @else
                            <blockquote class="blockquote blockquote-reverse uploadYoourReport">
                                <p class="mb-0"><img src="{{ asset('expert/images/svg-icons/clock-icon.svg')}}" alt="clock"> Upload your Report to get your feedback</p>
                            </blockquote>
                            @endif
                            @endif
                        </div><!-- //reportBox -->                                                
                    </div><!-- //col-md-6 col-12 -->
                    @empty
                    <div class="col-lg-12 col-md-12 col-12 card-deck">
                        <div class="reportBox border text-center">
                            No Reports Available
                        </div>
                    </div>
                    @endforelse

                </div><!-- /.row -->
 
            </div><!--//tab-pane -->
        </div><!-- //Tab panes End -->
    </div><!-- //col-12 -->
</div><!-- //row -->                
@endsection   
@section('modal')
@endsection   
