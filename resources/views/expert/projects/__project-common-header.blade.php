<style>

    .loader{
        position: fixed;
        top:0;
        left:0;
        right: 0;
        bottom:0;
        display:flex;
        align-items:center;
        justify-content:center;
        z-index: 9999;
        background: #ffffffa6;
    }
    .loader img{
        width:80px;
    }
</style>
<div class="loader d-none">
    <img src="{{ asset('images/Spin-1s-200px.gif') }}">
</div>
<div class="col-12">
    <div class="countCards">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('expert/images/svg-icons/admin-reports-icon.svg') }}" alt="all reports" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ landReportCount() }}</strong></p>
                        <p>All Reports</p>
                    </div>
                </article><!--//countCardArt-->
            </div><!--//col-lg-3-->

            <div class="col-lg-3 col-md-6">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('expert/images/svg-icons/delivered-Reports-icon.svg') }}" alt="Delivered Reports" />                                    
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ landReportCount(1) }}</strong></p>
                        <p>Delivered Reports</p>
                    </div>
                </article><!--//countCardArt-->
            </div><!--//col-lg-3-->

            <div class="col-lg-3 col-md-6">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('expert/images/svg-icons/inprogress-reports-icon.svg') }}" alt="Inprogress Reports" />                                    
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ landReportCount(2) }}</strong></p>
                        <p>Inprogress Reports</p>
                    </div>
                </article><!--//countCardArt-->
            </div><!--//col-lg-3-->

            <div class="col-lg-3 col-md-6">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('expert/images/svg-icons/request-reports-icon.svg') }}" alt="Pending Reports" />                                    
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ landReportCount('pending') }}</strong></p>
                        <p>Pending Reports</p>
                    </div>
                </article><!--//countCardArt-->
            </div><!--//col-lg-3-->                        

        </div><!--//row-->
    </div><!--//countCards-->
</div><!-- //col-12 -->
@php
$inquiries = pendingServiceInquiry();
$reports = expertPendingProject();
@endphp
<!-- Projects Notification-->
@if(count($inquiries) || count($reports))
<div class="col-12">
    <h3 class="page-sub-title">Projects Notification</h3><!-- //page-sub-title-->

    <div class="projectNotification">
        <div class="row">
            @if(count($reports))
            @foreach($reports as $report)
            <div class="col-md-6 notification">
                <div class="notificationCard">
                    <div class="landAddress">
                        <h4 class="card-title">{{ $report->size_of_land }} {{ __('Acres') }} {{ $report->land_name }}</h4>
                        <label class="landTitle">Land Title Search</label>
                        <label class="landLocation">{{ $report->address }}, {{ $report->city }}, {{ isset($report->state->name) ? $report->state->name : '' }}</label>
                        <label>Assigned by : <span>Admin Swati</span></label>
                    </div><!--//landAddress-->

                    <div class="acceptClose">
                        <a href="javascript:void(0)" class="acceptedCard acceptedCardReport" data-id="{{ $report->id }}"></a>
                        <a href="javascript:void(0)" class="closeCard closeCardReport" data-id="{{ $report->id }}"></a>
                        <span class="card-text align-self-end">{{ $report->created_at->diffForHumans() }}</span>
                    </div><!-- //acceptClose-->
                </div><!-- //notificationCard-->
            </div><!-- //col-md-6 -->
            @endforeach
            @endif
            @if(count($inquiries))
            @foreach($inquiries as $inquiry)
            <div class="col-md-6 notification">
                <div class="notificationCard">
                    <div class="landAddress">
                        <h4 class="card-title">{{ $inquiry->name }}</h4>
                        <label class="landTitle">{{ $inquiry->email }}</label>
                        <label class="landLocation">{{$inquiry->mobile}}</label>
                        <label>Assigned by : <span>Admin Swati</span></label>
                    </div><!--//landAddress-->

                    <div class="acceptClose">
                        <a href="javascript:void(0)" class="acceptedCard acceptedCardInqury" data-id="{{ $inquiry->id }}"></a>
                        <a href="javascript:void(0)" class="closeCard closeCardInquiry" data-id="{{ $inquiry->id }}"></a>
                        <span class="card-text align-self-end">{{ $inquiry->created_at->diffForHumans() }}</span>
                    </div><!-- //acceptClose-->
                </div><!-- //notificationCard-->
            </div><!-- //col-md-6 -->
            @endforeach
            @endif

            <div class="col-md-6" id="accepted-box">
                <div class="notificationCard">
                    <div class="accepted-box">
                        <p>You have accepted the Land Title Search Projects for 21 Acres Land Town</p>
                    </div>
                    <div class="assigned-box">
                        <label>Assigned by : <span>Admin Swati</span></label>
                        <span class="card-text align-self-end">2 days ago</span>
                    </div>
                </div><!-- //notificationCard-->
            </div><!-- //col-md-6 -->
        </div><!--//row-->
    </div><!-- //projectNotification-->
</div><!-- //col-12 Projects Notification -->
@endif
<script>
    $(document).ready(function () {
        $('.acceptedCardInqury').on('click', function () {
            $('.loader').toggleClass('d-none');
            var id = $(this).data('id');
            var card = $(this);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "get",
                url: "accept-enquiry/" + id,
                success: function (data) {
                    $('.loader').toggleClass('d-none');
                    card.parents('.notification').css("display", "none");
                    swal("", "Report Confirm successfully!", "success");
                },
                error: function () {
                    $('.loader').toggleClass('d-none');
                    swal("", "Something went wrong!", "error");
                }
            });
        });
         $('.closeCardInquiry').on('click', function () {
            $('.loader').toggleClass('d-none');
            var id = $(this).data('id');
            var card = $(this);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "get",
                url: "cancel-enquiry/" + id,
                success: function (data) {
                    $('.loader').toggleClass('d-none');
                    card.parents('.notification').css("display", "none");
                    swal("", "Enquiry Cancel successfully!", "success");
                },
                error: function () {
                    $('.loader').toggleClass('d-none');
                    swal("", "Something went wrong!", "error");
                    return false;
                }
            });
        });
        
        $('.closeCardReport').on('click', function () {
            $('.loader').toggleClass('d-none');
            var id = $(this).data('id');
            var card = $(this);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "get",
                url: "cancel-project/" + id,
                success: function (data) {
                    $('.loader').toggleClass('d-none');
                    card.parents('.notification').css("display", "none");
                    swal("", "Project Cancel successfully!", "success");
                },
                error: function () {
                    $('.loader').toggleClass('d-none');
                    swal("", "Something went wrong!", "error");
                    return false;
                }
            });
        });
        $('.acceptedCardReport').on('click', function () {
            $('.loader').toggleClass('d-none');
            var id = $(this).data('id');
            var card = $(this);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "get",
                url: "accept-project/" + id,
                success: function (data) {
                    $('.loader').toggleClass('d-none');
                    card.parents('.notification').css("display", "none");
                    swal("", "Project Confirm successfully!", "success");
                },
                error: function () {
                    $('.loader').toggleClass('d-none');
                    swal("", "Something went wrong!", "error");
                }
            });
        });
        $('.customFile').on('change', function () {
            $('.loader').toggleClass('d-none');
            var id = $(this).data('id');
            var document = $(this).data('document');
            var fd = new FormData();
            fd.append('id', id);
            var files = $(this)[0].files;
            if (files.length > 0) {
                fd.append('file', files[0]);
            }
            console.log(fd);
            var card = $(this);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "post",
                url: "project-report-upload",
                processData: false,
                contentType: false,
                data: fd,
                success: function (data) {
                    $('.loader').toggleClass('d-none');

                    swal("", "Report Document uploaded successfully", "success").then(function () {
                        location.reload();
                    });
                },
                error: function () {
                    $('.loader').toggleClass('d-none');
                    swal("", "Something went wrong!", "error");
                }
            });
        });
    });
</script>