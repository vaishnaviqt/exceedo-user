@extends('expert.layouts.master')
@section('title')
Your Projects
@endsection
@section('class')
reportsBody
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <h2>Your Projects</h2> 
    </div><!-- //col-12 pageTitle -->
    @include('expert.projects.__project-common-header')
    <div class="col-12">
        <!-- //Nav tabs Start -->
        <ul class="nav nav-tabs">
            <li class="nav-item">
                @php
                $report = 0;
                if(isset($allReports)){
                $report = $allReports->total();
                }else{
                $report = landReportCount();
                }
                @endphp
                <a class="nav-link @if(request()->is('expert/projects')) {{ __('active') }} @endif"  href="{{ url('expert/projects') }}">Projects <span>({{ $report }})</span></a>
            </li>
            <li class="nav-item">
                @php
                $delivered = 0;
                if(isset($deliveredReports)){
                $delivered = $deliveredReports->total();
                }else{
                $delivered = landReportCount(1);
                }
                @endphp
                <a class="nav-link @if(request()->is('expert/delivered-projects')) {{ __('active') }} @endif" href="{{ url('expert/delivered-projects') }}">Delivered Projects <span>({{ $delivered }})</span></a>
            </li>
            <li class="nav-item">
                @php
                $inprogress = 0;
                if(isset($inprogressReports)){
                $inprogress = $inprogressReports->total();
                }else{
                $inprogress = landReportCount(2);
                }
                @endphp
                <a class="nav-link @if(request()->is('expert/inprogress-projects')) {{ __('active') }} @endif" href="{{ url('expert/inprogress-projects') }}">Inprogress Projects <span>({{ $inprogress }})</span></a>
            </li>
            <li class="nav-item">
                @php
                $pending = 0;
                if(isset($pendingReports)){
                $pending = $pendingReports->total();
                }else{
                $pending = landReportCount('pending');
                }
                @endphp
                <a class="nav-link @if(request()->is('expert/pending-projects')) {{ __('active') }} @endif" href="{{ url('expert/pending-projects') }}">Pending Projects <span>({{ $pending }})</span></a>
            </li>
            <li class="nav-item">
                @php
                $pending = 0;
                if(isset($pendingReports)){
                $pending = $pendingReports->total();
                }else{
                $pending = landReportCount('pending');
                }
                @endphp
                <a class="nav-link @if(request()->is('expert/service-enquiry')) {{ __('active') }} @endif" href="{{ url('expert/service-enquiry') }}">{{ userValueAddedService() }} <span>({{ $pending }})</span></a>
            </li>
        </ul>
        <!-- //Nav tabs End -->

        <!-- //Search Form Div Start -->
        <div class="searchForm">
            <a href="javascript:void(0);" class="open-mobile-search">Search</a>
            @php
            $url ='';
            if(request()->is('expert/projects')){
            $url =url('expert/projects');
            }else if(request()->is('expert/delivered-projects')){
            $url =url('expert/delivered-projects');
            }else if(request()->is('expert/inprogress-projects')){
            $url =url('expert/inprogress-projects');
            }else if(request()->is('expert/pending-projects')){
            $url =url('expert/pending-projects');
            }
            @endphp
            <form action="{{ $url }}" method="post">
                @csrf
                <span>Search</span>
                <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('expert/images/svg-icons/close-icon.svg') }}" alt=""></a>
                <ul class="d-flex flex-wrap">
                    <li>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name" id="name" name="name" value="{{ $name }}">
                        </div><!-- //form-group -->
                    </li>

                    <li>
                        <div class="form-group">
                            <input class="form-control" id="filterDatepicker" type="text" placeholder="Date" name="date" value="{{ !empty($date) ? Carbon\Carbon::parse($date)->format('d-m-Y') : ''}}"/>
                        </div><!-- //form-group -->
                    </li>
                    <li>
                        <button type="submit" class="btn btn-primary">Search</button>
                    </li>
                </ul><!-- //ul -->
            </form><!-- //form -->
        </div><!-- //search-form -->
        <!-- //Search Form Div End -->

        <div class="customTable border">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Description</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($enquiries as $enquiry)
                    <tr>
                        <td scope="row">
                            <span class="m_tableTitle">Name</span>
                            {{ isset($enquiry->name) ?  ucwords($enquiry->name) : ''}}
                        </td>
                        <td>
                            <span class="m_tableTitle">Email</span>
                            {{ isset($enquiry->email) ?  ($enquiry->email) : ''}}
                        </td>
                        <td>
                            <span class="m_tableTitle">Mobile</span>
                            {{ isset($enquiry->mobile) ?  ucwords($enquiry->mobile) : ''}}
                        </td>
                        <td>
                            <span class="m_tableTitle">Description</span>
                            {{ isset($enquiry->description) ?  ucwords($enquiry->description) : ''}}
                        </td>
                        <td>
                            <span class="m_tableTitle">Created At</span>
                            {{ Carbon\Carbon::parse($enquiry->created_at)->format('d M, Y') }}
                        </td>
                       
                    </tr>
                    @empty
                    <tr>
                        <td scope="row" colspan="4">No data available</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            <div class="col-12">
                {{ $enquiries->links() }}
            </div><!-- //col-12 pagination -->
        </div><!--//payment-table-desktop-->
    </div><!-- //col-12 -->

</div><!-- //row --> 
<script>
    $(document).ready(function () {
        // datepicker
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#filterDatepicker').datepicker({
            minDate: today,
            format: 'dd-mm-yyyy'
        });
    })
</script>
@endsection   
@section('modal')
@endsection   
