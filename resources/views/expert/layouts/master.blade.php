<!DOCTYPE html>
<html lang="en">
    <head>
        @include('expert.partials.head')
        <style>

            .loader{
                position: fixed;
                top:0;
                left:0;
                right: 0;
                bottom:0;
                display:flex;
                align-items:center;
                justify-content:center;
                z-index: 9999;
                background: #ffffffa6;
            }
            .loader img{
                width:80px;
            }
        </style>
    </head>
    <body>  
        <div class="loader d-none">
            <img src="{{ asset('images/Spin-1s-200px.gif') }}">
        </div>
        <div class="wrapper">
            <div class="sidebar">
                @include('expert.partials.sidebar')
            </div><!--//sidebar--> 

            <div class="main">
                <header class="header"> 
                    @include('expert.partials.header')
                </header><!--//header-->

                <section class="pageContainer  @yield('class')">
                    @yield('content')

                </section><!--//pageContainer-->

                <footer class="footer">
                    @include('expert.partials.footer')
                </footer>    
            </div><!--//main-->
        </div><!--//wrapper--> 

        @yield('modal')
        @include('expert.partials.javascripts')

        @section('custom-script')
        @show
        <script>
            // Inline popups
            $('#inline-popups').magnificPopup({
                delegate: 'a',
                removalDelay: 500,
                callbacks: {
                    beforeOpen: function () {
                        this.st.mainClass = this.st.el.attr('data-effect');
                    }
                },
                midClick: true
            });

            // Multiple-Checkboxes
            $(document).ready(function () {
                $('#multiple-checkboxes, #multiple-checkboxes2').multiselect({
                    includeSelectAllOption: false,
                });
            });

            $('.secondary-Admin-show').click(function () {
                $('.secondary-Admin-edit').css('display', 'flex');
                $('ul.secondary-Admin-show').css('display', 'none');
            });
            $('.save-secondary-Admin').click(function () {
                $('.secondary-Admin-edit').css('display', 'none');
                $('ul.secondary-Admin-show').css('display', 'flex');
            });
        </script>
        <script>
            $(document).ready(function () {
                // datepicker
                var today, datepicker;
                today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                datepicker = $('#filterDatepicker').datepicker({
                    minDate: today,
                    format: 'dd-mm-yyyy'
                });
            })
        </script>


    </body>
</html>