@extends('expert.layouts.master')
@section('title')
Rating & Reviews
@endsection
@section('class')
 paymentsBody
@endsection
@section('content')

                @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<h2>{{ __('Rating & Reviews') }}</h2> 
                <!-- //Search Form Div Start -->
                    <div class="searchForm">
    <a href="javascript:void(0);" class="open-mobile-search">{{ __('Search') }}</a>                                   
    <form action="{{url('expert/reviews')}}" method="post">
        @csrf
        <span>{{ __('Search') }}</span>
                            <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('expert/images/svg-icons/close-icon.svg') }}" alt=""></a>
                            <ul class="d-flex flex-wrap">
                                <li>
                                    <div class="form-group">
                    <input class="form-control" id="datepicker" type="text" name="created_at" 
                           value="{{ !empty($startDate) ? Carbon\Carbon::parse($startDate)->format('d-m-Y') : '' }}" 
                           placeholder="Date"/>
                                    </div><!-- //form-group -->
                                </li>
                                <li>
                                    <div class="form-group">
                    <input type="text" class="form-control" placeholder="Customer Name" name="customerName" id="customerName" value="{{ $customerName }}">
                                    </div><!-- //form-group -->
                                </li>
                                <li>
                                    <div class="form-group">
                    <select class="form-control" id="ratingType" name="ratingType">
                        <option value="">{{ __('Rating Star') }}</option>
                        <option value="1" @if($ratingType == 1) {{ __('selected') }} @endif>
                                1 Star </option>
                        <option value="2" @if($ratingType == 2) {{ __('selected') }} @endif>
                                2 Star</option>
                        <option value="3" @if($ratingType == 3) {{ __('selected') }} @endif>
                                3 Star</option>
                        <option value="4" @if($ratingType == 4) {{ __('selected') }} @endif>
                                4 Star</option>
                        <option value="5" @if($ratingType == 5) {{ __('selected') }} @endif>
                                5 Star</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li>
                                    <div class="form-group">
                    <select class="form-control" id="serviceType" name="serviceType">
                        <option value="">{{ __('Lead Suggest') }}</option>
                        @if(count($services))
                        @foreach($services as $service)
                        @php
                        $selected = '';
                        if($service->id == $serviceType){
                        $selected = 'selected';
                        }
                        @endphp
                        <option value="{{ $service->id }}" {{ $selected }}>{{ $service->service_name }}</option>
                        @endforeach
                        @endif
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li>
                <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
                                </li>
                            </ul><!-- //ul -->
                        </form><!-- //form -->
                    </div><!-- //search-form -->
                <!-- //Search Form Div End -->

                <div class="rating-reviews">
    @forelse($reviews as $review)
                    <div class="comments-review">
                        <div class="d-flex justify-content-start flex-lg-nowrap flex-wrap mb-2">
            @php
            $path = asset('broker/images/profile-pic.png');
            if(Storage::disk('expert')->exists(str_replace('expert_image', '', $review->photo)))
            {
            $path = Storage::disk('expert')->url(str_replace('expert_image/', '', $review->photo));
            }
            @endphp
            <div><img class="d-flex align-self-start mr-3" src="{{ $path }}" alt="No Image"></div>
                            <div>
                <h5>{{ ucwords($review->username) }}</h5>
                <span>{{ $review->email }}</span>
                            </div>
                            <div>
                <div class="placeholder" style="color: lightgray;">
                    {!! userRatings($review->rating) !!}
                            </div>
                <label><span>{{ __('Lead Suggest') }} :</span> {{ ucwords($review->service_name) }} </label>                                            
                            </div>
                            <div>
                <span>{{ __('Appointment Date and Time') }}</span>
                <label>{{ isset($review->meeting_at) ? date('d M Y, H:i a', strtotime($review->meeting_at)) : '' }}</label>
                            </div>
                        </div><!--//d-flex justify-content-start flex-lg-nowrap flex-wrap-->
        <div><span>{{ __('Feedback') }} : </span>{{ ucfirst($review->feedback) }}</div>
                    </div><!--//comments-review-->
    @empty
    <div class="card-body2">         
        <div class="no-content-msg">   
            {{ __('No Data Found') }}
        </div>       
    </div>
    @endforelse
    <div class="col-12">
        <ul class="pagination justify-content-center">
          {{ $reviews->links() }}
        </ul>
    </div><!--//payment-table-desktop-->
                </div><!--//rating-reviews-->

                 @endsection   
@section('modal')
@endsection   
