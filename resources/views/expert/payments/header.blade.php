<!-- //Nav tabs Start -->
<div class="reportNavBar flex-row">
	<div class="reportTabs">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link @if(request()->is('expert/payments') ) active @endif"  href="{{ url('expert/payments') }}">Received Payments <span></span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->is('expert/made-payments')) active @endif"  href="{{ url('expert/made-payments') }}">Made Payments <span></span></a>
            </li>
        </ul>
	</div>
</div><!--//reportNavBar-->

<!-- //Nav tabs End -->

