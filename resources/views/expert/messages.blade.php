@extends('expert.layouts.master')
@section('title')
Message to Admin
@endsection
@section('class')
message-admin
@endsection
@section('content')
@include('flash-message')
<div>
    
    <div class="pageTitle d-flex align-items-center">
        <h2>Message to Admin</h2> 
    </div><!--//pageTitle-->
  
    <form action="{{ url('expert/expert-messages-send') }}" method="post">
        @csrf
        <div class="form-group mb-4">
            <textarea class="form-control @error('message') is-invalid @enderror" placeholder="Type your Message here" id="message" name="message" rows="8" required>{{ old('message') }}</textarea>
            <button type="submit" class="btn btn-primary">Send</button>
            @error('message')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div><!-- //form-group-->
    </form> 

    
    <div class="pageTitle d-flex align-items-center">
        <h2>All Queries</h2>
        
        <div class="sortDropdown">
            <label>Show by</label>
            <div class="dropdown custom-dropdown">
                <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle">
                    <span class="selected">{{ !empty($showBy) ? ucwords($showBy) :'Month' }}</span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a  class="messageShow" data-filter="week" href="javascript:;">Week</a></li>
                    <li><a class="messageShow" data-filter="month" href="javascript:;">Month</a></li>
                    <li><a  class="messageShow" data-filter="year" href="javascript:;">Year</a></li>
                </ul>
            </div><!-- //dropdown --> 
            <form id="messageShowBy" action="{{ url('expert/messages') }}" method="post">
                @csrf
                <input type="hidden" id="filterColumn" name="show_by" value="">
            </form>
        </div><!--//sortDropdown-->
    </div><!--//pageTitle-->
  
    <div class="rating-reviews">
        @forelse($messages as $message)
        <div class="message-box d-md-flex">
            <div>
                <img src="{{ asset('expert/images/svg-icons/message-icon.svg') }}" alt="">
            </div><!-- //div-->
            <div style="width: 70%">
                <p><span>Message : </span> {{ $message->message }}</p>
            </div><!-- //div-->
            <div>
                <span class="day-count">{{ Carbon\Carbon::parse($message->created_at)->diffForHumans() }}</span>
            </div><!-- //div-->
        </div><!--//message-box  d-md-flex-->
        @empty
        <div class="message-box d-md-flex">
            <div>
                <p>No Message Available</p>
            </div><!-- //div-->
        </div>
        @endforelse
    </div><!-- //rating-reviews -->
    {{ $messages->links() }} 
    
</div><!-- //row -->    
<script>
    $(document).ready(function () {
//        $('.messageShow').on('click', function () {
//            var value = $(this).data('filter');
//            $('#filterColumn').val(value);
//            $('#messageShowBy').submit();
//        });
    });
</script>
@endsection   
@section('modal')
@endsection   
