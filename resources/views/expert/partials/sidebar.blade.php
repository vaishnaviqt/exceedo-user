            <a href="javascript:void(0);" class="hamburgerMenu"></a>
            <div class="sidebarLogo">
                <a href="{{ route('expert.appointments') }}">
                    <img src="{{ asset('expert/images/logo.svg') }}" alt="Know your Land" />
                    <span>Know your Land</span>
                </a>
            </div><!--//sidebarLogo-->

            <div class="sideNavigation customScrollbar">
                <ul>
                    <li><a href="{{ route('expert.appointments') }}" class="nav_appoint @if( Request::route()->getName() == 'expert.appointments' ) active @endif">Appointments</a></li> 
                    <li><a href="{{ route('expert.projects') }}" class="nav_reports @if( Request::route()->getName() == 'expert.projects' ) active @endif">Projects</a></li>
                    <li><a href="{{ route('expert.your-land-listing') }}" class="nav_reports @if( Request::route()->getName() == 'expert.your-land-listing' ) active @endif">Land</a></li>
                    <li><a href="{{ route('expert.payments') }}" class="nav_payments @if( Request::route()->getName() == 'expert.payments' ) active @endif">Payments</a></li>
                    <li><a href="{{ route('expert.reviews') }}" class="nav_rating @if( Request::route()->getName() == 'expert.reviews' ) active @endif">Rating & Reviews</a></li>
                    <li><a href="{{ route('expert.messages') }}" class="nav_msg @if( Request::route()->getName() == 'expert.messages' ) active @endif">Message to Admin</a></li>
                    <li><a href="{{ route('expert.profile') }}" class="nav_profile @if( Request::route()->getName() == 'expert.profile' ) active @endif">Your Profile</a></li>
                    <li><a href="{{ route('expert.expertlogout') }}" class="nav_logout">Logout</a></li>
                </ul>
            </div><!--//navigation-->



