<!--<div class="pageTitle">
    <h1>Your Land Listing</h1>
</div>//pageTitle-->
<style>

    div.pac-container.pac-logo {
        z-index: 99999999999 !important;
    }
</style>

<div class="countCards">
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('user/images/svg-icons/all-land-icon.svg') }}" alt="All Land Listing" />
                </div>
                <div class="countRepDetail">
                    <p><strong>{{ countLand('', auth()->user())}}</strong></p>
                    <p>All Land Listing</p>
                </div>
            </article><!--//countCardArt-->
        </div><!--//col-lg-3-->

        <div class="col-lg-3 col-md-6">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('user/images/svg-icons/active-land-icon.svg') }}" alt="Active Land Listing" />                                    
                </div>
                <div class="countRepDetail">
                    <p><strong>{{countLand(1, auth()->user())}}</strong></p>
                    <p>Active Land Listing</p>
                </div>
            </article><!--//countCardArt-->
        </div><!--//col-lg-3-->

        <div class="col-lg-3 col-md-6">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('user/images/svg-icons/inactive-land-icon.svg') }}" alt="Inactive Land Listing" />                                    
                </div>
                <div class="countRepDetail">
                    <p><strong>{{ countLand('inactive', auth()->user())}}</strong></p>
                    <p>Inactive Land Listing</p>
                </div>
            </article><!--//countCardArt-->
        </div><!--//col-lg-3-->

        <div class="col-lg-3 col-md-6">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('user/images/svg-icons/delete-land-icon.svg') }}" alt="Deleted Land Listing" />                                    
                </div>
                <div class="countRepDetail">
                    <p><strong>{{ countLand(2, auth()->user())}}</strong></p>
                    <p>Deleted Land Listing</p>
                </div>
            </article><!--//countCardArt-->
        </div><!--//col-lg-3-->                        
    </div><!--//row-->
</div><!--//countCards-->

<div class="pageTitle d-flex align-items-center">
    <h2>Your Land Listing</h2>

    <div class="sortDropdown">
        <label>Show by</label>
        <div class="dropdown custom-dropdown">
            <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle">
                <span class="selected">{{ $filterDate }}</span><span class="caret"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="#" class="expertPaginate" data-value="week">Week</a></li>
                <li><a href="#" class="expertPaginate" data-value="month">Month</a></li>
                <li><a href="#" class="expertPaginate" data-value="year">Year</a></li>

            </ul>
        </div><!-- //dropdown --> 
        <form method="post" id="paginateSubmitForm">
            @csrf
            <input type="hidden" name="day" id="paginateHiddenFiled">
        </form>
    </div><!--//sortDropdown-->
</div><!--//pageTitle-->

<div class="exceedoTabs">
    <div class="reportNavBar">                        
        <div class="genReportLink">
            <a href="javascript:void(0);" class="userlisting" data-toggle="modal" data-target="#addLandListing">Add Land Listing</a>
        </div>
        <div class="reportTabs">
            <ul class="nav nav-tabs">
                <li>
                    @php
                    $land = 0;
                    if(isset($allLand)){
                    $land = $allLand->total();
                    }else{
                    $land = countLand('', auth()->user());
                    }
                    @endphp
                    <a class="@if( Request::route()->getName() == 'expert.your-land-listing' ) active @endif" href="{{ route('expert.your-land-listing') }}">All Land Listing ({{ $land }})</a>
                </li>
                <li>
                    @php
                    $active = 0;
                    if(isset($activeLand)){
                    $active = $activeLand->total();
                    }else{
                    $active = countLand(1, auth()->user());
                    }
                    @endphp
                    <a class="@if( Request::route()->getName() == 'expert.your-land-listing-active' ) active @endif" href="{{ route('expert.your-land-listing-active') }}">Active Land Listing ({{ $active}})</a>
                </li>
                <li>
                    @php
                    $inactive = 0;
                    if(isset($inactiveLands)){
                    $inactive = $inactiveLands->total();
                    }else{
                    $inactive = countLand('inactive', auth()->user());
                    }
                    @endphp
                    <a class="@if( Request::route()->getName() == 'expert.your-land-listing-inactive' ) active @endif" href="{{ route('expert.your-land-listing-inactive') }}">Inactive land listing ({{ $inactive }})</a>
                </li>
                <li>
                    @php
                    $requested = 0;
                    if(isset($requestLands)){
                    $requested = $requestLands->total();
                    }else{
                    $requested = countLand('deleted', auth()->user());
                    }
                    @endphp
                    <a class="@if( Request::route()->getName() == 'expert.your-land-listing-deleted' ) active @endif" href="{{ route('expert.your-land-listing-deleted') }}">Deleted land listing ({{ $requested }})</a>
                </li>
            </ul>
        </div>
    </div><!--//reportNavBar-->
</div>

<!-- Edit Profile model -->
<div class="modal fade w-600" id="addLandListing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered addLandListing" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="popupForm">
                    <form id="propertyStoreForm">
                        <div class="popupHeading">
                            <h2>Add Land Listing</h2>
                        </div>

                        <div class="addLand">
                            <div>
                                <div class="form-group">
                                    <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" value="">
                                    <input type="hidden" name="razorpay_order_id" id="razorpay_order_id" value="">
                                    <input type="hidden" name="razorpay_signature" id="razorpay_signature" value="">
                                    <input class="form-control" id="order_id" type="hidden" value="" name="order_id"/>
                                    <input class="form-control" id="user_id" type="hidden" value="{{ auth()->user()->id ?? '' }}" name="user_id"/>
                                    <input type="hidden" name="amount" value="400" id="amount">
                                    <label for="Name">Land Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="land_name" placeholder="Enter Land Name" id="land_Name">
                                </div>
                            </div>
                            <div>
                                <div class="form-group land_price">
                                    <label for="number">Land Price <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="land_price" placeholder="Enter Price" id="landPrice">
                                    <select class="form-control" id="reportType" name="price_unit">
                                        <option value="lakh">Lakh</option>
                                        <option value="cr">Cr.</option>
                                    </select>
                                </div>
                            </div><!--//div-->
                            <div class="flex-100">
                                <div class="form-group">
                                    <label for="user_address">Address <span class="text-danger">*</span></label>
                                    <input type="hidden" class="form-control" name="latitute" id="latitute">
                                    <input type="hidden" class="form-control" name="longitude" id="longitude">
                                    <input type="text" class="form-control" placeholder="" name="address" id="user_address">
                                </div>
                            </div><!--//div-->
                            <div>
                                <div class="form-group">
                                    <div class="selectRadio w-100">
                                        <label>In both of these, Which one you are ? <span class="text-danger">*</span></label>
                                        <div class="d-flex flex-row is_corporate_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="company" name="is_corporate" value="company">
                                                <label class="custom-control-label" for="company">Company</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="individual" name="is_corporate" value="individual">
                                                <label class="custom-control-label" for="individual">Individual</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--//div-->

                            <div class="gstInputFiled">
                                <div class="form-group">
                                    <label for="user_gst">GST No (if Applicable)<span class="text-danger">*</span></label>
                                    <input class="form-control" name="user_gst" id="user_gst" type="text" placeholder=" ">
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label for="user_pan">Pan No <span class="text-danger">*</span></label>
                                    <input class="form-control" name="user_pan" id="user_pan" type="text" placeholder=" ">
                                </div>
                            </div>

                            <div>
                                <div class="form-group">
                                    <div class="selectRadio w-100">
                                        <label>
                                            Do you want to attend the KYL as the role and exclusive Channel Partner to transact your land ?
                                            <span class="text-danger">*</span>
                                        </label>
                                        <div class="d-flex flex-row exclusive_channel_partner_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="yes" name="exclusive_channel_partner" value="yes">
                                                <label class="custom-control-label" for="yes">Yes</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="no" name="exclusive_channel_partner" value="no">
                                                <label class="custom-control-label" for="no">No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--//div-->
                            <div>
                                <div class="form-group">
                                    <div class="selectRadio w-100">
                                        <label>
                                            Then User is promoted to sign exclusive mandate with KYL <span class="text-danger">*</span>
                                        </label>
                                        <div class="d-flex flex-row exclusive_mandate_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="agree" name="exclusive_mandate" value="agree">
                                                <label class="custom-control-label" for="agree">I agree</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="disagree" name="exclusive_mandate" value="disagree">
                                                <label class="custom-control-label" for="disagree">I disagree</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--//div-->
                            <div>
                                <div class="form-group">
                                    <div class="selectRadio w-100">
                                        <label>
                                            Do you want to visit KYL Team to your Land to get verified?
                                            <span class="text-danger">*</span>
                                        </label>

                                        <div class="d-flex flex-row land_to_get_verified_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="yes2" name="land_to_get_verified" value="yes">
                                                <label class="custom-control-label" for="yes2">Yes</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="no2" name="land_to_get_verified" value="no">
                                                <label class="custom-control-label" for="no2">No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--//div-->
                            <div>
                                <div class="form-group">
                                    <div class="selectRadio w-100">
                                        <label>What would you do with your property ? <span class="text-danger">*</span></label>

                                        <div class="d-flex flex-row business_type_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="sale" name="business_type" value="sale">
                                                <label class="custom-control-label" for="sale">Sale</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="collaboration" name="business_type" value="collaboration">
                                                <label class="custom-control-label" for="collaboration">Collaboration</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="lease" name="business_type" value="lease">
                                                <label class="custom-control-label" for="lease">Lease</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--//div-->


                            <div class="flex-100 image_error">
                                <div class="uploadPhoto">
                                    <input type="file" id="image" name="image">
                                    <h4>Upload Photo or Just drag and drop</h4>
                                </div><!--//uploadPhoto-->
                            </div><!--//div-->
                            <div class="flex-100">
                                <div class="uploadImgSection preview-images-zone">
                                </div><!--//uploadImgSection-->
                            </div><!--//div-->

                        </div><!--//addLand-->

                        <div class="formBtn">
                            <button type="button" class="btn btn-primary propertyStoreButton">Add Listing</button>
                        </div> 
                    </form>
                </div><!--//popupForm--> 
            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
$('.expertPaginate').on('click', function (e) {
    e.preventDefault();
    var value = $(this).data('value');
    $('#paginateHiddenFiled').val(value);
    $('#paginateSubmitForm').submit();
    return false;
});
</script>
<script>
    //land detail store with ajax
    $(document).ready(function () {

        $('.userlisting').on('click', function () {
            $("#propertyStoreForm")[0].reset()
        });
        if ($('#image').length) {
            document.getElementById('image').addEventListener('change', readImage, false);
            //$(".preview-images-zone").sortable();
        }

        $('body').on('click', '.image-cancel', function () {
            let no = $(this).data('no');
            $(".preview-image.preview-show-" + no).remove();
            if ($(".uploadImgSection").find('.preview-image').length == 0) {
                $(".uploadImgSection").addClass('d-none');
            }
        });
        var num = 1;
        var tmp_files = new Array();
        function readImage() {
            if (window.File && window.FileList && window.FileReader) {

                var files = event.target.files; //FileList object
                //   console.log(files);

                var output = $(".uploadImgSection");
                for (let i = 0; i < files.length; i++) {
                    var file = files[i];
                    tmp_files.push(file);
                    // console.log(tmp_files)
                }

                //$("#other_images").val('');

                for (let j = 0; j < tmp_files.length; j++) {
                    var file_set = tmp_files[j];
                    var fileSize = files[j].size;
                    console.log(fileSize);
                    if (fileSize > 2000000) {
                        jQuery('#valid-img.invalid-feedback').html('The File size cannot be more than 2 MB.');
                        jQuery('#valid-img.invalid-feedback').delay(2000).fadeOut(1000);
                    } else {
                        var picReader = new FileReader();
                        picReader.addEventListener('load', function (event) {
                            var picFile = event.target;
                            var image = "<?php echo url('adminassets/images/svg-icons/gray-close-btn.svg') ?>";
                            $('.preview-image').remove();
                            var html = `
    <div class="preview-image preview-show-` + num + `">
     <a href="javascript:void(0);" class="close_div image-cancel" data-no="` + num + `">
<img src="` + image + `" alt="">
</a>
<img id="pro-img-` + num + `" src="` + picFile.result + `" alt="">
<input type="hidden" name="new_images" id="old-image` + j + `" value="` + picFile.result + `">
</div>
`;
                            output.append(html);
                            num = num + 1;
                            if (output.find('.preview-image').length > 0) {
                                output.removeClass('d-none');
                            }
                        });
                        picReader.readAsDataURL(file_set);
                    }

                }
                tmp_files = new Array();
            } else {
                console.log('Browser not support');
            }
        }
    });</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_map_key')}}&libraries=places&callback=initAutocomplete" async defer></script>
<script>
    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
                document.getElementById('user_address'));
        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
//    autocomplete.setFields(['address_component']);
        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener('place_changed', onPlaceChanged);
    }

    function onPlaceChanged() {
        var place = autocomplete.getPlace();
        document.getElementById('latitute').value = place.geometry.location.lat();
        document.getElementById('longitude').value = place.geometry.location.lng();
    }

</script>
<script>
    $(document).ready(function () {
        $(".propertyStoreButton").on('click', function (evt) {
            let myForm = document.getElementById('propertyStoreForm');
            let formData = new FormData(myForm);
            $('.loader').toggleClass('d-none');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: "store_property_validation",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('.loader').toggleClass('d-none');
                    if (data.order_id) {
                        $('#order_id').val(data.order_id);
                    }
                    var name = "{{ auth()->user()->name ?? '' }}";
                    var email = "{{ auth()->user()->email ?? '' }}";
                    var mobile = "{{ auth()->user()->mobile ?? '' }}";
                    var amount = $('#amount').val();
                    if (name == '') {
                        var name = $('#user_name').val();
                    }
                    if (email == '') {
                        var email = $('#user_email').val();
                    }
                    if (mobile == '') {
                        var mobile = $('#user_mobile').val();
                    }
                    var options = {
                        "key": "{{ config('app.razorpay_api_key') }}", // Enter the Key ID generated from the Dashboard
                        "amount": amount * 100, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                        "currency": "{{ config('app.currency') }}",
                        "name": "{{ config('app.account_name') }}",
                        "description": '',
                        "image": "{{ asset('images/logo-black.svg') }}",
                        "order_id": $('#order_id').val(), //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                        "handler": function (response) {
                            $('#razorpay_payment_id').val(response.razorpay_payment_id);
                            $('#razorpay_order_id').val(response.razorpay_order_id);
                            $('#razorpay_signature').val(response.razorpay_signature);
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            let myForm = document.getElementById('propertyStoreForm');
                            let formData = new FormData(myForm);
                            $('.loader').toggleClass('d-none');
                            $.ajax({
                                type: 'POST',
                                url: "user-property-store",
                                data: formData,
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function (data) {
                                    $('.loader').toggleClass('d-none');
                                    if (data.success) {
                                        $('#propertyStoreForm')[0].reset();
                                        swal({
                                            title: "Good job!",
                                            text: "Your request submitted successfully, please check your email",
                                            icon: "success",
                                        }).then((value) => {
                                            location.reload();
                                        });
                                    } else {
                                        swal({
                                            text: "Something went wrong!",
                                            icon: "error",
                                        });
                                    }
                                }

                            })
                        },
                        "prefill":
                                {
                                    "name": name,
                                    "email": email,
                                    "contact": mobile
                                },
//            "notes": {
//                "address": "Razorpay Corporate Office"
//            },
                        "theme": {
                            "color": "#3399cc"
                        }
                    };
                    var rzp1 = new Razorpay(options);
                    rzp1.on('payment.failed', function (response) {

                    });

                    rzp1.open();

                },
                error: function (errorResponse) {
//                console.log(errorResponse.responseJSON.errors);
                    $('.loader').toggleClass('d-none');
//                toastr.error(errorResponse.responseJSON.message);
                    $('.error-span').remove();
                    $.each(errorResponse.responseJSON.errors, function (field_name, error) {
                        if (field_name == "business_type") {
                            $('.business_type_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == "is_corporate") {
                            $('.is_corporate_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == "exclusive_channel_partner") {
                            $('.exclusive_channel_partner_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == "exclusive_mandate") {
                            $('.exclusive_mandate_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == "land_to_get_verified") {
                            $('.land_to_get_verified_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == "new_images") {
                            $('.image_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else {
                            $(document).find('[name=' + field_name + ']').parent().append('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        }
                    })
                }
            });

        });
    });
</script>