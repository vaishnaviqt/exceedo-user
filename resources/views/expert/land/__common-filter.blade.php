<div class="filterFormSection">
    <a href="javascript:void(0);" class="mobileFilterBtn">Search </a>                              
    <div class="filterForm">
        @php
        $url = "";
        if(request()->is('expert/your-land-listing')){
        $url = url('expert/your-land-listing');
        }elseif(request()->is('expert/your-land-listing-active')){
        $url = url('expert/your-land-listing-active');
        }elseif(request()->is('expert/your-land-listing-inactive')){
        $url = url('expert/your-land-listing-inactive');
        }elseif(request()->is('expert/your-land-listing-deleted')){
        $url = url('expert/your-land-listing-deleted');
        }
        @endphp
        <form action="{{ $url }}" method="POST">
            @csrf
            <h2>
                Search
                <a href="javascript:void(0);" class="m_filterCloseBtn"></a>
            </h2>
            <div class="row">
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Land Name" 
                                       value="{{$name ?? ''}}" id="landName" name="name"> 
                            </div>
                        </div><!--//col-lg-3-->

                        <div class="col-lg-3 col-md-6"> 
                            <div class="form-group">
                                <select class="form-control" id="filter_business_type" name="filter_business_type">
                                    <option value="">{{ __('Land Type') }}</option>
                                    <option value="sale" @if($businessType == 'sale') {{ __('selected') }} @endif>{{ __('Sale') }}</option>
                                    <option value="collaboration" @if($businessType == 'collaboration') {{ __('selected') }} @endif>{{ __('Collaboration') }}</option>
                                    <option value="lease" @if($businessType == 'lease') {{ __('selected') }} @endif>{{ __('Lease') }}</option>
                                </select> 
                            </div>
                        </div><!--//col-lg-3-->

                        <div class="col-lg-3 col-md-6">
                            <div class="form-group">
                                <input type="text" name="filter_address" class="form-control" placeholder="Land Area" 
                                       value="{{$address ?? ''}}" id="land-area"> 
                            </div>
                        </div><!--//col-lg-3-->

                        <div class="col-lg-3 col-md-6">
                            <div class="form-group">
                                <input class="form-control" id="filterDatepicker1" type="text" name="created_at" 
                                       value="{{ !empty($startDate) ? Carbon\Carbon::parse($startDate)->format('d-m-Y') : '' }}" placeholder="Date"/>
                            </div>
                        </div><!--//col-lg-3-->
                    </div><!--//row-->
                </div><!--//col-lg-10-->

                <div class="col-lg-2">
                    <button type="submit" class="btn btn-primary">Search </button>
                </div>
            </div><!--//row-->
        </form>
    </div><!--//filterForm-->
</div><!--filterFormSection-->
<script>
    $(document).ready(function () {
        // datepicker
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#filterDatepicker1').datepicker({
            maxDate: today,
            format: 'dd-mm-yyyy'
        });
    })
</script>