@extends('expert.layouts.master')
@section('title')
Land Listing
@endsection
@section('class')
@endsection
@section('content')

<h2>Deleted Land Listing</h2> 
@include('expert.land.header')
<div class="tab-content"> 
    @include('expert.land.__common-filter')
    <div class="reportsListing">
        <div class="row row_20">
            @forelse($deleteLands as $deleteLand)
            <div class="col-md-6">
                <article class="reportArticle">
                    <div class="repArtImg">
                        <img src="{{ asset('/staorage/'.$deleteLand->image) }}" />
                    </div>
                    <div class="repArtContent landArtCont"> 

                        <div class="row">
                            <div class="col-7">
                                <h2>{{ ucwords($deleteLand->land_name) }}<em class="featureIcon"></em></h2> 
                                <p>{{ $deleteLand->address }}</p>
                            </div>
                            <div class="col-5">
                                <div class="activeLand">
                                    <span class="badge badge-warning float-right mr-0">{{ __('Deleted') }}</span>
                                </div>
                            </div>
                        </div><!--//row-->

                        <div class="landCost">
                            <div class="row align-items-center">
                                <div class="col-lg-4">
                                    <div class="landArtPrice">₹ {{$deleteLand->max_price}} {{$deleteLand->price_unit}}</div>
                                </div> 
                            </div><!--//row-->
                        </div><!--//landCost-->

                       <div class="featFaciRow">
                            @if($deleteLand->wifi  == 1 || $deleteLand->parking == 1) 
                                    <div class="featFacilities">
                                        <span class="featFaTitle">Facilities :</span>
                                        @if($deleteLand->parking == 1)
                                        <span class="faci-icon parking-icon"></span>
                                        @endif
                                        <!--<span class="faci-icon noSmooking-icon"></span>-->
                                        @if($deleteLand->wifi == 1)
                                        <span class="faci-icon wifi-icon"></span>
                                        @endif
                                    </div> 
                                    @endif
                            <div class="landArtviews">
                                {{$deleteLand->created_at->diffForHumans()}} &bull; {{ $deleteLand->views ?? 0 }} views
                            </div>
                        </div><!--//featFacilities-->
                    </div><!--//repArtContent-->
                </article><!--//reportArticle-->
            </div><!--//col-md-6--> 
            @empty                     
            <div class="col-12">
                <div class="no-content-msg">   
                    No Property Available 
                </div>
            </div>
            @endforelse
        </div><!--//row-->
    </div><!--//reportsListing-->
    {{ $deleteLands->links() }}
</div><!--//tab-content-->
@endsection   
@section('modal')
<!-- Verify land model -->
<div class="modal fade w-400" id="getVerified" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="popupForm">
                    <form>
                        <div class="popupHeading">
                            <h2>Get Verified your Land</h2>
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="text" class="form-control" placeholder="Name" id="Name">
                            <label for="Name" class="floating-label">Name</label>
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="text" class="form-control" placeholder="Email id" id="email">
                            <label for="email" class="floating-label">Email id</label>
                        </div> 

                        <div class="form-group floating-field singleBorder">
                            <input type="text" class="form-control" placeholder="Mobile Number" id="number">
                            <label for="number" class="floating-label">Mobile Number</label>
                        </div>

                        <div class="form-group floating-field singleBorder mb-4">
                            <input type="text" class="form-control" placeholder="Site Address" id="site">
                            <label for="site" class="floating-label">Site Address</label>
                        </div> 

                        <div class="formBtn">
                            <button type="button" class="btn btn-primary">Submit</button>
                        </div> 
                    </form>
                </div><!--//popupForm--> 
            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->


<!-- Interested people model -->
<div class="modal fade w-380" id="interestedPeople" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="interPeople_modal">
                    <h2>Interested People for your Land</h2>

                    <div class="intPeopleList customScrollbar">
                        <article>
                            <h3>Abhit Bhatia</h3>
                            <p>+9560465467 <span>|</span> abhitbhatia1@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Mayank Sharma</h3>
                            <p>+9560465467 <span>|</span> mayank.sharma@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Yachna Verma</h3>
                            <p>+9560465467 <span>|</span> yachnaverma@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Abhit Bhatia</h3>
                            <p>+9560465467 <span>|</span> abhitbhatia1@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Abhit Bhatia</h3>
                            <p>+9560465467 <span>|</span> abhitbhatia1@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Mayank Sharma</h3>
                            <p>+9560465467 <span>|</span> mayank.sharma@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Yachna Verma</h3>
                            <p>+9560465467 <span>|</span> yachnaverma@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Abhit Bhatia</h3>
                            <p>+9560465467 <span>|</span> abhitbhatia1@gmail.com    </p>
                        </article> 
                    </div><!--//intPeopleList-->

                </div><!--//interPeople_modal-->

            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->


<!-- Inactive model -->
<div class="modal fade w-380" id="inactiveLand" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="inactive_modal">
                    <h2>Are you Sure you want to inactive your Land Listing ?</h2>

                    <div class="optionalBtns">
                        <span class="customRadioBtn">
                            <input type="radio" name="ans" id="yes">
                            <label for="yes">Yes, Do it</label>
                        </span>

                        <span class="customRadioBtn">
                            <input type="radio" name="ans" id="no">
                            <label for="no">No, Don’t</label>
                        </span>
                    </div><!--//optionalBtns-->

                    <textarea placeholder="Reason to inactive your listing"></textarea>

                    <div class="formBtn">
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </div><!--//interPeople_modal-->

            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->

@endsection   
