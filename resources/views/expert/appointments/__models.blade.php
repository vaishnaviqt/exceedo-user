
<div class="modal fade urban-planning-popup approval-appointment" id="approval-appointment-popup" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close" data-dismiss="modal"></button>
            <div class="modal-body suggestAnotherSchedule">

            </div>
        </div>
    </div>
</div>


<div class="modal fade suggestAnotherTime" id="suggest-another-time-popup" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close" data-dismiss="modal"></button>
            <div class="modal-body p-0"> 
                <h2>Suggest Another Time</h2> 
                <form>
                    <!-- <div class="form-filds">  -->
                        <div class="form-group mb-3">
                            <input type="hidden" name="id" value="" class="rescheduleId">
                            <input class="form-control rescheduleDate" id="datepicker" type="text" name="date" placeholder="Appointment Date"/> 
                        </div> 

                        <div class="form-group mb-3">
                            <input class="form-control rescheduleTime" id="timepicker" type="text" placeholder="Appointment Time"/> 
                        </div> 

                        <div class="form-group mb-3">
                            <input class="form-control rescheduleRemarks" id="remarks" type="text" placeholder="Any Further Remarks"/>
                        </div> 

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input rescheduleTerms" id="privacyPolicy" value="1" name="privacyPolicy">
                            <label class="custom-control-label" for="privacyPolicy">I agree to the <span> Privacy Policy, Terms of services</span> and <span> Cookie Policy</span> of this Website</label>
                        </div>
                        
                        <button class="btn btn-primary js-btn-next" id="rescheduleSubmitButton" type="button" title="Next">Submit</button>
                            
                    <!--</div>//form-filds-->
                </form><!--//form--> 
            </div>
        </div>
    </div>
</div>

<!-- Urban Planning Discussion-popup Start-->
<div id="urban-planning-popup" class="mfp-with-anim mfp-hide urban-planning-popup">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12">
                <ul>
                    <li>
                        <h2>Urban Planning Discussion</h2>
                        <span class="dateTime">Monday, 06 October, 08:00 am to 08:30 am </span>
                        <!--//threeDotMenu Start-->
                        <div class="dropdown">
                            <button class="btn" type="button" data-toggle="dropdown"></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="JavaScript:void(0);">Link</a></li>
                                <li><a href="JavaScript:void(0);">Link</a></li>
                            </ul>
                        </div>
                        <!--//threeDotMenu End-->
                    </li>
                    <li>
                        <span class="joinMeeting">Join the Meeting </span>
                        <a href="javascript:void(0);">https://us02web.zoom.us/j/303164402?pwd=WXg4M3lQUTh6NDZlS3lPQUlkMGNXZz09</a>
                    </li>
                    <li>
                        <label>Meeting ID: <span>303 164 402</span></label>
                        <label>Password: <span>095215</span></label>
                    </li>
                </ul>
                <footer>
                    Please join the meeting 10 minutes before
                </footer>
            </div><!--//.col-12 -->           
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //Urban Planning Discussion-popup End -->


<!-- view-all-appointments-popup Start-->
<div id="view-all-appointments-popup" class="mfp-with-anim mfp-hide viewAll-appointment">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row" id="allAppointmentCard">
        </div><!--/row-->

    </div><!-- //popup-body --> 
</div><!-- //view-all-appointments-popup End -->

<!-- thankyou-popup-popup -->
<div id="thankyou-popup" class="mfp-with-anim mfp-hide thankyou">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row">
            <div class="col-12">
                <h1><img src="images/svg-icons/success-icon.svg" alt=""></h1>
                <h2>Thankyou for Assigning Expert</h2>
                <span>Expert will get notification to get report done</span>
                <!-- <a href="javascript:void(0);" class="btn btn-primary">Ok</a> -->
                <input type="submit" class="btn btn-primary" value="Ok">
            </div><!-- //.col-12 -->
        </div><!--/row-->
    </div><!-- //popup-body --> 
</div><!-- //thankyou-popup-popup -->

