
            <div class="col-12">
                <h2>Pending Appointments 
                    <!--<span>(06 Oct 2020)</span>-->
                </h2>
            </div><!--//.col-12 -->  

            <div class="col-12">
                <div class="searchForm">
                    <form method="post" action="{{ url('admin/appointments') }}">
                        @csrf
                        <ul class="d-flex flex-wrap">
                            <li>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="date" id="pendingDate"value="{{ !empty($pendingDate) ? Carbon\Carbon::parse($pendingDate)->format('d-m-Y') : '' }}" placeholder=""/>
                                </div><!-- //form-group --> 
                            </li>
<!--                            <li>
                                <div class="form-group">
                                    <select class="form-control" name="appoint_id">
                                        <option value="">Appointment</option>
                                        <option>Appointment 01</option>
                                        <option>Appointment 02</option>
                                        <option>Appointment 03</option>
                                    </select>
                                </div> //form-group  
                            </li>-->
                            <li>
                                <!-- <a href="javascript:void(0);" class="btn btn-primary">Se</a> -->
                                <input type="button" value="pendingSeacrh" name="submit" class="btn btn-primary viewPendingAppointSearchForm">
                            </li>
                        </ul><!-- //ul -->
                    </form><!-- //form -->
                </div><!--//searchForm-->
            </div><!-- //.col-12-->

            <div class="col-12">
                <div class="appointments pendingAppoint">
                    <form>
                        @forelse($pendingAppoints as $pending)

                        <ul class="appointments-list">
                            <li class="order-md-1 order-1">
                                <h3>{{ $pending->remarks }}</h3>
                            </li>
                            <li class="order-md-2 order-3">{{ !empty($pending->date) ? Carbon\Carbon::parse($pending->date . ' ' . $pending->time)->format('d M, Y h:i A') : '' }}</li>
                            
                            <li class="order-md-4 order-4">
                                Join the Meeting <a href="javascript:void(0);">https://us02web.zoom.us/j/30316440wji383928292…</a>
                            </li>
                        </ul><!--//appointments-list-->
                        @empty
                        <p class="no-data mb-0">No Pending Appointment Available</p>
                        @endforelse
                    </form><!--//form-->
                </div><!--//appointments -->
            </div><!--//col-12-->
            <script>
    $(document).ready(function () {
        // datepicker
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#pendingDate').datepicker({
            maxDate: today,
            format: 'dd-mm-yyyy'
        });
    })
</script>