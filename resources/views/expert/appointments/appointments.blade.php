@extends('expert.layouts.master')
@section('title')
Appointments
@endsection
@section('content')
<style>

    .loader{
        position: fixed;
        top:0;
        left:0;
        right: 0;
        bottom:0;
        display:flex;
        align-items:center;
        justify-content:center;
        z-index: 9999;
        background: #ffffffa6;
    }
    .loader img{
        width:80px;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
<div class="loader d-none">
    <img src="{{ asset('images/Spin-1s-200px.gif') }}">
</div>
<section class="reportsBody" id="inline-popups">
    @include('flash-message')
    <h2>{{ __('Appointments') }}</h2>
    <div class="countCards">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/all-appointments-icon.svg') }}" alt="All Appointments" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $totalAppointments }}</strong></p>
                        <p>{{ __('All Appointments') }}</p>
                    </div>
                </article><!--//countCardArt-->
            </div><!--//col-lg-4-->

            <div class="col-lg-4 col-md-6">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/upcoming-appointments-icon.svg') }}" alt="Upcoming Appointments" />                                     
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $upcommingAppointments }}</strong></p>
                        <p>{{ __('Upcoming Appointments') }}</p>
                    </div>
                </article><!--//countCardArt-->
            </div><!--//col-lg-4-->

            <div class="col-lg-4 col-md-6">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="{{ asset('adminassets/images/svg-icons/done-appointments-icon.svg') }}" alt="Done Appointments" />                                
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $doneAppointments }}</strong></p>
                        <p>{{ __('Done Appointments') }}</p>
                    </div>
                </article><!--//countCardArt-->
            </div><!--//col-lg-4-->                       
        </div><!--//row-->
    </div><!--//countCards-->

    <div class="appointmentTopLinks mb-4">
        <a href="javascript:void(0);" class="allAppointButton" data-effect="mfp-zoom-in">{{ __('All Appointments') }}</a> 
        <a href="javascript:void(0);" class="pendingAppointButton" data-effect="mfp-zoom-in">{{ __('Pending Appointments') }}</a>
        <a href="javascript:void(0);" class="doneAppointButton" data-effect="mfp-zoom-in">{{ __('Complete Appointments') }}</a>
    </div>

    <!-- //Search Form Div Start -->
    <div class="searchForm">
        <a href="javascript:void(0);" class="open-mobile-search">{{ __('Search') }}</a>                                  
        <form action="{{url('expert/appointments')}}" method="POST">
            @csrf
            <span>{{ __('Search') }}</span>
            <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('adminassets/images/svg-icons/close-icon.svg') }}" alt=""></a>
            <ul class="d-flex flex-wrap">
                <li>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Appointment" id="appointment">
                    </div><!-- //form-group -->
                </li>
                <li>
                    <div class="form-group">
                        <select class="form-control" id="paymentType" name="payment_mode">
                            <option value="">Payment Method</option>
                            <option value="cash" @if($mode == 'cash') {{ __('selected') }} @endif>Cash</option>
                            <option value="card"  @if($mode == 'card') {{ __('selected') }} @endif>Card</option>
                        </select>
                    </div><!-- //form-group --> 
                </li>
                <li>
                    <button type="submit "class="btn btn-primary" id="submitAppointFilter">Search</button>
                </li>
            </ul><!-- //ul -->
        </form><!-- //form -->
    </div><!-- //searchForm -->
    <!-- //Search Form Div End -->

    <div class="customStyleCalendar">
        <div id="calendar"></div>
    </div>

</section><!--//pageContainer-->
<!-- approval-appointment-popup Start-->
@include('expert.appointments.__models')
<!-- view-all-appointments-popup Start-->
<a href="#approval-appointment-popup" class="d-none confirmSuggentPopup" data-effect="mfp-zoom-in"></a>
@endsection

@section('custom-script')
<script>
$(document).ready(function () {
    var baseUrl = $('meta[name="base_url"]').attr('content');
    var url = "/expert/appointment-details?paymentMode=<?php echo $mode ?>";
    var calendar = $('#calendar').fullCalendar({
        editable: true,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        events: baseUrl + url,
        selectable: true,
        selectHelper: true,

        eventClick: function (event)
        {
            if (new Date() <= event.start) {
                var title = event.title;
                var dateTime = $.fullCalendar.formatDate(event.start, "dddd, DD MMMM, hh:mm a");
                var evDate = $.fullCalendar.formatDate(event.start, "DD-MM-Y");
                var evTime = $.fullCalendar.formatDate(event.start, "HH:mm:ss");
                var eventId = event.id;
                var rescheduled_by = event.rescheduled_by;
                var canceled_by = event.canceled_by;
                var confirmButton = '';
                var button = '';
                if (event.status == 3) {
                    if (canceled_by == 0) {
                        var text = "User canceled this appointment";
                    } else if (canceled_by == 2) {
                        var text = "Admin cancel this appointment";
                    }
                    button = `<ul class="confirmAppointment">
                        <li>` + text + `</li>
                    </ul>`;
                }
                if (event.status == 2 && rescheduled_by != 1) {
                    button = `<ul class="confirmAppointment">
                    <li><a class="expertConfirmAppoint" data-href="` + baseUrl + "/expert/confirm-appointment/" + eventId + `" href="javascript:void(0);">Confirm the Appointment</a></li>
<li><a href="javascript:void(0);" data-id="` + eventId + `" data-date="` + evDate + `" data-time="` + evTime + `" class="expertAnotherSuggestPopup">Suggest Another time</a></li>                
</ul>`;
                } else if (event.status != 1 && event.status != 3) {
                    if (rescheduled_by != 1) {
                    var confirmButton = `<li><a class="expertConfirmAppoint" data-href="` + baseUrl + "/expert/confirm-appointment/" + eventId + `" href="javascript:void(0);">Confirm the Appointment</a></li>`;
                }
                    button = `<ul class="confirmAppointment">
                        ` + confirmButton + `
                        <li><a href="javascript:void(0);" data-id="` + eventId + `" data-date="` + evDate + `" data-time="` + evTime + `" class="expertAnotherSuggestPopup">Suggest Another time</a></li>
                    </ul>`;
                }
                var html = `<div class="row">
                <div class="col-12">
                    <ul>
                        <li>
                            <h2>` + title + `</h2>
                            <span class="dateTime">` + dateTime + ` </span>
                            
                        </li>
                    </ul>
                    ` + button + `
                </div>         
            </div>`;
                $('.suggestAnotherSchedule').html(html);
                $('#approval-appointment-popup').modal('show');
            }
        },
    });
});

$(".appointments").mCustomScrollbar({
    theme: "dark",
    scrollButtons: {scrollType: "stepped"},
    live: "on"
});

</script>

<script>
    $(document).ready(function () {
        $('body').on('click', '.expertConfirmAppoint', function (e) {
            e.preventDefault();
            var link = $(this).data('href');
            swal({
                text: "Are you sure want to confirm appointment?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willDelete) => {
                        if (willDelete) {
                            window.location.href = link;
                        } else {
                            return false;
                        }
                    });
        });
        $('body').on('click', '.expertAnotherSuggestPopup', function (e) {
            var id = $(this).data('id');
            var date = $(this).data('date');
            var time = $(this).data('time');
            $('.rescheduleDate').val(date);
            $('.rescheduleTime').val(time);
            $('.rescheduleId').val(id);
            $('.rescheduleRemarks').val('');
            $(".rescheduleTerms").prop("checked", false);
            $('#approval-appointment-popup').modal('hide');
            $('#suggest-another-time-popup').modal('show');

        });
    });
</script>
<script>
    $(document).ready(function () {
        $('body').on('click', '.viewAllAppointSearchForm', function () {
            viewAllAppointment();
        });
        $('.allAppointButton').on('click', function () {
            viewAllAppointment();
        });
    });
    function viewAllAppointment() {
        var all_expert_id = $('#allAppointExpert').val();
        var all_date = $('#allAppointDate').val();
        $('.loader').toggleClass('d-none');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "post",
            url: "view-all-appointment",
            data: {
                all_date: all_date,
                all_expert_id: all_expert_id,
            },
            success: function (data) {
                $('.loader').toggleClass('d-none');
                $('#allAppointmentCard').html(data.html);
                $(".appointments").mCustomScrollbar({
                    theme: "dark",
                    scrollButtons: {scrollType: "stepped"},
                    live: "on"
                });
                $.magnificPopup.open({
                    items: {
                        src: '#view-all-appointments-popup',
                        callbacks: {
                            beforeOpen: function () {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        type: 'inline'
                    }

                });
            },
            error: function () {
                alert("Fetching Expert Record Failed");
                return false;
            }
        });
        return false;
    }
    $(document).ready(function () {
        $('body').on('click', '.viewPendingAppointSearchForm', function () {
            viewPendingAppointment();
        });
        $('.pendingAppointButton').on('click', function () {
            viewPendingAppointment();
        });
    });
    function viewPendingAppointment() {
        var expert_id = $('#pendingExpertId').val();
        var date = $('#pendingDate').val();
        $('.loader').toggleClass('d-none');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "post",
            url: "view-pending-appointment",
            data: {
                date: date,
                expert_id: expert_id,
            },
            success: function (data) {
                $('.loader').toggleClass('d-none');
                $('#allAppointmentCard').html(data.html);
                $(".appointments").mCustomScrollbar({
                    theme: "dark",
                    scrollButtons: {scrollType: "stepped"},
                    live: "on"
                });
                $.magnificPopup.open({
                    items: {
                        src: '#view-all-appointments-popup',
                        callbacks: {
                            beforeOpen: function () {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        type: 'inline'
                    }

                });
            },
            error: function () {
                alert("Fetching Expert Record Failed");
                return false;
            }
        });
        return false;
    }

    //view done appointment
    $(document).ready(function () {
        $('body').on('click', '.viewDoneAppointSearchForm', function () {
            viewDoneAppointment();
        });
        $('.doneAppointButton').on('click', function () {
            viewDoneAppointment();
        });
    });
    function viewDoneAppointment() {
        var expert_id = $('#doneExpertId').val();
        var date = $('#doneDate').val();
        $('.loader').toggleClass('d-none');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "post",
            url: "view-done-appointment",
            data: {
                date: date,
                expert_id: expert_id,
            },
            success: function (data) {
                $('.loader').toggleClass('d-none');
                $('#allAppointmentCard').html(data.html);
                $(".appointments").mCustomScrollbar({
                    theme: "dark",
                    scrollButtons: {scrollType: "stepped"},
                    live: "on"
                });
                $.magnificPopup.open({
                    items: {
                        src: '#view-all-appointments-popup',
                        callbacks: {
                            beforeOpen: function () {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        type: 'inline'
                    }

                });
            },
            error: function () {
                alert("Fetching Expert Record Failed");
                return false;
            }
        });
        return false;
    }


    //reschedule appointment
    $(document).ready(function () {
        $('#rescheduleSubmitButton').on('click', function () {
            var rescheduleDate = $('.rescheduleDate').val();
            var rescheduleTime = $('.rescheduleTime').val();
            var rescheduleRemarks = $('.rescheduleRemarks').val();
            var rescheduleTerms = $('.rescheduleTerms').val();
            var rescheduleId = $('.rescheduleId').val();
            if (rescheduleDate == '') {
                $('.errorMsg').remove();
                $('.rescheduleDate').parent().after('<span class="text-danger errorMsg" role="alert"><small>The Appointment Date field is required.</small></span>');
                return false;
            }
            if (rescheduleTime == '') {
                $('.errorMsg').remove();
                $('.rescheduleTime').parent().after('<span class="text-danger errorMsg" role="alert"><small>The Appointment Time field is required.</small></span>');
                return false;
            }

            if (rescheduleRemarks == '') {
                $('.errorMsg').remove();
                $('.rescheduleRemarks').parent().after('<span class="text-danger errorMsg" role="alert"><small>The Remarks field is required.</small></span>');
                return false;
            }
            if ($('.rescheduleTerms').prop("checked") == false) {
                $('.errorMsg').remove();
                $('.rescheduleTerms').parent().after('<span class="text-danger errorMsg" role="alert"><small>The Terms field is required.</small></span>');
                return false;
            }
            $('.errorMsg').remove();
            $('.loader').toggleClass('d-none');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "post",
                url: "reschedule-appointment",
                data: {
                    date: rescheduleDate,
                    time: rescheduleTime,
                    remarks: rescheduleRemarks,
                    terms: rescheduleTerms,
                    id: rescheduleId,
                },
                success: function (data) {
                    $('.loader').toggleClass('d-none');
                    if (data.date == true) {
                        $('.errorMsg').remove();
                        $('.rescheduleDate').parent().parent().after('<span class="text-danger errorMsg" role="alert"><small>Please Select future date.</small></span>');
                        return false;
                    } else {
                        swal("Your appointment reschedule successfully!")
                                .then((value) => {
                                    location.reload();
                                });
                    }
                    if (data == true) {
                        swal("Your appointment reschedule successfully!")
                                .then((value) => {
                                    location.reload();
                                });

                    }
                },
                error: function () {
                    alert("Reschedule appointment failed");
                    return false;
                }
            });

        });
    });
</script>

@endsection
