

<div class="col-12">
    <h2>Complete Appointments 
        <!--<span>(06 Oct 2020)</span>-->
    </h2>

</div><!--//.col-12 -->  
<div class="col-12">
    <div class="d-none viewAppointmentError ml-5 mt-2 mb-3"></div>
    <div class="searchForm">
        <form method="post" action="{{ url('expert/appointments') }}" id="viewDoneAppointSearchForm">
            @csrf
            <ul class="d-flex flex-wrap">
                <li>
                    <div class="form-group">
                        <input class="form-control" type="text" name="done_date" id="doneDate" value="{{ !empty($doneDate) ? Carbon\Carbon::parse($doneDate)->format('d-m-Y') : '' }}" placeholder=""/>
                    </div><!-- //form-group --> 
                </li>
                <!--                <li>
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Appointment</option>
                                            <option>Appointment 01</option>
                                            <option>Appointment 02</option>
                                            <option>Appointment 03</option>
                                        </select>
                                    </div> //form-group  
                                </li>-->
                <li>
                    <!-- <a href="javascript:void(0);" class="btn btn-primary">Se</a> -->
                    <input type="button" value="doneSearch" name="submit" class="btn btn-primary viewDoneAppointSearchForm">
                </li>
            </ul><!-- //ul -->
        </form><!-- //form -->
    </div><!--//searchForm-->
</div><!-- //.col-12-->
<div class="col-12">
    <div class="appointments">
        <form>
            @forelse($doneAppoints as $appoint)
            <ul class="appointments-list">
                <li class="order-md-1 order-1"><h3>{{ $appoint->remarks }}</h3></li>
                <li class="order-md-2 order-3">{{ !empty($appoint->date) ? Carbon\Carbon::parse($appoint->date .' '.$appoint->time)->format('d M, Y h:i A') : ''}}</li>
                <li class="order-md-4 order-4">
                    Join the Meeting <a href="javascript:void(0);">https://us02web.zoom.us/j/30316440wji383928292…</a>
                </li>
            </ul><!--//appointments-list-->
            @empty
            <p class="no-data mb-0">No Appintment Available</p>
            @endforelse

        </form><!--//form-->
    </div><!--//appointments -->
</div><!--//col-12-->
<script>
    $(document).ready(function () {
        // datepicker
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#doneDate').datepicker({
            maxDate: today,
            format: 'dd-mm-yyyy'
        });
    })
</script>