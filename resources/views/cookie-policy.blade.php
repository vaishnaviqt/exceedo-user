@extends('layouts.master')

@section('title')
 Cookie
@endsection
@section('content')
<section class="commonFormPage adjustHeaderSpace staticPage">
    <div class="heading-title">
        <h2>Cookie Policy</h2>
    </div>
    <!--//heading-title-->
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5">
                <p class="text-left w-100 pt-3 title">
                    What are cookies?
                </p>
<p class="text-left w-100 pt-3 content">
                    Cookies are text files stored on the device you use to visit our website. Some cookies are essential for our websites to work, others collect anonymous or personal information to allow us to improve our websites and to show you relevant content.
                </p>
<p class="text-left w-100 pt-3 content">
                    At Exceedo Know Your Land we use different types of cookies and you can read more about these below. By continuing to use our website you agree to our use of cookies. You do have the choice to limit the usage of some of these cookies.
                </p>
<p class="text-left w-100 pt-5 title">
                    How long are cookies stored on my device?
                </p>
<p class="text-left w-100 pt-3 content">
                    Two types of cookies can be stored:
                </p>
<ul class="w-100">
<li>Session cookies which are automatically deleted when you close your browser</li>
<li>Persistent cookies which remain on your computer until they are deleted or expire. Persistent cookies can keep your user preferences to help make future<br>
                        browsing easier and more relevant.</li>
</ul>
<p class="text-left w-100 pt-5 title">
                    Types of cookies we use
                </p>
<p class="text-left w-100 pt-3 content">
                    Essential and Functional Cookies
                </p>
<p class="text-left w-100 pt-3 content">
                    Without these cookies, our website may not work properly. Some of these cookies are strictly necessary for Exceedo Know Your Land sites to ensure security, ease of use and functionality. Others provide you with services available across our websites and are needed to use some of its features. We also use functional cookies to ensure the speed of our web pages, to understand your location, enable video content and faster browsing.
                </p>
<p class="text-left w-100 pt-3 content">
                    Advertising and Marketing Cookies
                </p>
<p class="text-left w-100 pt-3 content">
                    Advertising and marketing cookies are used to show you adverts relevant to you, your interests and your interaction with Exceedo Know Your Land and our websites. We use this data to improve our communications with you. They are used to show you relevant advertisements, to limit the number of times you see an advert, and to help measure the effectiveness of our advertising campaigns.
                </p>
<p class="text-left w-100 pt-3 content">
                    These cookies can also remember that you have visited a website and collect information about your browsing habits in order to manage your preferences accordingly.
                </p>
<p class="text-left w-100 pt-3 content">
                    Some of these cookies can identify you as an individual based on IP address and a connected email address. These cookies are third party cookies used by third party companies with Exceedo Know Your Land�s permission.
                </p>
<p class="text-left w-100 pt-5 title">
                    Performance and analytics
                </p>
<p class="text-left w-100 pt-3 content">
                    We use performance &amp; analytics cookies to further improve our website in order to always provide you with the best possible online experience. We do not store any personal data which might identify you personally or as a client of Exceedo Know Your Land. These technologies are not always essential but without these cookies, certain website functionality may become unavailable. Some of these cookies are third party cookies used by third party companies with Exceedo Know Your Land�s permission.
                </p>
<p class="text-left w-100 pt-5 title">
                    Social media cookies
                </p>
<p class="text-left w-100 pt-3 content">
                    Social media cookies are placed on our website and, if you are logged into the social media platform, can identify your visit and collect information about your browsing habits. Sometimes we use these cookies to include or exclude you from social media advertising and to help measure the effectiveness of these advertising campaigns. These cookies are third party cookies used by third party companies with Exceedo Know Your Land�s permission.
                </p>
            </div>
            <!--//col-->
        </div>
        <!--//row-->
    </div><!-- //container -->
</section>
@endsection