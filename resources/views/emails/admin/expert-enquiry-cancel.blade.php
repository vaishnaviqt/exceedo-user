<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exceedo</title>

        <style>
            *{ padding: 0; margin: 0;}
            body{ background-color: #f9f9f9;}
        </style>
    </head>
    <body>
        <table style="background-color: #ffffff; border-top: #42bbac solid 5px; margin: 30px auto; max-width: 600px;">
            <tr>
                <td style="padding: 30px; font-size: 14px; color:#757575; line-height: 22px; font-family: Arial, Helvetica, sans-serif;">
                    <h1 style="text-align: center; margin: 0 0 25px;"><img src="{{ asset('adminassets/images/logo.svg') }}" alt="Exceedo" /></h1>

                    <h2 style="font-size: 14px; margin: 0 0 10px; color:#333">Dear Admin,</h2>

                    <p style="margin: 0 0 5px;">Your request for service request for '{{isset($enquiry->service->service_name) ? ucwords($enquiry->service->service_name) : ''}} service' to KYL expert  'Mr./Ms, {{ isset($enquiry->expert->name) ? ucwords($enquiry->expert->name) : '' }}', has been declined Kindly reach out to 'Mr./Ms. {{ isset($enquiry->expert->name) ? ucwords($enquiry->expert->name) : '' }}' for further action, if needed.Your can access your admin panel via ({{ url('/') }})</p>

                    <p style="margin: 0 0 20px;"><b>Summary of service request is as follows:</b></p>
                    <p style="margin: 0 0 20px;">
                        Assignment No:     
                        <br>Assignment : 'Service Type'
                        <br>Date of Assigning: 
                        <br>Date of Acceptance:
                        <br>Date of Cancellation:
                        <br>Contact Name of Expert:
                        <br>Mobile number of Expert:
                        <br>Timeline or any other remarks:
                    </p>

                    <p style="margin: 0 0 20px;"><b>Nature of Assignment:</b></p>
                    <p style="margin: 0 0 20px;">
                        Name of Client:
                        <br>Contact no. of Client:
                        <br>Email of Client:
                        <br>Remarks, If any:
                    </p>

                    <p style="margin:0">Regards,<br>
                        <strong style="color: #333;">Team Know Your Land</strong>
                    </p>
                </td>
            </tr> 
        </table>
    </body>
</html>