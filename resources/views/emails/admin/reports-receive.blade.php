<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exceedo</title>

        <style>
            *{ padding: 0; margin: 0;}
            body{ background-color: #f9f9f9;}
        </style>
    </head>
    <body>
        <table style="background-color: #ffffff; border-top: #42bbac solid 5px; margin: 30px auto; max-width: 600px;">
            <tr>
                <td style="padding: 30px; font-size: 14px; color:#757575; line-height: 22px; font-family: Arial, Helvetica, sans-serif;">
                    <h1 style="text-align: center; margin: 0 0 25px;"><img src="{{ asset('adminassets/images/logo.svg') }}" alt="Exceedo" /></h1>

                    <h2 style="font-size: 14px; margin: 0 0 10px; color:#333">Dear Admin,</h2>

                    <p style="margin: 0 0 5px;">Draft report for 'Land Feasibility/Land Title search' service has been uploaded by 'mr./Ms. {{ isset($project->expert->name) ? ucwords($project->expert->name) : '' }}' expert for 'assignment no. ' on 'date'.</p>

                    <p style="margin: 0 0 5px;">Kindly review the report by accessing the admin panel or click 'here' to login to take necessary action.</p>

                    <p style="margin: 0 0 20px;">
                        <b>Service summary</b><br>
                        Assignment No:<br>
                        Name of Assignment:<br>
                        Client:<br>
                        Date of Assignemnt given:<br>
                        Date of draft report sibmission:<br>
                        Expected date of submission:<br>
                        Remaks, if any:<br>
                    </p>


                    <p style="margin:0">Regards,<br>
                        <strong style="color: #333;">Team Know Your Land</strong>
                    </p>
                </td>
            </tr> 
        </table>
    </body>
</html>





