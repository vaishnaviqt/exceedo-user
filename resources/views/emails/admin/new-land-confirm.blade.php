<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exceedo</title>

        <style>
            *{ padding: 0; margin: 0;}
            body{ background-color: #f9f9f9;}
        </style>
    </head>
    <body>
        <table style="background-color: #ffffff; border-top: #42bbac solid 5px; margin: 30px auto; max-width: 600px;">
            <tr>
                <td style="padding: 30px; font-size: 14px; color:#757575; line-height: 22px; font-family: Arial, Helvetica, sans-serif;">
                    <h1 style="text-align: center; margin: 0 0 25px;"><img src="{{ asset('adminassets/images/logo.svg') }}" alt="Exceedo" /></h1>

                    <h2 style="font-size: 14px; margin: 0 0 10px; color:#333">Hi Admin,</h2>

                    <p style="margin: 0 0 5px;">A new land listing '{{$property->id}}' has been successfully added to KYL portal on '{{ Carbon\carbon::parse($property->created_at)->format('d M, Y') }}' by 'Admin'.
                    </p>

                    <p style="margin: 0 0 5px;">Kindly review the listing and other updates by accessing Admin dashboard.Your can access your Admin panel via ({{ url('/') }})</p>

                    <p style="margin: 0 0 20px;">
                        <b> Land Listing snapshot </b><br>
                        Listing Number: {{$property->id}}<br>
                        Location of Land: {{$property->address}}<br>
                        Area: {{$property->nearby_place}}<br>
                        Date of listing: {{ Carbon\carbon::parse($property->created_at)->format('d M, Y') }}<br>
                        Remaks, if any: {{$property->descritpion}}<br>
                    </p>

                    <p style="margin: 0 0 20px;">
                        <b> Details of Broker Assigned </b><br>
                        Name: {{ ucwords($property->user->name) }}<br>
                        Contact No: {{$property->user->mobile}}<br>
                        Email: {{$property->user->email}}<br>
                    </p>

                    <p style="margin:0">Regards,<br>
                        <strong style="color: #333;">Team Know Your Land</strong>
                    </p>
                </td>
            </tr> 
        </table>
    </body>
</html>




