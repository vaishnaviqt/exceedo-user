<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exceedo</title>

        <style>
            *{ padding: 0; margin: 0;}
            body{ background-color: #f9f9f9;}
        </style>
    </head>
    <body>
        <table style="background-color: #ffffff; border-top: #42bbac solid 5px; margin: 30px auto; max-width: 600px;">
            <tr>
                <td style="padding: 30px; font-size: 14px; color:#757575; line-height: 22px; font-family: Arial, Helvetica, sans-serif;">
                    <h1 style="text-align: center; margin: 0 0 25px;"><img src="{{ asset('adminassets/images/logo.svg') }}" alt="Exceedo" /></h1>

                    <h2 style="font-size: 14px; margin: 0 0 10px; color:#333">Dear Mr./Ms. {{ $user->name }},</h2>

                    <p style="margin: 0 0 5px;">Please note that your registeration/login details as an Admin on  Know Your Land  (KYL) team have been added by Admin on {{ date('d M, Y h:i A') }} as per details shared and verified with you as follows.</p>
                    Access details to KYL broker dashboard
                    <p style="margin: 0 0 5px;">Username: {{ $user->email }}</p>
                    Password: {{ $user->mobile }}

                    In case of any queries, please reach out to KYL team at (contact details).

                    <p style="margin: 0 0 5px;">Please login to ({{ url('/admin') }}) to take further actions. </p>

                    <p style="margin: 0 0 20px;"> </p>

                    <p style="margin: 0 0 20px;"> </p>

                    <p style="margin:0">Regards,<br>
                        <strong style="color: #333;">Team Know Your Land</strong>
                    </p>
                </td>
            </tr> 
        </table>
    </body>
</html>
