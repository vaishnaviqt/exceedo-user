<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exceedo</title>

        <style>
            *{ padding: 0; margin: 0;}
            body{ background-color: #f9f9f9;}
        </style>
    </head>
    <body>
        <table style="background-color: #ffffff; border-top: #42bbac solid 5px; margin: 30px auto; max-width: 600px;">
            <tr>
                <td style="padding: 30px; font-size: 14px; color:#757575; line-height: 22px; font-family: Arial, Helvetica, sans-serif;">
                    <h1 style="text-align: center; margin: 0 0 25px;"><img src="{{ asset('adminassets/images/logo.svg') }}" alt="Exceedo" /></h1>

                    <h2 style="font-size: 14px; margin: 0 0 10px; color:#333">Dear Admin,</h2>

                    <p style="margin: 0 0 5px;">Final '{{isset($report->valueAdded->service_name) ? ucwords($report->valueAdded->service_name) : ''}}' report with 'Assignment no.' has been successfully delivered to 'Mr./Ms. {{isset($report->user->name) ? ucwords($report->user->name) : ''}}' on '{{ Carbon\carbon::parse($report->created_at)->format('d M, Y') }}'.</p>

                    <p style="margin: 0 0 5px;">Kindly view all updates via admin login panel or click here ({{url('/')}}) to login. </p>

                    <p style="margin: 0 0 20px;">
                        <b>Service summary</b><br>
                        Assignment No:<br>
                        Name of Assignment:<br>
                        Client: {{isset($report->user->name) ? ucwords($report->user->name) : ''}}<br>
                        Date of Assignment given:<br>
                        Date of draft report submission:<br>
                        Date of feedback received:<br>
                        Expected date of submission:<br>
                        Final date of delivery:<br>
                        Remarks, if any:<br>
                    </p>

                    <p style="margin:0">Regards,<br>
                        <strong style="color: #333;">Team Know Your Land</strong>
                    </p>
                </td>
            </tr> 
        </table>
    </body>
</html>


