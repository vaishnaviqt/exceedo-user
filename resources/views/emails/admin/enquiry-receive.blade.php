<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exceedo</title>

        <style>
            * {
                padding: 0;
                margin: 0;
            }

            body {
                background-color: #f9f9f9;
            }
        </style>
    </head>
    <body>
        <table style="background-color: #ffffff; border-top: #42bbac solid 5px; margin: 30px auto; max-width: 600px;">
            <tr>
                <td style="padding: 30px; font-size: 14px; color:#757575; line-height: 22px; font-family: Arial, Helvetica, sans-serif;">
                    <h1 style="text-align: center; margin: 0 0 25px;"><img src="{{ asset('adminassets/images/logo.svg') }}" alt="Exceedo" /></h1>

                    <h2 style="font-size: 14px; margin: 0 0 10px; color:#333">Dear Admin,</h2>

                    <p style="margin: 0 0 5px;">You have received a new enquiry regarding '{{isset($service->service->service_name) ? ucwords($service->service->service_name) : ''}}' service on '{{ Carbon\carbon::parse($service->created_at)->format('d M, Y') }}' as per the details undersigned. </p>

                    <p style="margin: 0 0 5px;"><strong>Kindly retake the necessary action.</strong> </p>

                    <p style="margin: 0 0 20px;">Your can access your admin panel via ({{ url('/') }})</p>

                    <p style="margin: 0 0 20px;">Enquiry details:<br>
                        Contact person name: {{$service->name}}<br>
                        Email: {{$service->email}}<br>
                        Mobile Number: {{$service->mobile}}<br>
                        Service: {{isset($service->service->service_name) ? ucwords($service->service->service_name) : ''}}<br>
                        Remarks, if any : {{$service->comments}} </p>

                    <p style="margin:0">Regards,<br>
                        <strong style="color: #333;">Team Know Your Land</strong>
                    </p>
                </td>
            </tr>
        </table>
    </body>

</html>
