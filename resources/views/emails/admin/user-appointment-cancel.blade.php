<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exceedo</title>

        <style>
            * {
                padding: 0;
                margin: 0;
            }

            body {
                background-color: #f9f9f9;
            }
        </style>
    </head>

    <body>
        <table style="background-color: #ffffff; border-top: #42bbac solid 5px; margin: 30px auto; max-width: 600px;">
            <tr>
                <td style="padding: 30px; font-size: 14px; color:#757575; line-height: 22px; font-family: Arial, Helvetica, sans-serif;">
                    <h1 style="text-align: center; margin: 0 0 25px;"><img src="{{ asset('adminassets/images/logo.svg') }}" alt="Exceedo" /></h1>

                    <h2 style="font-size: 14px; margin: 0 0 10px; color:#333">Dear Admin,</h2>

                    <p style="margin: 0 0 5px;">The appointment scheduled for '{{ ucwords($appointment->service->service_name) }}' on '{{ Carbon\Carbon::parse($appointment->date.$appointment->time)->format('d M, Y h:i A')}}' with 'Mr./Ms. {{ ucwords($appointment->expert->name) }}' expert has been cancelled by the user due to some unavoidable reasons.</p>

                    <p style="margin: 0 0 5px;">You can login to your admin dashboard to view the details from here ({{url('/')}}).</p>
                    
                    <p style="margin: 0 0 5px;">
                        Appointment No: {{ $appointment->id }}<br>
                        Service category: {{ $appointment->service->service_name }}<br>
                        Client Name: {{ ucwords($appointment->user->name) }}<br>
                        Scheduled date and time of meeting: {{ Carbon\Carbon::parse($appointment->date.$appointment->time)->format('d M, Y h:i A')}}<br>
                        Date of cancellation:
                        Remarks, if any:
                    </p>

                </td>
            </tr>
        </table>
    </body>

</html>