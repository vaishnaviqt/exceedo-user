<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exceedo</title>

        <style>
            * {
                padding: 0;
                margin: 0;
            }

            body {
                background-color: #f9f9f9;
            }
        </style>
    </head>

    <body>
        <table style="background-color: #ffffff; border-top: #42bbac solid 5px; margin: 30px auto; max-width: 600px;">
            <tr>
                <td style="padding: 30px; font-size: 14px; color:#757575; line-height: 22px; font-family: Arial, Helvetica, sans-serif;">
                    <h1 style="text-align: center; margin: 0 0 25px;"><img src="{{ asset('adminassets/images/logo.svg') }}" alt="Exceedo" /></h1>

                    <h2 style="font-size: 14px; margin: 0 0 10px; color:#333">Dear Admin,</h2>

                    <p style="margin: 0 0 5px;">Your request for 'Expert Consultation' appointment for '{{ucwords($appointment->service->service_name)}}', scheduled for '({{ Carbon\Carbon::parse($appointment->date.$appointment->time)->format('d M, Y h:i A')}})' has been confirmed by 'Mr./Ms. ({{$appointment->expert->name}})' expert.</p>
                    <p style="margin: 0 0 5px;">Kindly login to Admin panel from here ({{url('/')}}) to take further action.</p>
                    <p style="margin: 0 0 5px;">
                        <b>Service summary<b><br>
                                Appointment No:{{$appointment->id}}<br>
                                Service category: {{ucwords($appointment->service->service_name)}}<br>
                                Client Name: {{ ucwords($appointment->user->name) }}<br>
                                Scheduled date and time of meeting: {{ Carbon\Carbon::parse($appointment->date.$appointment->time)->format('d M, Y h:i A')}}<br>
                                Expert assigned: {{ ucwords($appointment->expert->name) }}<br>
                                Remaks, if any: {{$appointment->remarks}}<br>
                                </p>

                                <p style="margin:0">Regards,<br>
                                    <strong style="color: #333;">Team Know Your Land</strong>
                                </p>
                                </td>
                                </tr>
                                </table>
                                </body>

                                </html>