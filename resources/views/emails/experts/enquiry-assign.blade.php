<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exceedo</title>

    <style>
        *{ padding: 0; margin: 0;}
        body{ background-color: #f9f9f9;}
    </style>
</head>
<body>
    <table style="background-color: #ffffff; border-top: #42bbac solid 5px; margin: 30px auto; max-width: 600px;">
        <tr>
            <td style="padding: 30px; font-size: 14px; color:#757575; line-height: 22px; font-family: Arial, Helvetica, sans-serif;">
                <h1 style="text-align: center; margin: 0 0 25px;"><img src="{{ asset('adminassets/images/logo.svg') }}" alt="Exceedo" /></h1>

                <h2 style="font-size: 14px; margin: 0 0 10px; color:#333">Hi {{ isset($enquiry->expert->name) ? ucwords($enquiry->expert->name) : '' }}</h2>

                <p style="margin: 0 0 5px;">Kindly note that you have received an assignment request by 'Know Your Land (KYL)' portal regarding '{{isset($enquiry->service->service_name) ? ucwords($enquiry->service->service_name) : ''}}' service on '{{ Carbon\carbon::parse($enquiry->created_at)->format('d M, Y') }}'. </p>

                <p style="margin: 0 0 5px;">Kindly access your expert dashboard panel for necessary action.</p>

                <p style="margin: 0 0 20px;">Your can reach to access the panel from here ({{ url('/') }}).</p>
                
                <p style="margin: 0 0 20px;">In case of any queries or troubles logging in, reach out to KYL team at 'contact number' or 'email'.</p>
                
                <p style="margin:0">Regards,<br>
                    <strong style="color: #333;">Team Know Your Land</strong>
                </p>
            </td>
        </tr> 
    </table>
</body>
</html>
