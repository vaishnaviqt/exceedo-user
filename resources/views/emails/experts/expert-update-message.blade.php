<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exceedo</title>

        <style>
            *{ padding: 0; margin: 0;}
            body{ background-color: #f9f9f9;}
        </style>
    </head>
    <body>
        <table style="background-color: #ffffff; border-top: #42bbac solid 5px; margin: 30px auto; max-width: 600px;">
            <tr>
                <td style="padding: 30px; font-size: 14px; color:#757575; line-height: 22px; font-family: Arial, Helvetica, sans-serif;">
                    <h1 style="text-align: center; margin: 0 0 25px;"><img src="{{ asset('adminassets/images/logo.svg') }}" alt="Exceedo" /></h1>

                    <h2 style="font-size: 14px; margin: 0 0 10px; color:#333">Dear Mr./Ms. {{ ucwords($message->name) }},</h2>

                    <p style="margin: 0 0 5px;">Please note that your registration/login details as an Expert on  Know Your Land (KYL) team have been 'edited' by Admin on '{{ Carbon\carbon::parse($message->created_at)->format('d M, Y') }}' as per details shared and verified with you as follows.</p>

                    <p style="margin: 0 0 5px;">
                        Name:  {{ ucwords($message->name) }}<br>
                        Contact No: {{ $message->mobile }}<br>
                        Email: {{ $message->email }}<br>
                    </p>

                    <p style="margin: 0 0 20px;">
                        Your login details are as follows: <br>
                        {{ $message->email }}<br>
                        {{ $message->mobile }}<br>
                    </p>

                    <p style="margin: 0 0 20px;">In case of any queries, please reach out to KYL team at (contact details).</p>

                    <p style="margin:0">Regards,<br>
                        <strong style="color: #333;">Team Know Your Land</strong>
                    </p>
                </td>
            </tr> 
        </table>
    </body>
</html>



