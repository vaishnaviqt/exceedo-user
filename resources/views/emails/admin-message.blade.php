@component('mail::message')
Hi {{$message->user->name}},
</br>
{{ $message->message }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
