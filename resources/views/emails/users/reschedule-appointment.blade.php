@component('mail::message')

Dear {{$appointment->user->name}},

This is to bring to your notice that your appointment with our expert on ({{ Carbon\Carbon::parse($appointment->date.$appointment->time)->format('d M, Y h:i A')}}) has been rescheduled for ({{ Carbon\Carbon::parse($appointment->date.$appointment->time)->format('d M, Y h:i A')}}) as per the availabiliy of the expert.  We sincerely hope to having your cooperation as per the change. 
You can login to your account at ({{ url('/') }}) to view your appointment. If you have any further concerns or want to learn more about our services, please get in touch with us (contact details attached).  
We appreciate that you have put your trust in us. Looking forward to meeting you.

Regards,
Team Know Your Land

@endcomponent