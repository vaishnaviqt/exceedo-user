<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exceedo</title>

    <style>
        * {
            padding: 0;
            margin: 0;
        }

        body {
            background-color: #f9f9f9;
        }
    </style>
</head>
Dear Admin,

<body>
    <table style="background-color: #ffffff; border-top: #42bbac solid 5px; margin: 30px auto; max-width: 600px;">
        <tr>
            <td style="padding: 30px; font-size: 14px; color:#757575; line-height: 22px; font-family: Arial, Helvetica, sans-serif;">
                <h1 style="text-align: center; margin: 0 0 25px;"><img src="{{ asset('adminassets/images/logo.svg') }}" alt="Exceedo" /></h1>

                <h2 style="font-size: 14px; margin: 0 0 10px; color:#333">Dear {{ $appointment->user->name }},</h2>

                <p style="margin: 0 0 5px;">Due to some unforeseen circumstances, we have to cancel your appointment with our expert on {{ Carbon\Carbon::parse($appointment->meeting_at)->format('d M, Y h:i A') }}). <br>
                    Your refund has been initiated and should reflect in your account within 15 working days. If you have any concerns or want to learn more about our services, please get in touch with us (contact details attached).
                    Look forward to serving you in future.
                </p>
                <p style="margin:0">Regards,<br>
                    <strong style="color: #333;">Team Know Your Land</strong>
                </p>
            </td>
        </tr>
    </table>
</body>

</html>