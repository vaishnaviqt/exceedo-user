<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exceedo</title>

        <style>
            *{ padding: 0; margin: 0;}
            body{ background-color: #f9f9f9;}
        </style>
    </head>
    <body>
        <table style="background-color: #ffffff; border-top: #42bbac solid 5px; margin: 30px auto; max-width: 600px;">
            <tr>
                <td style="padding: 30px; font-size: 14px; color:#757575; line-height: 22px; font-family: Arial, Helvetica, sans-serif;">
                    <h1 style="text-align: center; margin: 0 0 25px;"><img src="{{ asset('adminassets/images/logo.svg') }}" alt="Exceedo" /></h1>

                    <h2 style="font-size: 14px; margin: 0 0 10px; color:#333">Dear User,</h2>

                    <p style="margin: 0 0 5px;">Congratulations! You are now successfully registered with Exceedo Know Your Land. </p>

                    <p style="margin: 0 0 5px;"><strong>Thank you for joining us.</strong> </p>

                    <p style="margin: 0 0 20px;">To know more about Know Your Land and our service offerings, please visit ({{ url('/aboutus') }}). You can now login to your account at ({{ url('/') }}) anytime to manage your account and check status of your appointments or reports. If you have any queries, please get in touch with us (contact details attached)</p>

                    <p style="margin:0">Regards,<br>
                        <strong style="color: #333;">Team Know Your Land</strong>
                    </p>
                </td>
            </tr> 
        </table>
    </body>
</html>

