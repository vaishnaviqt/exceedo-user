<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Exceedo</title>

        <style>
            *{ padding: 0; margin: 0;}
            body{ background-color: #f9f9f9;}
        </style>
    </head>
    <body>
        <table style="background-color: #ffffff; border-top: #42bbac solid 5px; margin: 30px auto; max-width: 600px;">
            <tr>
                <td style="padding: 30px; font-size: 14px; color:#757575; line-height: 22px; font-family: Arial, Helvetica, sans-serif;">
                    <h1 style="text-align: center; margin: 0 0 25px;"><img src="{{ asset('adminassets/images/logo.svg') }}" alt="Exceedo" /></h1>

                    <h2 style="font-size: 14px; margin: 0 0 10px; color:#333">Dear Mr./Ms. {{isset($report->user->name) ? ucwords($report->user->name) : ''}},</h2>

                    <p style="margin: 0 0 5px;">Congratulations! Our experts have just finished making your report and it's ready to be downloaded. 
                        For your reference, we are attaching a copy of it with this e-mail. If you want to download it again,
                        please use this link (link here) or you can login to your user dashboard to download 
                        it directly from there ({{url('/')}}).</p>

                    <p style="margin: 0 0 5px;">We appreciate the time, patience, and the trust you have put in us. If you have any feedback for us,
                        or have any concerns with our service, please reach out to us (contact details attached). </p>

                    <p style="margin: 0 0 20px;">In case of any queries or troubles logging in, reach out to KYL team at 'contact number' or 'email'. </p>
                    <p style="margin: 0 0 20px;">We look forward to serving you again.</p>
                    <p style="margin: 0 0 20px;">
                        <b>Service summary</b><br>
                        Assignment No:<br>
                        Name of Assignment:<br>
                        Date of Assignemnt given:<br>
                        Final date of delivery:<br>
                        Remaks, if any<br>
                    </p>

                    <p style="margin:0">Regards,<br>
                        <strong style="color: #333;">Team Know Your Land</strong>
                    </p>
                </td>
            </tr> 
        </table>
    </body>
</html>
