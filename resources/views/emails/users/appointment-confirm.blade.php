@component('mail::message')

Dear {{$appointment->user->name}},

Congratulations! Your appointment with our expert on ({{ Carbon\Carbon::parse($appointment->date.$appointment->time)->format('d M, Y h:i A')}}) has been confirmed. 
Your meeting is scheduled for ({{ Carbon\Carbon::parse($appointment->date.$appointment->time)->format('d M, Y h:i A')}}). You can login to your account at ({{ url('/') }}) to manage your appointment. 
If you have any concerns or want to learn more about our services, please get in touch with us (contact details attached).  
We appreciate that you have put your trust in us. Looking forward to meeting you.

Regards,
Team Know Your Land

@endcomponent