@forelse($propertyLists as $productList)
                            <div class="col-sm-6">
                                <article class="featureArticle">
                                    <div class="featureArtImg">
                                        <a href="javascript:;" data-effect="mfp-zoom-in" onclick="aaa({{ $productList->id }})" class="productList" data-id="{{ $productList->id }}">
                                            <img src="{{ asset($productList->image) }}" alt="10 Acres Town Land">
                                        </a>
                                    </div>
                                    <div class="featureArtCont">
                                        <h3>
                                            <a href="javascript:;" data-effect="mfp-zoom-in" onclick="aaa({{ $productList->id }})" class="productList" data-id="{{ $productList->id }}">
                                                <span class="featlinkText">
                                                    {{ $productList->size_of_land }} Acres {{ $productList->land_name }} 
                                                </span>
                                                <em class="featureIcon"></em>
                                            </a>
                                        </h3>
                                        <p class="shortDetail">{{ $productList->address }}</p>
                                        <div class="featArtPric">₹ {{ $productList->max_price }} {{ $productList->price_unit }}</div>

                                        @if($productList->wifi == 1 || $productList->parking == 1) 
                                        <div class="featFacilities">
                                            <span class="featFaTitle">Facilities :</span>
                                            @if($productList->parking == 1)
                                            <span class="faci-icon parking-icon"></span>
                                            @endif
        <!--                                    <span class="faci-icon noSmooking-icon"></span>-->
                                            @if($productList->wifi == 1)
                                            <span class="faci-icon wifi-icon"></span>
                                            @endif
                                        </div><!--//featFacilities-->
                                        @endif
                                    </div>
                                </article><!--//featureArticle-->
                            </div><!--//col-sm-6-->
                            @empty
                            
                            @endforelse
                            

                            