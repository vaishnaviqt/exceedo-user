@extends('layouts.master')
@section('title')
Home
@endsection
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
<style>
    /* Start Gallery CSS */
    #gallery {
        padding-top: 40px;

        @media screen and (min-width: 991px) {
            padding: 60px 30px 0 30px;
        }
    }

    .img-wrapper {
        position: relative;
        margin-top: 15px;

        img {
            width: 100%;
        }
    }

    .img-overlay {
        background: rgba(0, 0, 0, 0.7);
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        opacity: 0;

        i {
            color: #fff;
            font-size: 3em;
        }
    }

    #overlay {
        background: rgba(0, 0, 0, 0.7);
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 999;
        // Removes blue highlight
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;

        img {
            margin: 0;
            width: 80%;
            height: auto;
            object-fit: contain;
            padding: 5%;

            @media screen and (min-width:768px) {
                width: 60%;
            }

            @media screen and (min-width:1200px) {
                width: 50%;
            }
        }
    }

    #nextButton {
        color: #fff;
        font-size: 2em;
        transition: opacity 0.8s;

        &:hover {
            opacity: 0.7;
        }

        @media screen and (min-width:768px) {
            font-size: 3em;
        }
    }

    #prevButton {
        color: #fff;
        font-size: 2em;
        transition: opacity 0.8s;

        &:hover {
            opacity: 0.7;
        }

        @media screen and (min-width:768px) {
            font-size: 3em;
        }
    }

    #exitButton {
        color: #fff;
        font-size: 2em;
        transition: opacity 0.8s;
        position: absolute;
        top: 15px;
        right: 15px;

        &:hover {
            opacity: 0.7;
        }

        @media screen and (min-width:768px) {
            font-size: 3em;
        }
    }
</style>
<section class="filterSection adjustHeaderSpace">
    <div class="container">
        <form method="post" action="{{ url('buy-your-land') }}" id="propertyListSearchForm">
            <div class="d-flex align-items-center">
                <a href="javascript:void(0);" class="filterClose"></a>

                @csrf
                <div class="filterFields">
                    <input type="text" class="form-control f_searchIcon" name="name" value="{{ $name }}" placeholder="Location" />
                </div>
                <!--//filterFields-->

                <div class="filterFields w-220">
                    <select class="form-control f_areaIcon" name="land_area">
                        <option value="">Land area</option>
                        <option value="0-2" @if($landArea=="0-2" ) {{ ('selected') }} @endif>0-2 acre</option>
                        <option value="2-5" @if($landArea=="2-5" ) {{ ('selected') }} @endif>2-5 acre</option>
                        <option value="5-10" @if($landArea=="5-10" ) {{ ('selected') }} @endif>5-10 acre</option>
                        <option value="10-20" @if($landArea=="10-20" ) {{ ('selected') }} @endif>10-20 acre</option>
                        <option value="20-50" @if($landArea=="20-50" ) {{ ('selected') }} @endif>20-50 acre</option>
                        <option value="50" @if($landArea=="50" ) {{ ('selected') }} @endif>50+ acre</option>
                    </select>
                </div>
                <!--//filterFields-->

                <div class="filterFields">
                    <select class="form-control f_areaIcon" name="businesstype">
                        <option value="">Land Type</option>
                        <option value="Commercial" @if($businessType=="Commercial" ) {{('selected') }} @endif>Commercial</option>
                        <option value="Industrial" @if($businessType=="Industrial" ) {{ ('selected') }} @endif>Industrial</option>
                        <option value="Institutional" @if($businessType=="Institutional" ) {{('selected') }} @endif>Institutional</option>
                        <option value="Recreational" @if($businessType=="Recreational" ) {{('selected') }} @endif>Recreational</option>
                        <option value="Mixed_use" @if($businessType=="Mixed_use" ) {{('selected') }} @endif>Mixed_use</option>
                        <option value="Agricultural" @if($businessType=="Agricultural" ) {{('selected') }} @endif>Agricultural</option>
                        <option value="Others" @if($businessType=="Others" ) {{ ('selected') }} @endif>Others</option>
                    </select>
                </div>

                <div class="filterRangeSlider">
                    <div class="rangeSlider">
                        <label>Price</label>
                        <section class="range-slider" id="facet-price-range-slider" data-options='{"output":{"suffix":" cr/acre"},"maxSymbol":"+"}'>
                            <input name="range_1" value="{{ !empty($range1) ? $range1 : '0' }}" min="0" max="100" step="1" type="range">
                            <input name="range_2" value="{{ !empty($range2) ? $range2 : '1250' }}" min="0" max="100" step="1" type="range">
                        </section>
                    </div><!-- //rangeSlider -->
                </div>

                <div class="filterBtns">
                    <button class="btn btn-primary">Search</button>
                    <a href="javascript:void(0);" id="resetFilterButton">Reset Filter</a>
                </div>
                <!--//filterBtns-->
            </div>
            <!--//d-flex-->
        </form>
    </div>
    <!--//container-->
</section>
<!--//filterSection-->
<script>
    $(document).ready(function() {
        $('body').on('click', '#resetFilterButton', function() {
            window.location.href = "{{ url('/buy-your-land') }}";
        });
    });
</script>

<section class="buyLandSection">
    <div class="switchBtnMobile">
        <a href="javscript:void(0);" id="switchTo_Map" class="showViewBtn">Map</a>
        <a href="javscript:void(0);" id="switchTo_List">List</a>
        <a href="javscript:void(0);" id="m_filterBtn" class="showViewBtn">Filter</a>
    </div>

    <div class="mapArea">
        <div id="mapArea" style="width:100%; height: 500px;"></div>
        <!--        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d46150.223079868716!2d76.7647584008476!3d30.714580997369396!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1605526928175!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>-->
    </div>
    <!--//mapArea-->

    <div class="container listArea">
        <div class="row row_10">
            <div class="col-lg-6 d-none d-lg-block"> &nbsp; </div>
            <div class="col-lg-6">
                <div class="listingRightSide">
                    <div class="listingHeader">
                        <h1>Commercial Areas</h1>

                        <div class="listSorting">
                            <label>Sort by Price:</label>
                            <select class="form-control" id="propertySortBy">


                                @endphp
                                <option value="price-low-to-high" @if($filter=='price-low-to-high' ) {{ __('selected') }} @endif>low-to-high</option>
                                <option value="price-high-to-low" @if($filter=='price-high-to-low' ) {{ __('selected') }} @endif>high-to-low</option>
                            </select>
                        </div>

                        <div class="m_filterBtn">
                            <a href="javascript:void(0);"></a>
                        </div>
                        <!--//m_filterBtn-->
                    </div>
                    <!--//listingHeader-->

                    <div class="itemCount">
                        {{ $propertyLists->total() }} Results
                    </div>

                    <div class="listingProducts" id="inline-popups">
                        <div class="row row_10 slider1 " id="results">

                            @include('buylanddata')

                        </div>
                        <!--//row-->
                        <div class="hidden">{{ $propertyLists->links() }}</div>
                    </div>
                    <!--//listingProducts-->
                    <div class="ajax-loading">
                        <p class="text-center">...loading...</p>
                    </div>
                </div>
                <!--//listingRightSide-->

            </div>
        </div>
        <!--//row-->
    </div>
    <!--//container-->
</section>
<!--//buyLandSection-->
<a href="#landDetail-popup" data-effect="mfp-zoom-in" class="d-none propertyPopupAnchor">
    fhbgf
</a>
<!-- landDetail-popup -->
<div id="landDetail-popup" class="mfp-with-anim mfp-hide">
    <button title="Close (Esc)" type="button" class="mfp-close"></button>
    <div class="popup-body propertyPopupBody">
    </div><!-- //popup-body -->
</div><!-- //landDetail-popup -->
<form action="{{ url('buy-your-land') }}" method="post" id="propertyFilterForm">
    @csrf
    <input type="hidden" name="filter" class="filterType">
</form>
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_map_key')}}&callback=initMap&libraries=&v=weekly" async></script>

<script>
    var map;

    function initMap() {
        map = new google.maps.Map(document.getElementById('mapArea'), {
            <?php
            if (count($properties)) {
                foreach ($properties as $key => $lon) {
                    if ($key == 0) {
            ?>
                        center: {
                            lat: <?php echo $lon->latitute ?>,
                            lng: <?php echo $lon->longitude ?>
                        },
                <?php
                    }
                }
            } else {
                ?>
                center: {
                    lat: 40.869176,
                    lng: -74.943978
                },
            <?php
            }
            ?>
            zoom: 10,
            styles: [{
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }]
            }, {
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#616161"
                }]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "color": "#f5f5f5"
                }]
            }, {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#bdbdbd"
                }]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#eeeeee"
                }]
            }, {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#757575"
                }]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#e5e5e5"
                }]
            }, {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#9e9e9e"
                }]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }]
            }, {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#757575"
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#dadada"
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#616161"
                }]
            }, {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#9e9e9e"
                }]
            }, {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#e5e5e5"
                }]
            }, {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#eeeeee"
                }]
            }, {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#c9c9c9"
                }]
            }, {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#9e9e9e"
                }]
            }]
        });
        //var warrenCountyCoordinates = [

        <?php
        //foreach ($properties as $lon) {
        ?>
        //    {lat: <?php //echo $lon->latitute    
                    ?>, lng: <?php //echo $lon->longitude    
                                ?>},
        <?php // }    
        ?>
        //];
        // Random polygon covering northeast region
        //var everythingElse = [
        <?php
        //foreach ($properties as $lon) {
        ?>
        //    {lat: <?php //echo $lon->latitute    
                    ?>, lng: <?php //echo $lon->longitude    
                                ?>},
        <?php // }    
        ?>
        //];
        //var warrenCountyBorder = new google.maps.Polygon({
        //paths: [everythingElse, warrenCountyCoordinates],
        //        strokeColor: '#000000',
        //        strokeOpacity: 0.8,
        //        strokeWeight: 2,
        //        fillColor: '#000000',
        //        fillOpacity: 0.35
        //});
        var breweries = [
            <?php
            if (count($properties)) {
                foreach ($properties as $lon) {
                    $image = asset($lon->image);
            ?> {
                        position: new google.maps.LatLng(<?php echo $lon->latitute ?>, <?php echo $lon->longitude ?>),
                        desc: '<div id="content">' +
                            '<div id="siteNotice">' +
                            '</div>' +
                            '<h1 id="firstHeading" class="firstHeading"><?php echo $lon->listing_by ?></h1>' +
                            '<div id="bodyContent">' +
                            '<p>The <b>Summerfield Brews</b> <?php echo $lon->address; ?>' +
                            '</p><br>' +
                            '<IMG class="ImageBorder" SRC="<?php echo $image ?>' +
                            '</div>' +
                            '</div>'
                    },
            <?php
                }
            }
            ?>
        ];
        var currentMarker = false

        function addMarker(brewery) {
            var marker = new google.maps.Marker({
                position: brewery.position,
                icon: 'http://www.jeffs-icons.com/map_icons/ICONS/Pin/pin-red_violet-R.png',
                map: map,
                zIndex: brewery[1]
            });
            marker.info = new google.maps.InfoWindow({
                content: brewery.desc,
                maxWidth: 300
            });
            // When marker is clicked, open infowindow and close any open ones
            google.maps.event.addListener(marker, 'click', function() {
                if (currentMarker) {
                    currentMarker.close();
                }
                currentMarker = marker.info
                currentMarker.open(map, marker);
            });
            // The same 'center' coordinates from initMap()  
            //          var myLatlng =  new google.maps.LatLng(40.869176, -74.943978);

            // If user clicks in map, close infowindow(s)
            //          google.maps.event.addListener(map, 'click', function() {
            //            marker.info.close(map, marker);
            //            map.setCenter(myLatlng);
            //          });

            // If user clicks in surrounding area, close infowindow(s)
            //          google.maps.event.addListener(warrenCountyBorder, 'click', function() {
            //            marker.info.close(warrenCountyBorder, marker);
            //            map.setCenter(myLatlng);
            //          });

        } // End addMarker() function

        // create marker from each brewery in our list
        for (var i = 0, brewery; brewery = breweries[i]; i++) {
            addMarker(brewery);
        }

        // Add the county border layer to the map
        //warrenCountyBorder.setMap(map);
    }

    function aaa(id) {
        /* e.preventDefault(); */
        var productId = id;
        var baseUrl = $('meta[name="base_url"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: baseUrl + "/property-detail",
            type: "post",
            data: {
                id: productId,
            },
            success: function(data) {
                /* alert("okk"); */
                if (data) {
                    $('.propertyPopupBody').html(data);
                    $.magnificPopup.open({
                        items: {
                            src: '#landDetail-popup',
                            callbacks: {
                                beforeOpen: function() {
                                    this.st.mainClass = this.st.el.attr('data-effect');
                                }
                            },
                            type: 'inline'
                        }

                    });
                }
            }
        });
    }
    $(document).ready(function() {
        $('.productList').on('click', function(e) {
            e.preventDefault();
            var productId = $(this).data('id');
            var baseUrl = $('meta[name="base_url"]').attr('content');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: baseUrl + "/property-detail",
                type: "post",
                data: {
                    id: productId,
                },
                success: function(data) {
                    if (data) {
                        $('.propertyPopupBody').html(data);
                        $.magnificPopup.open({
                            items: {
                                src: '#landDetail-popup',
                                callbacks: {
                                    beforeOpen: function() {
                                        this.st.mainClass = this.st.el.attr('data-effect');
                                    }
                                },
                                type: 'inline'
                            }

                        });
                    }
                }
            });
        });
        $('#propertySortBy').on('change', function() {
            var filter = $(this).val();
            $('.filterType').val(filter);
            $('#propertyFilterForm').submit();
        });
    });
</script>
<script>
    $(window).on('load', function() {
        $(".hidden").hide();
    })
    var page = 1;
    /* load_more(page); */
    $(".slider1").scroll(function() {

        if ($(".slider1").scrollTop() + $(".slider1").height() >= $(".slider1").height()) { //if user scrolled from top to bottom of the page
            /* alert("hjhjhj"); */
            page++;
            load_more(page);
        }
    });

    function load_more(page) {
        /* alert("okk"); */
        $.ajax({
                url: '?page=' + page,
                type: "get",
                datatype: "html",
                beforeSend: function() {
                    $('.ajax-loading').show();
                }
            })
            .done(function(buyland) {
                /* console.log(buyland.length); */

                if (buyland.length == "") {

                    $('.ajax-loading').html("No more records!");
                    return;
                }
                $('.ajax-loading').hide();
                $("#results").append(buyland.htmll);
            })
            .fail(function(jqXHR, ajaxOptions, thrownError) {
                //   alert('No response from server');
            });
    }
</script>
@endsection