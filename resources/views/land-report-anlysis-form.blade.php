@extends('layouts.master')
@section('title')
  Home
@endsection
@section('content')
<section class="commonFormPage adjustHeaderSpace">
    <div class="container">
        <div class="formPageHeading">
            <div class="row justify-content-center">
                <div class="col-sm-8">
                    <h1>Please Fill Out the Form to Generate Your Report Anaylsis</h1>
                    <h2>With the help of this form you will be generating your land report.</h2>
                </div>
            </div>
        </div><!--//formPageHeading-->


        <!--multisteps-form-->
        <div class="multisteps-form">
            <!--progress bar-->
            <div class="row justify-content-center">
                <div class="col-12 col-lg-11 col-md-11">
                    <div class="multisteps-form__progress">
                        <div class="multisteps-form__progress-btn js-active" title="Who you are">
                            <span class="noCount">01</span>
                            <span>Step 1</span>
                            <h3>Basic Details</h3>
                        </div>
                        <div class="multisteps-form__progress-btn" title="Basic Details">
                            <span class="noCount">02</span>
                            <span>Step 2</span>
                            <h3>Confirmation</h3>
                        </div>
                        <div class="multisteps-form__progress-btn" title="Payment">
                            <span class="noCount">03</span>
                            <span>Step 3</span>
                            <h3>Payment</h3>
                        </div>
                    </div>
                </div>
            </div>

            <!--form panels-->
            <div class="multiformContainer">
                <form class="multisteps-form__form">
                    <!--single form panel-->
                    <div class="multisteps-form__panel shadow rounded clearfix js-active" data-animation="fadeIn">
                        <h3 class="multisteps-form__title">Basic Details</h3>
                        <div class="multisteps-form__content">

                            <div class="form-filds">
                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="remarks" type="text" placeholder=" "/>
                                                <label for="remarks">Name</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label mobileNo">
                                                <input class="form-control" id="remarks" type="mobile" placeholder=" "/>
                                                <label for="remarks">Mobile Number</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="remarks" type="email" placeholder=" "/>
                                                <label for="remarks">Email id</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="remarks" type="text" placeholder=" "/>
                                                <label for="remarks">Full Address</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <span class="has-float-label">
                                                <select class="form-control select-label" placeholder="" onclick="this.setAttribute('value', this.value);" value="">
                                                    <option value=""></option>
                                                    <option value="1">Item1</option>
                                                    <option value="2">Item2</option>
                                                    <option value="3">Item3</option>
                                                    <option value="4">Item4</option>
                                                    <option value="5">Item5</option>
                                                </select>
                                                <label>State</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <span class="has-float-label">
                                                <select class="form-control select-label" onclick="this.setAttribute('value', this.value);" value="" placeholder="">
                                                    <option value=""></option>
                                                    <option value="1">Item1</option>
                                                    <option value="2">Item2</option>
                                                    <option value="3">Item3</option>
                                                    <option value="4">Item4</option>
                                                    <option value="5">Item5</option>
                                                </select>
                                                <label for="remarks">District</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <span class="has-float-label">
                                                <select class="form-control select-label" onclick="this.setAttribute('value', this.value);" value="" placeholder="">
                                                    <option value=""></option>
                                                    <option value="1">Item1</option>
                                                    <option value="2">Item2</option>
                                                    <option value="3">Item3</option>
                                                    <option value="4">Item4</option>
                                                    <option value="5">Item5</option>
                                                </select>
                                                <label for="remarks">Village</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile">Sazra</label>
                                            <span>Pdf, Png, Jpeg upto 5 mb</span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="selectRadio w-100">
                                        <label>Do you want Land analysis Report ?</label>
                                        <div class="d-flex flex-row">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="report" name="report" value="customEx">
                                                <label class="custom-control-label" for="report">Yes</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="reportNo" name="report" value="customEx">
                                                <label class="custom-control-label" for="reportNo">No</label>
                                            </div>
                                        </div>
                                    </div><!-- //selectRadio -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="selectRadio w-100">
                                        <label>Are you interesting in Selling  Land ?</label>
                                        <div class="d-flex flex-row">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="land" name="land" value="customEx">
                                                <label class="custom-control-label" for="land">Yes</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="landNo" name="land" value="customEx">
                                                <label class="custom-control-label" for="landNo">No</label>
                                            </div>
                                        </div>
                                    </div><!-- //selectRadio -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="selectRadio w-100">
                                        <label>Are you interesting in Selling more Land ?</label>
                                        <div class="d-flex flex-row">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="selling" name="selling" value="customEx">
                                                <label class="custom-control-label" for="selling">Yes</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="sellingNo" name="selling" value="customEx">
                                                <label class="custom-control-label" for="sellingNo">No</label>
                                            </div>
                                        </div>
                                    </div><!-- //selectRadio -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="privacyPolicy" name="privacyPolicy">
                                        <label class="custom-control-label" for="privacyPolicy">I agree to the <span> Privacy Policy, Terms of services</span> and <span> Cookie Policy</span> of this Website</label>
                                    </div>
                                </div><!-- //form-row mt-4 -->

                            </div><!-- //form-filds -->

                            <div class="button-row mt-5 clearfix">
                                <h4>
                                    <label>You need to Pay</label>
                                    INR <span>22,500</span>
                                </h4>
                                <button class="btn btn-primary js-btn-next" type="button" title="Next">Submit</button>
                            </div>
                        </div>
                    </div>

                    <!--single form panel-->
                    <div class="multisteps-form__panel clearfix shadow rounded" data-animation="fadeIn">
                        <!-- <h3 class="multisteps-form__title">Basic Details</h3> -->
                        <div class="multisteps-form__content">

                            <div class="form-filds">
                                <div class="form-row mt-4">
                                    <div class="col-md-6 col-8">
                                        <address>
                                            <h4>Abhit Bhatia</h4>
                                            <span>+91-9560465467</span>
                                            <span>abhitbhatia1@gmail.com</span>
                                        </address>
                                    </div><!-- //col-md-6 col-12 -->

                                    <div class="col-md-6 col-4">
                                        <button class="js-btn-prev float-right">
                                            Edit your Detail
                                        </button>
                                    </div><!-- //col-md-6 col-12 -->
                                </div><!-- //form-row mt-4 -->
                            </div><!-- //form-filds -->

                            <div class="button-row mt-5 clearfix">
                                <h4>
                                    <label>You need to Pay</label>
                                    INR <span>22,500</span>
                                </h4>
                                <button class="btn btn-primary js-btn-next" type="button" title="Next">Continue & Pay</button>
                            </div>
                        </div>
                    </div>

                    <!--single form panel-->
                    <div class="multisteps-form__panel clearfix shadow rounded" data-animation="fadeIn">
                        <h3 class="multisteps-form__title">Payment</h3>
                        <div class="multisteps-form__content">

                                <div class="form-filds">
                                    <h4 class="card-type mt-4 mb-2">Card Type</h4>
                                    <div class="cards-sections d-flex justify-content-between" id="selectCard">
                                        <div class="card">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="card1" name="card" value="customEx">
                                                <label class="custom-control-label text-center" for="card1">
                                                    <img src="images/master-card-black.jpg" alt="" class="black">
                                                    <img src="images/master-card-color.jpg" alt="" class="color">
                                                </label>
                                            </div>
                                        </div><!-- //card -->
                                        <div class="card">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="card2" name="card" value="customEx">
                                                <label class="custom-control-label text-center" for="card2">
                                                    <img src="images/visa-black.jpg" alt="" class="black">
                                                    <img src="images/visa-color.jpg" alt="" class="color">
                                                </label>
                                            </div>
                                        </div><!-- //card -->
                                        <div class="card">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="card3" name="card" value="customEx">
                                                <label class="custom-control-label text-center" for="card3">
                                                    <img src="images/paytm-black.jpg" alt="" class="black">
                                                    <img src="images/paytm-color.jpg" alt="" class="color">
                                                </label>
                                            </div>
                                        </div><!-- //card -->
                                    </div><!-- //cards-sections -->

                                    <div class="form-row mt-4">
                                        <div class="col-12">
                                            <div class="form-group input-group">
                                                <span class="has-float-label">
                                                    <input class="form-control" id="cardName" type="text" placeholder=" "/>
                                                    <label for="cardName">Name on Card</label>
                                                </span>
                                            </div>
                                        </div><!-- //col-12 -->
                                    </div><!-- //form-row mt-4 -->

                                    <div class="form-row mt-4">
                                        <div class="col-12">
                                            <div class="form-group input-group">
                                                <span class="has-float-label">
                                                    <input class="form-control" id="cardNumber" type="text" placeholder=" "/>
                                                    <label for="cardNumber">Card Number</label>
                                                </span>
                                            </div>
                                        </div><!-- //col-12 -->
                                    </div><!-- //form-row mt-4 -->

                                    <div class="form-row mt-4">
                                        <div class="col-7">
                                            <div class="form-group input-group">
                                                <span class="has-float-label">
                                                    <input class="form-control" id="expirationDate" type="text" placeholder=" "/>
                                                    <label for="expirationDate">Expiration Date</label>
                                                </span>
                                            </div>
                                        </div><!-- //col-12 -->
                                        <div class="col-5">
                                            <div class="form-group input-group">
                                                <span class="has-float-label">
                                                    <input class="form-control" id="cvv" type="text" placeholder=" "/>
                                                    <label for="cvv">CVV</label>
                                                </span>
                                            </div>
                                        </div><!-- //col-12 -->
                                    </div><!-- //form-row mt-4 -->
                                </div><!-- //form-filds -->

                            <div class="button-row mt-4">
                                <h4>
                                    <label>You need to Pay</label>
                                    INR <span>22,500</span> <span class="month-count">for 6 months</span>
                                </h4>
                                <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Continue</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div><!--//multiformContainer-->
        </div>

    </div><!-- //container -->
</section><!-- //commonFormPage -->

@endsection
