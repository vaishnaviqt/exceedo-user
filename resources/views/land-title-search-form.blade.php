@extends('layouts.master')
@section('title')
land Title Search
@endsection
@section('content')
<section class="commonFormPage adjustHeaderSpace">
    <div class="container">
        <div class="formPageHeading">
            <div class="row justify-content-center">
                <div class="col-sm-8">
                    <h1>{{ __('Please Fill Out the Form to Generate Your Report') }}</h1>
                    <h2>{{ __('With the help of this form you will be generating your land report.') }}</h2>
                </div>
            </div>
        </div>
        <!--//formPageHeading-->
        <!--multisteps-form-->
        <div class="multisteps-form">
            <!--progress bar-->
            <div class="row justify-content-center">
                <div class="col-12 col-lg-11 col-md-11">
                    <div class="multisteps-form__progress">
                        <div class="multisteps-form__progress-btn js-active" title="Who you are">
                            <span class="noCount">{{ __('01') }}</span>
                            <span>{{ __('Step 1') }}</span>
                            <h3>{{ __('Basic Details') }}</h3>
                        </div>
                        <div class="multisteps-form__progress-btn" title="Basic Details">
                            <span class="noCount">{{ __('02') }}</span>
                            <span>{{ __('Step 2') }}</span>
                            <h3>{{ __('Confirmation') }}</h3>
                        </div>
                        <div class="multisteps-form__progress-btn" title="Payment">
                            <span class="noCount">{{ __('03') }}</span>
                            <span>{{ __('Step 3') }}</span>
                            <h3>{{ __('Payment') }}</h3>
                        </div>
                    </div>
                </div>
            </div>

            <!--form panels-->
            <div class="multiformContainer">
                <form class="multisteps-form__form" method="post" action="{{ url('store-land-report-detail') }}" id="landMultiStepForm" enctype="multipart/form-data">
                    @csrf
                    <!--single form panel-->
                    <div class="multisteps-form__panel shadow rounded clearfix js-active" data-animation="fadeIn">
                        <h3 class="multisteps-form__title">{{ __('Basic Details') }}</h3>
                        <div class="multisteps-form__content">
                            <div class="form-filds">
                                @if(!Auth::check())
                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="name" type="text" placeholder="Enter Full Name" name="name" />
                                                <label for="remarks">{{ __('Name') }} <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label mobileNo">
                                                <input class="form-control" id="mobile" type="mobile" placeholder="Enter Mobile Number" name="mobile" />
                                                <label for="remarks">{{ __('Mobile Number') }} <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="email" type="email" placeholder="Enter Email" name="email" />
                                                <label for="email">{{ __('Email id') }} <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->
                                @endif
                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="land_name" type="text" placeholder="Enter Land Name" name="land_name" />
                                                <label for="land_name">{{ __('Land Name') }} <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="size_of_area" type="hidden" value="{{ $value->size_of_area }}" name="size_of_area" />
                                                <input class="form-control" id="amount" type="hidden" value="{{ $value->gst }}" name="amount" />
                                                <input class="form-control" id="user_id" type="hidden" value="{{ auth()->user()->id ?? '' }}" name="user_id" />
                                                <input class="form-control" id="order_id" type="hidden" value="" name="order_id" />
                                                <input class="form-control" id="value_added_service_id" type="hidden" value="{{ $value->value_added_service }}" name="service_id" />
                                                <input class="form-control" id="address" type="text" placeholder="Enter full address" name="address" />
                                                <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" value="">
                                                <input type="hidden" name="razorpay_order_id" id="razorpay_order_id" value="">
                                                <input type="hidden" name="razorpay_signature" id="razorpay_signature" value="">
                                                <label for="address">{{ __('Full Address') }} <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <span class="has-float-label">
                                                <select class="form-control select-label" onclick="this.setAttribute('value', this.value);" id="state_id" name="state_id">
                                                    <option value="">--</option>
                                                    @foreach($states as $state)
                                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="state_id">{{ __('State') }} <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="city" type="text" placeholder="Enter Village/Town/District" name="city" />
                                                <label for="city">{{ __('Village/Town/District') }} <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="custom-file">

                                            <input type="file" class="custom-file-input" id="customFile" name="image" required>
                                            <label class="custom-file-label" for="customFile">{{ __('Sazra') }}<span class="text-danger">*</span></label>
                                            <span>{{ __('Pdf, Png, Jpeg upto 5 mb') }}</span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="selectRadio w-100">
                                        <label>{{ __('Do you want Land analysis Report ?') }} <span class="text-danger">*</span></label>
                                        <div class="d-flex flex-row">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="report" name="analysis_report" value="1">
                                                <label class="custom-control-label" for="report">{{ __('Yes') }}</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="reportNo" name="analysis_report" value="1">
                                                <label class="custom-control-label" for="reportNo">{{ __('No') }}</label>
                                            </div>
                                        </div>
                                    </div><!-- //selectRadio -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="selectRadio w-100">
                                        <label>{{ __('Are you interesting in Selling Land ?') }} <span class="text-danger">*</span></label>
                                        <div class="d-flex flex-row">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="land" name="selling_more" value="1">
                                                <label class="custom-control-label" for="land">{{ __('Yes') }}</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="landNo" name="selling_more" value="0">
                                                <label class="custom-control-label" for="landNo">{{ __('No') }}</label>
                                            </div>
                                        </div>
                                    </div><!-- //selectRadio -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="privacyPolicy" name="terms" value="1">
                                        <label class="custom-control-label" for="privacyPolicy">I agree to the <span><a target="_blank" href="{{url('/privacy-policy')}}"> Privacy Policy</a></span>,<span><a target="_blank" href="{{url('/terms-conditions')}}"> Terms of services</a></span> and <span><a target="_blank" href="{{url('/cookie-policy')}}">Cookie Policy</a></span> of this Website </label>
                                    </div>
                                </div><!-- //form-row mt-4 -->
                            </div><!-- //form-filds -->

                            <div class="button-row mt-5 clearfix appendButton">
                                <div class="reportTotalVal">
                                    <span>{{ __('You need to Pay') }}</span> INR {{ $value->gst_value }}
                                    <span>{{ $value->amount}} + 18% GST</span>
                                </div>
                                <button class="btn btn-primary landReportSubmitButton" type="button" title="Next">{{ __('Submit') }}</button>
                            </div>
                        </div>
                    </div>

                    <!--single form panel-->
                    <div class="multisteps-form__panel clearfix shadow rounded" data-animation="fadeIn">
                        <!-- <h3 class="multisteps-form__title">Basic Details</h3> -->
                        <div class="multisteps-form__content">

                            <div class="form-filds">
                                <div class="form-row mt-4">
                                    <div class="col-md-6 col-8">
                                        <address class="landReportDetail">
                                            @if(Auth::check())
                                            <h4>{{ auth()->user()->name }}</h4>
                                            <span>{{ auth()->user()->mobile }}</span>
                                            <span>{{ auth()->user()->email }}</span>
                                            @endif
                                        </address>
                                    </div><!-- //col-md-6 col-12 -->

                                    <div class="col-md-6 col-4">
                                        <button class="js-btn-prev float-right backPrevious" type="button">
                                            Edit your Detail
                                        </button>
                                    </div><!-- //col-md-6 col-12 -->
                                </div><!-- //form-row mt-4 -->
                            </div><!-- //form-filds -->

                            <div class="button-row mt-5 clearfix">
                                <div class="reportTotalVal">
                                    <span>{{ __('You need to Pay') }}</span> INR {{ $value->gst_value }}
                                    <span>{{ $value->amount}} + 18% GST</span>
                                </div>
                                <button class="btn btn-primary landTitleSearchButton" type="button" title="Next">{{ __('Continue & Pay') }}</button>
                            </div>
                        </div>
                    </div>
                </form>

                <!--single form panel-->
                <div class="multisteps-form__panel clearfix shadow rounded" data-animation="fadeIn">
                    <h3 class="multisteps-form__title">Payment</h3>
                    <div class="multisteps-form__content">

                        <div class="form-filds">
                            <h4 class="card-type mt-4 mb-2">Card Type</h4>
                            <div class="cards-sections d-flex justify-content-between" id="selectCard">
                                <div class="card">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="card1" name="card" value="customEx">
                                        <label class="custom-control-label text-center" for="card1">
                                            <img src="images/master-card-black.jpg" alt="" class="black">
                                            <img src="images/master-card-color.jpg" alt="" class="color">
                                        </label>
                                    </div>
                                </div><!-- //card -->
                                <div class="card">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="card2" name="card" value="customEx">
                                        <label class="custom-control-label text-center" for="card2">
                                            <img src="images/visa-black.jpg" alt="" class="black">
                                            <img src="images/visa-color.jpg" alt="" class="color">
                                        </label>
                                    </div>
                                </div><!-- //card -->
                                <div class="card">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="card3" name="card" value="customEx">
                                        <label class="custom-control-label text-center" for="card3">
                                            <img src="images/paytm-black.jpg" alt="" class="black">
                                            <img src="images/paytm-color.jpg" alt="" class="color">
                                        </label>
                                    </div>
                                </div><!-- //card -->
                            </div><!-- //cards-sections -->

                            <div class="form-row mt-4">
                                <div class="col-12">
                                    <div class="form-group input-group">
                                        <span class="has-float-label">
                                            <input class="form-control" id="cardName" type="text" placeholder=" " />
                                            <label for="cardName">Name on Card</label>
                                        </span>
                                    </div>
                                </div><!-- //col-12 -->
                            </div><!-- //form-row mt-4 -->

                            <div class="form-row mt-4">
                                <div class="col-12">
                                    <div class="form-group input-group">
                                        <span class="has-float-label">
                                            <input class="form-control" id="cardNumber" type="text" placeholder=" " />
                                            <label for="cardNumber">Card Number</label>
                                        </span>
                                    </div>
                                </div><!-- //col-12 -->
                            </div><!-- //form-row mt-4 -->

                            <div class="form-row mt-4">
                                <div class="col-7">
                                    <div class="form-group input-group">
                                        <span class="has-float-label">
                                            <input class="form-control" id="expirationDate" type="text" placeholder=" " />
                                            <label for="expirationDate">Expiration Date</label>
                                        </span>
                                    </div>
                                </div><!-- //col-12 -->
                                <div class="col-5">
                                    <div class="form-group input-group">
                                        <span class="has-float-label">
                                            <input class="form-control" id="cvv" type="text" placeholder=" " />
                                            <label for="cvv">CVV</label>
                                        </span>
                                    </div>
                                </div><!-- //col-12 -->
                            </div><!-- //form-row mt-4 -->
                        </div><!-- //form-filds -->

                        <div class="button-row mt-4">
                            <div class="reportTotalVal">
                                <span>{{ __('You need to Pay') }}</span> INR {{ $value->gst_value }}
                                <span>{{ $value->amount}} + 18% GST</span>
                            </div>
                            <button class="btn btn-primary ml-auto" type="button" title="Next">Continue</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--//multiformContainer-->
        </div>


    </div><!-- //container -->
</section><!-- //commonFormPage -->
@endsection

@section('custom-script')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
    $(document).ready(function() {
        $('body').on('click', '.backPrevious', function() {
            $('.appendButton').append('<button class="btn btn-primary landReportSubmitButton" type="button" title="Next">Submit</button>');
        });

        $('body').on('click', '.landReportSubmitButton', function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "post",
                url: "land-report-validation",
                data: $('#landMultiStepForm').serialize(),
                success: function(data) {
                    $('.landReportSubmitButton').addClass('js-btn-next');
                    $('.landReportSubmitButton').click();
                    $('.landReportSubmitButton').remove();
                    if (data.user) {
                        $('#user_id').val(data.user.id);
                    }
                    if (data.order_id) {
                        $('#order_id').val(data.order_id);
                    }

                    var name = $('#name').val();
                    var email = $('#email').val();
                    var mobile = $('#mobile').val();
                    var html = `<h4>` + name + `</h4>
                    <span>` + mobile + `</span>
                    <span>` + email + `</span>`;
                    if (name != '') {
                        $('.landReportDetail').html(html);
                    }

                },
                error: function(errorResponse) {
                    $('.error-span').remove();
                    $.each(errorResponse.responseJSON.errors, function(field_name, error) {
                        if (field_name == 'analysis_report') {
                            $(document).find('[name=' + field_name + ']').parent().parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else if (field_name == 'selling_more') {
                            $(document).find('[name=' + field_name + ']').parent().parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        } else {
                            $(document).find('[name=' + field_name + ']').parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                        }
                    })
                }
            });
        });

        $('.landTitleSearchButton').on('click', function() {
            var name = "{{ auth()->user()->name ?? '' }}";
            var email = "{{ auth()->user()->email ?? '' }}";
            var mobile = "{{ auth()->user()->name ?? '' }}";
            var amount = $('#amount').val();
            if (name == '') {
                var name = $('#name').val();
            }
            if (email == '') {
                var email = $('#email').val();
            }
            if (mobile == '') {
                var mobile = $('#mobile').val();
            }
            var options = {
                "key": "{{ config('app.razorpay_api_key') }}", // Enter the Key ID generated from the Dashboard
                "amount": amount * 100, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                "currency": "{{ config('app.currency') }}",
                "name": "{{ config('app.account_name') }}",
                "description": '',
                "image": "{{ asset('images/logo-black.svg') }}",
                "order_id": $('#order_id').val(), //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                "handler": function(response) {
                    $('#razorpay_payment_id').val(response.razorpay_payment_id);
                    $('#razorpay_order_id').val(response.razorpay_order_id);
                    $('#razorpay_signature').val(response.razorpay_signature);
                    $('#landMultiStepForm').submit();

                },
                "prefill": {
                    "name": name,
                    "email": email,
                    "contact": mobile
                },
                //            "notes": {
                //                "address": "Razorpay Corporate Office"
                //            },
                "theme": {
                    "color": "#3399cc"
                }
            };
            var rzp1 = new Razorpay(options);
            rzp1.on('payment.failed', function(response) {

            });

            rzp1.open();
        });
    });
</script>
@endsection