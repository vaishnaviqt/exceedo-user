

                    @foreach($properties as $property)
                    <div class="col-sm-6" >
                        <article class="featureArticle">
                            <div class="featureArtImg">
                                <a href="{{ url('buy-your-land') }}">
                                    @php
                                    $path = '';
                                    if(count($property->propertyImage)){
                                    $path = asset($property->propertyImage[0]->image);
                                    }
                                    @endphp
                                    <img src="{{ asset('').$property->image }}" alt="{{ $property->land_name }}">
                                </a>
                            </div>
                            <div class="featureArtCont">
                                <h3>
                                    <a href="{{ url('buy-your-land') }}">
                                        <span class="featlinkText">
                                            {{ $property->size_of_land }} Acres {{ $property->land_name }} 
                                        </span>
                                        <em class="featureIcon"></em>
                                    </a>
                                </h3>
                                <p class="shortDetail">{{ $property->address }}</p>
                                <div class="featArtPric">₹ {{ $property->max_price }} Cr</div>
                                @if($property->wifi == 1 || $property->parking == 1) 
                                <div class="featFacilities">
                                    <span class="featFaTitle">Facilities :</span>
                                    @if($property->parking == 1)
                                    <span class="faci-icon parking-icon"></span>
                                    @endif
<!--                                    <span class="faci-icon noSmooking-icon"></span>-->
                                    @if($property->wifi == 1)
                                    <span class="faci-icon wifi-icon"></span>
                                    @endif
                                </div><!--//featFacilities-->
                                @endif
                            </div>
                    </div><!--//col-sm-6-->
                    @endforeach
               