@extends('layouts.master')
@section('title')
Home
@endsection
@section('splashscreen')
<div class="innerPageBanner landTitleSearchBanner feasibiltiyBanner">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Feasibility Analysis</h1>
                <p>Know Your Land Report is an exclusive information-cum-guiding document customized to give you a detailed analysis of prevailing conditions as well as a holistic picture of the prospects and potential of your land. The report not only gives clarity on legal and technical details but also lets you gain insights beyond the obvious and into the future.</p>
                <a href="{{ asset('report/report.pdf') }}" target="_blank" class="btn btn-primary">Download Sample Report</a>
            </div>
            <div class="col-md-6">
                <div class="mobileBanImageSpace"></div>
            </div>
        </div>
        <!--//row-->
    </div>
    <!--//container-->
</div>
<!--//innerPageBanner-->


@endsection
@section('content')

<section class="quoteReportSection" id="quoteReportSection">
    <div class="container">
        <div class="subSecHeading text-center">
            <h2>Get An Approximate Quote For Your Report</h2>
        </div>
        <!--//subSecHeading-->

        <div class="landReportForm">
            <form action="{{ url('land-title-search-form') }}" method="post" id="landSearchForm">
                @csrf
                <div class="landReportFormBox">
                    <label class="form-label">Please enter your land in acres to get calculate your amount for your report</label>

                    <div class="form-group floating-field">
                        <input type="hidden" class="form-control" id="amount" name="amount" value="">
                        <input type="hidden" class="form-control" id="gst_value" name="gst_value" value="">
                        <input type="hidden" class="form-control" id="gst" name="gst" value="">
                        <input type="hidden" class="form-control" id="land_title_search" name="value_added_service" value="2">
                        <input type="text" class="form-control" placeholder="Enter your land area" id="area" name="size_of_area">
                        <label for="area" class="floating-label">Enter your land area</label>
                        <span class="fieldFormat">Acres</span>
                    </div><!-- floating-field -->

                    <button class="btn btn-primary-bordered calculateButton">Calculate Amount</button>
                </div>
                <!--//landReportFormBox-->
                <div class="landRepBottomBox">
                    <div class="row align-items-center">
                        <div class="col-sm-6">
                            <div class="reportTotalVal">

                            </div>
                        </div>
                        <div class="col-sm-6 text-right">
                            <button class="btn btn-primary requestTitleSearchButton">Request title search</button>
                        </div>
                    </div>
                </div>
                <!--//landRepBottomBox-->
            </form>
        </div>
        <!--//landReportForm-->
    </div>
    <!--//container-->
</section>
<!--//quoteReport-->
<script>
    $(document).ready(function() {
        $('.calculateButton').on('click', function() {
            var area = $('#area').val();
            $('.error-message').remove();
            if (area == '') {
                $('#area').after('<span class="text-danger error-message">Land area field required</span>')
                return false;
            } else if (!$.isNumeric(area)) {
                $('#area').after('<span class="text-danger error-message">Land area filed only number required</span>')
                return false;
            }
            if (area > 10) {
                var amount = 67000 + (area - 10) * 3000;
            } else if (area > 5 && area <= 10) {
                var amount = 52000 + (area - 5) * 3000;
            } else if (area > 1 && area <= 5) {
                var amount = 20000 + (area - 1) * 8000;
            } else {
                var amount = 20000;
            }
            var gst = parseInt(amount + (amount * 18 / 100)).toLocaleString('en-IN');
            $('#amount').val(parseInt(amount).toLocaleString('en-IN'));
            $('#gst').val(gst);
            $('.reportTotalVal').html('<span>You need to Pay</span> INR ' + gst + "<span>(" + parseInt(amount).toLocaleString('en-IN') + "+18% GST)</span>");
            return false;
        });

        $('.requestTitleSearchButton').on('click', function() {
            var area = $('#area').val();
            var amount = $('#amount').val();
            if (area == '') {
                $('#area').after('<span class="text-danger error-message">Land area field required</span>')
                return false;
            } else if (!$.isNumeric(area)) {
                $('#area').after('<span class="text-danger error-message">Land area filed only number required</span>')
                return false;
            }
            if (area > 10) {
                var amount = 67000 + (area - 10) * 3000;
            } else if (area > 5 && area <= 10) {
                var amount = 52000 + (area - 5) * 3000;
            } else if (area > 1 && area <= 5) {
                var amount = 20000 + (area - 1) * 8000;
            } else {
                var amount = 20000;
            }
            var gst = parseInt(amount + (amount * 18 / 100)).toLocaleString('en-IN');
            var gst_value = amount + (amount * 18 / 100);
            $('#amount').val(parseInt(amount).toLocaleString('en-IN'));
            $('#gst').val(gst_value);
            $('#gst_value').val(gst);
            $('.reportTotalVal').html('<span>You need to Pay</span> INR ' + gst + "<span>(" + parseInt(amount).toLocaleString('en-IN') + "+18% GST)</span>");
            $('#landSearchForm').submit();

        });
    });
</script>
@endsection