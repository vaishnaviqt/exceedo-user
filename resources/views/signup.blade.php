@extends('layouts.master')
@section('title')
Sign Up
@endsection
@section('content')

<section class="signup-page">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="signupLeftContent">
                    <span class="headingTopLine"></span>
                    <h1>Welcome</h1>
                    <h2>To Know Your Land</h2>
                    
                </div>
            </div>
            <!--//col-md-6-->
            <div class="col-md-6">

                <div class="userSignBox">
                    <div class="userSignBox_scrollBox signupBox">

                        <div class="socialSignup">
                            <h3>- EASILY USING - </h3>
                            <div class="socialLoginBtn">
                                <a href="{{ url('login/user/facebook') }}" class="facebookLogin">Facebook</a>
                                <a href="{{ url('login/user/google') }}" class="googleLogin">Google</a>
                            </div>
                            <!--//socialLoginBtn-->
                        </div>
                        <!--//socialSignup-->

                        <div class="signupForm">
                            <h3>- OR USING EMAIL -</h3>

                            <form id="user_signup" method="post" name="frm_user_signup">
                                @csrf
                                <div class="alert alert-success alert-block user-msg1" style="display: none;">

                                    <button type="button" class="close" data-dismiss="alert">Ã—</button>
                                </div>
                                <div class="form-group floating-field mobile-field">
                                    <input type="text" class="form-control mobile" placeholder="Mobile Number" id="mobile_1" name="mobile_number">
                                    <label for="mobile_1" class="floating-label">Mobile Number</label>
                                    <span class="mobileCode">+91</span>
                                    <a href="javascript:void(0);" id="send_otp_user" class="sendOTP_Link">Send OTP</a>
                                    <span id="userOtp"></span>
                                </div><!-- floating-field -->

                                <!-- <div class="form-group floating-field">
                                    <input type="text" class="form-control" placeholder="Mobile Number" id="mobile">
                                    <label for="mobile" class="floating-label">Mobile Number</label>
                                </div>  -->

                                <div class="form-group floating-field">
                                    <input type="text" class="form-control" placeholder="OTP" id="OTP" required name="otp">
                                    <label for="OTP" class="floating-label">OTP</label>
                                </div><!-- floating-field -->

                                <div class="form-group floating-field">
                                    <input type="text" class="form-control" placeholder="Full Name" id="fullName" name="full_name" required>
                                    <label for="fullName" class="floating-label">Full Name</label>
                                </div><!-- floating-field -->

                                <div class="form-group floating-field mb-4">
                                    <input type="text" class="form-control" placeholder="Email Id" id="email" name="email" required>
                                    <label for="email" class="floating-label">Email Id</label>
                                </div><!-- floating-field -->

                                <div class="loginTermsCheck">
                                    <div class="customCheckbox">
                                        <input type="checkbox" name="terms" id="terms" required>
                                        <label for="terms">&nbsp;</label>
                                    </div>
                                    <span>I agree to the <a href="javascript:void(0);">Privacy Policy</a>, <a href="javascript:void(0);">Terms of services</a> and <a href="javascript:void(0);">Cookie Policy</a> of this Website</span>
                                </div>

                                <div class="LoginBtn">
                                    <button type="submit" id="btn_user" class="btn btn-primary">Sign up</button>
                                </div>

                       

                                <div class="accountSwitchOpt">
                                    You have an account ? <a href="{{ url('login') }}">Log in</a>
                                </div>
                            </form>
                        </div>
                        <!--//signupForm-->
                    </div>
                    <!--//userSignBox_scrollBox-->
                </div>
                <!--//userSignBox-->

            </div>
            <!--//col-md-6-->
        </div>
        <!--//row-->
    </div>
    <!--//container-->
</section>
<!--//signup-page-->
<script>
    //Send Otp To User during signup
    $("#send_otp_user").on("click", function () {

        let mobile_number = $("#user_signup .mobile").val();
        let _token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            type: 'POST',
            url: "{{ route('signupOtp') }}",
            data: {
                _token: _token,
                mobile: mobile_number,
                role: "user"
            },
            dataType: "json",
            success: function (data) {
                $('#userOtp').text(data.otp);
                toastr.success(data.message);
            },
            error: function (errorResponse) {
                console.log(errorResponse.responseJSON.errors);
                if ('mobile' in errorResponse.responseJSON.errors) {
                    toastr.error(errorResponse.responseJSON.errors.mobile);
                }


            }
        });

    });

    $('#user_signup').submit(function (e) {

        e.preventDefault();
        $(document).find("span.error-span").remove();
        $('.alert-block').hide();
        var formData = new FormData(this);

        $('#btn_user').prop("disabled", true);
        $('#btn_user').text('Sending...');


        $.ajax({

            type: 'POST',
            url: "{{ route('userSignup') }}",
            data: $("#user_signup").serialize(),
            dataType: "json",
            success: function (data) {
                $('#btn_user').prop("disabled", false);
                $('#btn_user').text('Submit');
                if (data.success) {
                    $('#user_signup')[0].reset();
                    $('.user-msg1').show().append('<strong>' + data.msg + '</strong>');
                    $('#userOtp').hide();
                } else {
                    $('.user-msg1').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
                }
            },
            error: function (errorResponse) {
                $('#btn_user').prop("disabled", false);
                $('#btn_user').text('Submit');
                $.each(errorResponse.responseJSON.errors, function (field_name, error) {

                    $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span" role="alert">' + error + '</span>');

                })
            }

        });

    });
</script>
@endsection