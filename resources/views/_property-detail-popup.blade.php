<div class="detailPopheader">
    <div class="logo">
        <a href="index.html">
            <img src="images/logo-black.png" alt="logo">
            Know your Land
        </a>
    </div>

</div>
<!--//detailPopheader-->

<div class="prop_DetailMain row no-gutters">
    <div class="col-lg-6">
        <!--Image gallery for Mobile-->
        <div class="d-block d-lg-none">
            <div class="mobileImageCarousel owl-carousel">
                <div>
                    <img src="{{ asset($propertyList->image) }}" class="mobileCarousel_Image" />
                </div>
                <div>
                    <img src="images/your-land-2.png" class="mobileCarousel_Image" />
                </div>
                <div>
                    <img src="images/your-land-3.png" class="mobileCarousel_Image" />
                </div>
                <div>
                    <img src="images/your-land-4.png" class="mobileCarousel_Image" />
                </div>
            </div>
            <!--//mobileImageCarousel-->
        </div>
        <!--Image gallery for Mobile-->

        <!--Image gallery for Desktop-->
        <div class="landDetail-lft-side d-none d-lg-block">
            <ul class="yourLands customScrollbar">
                @if(!empty($propertyList->video_link))
                <li>
                    <div class="iframeVideo">
                        <iframe width="100%" src="{{ 'https://www.youtube.com/embed/'.$propertyList->video_link }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </li>
                @endif
                @if(count($propertyList->propertyImage))
                @foreach($propertyList->propertyImage as $images)
                <li class="one-half">
                    <a href="images/your-land-2.png" data-lightbox="gallery">
                        <img src="{{ asset($images->image) }}" class="img-fluid img-thumbnail">
                    </a>
                </li>
                @endforeach
                @endif


            </ul>
        </div><!-- //landDetail-lft-side -->
        <!--Image gallery for Desktop-->
    </div>
    <!--//col-lg-6-->

    <div class="col-lg-6">
        <div class="landDetail-right-side">
            <div class="landDetail-title">
                <div class="row">
                    <div class="col-7">
                        <h3>{{ $propertyList->land_name }} <em class="featureIcon"></em></h3>
                        <p>{{ $propertyList->address }}</p>
                    </div>
                    <div class="col-5">
                        <div class="land-price">
                            ₹ {{ $propertyList->max_price }} {{ ucwords($propertyList->price_unit) ?? 'Cr' }}
                        </div>
                        @if(!empty($propertyList->est_payment))
                        <div class="selfTour">
                            Est. payment : <span>INR {{$propertyList->est_payment}}/mo</span>
                        </div><!-- //selfTour -->
                        @endif
                    </div>
                </div>
                <!--//row-->
            </div>
            <!--//landDetail-title-->

            @if($propertyList->wifi == 1 || $propertyList->parking == 1)
            <div class="featFacilities">
                <span class="featFaTitle">Facilities :</span>
                @if($propertyList->parking == 1)
                <span class="faci-icon parking-icon"></span>
                @endif
                <!--                                    <span class="faci-icon noSmooking-icon"></span>-->
                @if($propertyList->wifi == 1)
                <span class="faci-icon wifi-icon"></span>
                @endif
            </div>
            <!--//featFacilities-->
            @endif
            <div class="requiredDetail">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        @if(!empty($propertyList->session1_start_time) || !empty($propertyList->session2_start_time))
                        <div class="selfTour">
                            <p>Self tour daily : @if(!empty($propertyList->session1_start_time))
                                <span>
                                    {{ Carbon\Carbon::parse($propertyList->session1_start_time)->format('H:i A') }} - {{ Carbon\Carbon::parse($propertyList->session1_end_time)->format('H:i A') }}
                                </span>
                                @endif
                                @if(!empty($propertyList->session2_start_time))
                                <span>|</span> <span>
                                    {{ Carbon\Carbon::parse($propertyList->session2_start_time)->format('H:i A') }} - {{ Carbon\Carbon::parse($propertyList->session2_end_time)->format('H:i A') }}
                                </span>
                                @endif
                            </p>
                        </div><!-- //selfTour -->
                        @endif

                        <div class="selfTour">
                            <p><strong>Size of land - {{ $propertyList->size_of_land }} acres</strong></p>
                        </div><!-- //selfTour -->
                    </div>
                    <!--//col-md-8-->

                    <div class="col-md-4">
                        <div class="contactAgentBtn">
                            <button class="btn btn-primary contactbtn" id="contactAgentButton">Contact Agent</button>
                        </div>
                    </div>
                    <!--//col-md-4-->
                </div>
                <!--//row-->
            </div>
            <!--//requiredDetail-->

            <div class="detailTabContainer">
                <div class="detailTabNavBox">
                    <div class="detailTabNav owl-carousel">
                        <div><a href="#description" class="active">Description</a></div>
                        <div><a href="#agenttab">Agent</a></div>
                        <div><a href="#priceValue">Price and Value</a></div>
                        <div><a href="#nearbyPlaces">Nearby Places</a></div>
                        <div><a href="#loanPending">Land Title Report</a></div>
                        <div><a href="#loanPending">Land Title Report</a></div>
                    </div><!-- //nav nav-tabs -->
                </div>
                <!--//detailTabNavBox-->

                <div class="descriptionArea customScrollbar">
                    <section class="descriptionContents" id="description">
                        <p>{{ $propertyList->descritpion }}</p>
                    </section><!-- //descriptionContents -->

                    <section class="descriptionContents" id="agenttab">
                        <h3>Agent</h3>
                        <div class="clientdetails">
                            <div class="clientImg">
                                <img src="images/client-img.png" alt="">
                            </div>
                            <div class="companayName">
                                Mr. {{ ucwords($propertyList->agent->name) }}
                                <span>{{ $propertyList->agent->email }}</span>
                                <!-- <span>+91 {{ $propertyList->agent->mobile }}</span> -->
                            </div>
                        </div>
                    </section><!-- //descriptionContents -->

                    <section class="descriptionContents" id="priceValue">
                        <h3>Price and Value</h3>
                        <p>
                            {{ $propertyList->max_price }} {{ ucwords($propertyList->price_unit) ?? 'Cr' }}
                        </p>
                    </section><!-- //descriptionContents -->

                    <section class="descriptionContents" id="nearbyPlaces">
                        <h3>Nearby Places</h3>
                        <p>
                            {{ $propertyList->nearby_place }}
                        </p>
                    </section><!-- //descriptionContents -->

                    @php
                    $subsc = false;
                    $package = checkUserSubscription();
                    if(empty($package)){
                    $subsc =false;
                    }else if(isset($package->id)){
                    if($package->unlimited == 1){
                    $subsc = true;
                    }elseif(isset($package->detail)){
                    $listId = array_column($package->detail->toArray(), 'property_list_id');
                    if(in_array($propertyList->id, $listId)){
                    $subsc = true;
                    }elseif($propertyList->user_id == auth()->user()->id){
                    $subsc = true;
                    }
                    else{
                    $subsc = false;
                    }
                    }
                    }
                    @endphp
                    @if(Auth::check() && $subsc )
                    <section id="gallery">
                        <div class="container">
                            <div id="image-gallery">
                                <div class="row">
                                    @foreach($propertyList->propertyOtherImage as $image)
                                    @if($image->image_type == 1)
                                    <div class="col-lg-6 col-md-6 col-sm- col-xs-12 image">
                                        <div class="img-wrapper">
                                            <a href="{{ asset($image->image) }}"><img src="{{ asset($image->image) }}" class="img-responsive" style="width: 100%"></a>
                                            <div class="img-overlay">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach

                                </div><!-- End row -->
                            </div><!-- End image gallery -->
                        </div><!-- End container -->
                    </section>
                    </br></br>
                    <section class="descriptionContents" id="loanPending">
                        <div class="loanPending">
                            @foreach($propertyList->propertyOtherImage as $image)
                            @if($image->image_type == 0)
                            <p><a href="{{ asset($image->image) }}" target="_blank">download</a></p>
                            @endif
                            @endforeach
                        </div>
                        <!--//loanPending-->
                    </section><!-- //descriptionContents -->


                    @else
                    <section class="descriptionContents" id="loanPending">
                        <div class="loanPending text-center">
                            <img src="images/svg-icons/loan-pending.svg" class="loanImg" alt="">
                            <h3>To View the Loan Pending</h3>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page..</p>
                            </h4>
                            <ul>
                                <li>Surveying & Demarcation of site by Experts </li>
                                <li>Summary of Land Dur Diligence by Experts</li>
                                <li>Maximum Outreach to trusted agents</li>
                            </ul>
                            @if(!Auth::check())
                            <a class="btn btn-primary" href="{{ url('login') }}">Login</a>
                            @else
                            <a href="{{ url('user-subscription') }}" class="btn btn-primary">Buy Your Subscription</a>
                            @endif

                        </div>
                        <!--//loanPending-->
                    </section><!-- //descriptionContents -->

                    @endif
                </div><!-- //descriptionArea -->

            </div><!-- //rightBottom -->
        </div><!-- //landDetail-right-side -->
    </div>
    <!--//col-lg-6-->
</div>
<!--//row-->


<div class="contactAgentformSection">
    <div class="contactAgentformSection_mBox">
        <h3>
            Contact Agent
            <a href="javascript:void(0);" class="agentCloseBtn"></a>
        </h3>
        <div class="contactAgentForm">
            <form method="post" action="{{ url('agent-contacts') }}" id="agentContactForm">
                @csrf

                <input type="hidden" name="land_id" id="land_id" value="{{$propertyList->id}}">
                <div class="form-group floating-field">
                    <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Name" id="name" name="name" value="{{ old('name')}}" required>
                    <label for="name" class="floating-label">Name</label>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group floating-field mobile-field">
                    <input type="text" class="form-control @error('mobile') is-invalid @enderror" placeholder="Mobile Number" id="mobile" name="mobile" value="{{ old('mobile')}}" required>
                    <label for="mobile" class="floating-label">Mobile Number</label>
                    <span class="mobileCode">+91</span>
                    @error('mobile')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group floating-field mb-4">
                    <input type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Email" id="email" name="email" value="{{ old('email')}}" required>
                    <label for="email" class="floating-label">Email</label>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <button type="button" class="btn btn-primary w-100 agentContectButton">Contact Now</button>
            </form>
        </div>
        <!--//contactAgentForm-->
    </div><!-- //contactAgentformSection_mBox -->
</div>
<!--//contactAgentformSection-->
@if($errors->has('contactAdd'))
<script>
    $(document).ready(function() {
        $('.contactbtn').on('click', function() {
            $('#landDetail-popup').addClass('show_contactForm');
        });
    });
</script>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        //Detail popup - mobile image slider
        $('.mobileImageCarousel').owlCarousel({
            dots: false,
            nav: false,
            margin: 10,
            items: 1,
            responsive: {
                0: {
                    items: 1
                },
                575: {
                    items: 2
                }
            }
        });

        //Detail popup - detailTabNav
        $('.detailTabNav').owlCarousel({
            dots: false,
            nav: true,
            autoWidth: true
        });

        //Switch Views functionality            
        $('#switchTo_Map').on('click', function() {
            $(this).removeClass('showViewBtn');
            //$('#m_filterBtn').removeClass('showViewBtn');
            $('#switchTo_List').addClass('showViewBtn');

            $('.mapArea').show();
            //$('.footer').hide();
            $('.listArea').hide();
        });
        $('#switchTo_List').on('click', function() {
            $(this).removeClass('showViewBtn');
            $('#m_filterBtn').addClass('showViewBtn');
            $('#switchTo_Map').addClass('showViewBtn');

            $('.mapArea').hide();
            //$('.footer').show();
            $('.listArea').show();
        });

        //Detail Popup -Tab content scrolling
        $('.descriptionArea').scrollSpy({
            target: $('.detailTabNav a')
        }).scroll();

        //Agent contact form open and close functionality
        $('#contactAgentButton').on('click', function() {
            $('#landDetail-popup').addClass('show_contactForm');
        });
        $('a.agentCloseBtn').on('click', function() {
            $('#landDetail-popup').removeClass('show_contactForm');
        });

        //Magnific Popup JS Img Lightbox
        // Inline popups
        $('#inline-popups').magnificPopup({
            delegate: 'a',
            removalDelay: 500,
            callbacks: {
                beforeOpen: function() {
                    this.st.mainClass = this.st.el.attr('data-effect');
                }
            },
            midClick: true
        });
    });
</script>
<script>
    // Gallery image hover
    $(".img-wrapper").hover(
        function() {
            $(this).find(".img-overlay").animate({
                opacity: 1
            }, 600);
        },
        function() {
            $(this).find(".img-overlay").animate({
                opacity: 0
            }, 600);
        }
    );

    // Lightbox
    var $overlay = $('<div id="overlay"></div>');
    var $image = $("<img style='width: 50%'>");
    var $prevButton = $('<div id="prevButton"><i class="fa fa-arrow-left" aria-hidden="true"></i></div>');
    var $nextButton = $('<div id="nextButton"><i class="fa fa-arrow-right" aria-hidden="true"></i></div>');
    var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');

    // Add overlay
    $overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
    $("#gallery").append($overlay);

    // Hide overlay on default
    $overlay.hide();

    // When an image is clicked
    $(".img-overlay").click(function(event) {
        // Prevents default behavior
        event.preventDefault();
        // Adds href attribute to variable
        var imageLocation = $(this).prev().attr("href");
        // Add the image src to $image
        $image.attr("src", imageLocation);
        // Fade in the overlay
        $overlay.fadeIn("slow");
    });

    // When the overlay is clicked
    $overlay.click(function() {
        // Fade out the overlay
        $(this).fadeOut("slow");
    });

    // When next button is clicked
    $nextButton.click(function(event) {
        // Hide the current image
        $("#overlay img").hide();
        // Overlay image location
        var $currentImgSrc = $("#overlay img").attr("src");
        // Image with matching location of the overlay image
        var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
        // Finds the next image
        var $nextImg = $($currentImg.closest(".image").next().find("img"));
        // All of the images in the gallery
        var $images = $("#image-gallery img");
        // If there is a next image
        if ($nextImg.length > 0) {
            // Fade in the next image
            $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
        } else {
            // Otherwise fade in the first image
            $("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
        }
        // Prevents overlay from being hidden
        event.stopPropagation();
    });

    // When previous button is clicked
    $prevButton.click(function(event) {
        // Hide the current image
        $("#overlay img").hide();
        // Overlay image location
        var $currentImgSrc = $("#overlay img").attr("src");
        // Image with matching location of the overlay image
        var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
        // Finds the next image
        var $nextImg = $($currentImg.closest(".image").prev().find("img"));
        // Fade in the next image
        $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
        // Prevents overlay from being hidden
        event.stopPropagation();
    });

    // When the exit button is clicked
    $exitButton.click(function() {
        // Fade out the overlay
        $("#overlay").fadeOut("slow");
    });

    $(document).ready(function() {
        $('.agentContectButton').on('click', function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "post",
                url: "agent-contacts",
                data: $('#agentContactForm').serialize(),
                success: function(data) {
                    $('.loader').toggleClass('d-none');
                    $('.loader').toggleClass('d-none');
                    if (data.success) {
                        $('#agentContactForm')[0].reset();
                        swal({
                            title: "Good job!",
                            text: data.msg,
                            icon: "success",
                        }).then((value) => {
                            location.reload();
                        });
                    } else {
                        swal({
                            text: "Something went wrong!",
                            icon: "error",
                        });
                    }
                },
                error: function(errorResponse) {
                    //                console.log(errorResponse.responseJSON.errors);
                    $('.loader').toggleClass('d-none');
                    //                toastr.error(errorResponse.responseJSON.message);
                    $('.error-span').remove();
                    $.each(errorResponse.responseJSON.errors, function(field_name, error) {

                        $(document).find('[name=' + field_name + ']').parent().append('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');

                    })
                }
            });
        })
    });
</script>