@extends('layouts.master')
@section('title')
Sell Your Land
@endsection
@section('content')
<style>
    .loader {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 9999;
        background: #ffffffa6;
    }

    .loader img {
        width: 80px;
    }
</style>
<div class="loader d-none">
    <img src="{{ asset('images/Spin-1s-200px.gif') }}">
</div>
<section class="commonFormPage adjustHeaderSpace">
    <div class="container">
        <div class="formPageHeading">
            <div class="row justify-content-center">
                <div class="col-sm-8">
                    <h1>{{ __('List your Property') }}</h1>
                    <h2>{{ __('This would allow clients  to reach out to genuine buyers to sell their land at the best price along with complete end-to-end transaction facilitation.') }} </h2>
                </div>
            </div>
        </div>
        <!--//formPageHeading-->
        <!--multisteps-form-->
        <div class="multisteps-form">
            <!--progress bar-->
            <div class="row justify-content-center">
                <div class="col-12 col-lg-12 col-md-11">
                    <div class="multisteps-form__progress">
                        <div class="multisteps-form__progress-btn js-active" title="Who you are">
                            <span class="noCount">{{ __('01') }}</span>
                            <span>{{ __('Step 1') }}</span>
                            <h3>{{ __('Who you are') }}</h3>
                        </div>
                        <div class="multisteps-form__progress-btn" title="Basic Details">
                            <span class="noCount">{{ __('02') }}</span>
                            <span>{{ __('Step 2') }}</span>
                            <h3>{{ __('Basic Details') }}</h3>
                        </div>
                        <div class="multisteps-form__progress-btn" title="Payment">
                            <span class="noCount">{{ __('03') }}</span>
                            <span>{{ __('Step 3') }}</span>
                            <h3>{{ __('Payment') }}</h3>
                        </div>
                    </div>
                </div>
            </div>

            <!--form panels-->
            <div class="multiformContainer">
                <form class="multisteps-form__form" action="" id='upload_property_list' enctype="multipart/form-data" name="upload_property_list">
                    <!--single form panel-->
                    @csrf
                    <div class="multisteps-form__panel shadow rounded clearfix js-active" data-animation="fadeIn">
                        <h3 class="multisteps-form__title">{{ __('One of these, Which you are ?') }}</h3>
                        <div class="multisteps-form__content whoYouAre" id="whoYouAre">
                            <div class="form-filds">
                                <div class="form-row mt-4 justify-content-center">
                                    <div class="col-4">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="customRadio1" name="listing_by" value="owner">
                                            <label class="custom-control-label" for="customRadio1">{{ __('Owner') }}</label>
                                        </div>
                                    </div><!-- //col-6 -->

                                    <div class="col-4">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="customRadio2" name="listing_by" value="broker">
                                            <label class="custom-control-label" for="customRadio2">{{ __('Broker') }}</label>
                                        </div>
                                    </div><!-- //col-6 -->

                                </div><!-- //form-row mt-4 -->
                            </div><!-- //form-filds -->

                            <div class="button-row mt-4">
                                <button class="btn btn-primary js-btn-next" type="button" title="Next">{{ __('Continue') }}</button>
                            </div>
                        </div><!-- //multisteps-form__content -->
                    </div>

                    <!--single form panel-->
                    <div class="multisteps-form__panel clearfix shadow rounded" data-animation="fadeIn">
                        <h3 class="multisteps-form__title">{{ __('Basic Details') }}</h3>
                        <div class="multisteps-form__content">

                            <div class="form-filds">
                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" name="user_name" id="user_name" value="{{ auth()->user()->name ?? '' }}" type="text" placeholder=" " @if(Auth::check()) {{ __('disabled') }} @endif />
                                                @if(Auth::check())
                                                <input class="form-control" name="user_name" id="user_name_hidden" value="{{ auth()->user()->name ?? '' }}" type="hidden" placeholder=" " />
                                                @endif
                                                <label for="user_name">{{ __('Name') }} <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label mobileNo">
                                                <input class="form-control" name="user_mobile" id="user_mobile" type="mobile" value="{{ auth()->user()->mobile ?? '' }}" placeholder=" " @if(Auth::check() && !empty(auth()->user()->mobile)) {{ __('disabled') }} @endif/>
                                                @if(Auth::check())
                                                <input class="form-control" name="user_mobile" id="user_mobile_hidden" type="hidden" value="{{ auth()->user()->mobile ?? '' }}" placeholder=" " />
                                                @endif
                                                <label for="user_mobile">{{ __('Mobile Number') }} <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" name="user_email" id="user_email" type="email" value="{{ auth()->user()->email ?? '' }}" placeholder=" " @if(Auth::check()) {{ __('disabled') }} @endif />
                                                @if(Auth::check())
                                                <input class="form-control" name="user_email" id="user_email_hidden" type="hidden" value="{{ auth()->user()->email ?? '' }}" placeholder=" " />
                                                @endif
                                                <label for="user_email">{{ __('Email id') }} <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" name="user_address" id="user_address" type="text" placeholder=" " />
                                                <label for="user_address">{{ __('Full Address') }} <span class="text-danger">*</span></label>
                                                <input type="hidden" name="latitute" id="latitute" value="">
                                                <input type="hidden" name="longitude" id="longitude" value="">
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="customFile" name="file">

                                            <label class="custom-file-label" for="customFile">{{ __('Upload Sazra Plan') }} <span class="text-danger">*</span></label>
                                            <span>{{ __('Pdf, Png, Jpeg upto 5 mb') }}</span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="selectRadio w-100">
                                        <label>{{ __('In both of these, Which one you are ?') }} <span class="text-danger">*</span></label>

                                        <div class="d-flex flex-row is_corporate_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="company" name="is_corporate" value="company">
                                                <label class="custom-control-label" for="company">{{ __('Company') }}</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="individual" name="is_corporate" value="individual">
                                                <label class="custom-control-label" for="individual">{{ __('Individual') }}</label>
                                            </div>
                                        </div>
                                    </div><!-- //selectRadio -->
                                </div><!-- //form-row mt-4 -->
                                <div class="form-row mt-4 gstInputFiled">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control clsGstno" name="user_gst" id="user_gst" type="text" placeholder=" " MaxLength="15" />
                                                <label for="user_gst">{{ __('GST No (if applicable)') }} <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4 userpan">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" name="user_pan" id="user_pan" type="text" placeholder=" " MaxLength="10" pattern="[A-Z]{5}[0-9]{4}[A-Z]{1}" />
                                                <label for="user_pan">{{ __('Pan No') }} <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="selectRadio w-100">
                                        <label>{{ __('What would you do with your property ?') }} <span class="text-danger">*</span></label>

                                        <div class="d-flex flex-row ">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Commercial" name="business_type" value="Commercial">
                                                <label class="custom-control-label" for="Commercial">{{ __('Commercial') }}</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Industrial" name="business_type" value="Industrial">
                                                <label class="custom-control-label" for="Industrial">{{ __('Industrial') }}</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Institutional" name="business_type" value="Institutional">
                                                <label class="custom-control-label" for="Institutional">{{ __('Institutional') }}</label>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-row ">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Recreational" name="business_type" value="Recreational">
                                                <label class="custom-control-label" for="Recreational">{{ __('Recreational') }}</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Mixed_use" name="business_type" value="Mixed_use">
                                                <label class="custom-control-label" for="Mixed_use">{{ __('Mixed_use') }}</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Agricultural" name="business_type" value="Agricultural">
                                                <label class="custom-control-label" for="Agricultural">{{ __('Agricultural') }}</label>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-row business_type_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="Others" name="business_type" value="Others">
                                                <label class="custom-control-label" for="Others">{{ __('Others') }}</label>
                                            </div>
                                        </div>
                                    </div><!-- //selectRadio -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="rangeSlider">
                                        <label>{{ __('Asking Pricing for Sale') }} <span class="text-danger">*</span></label>
                                        <section class="range-slider" id="facet-price-range-slider" data-options='{"output":{"suffix":" Cr/acre"},"maxSymbol":"+"}'>
                                            <input name="min_price" id="min_price" value="0" min="0" max="100" step="1" type="range">
                                            <input name="max_price" id="max_price" value="100" min="0" max="100" step="1" type="range">
                                        </section>
                                    </div><!-- //rangeSlider -->
                                </div>

                                <div class="form-row mt-4">
                                    <div class="selectRadio w-100">
                                        <label>
                                            {{ __('Do you want to attend the KYL as the role and exclusive Channel Partner to transact your land ?') }}
                                            <span class="text-danger">*</span>
                                        </label>

                                        <div class="d-flex flex-row exclusive_channel_partner_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input rdogst" id="yes" name="exclusive_channel_partner" value="yes">
                                                <label class="custom-control-label" for="yes">{{ __('Yes') }}</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input rdogst" id="no" name="exclusive_channel_partner" value="no">
                                                <label class="custom-control-label" for="no">{{ __('No') }}</label>
                                            </div>
                                        </div>
                                    </div><!-- //selectRadio -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="selectRadio w-100" id="radiobox">
                                        <label>
                                            {{ __('Then User is promoted to sign exclusive mandate with KYL') }} <span class="text-danger">*</span>
                                        </label>

                                        <div class="d-flex flex-row exclusive_mandate_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="agree" name="exclusive_mandate" value="agree">
                                                <label class="custom-control-label" for="agree">{{ __('I agree') }}</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="disagree" name="exclusive_mandate" value="disagree">
                                                <label class="custom-control-label" for="disagree">{{ __('I disagree') }}</label>
                                            </div>
                                        </div>
                                    </div><!-- //selectRadio -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4" style="display:none">
                                    <div class="selectRadio w-100">
                                        <label>
                                            {{ __('Do you want to visit KYL Team to your Land to get verified?') }}
                                            <span class="text-danger">*</span>
                                        </label>

                                        <div class="d-flex flex-row land_to_get_verified_error">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="yes2" name="land_to_get_verified" value="yes">
                                                <label class="custom-control-label" for="yes2">{{ __('Yes') }}</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="no2" name="land_to_get_verified" value="no">
                                                <label class="custom-control-label" for="no2">{{ __('No') }}</label>
                                            </div>
                                        </div>
                                    </div><!-- //selectRadio -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="privacyPolicy" name="terms" value="1">
                                        <label class="custom-control-label" for="privacyPolicy">I agree to the <span><a target="_blank" href="{{url('/privacy-policy')}}"> Privacy Policy</a></span>,<span><a target="_blank" href="{{url('/terms-conditions')}}"> Terms of services</a></span> and <span><a target="_blank" href="{{url('/cookie-policy')}}">Cookie Policy</a></span> of this Website </label>
                                    </div>
                                </div><!-- //form-row mt-4 -->


                            </div><!-- //form-filds -->

                            <div class="button-row mt-5 clearfix">
                                <h4>
                                    <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" value="">
                                    <input type="hidden" name="razorpay_order_id" id="razorpay_order_id" value="">
                                    <input type="hidden" name="razorpay_signature" id="razorpay_signature" value="">
                                    <input class="form-control" id="order_id" type="hidden" value="" name="order_id" />
                                    <input class="form-control" id="user_id" type="hidden" value="{{ auth()->user()->id ?? '' }}" name="user_id" />
                                    <input type="hidden" name="amount" value="{{$value->gst}}" id="amount">
                                    <div class="reportTotalVal">
                                        <span>{{ __('You need to Pay') }}</span> INR {{ $value->gst_value }}
                                        <span>{{ $value->amount}} + 18% GST</span>
                                    </div>
                                </h4>
                                <button class="btn btn-primary" type="button" id="sell_land_info_submit">{{ __('Continue') }}</button>
                                <button class="btn btn-primary d-none" type="button" id="sell_land_payment_detailSubmit">{{ __('Continue') }}</button>

                            </div>
                        </div>
                    </div>

                    <!--single form panel-->
                    <div class="multisteps-form__panel clearfix shadow rounded" data-animation="fadeIn" id='payment_tab'>
                        <h3 class="multisteps-form__title">{{ __('Payment') }}</h3>
                        <div class="multisteps-form__content">

                            <div class="form-filds">
                                <h4 class="card-type mt-4 mb-2">{{ __('Card Type') }}</h4>
                                <div class="cards-sections d-flex justify-content-between" id="selectCard">
                                    <div class="card">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="card1" name="card" value="customEx">
                                            <label class="custom-control-label text-center" for="card1">
                                                <img src="images/master-card-black.jpg" alt="" class="black">
                                                <img src="images/master-card-color.jpg" alt="" class="color">
                                            </label>
                                        </div>
                                    </div><!-- //card -->
                                    <div class="card">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="card2" name="card" value="customEx">
                                            <label class="custom-control-label text-center" for="card2">
                                                <img src="images/visa-black.jpg" alt="" class="black">
                                                <img src="images/visa-color.jpg" alt="" class="color">
                                            </label>
                                        </div>
                                    </div><!-- //card -->
                                    <div class="card">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="card3" name="card" value="customEx">
                                            <label class="custom-control-label text-center" for="card3">
                                                <img src="images/paytm-black.jpg" alt="" class="black">
                                                <img src="images/paytm-color.jpg" alt="" class="color">
                                            </label>
                                        </div>
                                    </div><!-- //card -->
                                </div><!-- //cards-sections -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="cardName" type="text" placeholder=" " />
                                                <label for="cardName">Name on Card</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="cardNumber" type="text" placeholder=" " />
                                                <label for="cardNumber">Card Number</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-7">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="expirationDate" type="text" placeholder=" " />
                                                <label for="expirationDate">Expiration Date</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                    <div class="col-5">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="cvv" type="text" placeholder=" " />
                                                <label for="cvv">CVV</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->
                            </div><!-- //form-filds -->

                            <div class="button-row mt-4">
                                <h4>
                                    <label>You need to Pay</label>
                                    INR <span>22,500</span> <span class="month-count">for 6 months</span>
                                </h4>
                                <button class="btn btn-primary ml-auto" type="button" title="Next">Continue</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!--//multiformContainer-->
        </div>

    </div><!-- //container -->
</section><!-- //bookYourAppointment -->
@endsection

@section('custom-script')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_map_key')}}&libraries=places&callback=initAutocomplete" async defer></script>
<script>
    $(document).ready(function() {

        $("#user_pan").change(function() {
            var inputvalues = $(this).val();
            var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
            if (!regex.test(inputvalues)) {
                $("#user_pan").val("");
                $('.userpan').after('<span class="text-strong error-span text-danger" role="alert">' + '</span>');
                // alert("invalid PAN no");
                return regex.test(inputvalues);
            }
        });

        $(".clsGstno").change(function() {
            var inputvalues = $(this).val();
            var gstinformat = new RegExp('^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]1}[1-9A-Z]{1}Z[0-9A-Z]{1}$');
            if (gstinformat.test(inputvalues)) {
                return true;
            } else {

                $('.gstInputFiled').after('<span class="text-strong error-span text-danger" role="alert">' + 'invlid gst no' + '</span>');
                // alert('Please Enter Valid GSTIN Number');
                $(".clsGstno").val('');
                // $(".clsGstno").focus();
            }
        });

    });

    // $('.rdogst').on('click', function() {
    //     alert($(this).val());
    //     if ($(this).val() == "company") {
    //         $('.clsGstno').show();
    //     } else if ($(this).val() == "individual") {
    //         $('.clsGstno').hide();
    //     }
    // });

    $("input[name=exclusive_channel_partner]").on("change", function() {

        var test = $(this).val();
        // alert(test);
        if (test == "yes") {
            $("#radiobox").show();
        } else if (test == "no") {
            $("#radiobox").hide();

        }
    });
    var placeSearch, autocomplete;

    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('user_address'));

        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
        //    autocomplete.setFields(['address_component']);
        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener('place_changed', onPlaceChanged);
    }

    function onPlaceChanged() {
        var place = autocomplete.getPlace();
        document.getElementById('latitute').value = place.geometry.location.lat();
        document.getElementById('longitude').value = place.geometry.location.lng();
    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        //change colour when radio is selected
        $('#whoYouAre input:radio').change(function() {
            // Only remove the class in the specific `box` that contains the radio
            $('.custom-radio.highlight').removeClass('highlight');
            $(this).closest('.custom-radio').addClass('highlight');

            $('.btn-primary.opacity1').removeClass('opacity1');
            $('.btn-primary').addClass('opacity1');
        });
    });
</script>
<script>
    $("#sell_land_info_submit").on('click', function(evt) {
        let myForm = document.getElementById('upload_property_list');
        let formData = new FormData(myForm);
        $('.loader').toggleClass('d-none');
        $.ajax({
            type: 'POST',
            url: "store_property_validation",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('.loader').toggleClass('d-none');
                if (data.user_id) {
                    $('#user_id').val(data.user_id);
                }
                if (data.order_id) {
                    $('#order_id').val(data.order_id);
                }
                var name = "{{ auth()->user()->name ?? '' }}";
                var email = "{{ auth()->user()->email ?? '' }}";
                var mobile = "{{ auth()->user()->mobile ?? '' }}";
                var amount = $('#amount').val();
                if (name == '') {
                    var name = $('#user_name').val();
                }
                if (email == '') {
                    var email = $('#user_email').val();
                }
                if (mobile == '') {
                    var mobile = $('#user_mobile').val();
                }
                var options = {
                    "key": "{{ config('app.razorpay_api_key') }}", // Enter the Key ID generated from the Dashboard
                    "amount": amount * 100, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                    "currency": "{{ config('app.currency') }}",
                    "name": "{{ config('app.account_name') }}",
                    "description": '',
                    "image": "{{ asset('images/logo-black.svg') }}",
                    "order_id": $('#order_id').val(), //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                    "handler": function(response) {
                        $('#razorpay_payment_id').val(response.razorpay_payment_id);
                        $('#razorpay_order_id').val(response.razorpay_order_id);
                        $('#razorpay_signature').val(response.razorpay_signature);
                        $('#sell_land_payment_detailSubmit').click();

                    },
                    "prefill": {
                        "name": name,
                        "email": email,
                        "contact": mobile
                    },
                    //            "notes": {
                    //                "address": "Razorpay Corporate Office"
                    //            },
                    "theme": {
                        "color": "#3399cc"
                    }
                };
                var rzp1 = new Razorpay(options);
                rzp1.on('payment.failed', function(response) {

                });

                rzp1.open();

            },
            error: function(errorResponse) {
                //                console.log(errorResponse.responseJSON.errors);
                $('.loader').toggleClass('d-none');
                //                toastr.error(errorResponse.responseJSON.message);
                $('.error-span').remove();
                $.each(errorResponse.responseJSON.errors, function(field_name, error) {
                    if (field_name == "business_type") {
                        $('.business_type_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    } else if (field_name == "is_corporate") {
                        $('.is_corporate_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    } else if (field_name == "exclusive_channel_partner") {
                        $('.exclusive_channel_partner_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    } else if (field_name == "exclusive_mandate") {
                        $('.exclusive_mandate_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    } else if (field_name == "land_to_get_verified") {
                        $('.land_to_get_verified_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    } else {
                        $(document).find('[name=' + field_name + ']').parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    }
                })
            }
        });

    });
    $("#sell_land_payment_detailSubmit").on('click', function(evt) {
        let myForm = document.getElementById('upload_property_list');
        let formData = new FormData(myForm);
        $('.loader').toggleClass('d-none');
        $.ajax({
            type: 'POST',
            url: "store_property",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('.loader').toggleClass('d-none');
                if (data.success) {
                    $('#upload_property_list')[0].reset();
                    swal({
                        title: "Thank you for showing interest in Know Your Land (KYL) services. Your enquiry has been successfully submitted. The KYL team will get back to you shortly. Please visit your inbox for more details on your query",
                        text: data.message,
                        icon: "success",
                    }).then((value) => {
                        if (value) {
                            location.reload();
                        }
                    });
                } else {
                    swal({
                        text: data.msg,
                        icon: "error",
                    });
                }

            },

        });

    });
</script>
@endsection