@extends('layouts.master')
@section('title')
Sell Your Land
@endsection

@section('content')
<div class="innerPageBanner sellYourLandBanner">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-12">
                <h2>Sell your Land</h2>
                <h1>Why you need Listing here</h1>
                <p>This would allow clients to reach out to genuine buyers to sell their land at the best price along with complete end-to-end transaction facilitation. KYL report enables you to discover the pros and cons of your land through an in-depth analysis considering its location, land use, value, accessibility, emerging </p>
                <a href="#quoteReportSection" class="btn btn-primary">List your Property</a>
            </div>
        </div>
    </div>
    <!--//container-->
</div>
<!--//meetTheExpert-->
<section class="listingAttributes">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-12">
                <h2>Listing Attributes</h2>
                <p>This would allow clients to reach out to genuine buyers to sell their land at the best price along with complete end-to-end transaction facilitation. KYL report enables you to discover the pros and cons of your land through an in-depth analysis considering its location, land use, value, accessibility.</p>
            </div>
        </div>
    </div>
</section>
<section class="priceForYouSection" id="quoteReportSection">
    <div class="container">
        <div class="subSecHeading text-center">
            <h2>Get An Approximate Quote For Your Report</h2>
        </div>
        <!--//subSecHeading-->

        <div class="landReportForm">
            <form action="{{ url('sell-your-land-form') }}" method="post" id="landSearchForm">
                @csrf
                <div class="landReportFormBox">
                    <label class="form-label">Please enter your land in acres to get calculate your amount for your report</label>

                    <div class="form-group floating-field">
                        <input type="hidden" class="form-control" id="amount" name="amount" value="">
                        <input type="hidden" class="form-control" id="gst_value" name="gst_value" value="">
                        <input type="hidden" class="form-control" id="gst" name="gst" value="">
                        <input type="hidden" class="form-control" id="land_title_search" name="value_added_service" value="2">
                        <input type="text" class="form-control" placeholder="Enter your land area" id="area" name="size_of_area">
                        <label for="area" class="floating-label">Enter your land area</label>
                        <span class="fieldFormat">Acres</span>
                    </div><!-- floating-field -->

                    <button class="btn btn-primary-bordered calculateButton">Calculate Amount</button>
                </div>
                <!--//landReportFormBox-->
                <div class="landRepBottomBox">
                    <div class="row align-items-center">
                        <div class="col-sm-6">
                            <div class="reportTotalVal">

                            </div>
                        </div>
                        <div class="col-sm-6 text-right">
                            <button class="btn btn-primary requestTitleSearchButton">Request title search</button>
                        </div>
                    </div>
                </div>
                <!--//landRepBottomBox-->
            </form>
        </div>
        <!--//landReportForm-->
    </div>
    <!--//container-->
</section>
<!--//quoteReport-->
<!--//priceForYouSection-->
<script type="text/javascript">
    $(document).ready(function() {
        $('.calculateButton').on('click', function() {
            var area = $('#area').val();
            $('.error-message').remove();
            if (area == '') {
                $('#area').after('<span class="text-danger error-message">Land area field required</span>')
                return false;
            } else if (!$.isNumeric(area)) {
                $('#area').after('<span class="text-danger error-message">Land area filed only number required</span>')
                return false;
            }
            if (area > 50) {
                var amount = 97000 + (area - 50) * 1500;
            } else if (area > 10 && area <= 50) {
                var amount = 37000 + (area - 10) * 1500;
            } else if (area > 5 && area <= 10) {
                var amount = 27000 + (area - 5) * 2000;
            } else if (area > 1 && area <= 5) {
                var amount = 15000 + (area - 1) * 3000;
            } else {
                var amount = 15000;
            }
            var gst = parseInt(amount + (amount * 18 / 100)).toLocaleString('en-IN');
            var gst_value = amount + (amount * 18 / 100);
            $('#amount').val(parseInt(amount).toLocaleString('en-IN'));
            $('#gst').val(gst_value);
            $('.reportTotalVal').html('<span>You need to Pay</span> INR ' + gst + "<span>(" + parseInt(amount).toLocaleString('en-IN') + "+18% GST)</span>");
            return false;
        });

        $('.requestTitleSearchButton').on('click', function() {
            var area = $('#area').val();
            var amount = $('#amount').val();
            if (area == '') {
                $('#area').after('<span class="text-danger error-message">Land area field required</span>')
                return false;
            } else if (!$.isNumeric(area)) {
                $('#area').after('<span class="text-danger error-message">Land area filed only number required</span>')
                return false;
            }
            if (area > 50) {
                var amount = 97000 + (area - 50) * 1500;
            } else if (area > 10 && area <= 50) {
                var amount = 37000 + (area - 10) * 1500;
            } else if (area > 5 && area <= 10) {
                var amount = 27000 + (area - 5) * 2000;
            } else if (area > 1 && area <= 5) {
                var amount = 15000 + (area - 1) * 3000;
            } else {
                var amount = 15000;
            }
            var gst = parseInt(amount + (amount * 18 / 100)).toLocaleString('en-IN');
            var gst_value = amount + (amount * 18 / 100);
            $('#amount').val(parseInt(amount).toLocaleString('en-IN'));
            $('#gst').val(gst_value);
            $('#gst_value').val(gst);
            $('.reportTotalVal').html('<span>You need to Pay</span> INR ' + gst + "<span>(" + parseInt(amount).toLocaleString('en-IN') + "+18% GST)</span>");
            $('#landSearchForm').submit();

        });
    });
</script>
@if($errors->has('landReport'))
<script>
    swal("Your land report successfully stored!");
</script>
@endif
@endsection