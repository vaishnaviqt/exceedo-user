@extends('layouts.master')
@section('title')
  Login
@endsection
@php
    //   $stateData = (new \App\Helpers\Helpers)->getValueAddedService() ;
@endphp
@section('content')
 <section class="signup-page">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="signupLeftContent">
                            <span class="headingTopLine"></span>
                            <h1>Welcome</h1>
                            <h2>To Know Your Land</h2>

                            <div class="signupKylBtn d-block d-md-none">
                                <a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#joinBroker_expert">Join Kyl as Broker & Expert</a>
                            </div>
                        </div>
                    </div><!--//col-md-6-->
                    <div class="col-md-6">

                        <div class="userSignBox loginPageBox">
                            <ul class="nav nav-tabs" role="tablist">
                                <li>
                                    <a class="active" data-toggle="tab" href="#user" role="tab" aria-selected="true">User</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#broker" role="tab" aria-selected="false">Broker</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#expert" role="tab"  aria-selected="false">Expert</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="user" role="tabpanel">

                                    <div class="userSignBox_scrollBox">
                                        <div class="socialSignup">
                                            <h3>- EASILY USING - </h3>
                                            <div class="socialLoginBtn">
                                                <a href="{{ url('login/user/facebook') }}" class="facebookLogin">Facebook</a>
                                                <a href="{{ url('login/user/google') }}" class="googleLogin">Google</a>
                                            </div><!--//socialLoginBtn-->
                                        </div><!--//socialSignup-->

                                        <div class="signupForm">
                                            <h3>- OR USING EMAIL -</h3>

                                             <form method="POST" id="user_login" method="post">
                                            @csrf
                                                <div class="alert alert-success alert-block expert-msg1" style="display: none;">
                                
                                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                                </div>
                                                <div class="form-group floating-field mobile-field">
                                                    <input type="text" class="form-control mobile" name="mobile" placeholder="Mobile Number" id="mobile"  >
                                                    <label for="mobile" class="floating-label">Mobile Number</label>
                                                    <span class="mobileCode">+91</span>
                                                    <a href="javascript:void(0);" class="sendOTP_Link" id="send_otp_user">Send OTP</a>
                                                    <span id="userOtp"></span>
                                                </div><!-- floating-field -->

                                                <div class="form-group floating-field mb-4">
                                                    <input type="hidden" name="role" value="user">
                                                    <input type="text" class="form-control" placeholder="OTP" id="otp" name="otp">
                                                    <label for="otp" class="floating-label">OTP</label>
                                                </div><!-- floating-field -->

                                                <div class="LoginBtn">
                                                    <a href="javascript:void(0);">
                                                        <button type="submit" class="btn btn-primary" id="user_login_button">Login</button>
                                                    </a>
                                                </div>

                                                <div class="accountSwitchOpt">
                                                    You have an account ? <a href="{{route('signup')}}">Sign up as a New User</a>
                                                </div>
                                            </form>
                                        </div><!--//signupForm-->
                                    </div><!--//userSignBox_scrollBox-->

                                </div>
                                <div class="tab-pane fade" id="broker" role="tabpanel">

                                    <div class="userSignBox_scrollBox">
                                        <div class="socialSignup">
                                            <h3>- EASILY USING - </h3>
                                            <div class="socialLoginBtn">
                                                <a href="{{ url('login/broker/facebook') }}" class="facebookLogin">Facebook</a>
                                                <a href="{{ url('login/broker/google') }}" class="googleLogin">Google</a>
                                            </div><!--//socialLoginBtn-->
                                        </div><!--//socialSignup-->

                                        <div class="signupForm">
                                            <h3>- OR USING EMAIL -</h3>

                                        <form method="POST" id="broker_login" method="post">
                                            @csrf
                                                <div class="alert alert-success alert-block expert-msg1" style="display: none;">
                                
                                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                                </div>
                                                <div class="form-group floating-field mobile-field">
                                                    <input type="text" name ="mobile" class="form-control mobile" placeholder="Mobile Number" id="mobile" name="mobile">
                                                    <label for="mobile" class="floating-label">Mobile Number</label>
                                                    <span class="mobileCode">+91</span>
                                                    <a href="javascript:void(0);" class="sendOTP_Link" disabled id="send_otp_broker">Send OTP</a>
                                                <span id="brokerOtp"></span>
                                                </div><!-- floating-field -->

                                                <div class="form-group floating-field mb-4">
                                                    <input type="hidden" name="role" value="broker">
                                                    <input type="text" name="otp" class="form-control" placeholder="OTP" id="OTP">
                                                    <label for="otp" class="floating-label">OTP</label>
                                                </div><!-- floating-field -->

                                                <div class="LoginBtn">
                                                    <a href="javascript:void(0);">
                                                        <button type="submit" class="btn btn-primary" id="broker_login_button">Login</button>
                                                    </a>
                                                </div>

                                                <div class="accountSwitchOpt">
                                                    You have an account ? <a href="{{route('signup')}}">Sign up as a New User</a>
                                                </div>
                                            </form>
                                        </div><!--//signupForm-->
                                    </div><!--//userSignBox_scrollBox-->

                                </div>
                                <div class="tab-pane fade" id="expert" role="tabpanel">

                                    <div class="userSignBox_scrollBox">
                                        <div class="socialSignup">
                                            <h3>- EASILY USING - </h3>
                                            <div class="socialLoginBtn">
                                                <a href="{{ url('login/expert/facebook') }}" class="facebookLogin">Facebook</a>
                                                <a href="{{ url('login/expert/google') }}" class="googleLogin">Google</a>
                                            </div><!--//socialLoginBtn-->
                                        </div><!--//socialSignup-->

                                        <div class="signupForm">
                                            <h3>- OR USING EMAIL -</h3>

                                         <form method="POST" id="expert_login" method="post">
                                            @csrf
                                                <div class="alert alert-success alert-block expert-msg1" style="display: none;">
                                
                                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                                </div>
                                                <div class="form-group floating-field mobile-field">
                                                    <input type="text"  class="form-control mobile" id="mobile" name ="mobile" placeholder="Mobile Number" id="mobile">
                                                    <label for="mobile" class="floating-label">Mobile Number</label>
                                                    <span class="mobileCode">+91</span>
                                                    <a href="javascript:void(0);" class="sendOTP_Link" id="send_otp_expert">Send OTP</a>
                                                    <span id="expertOtp"></span>
                                                </div><!-- floating-field -->

                                                <div class="form-group floating-field mb-4">
                                                    <input type="hidden" name="role" value="expert">
                                                    <input type="text" name = "otp" class="form-control" placeholder="OTP" id="otp">
                                                    <label for="otp" class="floating-label">OTP</label>
                                                </div><!-- floating-field -->

                                                <div class="LoginBtn">
                                                    <a href="javascript:void(0);">
                                                        <button type="submit" class="btn btn-primary" id="expert_login_button">Login</button>
                                                    </a>
                                                </div>

                                                <div class="accountSwitchOpt">
                                                You have an account ? <a href="{{route('signup')}}">Sign up as a New User</a>
                                                </div>
                                            </form>
                                        </div><!--//signupForm-->
                                    </div><!--//userSignBox_scrollBox-->

                                </div>
                            </div>
                        </div><!--//userSignBox-->

                    </div><!--//col-md-6-->
                </div><!--//row-->
            </div><!--//container-->
        </section><!--//signup-page-->
@endsection

@section('modal')
    <!-- Modal -->
    <!-- Join Kyl as Broker & Expert -->
    <div class="modal fade" id="joinBroker_expert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                    <div class="row no-gutters">
                        <div class="col-md-5">
                            <div class="popupFormPic">
                                <img src="{{ asset('images/brokerModalBanner.png') }}">
                            </div>
                        </div><!--//col-md-4-->
                        <div class="col-md-7">
                            <div class="signup_Thankyou" id="signupThankBox">
                                <div class="thankIcon">
                                    <img src="{{ asset('images/thank-icon.svg') }}" />
                                </div>
                                <h2>Thankyou for Join Us !!</h2>
                                <p><span id="sign_up_thankyou_msg">Lorem Ipsum is simply dummy text of the printingand typesetting industry.</span></p>
                                <div class="signupHomeBtn">
                                    <a href="{{route('home')}}" class="btn btn-primary">Go to Homepage</a>
                                </div>
                            </div><!--//signup_Thankyou-->


                            <div class="loginPageBox" id="signupBrokerBox">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li>
                                        <a class="active" data-toggle="tab" href="#broker_signup" role="tab" aria-selected="false">Broker</a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#expert_signup" role="tab"  aria-selected="false">Expert</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContentSignup">
                                    <div class="tab-pane fade show active" id="broker_signup" role="tabpanel">
                                        <form name="frm_broker_signup" id="frm_broker_signup" method="post">
                                             @csrf
                                        <div class="popupForm">
                                            <div class="form-group floating-field">
                                                <input type="text" class="form-control" placeholder="Full Name" name="broker_full_name" id="broker_full_name">
                                                <label for="full_name" class="floating-label">Full Name</label>
                                            </div><!-- floating-field -->

                                            <div class="form-group floating-field">
                                                <input type="text" class="form-control" placeholder="Email id" id="broker_email" name="broker_email">
                                                <label for="email" class="floating-label">Email id</label>
                                            </div><!-- floating-field -->

                                            <div class="form-group floating-field mobile-field">
                                                <input type="text" class="form-control" placeholder="Mobile Number" name="broker_mobile_number" id="broker_mobile_number">
                                                <label for="mobile_number" class="floating-label">Mobile Number</label>
                                                <span class="mobileCode">+91</span>
                                            </div><!-- floating-field -->

                                            <div class="form-group floating-field">
                                                <select class="form-control" id="broker_services" name="broker_services[]" multiple="multiple">
                                                    @foreach($values as $value)
                                                    @if($value->is_service == 1 && $value->show_for_broker == 1)
                                                        <option value="{{ $value->id }}">{{ $value->service_name }}</option>
                                                    @endif
                                                    @endforeach
                                                </select>

                                                <label for="services_s1" class="floating-label">Services</label>
                                            </div><!-- floating-field -->


                                            <div class="formBtn">
                                                <button type="submit" id="btn_broker" class="btn btn-primary">Submit</button>
                                            </div>

                                        </div><!--//popupForm-->
                                    </form>

                                    </div><!--//broker_signup-->

                                    <div class="tab-pane fade" id="expert_signup" role="tabpanel">
                                        <div class="alert alert-success alert-block expert-msg" style="display: none;">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                        </div>

                                        <form name="frm_expert_signup" id="frm_expert_signup" method="post">
                                             @csrf
                                        <div class="popupForm">
                                            <div class="form-group floating-field">
                                                <input type="text" class="form-control" placeholder="Full Name" name="full_name" id="full_name">
                                                <label for="full_name" class="floating-label">Full Name</label>
                                            </div><!-- floating-field -->

                                            <div class="form-group floating-field">
                                                <input type="text" class="form-control" placeholder="Email id" name="email" id="email">
                                                <label for="email" class="floating-label">Email id</label>
                                            </div><!-- floating-field -->

                                            <div class="form-group floating-field mobile-field">
                                                <input type="text" class="form-control" placeholder="Mobile Number" name="mobile_number" id="mobile_number">
                                                <label for="mobile" class="floating-label">Mobile Number</label>
                                                <span class="mobileCode">+91</span>
                                            </div><!-- floating-field -->

                                            <div class="form-group floating-field">
                                                <select id="expert_services" multiple="multiple" class="form-control" name="exp_services[]" >
                                                    @foreach($values as $value)
                                                        <option value="{{ $value->id }}">{{ $value->service_name }}</option>
                                                    @endforeach
                                                </select>

                                                <label for="services_s2" class="floating-label">Services</label>
                                            </div><!-- floating-field -->


                                            <div class="formBtn">
                                                <button type="submit" id="btn_expert" class="btn btn-primary">Submit</button>
                                            </div>

                                        </div><!--//popupForm-->
                                    </form>

                                    </div><!--//expert_signup-->
                                </div><!--//tab-content-->

                            </div><!--//loginPageBox-->
                        </div><!--//col-md-8-->
                    </div><!--//row-->
                </div><!--//modal-body-->
            </div>
        </div>
    </div><!--//modal-->
@endsection

@section('custom-script')

<script src="{{ asset('js/select2.min.js') }}"></script>
<script src="{{ asset('js/login.js') }}"></script>
 <script type="text/javascript">

    $(document).ready(function() {
        $('#expert_services').select2();
        $('#broker_services').select2();
    });
        function signupBrokerFn(){
            document.getElementById("signupBrokerBox").style.display = "none";
            document.getElementById("signupThankBox").style.display = "flex";
        }


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('#frm_expert_signup').submit(function(e) {

        e.preventDefault();
        $(document).find("span.error-span").remove();
        $('.alert-block').hide();
        var formData = new FormData(this);

        $('#btn_expert').prop("disabled", true);
        $('#btn_expert').text('Sending...');


        $.ajax({

            type:'POST',
            url:"{{ route('storeexpert') }}",
             data: $("#frm_expert_signup").serialize(),
            dataType:"json",
           success:function(data){
                $('#btn_expert').prop("disabled", false);
                $('#btn_expert').text('Submit');
                if(data.success){
                    $('#frm_expert_signup')[0].reset();
                    //$('.expert-msg').show().append('<strong>'+data.msg+'</strong>');
                    $('#sign_up_thankyou_msg').html(data.msg);
                    signupBrokerFn();
                } else{
                    $('.expert-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>'+data.msg+'</strong>');
                }
           },
           error:function (errorResponse){
                $('#btn_expert').prop("disabled", false);
                $('#btn_expert').text('Submit');
                $.each(errorResponse.responseJSON.errors,function(field_name,error){
                    if(field_name == 'exp_services'){
                        $('#expert_services').next().after('<span class="text-strong error-span" role="alert">' +error+ '</span>');
                    }else{
                        $(document).find('[name='+field_name+']').after('<span class="text-strong error-span" role="alert">' +error+ '</span>');
                    }
                })
            }

        });

    });

    // broker signup form

$('#frm_broker_signup').submit(function(e) {

        e.preventDefault();
        $(document).find("span.error-span").remove();
        $('.alert-block').hide();
        var formData = new FormData(this);

        $('#btn_broker').prop("disabled", true);
        $('#btn_broker').text('Sending...');


        $.ajax({

            type:'POST',
            url:"{{ route('storebroker') }}",
            data: formData,
            cache:false,
            contentType: false,
            processData: false,

           success:function(data){
                $('#btn_broker').prop("disabled", false);
                $('#btn_broker').text('Submit');
                if(data.success){
                    $('#frm_broker_signup')[0].reset();
                    //$('.expert-msg').show().append('<strong>'+data.msg+'</strong>');
                    $('#sign_up_thankyou_msg').html(data.msg);
                    signupBrokerFn();
                } else{
                    $('.expert-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>'+data.msg+'</strong>');
                }
           },
           error:function (errorResponse){
                $('#btn_broker').prop("disabled", false);
                $('#btn_broker').text('Submit');
                $.each(errorResponse.responseJSON.errors,function(field_name,error){
                    if(field_name == 'broker_services'){
                        $('#broker_services').next().after('<span class="text-strong error-span" role="alert">' +error+ '</span>');
                    }else{
                        $(document).find('[name='+field_name+']').after('<span class="text-strong error-span" role="alert">' +error+ '</span>');
                    }
                })
            }

        });

    });

$("#send_otp_user").on("click", function(){

     let mobile_number = $("#user_login .mobile").val();
    let _token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: "{{ route('sendOtp') }}",
        data: {_token:_token,mobile:mobile_number,role:"user"},
        dataType: "json",
        success: function (data) {
            $('#userOtp').text(data.otp);
            toastr.success(data.message);
        },
        error: function (errorResponse) {
            console.log(errorResponse.responseJSON.errors);
            if('mobile' in errorResponse.responseJSON.errors){
                toastr.error(errorResponse.responseJSON.errors.mobile);
            }


        }
    });

});
//   send otp to broker and enable/disable otp btn
$("#send_otp_broker").on("click", function(){
 let mobile_number = $("#broker_login .mobile").val();
 let _token = $('meta[name="csrf-token"]').attr('content');
 $.ajax({
    type: 'POST',
    url: "{{ route('sendOtp') }}",
    data: {_token:_token,mobile:mobile_number,role:"broker"},
    dataType: "json",
    success: function (data) {
        $('#brokerOtp').text(data.otp);
        toastr.success(data.message);
    },
    error: function (errorResponse) {
        console.log(errorResponse.responseJSON.errors);
        if('mobile' in errorResponse.responseJSON.errors){
            toastr.error(errorResponse.responseJSON.errors.mobile);
        }


    }
});

});
//   send otp to expert and enable/disable otp btn
$("#send_otp_expert").on("click", function(){
    let mobile_number = $("#expert_login .mobile").val();
    let _token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: 'POST',
        url: "{{ route('sendOtp') }}",
        data: {_token:_token,mobile:mobile_number,role:"expert"},
        dataType: "json",
        success: function (data) {
           $('#expertOtp').text(data.otp);
            toastr.success(data.message);
        },
        error: function (errorResponse) {
            console.log(errorResponse.responseJSON.errors);
            if('mobile' in errorResponse.responseJSON.errors){
                toastr.error(errorResponse.responseJSON.errors.mobile);
            }


        }
    });
});


$('#broker_login').submit(function(e) {

        e.preventDefault();    
         $(document).find("span.error-span").remove();   
        var formData = new FormData(this);
        $('#broker_login_button').prop("disabled", true);
        $('#broker_login_button').text('Sending...');



        $.ajax({
            type:'POST',
            url:"{{ route('login') }}",
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
           success:function(data){
                $('#broker_login_button').prop("disabled", false);
                $('#broker_login_button').text('Login');
                if(data.success){
                    //alert(data.url);
                    window.location.href = data.url;
                    
                } else{
                    $('#broker_login .expert-msg1').show().removeClass('alert-success').addClass('alert-danger').append('<strong>'+data.msg+'</strong>');
                }
           },
           error:function (errorResponse){
                $('#broker_login_button').prop("disabled", false);
                $('#broker_login_button').text('Login');
                $.each(errorResponse.responseJSON.errors,function(field_name,error){
                    
                        $(document).find('#broker_login [for='+field_name+']').after('<span class="text-strong error-span" role="alert">' +error+ '</span>');
                   
                })
            }

        });

    });


$('#user_login').submit(function(e) {

        e.preventDefault();     
         $(document).find("span.error-span").remove();  
        var formData = new FormData(this);
        $('#user_login_button').prop("disabled", true);
        $('#user_login_button').text('Sending...');



        $.ajax({
            type:'POST',
            url:"{{ route('login') }}",
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
           success:function(data){
                $('#user_login_button').prop("disabled", false);
                $('#user_login_button').text('Login');
                if(data.success){
                    //alert(data.url);
                     window.location.href = data.url;
                    
                } else{
                    $('#user_login .expert-msg1').show().removeClass('alert-success').addClass('alert-danger').append('<strong>'+data.msg+'</strong>');
                }
           },
           error:function (errorResponse){
                $('#user_login_button').prop("disabled", false);
                $('#user_login_button').text('Login');
                $.each(errorResponse.responseJSON.errors,function(field_name,error){
                    
                        $(document).find('#user_login [for='+field_name+']').after('<span class="text-strong error-span" role="alert">' +error+ '</span>');
                   
                })
            }

        });

    });


$('#expert_login').submit(function(e) {

        e.preventDefault();    
         $(document).find("span.error-span").remove();   
        var formData = new FormData(this);
        $('#expert_login_button').prop("disabled", true);
        $('#expert_login_button').text('Sending...');



        $.ajax({
            type:'POST',
            url:"{{ route('login') }}",
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
           success:function(data){
                $('#expert_login_button').prop("disabled", false);
                $('#expert_login_button').text('Login');
                if(data.success){
                    //alert(data.url);
                     window.location.href = data.url;
                    
                } else{
                    $('#expert_login .expert-msg1').show().removeClass('alert-success').addClass('alert-danger').append('<strong>'+data.msg+'</strong>');
                }
           },
           error:function (errorResponse){
                $('#expert_login_button').prop("disabled", false);
                $('#expert_login_button').text('Login');
                $.each(errorResponse.responseJSON.errors,function(field_name,error){
                    
                        $(document).find('#expert_login [for='+field_name+']').after('<span class="text-strong error-span" role="alert">' +error+ '</span>');
                   
                })
            }

        });

    });

    </script>


@endsection
