@extends('layouts.master')
@section('title')
  Meet the expert
@endsection
@section('splashscreen')
<div class="innerPageBanner meetTheExpertBanner">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-12">
                <h1>Meet the Expert</h1>
                <p>KYL offers a unique, one-to-one consultation service with the experts and specialists on board to resolve all land-related queries with first-hand analysis and real-time problem diagnosis.</p>
                <a href="{{route('bookyourappointment')}}" class="btn btn-primary">Book your Appointment</a>
            </div>
        </div>
    </div><!--//container-->
</div><!--//meetTheExpert-->


@endsection
@section('content')
<section class="youMeetExpertSection">
    <div class="container">
        <h2>Why Should You Meet The Expert?</h2>
    </div>

    <div class="youMeetExpertSection_bg">
        <div class="container">
            <div class="row justify-content-md-end">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="javascript:void(0);"><span>01</span> On The Spot Doubt Clarification</a>
                        </div>
                        <div class="col-sm-6">
                            <a href="javascript:void(0);"><span>02</span> Get The Real Time Problem Diagnosis</a>
                        </div>
                        <div class="col-sm-6">
                            <a href="javascript:void(0);"><span>03</span> Achieve Pragmatic Solutions</a>
                        </div>
                        <div class="col-sm-6">
                            <a href="javascript:void(0);"><span>04</span> Develop Viable Strategy And Way Forward</a>
                        </div>
                    </div><!--//row-->
                </div><!--//col-lg-9-->
            </div><!--//row-->

        </div><!--//container-->
    </div><!--//youMeetExpertSection_bg-->
</section>
<!-- //youMeetExpert -->

<section class="meetOurExpertSection">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-8 text-center">
                <h2>Meet our Services Experts</h2>
                <p>Please enter your land in acres to get calculate your amount for your report anlaysis</p>
            </div>

            <div class="col-lg-10 col-md-12 col-12 pb-4">
                <form>
                    <div class="d-flex flex-sm-wrap flex-wrap">
                        <div class="custom-meet-radio">
                            <input type="radio" id="id01" name="serviceExpert">
                            <label class="custom-control-label" for="id01">Design & Development</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id02" name="serviceExpert">
                            <label class="custom-control-label" for="id02">Legal Advisory</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id03" name="serviceExpert">
                            <label class="custom-control-label" for="id03">Urban Planning</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id04" name="serviceExpert">
                            <label class="custom-control-label" for="id04">Transaction Advisory</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id05" name="serviceExpert">
                            <label class="custom-control-label" for="id05">Design & Development</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id06" name="serviceExpert">
                            <label class="custom-control-label" for="id06">Legal Advisory</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id07" name="serviceExpert">
                            <label class="custom-control-label" for="id07">Urban Planning</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id08" name="serviceExpert">
                            <label class="custom-control-label" for="id08">Transaction Advisory</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id09" name="serviceExpert">
                            <label class="custom-control-label" for="id09">Design & Development</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id10" name="serviceExpert">
                            <label class="custom-control-label" for="id10">Legal Advisory</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id11" name="serviceExpert">
                            <label class="custom-control-label" for="id11">Urban Planning</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id12" name="serviceExpert">
                            <label class="custom-control-label" for="id12">Transaction Advisory</label>
                        </div>
                    </div>
                </form>
            </div>
            <!-- //col-sm-11 -->

            <div class="col-sm-12 text-center">
                <a href="{{route('bookyourappointment')}}" class="btn btn-primary">Book your Appointment</a>
            </div>

        </div>
    </div>
</section>
<!-- //meetOurExpertSection -->


<section class="home_contactSection minHeight_100">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="contactTitle">
                    <h2>Contact Us</h2>
                    <h3>Please contact us if you need to learn more about KYL and our services.</h3>
                </div>

                <div class="contactForm">

                    <div class="form-group floating-field">
                        <input type="text" class="form-control" placeholder="Name" id="c_name">
                        <label for="c_name" class="floating-label">Name</label>
                    </div><!-- floating-field -->

                    <div class="form-group floating-field">
                        <input type="text" class="form-control" placeholder="Email" id="c_email">
                        <label for="c_email" class="floating-label">Email</label>
                    </div><!-- floating-field -->

                    <div class="form-group floating-field">
                        <input type="text" class="form-control" placeholder="Mobile Number" id="c_mobile">
                        <label for="c_mobile" class="floating-label">Mobile Number</label>
                    </div><!-- floating-field -->

                    <div class="form-group floating-field">
                        <textarea class="form-control" placeholder="Message" id="c_message"></textarea>
                        <label for="c_message" class="floating-label">Message</label>
                    </div><!-- floating-field -->

                    <div class="contactSubmitBtn">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div><!--//col-md-6-->

            <div class="col-md-6">
                <div class="contactRightCont">
                    <div class="contAdditionBtns">
                        <a href="tel:9315495030" class="btn btn-phone">+919315495030</a>
                        <a href="mailto:mail@kylnow.com" class="btn btn-mail">mail@kylnow.com</a>
                    </div>

                    <div class="locationTooltipBox">
                        <a href="javascript:void(0);" class="locationIcon"></a>
                        <div class="locationTooltip">
                            <h3>Exceedo Developers & Consultants Pvt. Ltd.</h3>
                            <p>Freedom Fighter Colony, Block B, Neb Sarai,
                            Sainik Farm, New Delhi, Delhi 110030</p>
                        </div>
                    </div>

                    <div class="viewLgMapBtn">
                        <a href="javascript:void(0);" class="btn">View Large map</a>
                    </div>
                </div><!--//contactRightCont-->
            </div><!--//col-md-6-->
        </div><!--//row-->
    </div><!--//container-->
</section><!--//home_contactSection-->
@endsection
