@extends('layouts.master')
@section('title')
Book Your Appointment
@endsection

@section('content')
<section class="commonFormPage adjustHeaderSpace">
    <div class="container">
        <div class="formPageHeading">
            <div class="row justify-content-center">
                <div class="col-sm-8">
                    <h1>Please Fill Out the Form to Book your Appointment</h1>
                    <h2>Pay INR 15,000 to book now.</h2>
                </div>
            </div>
        </div>
        <!--//formPageHeading-->

        <!--multisteps-form-->
        <div class="multisteps-form">
            <div class="multisteps-form__progress">
                <div class="multisteps-form__progress-btn js-active" title="User Info">
                    <span class="noCount">01</span>
                    <span>Step 1</span>
                    <h3>Basic Details</h3>
                </div>
                <div class="multisteps-form__progress-btn" title="Address">
                    <span class="noCount">02</span>
                    <span>Step 2</span>
                    <h3>Payment</h3>
                </div>
            </div>
            <!--multisteps-form__progress-->

            <!--form panels-->
            <div class="multiformContainer">
                <form class="multisteps-form__form" action="{{url('store-land-appointment')}}" id='upload_appointment_list' enctype="multipart/form-data" name="upload_appointment_list">
                    <!--single form panel-->
                    @csrf
                    <div class="multisteps-form__panel shadow rounded clearfix js-active" data-animation="fadeIn">
                        <h3 class="multisteps-form__title">Basic Details</h3>
                        <div class="multisteps-form__content">

                            <div class="form-filds">

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="name" name="name" type="text" value="{{ auth()->user()->name ?? '' }}" placeholder=" " @if(Auth::check()) {{ __('disabled') }} @endif />
                                                <label for="remarks">Name <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div>

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" name="email" id="email" value="{{ auth()->user()->email ?? '' }}" type="email" placeholder=" " @if(Auth::check()) {{ __('disabled') }} @endif />
                                                <label for="remarks">Email <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div>

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">

                                                <input class="form-control" name="mobile" id="number" value="{{ auth()->user()->mobile ?? '' }}" type="number" placeholder=" " @if(Auth::check() && !empty(auth()->user()->mobile)) {{ __('disabled') }} @endif/>
                                                <label for="remarks">Mobile Number <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div>

                                <div class="form-row mt-4">
                                    <div class="col-12 has-float-label">
                                        <div class="form-group floating-field singleBorder">
                                            <input type="text" class="form-control appDate" name="date">
                                            <label for="datepicker" class="active">Appointment Date <span class="text-danger">*</span></label>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12 has-float-label">
                                        <div class="form-group floating-field singleBorder">
                                            <input type="text" class="form-control rescheduleTime" name="time">
                                            <label for="timepicker" class="active">Appointment Time <span class="text-danger">*</span></label>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="remarks" type="text" name="remarks" placeholder=" " />
                                                <label for="remarks">Any Further Remarks <span class="text-danger">*</span></label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <select class="form-control" id="valueAddedServices" value="" name="value_added_service_id">
                                                <option value="">Value Added Services <span style="color: red">*</span></option>
                                                @foreach($services as $service)
                                                <option value="{{ $service->id }}">{{ $service->service_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row mt-0">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="privacyPolicy" name="terms">
                                        <label class="custom-control-label" for="privacyPolicy">I agree to the <span><a href="{{ route('privacypolicy') }}"> Privacy Policy </a>, <a href="{{ route('termsconditions') }}">Terms of services</a></span> and <span><a href="{{ route('cookiepolicy') }}">Cookie Policy</a></span> of this Website <span class="text-danger">*</span></label>
                                    </div>
                                </div><!-- //form-row mt-4 -->
                            </div><!-- //form-filds -->

                            <div class="button-row mt-5 clearfix">
                                <h4>
                                    <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" value="">
                                    <input type="hidden" name="razorpay_order_id" id="razorpay_order_id" value="">
                                    <input type="hidden" name="razorpay_signature" id="razorpay_signature" value="">
                                    <input class="form-control" id="order_id" type="hidden" value="" name="order_id" />
                                    <input class="form-control" id="user_id" type="hidden" value="{{ auth()->user()->id ?? '' }}" name="user_id" />
                                    <input type="hidden" name="amount" value="" id="amount">
                                    <div class="reportTotalVal">
                                    </div>
                                </h4>
                                <button class="btn btn-primary" type="button" id="sell_land_info_submit">{{ __('Continue') }}</button>
                                <button class="btn btn-primary d-none" type="button" id="sell_land_payment_detailSubmit">{{ __('Continue') }}</button>

                            </div>
                        </div>
                    </div>
                    <!--single form panel-->

                    <div class="multisteps-form__panel clearfix shadow rounded" data-animation="fadeIn">
                        <h3 class="multisteps-form__title">Payment</h3>
                        <div class="multisteps-form__content">

                            <div class="form-filds">
                                <h4 class="card-type mt-4 mb-2">Card Type</h4>
                                <div class="cards-sections d-flex justify-content-between" id="selectCard">
                                    <div class="card">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="card1" name="card" value="customEx">
                                            <label class="custom-control-label text-center" for="card1">
                                                <img src="images/master-card-black.jpg" alt="" class="black">
                                                <img src="images/master-card-color.jpg" alt="" class="color">
                                            </label>
                                        </div>
                                    </div><!-- //card -->
                                    <div class="card">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="card2" name="card" value="customEx">
                                            <label class="custom-control-label text-center" for="card2">
                                                <img src="images/visa-black.jpg" alt="" class="black">
                                                <img src="images/visa-color.jpg" alt="" class="color">
                                            </label>
                                        </div>
                                    </div><!-- //card -->
                                    <div class="card">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="card3" name="card" value="customEx">
                                            <label class="custom-control-label text-center" for="card3">
                                                <img src="images/paytm-black.jpg" alt="" class="black">
                                                <img src="images/paytm-color.jpg" alt="" class="color">
                                            </label>
                                        </div>
                                    </div><!-- //card -->
                                </div><!-- //cards-sections -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="cardName" type="text" placeholder=" " />
                                                <label for="cardName">Name on Card</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-12">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="cardNumber" type="text" placeholder=" " />
                                                <label for="cardNumber">Card Number</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->

                                <div class="form-row mt-4">
                                    <div class="col-7">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="expirationDate" type="text" placeholder=" " />
                                                <label for="expirationDate">Expiration Date</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                    <div class="col-5">
                                        <div class="form-group input-group">
                                            <span class="has-float-label">
                                                <input class="form-control" id="cvv" type="text" placeholder=" " />
                                                <label for="cvv">CVV</label>
                                            </span>
                                        </div>
                                    </div><!-- //col-12 -->
                                </div><!-- //form-row mt-4 -->
                            </div><!-- //form-filds -->

                            <div class="button-row mt-4">
                                <h4>
                                    <label>You need to Pay</label>
                                    INR <span>22,500</span> <span class="month-count">for 6 months</span>
                                </h4>
                                <button class="btn btn-primary ml-auto" type="button" title="Next">Continue</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!--//multiformContainer-->
        </div>
    </div><!-- //container -->
</section><!-- //commonFormPage -->


@endsection
@section('custom-script')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('.appDate').datepicker({
        minDate: today,
        format: 'dd-mm-yyyy'
    });
    datepicker = $('.rescheduleDate').datepicker({
        minDate: today,
        format: 'dd-mm-yyyy'
    });

    $('#appTime, .rescheduleTime').timepicker({
        uiLibrary: 'bootstrap'
    });
    // timepicker
    //    $('#appTime').timepicker({
    //      uiLibrary: 'bootstrap4'
    //    });
</script>

<script>
    $('#valueAddedServices').on('change', function() {
        var serviceId = $(this).val();
        var price = '';
        <?php
        foreach ($services as $service) {
        ?>
            if (serviceId == <?php echo $service->id ?? 'null' ?>) {
                price = <?php echo $service->price + ($service->price * 18 / 100)  ?? 'null' ?>
            }
        <?php
        }
        ?>
        var data = '<span>You need to Pay </span>INR ' + parseInt(price).toLocaleString('en-IN') + '<span>(' + parseInt(15000).toLocaleString('en-IN') + '+18% gst)</span>';
        $('.reportTotalVal').html(data);
        $('#amount').val(price);
    });
    $("#sell_land_info_submit").on('click', function(evt) {
        let myForm = document.getElementById('upload_appointment_list');
        let formData = new FormData(myForm);
        $('.loader').toggleClass('d-none');
        $.ajax({
            type: 'POST',
            url: "store_appointment_validation",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('.loader').toggleClass('d-none');
                if (data.user_id) {
                    $('#user_id').val(data.user_id);
                }
                if (data.order_id) {
                    $('#order_id').val(data.order_id);
                }
                var name = "{{ auth()->user()->name ?? '' }}";
                var email = "{{ auth()->user()->email ?? '' }}";
                var mobile = "{{ auth()->user()->mobile ?? '' }}";
                var amount = $('#amount').val();
                if (name == '') {
                    var name = $('#name').val();
                }
                if (email == '') {
                    var email = $('#email').val();
                }
                if (mobile == '') {
                    var mobile = $('#number').val();
                }
                var options = {
                    "key": "{{ config('app.razorpay_api_key') }}", // Enter the Key ID generated from the Dashboard
                    "amount": amount * 100, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                    "currency": "{{ config('app.currency') }}",
                    "name": "{{ config('app.account_name') }}",
                    "description": '',
                    "image": "{{ asset('images/logo-black.svg') }}",
                    "order_id": $('#order_id').val(), //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                    "handler": function(response) {
                        $('#razorpay_payment_id').val(response.razorpay_payment_id);
                        $('#razorpay_order_id').val(response.razorpay_order_id);
                        $('#razorpay_signature').val(response.razorpay_signature);
                        $('#sell_land_payment_detailSubmit').click();

                    },
                    "prefill": {
                        "name": name,
                        "email": email,
                        "contact": mobile
                    },
                    //            "notes": {
                    //                "address": "Razorpay Corporate Office"
                    //            },
                    "theme": {
                        "color": "#3399cc"
                    }
                };
                var rzp1 = new Razorpay(options);
                rzp1.on('payment.failed', function(response) {

                });

                rzp1.open();

            },
            error: function(errorResponse) {
                //                console.log(errorResponse.responseJSON.errors);
                $('.loader').toggleClass('d-none');
                //                toastr.error(errorResponse.responseJSON.message);
                $('.error-span').remove();
                $.each(errorResponse.responseJSON.errors, function(field_name, error) {
                    if (field_name == "business_type") {
                        $('.business_type_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    } else if (field_name == "is_corporate") {
                        $('.is_corporate_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    } else if (field_name == "exclusive_channel_partner") {
                        $('.exclusive_channel_partner_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    } else if (field_name == "exclusive_mandate") {
                        $('.exclusive_mandate_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    } else if (field_name == "land_to_get_verified") {
                        $('.land_to_get_verified_error').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    } else {
                        $(document).find('[name=' + field_name + ']').parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    }
                })
            }
        });

    });
    $("#sell_land_payment_detailSubmit").on('click', function(evt) {
        let myForm = document.getElementById('upload_appointment_list');
        let formData = new FormData(myForm);
        $('.loader').toggleClass('d-none');
        $.ajax({
            type: 'POST',
            url: "store-land-appointment",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('.loader').toggleClass('d-none');

                if (data.success) {
                    $('#upload_appointment_list')[0].reset();
                    swal({
                        title: "Thankyou for showing interest in Know Your Land(KYL)services.\n\
                Your appointment has been successfully booked. The KYL team will get back to you shortly.\n\
                Please visit your inbox for more details on your appointment.!",
                        text: data.message,
                        icon: "success",
                    }).then((value) => {
                        if (value) {
                            location.reload();
                        }
                    });

                } else {
                    swal({
                        text: data.msg,
                        icon: "error",
                    });
                }

            },

        });

    });
</script>

@endsection