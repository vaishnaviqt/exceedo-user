<!-- //Nav tabs Start -->
<div class="reportNavBar flex-row">
	<div class="reportTabs">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link @if( Request::route()->getName() == 'broker.payments' ) active @endif"  href="{{ route('broker.payments') }}">Received Payments <span></span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if( Request::route()->getName() == 'broker.made-payments' ) active @endif"  href="{{ route('broker.made-payments') }}">Made Payments <span></span></a>
            </li>
        </ul>
	</div>
</div><!--//reportNavBar-->

<!-- //Nav tabs End -->

