@extends('broker.layouts.master')
@section('title')
Land Listing
@endsection
@section('class')
@endsection
@section('content')
@include('broker.reports.header')
<div class="tab-content">
    @include('broker.reports.__comman_filter')
    <div class="reportsListing">
        <div class="row row_20">
            @forelse($allReports as $report)
            <div class="col-md-6">
                <article class="reportArticle">
                    <div class="repArtImg">
                        @if(Storage::disk('landReport')->exists(str_replace('land_report_image/','',$report->image)))
                        <img src="{{ Storage::disk('landReport')->url(str_replace('land_report_image/','',$report->image)) }}" alt="{{ $report->size_of_land }} Acres {{ $report->land_name }}" />
                        @endif
                    </div>
                    <div class="repArtContent">
                        <a href="javacript:void(0);" class="moreOptLink"></a>
                        <h2>{{ $report->size_of_land }} Acres {{ $report->land_name }}</h2>
                        <h3>{{ isset($report->user->mobile) ? $report->user->mobile : '' }} | {{ isset($report->user->email) ? $report->user->email : '' }}</h3>
                        <p class="repOneLine repTitleIcon">Land Title Search</p>
                        <p class="repOneLine repLocIcon">{{ $report->address }}, {{ $report->city }} {{ isset($report->state->name) ? $report->state->name : '' }}</p>

                        <div class="repArtAdditional">
                            @if(empty($report->expert_id) && $report->status == 0)
                            <p class="ArtAuthor">Pending by : <span>Admin</span></p>
                            @elseif(!empty($report->expert_id) && $report->status == 0)
                            <p class="ArtAuthor">Pending by : <span>{{ isset($report->expert->name)  ? $report->expert->name : '' }}</span></p>
                            @elseif(!empty($report->expert_id) && $report->status == 2)
                            <p class="ArtAuthor">Inprogress by : <span>{{ isset($report->expert->name)  ? $report->expert->name : '' }}</span></p>
                            @elseif($report->status == 1)
                            <p class="ArtAuthor">Delivered by  : <span>{{ isset($report->expert->name)  ? $report->expert->name : '' }}</span></p>
                            @endif
                            <p class="timeDetail">{{  $report->created_at->diffForHumans() }}</p>
                        </div>
                    </div><!--//repArtContent-->
                </article><!--//reportArticle-->
            </div><!--//col-md-6-->
            @empty 
            <div class="col-12">
                <div class="no-content-msg">   
                    No Land Available
                </div>
            </div>
            @endforelse

        </div><!--//row-->
    </div><!--//reportsListing-->
    <div class="pagination">
        {{ $allReports->links() }}
    </div><!--//pagination--> 
</div><!--//tab-content-->
</div><!--//exceedoTabs-->
@endsection   
@section('modal')
@endsection   
