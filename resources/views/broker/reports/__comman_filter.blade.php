<div class="filterFormSection">
    <a href="javascript:void(0);" class="mobileFilterBtn">Search </a>   
    @if(request()->is('broker/your-land-report'))
    <form action="{{ url('broker/your-land-report') }}" method="post">
        @elseif(request()->is('broker/your-land-report-delivered'))
        <form action="{{ url('broker/your-land-report-delivered') }}" method="post">
            @elseif(request()->is('broker/your-land-report-pending'))
            <form action="{{ url('broker/your-land-report-pending') }}" method="post">
                @elseif(request()->is('broker/your-land-report-inprogress'))
                <form action="{{ url('broker/your-land-report-inprogress') }}" method="post">
                    @endif
                    @csrf
                    <div class="filterForm">
                        <h2>
                            Search
                            <a href="javascript:void(0);" class="m_filterCloseBtn"></a>
                        </h2>

                        <div class="row">
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Name" id="Name" name="name" value="{{ $name ?? '' }}">
                                        </div>
                                    </div><!--//col-lg-3-->

                                    <!--
                                    <div class="col-lg-3 col-md-6"> 
                                        <div class="form-group">
                                            <select class="form-control" id="ReportType" onclick="this.setAttribute('value', this.value);" value="">
                                                <option></option>
                                                <option>Report Type 1</option>
                                                <option>Report Type 2</option>
                                                <option>Report Type 3</option>
                                            </select>
                                            <label for="ReportType" class="floating-label">Report Type</label>
                                        </div>
                                    </div>
                                    -->

                                    <div class="col-lg-3 col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" id="Location" onclick="this.setAttribute('value', this.value);" name="location">
                                                <option value="">Select State</option>
                                                @foreach($states as $state)
                                                <option value="{{ $state->id }}" @if($location == $state->id) {{ __('selected') }} @endif>{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div><!--//col-lg-3-->

                                    <div class="col-lg-3 col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Date" max="{{ date('Y-m-d') }}" value="{{ $date ?? '' }}" id="datepicker" name="date"> 
                                        </div>
                                    </div><!--//col-lg-3-->
                                </div><!--//row-->
                            </div><!--//col-lg-10-->

                            <div class="col-lg-2">
                                <button class="btn btn-primary" type="submit">Search </button>
                            </div>
                        </div><!--//row-->
                    </div><!--//filterForm-->
                </form>
                </div><!--filterFormSection-->
