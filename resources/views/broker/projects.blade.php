@extends('broker.layouts.master')
@section('title')
  Your Projects
@endsection

@section('class')
 reportsBody
@endsection
@section('content')


               

                @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



                    <div class="row">
                        <div class="col-12">
                            <h2>Your Projects</h2> 
                        </div><!-- //col-12 pageTitle -->

                        <div class="col-12">
                            <div class="countCards">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6">
                                        <article class="countCardArt">
                                            <div class="coutRepIcon">
                                                <img src="{{ asset('broker/images/svg-icons/admin-reports-icon.svg') }}" alt="all reports" />
                                            </div>
                                            <div class="countRepDetail">
                                                <p><strong>47</strong></p>
                                                <p>All Reports</p>
                                            </div>
                                        </article><!--//countCardArt-->
                                    </div><!--//col-lg-3-->
                                    
                                    <div class="col-lg-3 col-md-6">
                                        <article class="countCardArt">
                                            <div class="coutRepIcon">
                                                <img src="{{ asset('broker/images/svg-icons/delivered-Reports-icon.svg') }}" alt="Delivered Reports" />                                    
                                            </div>
                                            <div class="countRepDetail">
                                                <p><strong>32</strong></p>
                                                <p>Delivered Reports</p>
                                            </div>
                                        </article><!--//countCardArt-->
                                    </div><!--//col-lg-3-->
                                    
                                    <div class="col-lg-3 col-md-6">
                                        <article class="countCardArt">
                                            <div class="coutRepIcon">
                                                <img src="{{ asset('broker/images/svg-icons/inprogress-reports-icon.svg') }}" alt="Inprogress Reports" />                                    
                                            </div>
                                            <div class="countRepDetail">
                                                <p><strong>12</strong></p>
                                                <p>Inprogress Reports</p>
                                            </div>
                                        </article><!--//countCardArt-->
                                    </div><!--//col-lg-3-->
                                    
                                    <div class="col-lg-3 col-md-6">
                                        <article class="countCardArt">
                                            <div class="coutRepIcon">
                                                <img src="{{ asset('broker/images/svg-icons/request-reports-icon.svg') }}" alt="Pending Reports" />                                    
                                            </div>
                                            <div class="countRepDetail">
                                                <p><strong>12</strong></p>
                                                <p>Pending Reports</p>
                                            </div>
                                        </article><!--//countCardArt-->
                                    </div><!--//col-lg-3-->                        
                                </div><!--//row-->
                            </div><!--//countCards-->
                        </div><!-- //col-12 -->

                        <!-- Projects Notification-->
                        <div class="col-12">
                            <h3 class="page-sub-title">Projects Notification</h3><!-- //page-sub-title-->

                            <div class="projectNotification">
                                <div class="row">
                                    <div class="col-md-6 notification">
                                        <div class="notificationCard">
                                            <div class="landAddress">
                                                <h4 class="card-title">21 Acres Land Town</h4>
                                                <label class="landTitle">Land Title Search</label>
                                                <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                <label>Assigned by : <span>Admin Swati</span></label>
                                            </div><!--//landAddress-->

                                            <div class="acceptClose">
                                                <a href="javascript:void(0)" class="acceptedCard"></a>
                                                <a href="javascript:void(0)" class="closeCard"></a>
                                                <span class="card-text align-self-end">2 days ago</span>
                                            </div><!-- //acceptClose-->
                                        </div><!-- //notificationCard-->
                                    </div><!-- //col-md-6 -->

                                    <div class="col-md-6 notification">
                                        <div class="notificationCard">
                                            <div class="landAddress">
                                                <h4 class="card-title">21 Acres Land Town</h4>
                                                <label class="landTitle">Land Title Search</label>
                                                <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                <label>Assigned by : <span>Admin Swati</span></label>
                                            </div><!--//landAddress-->

                                            <div class="acceptClose">
                                                <a href="javascript:void(0)" class="acceptedCard"></a>
                                                <a href="javascript:void(0)" class="closeCard"></a>
                                                <span class="card-text align-self-end">2 days ago</span>
                                            </div><!-- //acceptClose-->
                                        </div><!-- //notificationCard-->
                                    </div><!-- //col-md-6 -->

                                    <div class="col-md-6" id="accepted-box">
                                        <div class="notificationCard">
                                            <div class="accepted-box">
                                                <p>You have accepted the Land Title Search Projects for 21 Acres Land Town</p>
                                            </div>
                                            <div class="assigned-box">
                                                <label>Assigned by : <span>Admin Swati</span></label>
                                                <span class="card-text align-self-end">2 days ago</span>
                                            </div>
                                        </div><!-- //notificationCard-->
                                    </div><!-- //col-md-6 -->
                                </div><!--//row-->
                            </div><!-- //projectNotification-->
                        </div><!-- //col-12 Projects Notification -->
    
                        <div class="col-12">
                            <!-- //Nav tabs Start -->
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#projects">Projects <span>(47)</span></a>
                                </li>
                                <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#deliveredProjects">Delivered Projects <span>(32)</span></a>
                                </li>
                                <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#inprogressProjects">Inprogress Projects <span>(12)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#requestProjects">Pending Projects <span>(12)</span></a>
                                </li>
                            </ul>
                            <!-- //Nav tabs End -->

                            <!-- //Search Form Div Start -->
                                <div class="searchForm">
                                    <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                   
                                    <form>
                                        <span>Search</span>
                                        <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('broker/images/svg-icons/close-icon.svg') }}" alt=""></a>
                                        <ul class="d-flex flex-wrap">
                                            <li>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Name" id="name">
                                                </div><!-- //form-group -->
                                            </li>
                                            <li>
                                                <div class="form-group">
                                                    <select class="form-control" id="reportType">
                                                        <option>Report Type</option>
                                                        <option>Delivered</option>
                                                        <option>Pending</option>
                                                        <option>Inprogress</option>
                                                    </select>
                                                </div><!-- //form-group --> 
                                            </li>
                                            <li>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Location" id="location">
                                                </div><!-- //form-group -->
                                            </li>
                                            <li>
                                                <div class="form-group">
                                                    <input class="form-control" id="datepicker" type="text" placeholder="Date"/>
                                                </div><!-- //form-group -->
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" class="btn btn-primary">Search</a>
                                            </li>
                                        </ul><!-- //ul -->
                                    </form><!-- //form -->
                                </div><!-- //search-form -->
                            <!-- //Search Form Div End -->
                            
                            <!-- //Tab panes Start -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="projects">
                                    <div class="row row_20">
                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <img src="{{ asset('broker/images/project-map.png')}}" alt="Generic placeholder image">
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                Delivered by : <span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>
                                                        </div><!-- //expartUpdate-->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse">
                                                    <header class="blockquote-header">Feeback by Admin</header>
                                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->                                                
                                        </div><!-- //col-md-6 col-12 -->

                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <div class="uploadReport">
                                                        <div class="uploadBtn">
                                                            <input type="file" id="customFile" class="custom-file-input">
                                                            <label for="customFile" class="custom-file-label"><img src="{{ asset('broker/images/svg-icons/upload-icon.svg')}}"></label>
                                                        </div><!--//uploadBtn -->                                                            
                                                        <span>Upload Report<br>If Ready</span>
                                                    </div><!--//uploadReport -->
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                InProgress by : <span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary d-none">Download PDF</a>                                                            
                                                        </div><!-- //expartUpdate-->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse uploadYoourReport">
                                                    <p class="mb-0"><img src="{{ asset('broker/images/svg-icons/clock-icon.svg')}}" alt="clock"> Upload your Report to get your feedback</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->   
                                        </div><!-- //col-md-6 col-12 -->

                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <img src="{{ asset('broker/images/project-map.png')}}" alt="Generic placeholder image">
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                Pending by : <span>Expert Musolan</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                                        </div><!-- //expartUpdate-->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse">
                                                    <header class="blockquote-header">Feeback by Admin</header>
                                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->                                                
                                        </div><!-- //col-md-6 col-12 -->

                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <div class="uploadReport">
                                                        <div class="uploadBtn">
                                                            <input type="file" id="customFile" class="custom-file-input">
                                                            <label for="customFile" class="custom-file-label"><img src="{{ asset('broker/images/svg-icons/upload-icon.svg')}}"></label>
                                                        </div><!--//uploadBtn -->                                                            
                                                        <span>Upload Report<br>If Ready</span>
                                                    </div><!--//uploadReport -->
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="reportUpdate">
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                InProgress by : <span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary d-none">Download PDF</a>
                                                        </div>
                                                        </div><!-- //reportUpdate -->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse uploadYoourReport">
                                                    <p class="mb-0"><img src="{{ asset('broker/images/svg-icons/clock-icon.svg')}}" alt="clock"> Upload your Report to get your feedback</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->   
                                        </div><!-- //col-md-6 col-12 -->
                                    </div><!-- /.row -->
                                </div><!--//tab-pane -->

                                <div class="tab-pane" id="deliveredProjects">
                                    <div class="row row_20">
                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <img src="{{ asset('broker/images/project-map.png')}}" alt="Generic placeholder image">
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                <span>Delivered by : </span>Expert Musolani
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                                        </div><!-- //expartUpdate-->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse">
                                                    <header class="blockquote-header">Feeback by Admin</header>
                                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->                                                
                                        </div><!-- //col-md-6 col-12 -->

                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <img src="{{ asset('broker/images/project-map.png')}}" alt="Generic placeholder image">
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                Delivered by : <span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                                        </div><!-- //expartUpdate-->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse">
                                                    <header class="blockquote-header">Feeback by Admin</header>
                                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->                                                
                                        </div><!-- //col-md-6 col-12 -->

                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <img src="{{ asset('broker/images/project-map.png')}}" alt="Generic placeholder image">
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                Delivered by : <span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                                        </div><!-- //expartUpdate-->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse">
                                                    <header class="blockquote-header">Feeback by Admin</header>
                                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->                                                
                                        </div><!-- //col-md-6 col-12 -->

                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <img src="{{ asset('broker/images/project-map.png')}}" alt="Generic placeholder image">
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                Delivered by : <span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                                        </div><!-- //expartUpdate-->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse">
                                                    <header class="blockquote-header">Feeback by Admin</header>
                                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->                                                
                                        </div><!-- //col-md-6 col-12 -->
                                    </div><!-- /.row -->
                                </div><!--//tab-pane -->

                                <div class="tab-pane" id="inprogressProjects">
                                    <div class="row row_20">
                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <div class="uploadReport">
                                                        <div class="uploadBtn">
                                                            <input type="file" id="customFile" class="custom-file-input">
                                                            <label for="customFile" class="custom-file-label"><img src="{{ asset('broker/images/svg-icons/upload-icon.svg')}}"></label>
                                                        </div><!--//uploadBtn -->                                                            
                                                        <span>Upload Report<br>If Ready</span>
                                                    </div><!--//uploadReport -->
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="reportUpdate">
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                InProgress by : <span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary d-none">Download PDF</a>
                                                        </div>
                                                        </div><!-- //reportUpdate -->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse uploadYoourReport">
                                                    <p class="mb-0"><img src="{{ asset('broker/images/svg-icons/clock-icon.svg')}}" alt="clock"> Upload your Report to get your feedback</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->   
                                        </div><!-- //col-md-6 col-12 -->

                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <div class="uploadReport">
                                                        <div class="uploadBtn">
                                                            <input type="file" id="customFile" class="custom-file-input">
                                                            <label for="customFile" class="custom-file-label"><img src="{{ asset('broker/images/svg-icons/upload-icon.svg')}}"></label>
                                                        </div><!--//uploadBtn -->                                                            
                                                        <span>Upload Report<br>If Ready</span>
                                                    </div><!--//uploadReport -->
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="reportUpdate">
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                InProgress by : <span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary d-none">Download PDF</a>
                                                        </div>
                                                        </div><!-- //reportUpdate -->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse uploadYoourReport">
                                                    <p class="mb-0"><img src="{{ asset('broker/images/svg-icons/clock-icon.svg')}}" alt="clock"> Upload your Report to get your feedback</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->   
                                        </div><!-- //col-md-6 col-12 -->

                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <div class="uploadReport">
                                                        <div class="uploadBtn">
                                                            <input type="file" id="customFile" class="custom-file-input">
                                                            <label for="customFile" class="custom-file-label"><img src="{{ asset('broker/images/svg-icons/upload-icon.svg')}}"></label>
                                                        </div><!--//uploadBtn -->                                                            
                                                        <span>Upload Report<br>If Ready</span>
                                                    </div><!--//uploadReport -->
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="reportUpdate">
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                InProgress by : <span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary d-none">Download PDF</a>
                                                        </div>
                                                        </div><!-- //reportUpdate -->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse uploadYoourReport">
                                                    <p class="mb-0"><img src="{{ asset('broker/images/svg-icons/clock-icon.svg')}}" alt="clock"> Upload your Report to get your feedback</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->   
                                        </div><!-- //col-md-6 col-12 -->

                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <div class="uploadReport">
                                                        <div class="uploadBtn">
                                                            <input type="file" id="customFile" class="custom-file-input">
                                                            <label for="customFile" class="custom-file-label"><img src="{{ asset('broker/images/svg-icons/upload-icon.svg')}}"></label>
                                                        </div><!--//uploadBtn -->                                                            
                                                        <span>Upload Report<br>If Ready</span>
                                                    </div><!--//uploadReport -->
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="reportUpdate">
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                InProgress by : <span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary d-none">Download PDF</a>
                                                        </div>
                                                        </div><!-- //reportUpdate -->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse uploadYoourReport">
                                                    <p class="mb-0"><img src="{{ asset('broker/images/svg-icons/clock-icon.svg')}}" alt="clock"> Upload your Report to get your feedback</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->   
                                        </div><!-- //col-md-6 col-12 -->
                                    </div><!-- /.row -->
                                </div><!--//tab-pane -->

                                <div class="tab-pane" id="requestProjects">
                                    <div class="row row_20">
                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <img src="{{ asset('broker/images/project-map.png')}}" alt="Generic placeholder image">
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                Pending by : <span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                                        </div><!-- //expartUpdate-->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse">
                                                    <header class="blockquote-header">Feeback by Admin</header>
                                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->                                                
                                        </div><!-- //col-md-6 col-12 -->

                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <img src="{{ asset('broker/images/project-map.png')}}" alt="Generic placeholder image">
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                Pending by : <span></span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                                        </div><!-- //expartUpdate-->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse">
                                                    <header class="blockquote-header">Feeback by Admin</header>
                                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->                                                
                                        </div><!-- //col-md-6 col-12 -->

                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <img src="{{ asset('broker/images/project-map.png')}}" alt="Generic placeholder image">
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                Pending by : <span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                                        </div><!-- //expartUpdate-->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse">
                                                    <header class="blockquote-header">Feeback by Admin</header>
                                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->                                                
                                        </div><!-- //col-md-6 col-12 -->

                                        <div class="col-lg-6 col-md-12 col-12 card-deck">
                                            <div class="reportBox border">
                                                <!--//threeDotMenu Start-->
                                                <div class="dropdown">
                                                    <button class="btn" type="button" data-toggle="dropdown"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                        <li><a href="JavaScript:void(0);">Link</a></li>
                                                    </ul>
                                                </div>
                                                <!--//threeDotMenu End-->
                                                <div class="media">
                                                    <img src="{{ asset('broker/images/project-map.png')}}" alt="Generic placeholder image">
                                                    <div class="media-body">
                                                        <h5 class="mt-0">21 Acres Land Town</h5>
                                                        <span>+9560465467 |</span> <span>abhitbhatia@gmail.com</span>
                                                        <label class="landTitle">Land Title Search</label>
                                                        <label class="landLocation">A-35, Sohna Road, Gurgaon, Haryana</label>
                                                        <div class="expartUpdate">
                                                            <h6>
                                                                Pending by : <span>Expert Musolani</span>
                                                                <span class="daysAgo">2 days ago</span>
                                                            </h6>
                                                            <a href="javascript:void(0);" class="btn btn-primary">Download PDF</a>                                                                
                                                        </div><!-- //expartUpdate-->
                                                    </div><!-- //media-body -->
                                                    </div><!--//media -->
                                                    <blockquote class="blockquote blockquote-reverse">
                                                    <header class="blockquote-header">Feeback by Admin</header>
                                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                    </blockquote>
                                            </div><!-- //reportBox -->                                                
                                        </div><!-- //col-md-6 col-12 -->
                                    </div><!-- /.row -->
                                </div><!--//tab-pane -->

                            </div><!-- //Tab panes End -->
                        </div><!-- //col-12 -->

                    </div><!-- //row -->                

                

                 @endsection   


@section('modal')

                  
@endsection   
