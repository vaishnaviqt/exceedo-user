@extends('broker.layouts.master')
@section('title')
  Appointments
@endsection
@section('class')
 reportsBody
@endsection
@section('content')


                <h2>Appointments</h2>

                @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

                <div class="countCards">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <article class="countCardArt">
                                <div class="coutRepIcon">
                                    <img src="{{ asset('broker/images/svg-icons/all-appointments-icon.svg') }}" alt="All Appointments" />
                                </div>
                                <div class="countRepDetail">
                                    <p><strong>47</strong></p>
                                    <p>All Appointments</p>
                                </div>
                            </article><!--//countCardArt-->
                        </div><!--//col-lg-4-->
                        
                        <div class="col-lg-4 col-md-6">
                            <article class="countCardArt">
                                <div class="coutRepIcon">
                                    <img src="{{ asset('broker/images/svg-icons/upcoming-appointments-icon.svg') }}" alt="Upcoming Appointments" />                                    
                                </div>
                                <div class="countRepDetail">
                                    <p><strong>32</strong></p>
                                    <p>Upcoming Appointments</p>
                                </div>
                            </article><!--//countCardArt-->
                        </div><!--//col-lg-4-->
                        
                        <div class="col-lg-4 col-md-6">
                            <article class="countCardArt">
                                <div class="coutRepIcon">
                                    <img src="{{ asset('broker/images/svg-icons/done-appointments-icon.svg') }}" alt="Done Appointments" />                                    
                                </div>
                                <div class="countRepDetail">
                                    <p><strong>12</strong></p>
                                    <p>Done Appointments</p>
                                </div>
                            </article><!--//countCardArt-->
                        </div><!--//col-lg-4-->                       
                    </div><!--//row-->
                </div><!--//countCards--> 

                <h2>All Appointments</h2>

                <!-- //Search Form Div Start -->
                    <div class="searchForm">
                        <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                   
                        <form>
                            <span>Search</span>
                            <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('broker/images/svg-icons/close-icon.svg') }}" alt=""></a>
                            <ul class="d-flex flex-wrap">
                                <li>
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Expert Name</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Customer Name" id="customerName">
                                    </div><!-- //form-group -->
                                </li>                                            
                                <li>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Appointment" id="appointment">
                                    </div><!-- //form-group -->
                                </li>
                                <li>
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Payment Method</option>
                                            <option>Case</option>
                                            <option>Card</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="btn btn-primary">Search</a>
                                </li>
                            </ul><!-- //ul -->
                        </form><!-- //form -->
                    </div><!-- //searchForm -->
                <!-- //Search Form Div End -->
                
                <!--
                <div class="calender-section">
                    <div id='calendar'></div>
                </div>
                -->
                <!-- //calender-section -->
                
                <div class="mb-3">
                    <img src="{{ asset('broker/images/calendar-img.png') }}" style="width:100%" />
                </div>


                <div class="row">
                    <!-- // DEMO LINKS FOR POPUPS -->
                    <div class="col-12 mt-5">
                        <h2 class="mt-5 mb-2">Appointment related Popups</h2>
                        <style>
                            .tempList{ list-style: none; padding: 0; margin: 0; padding: 0;}
                            .tempList li { list-style: none; display: inline-block;}
                            .tempList li a{ display: block; padding: 10px 15px; margin: 0 10px 10px 0; border:#aaa solid 1px; border-radius: 5px;}
                        </style>
                        <ul class="tempList">
                            <li><a href="#urban-planning-popup" data-effect="mfp-zoom-in">Appointments Details (Popup)</a></li>
                            <li><a href="#approval-appointment-popup" data-effect="mfp-zoom-in">Approval appointment (Popup)</a></li>
                            <li><a href="#view-all-appointments-popup" data-effect="mfp-zoom-in">View all Appointments (Popup)</a></li>
                            <li><a href="#pending-appointment-popup" data-effect="mfp-zoom-in">Pending appintment (Popup)</a></li>
                            <li><a href="#suggest-another-time-popup" data-effect="mfp-zoom-in">Suggest another time (Popup)</a></li>
                            <li><a href="#thankyou-popup" data-effect="mfp-zoom-in">Approval Confirmation (Popup)</a></li>
                        </ul>
                    </div><!-- //.col-12-->
                    <!-- // DEMO LINKS FOR POPUPS -->
                </div><!-- //row -->     


                 @endsection   


@section('modal')

                  <!-- Urban Planning Discussion-popup Start-->
    <div id="urban-planning-popup" class="mfp-with-anim mfp-hide urban-planning-popup">
        <div class="popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close"></button>
            <div class="row">
                <div class="col-12">
                    <ul>
                        <li>
                            <h2>Urban Planning Discussion</h2>
                            <span class="dateTime">Monday, 06 October, 08:00 am to 08:30 am </span>
                            <!--//threeDotMenu Start-->
                            <div class="dropdown">
                                <button class="btn" type="button" data-toggle="dropdown"></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                </ul>
                            </div>
                            <!--//threeDotMenu End-->
                        </li>
                        <li>
                            <span class="joinMeeting">Join the Meeting </span>
                            <a href="javascript:void(0);">https://us02web.zoom.us/j/303164402?pwd=WXg4M3lQUTh6NDZlS3lPQUlkMGNXZz09</a>
                        </li>
                        <li>
                            <label>Meeting ID: <span>303 164 402</span></label>
                            <label>Password: <span>095215</span></label>
                        </li>
                    </ul>
                    <footer>
                        Please join the meeting 10 minutes before
                    </footer>
                </div><!--//.col-12 -->           
            </div><!--/row-->
        </div><!-- //popup-body --> 
    </div><!-- //Urban Planning Discussion-popup End -->

    <!-- approval-appointment-popup Start-->
    <div id="approval-appointment-popup" class="mfp-with-anim mfp-hide urban-planning-popup approval-appointment">
        <div class="popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close"></button>
            <div class="row">
                <div class="col-12">
                    <ul>
                        <li>
                            <h2>Urban Planning Discussion</h2>
                            <span class="dateTime">Monday, 06 October, 08:00 am to 08:30 am </span>
                            <!--//threeDotMenu Start-->
                            <div class="dropdown">
                                <button class="btn" type="button" data-toggle="dropdown"></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                    <li><a href="JavaScript:void(0);">Link</a></li>
                                </ul>
                            </div>
                            <!--//threeDotMenu End-->
                        </li>
                    </ul>
                    <ul class="confirmAppointment">
                        <li>Confirm the Appointment</li>
                        <li><a href="javascript:void(0);">Suggest Another time</a></li>
                        <li><a href="javascript:void(0);" class="btn btn-primary">Yes</a></li>
                    </ul><!-- //confirmAppointment -->
                </div><!--//.col-12 -->           
            </div><!--/row-->
        </div><!-- //popup-body --> 
    </div><!-- //approval-appointment-popup End -->

    <!-- view-all-appointments-popup Start-->
    <div id="view-all-appointments-popup" class="mfp-with-anim mfp-hide viewAll-appointment">
        <div class="popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close"></button>
            <div class="row">
                <div class="col-12">
                    <h2>All Appointments <span>(06 Oct 2020)</span></h2>
                </div><!--//.col-12 -->  
                
                <div class="col-12">
                    <div class="searchForm">
                        <form>
                            <ul class="d-flex flex-wrap">
                                <li>
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Expert Name</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li>
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Time</option>
                                            <option>10:00 AM</option>
                                            <option>11:00 AM</option>
                                            <option>12:00 PM</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li>
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Appointment</option>
                                            <option>Appointment 01</option>
                                            <option>Appointment 02</option>
                                            <option>Appointment 03</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li>
                                    <!-- <a href="javascript:void(0);" class="btn btn-primary">Se</a> -->
                                    <input type="submit" value="Search" class="btn btn-primary">
                                </li>
                            </ul><!-- //ul -->
                        </form><!-- //form -->
                    </div><!--//searchForm-->
                </div><!-- //.col-12-->

                <div class="col-12">
                    <div class="appointments">
                        <form>
                            <ul class="appointments-list">
                                <li class="order-md-1 order-1"><h3>Architectural Discussion</h3></li>
                                <li class="order-md-2 order-3">08:00 am to 08:30 am</li>
                                <li class="order-md-3 order-2">
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Assigned to Expert</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li class="order-md-4 order-4">
                                    Join the Meeting <a href="javascript:void(0);">https://us02web.zoom.us/j/30316440wji383928292…</a>
                                </li>
                            </ul><!--//appointments-list-->

                            <ul class="appointments-list">
                                <li class="order-md-1 order-1"><h3>Architectural Discussion</h3></li>
                                <li class="order-md-2 order-3">08:00 am to 08:30 am</li>
                                <li class="order-md-3 order-2">
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Assigned to Expert</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li class="order-md-4 order-4">
                                    Join the Meeting <a href="javascript:void(0);">https://us02web.zoom.us/j/30316440wji383928292…</a>
                                </li>
                            </ul><!--//appointments-list-->

                            <ul class="appointments-list">
                                <li class="order-md-1 order-1"><h3>Architectural Discussion</h3></li>
                                <li class="order-md-2 order-3">08:00 am to 08:30 am</li>
                                <li class="order-md-3 order-2">
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Assigned to Expert</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li class="order-md-4 order-4">
                                    Join the Meeting <a href="javascript:void(0);">https://us02web.zoom.us/j/30316440wji383928292…</a>
                                </li>
                            </ul><!--//appointments-list-->

                            <ul class="appointments-list">
                                <li class="order-md-1 order-1"><h3>Architectural Discussion</h3></li>
                                <li class="order-md-2 order-3">08:00 am to 08:30 am</li>
                                <li class="order-md-3 order-2">
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Assigned to Expert</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li class="order-md-4 order-4">
                                    Join the Meeting <a href="javascript:void(0);">https://us02web.zoom.us/j/30316440wji383928292…</a>
                                </li>
                            </ul><!--//appointments-list-->

                            <ul class="appointments-list">
                                <li class="order-md-1 order-1"><h3>Architectural Discussion</h3></li>
                                <li class="order-md-2 order-3">08:00 am to 08:30 am</li>
                                <li class="order-md-3 order-2">
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Assigned to Expert</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li class="order-md-4 order-4">
                                    Join the Meeting <a href="javascript:void(0);">https://us02web.zoom.us/j/30316440wji383928292…</a>
                                </li>
                            </ul><!--//appointments-list-->

                            <ul class="appointments-list">
                                <li class="order-md-1 order-1"><h3>Architectural Discussion</h3></li>
                                <li class="order-md-2 order-3">08:00 am to 08:30 am</li>
                                <li class="order-md-3 order-2">
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Assigned to Expert</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li class="order-md-4 order-4">
                                    Join the Meeting <a href="javascript:void(0);">https://us02web.zoom.us/j/30316440wji383928292…</a>
                                </li>
                            </ul><!--//appointments-list-->
                        </form><!--//form-->
                    </div><!--//appointments -->
                </div><!--//col-12-->
            </div><!--/row-->
        </div><!-- //popup-body --> 
    </div><!-- //view-all-appointments-popup End -->

    <!-- Pending appintment Popup Start-->
    <div id="pending-appointment-popup"class="mfp-with-anim mfp-hide viewAll-appointment">
        <div class="popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close"></button>
            <div class="row">
                <div class="col-12">
                    <h2>All Appointments <span>(06 Oct 2020)</span></h2>
                </div><!--//.col-12 -->  
                
                <div class="col-12">
                    <div class="searchForm">
                        <form>
                            <ul class="d-flex flex-wrap">
                                <li>
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Expert Name</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li>
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Time</option>
                                            <option>10:00 AM</option>
                                            <option>11:00 AM</option>
                                            <option>12:00 PM</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li>
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Appointment</option>
                                            <option>Appointment 01</option>
                                            <option>Appointment 02</option>
                                            <option>Appointment 03</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li>
                                    <!-- <a href="javascript:void(0);" class="btn btn-primary">Se</a> -->
                                    <input type="submit" value="Search" class="btn btn-primary">
                                </li>
                            </ul><!-- //ul -->
                        </form><!-- //form -->
                    </div><!--//searchForm-->
                </div><!-- //.col-12-->

                <div class="col-12">
                    <div class="appointments">
                        <form>
                            <ul class="appointments-list">
                                <li class="order-md-1 order-1"><h3>Architectural Discussion</h3></li>
                                <li class="order-md-2 order-3">08:00 am to 08:30 am</li>
                                <li class="order-md-3 order-2">
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Assigned to Expert</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li class="order-md-4 order-4">
                                    Join the Meeting <a href="javascript:void(0);">https://us02web.zoom.us/j/30316440wji383928292…</a>
                                </li>
                            </ul><!--//appointments-list-->

                            <ul class="appointments-list">
                                <li class="order-md-1 order-1"><h3>Architectural Discussion</h3></li>
                                <li class="order-md-2 order-3">08:00 am to 08:30 am</li>
                                <li class="order-md-3 order-2">
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Assigned to Expert</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li class="order-md-4 order-4">
                                    <span class="not-accepted">Expert has not accepted the appointment time</span>
                                </li>
                            </ul><!--//appointments-list-->

                            <ul class="appointments-list">
                                <li class="order-md-1 order-1"><h3>Architectural Discussion</h3></li>
                                <li class="order-md-2 order-3">08:00 am to 08:30 am</li>
                                <li class="order-md-3 order-2">
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Assigned to Expert</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li class="order-md-4 order-4">
                                    Join the Meeting <a href="javascript:void(0);">https://us02web.zoom.us/j/30316440wji383928292…</a>
                                </li>
                            </ul><!--//appointments-list-->

                            <ul class="appointments-list">
                                <li class="order-md-1 order-1"><h3>Architectural Discussion</h3></li>
                                <li class="order-md-2 order-3">08:00 am to 08:30 am</li>
                                <li class="order-md-3 order-2">
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Assigned to Expert</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li class="order-md-4 order-4">
                                    Join the Meeting <a href="javascript:void(0);">https://us02web.zoom.us/j/30316440wji383928292…</a>
                                </li>
                            </ul><!--//appointments-list-->

                            <ul class="appointments-list">
                                <li class="order-md-1 order-1"><h3>Architectural Discussion</h3></li>
                                <li class="order-md-2 order-3">08:00 am to 08:30 am</li>
                                <li class="order-md-3 order-2">
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Assigned to Expert</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li class="order-md-4 order-4">
                                    Join the Meeting <a href="javascript:void(0);">https://us02web.zoom.us/j/30316440wji383928292…</a>
                                </li>
                            </ul><!--//appointments-list-->

                            <ul class="appointments-list">
                                <li class="order-md-1 order-1"><h3>Architectural Discussion</h3></li>
                                <li class="order-md-2 order-3">08:00 am to 08:30 am</li>
                                <li class="order-md-3 order-2">
                                    <div class="form-group">
                                        <select class="form-control" id="reportType">
                                            <option>Assigned to Expert</option>
                                            <option>Abhit Bhatia</option>
                                            <option>Mahira Sharma</option>
                                            <option>Vipin Vindal</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li class="order-md-4 order-4">
                                    Join the Meeting <a href="javascript:void(0);">https://us02web.zoom.us/j/30316440wji383928292…</a>
                                </li>
                            </ul><!--//appointments-list-->
                        </form><!--//form-->
                    </div><!--//appointments -->
                </div><!--//col-12-->
            </div><!--/row-->
        </div><!-- //popup-body --> 
    </div><!-- //Pending appintment Popup End -->

    <!-- Suggest Another Time Popup Start-->
    <div id="suggest-another-time-popup"class="mfp-with-anim mfp-hide suggestAnotherTime">
        <div class="popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close"></button>
            <div class="row">
                <div class="col-12">
                    <h2>Suggest Another Time</h2>
                </div><!--//.col-12 -->  
                
                <div class="col-12">
                    <form>
                        <div class="form-filds">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <span class="has-float-label">
                                            <input class="form-control" id="datepicker" type="text" placeholder=" "/>
                                            <label for="datepicker">Appointment Date</label>
                                        </span>
                                    </div>
                                </div><!-- //col-12 -->
    
                                <div class="col-12">
                                    <div class="form-group">
                                        <span class="has-float-label">
                                            <input class="form-control" id="timepicker" type="text" placeholder=""/>
                                            <label for="timepicker">Appointment Time</label>
                                        </span>
                                    </div>
                                </div><!-- //col-12 -->
    
                                <div class="col-12">
                                    <div class="form-group mb-3">
                                        <span class="has-float-label">
                                            <input class="form-control" id="remarks" type="text" placeholder=" "/>
                                            <label for="remarks">Any Further Remarks</label>
                                        </span>
                                    </div>
                                </div><!-- //col-12 -->
    
                                <div class="col-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="privacyPolicy" name="privacyPolicy">
                                        <label class="custom-control-label" for="privacyPolicy">I agree to the <span> Privacy Policy, Terms of services</span> and <span> Cookie Policy</span> of this Website</label>
                                    </div>
                                </div><!--//col-12-->
    
                                <div class="col-12">
                                    <button class="btn btn-primary js-btn-next" type="button" title="Next">Submit</button>
                                </div><!--//col-12-->
                            </div><!--//.row-->
                        </div><!--//form-filds-->
                    </form><!--//form-->
                </div><!-- //.col-12-->
            </div><!--/row-->
        </div><!-- //popup-body --> 
    </div><!-- //Suggest Another Time Popup End -->

    <!-- thankyou-popup-popup -->
    <div id="thankyou-popup" class="mfp-with-anim mfp-hide thankyou">
        <div class="popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close"></button>
            <div class="row">
                <div class="col-12">
                    <h1><img src="{{ asset('broker/images//svg-icons/success-icon.svg') }}" alt=""></h1>
                    <h2>Thankyou for Assigning Expert</h2>
                    <span>Expert will get notification to get report done</span>
                    <!-- <a href="javascript:void(0);" class="btn btn-primary">Ok</a> -->
                    <input type="submit" class="btn btn-primary" value="Ok">
                </div><!-- //.col-12 -->
            </div><!--/row-->
        </div><!-- //popup-body --> 
    </div><!-- //thankyou-popup-popup -->    

    @endsection   
