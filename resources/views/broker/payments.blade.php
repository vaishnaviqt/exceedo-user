@extends('broker.layouts.master')
@section('title')
 Payments
@endsection
@section('class')
 paymentsBody
@endsection
@section('content')
                    <div class="row">
                        <div class="col-6">
                            <h2>Payments</h2>   
                        </div><!-- //col-6 -->

                        <div class="col-6 text-right showBy mb-1">
                            <div class="dropdown">
                                <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="selected">{{ $paginate ?? 10 }}</span><span class="caret"></span></a>
                              <ul class="dropdown-menu">
                <li><a href="#"  class="paymentPaginate" data-value="10">10</a></li>
                <li><a href="#"  class="paymentPaginate" data-value="25">25</a></li>
                <li><a href="#"  class="paymentPaginate" data-value="50">50</a></li>
                <li><a href="#"  class="paymentPaginate" data-value="100">100</a></li>
                              </ul>
                            </div><!-- //dropdown -->
                            <span class="float-right pt-2">Show</span>
                        </div><!-- //col-6 showBy -->
    <form method="post" id="paginateSubmitForm">
        @csrf
        <input type="hidden" name="paginate" id="paginateHiddenFiled">
    </form>
    
                        <div class="col-12">
                            <!-- //Search Form Div Start -->
                                <div class="searchForm">
                                    <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                   
            <form action="{{ url('broker/payments')}}" method="post">
                @csrf
                                        <span>Search</span>
                                        <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('broker/images/svg-icons/close-icon.svg') }}" alt=""></a>
                                        <ul class="d-flex flex-wrap">
                                            <li>
                                                <div class="form-group">
                            <input class="form-control" id="paymentDatepicker" type="text" placeholder="Date" name="date" value="{{ !empty($date) ? Carbon\Carbon::parse($date)->format('d-m-Y') : '' }}"/>
                                                </div><!-- //form-group -->
                                            </li>
                                            <li>
                                                <div class="form-group">
                            <input type="text" class="form-control" placeholder="Payment Method" 
                                   value="{{$paymentType ?? ''}}" id="paymentType" name="paymentType">
                                                </div><!-- //form-group -->
                                            </li>
                                            <li>
                        <button type="submit" class="btn btn-primary">Search</button>
                                            </li>
                                        </ul><!-- //ul -->
                                    </form><!-- //form -->
                                </div><!-- //search-form -->
                            <!-- //Search Form Div End -->
                        </div><!-- //col-12 -->

                        <div class="col-12">
                            <div class="payment-table-desktop border">
                                <table class="table table-hover">
                                    <thead>
                                      <tr>
                        <th>Name</th>
                        <th>Transaction Id</th>
                        <th>Payment Method</th>
                                        <th>Amount</th>
                        <th>Bank</th>
                        <th>Status</th>
                                        <th>Payment at</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                    @forelse($payments as $payment)
                                      <tr>
                        <td scope="row">{{ucwords($payment->user->name)}}</td>
                        <td>{{$payment->transaction_id}}</td>
                        <td>{{$payment->method}}</td>
                        <td>{{$payment->currency}} {{$payment->amount}}</td>
                        <td>{{$payment->bank}}</td>
                        <td>{{$payment->status}}</td>
                        <td>{{ isset($payment->created_at) ? date('d M Y, H:i a', strtotime($payment->created_at)) : '' }}</td>
                                      </tr>
                    @empty
                                      <tr>
                        <td scope="row" colspan="7">No Records Available</td>
                                      </tr>
                    @endforelse
                                    </tbody>
                                </table>
                                <div class="col-12">
                  {{ $payments->links() }}
                                </div><!-- //col-12 pagination -->
                            </div><!--//payment-table-desktop-->

                            <div class="payment-table-Mobile border">
            @forelse($payments as $payment)
                                <div class="d-flex flex-wrap justify-content-between payment-list">
                <div><b>{{ucwords($payment->user->name)}}</b></div>
                <div class="w-100"><b>{{$payment->transaction_id}}</b></div>
                <div>{{$payment->method}}</div>
                <div>{{$payment->currency}} {{$payment->amount}}</div>
                <div>{{$payment->bank}}</div>
                <div>{{$payment->status}}</div>
                <div>{{ isset($payment->created_at) ? date('d M Y, H:i a', strtotime($payment->created_at)) : '' }}</div>
                                </div><!-- //d-flex flex-wrap-->
            @empty
                                <div class="d-flex flex-wrap justify-content-between payment-list">
                <p> No Records Available </p>
            </div>
            @endforelse

                                <div class="col-12">
                {{ $payments->links() }}
                                </div><!-- //col-12 pagination -->
                            </div><!--//payment-table-Mobile-->
                        </div><!-- //col-12 -->

                    </div><!-- //row -->                        
                 @endsection   
@section('modal')
@endsection   
@section('custom-script')
<script type="text/javascript">
    $('.paymentPaginate').on('click', function (e) {
        e.preventDefault();
        var value = $(this).data('value');
        $('#paginateHiddenFiled').val(value);
        $('#paginateSubmitForm').submit();
        return false;
    });
</script>
<script>
    $(document).ready(function () {
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#paymentDatepicker').datepicker({
            maxDate: today,
            format: 'dd-mm-yyyy'
        });
    })
</script>
@endsection
