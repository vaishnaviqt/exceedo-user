
@php
$notifications = notificationData('expert');

@endphp

<a href="javascript:void(0);" class="hamburgerMenu"></a>
<div class="mobileLogo">
    <a href="{{ route('broker.appointments') }}">
        <img src="{{ asset('broker/images/logo.svg') }}" alt="Know your Land" /> 
    </a>
</div>


<div class="headRightLinks">
    <a href="javscript:void(0);" class="notificationLink"   id="headNotiDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        @php
        $totalNoti ='';
        if(count($notifications)){
        $totalNoti = count($notifications);
        }
        @endphp
        @if(!empty($totalNoti))
        <span>{{ $totalNoti }}</span>
        @endif
        <img src="{{ asset('broker/images/svg-icons/noti-icon.svg') }}" alt="notification" />
        <em>Notifications</em>
    </a>             
    <div class="dropdown-menu notificationDropdown" aria-labelledby="headNotiDropdown">
        @if(count($notifications))
        @foreach($notifications as $noti)
        <div class="notiData">
             @php
            if(!empty($noti->apppointment_id)){
            $url = url('expert/appointments');
            }elseif(!empty($noti->property_id)){
            $url = url('expert/your-land-listing');
            }elseif(!empty($noti->land_report_id)){
            $url = url('expert/projects');
            }elseif(!empty($noti->service_enquiry_id)){
            $url = url('expert/service-enquiry');
            }else{
            $url = url('expert/dashboard');
            }
            @endphp
            <a href="{{ $url }}">
                {{ $noti->message }}</a>
        </div>
        @endforeach
        @endif
    </div> 

    <a href="javscript:void(0);">
        @php
        $path = asset('broker/images/svg-icons/user-icon.svg');
        if(Storage::disk('broker')->exists(str_replace('broker_image', '', auth()->user()->photo)))
        {
        $path = Storage::disk('broker')->url(str_replace('broker_image/', '', auth()->user()->photo));
        }
        @endphp
        <img class="rounded-circle" src="{{ $path }}" alt="User" />
        <em class="headerUserName">Hi, {{ auth()->user()->name }}</em>
    </a>
</div>
