<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>{{ config('app.name', 'Exceedo') }} &#8211; @yield('title')</title>

<link rel="stylesheet" type="text/css" href="{{ asset('broker/css/bootstrap.min.css') }}">

<!-- //FullCalendar JS -->
<link rel="stylesheet" type="text/css" href="{{ asset('broker/css/calendar-main.css') }}"><!-- //Magnific Popup Css -->
<link rel="stylesheet" href="{{ asset('broker/css/magnific-popup.css') }}"><!-- //M-customScrollbar Css -->
<link rel="stylesheet" href="{{ asset('broker/css/jquery.mCustomScrollbar.min.css') }}"><!-- //Float Label Css -->
<link rel="stylesheet" type="text/css" href="{{ asset('broker/css/bootstrap-float-label.min.css') }}"><!-- //Time Picker Css -->

<link href="{{ asset('broker/css/gijgo.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('broker/css/broker-style.css?v=1.1') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('broker/css/broker-responsive.css?v=1.1') }}">  
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css" />
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ asset('broker/js/jquery.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('upload/css/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ asset('upload/css/jquery.fileupload-ui.css') }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="base_url" content="{{ url('/') }}">