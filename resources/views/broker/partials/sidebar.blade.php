<a href="javascript:void(0);" class="hamburgerMenu"></a>
<div class="sidebarLogo">
    <a href="{{ route('broker.projects') }}">
        <img src="{{ asset('broker/images/logo.svg') }}" alt="Know your Land" />
        <span>Know your Land</span>
    </a>
</div><!--//sidebarLogo-->

<div class="sideNavigation customScrollbar">
    <ul>
        <!--<li><a href="{{ route('broker.appointments') }}" class="nav_appoint @if( Request::route()->getName() == 'broker.appointments' ) active @endif">Appointments</a></li>--> 
        <li><a href="{{ route('broker.projects') }}" class="nav_projects @if( Request::route()->getName() == 'broker.projects' ) active @endif">Projects</a></li>
        <li><a href="{{ route('broker.payments') }}" class="nav_payments @if( Request::route()->getName() == 'broker.payments' ) active @endif">Payments</a></li>
        <li><a href="{{ route('broker.your-land-report') }}" class="nav_reports @if( Request::route()->getName() == 'broker.your-land-report' || Request::route()->getName() == 'broker.your-land-report-delivered' || Request::route()->getName() == 'broker.your-land-report-inprogress' || Request::route()->getName() == 'broker.your-land-report-pending' ) active @endif">Your Land Report</a></li>
        <!--<li><a href="{{ route('broker.reviews') }}" class="nav_rating @if( Request::route()->getName() == 'broker.reviews' ) active @endif">Rating & Reviews</a></li>-->
        <li><a href="{{ route('broker.contacts') }}" class="nav_rating @if( Request::route()->getName() == 'broker.contacts' ) active @endif">Agent Contact</a></li>
        <li><a href="{{ route('broker.messages') }}" class="nav_msg @if( Request::route()->getName() == 'broker.messages' ) active @endif">Message to Admin</a></li>
        <li><a href="{{ route('broker.profile') }}" class="nav_profile @if( Request::route()->getName() == 'broker.profile' ) active @endif">Your Profile</a></li>
        <li><a href="{{ route('broker.brokerlogout') }}" class="nav_logout">Logout</a></li>
    </ul>
</div><!--//navigation-->



