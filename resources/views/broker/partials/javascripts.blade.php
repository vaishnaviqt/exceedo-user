
<script type="text/javascript" src="{{ asset('broker/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('broker/js/bootstrap.min.js') }}"></script>  
<!-- //FullCalendar JS -->
<script type="text/javascript" src="{{ asset('broker/js/calendar-main.js') }}"></script><!-- //Magnific Popup JS -->
<script type="text/javascript" src="{{ asset('broker/js/jquery.magnific-popup.min.js') }}"></script><!-- //M-customScrollbar -->
<script type="text/javascript" src="{{ asset('broker/js/jquery.mCustomScrollbar.concat.min.js') }}"></script><!-- //Time Picker js -->

<script type="text/javascript" src="{{ asset('broker/js/gijgo.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('broker/js/expert-custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/vendor/jquery.ui.widget.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/load-image.all.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/canvas-to-blob.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/jquery.fileupload.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/jquery.fileupload-process.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/jquery.fileupload-validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('upload/js/jquery.fileupload-image.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>