<div class="searchForm">
    <a href="javascript:void(0);" class="open-mobile-search">Search</a>   
    @php
    $url = '';
    if(request()->is('broker/projects')){
    $url = url('broker/projects');
    }elseif(request()->is('broker/active-projects')){
    $url = url('broker/active-projects');
    }elseif(request()->is('broker/inactive-projects')){
    $url = url('broker/inactive-projects');
    }elseif(request()->is('broker/deleted-projects')){
    $url = url('broker/deleted-projects');
    }
    @endphp
    <form action="{{ $url }}" method="POST">
        @csrf
        <span>Search</span>
        <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('broker/images/svg-icons/close-icon.svg') }}" alt=""></a>
        <ul class="d-flex flex-wrap">
            <li>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Land Name" 
                           value="{{$name ?? ''}}" id="Name" name="name">
                </div><!-- //form-group -->
            </li>
            <li>
                <div class="form-group">
                    <select class="form-control" id="reportType" name="filter_business_type">
                        <option value="">{{ __('Land Type') }}</option>
                        <option value="sale" @if($businessType == 'sale') {{ __('selected') }} @endif>{{ __('Sale') }}</option>
                        <option value="collaboration" @if($businessType == 'collaboration') {{ __('selected') }} @endif>{{ __('Collaboration') }}</option>
                        <option value="lease" @if($businessType == 'lease') {{ __('selected') }} @endif>{{ __('Lease') }}</option>
                    </select>
                </div><!-- //form-group --> 
            </li>
            <li>
                <div class="form-group">
                    <select class="form-control f_areaIcon" name="land_area">
                        <option value="">Land area</option>
                        <option value="10-20" @if($landArea == "10-20") {{ ('selected') }} @endif>10-20</option>
                        <option value="20-50" @if($landArea == "20-50") {{ ('selected') }} @endif>20-50</option>
                        <option value="50-100" @if($landArea == "50-100") {{ ('selected') }} @endif>50-100</option>
                        <option value="100-150" @if($landArea == "100-150") {{ ('selected') }} @endif>100-150</option>
                        <option value="150-200" @if($landArea == "150-200") {{ ('selected') }} @endif>150-200</option>
                        <option value="200" @if($landArea == "200") {{ ('selected') }} @endif>200+</option>
                    </select>
                </div><!-- //form-group -->
            </li>
            <li>
                <div class="form-group">
                    <input class="form-control" id="datepicker1" type="text" name="created_at" 
                           value="{{ !empty($startDate) ? Carbon\Carbon::parse($startDate)->format('d-m-Y') : '' }}" placeholder="Date"/>
                </div><!-- //form-group -->
            </li>
            <li>
                <button type="submit" class="btn btn-primary">Search </button>
            </li>
        </ul><!-- //ul -->
    </form><!-- //form -->
</div><!-- //search-form -->

<script>
    $(document).ready(function () {
        var today, datepicker;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        datepicker = $('#datepicker1').datepicker({
            maxDate: today,
            format: 'dd-mm-yyyy'
        });
    })
</script>