@extends('broker.layouts.master')
@section('title')
Land Listing
@endsection
@section('class')
reportsBody
@endsection
@section('content')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<h2>Deleted Projects</h2> 
@include('broker.projects.header')
<div class="tab-content"> 
    <!-- //Search Form Div Start -->
    @include('broker.projects.__common_filter')  
    <!-- //Search Form Div End -->

    <div class="reportsListing">
        <div class="row row_20">
            @forelse($deleteLands as $deleteLand)
            <div class="col-md-6">
                <article class="reportArticle">
                    <div class="repArtImg">
                        <img src="{{ asset($deleteLand->image) }}" />
                    </div>
                    <div class="repArtContent landArtCont"> 

                        <!--                        <a href="javacript:void(0);" class="moreOptLink"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                                                <div class="dropdown-menu dropdown-menu-right moreLink-menu">
                                                    <a href="javacript:void(0);">Your Views : 132</a>
                                                    <a href="javacript:void(0);" data-toggle="modal" data-target="#interestedPeople">Interested Peoples</a>
                                                    <a href="javacript:void(0);">Renew Listing</a>
                                                    <a href="javacript:void(0);">Delete</a> 
                                                </div>//moreLink-menu-->

                        <div class="row">
                            <div class="col-7">
                                <h2>{{ ucwords($deleteLand->land_name) }}<em class="featureIcon"></em></h2> 
                                <p>{{ $deleteLand->address }}</p>
                            </div>
                            <div class="col-5">
                                <div class="activeLand">
                                    <span class="badge badge-warning float-right">{{ __('Deleted') }}</span>
                                </div>
                            </div>
                        </div><!--//row-->

                        <div class="landCost">
                            <div class="row row_20 align-items-center">
                                <div class="col-lg-4">
                                    <div class="landArtPrice">₹ {{$deleteLand->max_price}} {{$deleteLand->price_unit}}</div>
                                </div>
                                <div class="col-lg-8 text-lg-right">
                                    <div class="expDetail">

                                    </div>
                                </div>
                            </div><!--//row-->
                        </div><!--//landCost-->

                        <div class="featFaciRow">
                            @if($deleteLand->wifi  == 1 || $deleteLand->parking == 1) 
                            <div class="featFacilities">
                                <span class="featFaTitle">Facilities :</span>
                                @if($deleteLand->parking == 1)
                                <span class="faci-icon parking-icon"></span>
                                @endif
<!--                                    <span class="faci-icon noSmooking-icon"></span>-->
                                @if($deleteLand->wifi == 1)
                                <span class="faci-icon wifi-icon"></span>
                                @endif
                            </div>
                            @endif
                            <div class="landArtviews">
                                {{$deleteLand->created_at->diffForHumans()}} &bull; {{ $deletedLand->views ?? 0 }} views
                            </div>
                        </div><!--//featFacilities-->
                    </div><!--//repArtContent-->
                </article><!--//reportArticle-->
            </div><!--//col-md-6--> 
            @empty                     
            <div class="col-12">
                <div class="no-content-msg">   
                    No Property Available 
                </div>
            </div>
            @endforelse
        </div><!--//row-->
    </div><!--//reportsListing-->
    {{ $deleteLands->links() }}
</div><!--//tab-content-->
@endsection   
@section('modal')
@endsection   

