@extends('broker.layouts.master')
@section('title')
Land Listing
@endsection
@section('class')
reportsBody
@endsection
@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<h2>All Projects</h2> 
@include('broker.projects.header')

<div class="tab-content"> 
    <!-- //Search Form Div Start -->
@include('broker.projects.__common_filter')    
    <!-- //Search Form Div End -->

    <div class="reportsListing">
        <div class="row row_20">
            @forelse($allLands as $land)
            <div class="col-md-6">
                <article class="reportArticle">
                    <div class="repArtImg">
                        <img src="{{ asset($land->image) }}" />
                    </div>
                    <div class="repArtContent landArtCont">

                        <div class="row">
                            <div class="col-7">
                                <h2> {{ ucwords($land->land_name) }} <em class="featureIcon"></em></h2> 
                                <p>{{ $land->address }}</p>
                            </div>
                            <div class="col-5">
                                <div class="activeLand">
                                    @if($land->status == 1)
                                    <span class="badge badge-success float-right mr-0">{{ __('Active') }}</span><!--//badge-success-->
                                    @elseif(!empty($land->deleted_at))
                                    <span class="badge badge-warning float-right mr-0">{{ __('Deleted') }}</span>
                                    @elseif($land->status == 0)
                                    <span class="badge badge-danger float-right mr-0">{{ __('Inactive') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div><!--//row-->

                        <div class="landCost">
                            <div class="row row_20 align-items-center">
                                <div class="col-lg-4">
                                    <div class="landArtPrice">₹ {{$land->max_price}} {{$land->price_unit}}</div>
                                </div>
                                <div class="col-lg-8 text-lg-right">
                                    <div class="expDetail">

                                    </div>
                                </div>
                            </div><!--//row-->
                        </div><!--//landCost-->

                        <div class="featFaciRow">
                            @if($land->wifi  == 1 || $land->parking == 1) 
                                <div class="featFacilities">
                                    <span class="featFaTitle">Facilities :</span>
                                    @if($land->parking == 1)
                                    <span class="faci-icon parking-icon"></span>
                                    @endif
<!--                                    <span class="faci-icon noSmooking-icon"></span>-->
                                    @if($land->wifi == 1)
                                    <span class="faci-icon wifi-icon"></span>
                                    @endif
                                </div>
                             @endif
                            <div class="landArtviews">
                                {{$land->created_at->diffForHumans()}} &bull; {{ $land->views }} views
                            </div>
                        </div><!--//featFacilities-->
                    </div><!--//repArtContent-->
                </article><!--//reportArticle-->
            </div><!--//col-md-6--> 
            @empty                     
            <div class="col-12">
                <div class="no-content-msg">   
                    No Property Available 
                </div>
            </div>
            @endforelse
        </div><!--//row-->
    </div><!--//reportsListing-->
    {{ $allLands->links() }}
</div><!--//tab-content-->

@endsection   
@section('modal')
@endsection   

