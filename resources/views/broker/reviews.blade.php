@extends('broker.layouts.master')
@section('title')
Rating & Reviews
@endsection
@section('class')
 paymentsBody
@endsection
@section('content')


               

                @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



                    
                <h2>Rating & Reviews</h2> 

                <!-- //Search Form Div Start -->
                    <div class="searchForm">
                        <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                   
                        <form>
                            <span>Search</span>
                            <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('broker/images/svg-icons/close-icon.svg') }}" alt=""></a>
                            <ul class="d-flex flex-wrap">
                                <li>
                                    <div class="form-group">
                                        <input class="form-control" id="datepicker" type="text" placeholder="Date"/>
                                    </div><!-- //form-group -->
                                </li>
                                <li>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Name" id="name">
                                    </div><!-- //form-group -->
                                </li>
                                <li>
                                    <div class="form-group">
                                        <select class="form-control" id="paymentType">
                                            <option>Rating Star</option>
                                            <option>1 Star </option>
                                            <option>2 Star</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li>
                                    <div class="form-group">
                                        <select class="form-control" id="paymentType">
                                            <option>Leads Suggestion</option>
                                            <option>Leads Suggestion 01</option>
                                            <option>Leads Suggestion 02</option>
                                        </select>
                                    </div><!-- //form-group --> 
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="btn btn-primary">Search</a>
                                </li>
                            </ul><!-- //ul -->
                        </form><!-- //form -->
                    </div><!-- //search-form -->
                <!-- //Search Form Div End -->

                <div class="rating-reviews">
                    <h4>10 Nov 2020</h4>
                    <div class="comments-review">
                        <div class="d-flex justify-content-start flex-lg-nowrap flex-wrap mb-2">
                            <div><img class="d-flex align-self-start mr-3" src="{{ asset('broker/images/review-img.png') }}" alt="No Image"></div>
                            <div>
                                <h5>Mahira Sharma</h5>
                                <span>mahirasharma.21@gmail.com</span>
                            </div>
                            <div>
                                <div class="Stars" style="--rating: 3;" aria-label="Rating of this product is 3.3 out of 5."></div>
                                <label><span>Lead Suggest :</span> Land title search, Listing Property</label>                                            
                            </div>
                            <div>
                                <span>Appointment Date and Time</span>
                                <label>10 Nov 2020, 08:30 pm to 09:00 pm</label>
                            </div>
                        </div><!--//d-flex justify-content-start flex-lg-nowrap flex-wrap-->
                        <div><span>Feedback : </span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                    </div><!--//comments-review-->

                    <div class="comments-review">
                        <div class="d-flex justify-content-start flex-lg-nowrap flex-wrap mb-2">
                            <div><img class="d-flex align-self-start mr-3" src="{{ asset('broker/images/svg-icons/no-image.svg') }}" alt="No Image"></div>
                            <div>
                                <h5>Abhit Bhatia</h5>
                                <span>abhitbhatia1@gmail.com</span>
                            </div>
                            <div>
                                <div class="Stars" style="--rating: 4.5;" aria-label="Rating of this product is 3.3 out of 5."></div>
                                <label><span>Lead Suggest :</span> Land title search, Listing Property</label>                                            
                            </div>
                            <div>
                                <span>Appointment Date and Time</span>
                                <label>10 Nov 2020, 08:30 pm to 09:00 pm</label>
                            </div>
                        </div><!--//d-flex justify-content-start flex-lg-nowrap flex-wrap-->
                        <div><span>Feedback : </span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                    </div><!--//comments-review-->

                    <div class="comments-review">
                        <div class="d-flex justify-content-start flex-lg-nowrap flex-wrap mb-2">
                            <div><img class="d-flex align-self-start mr-3" src="{{ asset('broker/images/svg-icons/no-image.svg') }}" alt="No Image"></div>
                            <div>
                                <h5>Abhit Bhatia</h5>
                                <span>abhitbhatia1@gmail.com</span>
                            </div>
                            <div>
                                <div class="Stars" style="--rating: 4.5;" aria-label="Rating of this product is 3.3 out of 5."></div>
                                <label><span>Lead Suggest :</span> Land title search, Listing Property</label>                                            
                            </div>
                            <div>
                                <span>Appointment Date and Time</span>
                                <label>10 Nov 2020, 08:30 pm to 09:00 pm</label>
                            </div>
                        </div><!--//d-flex justify-content-start flex-lg-nowrap flex-wrap-->
                        <div><span>Feedback : </span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                    </div><!--//comments-review-->                            
                </div><!--//rating-reviews-->

                <div class="rating-reviews">
                    <h4>09 Nov 2020</h4>
                    <div class="comments-review">
                        <div class="d-flex justify-content-start flex-lg-nowrap flex-wrap mb-2">
                            <div><img class="d-flex align-self-start mr-3" src="{{ asset('broker/images/review-img.png') }}" alt="No Image"></div>
                            <div>
                                <h5>Mahira Sharma</h5>
                                <span>mahirasharma.21@gmail.com</span>
                            </div>
                            <div>
                                <div class="Stars" style="--rating: 3;" aria-label="Rating of this product is 3.3 out of 5."></div>
                                <label><span>Lead Suggest :</span> Land title search, Listing Property</label>                                            
                            </div>
                            <div>
                                <span>Appointment Date and Time</span>
                                <label>10 Nov 2020, 08:30 pm to 09:00 pm</label>
                            </div>
                        </div><!--//d-flex justify-content-start flex-lg-nowrap flex-wrap-->
                        <div><span>Feedback : </span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                    </div><!--//comments-review-->

                    <div class="comments-review">
                        <div class="d-flex justify-content-start flex-lg-nowrap flex-wrap mb-2">
                            <div><img class="d-flex align-self-start mr-3" src="{{ asset('broker/images/svg-icons/no-image.svg') }}" alt="No Image"></div>
                            <div>
                                <h5>Abhit Bhatia</h5>
                                <span>abhitbhatia1@gmail.com</span>
                            </div>
                            <div>
                                <div class="Stars" style="--rating: 4.5;" aria-label="Rating of this product is 3.3 out of 5."></div>
                                <label><span>Lead Suggest :</span> Land title search, Listing Property</label>                                            
                            </div>
                            <div>
                                <span>Appointment Date and Time</span>
                                <label>10 Nov 2020, 08:30 pm to 09:00 pm</label>
                            </div>
                        </div><!--//d-flex justify-content-start flex-lg-nowrap flex-wrap-->
                        <div><span>Feedback : </span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                    </div><!--//comments-review-->

                    <div class="comments-review">
                        <div class="d-flex justify-content-start flex-lg-nowrap flex-wrap mb-2">
                            <div><img class="d-flex align-self-start mr-3" src="{{ asset('broker/images/svg-icons/no-image.svg') }}" alt="No Image"></div>
                            <div>
                                <h5>Abhit Bhatia</h5>
                                <span>abhitbhatia1@gmail.com</span>
                            </div>
                            <div>
                                <div class="Stars" style="--rating: 4.5;" aria-label="Rating of this product is 3.3 out of 5."></div>
                                <label><span>Lead Suggest :</span> Land title search, Listing Property</label>                                            
                            </div>
                            <div>
                                <span>Appointment Date and Time</span>
                                <label>10 Nov 2020, 08:30 pm to 09:00 pm</label>
                            </div>
                        </div><!--//d-flex justify-content-start flex-lg-nowrap flex-wrap-->
                        <div><span>Feedback : </span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                    </div><!--//comments-review-->                            
                </div><!--//rating-reviews-->                             

                

                 @endsection   


@section('modal')

                  
@endsection   
