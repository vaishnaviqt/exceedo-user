@extends('broker.layouts.master')
@section('title')
Payments
@endsection
@section('class')
paymentsBody
@endsection
@section('content')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div>
    <div class="pageTitle d-flex align-items-center">
        <h2>Agent Contacts</h2>
        
        <div class="sortDropdown">
            <label>Show</label>
            <div class="dropdown custom-dropdown">
                <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle min-w-60">
                    <span class="selected">{{ $paginate ?? 10 }}</span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" class="contactPaginate" data-value="10">10</a></li>
                    <li><a href="#" class="contactPaginate" data-value="25">25</a></li>
                    <li><a href="#" class="contactPaginate" data-value="50">50</a></li>
                    <li><a href="#" class="contactPaginate" data-value="100">100</a></li>
                </ul>
            </div><!-- //dropdown --> 
            <form method="post" id="paginateSubmitForm">
                @csrf
                <input type="hidden" name="paginate" id="paginateHiddenFiled">
            </form> 
        </div><!--//sortDropdown-->
    </div><!--//pageTitle-->
 

    <!-- //Search Form Div Start -->
    <!--        <div class="searchForm">
                <a href="javascript:void(0);" class="open-mobile-search">Search</a>                                   
                    <form>
                    
                    <span>Search</span>
                    <a href="javascript:void(0);" class="closeSearch" id="closeSearch"><img src="{{ asset('broker/images/svg-icons/close-icon.svg') }}" alt=""></a>
                    <ul class="d-flex flex-wrap">
                        <li>
                            <div class="form-group">
                                <input class="form-control" id="datepicker" type="text" placeholder="Date" name="date" value="{{ !empty($date) ? Carbon\Carbon::parse($date)->format('d-m-Y') : '' }}"/>
                            </div> //form-group 
                        </li>
                        <li>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Name" id="name">
                            </div> //form-group 
                        </li>
                        <li>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Appointment" id="appointment">
                            </div> //form-group 
                        </li>
                        <li>
                            <div class="form-group">
                                <select class="form-control" id="paymentType" name="paymentType">
                                    <option value="">Payment Method</option>
                                    
                                </select>
                            </div> //form-group  
                        </li>
                        <li>
                            <button type="submit" class="btn btn-primary">Search</button>
                        </li>
                    </ul> //ul 
                </form> //form 
            </div> //search-form -->
    <!-- //Search Form Div End --> 

    <div class="payment-table-desktop border">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Land Name</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                @forelse($contacts as $contact)
                <tr>
                    <td scope="row">{{isset($contact->land->land_name) ? ucwords($contact->land->land_name) : ''}}</td>
                    <td>{{ucwords($contact->name)}}</td>
                    <td>{{$contact->mobile}}</td>
                    <td>{{$contact->email}}</td>
                    <td>{{ isset($contact->created_at) ? date('d M Y, H:i a', strtotime($contact->created_at)) : '' }}</td>
                </tr>
                @empty
                <tr>
                    <td scope="row" colspan="7">No Records Available</td>
                </tr>
                @endforelse
            </tbody>
        </table>

        <div class="col-12">
            {{ $contacts->links() }}
        </div><!-- //col-12 pagination -->
    </div><!--//payment-table-desktop-->


    <div class="payment-table-Mobile border">
        @forelse($contacts as $contact)
        <div class="d-flex flex-wrap justify-content-between payment-list">
            <div><b>{{isset($contact->land->land_name) ? ucwords($contact->land->land_name) : ''}}</b></div>
            <div class="w-100"><b>{{$contact->name}}</b></div>
            <div>{{$contact->mobile}}</div>
            <div>{{$contact->email}}</div>
            <div>{{ isset($contact->created_at) ? date('d M Y, H:i a', strtotime($contact->created_at)) : '' }}</div>
        </div><!-- //d-flex flex-wrap-->
        @empty
        <div class="d-flex flex-wrap justify-content-between payment-list">
            <p> No Records Available </p>
        </div>
        @endforelse

        <div class="col-12">
            {{ $contacts->links() }}
        </div><!-- //col-12 pagination -->
    </div><!--//payment-table-Mobile-->
    
</div>                       
@endsection   
@section('modal')
@endsection   
@section('custom-script')
<script type="text/javascript">
    $('.contactPaginate').on('click', function (e) {
        e.preventDefault();
        var value = $(this).data('value');
        $('#paginateHiddenFiled').val(value);
        $('#paginateSubmitForm').submit();
        return false;
    });
</script>
@endsection

