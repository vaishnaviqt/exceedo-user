<!DOCTYPE html>
<html lang="en">
<head>
     @include('broker.partials.head')
             <style>

            .loader{
                position: fixed;
                top:0;
                left:0;
                right: 0;
                bottom:0;
                display:flex;
                align-items:center;
                justify-content:center;
                z-index: 9999;
                background: #ffffffa6;
            }
            .loader img{
                width:80px;
            }
        </style>
</head>
<body>  
    <div class="loader d-none">
            <img src="{{ asset('images/Spin-1s-200px.gif') }}">
        </div>
    <div class="wrapper">
        <div class="sidebar">

             @include('broker.partials.sidebar')
 
        </div><!--//sidebar-->


        <div class="main">
            <header class="header"> 

                 @include('broker.partials.header')
            </header><!--//header-->


            <section class="pageContainer @yield('class')" id="inline-popups">
                 @yield('content')
            </section><!--//pageContainer-->

            <footer class="footer">
                 @include('broker.partials.footer')
            </footer>    
        
        </div><!--//main-->

    </div><!--//wrapper--> 

   


     
    
    @yield('modal')
    @include('broker.partials.javascripts')
    
    @section('custom-script')
    @show
    
    <script>
        // Inline popups
//        $('#inline-popups').magnificPopup({
//            delegate: 'a',
//            removalDelay: 500,
//            callbacks: {
//                beforeOpen: function() {
//                this.st.mainClass = this.st.el.attr('data-effect');
//                }
//            },
//            midClick: true
//        });

        //FullCalendar Start
//         document.addEventListener('DOMContentLoaded', function() {
//            var calendarEl = document.getElementById('calendar');
//
//            var calendar = new FullCalendar.Calendar(calendarEl, {
//            headerToolbar: {
//                left: 'prev,next today',
//                center: 'title',
//                right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
//            },
//            initialDate: '2020-12-21',
//            navLinks: true, // can click day/week names to navigate views
//            businessHours: true, // display business hours
//            editable: true,
//            selectable: true,
//            events: [
//                {
//                title: 'Business Lunch',
//                start: '2020-12-03T13:00:00',
//                constraint: 'businessHours'
//                },
//                {
//                title: 'Meeting',
//                start: '2020-12-13T11:00:00',
//                constraint: 'availableForMeeting', // defined below
//                color: '#257e4a'
//                },
//                {
//                title: 'Conference',
//                start: '2020-12-18',
//                end: '2020-12-20'
//                },
//                {
//                title: 'Party',
//                start: '2020-12-29T20:00:00'
//                },
//
//                // areas where "Meeting" must be dropped
//                {
//                groupId: 'availableForMeeting',
//                start: '2020-12-11T10:00:00',
//                end: '2020-12-11T16:00:00',
//                display: 'background'
//                },
//                {
//                groupId: 'availableForMeeting',
//                start: '2020-12-13T10:00:00',
//                end: '2020-12-13T16:00:00',
//                display: 'background'
//                },
//
//                // red areas where no events can be dropped
//                {
//                start: '2020-12-24',
//                end: '2020-12-28',
//                overlap: false,
//                display: 'background',
//                color: '#ff9f89'
//                },
//                {
//                start: '2020-12-06',
//                end: '2020-12-08',
//                overlap: false,
//                display: 'background',
//                color: '#ff9f89'
//                }
//            ]
//            });
//
//            calendar.render();
//        });
//        //FullCalendar End
//
//        $(document).ready(function(){
//            $(".appointments").mCustomScrollbar({
//                theme           : "dark",
//                scrollButtons   : { scrollType: "stepped" },
//                live            : "on"
//            });
//        });
    </script>
    
 
</body>
</html>