@extends('broker.layouts.master')
@section('title')
Message to Admin
@endsection
@section('class')
message-admin
@endsection
@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if(session()->has('success'))
<div class="alert alert-success">
    {{ session()->get('success') }}
</div>
@endif
@if(session()->has('error'))
<div class="alert alert-danger">
    {{ session()->get('error') }}
</div>
@endif
<div>
    
    <div class="pageTitle d-flex align-items-center">
        <h2>Message to Admin</h2> 
    </div><!--//pageTitle--> 

    <div>
        <form action="{{ url('broker/message-send') }}" method="post">
            @csrf
            <div class="form-group mb-4">
                <textarea class="form-control @error('message') is-invalid @enderror" placeholder="Type your Message here" id="message" rows="8" name="message" required></textarea>
                <button type="submit" class="btn btn-primary">Send</button>
                @error('message')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div><!-- //form-group-->
        </form>
    </div><!-- //mb-3 -->

    <div class="pageTitle d-flex align-items-center">
        <h2>All Queries</h2>
        
        <div class="sortDropdown">
            <label>Show by</label>
            <div class="dropdown custom-dropdown">
                <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle">
                    <span class="selected">{{ !empty($showBy) ? $showBy: 'Month' }}</span><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a class="messageShow" data-filter="week" href="javascript:void(0);">Week</a></li>
                    <li><a class="messageShow" data-filter="month" href="javascript:void(0);">Month</a></li>
                    <li><a class="messageShow" data-filter="year" href="javascript:void(0);">Year</a></li>
                </ul>
            </div><!-- //dropdown -->
             
            <form id="messageShowBy" action="{{ url('broker/messages') }}" method="post">
                @csrf
                <input type="hidden" id="filterColumn" name="show_by" value="">
            </form>
        </div><!--//sortDropdown-->
    </div><!--//pageTitle-->
   
    <div class="rating-reviews">
        @forelse($messages as $message)
        <div class="message-box d-md-flex">
            <div>
                <img src="{{ asset('broker/images/svg-icons/message-icon.svg') }}" alt="">
            </div><!-- //div-->
            <div style="width: 90%">
                <p><span>Message : </span> {{ $message->message }}</p>
            </div><!-- //div-->
            <div>
                <span class="day-count">{{ Carbon\Carbon::parse($message->created_at)->diffForHumans() }}</span>
            </div><!-- //div-->
        </div><!--//message-box  d-md-flex-->
        @empty

        <div class="no-content-msg">   
            No Message Available
        </div>
         
        @endforelse 
    </div><!-- //rating-reviews --> 
</div><!-- //row -->  
@endsection   
@section('modal')
@endsection   
