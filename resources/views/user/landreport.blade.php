@extends('user.layouts.master')
@section('title')
  Land Listing
@endsection
@section('class')
 
@endsection
@section('content')


               

                @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

                
               
                

               @include('user.landreportpartials.header')
                    <div class="tab-content">
                        <div class="filterFormSection">
                            <a href="javascript:void(0);" class="mobileFilterBtn">Search </a>                              
                            <div class="filterForm">
                                <h2>
                                    Search
                                    <a href="javascript:void(0);" class="m_filterCloseBtn"></a>
                                </h2>
                                <div class="row">
                                    <div class="col-lg-10">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-6">
                                                <div class="form-group floating-field">
                                                    <input type="text" class="form-control" placeholder="Name" id="Name">
                                                    <label for="Name" class="floating-label">Name</label>
                                                </div>
                                            </div><!--//col-lg-3-->
                                            
                                            <div class="col-lg-3 col-md-6"> 
                                                <div class="form-group floating-field">
                                                    <select class="form-control" id="ReportType" onclick="this.setAttribute('value', this.value);" value="">
                                                        <option></option>
                                                        <option>Report Type 1</option>
                                                        <option>Report Type 2</option>
                                                        <option>Report Type 3</option>
                                                    </select>
                                                    <label for="ReportType" class="floating-label">Report Type</label>
                                                </div>
                                            </div><!--//col-lg-3-->
                                            
                                            <div class="col-lg-3 col-md-6">
                                                <div class="form-group floating-field">
                                                    <input type="text" class="form-control" placeholder="Location" id="Location">
                                                    <label for="Location" class="floating-label">Location</label>
                                                </div>
                                            </div><!--//col-lg-3-->
                                            
                                            <div class="col-lg-3 col-md-6">
                                                <div class="form-group floating-field">
                                                    <input type="date" class="form-control form-calendar2" placeholder="Date" id="Date">
                                                    <label for="Date" class="floating-label">Date</label>
                                                </div>
                                            </div><!--//col-lg-3-->
                                        </div><!--//row-->
                                    </div><!--//col-lg-10-->

                                    <div class="col-lg-2">
                                        <button class="btn btn-primary">Search </button>
                                    </div>
                                </div><!--//row-->
                            </div><!--//filterForm-->
                        </div><!--filterFormSection-->
                        
                        <div class="reportsListing">
                            <div class="row row_20">
                                <div class="col-md-6">
                                    <article class="reportArticle">
                                        <div class="repArtImg">
                                            <img src="{{ asset('user/images/sample-report.png') }}" alt="21 Acres Land Town" />
                                        </div>
                                        <div class="repArtContent">
                                            <a href="javacript:void(0);" class="moreOptLink"></a>
                                            <h2>21 Acres Land Town</h2>
                                            <h3>+9560465467 | abhitbhatia@gmail.com</h3>
                                            <p class="repOneLine repTitleIcon">Land Title Search</p>
                                            <p class="repOneLine repLocIcon">A-35, Sohna Road, Gurgaon, Haryana</p>

                                            <div class="repArtAdditional">
                                                <p class="ArtAuthor">Pending by : <span>Admin</span></p>
                                                <p class="timeDetail">2 days ago</p>
                                            </div>
                                        </div><!--//repArtContent-->
                                    </article><!--//reportArticle-->
                                </div><!--//col-md-6-->
                                
                                <div class="col-md-6">
                                    <article class="reportArticle">
                                        <div class="repArtImg">
                                            <img src="{{ asset('user/images/sample-report.png') }}" alt="21 Acres Land Town" />
                                        </div>
                                        <div class="repArtContent">
                                            <a href="javacript:void(0);" class="moreOptLink"></a>
                                            <h2>21 Acres Land Town</h2>
                                            <h3>+9560465467 | abhitbhatia@gmail.com</h3>
                                            <p class="repOneLine repTitleIcon">Land Title Search</p>
                                            <p class="repOneLine repLocIcon">A-35, Sohna Road, Gurgaon, Haryana</p>

                                            <div class="repArtAdditional">
                                                <p class="ArtAuthor">Inprogress by : <span>Expert Musolani</span></p>
                                                <p class="timeDetail">2 days ago</p>
                                            </div>
                                        </div><!--//repArtContent-->
                                    </article><!--//reportArticle-->
                                </div><!--//col-md-6-->
                                
                                <div class="col-md-6">
                                    <article class="reportArticle">
                                        <div class="repArtImg">
                                            <img src="{{ asset('user/images/sample-report.png') }}" alt="21 Acres Land Town" />
                                        </div>
                                        <div class="repArtContent">
                                            <a href="javacript:void(0);" class="moreOptLink"></a>
                                            <h2>21 Acres Land Town</h2>
                                            <h3>+9560465467 | abhitbhatia@gmail.com</h3>
                                            <p class="repOneLine repTitleIcon">Land Title Search</p>
                                            <p class="repOneLine repLocIcon">A-35, Sohna Road, Gurgaon, Haryana</p>

                                            <div class="repArtAdditional">
                                                <p class="ArtAuthor">Delivered by : <span>Expert Musolani</span></p>
                                                <p class="timeDetail">2 days ago</p>
                                            </div>
                                        </div><!--//repArtContent-->
                                    </article><!--//reportArticle-->
                                </div><!--//col-md-6-->
                                
                                <div class="col-md-6">
                                    <article class="reportArticle">
                                        <div class="repArtImg">
                                            <img src="{{ asset('user/images/sample-report.png') }}" alt="21 Acres Land Town" />
                                        </div>
                                        <div class="repArtContent">
                                            <a href="javacript:void(0);" class="moreOptLink"></a>
                                            <h2>21 Acres Land Town</h2>
                                            <h3>+9560465467 | abhitbhatia@gmail.com</h3>
                                            <p class="repOneLine repTitleIcon">Land Title Search</p>
                                            <p class="repOneLine repLocIcon">A-35, Sohna Road, Gurgaon, Haryana</p>

                                            <div class="repArtAdditional">
                                                <p class="ArtAuthor">Pending by : <span>Admin</span></p>
                                                <p class="timeDetail">2 days ago</p>
                                            </div>
                                        </div><!--//repArtContent-->
                                    </article><!--//reportArticle-->
                                </div><!--//col-md-6-->
                                
                                <div class="col-md-6">
                                    <article class="reportArticle">
                                        <div class="repArtImg">
                                            <img src="{{ asset('user/images/sample-report.png') }}" alt="21 Acres Land Town" />
                                        </div>
                                        <div class="repArtContent">
                                            <a href="javacript:void(0);" class="moreOptLink"></a>
                                            <h2>21 Acres Land Town</h2>
                                            <h3>+9560465467 | abhitbhatia@gmail.com</h3>
                                            <p class="repOneLine repTitleIcon">Land Title Search</p>
                                            <p class="repOneLine repLocIcon">A-35, Sohna Road, Gurgaon, Haryana</p>

                                            <div class="repArtAdditional">
                                                <p class="ArtAuthor">Inprogress by : <span>Expert Musolani</span></p>
                                                <p class="timeDetail">2 days ago</p>
                                            </div>
                                        </div><!--//repArtContent-->
                                    </article><!--//reportArticle-->
                                </div><!--//col-md-6-->
                                
                                <div class="col-md-6">
                                    <article class="reportArticle">
                                        <div class="repArtImg">
                                            <img src="{{ asset('user/images/sample-report.png') }}" alt="21 Acres Land Town" />
                                        </div>
                                        <div class="repArtContent">
                                            <a href="javacript:void(0);" class="moreOptLink"></a>
                                            <h2>21 Acres Land Town</h2>
                                            <h3>+9560465467 | abhitbhatia@gmail.com</h3>
                                            <p class="repOneLine repTitleIcon">Land Title Search</p>
                                            <p class="repOneLine repLocIcon">A-35, Sohna Road, Gurgaon, Haryana</p>

                                            <div class="repArtAdditional">
                                                <p class="ArtAuthor">Delivered by : <span>Expert Musolani</span></p>
                                                <p class="timeDetail">2 days ago</p>
                                            </div>
                                        </div><!--//repArtContent-->
                                    </article><!--//reportArticle-->
                                </div><!--//col-md-6-->
                            </div><!--//row-->
                        </div><!--//reportsListing-->

                        <div class="pagination">
                            <ul>
                                <li class="text-btn"><a href="javascript:void(0);">Prev</a></li>
                                <li><a href="javascript:void(0);" class="active">1</a></li>
                                <li><a href="javascript:void(0);">2</a></li>
                                <li><a href="javascript:void(0);">3</a></li>
                                <li><a href="javascript:void(0);">4</a></li>
                                <li><a href="javascript:void(0);">5</a></li>
                                <li><a href="javascript:void(0);">6</a></li>
                                <li><a href="javascript:void(0);">7</a></li>
                                <li><a href="javascript:void(0);">8</a></li>
                                <li><a href="javascript:void(0);">9</a></li>
                                <li><a href="javascript:void(0);">10</a></li>
                                <li class="text-btn"><a href="javascript:void(0);">Next</a></li>
                            </ul>
                        </div><!--//pagination--> 
                        
                    </div><!--//tab-content-->
                </div><!--//exceedoTabs-->

                 @endsection   


@section('modal')

                 
    @endsection   
