            <a href="javascript:void(0);" class="hamburgerMenu"></a>
            <div class="sidebarLogo">
                <a href="{{ route('user.dashboard') }}">
                    <img src="{{ asset('user/images/logo.svg') }}" alt="Know your Land" />
                    <span>Know your Land</span>
                </a>
            </div><!--//sidebarLogo-->

            <div class="sideNavigation customScrollbar">


                <ul>

                    <li><a href="{{ route('user.dashboard') }}" class="nav_dashboard @if( Request::route()->getName() == 'user.dashboard' ) active @endif">Dashboard</a></li>
                    <li><a href="{{ route('user.your-land-report') }}" class="nav_reports @if( Request::route()->getName() == 'user.your-land-report' || Request::route()->getName() == 'user.your-land-report-delivered' || Request::route()->getName() == 'user.your-land-report-inprogress' || Request::route()->getName() == 'user.your-land-report-pending' ) active @endif">Your Land Report</a></li>
                    <li><a href="{{ route('user.appointments') }}" class="nav_appoint @if( Request::route()->getName() == 'user.appointments' ) active @endif">Appointments</a></li>
                    <li><a href="{{ route('user.your-land-listing') }}" class="nav_land @if( Request::route()->getName() == 'user.your-land-listing' || Request::route()->getName() == 'user.your-land-listing-active' || Request::route()->getName() == 'user.your-land-listing-inactive' || Request::route()->getName() == 'user.your-land-listing-deleted' ) active @endif">Your Land Listing</a></li>
                    <li><a href="{{ route('user.payments') }}" class="nav_payments @if( Request::route()->getName() == 'user.payments' ) active @endif">Payments</a></li>
                    <li><a href="{{ route('user.profile') }}" class="nav_profile @if( Request::route()->getName() == 'user.profile' ) active @endif">Your Profile</a></li>
                    <li><a href="{{ route('user.userlogout') }}" class="nav_logout">Logout</a></li>  
                </ul>
            </div><!--//navigation-->



           



