
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<style>
    #toast-container > .toast-info{
        background-image: none !important;
    }
    #toast-container > .toast-warning{
        content: "";
        background-image: none !important;
    }
    #toast-container > .toast-success{
        content: "";
        background-image: none !important;
    }

</style>

@php
$notifications = notificationData('user');
@endphp
<script>
    $(document).ready(function () {
        toastr.options = {
            timeOut: 0,
            extendedTimeOut: 0,
            "positionClass": "toast-bottom-right",
        };
        toastr.options.closeButton = true;
<?php
if (count($notifications)) {
    foreach ($notifications as $noti) {
        ?>
                toastr.options.closeHtml = '<button class="text-white markReadNoti" data-id="<?php echo $noti->id ?>">×</button>';
                toastr.info("{{ $noti->message}}");
        <?php
    }
}
?>
        $('.markReadNoti').on('click', function () {
            var id = $(this).data('id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "get",
                url: "mark-read-notification/" + id,

                success: function (data) {
                }
            });
        });
    });
</script>

<a href="javascript:void(0);" class="hamburgerMenu"></a>
<div class="mobileLogo">
    <a href="{{ route('user.dashboard') }}">
        <img src="{{ asset('user/images/logo.svg') }}" alt="Know your Land" /> 
    </a>
</div>

<div class="headRightLinks">
    <a href="javscript:void(0);" class="notificationLink"   id="headNotiDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        @if(count($notifications))
        <span>{{ count($notifications) }}</span>
        @endif
        <img src="{{ asset('user/images/svg-icons/noti-icon.svg') }}" alt="notification" />
        <em>Notifications</em>
    </a>             
    <div class="dropdown-menu notificationDropdown" aria-labelledby="headNotiDropdown">
        @if(count($notifications))
        @foreach($notifications as $noti)
        <div class="notiData">
             @php
            if(!empty($noti->apppointment_id)){
            $url = url('user/appointments');
            }elseif(!empty($noti->property_id)){
            $url = url('user/your-land-listing');
            }elseif(!empty($noti->land_report_id)){
            $url = url('user/your-land-report');
            }else{
            $url = url('user/dashboard');
            }
            @endphp
            <a href="{{ $url }}">
                {{ $noti->message }}</a>
        </div>
        @endforeach
        @endif
    </div> 

    <a href="javscript:void(0);">
        @php
        $path = asset('broker/images/svg-icons/user-icon.svg');
        if(Storage::disk('user')->exists(str_replace('user_image', '', auth()->user()->photo)))
        {
        $path = Storage::disk('user')->url(str_replace('user_image/', '', auth()->user()->photo));
        }
        @endphp
        <img class="rounded-circle" src="{{ $path }}" alt="User" />
        <em class="headerUserName">His, {{ auth()->user()->name }}</em>
    </a>
</div>



