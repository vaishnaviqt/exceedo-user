<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>{{ config('app.name', 'Exceedo') }} &#8211; @yield('title')</title>
<link rel="stylesheet" href="{{ asset('expert/css/magnific-popup.css') }}"><!-- //M-customScrollbar Css -->
<link rel="stylesheet" href="{{ asset('expert/css/jquery.mCustomScrollbar.min.css') }}"><!-- //Float Label Css -->
<link rel="stylesheet" type="text/css" href="{{ asset('expert/css/bootstrap-float-label.min.css') }}"><!-- //Time Picker Css -->
<link rel="stylesheet" type="text/css" href="{{ asset('user/css/bootstrap.min.css') }}">
<!-- <link rel="stylesheet" type="text/css" href="{{ asset('expert/css/expert-style.css?v=1.1') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('expert/css/expert-responsive.css?v=1.1') }}">   -->
<link rel="stylesheet" type="text/css" href="{{ asset('user/css/user-style.css?v=1.2') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('user/css/user-responsive.css?v=1.2') }}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('user/css/owl.carousel.css') }}"> 



<link rel="stylesheet" href="{{ asset('upload/css/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ asset('upload/css/jquery.fileupload-ui.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<script type="text/javascript" src="{{ asset('user/js/jquery.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('user/css/calendar-main.css') }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="base_url" content="{{ url('/') }}">
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ asset('user/js/gijgo.min.js') }}"></script>

