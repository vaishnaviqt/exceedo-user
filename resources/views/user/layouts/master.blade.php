<!DOCTYPE html>
<html lang="en">
    <head>
        @include('user.partials.head')
        <style>

            .loader{
                position: fixed;
                top:0;
                left:0;
                right: 0;
                bottom:0;
                display:flex;
                align-items:center;
                justify-content:center;
                z-index: 9999;
                background: #ffffffa6;
            }
            .loader img{
                width:80px;
            }
        </style>
    </head>
    <body>  
        <div class="loader d-none">
            <img src="{{ asset('images/Spin-1s-200px.gif') }}">
        </div>
        <div class="wrapper">
            <div class="sidebar">

                @include('user.partials.sidebar')

            </div><!--//sidebar-->


            <div class="main">
                <header class="header"> 

                    @include('user.partials.header')
                </header><!--//header-->


                <section class="pageContainer @yield('class')">
                    @yield('content')
                </section><!--//pageContainer-->

                <footer class="footer">
                    @include('user.partials.footer')
                </footer>    

            </div><!--//main-->

        </div><!--//wrapper--> 

        @yield('modal')
        @include('user.partials.javascripts')

        @section('custom-script')
        @show
        <script type="text/javascript">
            $(document).ready(function () {
                //Appointment
                $(".appointment-owlCarousel").owlCarousel({
                    items: 1,
                    dots: false,
                    nav: true,
                    margin: 20,
                    responsive: {
                        768: {
                            items: 2,
                            nav: false
                        },
                        1140: {
                            items: 3,
                            nav: false
                        }
                    }
                });
                $('#app_next').click(function () {
                    $(".appointment-owlCarousel").trigger('next.owl.carousel');
                });
                $('#app_prev').click(function () {
                    $(".appointment-owlCarousel").trigger('prev.owl.carousel');
                });

                //Recent Report
                $(".report-owlCarousel").owlCarousel({
                    items: 1,
                    dots: false,
                    nav: true,
                    margin: 20,
                    responsive: {
                        768: {
                            items: 2,
                            nav: false
                        }
                    }
                });
                $('#report_next').click(function () {
                    $(".report-owlCarousel").trigger('next.owl.carousel');
                });
                $('#report_prev').click(function () {
                    $(".report-owlCarousel").trigger('prev.owl.carousel');
                });

                // Inline popups
                $('#inline-popups').magnificPopup({
                    delegate: 'a',
                    removalDelay: 500,
                    callbacks: {
                        beforeOpen: function () {
                            this.st.mainClass = this.st.el.attr('data-effect');
                        }
                    },
                    midClick: true
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                var today, datepicker;
                today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                datepicker = $('#datePickerSearch').datepicker({
                    maxDate: today,
                    format: 'dd-mm-yyyy'
                });

                $('.paymentPaginate').on('click', function () {
                    $('#paginateHiddenFiled').val($(this).data('value'));
                    $('#paginateSubmitForm').submit();
                });
            });
        </script>


    </body>
</html>