@extends('user.layouts.master')
@section('title')
Land Listing
@endsection
@section('class')
@endsection
@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<h2>Inactive Land Listing</h2> 
@include('user.landlistingpartials.header')
<div class="tab-content"> 
    <div class="filterFormSection">
        <a href="javascript:void(0);" class="mobileFilterBtn">Search </a>                              
        <div class="filterForm">
            <form action="{{url('user/your-land-listing-inactive')}}" method="POST">
                @csrf
                <h2>
                    Search
                    <a href="javascript:void(0);" class="m_filterCloseBtn"></a>
                </h2>
                <div class="row">
                    <div class="col-lg-10">
                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <div class="form-group floating-field">
                                    <input type="text" class="form-control" placeholder="Land Name" 
                                           value="{{$name ?? ''}}" id="Name" name="name">
                                    <label for="Name" class="floating-label">Land Name</label>
                                </div>
                            </div><!--//col-lg-3-->

                            <div class="col-lg-3 col-md-6"> 
                                <div class="form-group floating-field">
                                    <select class="form-control" id="reportType" name="filter_business_type">
                                        <option value="">{{ __('Land Type') }}</option>
                                        <option value="Commercial" @if($businessType=='Commercial' ) {{ __('selected') }} @endif>{{ __('Commercial') }}</option>
                                        <option value="Industrial" @if($businessType=='Industrial' ) {{ __('selected') }} @endif>{{ __('Industrial') }}</option>
                                        <option value="Institutional" @if($businessType=='Institutional' ) {{ __('selected') }} @endif>{{ __('Institutional') }}</option>
                                        <option value="Recreational" @if($businessType=='Recreational' ) {{ __('selected') }} @endif>{{ __('Recreational') }}</option>
                                        <option value="Mixed_use" @if($businessType=='Mixed_use' ) {{ __('selected') }} @endif>{{ __('Mixed_use') }}</option>
                                        <option value="Agricultural" @if($businessType=='Agricultural' ) {{ __('selected') }} @endif>{{ __('Agricultural') }}</option>
                                        <option value="Others" @if($businessType=='Others' ) {{ __('selected') }} @endif>{{ __('Others') }}</option>
                                    </select>
                                    <label for="LandType" class="floating-label">Land Type</label>
                                </div>
                            </div><!--//col-lg-3-->

                            <div class="col-lg-3 col-md-6">
                                <div class="form-group floating-field">
                                    <input type="text" name="filter_address" class="form-control" placeholder="Land Area " 
                                           value="{{$address ?? ''}}" id="land-area">
                                    <label for="Location" class="floating-label">Land Area </label>
                                </div>
                            </div><!--//col-lg-3-->

                            <div class="col-lg-3 col-md-6">
                                <div class="form-group floating-field">
                                    <input class="form-control" id="datePickerSearch" type="text" name="created_at" 
                                           value="{{ !empty($startDate) ? Carbon\Carbon::parse($startDate)->format('d-m-Y') : '' }}" placeholder="Date"/>
                                    <label for="Date" class="floating-label">Date</label>
                                </div>
                            </div><!--//col-lg-3-->
                        </div><!--//row-->
                    </div><!--//col-lg-10-->

                    <div class="col-lg-2">
                        <button type="submit" class="btn btn-primary">Search </button>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-primary" data-toggle="tab" onclick="formReset()">Reset</button>
                    </div>
                </div><!--//row-->
            </form>
        </div><!--//filterForm-->
    </div><!--filterFormSection-->

    <div class="reportsListing">
        <div class="row row_20">
            @forelse($inactiveLands as $inactiveLand)
            <div class="col-md-6">
                <article class="reportArticle">
                    <div class="repArtImg">
                        <img src="{{ asset($inactiveLand->image) }}" />
                    </div>
                    <div class="repArtContent landArtCont">

                        <div class="row">
                            <div class="col-7">
                                <h2>{{ ucwords($inactiveLand->land_name) }}<em class="featureIcon"></em></h2> 
                                <p>{{ $inactiveLand->address }}</p>
                            </div>
                            <div class="col-5">
                                <div class="activeLand">
                                    <span class="badge badge-danger float-right">{{ __('Inactive') }}</span>
                                </div>
                            </div>
                        </div><!--//row-->

                        <div class="landCost">
                            <div class="row align-items-center">
                                <div class="col-lg-4">
                                    <div class="landArtPrice">₹ {{$inactiveLand->max_price}} {{$inactiveLand->price_unit}}</div>
                                </div>
                                <div class="col-lg-8 text-lg-right">
                                    <div class="expDetail">

                                    </div>
                                </div>
                            </div><!--//row-->
                        </div><!--//landCost-->

                        <div class="featFaciRow">
                            @if($inactiveLand->wifi  == 1 || $inactiveLand->parking == 1)
                            <div class="featFacilities">
                                <span class="featFaTitle">Facilities :</span>
                                @if($inactiveLand->parking == 1)
                                <span class="faci-icon parking-icon"></span>
                                @endif
                                @if($inactiveLand->wifi == 1)
                                <span class="faci-icon wifi-icon"></span>
                                @endif
                            </div><!--//featFacilities-->
                            @endif
                            <div class="landArtviews">
                                {{$inactiveLand->created_at->diffForHumans()}} &bull; {{$inactiveLand->views ?? 0}} views
                            </div>
                        </div><!--//featFacilities-->
                    </div><!--//repArtContent-->
                </article><!--//reportArticle-->
            </div><!--//col-md-6--> 
            @empty                     
            <div class="col-12">
                <div class="no-content-msg">   
                    No Property Available 
                </div>
            </div>
            @endforelse
        </div><!--//row-->
    </div><!--//reportsListing-->
    {{ $inactiveLands->links() }}
</div><!--//tab-content-->

@endsection   


@section('modal')
<!-- Verify land model -->
<div class="modal fade w-400" id="getVerified" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="popupForm">
                    <form>
                        <div class="popupHeading">
                            <h2>Get Verified your Land</h2>
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="text" class="form-control" placeholder="Name" id="Name">
                            <label for="Name" class="floating-label">Name</label>
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="text" class="form-control" placeholder="Email id" id="email">
                            <label for="email" class="floating-label">Email id</label>
                        </div> 

                        <div class="form-group floating-field singleBorder">
                            <input type="text" class="form-control" placeholder="Mobile Number" id="number">
                            <label for="number" class="floating-label">Mobile Number</label>
                        </div>

                        <div class="form-group floating-field singleBorder mb-4">
                            <input type="text" class="form-control" placeholder="Site Address" id="site">
                            <label for="site" class="floating-label">Site Address</label>
                        </div> 

                        <div class="formBtn">
                            <button type="button" class="btn btn-primary">Submit</button>
                        </div> 
                    </form>
                </div><!--//popupForm--> 
            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->


<!-- Interested people model -->
<div class="modal fade w-380" id="interestedPeople" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="interPeople_modal">
                    <h2>Interested People for your Land</h2>

                    <div class="intPeopleList customScrollbar">
                        <article>
                            <h3>Abhit Bhatia</h3>
                            <p>+9560465467 <span>|</span> abhitbhatia1@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Mayank Sharma</h3>
                            <p>+9560465467 <span>|</span> mayank.sharma@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Yachna Verma</h3>
                            <p>+9560465467 <span>|</span> yachnaverma@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Abhit Bhatia</h3>
                            <p>+9560465467 <span>|</span> abhitbhatia1@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Abhit Bhatia</h3>
                            <p>+9560465467 <span>|</span> abhitbhatia1@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Mayank Sharma</h3>
                            <p>+9560465467 <span>|</span> mayank.sharma@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Yachna Verma</h3>
                            <p>+9560465467 <span>|</span> yachnaverma@gmail.com    </p>
                        </article>

                        <article>
                            <h3>Abhit Bhatia</h3>
                            <p>+9560465467 <span>|</span> abhitbhatia1@gmail.com    </p>
                        </article> 
                    </div><!--//intPeopleList-->

                </div><!--//interPeople_modal-->

            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->
<!-- Inactive model -->
<div class="modal fade w-380" id="inactiveLand" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="inactive_modal">
                    <h2>Are you Sure you want to inactive your Land Listing ?</h2>

                    <div class="optionalBtns">
                        <span class="customRadioBtn">
                            <input type="radio" name="ans" id="yes">
                            <label for="yes">Yes, Do it</label>
                        </span>

                        <span class="customRadioBtn">
                            <input type="radio" name="ans" id="no">
                            <label for="no">No, Don’t</label>
                        </span>
                    </div><!--//optionalBtns-->

                    <textarea placeholder="Reason to inactive your listing"></textarea>

                    <div class="formBtn">
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </div><!--//interPeople_modal-->

            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->

@endsection   
