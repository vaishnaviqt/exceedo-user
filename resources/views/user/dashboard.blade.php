@extends('user.layouts.master')
@section('title')
Dashboard
@endsection
@section('class')
@endsection
@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="pageTitle">
    <h1>Dashboard</h1>
</div><!--//pageTitle-->
<div class="countCards">
    <div class="row">                        
        <div class="col-lg-3 col-md-6">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('user/images/svg-icons/appointments-icon.svg') }}" alt="Appointments">                                    
                </div>
                <div class="countRepDetail">
                    <p><strong>{{ $totalappointment }}</strong></p>
                    <p>Appointments</p>
                </div>
            </article><!--//countCardArt-->
        </div><!--//col-lg-3-->                         

        <div class="col-lg-3 col-md-6">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('user/images/svg-icons/allReport-icon.svg') }}" alt="Your Land Report">                                    
                </div>
                <div class="countRepDetail">
                    <p><strong>{{ $totalReport }}</strong></p>
                    <p>Your Land Report</p>
                </div>
            </article><!--//countCardArt-->
        </div><!--//col-lg-3-->
        <div class="col-lg-6 col-md-12">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('user/images/svg-icons/all-land-icon.svg') }}" alt="Your Active Land Listing">
                </div>
                <div class="countRepDetail countRepDetail-170 pr-3">
                    <div>
                        <strong>
                            {{ countLand(1, auth()->user())}}
                        </strong>
                        <a href="javascript:void(0);" class="dotmark"  id="notificationDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&bull;</a>

                        <div class="dropdown-menu notificationDropdown" aria-labelledby="notificationDropdown">
                            <div class="notiData">
                                Your Land Listing is about to expire<br>
                                <a href="javascript:void(0);">Renew</a>
                            </div>
                            <div class="notiData">
                                Your Land Listing is about to expire<br>
                                <a href="javascript:void(0);">Renew</a>
                            </div>
                            <div class="notiData">
                                Your Gurgaon Land Listing is now active
                            </div>
                        </div> 
                    </div> 
                    <p>Your Active Land Listing</p>
                </div>
                <div class="countRepDetail dashCounSepBox">
                    <p><strong>{{ countLand('inactive', auth()->user())}}</strong></p>
                    <p>Your Inactive Land Listing</p>
                </div>
            </article><!--//countCardArt-->
        </div><!--//col-lg-3-->                      
    </div><!--//row-->
</div><!--//countCards-->

<!--<section class="graphSection">                    
    <div class="mb-4">
        <div class="pageTitle d-flex align-items-center">
            <h2>Appointments</h2>
            
            <div class="sortDropdown">
                <label>Show by</label>
                <div class="dropdown custom-dropdown">
                    <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" 
                       class="dropdown-toggle">
                        <span class="selected">{{ $filterDate }}</span><span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"  class="expertPaginate" data-value="week">Week</a></li>
                        <li><a href="#"  class="expertPaginate" data-value="month">Month</a></li>
                        <li><a href="#"  class="expertPaginate" data-value="year">Year</a></li>
                    </ul>
                </div> //dropdown 
            </div>//sortDropdown
        </div>//pageTitle
    
        <div class="graphBox" style="display: none">
                    <canvas id="myChart" width="400" height="300"></canvas>
                </div>
    </div>//div-repeat 

    <form method="post" id="paginateSubmitForm">
        @csrf
        <input type="hidden" name="day" id="paginateHiddenFiled">
    </form>

    <div class="div-repeat">
        <div class="pageTitle d-flex align-items-center">
            <h2>Land Listing</h2>

        filters
            
        </div>//pageTitle

        <div class="graphBox" style="display: none">
                    <canvas id="landdetailChart" width="400" height="300"></canvas>
                </div>
    </div>//div-repeat 
</section> -->

@if(count($doneAppointments))
<section class="dasboard_appArticles">                              
    <div class="pageTitle d-flex align-items-center flex-wrap mb-0">
        <h2>Your Recent Appointments</h2>
        <div class="dashCarouselCtrl">
            <a href="javascript:void(0);" class="d_prev" id="app_prev"></a>
            <a href="javascript:void(0);" class="d_next" id="app_next"></a>
        </div>
        <div class="genReportLink">
            <a href="{{url('user/appointments')}}">Book Appointment</a>
        </div>
    </div><!--//pageTitle-->

    <div class="appointment-owlCarousel owl-carousel">
        @foreach($doneAppointments as $appointment)
        @if(!empty($appointment->expert))
        <div>
            <article class="appointmentArticle">
                <div class="app_userRow">
                    <!--<a href="javacript:void(0);" class="moreOptLink"></a>-->
                    <div class="app_userPic">
                        @php
                        $path = asset('expert/images/profile-pic.png');
                        if(Storage::disk('expert')->exists(str_replace('expert_image', '', $appointment->expert->photo)))
                        {
                        $path = Storage::disk('expert')->url(str_replace('expert_image/', '', $appointment->expert->photo));
                        }
                        @endphp
                        <img src="{{ $path }}" />
                    </div>
                    <div class="app_userName">
                        <h3>{{ ucwords($appointment->expert->name)}}</h3>
                        <h4>Service: {{isset($appointment->service->service_name) ? $appointment->service->service_name : '' }}</h4>
                    </div>
                </div><!--//app_userRow-->

                <div class="app_discussion">
                    <h3>Discussion</h3>
                    <p>{{ ucwords($appointment->remarks) }}</p>
                </div><!--//app_discussion-->

                <div class="app_time">{{$appointment->date}} - {{date('h:i A', strtotime($appointment->time))}}</div>
            </article><!--//appointmentArticle-->
        </div>
        @endif
        @endforeach
    </div><!--//appointment-owlCarousel--> 
</section><!--//dasboard_appArticles--> 
@endif

@if(count($userReports))
<section class="dasboard_appArticles mb-0">                              
    <div class="pageTitle d-flex align-items-center flex-wrap mb-0">
        <h2>Your Recent Reports</h2>
        <div class="dashCarouselCtrl">
            <a href="javascript:void(0);" class="d_prev" id="report_prev"></a>
            <a href="javascript:void(0);" class="d_next" id="report_next"></a>
        </div>
        <div class="genReportLink">
            <a href="{{url('user/your-land-report')}}">Generate Report</a>
        </div>
    </div><!--//pageTitle-->

    <div class="report-owlCarousel owl-carousel">
        @foreach($userReports as $report)
        @if(!empty($appointment->user))
        <div>
            <article class="reportArticle">
                <div class="repArtImg">
                    @if(Storage::disk('landReport')->exists(str_replace('land_report_image/','',$report->image)))
                    <img src="{{ Storage::disk('landReport')->url(str_replace('land_report_image/','',$report->image)) }}" alt="{{ $report->size_of_land }} Acres {{ $report->land_name }}" />
                    @endif
                </div>
                <div class="repArtContent">
                    <h2>{{ $report->size_of_land }} Acres {{ ucwords($report->land_name) }}</h2>
                    <h3>{{ isset($report->user->mobile) ? $report->user->mobile : '' }} | {{ isset($report->user->email) ? $report->user->email : '' }}</h3>
                    <p class="repOneLine repTitleIcon">Land Title Search</p>
                    <p class="repOneLine repLocIcon">{{ $report->address }}, {{ $report->city }} {{ isset($report->state->name) ? $report->state->name : '' }}</p>

                    <div class="repArtAdditional">
                        @if(empty($report->expert_id) && $report->status == 0)
                        <p class="ArtAuthor">Pending by : <span>Admin</span></p>
                        @elseif(!empty($report->expert_id) && $report->status == 0)
                        <p class="ArtAuthor">Pending by : <span>{{ isset($report->expert->name)  ? $report->expert->name : '' }}</span></p>
                        @elseif(!empty($report->expert_id) && $report->status == 2)
                        <p class="ArtAuthor">Inprogress by : <span>{{ isset($report->expert->name)  ? $report->expert->name : '' }}</span></p>
                        @elseif($report->status == 1)
                        <p class="ArtAuthor">Delivered by  : <span>{{ isset($report->expert->name)  ? $report->expert->name : '' }}</span></p>
                        @endif
                        <p class="timeDetail">{{  $report->created_at->diffForHumans() }}</p>
                    </div>
                </div><!--//repArtContent-->
            </article>
        </div> 
        @endif
        @endforeach
    </div><!--//appointment-owlCarousel--> 
</section><!--//dasboard_appArticles-->
@endif

@endsection   
@section('custom-script')
<script>
    $('.expertPaginate').on('click', function (e) {
    e.preventDefault();
    var value = $(this).data('value');
    $('#paginateHiddenFiled').val(value);
    $('#paginateSubmitForm').submit();
    return false;
    });
    window.onload = function () {
    // Admin appointment chart show
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
    type: 'line',
            data: {
            labels: {!! $adminEarningDate !!},
                    datasets: [{
                    label: "{!! $doneAppointmentTitle !!}",
                            data: {!! $adminEarningAmount !!},
                            backgroundColor: 'transparent',
                            borderColor: "#4bc0c0",
                            fill: true,
                            borderWidth: 2
                    },
                    {
                    label: "{!! $pendingAppointmentTitle !!}",
                            data: {!! $pendingAppointment !!},
                            backgroundColor: 'transparent',
                            borderColor: "#ff6384",
                            fill: true,
                            borderWidth: 2
                    },
                    ]
            },
            options: {
            scales: {
            yAxes: [{
            ticks: {
            beginAtZero:true
            }
            }]
            }
            }
    });
    // Admin land chart show
    var ctx = document.getElementById('landdetailChart').getContext('2d');
    var myChart = new Chart(ctx, {
    type: 'line',
            data: {
            labels: {!! $landVerifiedDate !!},
                    datasets: [{
                    label: "Active lands",
                            data: {!! $landVerifiedDeatil !!},
                            backgroundColor: 'transparent',
                            borderColor: "#4bc0c0",
                            fill: true,
                            borderWidth: 2
                    },
                    {
                    label: "Inactive lands",
                            data: {!! $landUnverifiedDeatil !!},
                            backgroundColor: 'transparent',
                            borderColor: "#ff6384",
                            fill: true,
                            borderWidth: 2
                    },
                    ]
            },
            options: {
            scales: {
            yAxes: [{
            ticks: {
            beginAtZero:true
            }
            }]
            }
            }
    });
    };

</script>

@endsection
