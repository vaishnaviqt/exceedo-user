
<div class="col-12">
    <h2>Pending Appointments 
        <!--<span>(06 Oct 2020)</span>-->
    </h2>
</div><!--//.col-12 -->  

<div class="col-12">
    <div class="searchForm">
        <form method="post" action="{{ url('admin/appointments') }}">
            @csrf
            <ul class="d-flex flex-wrap">
                <li>
                    <div class="form-group">
                        <input class="form-control" type="date" name="date" id="pendingDate"value="{{ !empty($pendingDate) ? Carbon\Carbon::parse($pendingDate)->format('Y-m-d') : '' }}" placeholder=""/>
                    </div><!-- //form-group --> 
                </li>
                            <li>
                                                <div class="form-group">
                        <select class="form-control" id="upcommingServiceId" name="value_added_service_id">
                            <option value="">Select Value Added Services</option>
                            @if(count($services))
                            @foreach($services as $service)
                            @php
                            $selected = '';
                            if($serviceId == $service->id){
                            $selected = 'selected';
                            }
                            @endphp
                            <option value="{{ $service->id }}" {{ $selected }}>{{ $service->service_name }}</option>
                            @endforeach
                            @endif

                                                    </select>
                    </div>
                </li>
                <li>
                    <!-- <a href="javascript:void(0);" class="btn btn-primary">Se</a> -->
                                <div class="row row_10">
                                <div class="col-6">
                    <input type="button" value="pendingSeacrh" name="submit" class="btn btn-primary viewPendingAppointSearchForm">
                                </div>
                                <div class="col-6">
                            <input type="button" id="resetUpcommingForm"  class="btn btn-primary" value="Reset">
                    </div>
                    </div>
                </li>
            </ul><!-- //ul -->
        </form><!-- //form -->
    </div><!--//searchForm-->
</div><!-- //.col-12-->

<div class="col-12">
    <div class="appointments pendingAppoint">
        <form>
            @forelse($pendingAppoints as $pending)

            <ul class="appointments-list">
                <li class="order-md-1 order-1">
                    <h3>{{ isset($pending->service->service_name) ? $pending->service->service_name : '' }}</h3>
                </li>
                <li class="order-md-2 order-3">
                    {{ !empty($pending->date) ? Carbon\Carbon::parse($pending->date . ' ' . $pending->time)->format('d M, Y h:i A') : '' }}
                    <br>
                    Service: {{ isset($pending->service->service_name) ? $pending->service->service_name : '' }}
                    <br>
                    Amount:  Rs {{ $pending->price }}
                    <br>
                    @if($pending->rescheduled_by == 1 && $pending->status == 0)
                    @if(isset($pending->expert->name))
                    {{ isset($pending->expert->name) ? $pending->expert->name : ''  }} expert rescheduled this appointment.
                    @endif
                    @elseif($pending->rescheduled_by == 0 && $pending->status == 0)
                    @if(isset($pending->user->name))
                    {{ $pending->user->name  }} user rescheduled this appointment.
                    @endif
                    @endif

                    @if($pending->canceled_by == 2 && $pending->status == 3)
                    You cancel this appointment.
                    @elseif($pending->canceled_by == 0 && $pending->status == 3)
                    @if(isset($pending->user->name))
                    {{ $pending->user->name  }} user cancel this appointment.
                    @endif
                    @endif
                </li>
                <li class="order-md-4 order-4 w-100">
                    Expert: {{ isset($pending->expert->name) ? $pending->expert->name : 'Not assign yet' }}
                </li>
            </ul><!--//appointments-list-->
            @empty
            <p class="no-data mb-0">No Pending Appointment Available</p>
            @endforelse
        </form><!--//form-->
    </div><!--//appointments -->
</div><!--//col-12-->