<div class="col-12">
    <h2>Complete Appointments 
        <!--<span>(06 Oct 2020)</span>-->
    </h2>

</div><!--//.col-12 -->  
<div class="col-12">
    <div class="d-none viewAppointmentError ml-5 mt-2 mb-3"></div>
    <div class="searchForm">
        <form method="post" action="{{ url('expert/appointments') }}" id="viewDoneAppointSearchForm">
            @csrf
            <ul class="d-flex flex-wrap">
                <li>
                    <div class="form-group">
                        <input class="form-control" type="date" name="done_date" id="doneDate" value="{{ !empty($doneDate) ? Carbon\Carbon::parse($doneDate)->format('Y-m-d') : '' }}" placeholder=""/>
                    </div><!-- //form-group --> 
                </li>

                <li>
                                    <div class="form-group">
                        <select class="form-control" id="doneserviceId" name="value_added_service_id">
                            <option value="">Select Value Added Services</option>
                            @if(count($services))
                            @foreach($services as $service)
                            @php
                            $selected = '';
                            if($serviceId == $service->id){
                            $selected = 'selected';
                            }
                            @endphp
                            <option value="{{ $service->id }}" {{ $selected }}>{{ $service->service_name }}</option>
                            @endforeach
                            @endif

                                        </select>
                    </div>
                </li>

                    <!-- <a href="javascript:void(0);" class="btn btn-primary">Se</a> -->
                <li>
                    <div class="row">
                    <div class="row row_10">
                    <div class="col-6">
                    <input type="button" value="doneSearch" name="submit" class="btn btn-primary viewDoneAppointSearchForm">
                   </div>
                   <div class="col-6">
                            <input type="button" id="resetCompleteForm"  class="btn btn-primary" value="Reset">
                           
                    </div>
                    
                    </div>
                </li>

            </ul><!-- //ul -->
        </form><!-- //form -->
    </div><!--//searchForm-->
</div><!-- //.col-12-->
<div class="col-12">
    <div class="appointments">
        <form>
            @forelse($doneAppoints as $appoint)
            <ul class="appointments-list">
                <li class="order-md-1 order-1">
                    <h3>{{  isset($appoint->service->service_name) ? $appoint->service->service_name : '' }}</h3>
                    <br>
                    Expert: {{ isset($appoint->expert->name) ? $appoint->expert->name : 'Not assign yet' }}
                </li>
                <li class="order-md-2 order-3">
                    {{ !empty($appoint->date) ? Carbon\Carbon::parse($appoint->date .' '.$appoint->time)->format('d M, Y h:i A') : ''}}
                    <br>
                    Service: {{ isset($appoint->service->service_name) ? $appoint->service->service_name : '' }}
                    <br>
                    Amount:  Rs {{ $appoint->price }}
                </li>
                
                <li class="order-md-4 order-4">
                    <a href="javascript:void(0);" style="float:right;" data-toggle="modal" 
                       data-target="#feedbackModel" data-id="{{$appoint->id}}" class="get-appointment modelopen">Feedback</a>
                </li>
            </ul><!--//appointments-list-->
            @empty
            <p class="no-data mb-0">No Appointment Available</p>
            @endforelse

        </form><!--//form-->
    </div><!--//appointments -->
</div><!--//col-12-->

<!-- Feedback model -->
<div class="modal fade w-330 modelopen" id="feedbackModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>
                <form id="review-rating-form">     
                    <div class="feedbackModelBody">
                        <h2>Appointment Feedback</h2>
                        <div class="col-sm-12">
                            <div class="stars">
                                <input type="hidden" id="starRating" name="rating" value="">
                                <input class="star star-5 starRatingValue" value="5" id="star-5" type="radio" name="rating"/>
                                <label class="star star-5" for="star-5"></label>
                                <input class="star star-4 starRatingValue" value="4" id="star-4" type="radio" name="rating"/>
                                <label class="star star-4" for="star-4"></label>
                                <input class="star star-3 starRatingValue" value="3" id="star-3" type="radio" name="rating"/>
                                <label class="star star-3" for="star-3"></label>
                                <input class="star star-2 starRatingValue" value="2" id="star-2" type="radio" name="rating"/>
                                <label class="star star-2" for="star-2"></label>
                                <input class="star star-1 starRatingValue" value="1" id="star-1" type="radio" name="rating"/>
                                <label class="star star-1" for="star-1"></label>
                            </div>               
                        </div>
                        <textarea id="feedback" name="feedback" placeholder="Reason to inactive your listing"></textarea>
                    </div>
                    <input type="hidden" name="appointment_id" id="appointment_id"> 
                    <div class="feedbackModelFooter">
                        <div class="formBtn">
                            <button type="submit" id="service-feedback" class="btn btn-primary review-btn-submit">Submit</button>
                        </div>
                    </div><!--//feedbackModelFooter-->
                </form>
            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal--> 