
<!-- view-all-appointments-popup Start-->
<div id="view-all-appointments-popup" class="mfp-with-anim mfp-hide viewAll-appointment">
    <div class="popup-body">
        <button title="Close (Esc)" type="button" class="mfp-close"></button>
        <div class="row" id="allAppointmentCard">
        </div><!--/row-->

    </div><!-- //popup-body --> 
</div><!-- //view-all-appointments-popup End -->

<div class="modal fade urban-planning-popup approval-appointment" id="approval-appointment-popup" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close" data-dismiss="modal"></button>
            <div class="modal-body suggestAnotherSchedule">

            </div>
        </div>
    </div>
</div>

<div class="modal fade w-400" id="addAppointment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <form action="{{ url('user/add-appointment') }}" method="post" id="addAppointForm">
                    @csrf
                    <div class="popupForm">
                        <div class="popupHeading">
                            <h2>Book your Appointment</h2>
                        </div>
                        <div class="form-group mb-3">
                            <input type="hidden" name="price" value="" id="serviceInputPrice">
                            <input type="hidden" name="razorpay_payment_id" value="" id="razorpay_payment_id">
                            <input type="hidden" name="razorpay_order_id" value="" id="razorpay_order_id">
                            <input type="hidden" name="razorpay_signature" value="" id="razorpay_signature">
                            <input type="hidden" name="generated_signature" value="" id="generated_signature">
                            <select class="form-control" id="valueAddedServices" value="" name="value_added_service">
                                <option value="0">Value Added Services</option>
                                @foreach($services as $service)
                                <option value="{{ $service->id }}">{{ $service->service_name }}</option>
                                @endforeach
                            </select> 
                        </div>

                        <div class="form-group mb-3">
                            <input type="text" class="form-control form-calendar2 appDate" placeholder="Appointment Date" name="date" value="" id="appDate"> 
                        </div>

                        <div class="form-group mb-3">
                            <input type="text" class="form-control form-time" placeholder="Appointment Time" name="time" id="appTime"> 
                        </div> 

                        <div class="form-group mb-3">
                            <input type="text" class="form-control" placeholder="Any Further Remarks" name="remarks" id="remarks"> 
                        </div>

                        <div class="form-group mb-3">
                            <select class="form-control" id="payment_mode" value="" name="payment_mode">
                                <option value="">Payment Mode</option>
                                <option value="Cash">Cash</option>
                                <option value="Cheque">Cheque</option>
                                <option value="Online">Online</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>

                        <div class="termsCheck">
                            <div class="customCheckbox">
                                <input type="checkbox" name="terms" id="terms" name="terms" value="1">
                                <label for="terms">&nbsp;</label>
                            </div>
                            <span>I agree to the <a href="{{ route('privacypolicy') }}">Privacy Policy</a>, 
                                <a href="{{ route('termsconditions') }}">Terms of services</a> and 
                                <a href="{{ route('cookiepolicy') }}">Cookie Policy</a> of this Website</span>
                        </div>
                    </div><!--//popupForm-->


                    <div class="addAppBottomBox">
                        <div class="row align-items-center">
                            <div class="col-sm-6">
                                <div class="reportTotalVal" id="servicePrice">
                                    <!--<span>You need to Pay</span> <span id="servicePrice"></span>-->
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">                                 
                                <button class="btn btn-primary" type="button" id="addAppointButton">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->

<div class="modal fade urban-planning-popup approval-appointment" id="approval-appointment-popup" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close" data-dismiss="modal"></button>
            <div class="modal-body suggestAnotherSchedule">

            </div>
        </div>
    </div>
</div>


<div class="modal fade suggestAnotherTime" id="suggest-another-time-popup" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content popup-body">
            <button title="Close (Esc)" type="button" class="mfp-close" data-dismiss="modal"></button>
            <div class="modal-body p-0"> 
                <h2>Suggest Another Time</h2> 

                <form>
                    <!-- <div class="form-filds">  -->
                    <div class="form-group mb-3"> 
                        <input type="hidden" name="id" value="" class="rescheduleId">
                        <input class="form-control rescheduleDate" id="datepicker" type="text" name="date" placeholder="Appointment Date "/>  
                    </div> 

                    <div class="form-group mb-3"> 
                        <input class="form-control rescheduleTime" id="timepicker" type="text" placeholder="Appointment Time"/> 
                    </div> 

                    <div class="form-group mb-3"> 
                        <input class="form-control rescheduleRemarks" id="remarks" type="text" placeholder="Any Further Remarks "/> 
                    </div> 

                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input rescheduleTerms" id="privacyPolicy" value="1" name="privacyPolicy">
                        <label class="custom-control-label" for="privacyPolicy">I agree to the <span> Privacy Policy, Terms of services</span> and <span> Cookie Policy</span> of this Website</label>
                    </div> 

                    <button class="btn btn-primary js-btn-next" id="rescheduleSubmitButton" type="button" title="Next">Submit</button>

                    <!--</div>//form-filds-->
                </form><!--//form-->                   
            </div>
        </div>
    </div>
</div>



<!-- Reschedule Appointment model -->
<div class="modal fade w-400" id="rescheduleAppointment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <form>
                    <div class="popupForm">
                        <div class="popupHeading">
                            <h2>Reschedule your Appointment</h2>
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="date" class="form-control form-calendar2" placeholder="Appointment Date" id="appDate2">
                            <label for="appDate2" class="floating-label">Appointment Date</label>
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="text" class="form-control form-time" placeholder="Appointment Time" id="appTime2">
                            <label for="appTime2" class="floating-label">Appointment Time</label>
                        </div> 

                        <div class="form-group floating-field singleBorder mb-3">
                            <input type="text" class="form-control" placeholder="Any Further Remarks" id="remarks2">
                            <label for="remarks2" class="floating-label">Any Further Remarks</label>
                        </div>

                        <div class="termsCheck mb-4">
                            <div class="customCheckbox">
                                <input type="checkbox" name="terms" id="terms2">
                                <label for="terms2">&nbsp;</label>
                            </div>
                            <span>I agree to the <a href="javascript:void(0);">Privacy Policy</a>, <a href="javascript:void(0);">Terms of services</a> and <a href="javascript:void(0);">Cookie Policy</a> of this Website</span>
                        </div>

                        <div class="formBtn">
                            <button type="button" class="btn btn-primary">Submit</button>
                        </div>

                    </div><!--//popupForm-->
                </form>

            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->


<!-- Rebook Appointment model -->
<div class="modal fade w-400" id="rebookAppointment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <form>
                    <div class="popupForm">
                        <div class="popupHeading">
                            <h2>Rebook Appointment with Same Expert</h2>
                        </div>

                        <div class="rebookExpert">
                            <div class="row">
                                <div class="col-6"><span>Expert Name</span> </div>
                                <div class="col-6 text-right">Ayushi Sharma</div>
                            </div>
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="date" class="form-control form-calendar2" placeholder="Appointment Date" id="appDate3">
                            <label for="appDate3" class="floating-label">Appointment Date</label>
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="text" class="form-control form-time" placeholder="Appointment Time" id="appTime3">
                            <label for="appTime3" class="floating-label">Appointment Time</label>
                        </div> 

                        <div class="form-group floating-field singleBorder mb-3">
                            <input type="text" class="form-control" placeholder="Any Further Remarks" id="remarks3">
                            <label for="remarks3" class="floating-label">Any Further Remarks</label>
                        </div>

                        <div class="termsCheck mb-4">
                            <div class="customCheckbox">
                                <input type="checkbox" name="terms" id="terms3">
                                <label for="terms3">&nbsp;</label>
                            </div>
                            <span>I agree to the <a href="javascript:void(0);">Privacy Policy</a>, <a href="javascript:void(0);">Terms of services</a> and <a href="javascript:void(0);">Cookie Policy</a> of this Website</span>
                        </div>

                        <div class="formBtn">
                            <button type="button" class="btn btn-primary">Submit</button>
                        </div>

                    </div><!--//popupForm-->
                </form>

            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->


<!-- Add appointment (2) model -->
<div class="modal fade w-630" id="addAppointmentModel_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="addAppointmentOptions">
                    <h2>Book your Appointment</h2>

                    <div class="d-flex flex-sm-wrap flex-wrap mb-3">
                        <div class="custom-meet-radio">
                            <input type="radio" id="id01" name="serviceExpert">
                            <label class="custom-control-label" for="id01">Design &amp; Development</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id02" name="serviceExpert">
                            <label class="custom-control-label" for="id02">Legal Advisory</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id03" name="serviceExpert">
                            <label class="custom-control-label" for="id03">Urban Planning</label>
                        </div> 

                        <div class="custom-meet-radio">
                            <input type="radio" id="id04" name="serviceExpert">
                            <label class="custom-control-label" for="id04">Transaction Advisory</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id05" name="serviceExpert">
                            <label class="custom-control-label" for="id05">Design &amp; Development</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id06" name="serviceExpert">
                            <label class="custom-control-label" for="id06">Legal Advisory</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id07" name="serviceExpert">
                            <label class="custom-control-label" for="id07">Urban Planning</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id08" name="serviceExpert">
                            <label class="custom-control-label" for="id08">Transaction Advisory</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id09" name="serviceExpert">
                            <label class="custom-control-label" for="id09">Design &amp; Development</label>
                        </div>

                        <div class="custom-meet-radio">
                            <input type="radio" id="id10" name="serviceExpert">
                            <label class="custom-control-label" for="id10">Legal Advisory</label>
                        </div> 

                        <div class="custom-meet-radio">
                            <input type="radio" id="id11" name="serviceExpert">
                            <label class="custom-control-label" for="id11">Urban Planning</label>
                        </div> 

                        <div class="custom-meet-radio">
                            <input type="radio" id="id12" name="serviceExpert">
                            <label class="custom-control-label" for="id12">Transaction Advisory</label>
                        </div> 
                    </div><!--//d-flex-->

                    <div class="appNextBtn">
                        <a href="javascript:void(0);" class="btn btn-primary">Next</a>
                    </div>


                </div><!--//addAppointmentOptions-->

            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->


<!-- Thankyou model -->
<div class="modal fade w-300" id="thanksModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="thankyouBox">
                    <div class="thankIcon">
                        <img src="images/svg-icons/thank-icon.svg" alt="Thankyou for your feedback" />
                    </div>
                    <h2>Thankyou for your Feedback</h2>
                    <p>Lorem Ipsum is simply dummy text of the printingand typesetting industry.</p>
                    <div class="thankBtn">
                        <a href="javascript:void(0);" class="btn btn-primary">Ok</a>
                    </div>
                </div> 

            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->


<!-- Cancel Appointment model -->
<div class="modal fade w-500" id="cancelAppointment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="cancelAppBox"> 
                    <h2>Are you Sure you want to cancel your appointment. </h2>
                    <p>Your entire booking amount shall be forfeited in that case. No refund on cancelation by the user.</p>
                    <div class="text-center">
                        <a href="javascript:void(0);" class="btn btn-default-bordered">No, Don’t</a>
                        <a href="javascript:void(0);" class="btn btn-primary">Yes, Do it</a>
                    </div>
                </div> 

            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->

<!-- Join meeting model -->
<div class="modal fade w-500" id="joinMeeting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="joinMeetingBox"> 
                    <div class="joinMeetHeader">
                        <h2>Urban Planning Discussion</h2>
                        <span class="dateTime">Monday, 06 October, 08:00 am to 08:30 am </span> 

                        <div class="dropdown">
                            <button type="button" data-toggle="dropdown"></button>
                            <div class="dropdown-menu dropdown-menu-right moreLink-menu">
                                <a href="JavaScript:void(0);">Link</a> 
                                <a href="JavaScript:void(0);">Link</a> 
                            </div>
                        </div> 
                    </div>

                    <div class="joinMeetBody">
                        <div class="meetingLink">
                            <span class="joinMeeting">Join the Meeting </span>
                            <a href="javascript:void(0);">https://us02web.zoom.us/j/303164402?pwd=WXg4M3lQUTh6NDZlS3lPQUlkMGNXZz09</a>
                        </div>

                        <div class="meetingDetails">
                            <label>Meeting ID: <span>303 164 402</span></label>
                            <label>Password: <span>095215</span></label>
                        </div>
                    </div><!--//joinMeetBody--> 
                </div><!--//joinMeetingBox-->

                <div class="joinMeetingFooter">
                    Please join the meeting 10 minutes before
                </div>

            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->


<!-- waitingMeeting model -->
<div class="modal fade w-500" id="waitingMeeting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="joinMeetingBox approval-appointment"> 
                    <div class="joinMeetHeader">
                        <h2>Urban Planning Discussion</h2>
                        <span class="dateTime">Monday, 06 October, 08:00 am to 08:30 am </span> 

                        <div class="dropdown">
                            <button type="button" data-toggle="dropdown"></button>
                            <div class="dropdown-menu dropdown-menu-right moreLink-menu">
                                <a href="JavaScript:void(0);">Link</a> 
                                <a href="JavaScript:void(0);">Link</a> 
                            </div>
                        </div> 
                    </div>

                    <div class="joinMeetBody">
                        <div class="waitingData">
                            Waiting for the Expert to Confirm this slot
                        </div> 
                    </div><!--//joinMeetBody--> 
                </div><!--//joinMeetingBox--> 
            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->


<!-- Feedback model -->
<div class="modal fade w-330" id="feedbackModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="feedbackModelBody">
                    <h2>Appointment Feedback</h2>
                    <div class="feedbackRating">
                        <div class="Stars" style="--rating: 3;" aria-label="Rating of this product is 3.3 out of 5."></div>
                    </div>
                    <textarea placeholder="Reason to inactive your listing"></textarea>
                </div>

                <div class="feedbackModelFooter">
                    <h4>Do you want to list anything from these</h4>
                    <div class="optionalBtns">
                        <div class="row">
                            <div class="col-6">
                                <span class="customRadioBtn">
                                    <input type="radio" name="ans" id="a1" checked>
                                    <label for="a1">Land Lisiting</label>
                                </span>
                            </div>

                            <div class="col-6">
                                <span class="customRadioBtn">
                                    <input type="radio" name="ans" id="a2">
                                    <label for="a2">Feasibility Analysis</label>
                                </span>
                            </div>
                            <div class="col-12">
                                <span class="customRadioBtn">
                                    <input type="radio" name="ans" id="a3">
                                    <label for="a3">Land Title Search</label>
                                </span>
                            </div>
                        </div>

                    </div><!--//optionalBtns-->

                    <div class="formBtn">
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </div><!--//feedbackModelFooter-->

            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->  

<script>
    var today, datepicker;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('.appDate').datepicker({
        minDate: today,
        format: 'dd-mm-yyyy'
    });
    datepicker = $('.rescheduleDate').datepicker({
        minDate: today,
        format: 'dd-mm-yyyy'
    });

    $('#appTime, .rescheduleTime').timepicker({
        uiLibrary: 'bootstrap'
    });
    // timepicker
//    $('#appTime').timepicker({
//      uiLibrary: 'bootstrap4'
//    }); 
</script>
