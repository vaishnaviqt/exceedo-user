@extends('user.layouts.master')
@section('title')
Appointments
@endsection
@section('class')
@endsection
@section('content')
<style>

    .loader{
        position: fixed;
        top:0;
        left:0;
        right: 0;
        bottom:0;
        display:flex;
        align-items:center;
        justify-content:center;
        z-index: 9999;
        background: #ffffffa6;
    }
    .loader img{
        width:80px;
    }

    div.stars {
        width: 270px;
        display: inline-block;
    }

    input.star { display: none; }

    label.star {
        float: right;
        padding: 10px;
        font-size: 36px;
        color: #444;
        transition: all .2s;
    }

    input.star:checked ~ label.star:before {
        content: '\f005';
        color: #FD4;
        transition: all .25s;
    }

    input.star-5:checked ~ label.star:before {
        color: #FE7;
        text-shadow: 0 0 20px #952;
    }

    input.star-1:checked ~ label.star:before { color: #F62; }

    label.star:hover { transform: rotate(-15deg) scale(1.3); }

    label.star:before {
        content: '\f006';
        font-family: FontAwesome;
    }
</style>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
<div class="loader d-none">
    <img src="{{ asset('images/Spin-1s-200px.gif') }}">
</div>
 
    @include('flash-message')
    <div class="pageTitle d-flex flex-wrap justify-content-between">
        <h1 class="mb-2">Appointments</h1>

                          
        <div class="genReportLink float-right">
            <a href="javascript:void(0);" id="addAppointmentPopupShow" data-toggle="modal" data-target="#addAppointment">Book Appointment </a>
        </div>
    </div><!--//pageTitle-->

    <div class="countCards mb-4">
        <div class="row">
            <div class="col-lg-4 col-md-6">
            <a href="javascript:void(0);" class="allAppointButton">     
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="images/svg-icons/all-app-icon.svg" alt="all Appointments" />
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $totalAppointments }}</strong></p>
                        <p>All Appointments</p>
                    </div>
                </article><!--//countCardArt-->
                </a>  
            </div><!--//col-lg-3-->

            <div class="col-lg-4 col-md-6">
            <a href="javascript:void(0);" class="pendingAppointButton">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="images/svg-icons/upcoming-app-icon.svg" alt="Upcomming Appointments" />                                    
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $upcommingAppointments }}</strong></p>
                        <p>Upcomming Appointments</p>
                    </div>
                </article><!--//countCardArt-->
                </a>
            </div><!--//col-lg-3-->

            <div class="col-lg-4 col-md-6">
            <a href="javascript:void(0);" class="doneAppointButton">
                <article class="countCardArt">
                    <div class="coutRepIcon">
                        <img src="images/svg-icons/done-app-icon.svg" alt="Done Appointments" />                                    
                    </div>
                    <div class="countRepDetail">
                        <p><strong>{{ $doneAppointments }}</strong></p>
                        <p>Done Appointments</p>
                    </div>
                </article><!--//countCardArt-->
                </a>
            </div><!--//col-lg-3-->                      
        </div><!--//row-->
    </div><!--//countCards-->
    <!-- <div class="appointmentTopLinks mb-4">
        <a href="javascript:void(0);" class="allAppointButton" data-effect="mfp-zoom-in">All Appointments</a> 
        <a href="javascript:void(0);" class="pendingAppointButton" data-effect="mfp-zoom-in">Pending Appointments</a>
        <a href="javascript:void(0);" class="doneAppointButton" data-effect="mfp-zoom-in">Done Appointments</a>
    </div> -->
    <div class="customStyleCalendar">
        <div id="calendar"></div>
    </div><!--//customStyleCalendar-->

<!-- Add Appointment model -->
@include('user.appointments.__models')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
        $(document).on('click',"#resetForm",function(){
            $('#allAppointExpert option').removeAttr('selected')
            .filter('[value=""]')
            .attr('selected', true);
            $('#allAppointDate').val('');
            $('#serviceId option').removeAttr('selected')
            .filter('[value=""]')
            .attr('selected', true);
            viewAllAppointment();
        })
       
        $(document).on('click',"#resetCompleteForm",function(){
            $('#doneExpertId option').removeAttr('selected')
            .filter('[value=""]')
            .attr('selected', true);
            $('#doneDate').val('');
            $('#doneserviceId option').removeAttr('selected')
            .filter('[value=""]')
            .attr('selected', true);
            viewDoneAppointment();
        })
        
        $(document).on('click',"#resetUpcommingForm",function(){
            $('#pendingExpertId option').removeAttr('selected')
            .filter('[value=""]')
            .attr('selected', true);
            $('#pendingDate').val('');
            $('#upcommingServiceId option').removeAttr('selected')
            .filter('[value=""]')
            .attr('selected', true);
            viewPendingAppointment();
        })
        </script>
<script>
$(document).ready(function () {
    $('#addAppointmentPopupShow').on('click', function () {
        $('#valueAddedServices').val(0);
        $('#remarks').val('');
        $("#terms").prop("checked", false);
    });
    $('#valueAddedServices').on('change', function () {
        var serviceId = $(this).val();
        var price = '';
<?php
foreach ($services as $service) {
    ?>
            if (serviceId == <?php echo $service->id ?? 'null' ?>) {
                price = <?php echo $service->price + ($service->price*18/100) ?? 'null' ?>
            }
    <?php
}
?>
        var data = '<span>You need to Pay</span>INR ' + price;
        $('#servicePrice').html(data);
        $('#serviceInputPrice').val(price);
    });
//add appointment validation
    $('#addAppointButton').on('click', function (e) {
        e.preventDefault();
        var appointDate = $('#appDate').val();
        var appointTime = $('#appTime').val();
        var remarks = $('#remarks').val();
        var terms = $('#terms').val();
        var service = $('#valueAddedServices').val();
        var servicePrice = $('#serviceInputPrice').val();

        $('.errorMsg').remove();
        $('.loader').toggleClass('d-none');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "post",
            url: "add-appointment-validate",
            data: $("#addAppointForm").serialize(),
            success: function (data) {
                $('.loader').toggleClass('d-none');
                var order_id = '';
                if (data.order_id) {
                    order_id = data.order_id;
                }
                var amount = $('#serviceInputPrice').val() * 100;

                var remarks = $('#remarks').val();
                var options = {
                    "key": "{{ config('app.razorpay_api_key') }}", // Enter the Key ID generated from the Dashboard
                    "amount": amount, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                    "currency": "{{ config('app.currency') }}",
                    "name": "{{ config('app.account_name') }}",
                    "description": remarks,
                    "image": "{{ asset('images/logo-black.svg') }}",
                    "order_id": order_id, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                    "handler": function (response) {
                        $('#razorpay_payment_id').val(response.razorpay_payment_id);
                        $('#razorpay_order_id').val(response.razorpay_order_id);
                        $('#razorpay_signature').val(response.razorpay_signature);
                        $('#addAppointForm').submit();
                    },
                    "prefill": {
                        "name": "{{ auth()->user()->name }}",
                        "email": "{{ auth()->user()->email }}",
                        "contact": "{{ auth()->user()->mobile }}"
                    },
//            "notes": {
//                "address": "Razorpay Corporate Office"
//            },
                    "theme": {
                        "color": "#3399cc"
                    }
                };
                var rzp1 = new Razorpay(options);
                rzp1.on('payment.failed', function (response) {

                });

                rzp1.open();


            },
            error: function (errorResponse) {
                $('.loader').toggleClass('d-none');
                $('.error-span').remove();
                $.each(errorResponse.responseJSON.errors, function (field_name, error) {
                    if (field_name == 'terms') {
                        $(document).find('[name=' + field_name + ']').parent().parent().after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    } else {
                        $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');
                    }
                })
            }
        });

    });
    var baseUrl = $('meta[name="base_url"]').attr('content');
    var calendar = $('#calendar').fullCalendar({
        editable: true,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        events: baseUrl + "/user/appointment-details",
        selectable: true,
        selectHelper: true,
        select: function (start, end, allDay)
        {
            var start = $.fullCalendar.formatDate(start, "DD-MM-Y");
            var end = $.fullCalendar.formatDate(end, "HH:mm:ss");
            $('#appDate').val(start);
            $('#appTime').val(end);
            $('#valueAddedServices').val(0);
            $('#remarks').val('');
            $("#terms").prop("checked", false);

            $('#addAppointment').modal('show');
        },
        editable: true,
        eventClick: function (event)
        {
            if (new Date() <= event.start) {
                var title = event.title;
                var dateTime = $.fullCalendar.formatDate(event.start, "dddd, DD MMMM, hh:mm a");
                var evDate = $.fullCalendar.formatDate(event.start, "DD-MM-Y");
                var evTime = $.fullCalendar.formatDate(event.start, "HH:mm:ss");
                var eventId = event.id;
                var rescheduled_by = event.rescheduled_by;
                var canceled_by = event.canceled_by;
                var rescheduleButton = '';
                var button = '';
                if (rescheduled_by == 1) {
                    var rescheduleButton = `<li><a href="javascript:void(0);" data-id="` + eventId + `" data-date="` + evDate + `" data-time="` + evTime + `" class="userAnotherSuggestPopup">Suggest Another time</a></li>`;
                }
                if (event.status == 3) {
                    if (canceled_by == 0) {
                        var text = "You cancel this appointment";
                    } else if (canceled_by == 2) {
                        var text = "Admin cancel this appointment";
                    }
                    button = `<ul class="confirmAppointment">
                        <li>` + text + `</li>
                    </ul>`;
                } else if (event.status != 1 && rescheduled_by == 1) {
                    button = `<ul class="confirmAppointment">
                        <li><a class="userConfirmAppoint" data-href="` + baseUrl + "/user/confirm-appointment/" + eventId + `" href="javascript:;">Confirm the Appointment</a></li>
                        ` + rescheduleButton + `
                    </ul>`;
                } else if (event.status != 1) {
                    button = `<ul class="confirmAppointment">
                        <li>Waiting for the Admin to Confirm this slot </li>
                    </ul>`;
                } else {
                    button = `<ul class="confirmAppointment">
                        <li>Your Appointment Booked </li>
                <li><a class="usercancelAppoint" data-href="` + baseUrl + "/user/cancel-appointment/" + eventId + `" href="javascript:;">Cancel the Appointment</a></li>
                    </ul>`;
                }
                var html = `<div class="row">
                <div class="col-12">
                    <ul>
                        <li>
                            <h2>` + title + `</h2>
                            <span class="dateTime">` + dateTime + ` </span>
                            
                        </li>
                    </ul>
                    ` + button + `
                </div>         
            </div>`;
                $('.suggestAnotherSchedule').html(html);
                $('#approval-appointment-popup').modal('show');
            }
        },
    }
    );
});</script>
<script>
    $(document).ready(function () {
        $('body').on('click', '.usercancelAppoint', function (e) {
            e.preventDefault();
            var link = $(this).data('href');
            swal({
                title: "Are you sure want to cancel appointment?",
                text: "If you cancel this appointment, payment not refuned",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willDelete) => {
                        if (willDelete) {
                            window.location.href = link;
                        } else {
                            return false;
                        }
                    });
        });
//        user reschedule appointment
        $('body').on('click', '.userAnotherSuggestPopup', function (e) {
            var id = $(this).data('id');
            var date = $(this).data('date');
            var time = $(this).data('time');
            $('.rescheduleDate').val(date);
            $('.rescheduleTime').val(time);
            $('.rescheduleId').val(id);
            $('.rescheduleRemarks').val('');
            $(".rescheduleTerms").prop("checked", false);
            $('#approval-appointment-popup').modal('hide');
            $('#suggest-another-time-popup').modal('show');
        });
        $('#rescheduleSubmitButton').on('click', function () {
            var rescheduleDate = $('.rescheduleDate').val();
            var rescheduleTime = $('.rescheduleTime').val();
            var rescheduleRemarks = $('.rescheduleRemarks').val();
            var rescheduleTerms = $('.rescheduleTerms').val();
            var rescheduleId = $('.rescheduleId').val();
            if (rescheduleDate == '') {
                $('.errorMsg').remove();
                $('.rescheduleDate').parent().after('<span class="text-danger errorMsg" role="alert"><small>The Appointment Date field is required.</small></span>');
                return false;
            }
            if (rescheduleTime == '') {
                $('.errorMsg').remove();
                $('.rescheduleTime').parent().after('<span class="text-danger errorMsg" role="alert"><small>The Appointment Time field is required.</small></span>');
                return false;
            }

            if (rescheduleRemarks == '') {
                $('.errorMsg').remove();
                $('.rescheduleRemarks').parent().after('<span class="text-danger errorMsg" role="alert"><small>The Remarks field is required.</small></span>');
                return false;
            }
            if ($('.rescheduleTerms').prop("checked") == false) {
                $('.errorMsg').remove();
                $('.rescheduleTerms').parent().after('<span class="text-danger errorMsg" role="alert"><small>The Terms field is required.</small></span>');
                return false;
            }
            $('.errorMsg').remove();
            $('.loader').toggleClass('d-none');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "post",
                url: "reschedule-appointment",
                data: {
                    date: rescheduleDate,
                    time: rescheduleTime,
                    remarks: rescheduleRemarks,
                    terms: rescheduleTerms,
                    id: rescheduleId,
                },
                success: function (data) {
                    $('.loader').toggleClass('d-none');
                    if (data.date == true) {
                        $('.errorMsg').remove();
                        $('.rescheduleDate').parent().parent().after('<span class="text-danger errorMsg" role="alert"><small>Please Select future date.</small></span>');
                        return false;
                    }
                    if (data == true) {
                        swal("Your appointment reschedule successfully!")
                                .then((value) => {
                                    location.reload();
                                });
                    }
                },
                error: function () {
                    alert("Reschedule appointment failed");
                    return false;
                }
            });
        });
//        user appoitment confirmation
        $('body').on('click', '.userConfirmAppoint', function (e) {
            e.preventDefault();
            var link = $(this).data('href');
            swal({
                text: "Are you sure want to confirm appointment?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willDelete) => {
                        if (willDelete) {
                            window.location.href = link;
                        } else {
                            return false;
                        }
                    });
        });
        $('body').on('click', '.viewAllAppointSearchForm', function () {
            viewAllAppointment();
        });
        $('.allAppointButton').on('click', function () {
            viewAllAppointment();
        });
    });
    function viewAllAppointment() {
        var all_expert_id = $('#allAppointExpert').val();
        var all_date = $('#allAppointDate').val();
        let service_id = $('#serviceId').val();
        $('.loader').toggleClass('d-none');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "post",
            url: "view-all-appointment",
            data: {
                all_date: all_date,
                service_id:service_id,
                all_expert_id: all_expert_id,
            },
            success: function (data) {
                $('.loader').toggleClass('d-none');
                $('#allAppointmentCard').html(data.html);
                $(".appointments").mCustomScrollbar({
                    theme: "dark",
                    scrollButtons: {scrollType: "stepped"},
                    live: "on"
                });
                $.magnificPopup.open({
                    items: {
                        src: '#view-all-appointments-popup',
                        callbacks: {
                            beforeOpen: function () {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        type: 'inline'
                    }

                });
            },
            error: function () {
                alert("Fetching Expert Record Failed");
                return false;
            }
        });
        return false;
    }
    $(document).ready(function () {
        $('body').on('click', '.viewPendingAppointSearchForm', function () {
            viewPendingAppointment();
        });
        $('.pendingAppointButton').on('click', function () {
            viewPendingAppointment();
        });
    });
    function viewPendingAppointment() {
        var expert_id = $('#pendingExpertId').val();
        var date = $('#pendingDate').val();
        var serviceId = $('#upcommingServiceId').val();
        $('.loader').toggleClass('d-none');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "post",
            url: "view-pending-appointment",
            data: {
                date: date,
                expert_id: expert_id,
                serviceId: serviceId,
  
            },
            success: function (data) {
                $('.loader').toggleClass('d-none');
                $('#allAppointmentCard').html(data.html);
                $(".appointments").mCustomScrollbar({
                    theme: "dark",
                    scrollButtons: {scrollType: "stepped"},
                    live: "on"
                });
                $.magnificPopup.open({
                    items: {
                        src: '#view-all-appointments-popup',
                        callbacks: {
                            beforeOpen: function () {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        type: 'inline'
                    }

                });
            },
            error: function () {
                alert("Fetching Expert Record Failed");
                return false;
            }
        });
        return false;
    }

    //view done appointment
    $(document).ready(function () {
        $('body').on('click', '.viewDoneAppointSearchForm', function () {
            viewDoneAppointment();
        });
        $('.doneAppointButton').on('click', function () {
            viewDoneAppointment();
        });
    });
    function viewDoneAppointment() {
        var expert_id = $('#doneExpertId').val();
        var date = $('#doneDate').val();
        var serviceId = $('#doneserviceId').val();
        $('.loader').toggleClass('d-none');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "post",
            url: "view-done-appointment",
            data: {
                date: date,
                expert_id: expert_id,
                serviceId: serviceId,
            },
            success: function (data) {
                $('.loader').toggleClass('d-none');
                $('#allAppointmentCard').html(data.html);
                $(".appointments").mCustomScrollbar({
                    theme: "dark",
                    scrollButtons: {scrollType: "stepped"},
                    live: "on"
                });
                $.magnificPopup.open({
                    items: {
                        src: '#view-all-appointments-popup',
                        callbacks: {
                            beforeOpen: function () {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        type: 'inline'
                    }

                });
            },
            error: function () {
                alert("Fetching Expert Record Failed");
                return false;
            }
        });
        return false;
    }
</script>
<script type="text/javascript">
    // getting rating value
    $(document).ready(function () {
        $('body').on('change', '.starRatingValue', function () {
            $('#starRating').val($(this).val());
        });

 // getting appointment id 
        $('body').on('click', '.get-appointment', function (e) {
            e.preventDefault();
            var appointmentId = $(this).data('id');
            $('#appointment_id').val(appointmentId);
        });


        // submit user feedback
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('body').on('click', '.review-btn-submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: "store-reviews",
                data: $("#review-rating-form").serialize(),
                success: function (data) {
                    $('#service-feedback').prop("disabled", false);
                    $('#service-feedback').text('Submit');
                    if (data.success) {
                        $('#review-rating-form')[0].reset();
                        swal({
                            title: "Good job!",
                            text: "Your review submitted successfully!",
                            icon: "success",
                        }).then((value) => {
                            location.reload();
                        });
                    } else {
                        swal({
                            text: "Something went wrong!",
                            icon: "error",
                        });
                    }
                },
                error: function (errorResponse) {
                    $('#service-feedback').prop("disabled", false);
                    $('#service-feedback').text('Submit');
                    $.each(errorResponse.responseJSON.errors, function (field_name, error) {
                        $(document).find('[name=' + field_name + ']').after('<span class="text-strong error-span text-danger" role="alert">' + error + '</span>');

                    })
                }
            });

        });

    });
</script>



@endsection   
