@extends('user.layouts.master')
@section('title')
Your Profile
@endsection
@section('class')
paymentsBody
@endsection
@section('content')
@include('flash-message')
<h2>Your Profile</h2>  
    <div class="upload-response"></div>
<div class="profileSection mb-5">
    <div class="profileUserImg">
        <img src="{{asset('images/loader.gif')}}" id="photoLoader">
        @php
        $path = asset('broker/images/profile-pic.png');
        if(Storage::disk('user')->exists(str_replace('user_image', '', $user->photo)))
        {
        $path = Storage::disk('user')->url(str_replace('user_image/', '', $user->photo));
        }
        @endphp
        <img class="userPhoto" src="{{ $path }}" alt="{{ $user->name }}" />
        <div class="cameraIcon">
            <form>
                @csrf
                <button class="d-none"><i class="fas fa-camera-retro"></i></button>
                <input class="uploadAvatar" type="file" name="photo" />
            </form>
        </div>
    </div><!--//profileUserImg-->
    <div class="profileUserDetail">
        <div class="row mb-3">
            <div class="col-lg-3 col-4">
                <span>Name</span>
            </div>
            <div class="col">
                {{ $user->name }}
            </div>
        </div><!--//row-->

        <div class="row mb-3">
            <div class="col-lg-3 col-4">
                <span>Mobile Number </span>
            </div>
            <div class="col">
                {{ $user->mobile }} 
            </div>
        </div><!--//row-->

        <div class="row">
            <div class="col-lg-3 col-4">
                <span>Email id</span>
            </div>
            <div class="col">
                {{ $user->email }}
            </div>
        </div><!--//row-->
    </div><!--//profileUserDetail-->

    <div class="editProfileBtn">
        <a href="javascript:void(0);" class="btn btn-primary editprofileButton" data-toggle="modal" data-target="#editProfile">Edit your Profile</a>
    </div>
</div><!--//profileSection-->                           
@endsection   
@section('modal')
<!-- Edit Profile model -->
<div class="modal fade w-400" id="editProfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="popupForm">
                    <form action="{{ url('user/profile') }}" method="post">
                        @csrf
                        <div class="popupHeading">
                            <h2>Edit Your Profile</h2>
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                            <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Name" id="Name" name="name" value="{{ old('name', $user->name) }}">
                            <label for="Name" class="floating-label">Name</label>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="text" class="form-control @error('mobile') is-invalid @enderror" placeholder="Mobile Number" id="number" name="mobile" value="{{ old('mobile', $user->mobile) }}">
                            <label for="number" class="floating-label">Mobile Number</label>
                            @error('mobile')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group floating-field singleBorder">
                            <input type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Email id" id="email" name="email" value="{{ old('email', $user->email) }}" disabled>
                            <label for="email" class="floating-label">Email id</label>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div> 

                        <div class="formBtn">
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div> 
                    </form>
                </div><!--//popupForm--> 
            </div><!--//modal-body--> 
        </div>
    </div>
</div><!--//modal-->
@if($errors->has('userProfile'))
<script>
    $(document).ready(function () {
        $('.editprofileButton').click();
    });
</script>
@endif
@endsection   
