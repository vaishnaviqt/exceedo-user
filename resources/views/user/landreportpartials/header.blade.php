<div class="pageTitle">
    <h1>Land Listing</h1>
</div><!--//pageTitle-->

<div class="countCards">
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <a class="nav-link @if( Request::route()->getName() == 'user.your-land-report' ) active @endif" href="{{ route('user.your-land-report') }}">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('user/images/svg-icons/allReport-icon.svg') }}" alt="all reports" />
                </div>
                <div class="countRepDetail">
                    <p><strong>{{ landReportCount() }}</strong></p>
                    <p>All Reports</p>
                </div>
            </article><!--//countCardArt-->
            </a>
        </div><!--//col-lg-3-->

        <div class="col-lg-3 col-md-6">
            <a class="nav-link @if( Request::route()->getName() == 'user.your-land-report-delivered' ) active @endif" href="{{ route('user.your-land-report-delivered') }}">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('user/images/svg-icons/deliveryReport-icon.svg') }}" alt="Delivered Reports" />                                    
                </div>
                <div class="countRepDetail">
                    <p><strong>{{ landReportCount(1) }}</strong></p>
                    <p>Delivered Reports</p>
                </div>
            </article><!--//countCardArt-->
            </a>
        </div><!--//col-lg-3-->

        <div class="col-lg-3 col-md-6">
            <a class="@if( Request::route()->getName() == 'user.your-land-report-inprogress' ) active @endif" href="{{ route('user.your-land-report-inprogress') }}">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('user/images/svg-icons/inprogressReport-icon.svg') }}" alt="Inprogress Reports" />                                    
                </div>
                <div class="countRepDetail">
                    <p><strong>{{ landReportCount(2) }}</strong></p>
                    <p>Inprogress Reports</p>
                </div>
            </article><!--//countCardArt-->
            </a>
        </div><!--//col-lg-3-->

        <div class="col-lg-3 col-md-6">
            <a class="@if( Request::route()->getName() == 'user.your-land-report-pending' ) active @endif" href="{{ route('user.your-land-report-pending') }}">
            <article class="countCardArt">
                <div class="coutRepIcon">
                    <img src="{{ asset('user/images/svg-icons/pendingReport-icon.svg') }}" alt="Pending Reports" />                                    
                </div>
                <div class="countRepDetail">
                    <p><strong>{{ landReportCount('pending') }}</strong></p>
                    <p>Pending Reports</p>
                </div>
            </article><!--//countCardArt-->
            </a>
        </div><!--//col-lg-3-->                        
    </div><!--//row-->
</div><!--//countCards-->


<div class="exceedoTabs">
    <div class="reportNavBar">                        
        <div class="genReportLink">
           
        </div>

        <div class="reportTabs">
            <ul class="nav nav-tabs" style="display:none;">
                <li>
                    @php
                    $report = 0;
                    if(isset($allReports)){
                    $report = $allReports->total();
                    }else{
                    $report = reportsCount('', auth()->user());
                    }
                    @endphp
                    <a class="@if( Request::route()->getName() == 'user.your-land-report' ) active @endif" href="{{ route('user.your-land-report') }}">All REPORTS ({{ $report }})</a>
                </li>
                <li>
                    @php
                    $delivered = 0;
                    if(isset($deliveredReports)){
                    $delivered = $deliveredReports->total();
                    }else{
                    $delivered = reportsCount(1, auth()->user());
                    }
                    @endphp
                    <a class="@if( Request::route()->getName() == 'user.your-land-report-delivered' ) active @endif" href="{{ route('user.your-land-report-delivered') }}">DELIVERED REPORTS ({{ $delivered }})</a>
                </li>
                <li>
                    @php
                    $inprogress = 0;
                    if(isset($inprogressReports)){
                    $inprogress = $inprogressReports->total();
                    }else{
                    $inprogress = reportsCount(2, auth()->user());
                    }
                    @endphp
                    <a class="@if( Request::route()->getName() == 'user.your-land-report-inprogress' ) active @endif" href="{{ route('user.your-land-report-inprogress') }}">INPROGRESS REPORTS ({{ $inprogress }})</a>
                </li>
                <li>
                    @php
                    $pending = 0;
                    if(isset($pendingReports)){
                    $pending = $pendingReports->total();
                    }else{
                    $pending = reportsCount('pending', auth()->user());
                    }
                    @endphp
                    <a class="@if( Request::route()->getName() == 'user.your-land-report-pending' ) active @endif" href="{{ route('user.your-land-report-pending') }}">Pending REPORTS ({{ $pending }})</a>
                </li>
            </ul>
        </div>
    </div><!--//reportNavBar-->
