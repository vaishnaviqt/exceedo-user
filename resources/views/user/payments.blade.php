@extends('user.layouts.master')
@section('title')
Payments
@endsection
@section('class')
@endsection
@section('content')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="pageTitle d-flex align-items-center">
    <h2>Payments</h2>

    <div class="sortDropdown">
        <label>Show</label>
        <div class="dropdown custom-dropdown">
            <a href="javascript:void(0);" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle min-w-60">
                <span class="selected">{{ $paginate ?? '' }}</span><span class="caret"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="javascript:void(0);"  class="paymentPaginate" data-value="10">10</a></li>
                <li><a href="javascript:void(0);"  class="paymentPaginate" data-value="25">25</a></li>
                <li><a href="javascript:void(0);"  class="paymentPaginate" data-value="50">50</a></li>
                <li><a href="javascript:void(0);"  class="paymentPaginate" data-value="100">100</a></li>
            </ul>
        </div><!-- //dropdown -->
    </div><!--//sortDropdown-->
</div><!--//pageTitle-->
<form method="post" id="paginateSubmitForm">
    @csrf
    <input type="hidden" name="day" id="paginateHiddenFiled">
</form>
<div class="filterFormSection">
    <a href="javascript:void(0);" class="mobileFilterBtn">Search </a>                              
    <div class="filterForm">
        <form action="{{url('user/payments')}}"  method="POST">
            @csrf
            <h2>
                Search
                <a href="javascript:void(0);" class="m_filterCloseBtn"></a>
            </h2>
            <div class="row">
                <div class="col-lg-10">
                    <div class="row">                                    
                        <div class="col-lg-3 col-md-6">
                            <div class="form-group">
                                <input class="form-control" id="datePickerSearch" type="text" name="created_at" value="{{ !empty($startDate) ? Carbon\Carbon::parse($startDate)->format('d-m-Y') : '' }}" placeholder="Date"/>
                            </div>
                        </div><!--//col-lg-3-->

                        <div class="form-group">
                            <select class="form-control" id="paymentType" name="paymentType">
                                <option value="">Payment Method</option>
                                <option value="cash" @if($paymentType == 'cash') {{ __('selected') }} @endif>Cash</option>
                                <option value="card" @if($paymentType == 'card') {{ __('selected') }} @endif>Card</option>
                                <option value="netbanking" @if($paymentType == 'netbanking') {{ __('selected') }} @endif>netbanking</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <select class="form-control" id="serviceType" name="serviceType">
                                <option value="">Services</option>
                                @if(count($services))
                                @foreach($services as $service)
                                @php
                                $selected = '';
                                if($service->id == $serviceType){
                                $selected = 'selected';
                                }
                                @endphp
                                <option value="{{ $service->id }}" {{ $selected }}>{{ $service->service_name }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="col-lg-2">
                            <button type="submit" class="btn btn-primary">Search </button>
                        </div>
                        <div class="col-lg-2">
                            <button class="btn btn-primary" data-toggle="tab" onclick="formReset()">Reset</button>
                        </div>
                    </div><!--//row-->

                </div><!--//col-lg-10-->
            </div><!--//row-->
        </form>
    </div><!--//filterForm-->
</div><!--filterFormSection-->
<div class="commonTableContainer">
    <div class="custom-table-responsive">
        <table class="table customTable">
            <thead>
                <tr>
                    <th width="120">Payment Id</th>
                    <th width="100">Transaction Id</th>
                    <th width="100">Amount</th>
                    <th width="150">Payment Mode</th>
                    <th width="100">Service</th>
                    <th width="100">Bank</th>
                    <th width="190">Payment at</th>
                    <th width="190">Download</th>
                </tr>
            </thead>
            <tbody>
                @forelse($userPayments as $payments)
                <tr>
                    <td>
                        <span class="">{{$payments->paymentId}}</span>
                    </td>
                    <td>
                        <span class="">{{isset($payments->transaction_id) ? $payments->transaction_id : '' }}</span>
                    </td>
                    <td>
                        <span class="">{{isset($payments->currency) ? $payments->currency : '' }}
                            {{isset($payments->amount) ? $payments->amount : '' }}</span>
                    </td>
                    <td>
                        <span class="">{{isset($payments->method) ? $payments->method : '' }}</span>
                        <!--                        Thu, 09 Jul 2020 1:00 PM-->
                    </td>
                    <td>
                        <span class="">{{isset($payments->service_name) ? ucwords($payments->service_name) : 'N/A'}}</span>
                    </td>
                    <td>
                        <span class="">{{isset($payments->bank) ? $payments->bank : ''}}</span>
                    </td>
                    <td>
                        <span class="">{{$payments->created_at}}</span>
                    </td>
                    <td scope="row">
                        @if(!empty($payments->file) && $payments->file!=null)
                        <a href="{{route('user.download',['id'=>$payments->paymentId])}}"><i class="fa fa-download fa-2x text-danger"></i></a>
                        @else
                        <h6>N/A</h6>
                        @endif
                    </td>
<!--                    <td>
                        <span class="statusLabel inprogress">{{isset($payments->status) ? $payments->status : '' }}</span>
                    </td>-->
                </tr>
                @empty
                <tr>
                    <td scope="row" colspan="7">No Records Available</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div><!--//table-reponsive-->                    
    {{$userPayments->links()}}
</div><!--//commonTableContainer-->                   
@endsection   

@section('custom-script')
<script>
    formReset = function () {
        window.location.href = "<?php url('user/payments'); ?>";
    }
</script>

@endsection  
  
