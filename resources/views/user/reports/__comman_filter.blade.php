<div class="filterFormSection">
    <a href="javascript:void(0);" class="mobileFilterBtn">Search </a>   
    @if(request()->is('user/your-land-report'))
    <form action="{{ url('user/your-land-report') }}" method="post">
        @elseif(request()->is('user/your-land-report-delivered'))
        <form action="{{ url('user/your-land-report-delivered') }}" method="post">
            @elseif(request()->is('user/your-land-report-pending'))
            <form action="{{ url('user/your-land-report-pending') }}" method="post">
                @elseif(request()->is('user/your-land-report-inprogress'))
                <form action="{{ url('user/your-land-report-inprogress') }}" method="post">
                    @endif
                    @csrf
                    <div class="filterForm">
                        <h2>
                            Search
                            <a href="javascript:void(0);" class="m_filterCloseBtn"></a>
                        </h2>

                        <div class="row">
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6">
                                        <div class="form-group floating-field">
                                            <input type="text" class="form-control" placeholder="Name" id="Name" name="name" value="{{ $name ?? '' }}">
                                            <label for="Name" class="floating-label">Name</label>
                                        </div>
                                    </div><!--//col-lg-3-->

                                    <!--                                    <div class="col-lg-3 col-md-6"> 
                                                                            <div class="form-group floating-field">
                                                                                <select class="form-control" id="ReportType" onclick="this.setAttribute('value', this.value);" value="">
                                                                                    <option></option>
                                                                                    <option>Report Type 1</option>
                                                                                    <option>Report Type 2</option>
                                                                                    <option>Report Type 3</option>
                                                                                </select>
                                                                                <label for="ReportType" class="floating-label">Report Type</label>
                                                                            </div>
                                                                        </div>//col-lg-3-->

                                    <div class="col-lg-3 col-md-6">
                                        <div class="form-group floating-field">
                                            <select class="form-control" id="Location" onclick="this.setAttribute('value', this.value);" name="location">
                                                <option value="">Select State</option>
                                                @foreach($states as $state)
                                                <option value="{{ $state->id }}" @if($location == $state->id) {{ __('selected') }} @endif>{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                            <label for="Location" class="floating-label">Location</label>
                                        </div>
                                    </div><!--//col-lg-3-->

                                    <div class="col-lg-3 col-md-6">
                                        <div class="form-group">
                                            <input class="form-control" id="datePickerSearch" type="text" name="created_at" placeholder="Date" max="{{ date('Y-m-d') }}" value="{{ $date ?? '' }}" id="Date" name="date" />

                                            <!--                                             
                                            <input type="date" class="form-control form-calendar" placeholder="Date" max="{{ date('Y-m-d') }}" value="{{ $date ?? '' }}" id="Date" name="date">
                                            <label for="Date" class="floating-label">Date</label> -->
                                        </div>
                                    </div><!--//col-lg-3-->
                                </div><!--//row-->
                            </div><!--//col-lg-10-->

                            <div class="col-lg-2">
                                <button class="btn btn-primary" type="submit">Search </button>
                            </div>
                            <div class="col-lg-2">
                                <button class="btn btn-primary" data-toggle="tab" onclick="formReset()">Reset</button>
                            </div>
                        </div><!--//row-->
                    </div><!--//filterForm-->
                </form>
                </div><!--filterFormSection-->

                @section('custom-script')
                <script>
                    formReset = function () {
                        window.location.href = "<?php url('user/reports'); ?>";
                    }
                </script>
                @endsection
