@extends('layouts.master')

@section('title')
 Privacy Policy
@endsection
@section('content')
<section class="commonFormPage adjustHeaderSpace staticPage">
    <div class="heading-title">
        <h2>Privacy Policy</h2>
    </div>
    <!--//heading-title-->
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5">
                <p class="text-left w-100 pt-3 title">
                            Our commitment to privacy
                        </p>
<p class="text-left w-100 pt-3 content">
                            Respect for your privacy is coded into our DNA. Since we started Exceedo Know Your Land, we�ve aspired to build our Services with a set of strong privacy principles in mind.
                        </p>
<p class="text-left w-100 pt-3 content">
                            Following part explains what we do with the personal information we hold, how we protect it, and it explains your privacy rights (if applicable).
                        </p>
<p class="text-left w-100 pt-5 title">
                            If we change this Privacy Statement:
                        </p>
<p class="text-left w-100 pt-3 content">
                            From time to time, Exceedo Know Your Land may revise this Policy. The date of the last update can be found at the top of the Policy, and this policy describes how we have handled personal data in the twelve months prior to the date indicated. You should check this page from time to time to review any changes we have made.
                        </p>
<p class="text-left w-100 pt-5 title">
                            The personal information we collect:
                        </p>
<p class="text-left w-100 pt-3 content">
                            When we collect personal information, we are open about how we will use it.
                        </p>
<p class="text-left w-100 pt-5 title">
                            We collect information:
                        </p>
<ul class="w-100">
<li>Through our website or through our services � for example names, phone number, addresses or email details.</li>
<li>From meetings with the experts, staff or business contacts, such as exchanging business cards, or collecting information at conferences or business events.</li>
<li>Information that you provide to us in connection with the services we provide, for example in connection with your land ownership, land use and land development (sale/use deed) agreement.</li>
<li>Information that we use to validate your identity, phone number or proof of residency document.</li>
</ul>
<p class="text-left w-100 pt-5 title">
                            Our legal basis for processing your information
                        </p>
<p class="text-left w-100 pt-3 content">
                            We rely upon a number of different legal bases for processing personal information and special category personal information � these include processing personal information where it is in our legitimate interests to do so, where this is necessary for the fulfilment of a contract or where the processing is necessary to carry out our obligations under employment law. Where we rely on our legitimate interests, this means that we use personal information to run our business and to provide the services we have been asked to provide. We only collect information that has been supplied voluntarily, you do not have to provide us with personal information.  However, if you do not provide us with information we need by law or require to do work, we may not be able to offer certain products and services.
                        </p>
<p class="text-left w-100 pt-5 title">
                            How we use your information
                        </p>
<p class="text-left w-100 pt-3 content">
                            We use the data collected for various purposes:
                        </p>
<ul class="w-100">
<li>To respond to your requests and notify you about changes in our Services</li>
<li>To meet with regulatory and legal requirements to verify who you are as a condition of providing Exceedo Know Your Land services and products.</li>
<li>To gather analysis or valuable information so that we can improve our Services</li>
<li>To allow you to participate in interactive features of our Service when you choose to do so</li>
<li>To provide you with news, special offers and general information about other goods, services and events which we offer that are similar to those that you have already purchased or enquired about unless you have opted not to receive such information</li>
</ul>
<p class="text-left w-100 pt-5 title">
                            Whom we share personal information with
                        </p>
<p class="text-left w-100 pt-3 content">
                            We may share your personal information with:
                        </p>
<ul class="w-100">
<li>Exceedo Know Your Land employees who require it to perform their jobs.</li>
<li>Organizations that support the products or services we provide to you.</li>
<li>Anyone you give us permission to share it with.</li>
<li>Official bodies to detect and prevent criminal activity e.g. money laundering, theft, fraud, terrorism, cybercrime.</li>
</ul>
<p class="text-left w-100 pt-3 content">
                            We will never sell your personal information and we take steps to keep your details safe and secure.
                        </p>
<p class="text-left w-100 pt-5 title">
                            Where we keep and process your information
                        </p>
<p class="text-left w-100 pt-3 content">
                            Your information, including Personal Data, may be transferred to � and maintained on � computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.
                        </p>
<p class="text-left w-100 pt-3 content">
                            We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.
                        </p>
<p class="text-left w-100 pt-3 content">
                            Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.
                        </p>
<p class="text-left w-100 pt-3 content">
                            All Personal Information collected may be retained with us for such periods as may be necessary and to comply with our legal obligations, to resolve disputes, and enforce our agreements.
                        </p>
<p class="text-left w-100 pt-5 title">
                            Keeping your information safe
                        </p>
<p class="text-left w-100 pt-3 content">
                            We commit to maintaining the deployment of appropriate security to protect personal information wherever it is located, and whether it is in electronic or manual form. To do this we may use a variety of mechanisms depending on where the information is stored and the relationship between Exceedo Know Your Land  and any recipient organizations.
                        </p>
<p class="text-left w-100 pt-5 title">
                            Your Rights
                        </p>
<p class="text-left w-100 pt-3 content">
                            Where applicable you may have the following rights over your personal information:
                        </p>
<p class="text-left w-100 pt-5 title">
                            Request a copy of the information we hold
                        </p>
<p class="text-left w-100 pt-3 content">
                            If you wish to be informed what Personal Data we hold about you and if you want it to be removed from our systems, please contact us.
                        </p>
<p class="text-left w-100 pt-3 content">
                            In certain circumstances, you have the following data protection rights:
                        </p>
<ul class="w-100">
<li><span class="custom-gray">The right to access, update or to delete</span> the information we have on you.</li>
<li><span class="custom-gray">The right of rectification.</span> You have the right to have your information rectified if that information is inaccurate or incomplete.</li>
<li><span class="custom-gray">The right of restriction. </span>You have the right to request that we restrict the processing of your personal information.</li>
</ul>
<h3 class="pt-5">Service Providers</h3>
<p class="text-left w-100 pt-3 content">
                            We may employ third party companies and individuals to facilitate our Service (�Service Providers�), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Services are used.
                        </p>
<p class="text-left w-100 pt-3 content">
                            These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.
                        </p>
<p class="text-left w-100 pt-5 title">
                            Analytics
                        </p>
<p class="text-left w-100 pt-3 content">
                            We may use third-party Service Providers to monitor and analyze the use of our Service.
                        </p>
<ul class="w-100 pt-4">
<li>
<h4>Google Analytics</h4>
<p class="text-left w-100 pt-3 content">
                                    Google Analytics is a web analytics service offered by Google that tracks and reports website traffic. Google uses the data collected to track and monitor the use of our Service. This data is shared with other Google services. Google may use the collected data to contextualize and personalize the ads of its own advertising network.
                                </p>
<p class="text-left w-100 pt-3 content">
                                    You can opt-out of having made your activity on the Service available to Google Analytics by installing the Google Analytics opt-out browser add-on. The add-on prevents the Google Analytics JavaScript (ga.js, analytics.js, and dc.js) from sharing information with Google Analytics about visits activity.
                                </p>
<p class="text-left w-100 pt-3 content">
                                    For more information on the privacy practices of Google, please visit the Google Privacy &amp; Terms web page: <a target="_blank" href="https://www.google.com/intl/en/policies/privacy/" class="custom-gray-300  font-medium" rel="noopener">http://www.google.com/intl/en/policies/privacy/</a>
                                </p>
</li>
</ul>
<h3 class="pt-5">Payments</h3>
<p class="text-left w-100 pt-3 content">
                            For paid products and/or services of Exceedo Know Your Land, we use third-party services for payment processing (e.g. Payment Processors).
                        </p>
<p class="text-left w-100 pt-3 content">
                            We will not store or collect your payment (card/banking/UPI) details. That information is provided directly to our third-party Payment Processors whose use of your personal information is governed by their Privacy Policy.
                        </p>
<p class="text-left w-100 pt-3 content">
                            These Payment Processors acknowledges and represents that it is and shall remain compliant with the applicable information and data security mandates prescribed under the RBI Guidelines on Regulation of Payment Aggregators and Payment Gateways and with Payment Card Industry Data Security Standard (PCI DSS) or other comparable industry standards governing physical/ logical security cardholder data across Payment Processor�s environment and ensure that the card holder�s data is secured in accordance with the standards.
                        </p>
<p class="text-left w-100 pt-3 content">
                            The payment processors we work with are:
                        </p>
<p class="text-left w-100 pt-5 title">
                            Razorpay
                        </p>
<p class="text-left w-100 pt-3 content">Terms and Conditions of Razorpay can be viewed at <a target="_blank" href="https://razorpay.com/terms" class="custom-gray  font-medium" rel="noopener">https://razorpay.com/terms</a></p>
<p class="text-left w-100 pt-3 content">Privacy Policy of Razorpay can be viewed at <a target="_blank" href="https://razorpay.com/privacy" class="custom-gray  font-medium" rel="noopener">https://razorpay.com/privacy</a></p>
<h3 class="pt-5">Links To Other Sites</h3>
<p class="text-left w-100 pt-3 content">
                            Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party�s site. We strongly advise you to review the Privacy Policy of every site you visit.
                        </p>
<p class="text-left w-100 pt-3 content">
                            We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.
                        </p>
<h3 class="pt-5">Contact Us</h3>
<p class="text-left w-100 pt-3 content">
                            If you have any questions about this Privacy Policy, please contact us by using the contact information  <a href="home.html#contactUs" class="custom-gray"> we provided on our Contact page</a>
                        </p>
            </div>
            <!--//col-->
        </div>
        <!--//row-->
    </div><!-- //container -->
</section>
@endsection