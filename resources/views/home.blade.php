@extends('layouts.master')
@section('title')
Home
@endsection
@include('flash-message')
@if(!empty($className))
@section('splashscreen')

<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<div class="splashscreen {{ $className }}" id="splash">
    <div class="splashContent"> 
        <div class="splashlogo">
            <img src="{{ asset('images/splash-logo.png') }}" alt="Exceedo">
        </div>
        <span class="splashLine"></span>
        <h2>Making every square yard count</h2>
    </div>
</div>
@endsection
@endif
@section('content')
<div class="homeBanner">
    <div id="bannerSlider" class="owl-carousel"> 
        <div class="item">
            <img src="{{ asset('images/home-banner-4.jpg') }}" alt="" class="banMainImg">
            <div class="container">
                <div class="bannerContent">
                    <span class="banTopLine"></span>
                    <h2>Buy your Land</h2>
                    <p>A comprehensive land solutions platform that enables you to discover the best potential of your land and its development.</p>

                    <div class="banKnowBtn">
                        <a href="{{ route('buyyourland') }}" class="btn btn-primary">know More</a>
                    </div>
                </div><!--//bannerContent-->
            </div><!--//container-->
        </div>
        <div class="item">
            <img src="{{ asset('images/home-banner-5.jpg') }}" alt="" class="banMainImg">
            <div class="container">
                <div class="bannerContent">
                    <span class="banTopLine"></span>
                    <h2>Sell your Land</h2>
                    <p>A comprehensive land solutions platform that enables you to discover the best potential of your land and its development.</p>

                    <div class="banKnowBtn">
                        <a href="{{route('sellland') }}" class="btn btn-primary">know More</a>
                    </div>
                </div><!--//bannerContent-->
            </div><!--//container-->
        </div> 
        <div class="item">
            <img src="{{ asset('images/home-banner-3.jpg') }}" alt="" class="banMainImg">
            <div class="container">
                <div class="bannerContent">
                    <span class="banTopLine"></span>
                    <h2>Feasibility Analysis</h2>
                    <p>A comprehensive land solutions platform that enables you to discover the best potential of your land and its development.</p>

                    <div class="banKnowBtn">
                        <a href="{{ route('feasibilityanalysis') }}" class="btn btn-primary">know More</a>
                    </div>
                </div><!--//bannerContent-->
            </div><!--//container-->
        </div>
        <div class="item">
            <img src="{{ asset('images/home-banner-2.jpg') }}" alt="" class="banMainImg">
            <div class="container">
                <div class="bannerContent">
                    <span class="banTopLine"></span>
                    <h2>Land Title Search</h2>
                    <p>A comprehensive land solutions platform that enables you to discover the best potential of your land and its development.</p>

                    <div class="banKnowBtn">
                        <a href="{{ route('landtitlesearch') }}" class="btn btn-primary">know More</a>
                    </div>
                </div><!--//bannerContent-->
            </div><!--//container-->
        </div>
    </div>

    <div id="bannerThumbnails" class="owl-carousel">
        <div class="thumbBanTiles" data-dot="<button role='button' class='owl-dot'>01</button>" onclick="location.href = '{{ route('buyyourland') }}'">
            <img src="{{ asset('images/home-banner-4.jpg') }}">
            <div class="thumbContent">
                <h3>Buy your Land</h3>
                <span class="thumbHeadSep"></span>
                <p>It is a long established fact that a reader will  be distracted</p>
            </div>
        </div>
        <div class="thumbBanTiles" data-dot="<button role='button' class='owl-dot'>02</button>" onclick="location.href = '{{ route('sellland') }}'">
            <img src="{{ asset('images/home-banner-5.jpg') }}">
            <div class="thumbContent">
                <h3>Sell your Land</h3>
                <span class="thumbHeadSep"></span>
                <p>It is a long established fact that a reader will  be distracted</p>
            </div>
        </div> 
        <div class="thumbBanTiles" data-dot="<button role='button' class='owl-dot'>03</button>" onclick="location.href = '{{ route('feasibilityanalysis') }}'">
            <img src="{{ asset('images/home-banner-3.jpg') }}">
            <div class="thumbContent">
                <h3>Feasibility Analysis</h3>
                <span class="thumbHeadSep"></span>
                <p>It is a long established fact that a reader will  be distracted</p>
            </div>
        </div>
        <div class="thumbBanTiles" data-dot="<button role='button' class='owl-dot'>04</button>" onclick="location.href = '{{ route('landtitlesearch') }}'">
            <img src="{{ asset('images/home-banner-2.jpg') }}">
            <div class="thumbContent">
                <h3>Land Title Search</h3>
                <span class="thumbHeadSep"></span>
                <p>It is a long established fact that a reader will  be distracted</p>
            </div>
        </div>
    </div>
</div><!--//homeBanner-->


<section class="home_featureSection minHeight_100_2"> 
    <div class="container">
        <div class="homeSecTitle d-flex justify-content-between align-items-center">
            <h2 class="mb-0">Featured Land Listing</h2>
            <div class="featureViewBtn">
                <a href="{{ url('buy-your-land') }}" class="btn btn-primary">View All</a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                
                    @foreach($properties as $property)
                    <div class="col-sm-6">
                        <article class="featureArticle">
                            <div class="featureArtImg">
                                <a href="{{ url('buy-your-land') }}">
                                    @php
                                    $path = '';
                                    if(count($property->propertyImage)){
                                    $path = asset($property->propertyImage[0]->image);
                                    }
                                    @endphp
                                    <img src="{{ asset('storage/'.$property->image) }}" alt="{{ $property->land_name }}">
                                </a>
                            </div>
                            <div class="featureArtCont">
                                <h3>
                                    <a href="{{ url('buy-your-land') }}">
                                        <span class="featlinkText">
                                            {{ $property->size_of_land }} Acres {{ $property->land_name }} 
                                        </span>
                                        <em class="featureIcon"></em>
                                    </a>
                                </h3>
                                <p class="shortDetail">{{ $property->address }}</p>
                                <div class="featArtPric">₹ {{ $property->max_price }} Cr</div>
                                @if($property->wifi == 1 || $property->parking == 1) 
                                <div class="featFacilities">
                                    <span class="featFaTitle">Facilities :</span>
                                    @if($property->parking == 1)
                                    <span class="faci-icon parking-icon"></span>
                                    @endif
<!--                                    <span class="faci-icon noSmooking-icon"></span>-->
                                    @if($property->wifi == 1)
                                    <span class="faci-icon wifi-icon"></span>
                                    @endif
                                </div><!--//featFacilities-->
                                @endif
                            </div>
                    </div><!--//col-sm-6-->
                    @endforeach
                </div><!--row-->
                {{ $properties->links() }}
            </div><!--//col-lg-6-->

            <div class="col-lg-6 d-none d-lg-block">

                <div  id="mapArea" style="width:100%; height: 867px;"></div>
        <!--        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d46150.223079868716!2d76.7647584008476!3d30.714580997369396!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1605526928175!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>-->

            </div><!--//col-lg-6-->
        </div><!--//row-->
    </div><!--//container-->
</section><!--//home_featureSection-->
@if(count($valueAddedServices))
<section class="home_valueSection minHeight_100_2">
    <div class="container">
        <div class="valueAddedDetail">
            <div class="valueServiceSummary">
                <div class="homeSecTitle d-flex justify-content-between align-items-center">
                    <h2 class="mb-0">Value Added Services</h2>
                    <div class="featureViewBtn">
                        <a href="{{url('aboutus#moreValueAddedServices')}}" class="btn btn-primary">View All</a>
                    </div>
                </div><!--//homeSecTitle-->

                <div class="row">
                    <div class="col-lg-6">
                        <p>KYL offers a unique, one-to-one consultation service with the experts and specialists on board to resolve all land-related queries with first-hand analysis and real-time problem diagnosis.</p>
                    </div><!--//col-lg-10-->
                </div><!--//row-->
            </div><!--//valueServiceSummary--> 
        </div><!--//valueAddedDetail-->

        <div class="valueArticlesCarousel owl-carousel"> 
            @foreach($valueAddedServices as $services)
            <article class="valArticle">
                <a href="javascript:void(0);" data-toggle="modal" data-target="#serviceModal" data-id="{{ $services->id }}" data-name="{{ $services->service_name }}" class="getName modelopen">
                    @if(!empty($services->service_icon))
                    <div class="valArtIcon">
                        <img src="{{ asset($services->service_icon) }}" alt="{{ $services->service_name }}" />
                    </div>
                    @endif
                    <h3>{{ $services->service_name }}<span class="titleArrowIcon"></span></h3>
                    <p>{{ $services->description ?? '' }}</p>                            
                </a>
            </article> 
            @endforeach
        </div><!--//valueAddedArticles-->
    </div><!--//container-->
</section><!--//home_valueSection-->
@endif
<section class="home_contactSection minHeight_100_2" id="contactUs">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="contactTitle">
                    <h2>Contact Us</h2>
                    <h3>Please contact us if you need to learn more about KYL and our services.</h3>
                </div>

                <div class="contactForm">
                    <div class="alert alert-success alert-block expert-msg" style="display: none;">

                        <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>
                    <form name="contactus_form" id="contactus_form" method="post">
                        @csrf

                        <div class="form-group floating-field">
                            <input type="text" class="form-control" placeholder="Name" id="c_name" name="c_name">
                            <label for="c_name" class="floating-label">Name</label>
                        </div><!-- floating-field -->

                        <div class="form-group floating-field">
                            <input type="text" class="form-control" placeholder="Email" id="c_email" name="c_email">
                            <label for="c_email" class="floating-label">Email</label>
                        </div><!-- floating-field -->

                        <div class="form-group floating-field mobile-field">
                            <input type="text" class="form-control" placeholder="Mobile Number" id="c_mobile" name="c_mobile">
                            <label for="c_mobile" class="floating-label">Mobile Number</label>
                            <span class="mobileCode">+91</span>
                        </div><!-- floating-field -->

                        <div class="form-group floating-field">
                            <textarea class="form-control" placeholder="Message" id="c_message" name="c_message"></textarea>
                            <label for="c_message" class="floating-label">Message</label>
                        </div><!-- floating-field -->

                        <div class="contactSubmitBtn">
                            <button class="btn btn-primary" id="send_email">Submit</button>
                        </div>
                    </form>
                </div>
            </div><!--//col-md-6-->

            <div class="col-md-6">
                <div class="contactRightCont">
                    <div class="contAdditionBtns">
                        <a href="tel:9315495030" class="btn btn-phone">+919315495030</a>
                        <a href="mailto:mail@kylnow.com" class="btn btn-mail">mail@kylnow.com</a>
                    </div>

                    <div class="locationTooltipBox">
                        <a href="javascript:void(0);" class="locationIcon"></a>
                        <div class="locationTooltip">
                            <h3>Exceedo Developers & Consultants Pvt. Ltd.</h3>
                            <p>Freedom Fighter Colony, Block B, Neb Sarai, 
                                Sainik Farm, New Delhi, Delhi 110030</p>
                        </div>
                    </div>

                    <div class="viewLgMapBtn">
                       <a href="https://maps.google.com/maps?ll=28.510225,77.201031&z=17&t=m&hl=en&gl=IN&mapclient=embed&cid=15876987383248892398" class="btn" target="_blank">View Large map</a>
                    </div>
                </div><!--//contactRightCont-->
            </div><!--//col-md-6-->
        </div><!--//row-->
    </div><!--//container-->
</section><!--//home_contactSection-->

<!-- Modal -->     
<!-- serviceModal -->
<!--<div class="modal fade w-400 modelopen" id="serviceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body p-0"> 
                <a href="javascript:void(0);" class="closeBtn" data-dismiss="modal" aria-label="Close"></a>

                <div class="row no-gutters">
                    <div class="col-md-5">
                        <div class="popupFormPic">
                            <img src="images/anciliary-services-pic.png">
                        </div>
                    </div>//col-md-4
                    <div class="col-md-7">
                        <div class="popupForm">
                            <form id="frm_added_service">
                                <div class="popupHeading">
                                    <h2 id="book-appointment-title"></h2>
                                </div>
                                <input type="hidden" name="value_added_service_id" id="value_added_service_id"> 
                                <div class="form-group floating-field">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" 
                                           placeholder="Name" id="name" name="name" value="{{ old('name') }}">
                                    <label for="name" class="floating-label">Name</label>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group floating-field">
                                    <input type="text" class="form-control @error('email') is-invalid @enderror" 
                                           placeholder="Email" id="email" name="email" value="{{ old('email') }}">
                                    <label for="email" class="floating-label">Email</label>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group floating-field mobile-field">
                                    <input type="text" class="form-control @error('mobile') is-invalid @enderror" 
                                           placeholder="Mobile Number" id="mobile" name="mobile" value="{{ old('mobile') }}">
                                    <label for="mobile" class="floating-label">Mobile Number</label>
                                    <span class="mobileCode">+91</span>
                                    @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group floating-field mb-20">
                                    <textarea class="form-control @error('comments') is-invalid @enderror" 
                                              placeholder="Comments" id="comments" name="comments">{{old('comments')}}
                                    </textarea>
                                    <label for="comments" class="floating-label">Comments</label>
                                    @error('comments')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="formBtn">
                                    <button type="submit" id="service-enquiry" class="btn btn-primary btn-submit"> Submit </button>
                                </div>
                            </form>
                        </div>//popupForm
                    </div>//col-md-8
                </div>//row

            </div>//modal-body 
        </div>
    </div>
</div>//modal-->

<!-- landDetail-popup -->
<div id="landDetail-popup" class="mfp-with-anim mfp-hide">
    <button title="Close (Esc)" type="button" class="mfp-close"></button>
    <div class="popup-body propertyPopupBody">
    </div><!-- //popup-body --> 
</div><!-- //landDetail-popup -->

@endsection
@section('custom-script')
@if(!empty($className))
<script>
$(document).ready(function(){
        setTimeout(function () {
    $("#splash").fadeOut();
    }, 2200);
    });</script>

@endif
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_map_key')}}&callback=initMap&libraries=&v=weekly" async></script>

<script>
            var map;
            function initMap() {
            map = new google.maps.Map(document.getElementById('mapArea'), {
<?php
if (count($properties)) {
    foreach ($properties as $key => $lon) {
        if ($key == 0) {
            ?>
                        center:  {lat: <?php echo $lon->latitute ?>, lng: <?php echo $lon->longitude ?>},
            <?php
        }
    }
} else {
    ?>
                center:  {lat: 40.869176, lng: - 74.943978},
    <?php
}
?>
            zoom: 10,
                    styles: [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]}, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"elementType": "labels.text.fill", "stylers": [{"color": "#616161"}]}, {"elementType": "labels.text.stroke", "stylers": [{"color": "#f5f5f5"}]}, {"featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{"color": "#bdbdbd"}]}, {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#eeeeee"}]}, {"featureType": "poi", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"}]}, {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"}]}, {"featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}, {"featureType": "road", "elementType": "geometry", "stylers": [{"color": "#ffffff"}]}, {"featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{"color": "#757575"}]}, {"featureType": "road.highway", "elementType": "geometry", "stylers": [{"color": "#dadada"}]}, {"featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{"color": "#616161"}]}, {"featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}, {"featureType": "transit.line", "elementType": "geometry", "stylers": [{"color": "#e5e5e5"}]}, {"featureType": "transit.station", "elementType": "geometry", "stylers": [{"color": "#eeeeee"}]}, {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#c9c9c9"}]}, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}]
            });
//var warrenCountyCoordinates = [

<?php
//foreach ($properties as $lon) {
?>
//    {lat: <?php //echo $lon->latitute            ?>, lng: <?php //echo $lon->longitude            ?>},
<?php // }            ?>
//];
// Random polygon covering northeast region
//var everythingElse = [
<?php
//foreach ($properties as $lon) {
?>
//    {lat: <?php //echo $lon->latitute            ?>, lng: <?php //echo $lon->longitude            ?>},
<?php // }            ?>
//];
//var warrenCountyBorder = new google.maps.Polygon({
//paths: [everythingElse, warrenCountyCoordinates],
//        strokeColor: '#000000',
//        strokeOpacity: 0.8,
//        strokeWeight: 2,
//        fillColor: '#000000',
//        fillOpacity: 0.35
//});
            var breweries = [
<?php
if (count($properties)) {
    foreach ($properties as $lon) {
        $image = asset($lon->image);
        ?>
                    {
                    position: new google.maps.LatLng(<?php echo $lon->latitute ?>, <?php echo $lon->longitude ?>),
                            desc: '<div id="content">' +
                            '<div id="siteNotice">' +
                            '</div>' +
                            '<h4 id="firstHeading" class="firstHeading"><?php echo $lon->land_name ?></h4>' +
                            '<div id="bodyContent">' +
                            '<p>The <b>Summerfield Brews</b> <?php echo $lon->address; ?>' +
                            '</p><br>' +
                            '</div>' +
                            '</div>'
                    }
                    ,
        <?php
    }
}
?>
            ];
            var currentMarker = false
                    function addMarker(brewery) {
                    var marker = new google.maps.Marker({
                    position: brewery.position,
                            icon: 'http://www.jeffs-icons.com/map_icons/ICONS/Pin/pin-red_violet-R.png',
                            map: map,
                            zIndex: brewery[1]
                    });
                    marker.info = new google.maps.InfoWindow({
                    content: brewery.desc,
                            maxWidth: 300
                    });
                    // When marker is clicked, open infowindow and close any open ones
                    google.maps.event.addListener(marker, 'click', function () {
                    if (currentMarker) {
                    currentMarker.close();
                    }
                    currentMarker = marker.info
                            currentMarker.open(map, marker);
                    });
                    } // End addMarker() function

// create marker from each brewery in our list
            for (var i = 0, brewery; brewery = breweries[i]; i++) {
            addMarker(brewery);
            }

// Add the county border layer to the map
//warrenCountyBorder.setMap(map);
            }
</script>
<script type="text/javascript">
    $(document).ready(function(){

    document.cookie = "homebanner=10";
    $('.productList').on('click', function (e) {
    e.preventDefault();
    var productId = $(this).data('id');
    var baseUrl = $('meta[name="base_url"]').attr('content');
    $.ajaxSetup({
    headers: {
    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
    url: baseUrl + "/property-detail",
            type: "post",
            data: {
            id: productId,
            },
            success: function (data) {
            if (data) {
            console.log(data);
            $('.propertyPopupBody').html(data);
            $.magnificPopup.open({
            items: {
            src: '#landDetail-popup',
                    callbacks: {
                    beforeOpen: function () {
                    this.st.mainClass = this.st.el.attr('data-effect');
                    }
                    },
                    type: 'inline'
            }

            });
            }
            }
    });
    });
    });</script>

<script type="text/javascript">

    $('#contactus_form').submit(function (e) {

    e.preventDefault();
    $(document).find("span.error-span").remove();
    $('.alert-block').hide();
    var formData = new FormData(this);
    $('#send_email').prop("disabled", true);
    $('#send_email').text('Sending...');
    $.ajax({

    type: 'POST',
            url: "{{ route('sendemail') }}",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
            $('#send_email').prop("disabled", false);
            $('#send_email').text('Submit');
            if (data.success) {
            $('#contactus_form')[0].reset();
            $('.expert-msg').show().append('<strong>' + data.msg + '</strong>');
            //$('#send_email_thankyou_msg').html(data.msg);
            } else {
            $('.expert-msg').show().removeClass('alert-success').addClass('alert-danger').append('<strong>' + data.msg + '</strong>');
            }
            },
            error: function (errorResponse) {
            $('#send_email').prop("disabled", false);
            $('#send_email').text('Submit');
            $.each(errorResponse.responseJSON.errors, function (field_name, error) {

            $(document).find('[for=' + field_name + ']').after('<span class="text-strong error-span" role="alert">' + error + '</span>');
            })
            }

    });
    });
</script>
@endsection