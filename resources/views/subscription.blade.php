@extends('layouts.master')
@section('title')
Home
@endsection
@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    section.pricing {
        background: #007bff;
        background: linear-gradient(to right, #0062E6, #33AEFF);
    }

    .pricing .card {
        border: none;
        border-radius: 1rem;
        transition: all 0.2s;
        box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
        width: 100%;
    }

    .pricing hr {
        margin: 1.5rem 0;
    }

    .pricing .card-title {
        margin: 0.5rem 0;
        font-size: 0.9rem;
        letter-spacing: .1rem;
        font-weight: bold;
    }

    .pricing .card-price {
        font-size: 3rem;
        margin: 0;
    }

    .pricing .card-price .period {
        font-size: 0.8rem;
    }

    .pricing ul li {
        margin-bottom: 1rem;
    }

    .pricing .text-muted {
        opacity: 0.7;
    }

    .pricing .btn {
        font-size: 80%;
        border-radius: 5rem;
        letter-spacing: .1rem;
        font-weight: bold;
        padding: 1rem;
        opacity: 0.7;
        transition: all 0.2s;
    }

    /* Hover Effects on Card */

    @media (min-width: 992px) {
        .pricing .card:hover {
            margin-top: -.25rem;
            margin-bottom: .25rem;
            box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.3);
        }
        .pricing .card:hover .btn {
            opacity: 1;
        }
    }
</style>
<section class="pricing py-5">
    <div class="container">
        <div class="row">
            @foreach($packages as $package)
            <!-- Free Tier -->
            <div class="col-lg-4">
                <div class="card mb-5 mb-lg-0">
                    <form id="package{{ $package->id }}">
                        <div class="card-body">
                            <input type="hidden" name="package_id" value="{{ $package->title }}">
                            <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                            <h5 class="card-title text-muted text-uppercase text-center">{{ $package->title }}</h5>
                            <h6 class="card-price text-center">{{ __('INR') }} {{ $package->price }}<span class="period">/{{ $package->duration }}month</span></h6>
                            <hr>
                            @if($package->unlimited != 1)
                            <div class="form-group">
                                <select class="form-control propertyId" name="propertyId[]"  multiple="multiple" data-limit="{{ $package->show_land }}">
                                    @foreach($properties as $property)
                                    <option value="{{ $property->id }}">{{ $property->land_name }}</option>
                                    @endforeach
                                </select>
                            </div><!-- //form-group -->
                            @endif
                            <ul class="fa-ul">
                                @if($package->unlimited == 1)
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>{{ __('Unlimited') }}</li>
                                @else
                                @if(!empty($package->show_land))
                                <li><span class="fa-li"><i class="fas fa-check"></i></span>{{ $package->show_land }} {{ __('land Detail show') }}</li>
                                @endif
                                @endif

                            </ul>
                            <button  class="btn btn-block btn-primary text-uppercase userBuyPackage" type="button" data-package="{{'package'.$package->id}}" data-id="{{ $package->id }}">Button</button>
                        </div>
                    </form>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
<form id="userSubscriptionForm">
    <input type="hidden" name="amount" value="" id="packageAmount" value="">
    <input type="hidden" name="packageId" value="" id="packageId" value="">
    <input type="hidden" name="razorpay_payment_id" value="" id="razorpay_payment_id">
    <input type="hidden" name="razorpay_order_id" value="" id="razorpay_order_id">
    <input type="hidden" name="razorpay_signature" value="" id="razorpay_signature">
    <input type="hidden" name="property" value="" id="propertyListId" multiple="multiple">

</form>
@endsection
@section('custom-script')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.userBuyPackage').on('click', function (e) {
        e.preventDefault()
        var package = $(this).data('package');
        var id = $(this).data('id');
        $('#propertyListId').val($('.propertyId').val());
        $('#packageId').val(id);
        $.ajax({
            type: 'post',
            url: "user-subscription-token-generate",
            data: {
                packageId: id,
            },
            success: function (data) {
                var order_id = data.order_id;
                var options = {
                    "key": "{{ config('app.razorpay_api_key') }}", // Enter the Key ID generated from the Dashboard
                    "amount": data.amount * 100, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                    "currency": "{{ config('app.currency') }}",
                    "name": "{{ config('app.account_name') }}",
//                        "description": remarks,
                    "image": "{{ asset('images/logo-black.svg') }}",
                    "order_id": order_id, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                    "handler": function (response) {
                        $('#razorpay_payment_id').val(response.razorpay_payment_id);
                        $('#razorpay_order_id').val(response.razorpay_order_id);
                        $('#razorpay_signature').val(response.razorpay_signature);
                        paymentStore();
                    },
                    "prefill": {
                        "name": "{{ auth()->user()->name }}",
                        "email": "{{ auth()->user()->email }}",
                        "contact": "{{ auth()->user()->mobile }}"
                    },
//            "notes": {
//                "address": "Razorpay Corporate Office"
//            },
                    "theme": {
                        "color": "#3399cc"
                    }
                };
                var rzp1 = new Razorpay(options);
                rzp1.on('payment.failed', function (response) {

                });

                rzp1.open();

            },

        });

    });
});

function paymentStore() {
    $.ajax({
        type: 'post',
        url: "user-subscription-payment",
        data: $('#userSubscriptionForm').serialize(),
        success: function (data) {
            if (data.success) {
                swal({
                    text: data.msg,
                    icon: "success",
                }).then((value) => {
                    window.location.href = "{{ url('buy-your-land') }}";
                });
            } else {
                swal({
                    text: data.msg,
                    icon: "error",
                });
            }
        }
    });
}
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
jQuery(document).ready(function () {
    $(".propertyId").select2({
        tags: true,
        maximumSelectionLength: $('.propertyId').data('limit'),
        placeholder: "Land List"
    });


})

</script>
@endsection