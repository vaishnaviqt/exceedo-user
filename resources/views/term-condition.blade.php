@extends('layouts.master')

@section('title')
 Terms & Conditions
@endsection
@section('content')
<section class="commonFormPage adjustHeaderSpace staticPage">
    <div class="heading-title">
        <h2>Terms &amp; Conditions</h2>
    </div>
    <!--//heading-title-->
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5">
                <p class="text-left w-100 pt-3 title">
                            PLEASE READ THE FOLLOWING TERMS OF USE AND DISCLAIMERS CAREFULLY BEFORE USING THIS WEB SITE
                        </p>
<p class="text-left w-100 pt-5 title">
                            Acceptance of terms
                        </p>
<p class="text-left w-100 pt-3 content">
                            Exceedo Know Your Land  maintains this Website (the �Site�). Your access to and use of this Site is subject to the following Terms of Use. Exceedo Know Your Land  reserves the right to update these Terms of Use at any time without any prior notice to you. The most current version of the Terms of Use may be accessed by clicking on the �Terms of Use� hypertext link located at the bottom of the Site. By using this Site, you accept, without limitation or qualification, these Terms of Use. If you do NOT agree to these Terms of Use, please do NOT use this Site.
                        </p>
<p class="text-left w-100 pt-5 title">
                            Limited License
                        </p>
<p class="text-left w-100 pt-3 content">
                            Exceedo Know Your Land  grants you a non-exclusive, non-transferable, revocable license to access and use our Website in order for you to make purchases of electronic documents and related services through our Website, strictly in accordance with our Legal Terms.
                        </p>
<p class="text-left w-100 pt-5 title">
                            Accuracy and completeness of information
                        </p>
<p class="text-left w-100 pt-3 content">
                            While Exceedo Know Your Land  strives to ensure that the information contained in this Site is accurate and reliable, Exceedo Know Your Land makes no warranties or representations as to the accuracy, correctness, reliability or otherwise with respect to such information, and assumes no liability or responsibility for any omissions or errors in the content of this Site.
                        </p>
<p class="text-left w-100 pt-5 title">
                            Modification of Site
                        </p>
<p class="text-left w-100 pt-3 content">
                            Exceedo Know Your Land  will periodically revise the information, services and resources contained in this Site and reserves the right to make such changes without any obligation to notify past, current or prospective visitors.
                        </p>
<p class="text-left w-100 pt-5 title">
                            Your use of the Site
                        </p>
<p class="text-left w-100 pt-3 content">
                            You may download content for non-commercial, personal use only, provided copyright, trademark or other proprietary notices remain unchanged and visible. No right, title or interest in any downloaded materials is transferred to you as a result of any such downloading or copying. You agree that you will not otherwise copy, modify, alter, display, distribute, sell, broadcast or transmit any material on the Site in any manner without the written permission of Exceedo Know Your Land.
                        </p>
<p class="text-left w-100 pt-5 title">
                            No unlawful or prohibited use
                        </p>
<p class="text-left w-100 pt-3 content">
                            As a condition of your use of the Site, you will not use the Site for any purpose that is unlawful or prohibited by these Terms of Use or any applicable laws.
                        </p>
<p class="text-left w-100 pt-5 title">
                            Unsolicited submissions
                        </p>
<p class="text-left w-100 pt-3 content">
                            Exceedo Know Your Land  does not accept or consider any creative ideas, suggestions or materials from the public (�Submissions�), therefore, you should not make any Submissions to Exceedo Know Your Land. If you do send us a Submission, despite our request not to do so, then such Submission will be considered non-confidential and non-proprietary and shall immediately become the property of Exceedo Know Your Land.
                        </p>
<p class="text-left w-100 pt-5 title">
                            Privacy policy
                        </p>
<p class="text-left w-100 pt-3 content">
                            Exceedo Know Your Land�s use of any personal data you submit to the Site is governed by the Site�s <a href="https://www.kylnow.com/privacy_policy" class="custom-gray">Privacy Policy</a></p>
<p>.
                        </p>
<p class="text-left w-100 pt-5 title">
                            Disclaimers
                        </p>
<p class="text-left w-100 pt-3 content">
                            THE WEBSITE IS PROVIDED ON AN �AS IS� BASIS. EXCEEDO KNOW YOUR LAND EXPRESSLY DISCLAIMS ALL WARRANTIES, INCLUDING THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. EXCEEDO KNOW YOUR LAND DISCLAIMS ALL RESPONSIBILITY FOR ANY LOSS, INJURY, CLAIM, LIABILITY OR DAMAGE OF ANY KIND RESULTING FROM, ARISING OUT OF OR ANY WAY RELATED TO
                        </p>
<ol class="w-100 pt-3">
<li>(A) ANY ERRORS IN OR OMISSIONS FROM THIS WEBSITE AND THE CONTENT, INCLUDING BUT NOT LIMITED TO TECHNICAL INACCURACIES AND TYPOGRAPHICAL ERRORS, </li>
<li>(B) ANY THIRD PARTY WEB SITES OR CONTENT THEREIN DIRECTLY OR INDIRECTLY ACCESSED THROUGH LINKS IN THIS WEBSITE, INCLUDING BUT NOT LIMITED TO ANY ERRORS IN OR OMISSIONS THEREFROM,</li>
<li>(C) THE UNAVAILABILITY OF THE WEBSITE OR ANY PORTION THEREOF, </li>
<li>(D) YOUR USE OF THIS WEBSITE OR (E) YOUR USE OF ANY EQUIPMENT OR SOFTWARE IN CONNECTION WITH THE WEBSITE.</li>
</ol>
<p class="text-left w-100 pt-5 title">
                            Limitation of liability
                        </p>
<p class="text-left w-100 pt-3 content">
                            In no event and under no legal or equitable theory, whether in tort, contract, strict liability or otherwise, shall Exceedo Know Your Land be liable for any direct, indirect, special, incidental or consequential damages arising out of any use of the information contained herein, including, without limitation, damages for lost profits, loss of goodwill, loss of data, work stoppage, accuracy of results, or computer failure or malfunction.
                        </p>
<p class="text-left w-100 pt-5 title">
                            Indemnification
                        </p>
<p class="text-left w-100 pt-3 content">
                            You agree to defend, indemnify and hold Exceedo Know Your Land harmless from and against any and all claims, damages, costs and expenses, including attorney�s fees, arising from and related to your use of the Site.
                        </p>
<p class="text-left w-100 pt-5 title">Copyright s &amp; Trademarks</p>
<p class="text-left w-100 pt-3 content">
                            Unless otherwise noted, all materials including without limitation, logos, brand names, images, designs, photographs, video clips and written and other materials that appear as part of our Website are copyrights, trademarks, service marks, trade dress and/or other intellectual property whether registered or unregistered (�Intellectual Property�) owned, controlled or licensed by Exceedo Know Your Land.
                        </p>
<p class="text-left w-100 pt-3 content">
                            Nothing on our Website should be construed as granting, by implication, estoppel or otherwise, any license or right to use any Intellectual Property displayed or used on our Website, without the prior written permission of the Intellectual Property owner. Exceedo Know Your Land aggressively enforces its intellectual property rights to the fullest extent of the law. The names and logos of Exceedo Know Your Land, may not be used in any way, including in advertising or publicity pertaining to distribution of materials on our Website, without prior, written permission from Exceedo Know Your Land. Exceedo Know Your Land prohibits use of any logo of Exceedo Know Your Land or any of its affiliates as part of a link to or from any Website unless Exceedo Know Your Land approves such link in advance and in writing. Fair use of Exceedo Know Your Land�s Intellectual Property requires proper acknowledgment. Other product and company names mentioned in our Website may be the Intellectual Property of their respective owners.
                        </p>
<p class="text-left w-100 pt-5 title">Links to third-party sites</p>
<p class="text-left w-100 pt-3 content">
                            As a convenience to users, this Site may link to other sites owned and operated by third parties and not maintained by Exceedo Know Your Land. However, even if such third parties are affiliated with Exceedo Know Your Land . Exceedo Know Your Land has no control over these linked sites, all of which have separate privacy and data collection practices and legal policies independent of Exceedo Know Your Land. Exceedo Know Your Land is not responsible for the contents of any linked sites and does not make any representations regarding the content or accuracy of material on such sites. Viewing such third party sites is entirely at your own risk.
                        </p>
<p class="text-left w-100 pt-5 title">Cautionary language regarding forward-looking statements</p>
<p class="text-left w-100 pt-3 content">
                            This Site may contain statements, estimates or projections that constitute �forward-looking statements� . Any such forward looking statements are inherently speculative and are based on currently available information, operating plans and projections about future events and trends. As such, they are subject to numerous risks and uncertainties. Actual results and performance may be significantly different from Exceedo Know Your Land�s historical experience and our present expectations or projections. Exceedo Know Your Land undertakes no obligation to publicly update or revise any forward-looking statements.
                        </p>
<p class="text-left w-100 pt-5 title">Jurisdiction</p>
<p class="text-left w-100 pt-3 content">
                            Exceedo Know Your Land maintains and operates this Site from its office in Delhi. These Terms of Use are governed and interpreted under the laws of the State of Delhi, India. By using this site you consent to the jurisdiction of the courts located in South Delhi District  for any action arising from these Terms of Use. Any cause of action or claim you may have with respect to the Site must be commenced within one (1) year after the claim or cause of action arises. If any portion of these Terms of Use is deemed unlawful, void or unenforceable, then that part shall be deemed severable and shall be construed in accordance with applicable law. Such a term will not affect the validity and enforceability of any remaining provisions. Exceedo Know Your Land�s failure to act with respect to a breach of these Terms of Use by you or others does not constitute a waiver and shall not limit Exceedo Know Your Land�s rights with respect to such breach or any subsequent breaches.
                        </p>
<p class="text-left w-100 pt-5 title">Refunds</p>
<p class="text-left w-100 pt-3 content">
                            For paid products and/or services of Exceedo Know Your Land, we use third-party services for payment processing (e.g. Payment Processors).
                        </p>
<p class="text-left w-100 pt-3 content">
                            The payment processors we work with are:
                        </p>
<p class="text-left w-100 pt-3 content">
                            <span class="custom-gray">Razorpay Privacy Policy can be viewed at </span><a target="_blank" href="https://razorpay.com/privacy" rel="noopener">https://razorpay.com/privacy</a>
                        </p>
<p class="text-left w-100 pt-3 content">
                            The refunds shall be guided by the terms and conditions of payment gateway Razorpay, which can be viewed at <a target="_blank" href="https://razorpay.com/terms" rel="noopener">https://razorpay.com/terms</a>
                        </p>
            </div>
            <!--//col-->
        </div>
        <!--//row-->
    </div><!-- //container -->
</section>
@endsection